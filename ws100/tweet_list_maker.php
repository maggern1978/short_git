<?php

include_once 'functions.php';

//$mode = 'users';
//$mode = 'breaking';

if (!isset($mode))
{
	$mode = 'users';
}

if ($mode == 'users')
{
  $readdirectory = 'users/';
  $savefile = 'data/displaylist/user_display_list';
}
else
{
  $readdirectory = 'breaking/';
  $savefile = 'data/displaylist/breaking_display_list';
}

$user_list = listfiles($readdirectory);

$tweets = [];
$media = [];
$replies = [];

foreach ($user_list as $user)
{

	$localfiles = listfiles($readdirectory . $user . '/tweets/');

    if (!empty($localfiles))
    {

    	foreach ($localfiles as $index => $file)
    	{
    		
    		$array = [];
    		$array[] = $tempfile = str_replace(".json", "", $file);
    		$array[] = $user; 

    		if(strpos($tempfile, '_media') !== false and strpos($tempfile, '_reply') !== false ) //both
    		{
    			$media[] = $array;
    			$replies[] = $array;
    		}
    		else if(strpos($tempfile, '_media') !== false) //onlye media
    		{
    			$media[] = $array;
    			$tweets[] = $array;
                $replies[] = $array;
    		}
    		else if(strpos($tempfile, '_reply') !== false) //_reply
    		{
    			$replies[] = $array;
    		}
    		else
    		{
    			$tweets[] = $array;
                $replies[] = $array;
    		}					

    	}

    }

}

usort($tweets, function ($item1, $item2) 
{
    $temp1 = str_replace("_media,", "", $item1[0]);
    $temp2 = str_replace("_media,", "", $item2[0]);

    if ((int)$temp1 < (int)$temp2)
    {
    	return true;
    }
    else
    {
    	return false;
    }
    
});

usort($replies, function ($item1, $item2) {
    $temp1 = str_replace("_reply,", "", $item1[0]);
    $temp2 = str_replace("_reply,", "", $item2[0]);

    $temp1 = str_replace("_media,", "", $temp1);
    $temp2 = str_replace("_media,", "", $temp2);    

    if ((int)$temp1 < (int)$temp2)
    {
    	return true;
    }
    else
    {
    	return false;
    }

});

usort($media, function ($item1, $item2) {

    $temp1 = str_replace("_reply,", "", $item1[0]);
    $temp2 = str_replace("_reply,", "", $item2[0]);

    $temp1 = str_replace("_media,", "", $temp1);
    $temp2 = str_replace("_media,", "", $temp2);   

    if ((int)$temp1 < (int)$temp2)
    {
    	return true;
    }
    else
    {
    	return false;
    }

});

saveCSVx($tweets, $savefile . '_tweets.csv');
saveCSVx($replies, $savefile . '_replies.csv');
saveCSVx($media, $savefile . '_media.csv');


?>