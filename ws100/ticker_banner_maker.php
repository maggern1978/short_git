<?php

include_once('../code/functions.php');
include_once('../code/functions_flashtweets.php');

$listfiles = listfiles('data/ticker_list/');

if (count($listfiles) > 1)
{
	echo 'Error, too many files in data/ticker_list. Returning!';
	return;
}

if (!$tickers = readCSVtab('data/ticker_list/' . $listfiles[0]))
{
	echo 'Error reading ticker list!';
	exit();
}


$bulkTickerData = [];


//Kode,Navn,Kallenavn
foreach ($tickers as $key => $ticker)
{
	
	if (!isset($ticker[1]) or $ticker[1] == '' or !isset($ticker[2]) or $ticker[2] == '')
	{
		continue;
	}


	if(mb_strtolower($ticker[0]) == 'kode' or mb_strtolower($ticker[1]) == 'navn' or mb_strtolower($ticker[2]) == 'kallenavn' )
	{
		continue;
	}

		
	if (!isset($ticker[4]) or $ticker[4] != 'ja')
	{
		//echo 'Mode er forside, skipping ' . $ticker[0] . '<br>';
		
		continue;
	}
	else 
	{
		echo 'Adding ' . $ticker[0]  . '<br>';
	}

	$ticker[1] = trim($ticker[1]);
	$ticker[2] = trim($ticker[2]);
	$ticker[0] = trim($ticker[0]);

	$bulkTickerData[] = $ticker;

	
}

$alltickers = [];

foreach ($bulkTickerData as $file)
{
	if (!$data = readJSON('../code/data/tickers/' . mb_strtolower($file[0]) . '.json'))
	{
		echo 'Error reading ' . $file[0] . '<br>';
	}
	else
	{
		$alltickers[] = $data;
	}
}

ob_start();
?>

<div class="d-flex flex-wrap justify-content-center bg-dark text-white ticker-holder ">

	<?php displayticker($alltickers, 'S&P 500', 1,true, 0); ?>

	<?php displayticker($alltickers, 'Nasdaq', 1,false, 0); ?>
	<?php displayticker($alltickers, 'Dow Jones', 1,false,0); ?>
	<?php displayticker($alltickers, 'Crude Oil', 2,false,2); ?>
	<?php displayticker($alltickers, 'Gold', 2, false, 0); ?>
	<?php displayticker($alltickers, 'EUR/USD', 3, false,3); ?>
	<?php displayticker($alltickers, 'Bitcoin', 4, false,0); ?>
	<?php displayticker($alltickers, 'US 10-YR', 3, false,2); ?>
</div>

<?php

$content = ob_get_contents();

if (strpos($content , 'error') !== false or strpos($content , 'in line') !== false) {
	echo 'Error found, will not update. <br>';
	return;
}

if ($f = fopen("../public_html/ticker_line.html", "w"))
{
	fwrite($f, $content);
	fclose($f);
}
else
{
	echo 'Error while writing to file!<br>';
}



function displayticker($alltickers, $target, $rank, $first, $decimals)
{


	foreach ($alltickers as $ticker) 
	{
		if ($ticker == false)
		{
			continue;
		}

		if ($target != $ticker['name'])
		{
			continue;
		}
		
		if ($ticker['change_p'] > 0)
		{
			$subfix = 'plus';
		}
		else if ($ticker['change_p'] < 0)
		{
			$subfix = 'minus';
		}		
		else if ($ticker['change_p'] == 0)
		{
			$subfix = 'zero';
		}		


		if ($rank == 1)
		{
			$visibility = '';
		}
		else if ($rank == 2)
		{
			$visibility = 'd-none d-sm-block';
		}
		else if ($rank == 3)
		{
			$visibility = 'd-none d-sm-none d-md-block';
		}
		else if ($rank == 4)
		{
			$visibility = 'd-none d-sm-none d-md-none d-lg-block';
		}
		else if ($rank == 5)
		{
			$visibility = 'd-none d-sm-none d-md-none d-lg-none d-xl-block';
		}							
		else
		{
			$visibility = '';
		}

		$time_ago_string = time_elapsed_string($ticker['timestamp']); //utc

		?>
		<div class=" <?php echo $visibility; ?>">
		<div class="d-none d-sm-block">
				<div class="d-flex flex-row " data-toggle="tooltip" title="Updated <?php echo $time_ago_string ;?>">
					<div class="ticker-text ticker-name <?php if ($first == true){echo ' ml-4 '; } ?> mr-2 mt-1 mb-1 font-weight-bold "><a class="text-white" href="ticker.php?ticker=<?php echo $ticker['code'] . '&name='. urlencode($target); ?>"><?php echo $ticker['name'];?></a>
					</div>
					<div class="d-none d-sm-none d-md-block ticker-text ticker-close mr-2 mt-1 mb-1"><?php echo number_format(round($ticker['close'],$decimals),$decimals,'.',',');?>
					</div>	
					<div class="ticker-change mr-4 mt-1 mb-1 ticker-change-<?php echo $subfix;?> "><?php echo round($ticker['change_p'],2);?>%
					</div>	
				</div>
			</div>

			<div class="d-sm-none d-md-none d-lg-none d-xl-none d-flex flex-column flex-sm-row align-items-center justify-content-center mx-2 my-1" data-toggle="tooltip" title="Updated <?php echo $time_ago_string; ?>">
				<div class="ticker-text ticker-name font-weight-bold"><a class="text-white" href="ticker.php?ticker=<?php echo $ticker['code'] . '&name='. urlencode($target); ?>"><?php echo $ticker['name'];?></a>
				</div>
				<div class="d-flex flex-row justify-content-center text-center mx-auto">
					<div class="ticker-text ticker-close mr-1 mt-1 mb-1"><?php echo number_format(round($ticker['close'],$decimals),$decimals,'.',',') ;?>
					</div>	
					<div class="ticker-change mt-1 mb-1 ticker-change-<?php echo $subfix;?> "><?php echo round($ticker['change_p'],2);?>%
					</div>	
				</div>
			</div>

		</div>

		<?php 
		break;
	}
	
}


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}



?>