<?php

require_once('../vendor/autoload.php');
require_once('../code/functions.php');

use Dusterio\LinkPreview\Client;

//load list
if (!$csv = readCSVkomma('data/displaylist/user_display_list_replies.csv'))
{
	echo 'Read error of preview.csv...returningen. <br>';
	return;
}

$mode = '';

$round = 0;

foreach ($csv as $key => $entry)
{

	$stripped_file_name = str_replace("_media", '', $entry[0]);
	$stripped_file_name = str_replace("_reply", '', $stripped_file_name);

	if (file_exists('../code/previews/users/' . $stripped_file_name . '.json') or file_exists( '../code/previews/breaking/' . $stripped_file_name . '.json'))
	{
		//echo $key . '. Already downloaded, skipping...<br>';
		continue;
	}

	if (!isset($entry[1]))
	{
		continue;
	}

	if (file_exists('../code/users/' . mb_strtolower($entry[1]) . '/tweets/' . $entry[0] . '.json'))
	{

		if (!$data = readJSON('../code/users/' . mb_strtolower($entry[1]) . '/tweets/' . $entry[0] . '.json'))
		{
			echo 'Read error of ' . $entry[0] . ' ' . $entry[1] . '<br>';
			continue;
		}

		$mode = 'users';

	}
	else if (file_exists('../code/breaking/' . mb_strtolower($entry[1]) . '/tweets/' . $entry[0] . '.json'))
	{
		if (!$data = readJSON('../code/breaking/' . mb_strtolower($entry[1]) . '/tweets/' . $entry[0] . '.json'))
		{
			echo 'Read error of ' . $entry[1] . ' ' . $entry[0] . '<br>';
			continue;
		}

		$mode = 'breaking';
	}
	else
	{
		echo 'No file found...<br>';
		var_dump($entry);
		echo '../code/users/' . mb_strtolower($entry[1]) . '/tweets/' . $entry[0] . '.json';
		continue;
	}

	if (isset($data['data']['entities']['urls']))
	{

		$previews = [];

		foreach ($data['data']['entities']['urls'] as $index => $url)
		{

			if (isset($url['title']) and isset($url['description']) and isset($url['images']) and (isset($url['expanded_url']) or isset($url['unwound_url']) or isset($url['url'])))
			{
				echo $key . '. -> ' . $index . '. All set, skippping...<br>';
				continue;
			}
			else
			{
				echo $key . '. -> ' . $index . '. Data missing, downloading preview info...<br>';

				//early save to only give one download chance!
				if ($mode == 'users')
				{
					saveJSON($previews, '../code/previews/users/' . $stripped_file_name . '.json');
				}
				else if ($mode == 'breaking')
				{
					saveJSON($previews, '../code/previews/breaking/' . $stripped_file_name . '.json');
				}


				echo '<br>---------------<br>';

				$url_local = '';

				$url_source = '';

				if(isset($url['unwound_url']))
				{
					$url_local = $url['unwound_url'];
					$url_source = 'unwound_url';
				}	
				else if(isset($url['url']))
				{
					$url_local = $url['url'];
					$url_source = 'url';
				}	
				else if(isset($url['expanded_url']))
				{
					$url_local = $url['expanded_url'];
					$url_source = 'expanded_url';
				}	
			
				//var_dump($url);

				$previewClient = new Client($url_local);
				$preview = $previewClient->getPreview('general');
				$preview = $preview->toArray();

		
				$previews[$index] = $preview;
				$previews[$index]['entities'] = $data['data']['entities']['urls'];
				$previews[$index]['url_source'] = $url_local;

				if ($preview['cover'] == '' and $preview['title'] == '')
				{
					echo 'Cover and title not set, nulling...<br>';
					$previews[$index] = null;
				}
				
			}
		}

	}
	else
	{
		//echo 'NOT SET<br>';
		//var_dump($data);
		continue;
	}


	if ($mode == 'users')
	{
		saveJSON($previews, '../code/previews/users/' . $stripped_file_name . '.json');
	}
	else if ($mode == 'breaking')
	{
		saveJSON($previews, '../code/previews/breaking/' . $stripped_file_name . '.json');
	}	


	if ($round++ > 100)
	{
		break;
	}

}



?>