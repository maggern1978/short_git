<?php 

include_once('functions.php');
include_once('functions_flashtweets.php');

if (!isset($mode))
{
	$mode = 'forside'; //normal // bonds //forside
}

$listfiles = listfiles('data/ticker_list/');

if (count($listfiles) > 1)
{
	echo 'Error, too many files in data/ticker_list!!';
}

if (!$tickers = readCSVtab('data/ticker_list/' . $listfiles[0]))
{
	echo 'Error reading ticker list!';
	exit();
}

echo '<h3>Mode er ' . $mode . '</h3>';
echo 'Server time frame is ' . date('H:i') . '<br>';
$bulkTickerData = [];
$now = date('Hi');
//$now = '1500';
//echo 'Now is ' . $now . ' <br>';


$skipped_array = [];

//Kode,Navn,Kallenavn
foreach ($tickers as $key => $ticker)
{
	
	if (!isset($ticker[1]) or $ticker[1] == '' or !isset($ticker[2]) or $ticker[2] == '')
	{
		continue;
	}

	if (mb_strtolower($ticker[0]) == 'kode' or mb_strtolower($ticker[1]) == 'navn' or mb_strtolower($ticker[2]) == 'kallenavn' )
	{
		continue;
	}

	if ($mode == 'forside')
	{
		
		if (trim($ticker[4]) != 'ja')
		{
			//echo 'Mode er forside, skipping ' . $ticker[0] . '<br>';
			continue;
		}
		else
		{
			//echo 'Not skipping...<br>';
		}
		
	}

	//var_dump($ticker);

	$ticker[1] = trim($ticker[1]);
	$ticker[2] = trim($ticker[2]);
	$ticker[0] = trim($ticker[0]);

	$tickers[$key] = $ticker;

	if (isset($ticker[5]) and isset($ticker[6]))
	{
		//echo 'From ' . $ticker[5] . ' to ' . $ticker[6] . '<br>';
	}

	

	

	if ($ticker[5] > $ticker[6])
	{

		$mode_local = 'omvendt';
	}
	else
	{
		$mode_local = 'vanlig';
	}

	$was_added = 0;

	if ($mode_local == 'vanlig')
	{
		if ($ticker[5] <= $now and $now <= $ticker[6])
		{
			echo 'Ticker ' . $ticker[0] .  ' is within time frame. (' . $ticker[5] . ' til ' . $ticker[6] . '). Will add to download list. <br>';
			$bulkTickerData[] = $ticker[0];
			$was_added = 1;
		}
	}


	if ($mode_local == 'omvendt')
	{
		if ($now >= $ticker[6] and $now <= $ticker[5])
		{
		
		}
		else
		{
			echo 'Ticker ' . $ticker[0] .  ' is within time frame. (' . $ticker[5] . ' til ' . $ticker[6] . '). Will add to download list. <br>';
			$bulkTickerData[] = $ticker[0];
			$was_added = 1;
		}
	}

	if ($was_added == 0)
	{
		errorecho ('Ticker ' . $ticker[0] .  ' NOT ADDED (' . $ticker[5] . ' til ' . $ticker[6] . ') <br>');
		$skipped_array[$ticker[0]] = 1;
	}
	
}


//var_dump($skipped_array);

//echo separate bonds

$bondsTickerData = [];

foreach ($bulkTickerData as $key => $ticker)
{
	
	if (strripos($ticker, 'BOND') !== false )
	{
		$bondsTickerData[] = $ticker;
		unset($bulkTickerData[$key]);
	}

}

echo 'Getting ' . count($bulkTickerData) . ' tickers or ' .  count($bondsTickerData)  .  ' bonds<br>';


if ($mode == 'bonds')
{

	if ($download_bonds = ticker_download_bonds($bondsTickerData,''))
	{
		saveJSON($download_bonds,'json/tickers_bonds_all.json');
	}

	foreach ($download_bonds as $key => $bond)
	{
		saveJSON($bond, 'data/tickers/' . mb_strtolower($key) . '.json');
	}

}
else if ($mode == 'normal')
{


	if ($download = ticker_download_eodhistorical($bulkTickerData))
	{
		saveJSON($download,'json/tickers_all.json');
	}


	foreach ($tickers as $key => $ticker)
	{

		if (!isset($ticker[1]) or $ticker[1] == '' or !isset($ticker[2]) or $ticker[2] == '')
		{
			continue;
		}

		if(mb_strtolower($ticker[0]) == 'kode' or mb_strtolower($ticker[1]) == 'navn' or mb_strtolower($ticker[2]) == 'kallenavn' )
		{
			continue;
		}


		$found = 0; 
		foreach ($download as $entry)
		{
			if ($ticker[0] == $entry['code'])
			{
				$entry['name'] = $ticker[2];
				saveJSON($entry, 'data/tickers/' . mb_strtolower($entry['code']) . '.json');
				$found = 1;
				break;
			}
		}

		if ($found == 0 and $mode != 'forside')
		{
			if (!isset($skipped_array[$ticker[0]]))
			{
				echo '<strong>Error, not found: ' . $ticker[0] . '</strong><br>';
			}
		}
		
	}

}

else if ($mode == 'forside')
{

	if ($download = ticker_download_eodhistorical($bulkTickerData))
	{
		saveJSON($download,'json/tickers_forside_all.json');
	}

	foreach ($tickers as $key => $ticker)
	{

		if (!isset($ticker[1]) or $ticker[1] == '' or !isset($ticker[2]) or $ticker[2] == '')
		{
			continue;
		}

		if (mb_strtolower($ticker[0]) == 'kode' or mb_strtolower($ticker[1]) == 'navn' or mb_strtolower($ticker[2]) == 'kallenavn' )
		{
			continue;
		}


		$found = 0; 
		foreach ($download as $entry)
		{
			if ($ticker[0] == $entry['code'])
			{
				$entry['name'] = $ticker[2];
				saveJSON($entry, 'data/tickers/' . mb_strtolower($entry['code']) . '.json');
				$found = 1;
				break;
			}
		}

		if ($found == 0 and $mode != 'forside')
		{
			if (!isset($skipped_array[$ticker[0]]))
			{
				echo '<strong>Error, not found: ' . $ticker[0] . '</strong><br>';
			}
			
		}
		
	}
}











?>