<?php

$filetarget = '../../code/data/ticker_list/';
$filetarget_fallback =  '../../code/data/upload/stocks_fallback/';
include '../../code/functions.php';
include '../../code/functions_flashtweets.php';

if ($_FILES['file']['name'] != "")  
{
    $path = $_FILES['file']['name'];

    if (strpos($path, '.tsv') !== false)
	{
		echo 'Filtype ok (kun .tsv). <br>';
	}
	else
	{
		exit();
	}

	$tmpName = $_FILES['file']['tmp_name'];
	$csvAsArray = array_map('str_getcsv', file($tmpName));

	//var_dump($csvAsArray);

	$found = 0;

	foreach ($csvAsArray as $line)
	{

		if (strpos(mb_strtolower($line[0]), mb_strtolower('COMMODITIES')) !== false)
		{
			$found = 1;
			break;
		}

	}

	foreach ($csvAsArray as $line)
	{

		if (strpos(mb_strtolower($line[0]), mb_strtolower('CURRENCIES')) !== false)
		{
			$found = 1;
			break;
		}

	}	

	if ($found == 0)
	{
		exit();
	}

	echo 'Sjekker innhold. Fant ordene COMMODITIES og CURRENCIES i fil. Lagrer.<br>';
    $pathto = $filetarget . $path;

	// Get array of all source files
	$files = scandir($filetarget);
	// Identify directories
	$source = $filetarget;
	$destination = $filetarget_fallback;
	// Cycle through all source files

	$delete = [];

	foreach ($files as $file) {
	  if (in_array($file, array(".",".."))) continue;
	  // If we copied this successfully, mark it for deletion
	  if (copy($source.$file, $destination.$file)) 
	  {
	    $delete[] = $source.$file;
	  }
	  else
	  {
	  	echo 'Copy error!<br>';
	  }
	}
	// Delete all successfully-copied files
	foreach ($delete as $file) 
	{
	  unlink($file);
	}

    move_uploaded_file($_FILES['file']['tmp_name'],$pathto) or die( "Could not copy file!");

    echo 'Opplasting-script ferdig.';
}
else 
{
    die("Error! No file specified!");
}


?>