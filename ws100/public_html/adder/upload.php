<?php

$filetarget = '../../code/data/upload/user_master/';
$filetarget_fallback =  '../../code/data/upload/user_master_fallback/';
include '../../code/functions.php';

if ($_FILES['file']['name'] != "")  
{
    $path = $_FILES['file']['name'];

    if (strpos($path, '.csv') !== false)
	{
		echo 'Filtype ok (kun .csv). <br>';
	}
	else
	{
		exit();
	}

	$tmpName = $_FILES['file']['tmp_name'];
	$csvAsArray = array_map('str_getcsv', file($tmpName));

	$found = 0;

	foreach ($csvAsArray as $line)
	{

		if (strpos($line[0], 'Twitter-kontoer') !== false)
		{
			$found = 1;
			break;
		}

	}

	if ($found == 0)
	{
		exit();
	}

	echo 'Fant Twitter-kontoer i csv-fil. Lagrer.<br>';
    $pathto = $filetarget . $path;


	// Get array of all source files
	$files = scandir($filetarget);
	// Identify directories
	$source = $filetarget;
	$destination = $filetarget_fallback;
	// Cycle through all source files

	$delete = [];

	foreach ($files as $file) {
	  if (in_array($file, array(".",".."))) continue;
	  // If we copied this successfully, mark it for deletion
	  if (copy($source.$file, $destination.$file)) 
	  {
	    $delete[] = $source.$file;
	  }
	  else
	  {
	  	echo 'Copy error!<br>';
	  }
	}
	// Delete all successfully-copied files
	foreach ($delete as $file) {
	  unlink($file);
	}

    move_uploaded_file($_FILES['file']['tmp_name'],$pathto) or die( "Could not copy file!");

    echo 'Opplasting-script ferdig.';
}
else 
{
    die("No file specified!");
}

echo '<br>Prøver å generere nye lister:<br>';


$user_list = [];
$breaking_list = [];

$switch = 'user';

echo '<br><h3>Forsøker å lage bruker-listen:</h3>';

foreach ($csvAsArray as $line)
{
	if (is_numeric($line[0]) and $line[1] != '')
	{

		if ($switch == 'user')
		{
			echo $line[0] . '. ' . $line[1] . '<br>';
			$user_list[] = trim($line[1]) . "\n";
		}
		else
		{
			echo $line[0] . '. ' . $line[1] . '<br>';
			$breaking_list[] = trim($line[1]) . "\n"; 
		}

	}

	if ($line[0] == 'Breaking-brukere')
	{
		$switch = 'breaking';
		echo '<br><br><h3>Forsøker å lage breaking-listen:</h3>';
	}

}
echo '<br><br>';
if (count($breaking_list) > 10)
{
	file_put_contents('../../code/data/breaking_list.csv', $breaking_list);
	echo 'Lagret breaking.<br>';
}
else
{
	echo 'Mindre enn 10 oppføringer for breaking. Noe feil? Lagrer ikke...<br>';
}

if (count($user_list) > 50)
{
	file_put_contents('../../code/data/user_list.csv', $user_list);
	echo 'Lagret users.<br><br>';
}
else
{
	echo 'Mindre enn 50 oppføringer for usere. Noe feil? Lagrer ikke...<br>';
}

echo 'OBS: Rangeringen av brukere er nullstilt, vil bli oppdatert ved neste programkjøring av control_common.php. Vanligvis innen en time, trolig etter 15 minutter.';

?>