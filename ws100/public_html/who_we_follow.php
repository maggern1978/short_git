<?php

include 'header.html';
include_once '../code/functions.php';

$files = listfiles('../code/data/upload/user_master/');

if (!$data = readCSVkomma('../code/data/upload/user_master/' . $files[0]))
{
	echo 'Could not read user list<br>';
	exit();
}

$users = '';
$breaking = '';

foreach ($data as $key => $line)
{
	if ($line[0] == 'Breaking-brukere')
	{
		$users = array_slice($data,0,$key);
		$breaking = array_slice($data,$key);
	}
}

foreach ($users as $key => $user)
{
	if (!is_numeric($user[0])  or $user[1] == '')
	{
		unset($users[$key]);
	}

	if ($user[1] == 'zerohedge')
	{
		$users[$key][2] = 'zerohedge';
	}
}

foreach ($breaking as $key => $user)
{
	if (!is_numeric($user[0]) or $user[1] == '')
	{
		unset($breaking[$key]);
	}
}

usort($users, function ($item1, $item2) {
	return $item1[2] <=> $item2[2];
});

usort($breaking, function ($item1, $item2) {
	return $item1[2] <=> $item2[2];
});


$count = count($users);
$half = ceil($count/2)-1;

?>
<div class="container mt-3">
	<div clasS="row">
		<div class="col-12  mb-2">
			<h1>Who we follow</h1>
			<p>We follow the 100 most important Twitter accounts in finance. Read more <a href="about_us.php">here</a>.</p>
				<span class="h2">Users </span>(alphabetically)
			</div>
		</div>
	</div>
	<div class="container mt-2">
		<div clasS="row">
			<div class="col-12 col-sm-6">
				<?php

				$runner = 1;

				foreach ($users as $key => $user)
				{
	//http://localhost/christer/public_html/index.php?start=0&show=25&name=business

					if ($user[1] == '')
					{
						continue;
					}

					if ($user_data = readJSON('../code/users_info/users/' . mb_strtolower($user[1]) . '.json'))
					{
						if(isset($user_data['data']))
						{
							$user[2] = $user_data['data'][0]['name'];
						}

					}
		

					?>

					
					<div class="d-flex flex-row border-bottom mb-1">
						<div class=""><?php
						//$user_data['data']['0']['profile_image_url'] = 'img/dummy_profile.jpg';
							
						if (isset($user_data['data']))
						{
							echo '<img class="rounded-circle lazyload" data-src="' . $user_data['data']['0']['profile_image_url'] . '" width="58px">';
						}
						else
						{
							$user_data['data']['0']['profile_image_url'] = 'img/dummy_profile.jpg';
							echo '<img class="rounded-circle lazyload" data-src="' . $user_data['data']['0']['profile_image_url'] . '" width="58px">';
						}	 
							?>
						</div>
						<div class="d-flex flex-column  p-2 mb-2">
							<div class="d-flex flex-column  flex-lg-row">
								<div class="pr-2"><?php echo '<a href="index.php?start=0&show=25&name=' . $user[1] . '"><span class=" text-dark h4 font-weight-bold">'. $runner++ . '. ' . $user[2] . '</span></a>';?>
									
								</div>
								<div class="pt-1"><?php echo '<a href="index.php?start=0&show=25&name=' . $user[1] . '"><span class="text-secondary">@' . $user[1] . '</span></a>';?>
									
								</div>
							</div>

							<div class="pr-2 mt-1">
								<?php
								if ($user[3] != '')
								{
									echo $user[3]; 
								}
								else
								{
									echo $user_data['data']['0']['description'];
								}

								?>
							</div>
						</div>				
					</div>

					<?php

					if ($key == $half) 
					{
						echo '</div><div class="col-12 col-sm-6">';
					}

				}
				?>
			</div>
		</div>
	</div>
	<div class="container mt-4">
		<div clasS="row">
			<div class="col-12  mb-2">
					<span class="h2">News sources </span>(alphabetically)
				</div>
			</div>
		</div>
	</div>

	<div class="container mt-2">
		<div clasS="row">
			<div class="col-12 col-sm-6">
				<?php

				$count = count($breaking);
				$half = ceil($count/2)-1;


				$runner = 1;

				foreach ($breaking as $key => $user)
				{
	//http://localhost/christer/public_html/index.php?start=0&show=25&name=business

					if ($user[1] == '')
					{
						continue;
					}

					if ($user_data = readJSON('../code/users_info/breaking/' . mb_strtolower($user[1]) . '.json'))
					{
						if(isset($user_data['data']))
						{
							$user[2] = $user_data['data'][0]['name'];
						}

					}
					

					?>

					
					<div class="d-flex  border-bottom mb-1">
						<div class=""><?php
						//$user_data['data']['0']['profile_image_url'] = 'img/dummy_profile.jpg';
							
						if (isset($user_data['data']))
						{
							echo '<img class="rounded-circle lazyload" data-src="' . $user_data['data']['0']['profile_image_url'] . '" width="58px">';
						}
						else
						{
							$user_data['data']['0']['profile_image_url'] = 'img/dummy_profile.jpg';
							echo '<img class="rounded-circle lazyload" data-src="' . $user_data['data']['0']['profile_image_url'] . '" width="58px">';
						}	 
							?>
						</div>
						<div class="d-flex flex-column  p-2 mb-2">
							<div class="d-flex flex-column  flex-lg-row">
								<div class="pr-2"><?php echo '<a href="index.php?start=0&show=25&name=' . $user[1] . '"><span class=" text-dark h4 font-weight-bold">'. $runner++ . '. ' . $user[2] . '</span></a>';?>
									
								</div>
								<div class="pt-1"><?php echo '<a href="index.php?start=0&show=25&name=' . $user[1] . '"><span class="text-secondary">@' . $user[1] . '</span></a>';?>
								</div>
							</div>

							<div class="pr-2 mt-1">
								<?php
								if ($user[3] != '')
								{
									echo $user[3]; 
								}
								else
								{
									if (isset($user_data['data']['0']['description']))
									{
										echo $user_data['data']['0']['description'];
									}
									else
									{
										echo '-';
									}
									
								}

								?>
							</div>
						</div>						

					</div>

					<?php

					if ($key == $half) 
					{
						echo '</div><div class="col-12 col-sm-6">';
					}

				}
				?>	

			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
<?php echo include 'footer.html';?>
</body>
</html>
