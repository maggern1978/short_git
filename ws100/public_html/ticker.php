<?php

include_once('../code/functions.php');
include '../public_html/header.html';

if (!isset($_GET['ticker']))
{
	echo 'Missing ticker.<br>';
	return;
}
else
{
	$ticker = mb_strtolower(htmlspecialchars($_GET["ticker"]));
}

if (!isset($_GET['name']))
{
	echo 'Missing name.<br>';
	return;
}
else
{
    $name = htmlspecialchars($_GET["name"]);
}

if ($name == '' or $ticker == '')
{
	return;
}

if (!$data = readJSON('../code/data/tickers.history/' . mb_strtolower($ticker) . '.json'))
{
	echo 'Read error. <br>';
	return;
}

//var_dump($data);

if ($today = readJSON('../code/data/tickers/' . mb_strtolower($ticker) . '.json'))
{
	
	if (isset($today['date']))
	{
		$array = [];
		$array['date'] = $today['date'];
		$array['close'] = $today['close'];
		$array['volume'] = $today['volume'];
		$array['adjusted_close'] = $today['close'];
		$array['timestamp'] = $today['timestamp'];
		$data[] = $array;
	}
	else
	{
		$today = false;
	}
	
}
else
{
	$today = false;
}

?>

<script src="js/echarts.min.js"></script>
<div class="container mt-3">
<div class="">
<div class="d-flex flex-column flex-md-row justify-content-md-between ">
<?php
        echo '<div class="h2">' . $name . '</div> ';

        if ($today != false)
        {
            if ($today['change'] < 0)
            {
                $color = 'text-primary';
            }
            else if ($today['change'] > 0)
            {
                 $color = 'text-success';
            }
            else
            {   
                 $color = 'text-primary';
            }            


            if ($today['change'] > 0)
            {
               $prefix = '+';
            }
            else
            {
                $prefix = '';
            }

            ?>
                <div class="d-flex h3 pt-1">
                  <div class="mr-3 font-weight-bold"><?php echo number_format($today['close'],2,".",","); ?></div>
                  <div class="mr-3 <?php echo $color; ?>"><?php echo $prefix . '' . number_format($today['change'],2,".",","); ?></div>
                  <div class=" <?php echo $color; ?>"><?php echo $prefix . '' . number_format($today['change_p'],2,".",","); ?>%</div>
                </div>

            <?php

        }
        else
        {
            $myLastElement = end($data);
            $mySecondToLastElement = prev($data);

            $change = $myLastElement['close'] -  $mySecondToLastElement['close'];
            $change_p = ($change/$mySecondToLastElement['close'])*100;

            if ($change < 0)
            {
                $color = 'text-primary';
            }
            else if ($change > 0)
            {
                 $color = 'text-success';
            }
            else
            {   
                 $color = 'text-primary';
            }   

            if ($change > 0)
            {
               $prefix = '+';
            }
            else
            {
                $prefix = '';
            }

            ?>
                <div class="d-flex h3 pt-1">
                  <div class="mr-3 font-weight-bold"><?php echo number_format($myLastElement['close'],2,".",","); ?></div>
                  <div class="mr-3 <?php echo $color; ?>"><?php echo $prefix . '' . number_format($change,2,".",","); ?></div>
                  <div class=" <?php echo $color; ?>"><?php echo $prefix . '' . number_format($change_p,2,".",","); ?>%</div>
                </div>

            <?php           



        }
?>
</div>
</div>
	<!-- prepare a DOM container with width and height -->
	<div class="mx-auto" id="main" style="width: 100%;height:400px;"></div>
	<script type="text/javascript">

		<?php 
		$dataholder = json_encode($data);
		echo "var data = ". $dataholder . ";\n"; ?>

		var raw = [];
		var adjusted = [];
		var volume = [];
		var dates = [];
		var count = data.length;

		for (i = 0; i < count; i++)
		{
			raw[i] = data[i].close;
			adjusted[i] = data[i].adjusted_close;
			volume[i] = data[i].volume;
			dates[i] = data[i].date;
		}

		var max_volume = Math.max.apply(null, volume);
		var min_adjusted = Math.min.apply(null, adjusted);
		var max_adjusted = Math.max.apply(null, adjusted);
		var min_raw = Math.min.apply(null, raw);

		if (min_raw < min_adjusted)
		{
			var min_stocks = (min_raw);
		}
		else
		{
			var min_stocks = (min_adjusted);
		}

		if (min_stocks > 0)
		{

			if (max_adjusted > 10000)
			{
				var leftover = min_stocks % 1000;
			}
			else if (max_adjusted > 5000)
			{
				var leftover = min_stocks % 200;
			}
			else if (max_adjusted > 1000)
			{
				var leftover = min_stocks % 100;
			}
			else if (max_adjusted > 100)
			{
				var leftover = min_stocks % 10;
			}
			else if (max_adjusted > 50)
			{
				var leftover = min_stocks % 5;
			}		
			else
			{
				var leftover = min_stocks % 2;
			}
			min_stocks = min_stocks - leftover;
		}

		var myChart = echarts.init(document.getElementById('main'));

		var w;
		var h;
		var left;
		var right;
		var headingFontSize;
		var toppen;



		function setparameters(max_adjusted) 
        {

			w = window.innerWidth;
			h = window.innerHeight;

			if (w >= 1200) {
				left = '5%';
				right = '2%';
				headingFontSize = 18;
				toppen = '50';
			}
			else if (w >= 992 ) {
				left = '7%';
				right = '4%';
				headingFontSize = 16;
				toppen = '50';
			}
			else if (w >= 768) {
				left = '8%';
				right = '4%';
				headingFontSize = 18;
				toppen = '50';
			}
			else if (w >= 576) {
				left = '10%';
				right = '4%';
				headingFontSize = 18;
				toppen = '50';
			}
			else if (w >= 407) {
				left = '13%';
				right = '4%';
				headingFontSize = 18;
				toppen = '50';
            //console.log('440');
            }
            else if (w >= 340) {
            	left = '15%';
            	right = '4%';
            	headingFontSize = 18;
            	toppen = '90';
                //console.log('340');
            }
            else {
            	left = '16%';
            	right = '4%';
            	headingFontSize = 18;
            	toppen = '90';
                //console.log('last');
            }
        }

    setparameters();

    option = {
    	textStyle: {
    		fontFamily: ['Encode Sans','sans-serif'],
    	},

    	toolbox: {
    		show : false,
    		itemSize: 15,
    		feature : {
    			mark : {show: false},
    			dataZoom : {show: false},
    			magicType : {show: false, type: ['line', 'bar']},
    			restore : {show: false},
    			dataView : {
    				show: true,
    				title: 'Data',
    				lang: ['Price', 'Exit', 'Refresh'],

    			}
    		}
    	},    
    	grid: {
    		top:    toppen,
    		bottom: 100,
    		left:   left,
    		right:  right,
    	},
    	legend: {
    		top: 10,
    		itemWidth: 35,
    		itemHeight: 15,
    		borderWidth: 0,
    		textStyle: {
    			fontSize: 16,
    		},
    		data: [{
    				name: 'Price',
    				icon: 'rect',
    			},
    			{
    				name: 'Price (not adjusted)',
    				icon: 'rect',
    			},
				{
    				name: 'Volume',
    				icon: 'rect',
    			},    			
    		],
    		selected: {
    			'Price (not adjusted)': false,
    			'Volume': false,
    		},

    		},
    		animation: false,
    		backgroundColor: '#f6f6f6',
    		title: {
        //backgroundColor: '#000000',
        left: 'center',
        padding: [0,00,0,00],
        text: '',
        textStyle: {
        	color: '#000000',
        	fontSize: headingFontSize,
        	width: 400,
        },
        subtextStyle: {
        	fontSize: 14,
        	color: '#000000',
        }
    },
    tooltip: {
    	trigger: 'axis',
    	axisPointer: {
    		type: 'cross',
    		crossStyle: {
    			color: '#999'
    		}
    	},
    	backgroundColor: '#000000',
    	confine: true,

    },

    xAxis: [
    {
    	name: 'Date',
    	type: 'category',
    	data: dates,
    	axisPointer: {
    		type: 'shadow'
    	},
    	nameTextStyle: {
    		fontSize: 16,
    		padding: [15,0,100,0],
    		align: 'bottom',

    	},
    	nameRotate: '0',
    	nameLocation: 'middle',
    }
    ],
    yAxis: [
    {
    	type: 'value',
    	name: 'Price',
    	axisLabel: {
    		formatter: '{value}',
    	},
    	nameTextStyle: {
    		fontSize: 16,
    		padding: [15,0,0,30],
    		align: 'right',
    	},
    	nameLocation: 'start',
    	min: min_stocks,

    },    
    {
    	type: 'value',
    	name: 'Volume',
    	axisLabel: {
    		formatter: '{value}',
    	},
    	nameTextStyle: {
    		fontSize: 16,
    		padding: [15,0,0,30],
    		align: 'left',
    	},
    	nameLocation: 'start',
    	show: false,
    	max: max_volume,

    }, 

   
    ],
    dataZoom: [
    {
    	show: true,
    	type: 'slider',
    	bottom: 10,
        right: 18,
    	start: 75,
    	end: 100,
    	handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
    	handleSize: '105%',
    	handleStyle: {
    		color: 'black',
    	},
    },
    ],
    series: [
    {
    	name:'Price',
    	type:'line',
    	connectNulls: true,
    	data: adjusted,
    	showSymbol: false,
    	lineStyle: {
    		width: 3, 
    	},
    	yAxisIndex: 0,
    	itemStyle: {
    		color: 'rgba(255, 0, 0, 0.8)',
    	}
    },    
    {
    	name:'Price (not adjusted)',
    	type:'line',
    	connectNulls: true,
    	data: raw, 
    	lineStyle: {
    		width: 3, 
    	},
    	showSymbol: false,
    	yAxisIndex: 0,
    },
    {
    	name:'Volume',
    	type:'bar',
    	connectNulls: true,
    	data: volume, 
    	lineStyle: {
    		width: 3, 
    	},
    	showSymbol: false,
    	yAxisIndex: 1,
    	
    },
  
    ],

};
       // use configuration item and data specified to show chart

       myChart.setOption(option);

       window.addEventListener('resize', function(event){
       	setparameters();

       	option = {
       		grid: {
       			top:    toppen,
       			bottom: 100,
       			left:   left,
       			right:  right,
       		},
       	}  
       	myChart.setOption(option);
       	myChart.resize();
       });

   </script>
   <div class="text-center mt-2">Updated: <?php

   	$end = end($data);

   	if (isset($end['timestamp']))
   	{
   		$time_ago_string = time_elapsed_string($end['timestamp']); //utc
   		echo $time_ago_string;
   	}
   	else
   	{
   		$time_ago_string = time_elapsed_string($end['date']); //utc
   		echo $time_ago_string;
   	}


   ?>
   	
   </div>
</div>    
<br><br><br>
<?php echo include 'footer.html';?>
</body>
</html>
<?php


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>