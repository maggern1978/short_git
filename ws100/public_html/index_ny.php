<?php 

  //set headers to NOT cache a page
  //header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
  //header("Pragma: no-cache"); //HTTP 1.0
  //header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

  //or, if you DO want a file to cache, use:
  //30days (60sec * 60min * 24hours * 30days)


//todo timestamp på retweets er feil!
//endre timestamp før man sender seg selv til tweet
//cache user info for later use. 

//christer
//christer2021
//@todo hent mer info fra tweet med video så video kan integreres i siten. 
//@todo hent inn full tekst fra refrerte tweeter
//todo legg inn linker der det finnes
//hvis video ikke laster, lag preview i stedet
//http://localhost/christer/public_html/index.php?start=00&show=10&name=sjosephburns
//skjul image not loaded icon
//https://bootswatch.com/simplex/

include_once '../code/functions.php';

if (isset($_GET['mode']))
{
	$mode = htmlspecialchars($_GET["mode"]);
}
else
{
	$mode = 'users';
}

if (isset($_GET['start']))
{
	$start = htmlspecialchars($_GET["start"]);
}
else
{
	$start = 0;
}

$url_string = 'start=' . $start . '&mode=' . $mode; 

if (isset($_GET['show']) and is_numeric($_GET['show']))
{
	$show = htmlspecialchars($_GET["show"]);
}
else
{
	$show = 25;
}

if (isset($_GET['type']))
{
	$type = htmlspecialchars($_GET["type"]);
}
else
{
	$type = 'tweets';
}

$url_string .= '&show=' . $show;

$source = 'users';

if (isset($_GET['name']))
{
	$temp_name = mb_strtolower(htmlspecialchars($_GET["name"]));

	if (file_exists('../code/users/' . $temp_name . '/'))
	{
		$name = $temp_name;
		$url_string .= '&name=' . $name;
		$source = 'users';
	}
	else if (file_exists('../code/breaking/' . $temp_name . '/'))
	{
		
		$name = $temp_name;
		$url_string .= '&name=' . $name;
		$source = 'breaking';
	}	
	else 
	{
		$name = false;
	}
}
else
{
	$name = false;
}

if ($name == false and $mode == 'users')
{

	if (!$tweets_file = new SplFileObject('../code/data/displaylist/user_display_list_tweets.csv'))
	{
		///echo 'Could not read user tweets list<br>';
		$file = false;
	}

	if (!$replies_file = new SplFileObject('../code/data/displaylist/user_display_list_replies.csv'))
	{
		//echo 'Could not read user replies list<br>';
		$file = false;
	}

	if (!$media_file = new SplFileObject('../code/data/displaylist/user_display_list_media.csv'))
	{
		//echo 'Could not read media tweets list<br>';
		$file = false;
	}		

}
else if ($name == false and $mode == 'breaking')
{

	if (!$tweets_file = new SplFileObject('../code/data/displaylist/breaking_display_list_tweets.csv'))
	{
		//echo 'Could not read user tweets list<br>';
		$file = false;
	}

	if (!$replies_file = new SplFileObject('../code/data/displaylist/breaking_display_list_replies.csv'))
	{
		//echo 'Could not read user replies list<br>';
		$file = false;
	}

	if (!$media_file = new SplFileObject('../code/data/displaylist/breaking_display_list_media.csv'))
	{
		//echo 'Could not read media tweets list<br>';
		$file = false;
	}

	$source = 'breaking';

}
else
{	
	if ($source == 'users')
	{

		if (!$tweets_file = new SplFileObject('../code/users/' . $temp_name . '/' . $temp_name . '_tweets.csv'))
		{
			//echo 'Could not read user tweets list<br>';
			$file = false;
		}

		if (!$replies_file = new SplFileObject('../code/users/' . $temp_name . '/' . $temp_name . '_replies.csv'))
		{
			//echo 'Could not read user replies list<br>';
			$file = false;
		}

		if (!$media_file = new SplFileObject('../code/users/' . $temp_name . '/' . $temp_name . '_media.csv'))
		{
			//echo 'Could not read media tweets list<br>';
			$file = false;
		}	

	}
	if ($source == 'breaking')
	{

		if (!$tweets_file = new SplFileObject('../code/breaking/' . $temp_name . '/' . $temp_name . '_tweets.csv'))
		{
			//echo 'Could not read user tweets list<br>';
			$file = false;
		}

		if (!$replies_file = new SplFileObject('../code/breaking/' . $temp_name . '/' . $temp_name . '_replies.csv'))
		{
			//echo 'Could not read user replies list<br>';
			$file = false;
		}

		if (!$media_file = new SplFileObject('../code/breaking/' . $temp_name . '/' . $temp_name . '_media.csv'))
		{
			// 'Could not read media tweets list<br>';
			$file = false;
		}	

	}

}

$stop = $start + $show; 

$tweets_data = tweet_reader($tweets_file, $start, $stop);
$replies_data = tweet_reader($replies_file, $start, $stop);
$media_data = tweet_reader($media_file, $start, $stop);

$user_list = [];

if (!$user_list = readJSON('../code/data/allusers_search_array.json'))
{
	//echo 'Could not read breaking_display_list<br>'; //will continue without
	$user_list = [];
}

if ($mode == 'breaking')
{
	if (!$file = new SplFileObject('../code/data/displaylist/user_display_list_tweets.csv'))
	{
		//echo 'Could not read breaking_display_list<br>';
		$file = false;
	}
}
else if ($mode == 'users')
{
	if (!$file = new SplFileObject('../code/data/displaylist/breaking_display_list_tweets.csv'))
	{
		//echo 'Could not read breaking_display_list<br>';
		$file = false;
	}
}


if ($file != false)
{
	$breaking_tweets_list = tweet_reader($file, 0, 80);
}
else
{
	$breaking_tweets_list = [];
}

include 'header.html';
include 'ticker_line.html';

$GLOBALS['video_js_loaded'] = false; 
$GLOBALS['users_runner'] = 0; 
$GLOBALS['users_runner_limit'] = 5; //no lazyload of first posts!

$video_js_loaded = 0;

?>

<div class="container-fluid mt-3">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-xl-8 offset-xl-2">
			<div id="main_content_holder" class="row ">
				<div class="col-12 col-sm-8 col-md-8 col-xl-6 offset-xl-1">
					<div class="d-flex justify-content-between ">
						<div class="menu-font menu-div flex-grow-1 text-center">
							<div class="d-flex ">
								<div id='menu_tweets' class="p-2 mx-auto <?php if ($type == 'tweets') { echo 'menu-active';} ?> "><span class="font-weight-bold">Tweets</span></div>
							</div>
						</div>
						<div class="menu-font menu-div flex-grow-1 text-center">
							<div class="d-flex">
								<div  id='menu_tweetsandreplies' class="p-2 mx-auto <?php if ($type == 'reply') { echo 'menu-active';} ?>"><span class="font-weight-bold">Tweets & replies</span></div>
							</div>
						</div>
						<div class="menu-font menu-div flex-grow-1 text-center d-none d-sm-inline">
							<div class="d-flex">
								<div id='menu_media' class="p-2 mx-auto <?php if ($type == 'media') { echo 'menu-active';} ?>"><span class="font-weight-bold">Media</span></div>
							</div>
						</div>
						<div class="d-block d-sm-none menu-font  flex-grow-1 text-center">
							<div class="d-flex">
								<div id='menu_breaking' class="p-2 mx-auto "><span class="font-weight-bold breaking-small-color "><a href="#breaking_news">Breaking</a></span></div>
							</div>
						</div>						
					</div>
					<br>
					<div class="d-flex flex-column"><?php $stylestring = ' style="display: none;"'; ?>
						<div class="tweets " <?php if ($type != 'tweets') { echo $stylestring; } else { echo ' style="display: block"';} ?>>

							<?php

							$user_info_cache = [];
							$tweet_reader_cache = [];

							if (isset($tweets_data[0]) and $tweets_data[0][0] == null)
							{
								echo '<div class="text-center">Nothing to show. <br></div>';
							}

							foreach ($tweets_data as $key => $tweet)
							{
								//no cache on first
								if(!isset($tweet[0]) or !isset($tweet[1]))
								{
									continue;
								}

								$data = get_tweet_data($tweet, $source);

								$tweet_reader_cache[$tweet[0]] = $data;								
								
								if (isset($user_info_cache[$tweet[1]]))
								{
									$user_data = $user_info_cache[$tweet[1]];
								}
								else
								{
									$user_data = get_user_data($tweet, $source);
									$user_info_cache[$tweet[1]] = $user_data;
								}

								//$user_data = get_user_data($tweet, $source, $user_info_cache);

								$type_local = 'tweets';

								if ($data !== false)
								{
									display_tweet($user_data, $data, $show, $type_local,'no','no',$user_list, $source);
								}

							}


							?>

						</div>
						<div class="container-lg mt-5 mb-5 tweets" <?php if ($type != 'tweets') { echo $stylestring;} else { echo ' style="display: block"';} ?>>
							<div class="row">
								<div id="pagination-tweet" class=" col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
								</div>
							</div>
						</div>							
						<div class="reply" <?php if ($type != 'reply') { echo $stylestring;} ?>>
							<?php

							if (isset($replies_data[0]) and $replies_data[0][0] == null)
							{
								echo '<div class="text-center">Nothing to show. <br></div>';
							}

							foreach ($replies_data as $key => $tweet)
							{
								if(!isset($tweet[0]) or !isset($tweet[1]))
								{
									continue;
								}
								
								if (isset($tweet_reader_cache[$tweet[0]]))
								{
									$data = $tweet_reader_cache[$tweet[0]];
								}
								else
								{
									$data = get_tweet_data($tweet, $source);
									$tweet_reader_cache[$tweet[0]] = $data;
								}

								if (isset($user_info_cache[$tweet[1]]))
								{
									$user_data = $user_info_cache[$tweet[1]];

								}
								else
								{
									$user_data = get_user_data($tweet, $source);
									$user_info_cache[$tweet[1]] = $user_data;

								}

								$type_local = 'reply';

								if ($data !== false)
								{
									display_tweet($user_data, $data, $show, $type_local,'no','no',$user_list, $source);
								}
								
							}



							?>
						</div>
						<div class="container-lg mt-5 mb-5 reply " <?php if ($type != 'reply') { echo $stylestring;}else { echo 'style="display:block;"';} ?>>
							<div class="row">
								<div id="pagination-reply" class=" col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
								</div>
							</div>
						</div>

						<div class="media" <?php if ($type != 'media') { echo $stylestring;}else { echo ' style="display: block"';} ?>>
							<?php

							if (isset($media_data[0]) and $media_data[0][0] == null)
							{
								echo '<div class="text-center">Nothing to show. <br></div>';
							}

							foreach ($media_data as $key => $tweet)
							{
								if(!isset($tweet[0]) or !isset($tweet[1]))
								{
									continue;
								}

								if (isset($tweet_reader_cache[$tweet[0]]))
								{
									$data = $tweet_reader_cache[$tweet[0]];
								}
								else
								{
									$data = get_tweet_data($tweet, $source);
									$tweet_reader_cache[$tweet[0]] = $data;
								}

								if (isset($tweet[1]) and isset($user_info_cache[$tweet[1]]))
								{
									$user_data = $user_info_cache[$tweet[1]];
								}
								else if (isset($tweet[1]))
								{
									$user_data = get_user_data($tweet, $source);
									$user_info_cache[$tweet[1]] = $user_data;
								}

								$type_local = 'media';

								if ($data !== false)
								{
									display_tweet($user_data, $data, $show, $type_local,'no','no',$user_list, $source);
								}

							}


							?>
						</div>
						<div class="container-lg mt-5 mb-5 media" <?php if ($type != 'media') { echo $stylestring;} else { echo ' style="display: block"';} ?>>
							<div class="row">
								<div id="pagination-media" class=" col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
								</div>
							</div>
						</div>	
					</div>
				</div>

				<div class="col-12 col-sm-4 col-md-4 border-left ">
					<div class=" breaking-height"><?php
						if ($mode == 'users')
						{

							?><h3 id="breaking_news"><a href="index.php?mode=breaking">Breaking news</a>
							<img data-toggle="tooltip" title="Source: Twitter.com" class="float-right" src="img/Twitter_logo_blue_32.png" width="24">	
						</h3>
						<?php

					}
					else
					{
						?>
						<h3 id="breaking_news"><a href="index.php">Users</a></h3>
						<?php
					}

					$user_info_cache = [];

					foreach ($breaking_tweets_list as $key => $tweet)
					{
						if (!isset($tweet[1]))
						{
							continue;
						}

						if ($mode == 'users')
						{

							if (!$data = readJSON('../code/breaking/' . $tweet[1] . '/tweets/' . $tweet[0] . '.json'))
							{
								//echo 'Error';
								continue;
							}
						}

						else if ($mode == 'breaking')
						{

							if (!$data = readJSON('../code/users/' . $tweet[1] . '/tweets/' . $tweet[0] . '.json'))
							{
								//echo 'Error';
								continue;
							}
						}

							//cache user info for later use. 
							//echo '../code/users_info/' . $mode . '/' . $tweet[1] . '.json';

						if (!isset($user_info_cache[$tweet[1]]))
						{
							if ($mode == 'breaking')
							{

								if (!$user_data = readJSON('../code/users_info/users/' . $tweet[1] . '.json'))
								{
									continue;
								}

							}
							else if ($mode == 'users')
							{

								if (!$user_data = readJSON('../code/users_info/breaking/' . $tweet[1] . '.json'))
								{
									continue;
								}

							}								


							$user_info_cache[$tweet[1]] = $user_data;

						}
						else
						{
							$user_data = $user_info_cache[$tweet[1]];
						}


						$text = $data['data']['text'];

						if (isset($data['data']['entities']['urls']))
						{
							foreach ($data['data']['entities']['urls'] as $entry)
							{
								$url = $entry['url'];

								$text = str_replace($url, '', $text);
							}
						}

						?>
						<div>
							<div class="d-flex  flex-column pb-2 mb-1 border-bottom">
								<div class="d-flex justify-content-between">
									<div class="d-flex mt-2 w-100">
										<div class="d-flex flex-column w-100 kolonnebredde_breaking" id="kolonnebredde_breaking">
											<div class="d-flex flex-wrap text-breaking">
												
												<div class="mr-2">
													<a href="<?php echo 'https://twitter.com/' .  $user_data['data']['0']['username']; ?>">
														<img class="rounded-circle lazyload" data-src="<?php echo $user_data['data']['0']['profile_image_url']; ?>" width="15px" height="15px"> 
													</a>
												</div>
												<div class="mr-2">
													<span class="text text-breaking-header">
														<a class="font-weight-bold text-dark" href="https://twitter.com/<?php echo $user_data['data']['0']['username']; ?>">
															<?php echo $user_data['data']['0']['name']; ?>
														</a>
													</span>
												</div>													
												<div class="mr-2">
													<span class="text text-breaking-header">
														<a href="https://twitter.com/<?php echo $user_data['data']['0']['username'];?>">
															@<?php echo $user_data['data']['0']['username']; ?>
														</a>
													</span>
												</div>
												<div class="mr-2 text text tweettime text-breaking-header" data-toggle="tooltip" title="<?php echo date_format(date_create($data['data']['created_at']),"Y/m/d H:i:s"); ?>">
													<a href="<?php echo 'https://twitter.com/' . $user_data['data']['0']['username'] . '/status/' . $data['data']['id']; ?>">
														<div class="tweet-user" created_at="<?php echo $data['data']['created_at']; ?>">
															<?php																						
															echo time_elapsed_string($data['data']['created_at']);
															?></div> 
														</a>
													</div>
												</div>
												<div class="text-breaking mt-1 word-wrap">
													<?php 

													if(isset($data['data']['entities']['mentions']))
													{
														foreach ($data['data']['entities']['mentions'] as $mention)
														{

															if (strripos($text, '@' . $mention['username']))
															{
																if (isset($user_list[$mention['username']])) //in our list? 
																{
																	$mention_link = '<a href="' . 'index.php?' . 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($user_data['data']['0']['username']) . '">' .'@' . $mention['username'] . '</a>';
																	$text = str_ireplace('@' . $mention['username'], $mention_link, $text);

																}
																else
																{
																	$mention_link = '<a href="' . 'https://twitter.com/' . $mention['username'] . '">' .'@' . $mention['username'] . '</a>';
																	$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
																}


															}
															
														}
													}

													if (isset($data['data']['entities']['hashtags']))
													{

														foreach ($data['data']['entities']['hashtags'] as $hashtag)
														{

															$link = '<a target="_blank" href="' . 'https://twitter.com/search?q=' . $hashtag['tag'] . '">#' . $hashtag['tag'] . '</a>';
															$text = str_ireplace('#' . $hashtag['tag'], $link, $text);

														}

													}

													echo $text . ' ';
													
													if (isset($data['data']['entities']['urls']))
													{
														foreach ($data['data']['entities']['urls'] as $url)
														{

															if (isset($url['url']) and !strpos($url['expanded_url'], 'twitter.com') !== false)
															{
																echo '| <a class="text-primary" href="' . $url['url'] . '"> Link</a>';
																continue;
															}

														}

													}

													?>
													<span class="float-right">
														<a class="float-right liketweet" target="_blank" href="https://twitter.com/intent/like?tweet_id=<?php echo $data['data']['id']; ?>"><svg width="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 72"><path fill="#AAB8C2" d="M38.723,12c-7.187,0-11.16,7.306-11.723,8.131C26.437,19.306,22.504,12,15.277,12C8.791,12,3.533,18.163,3.533,24.647 C3.533,39.964,21.891,55.907,27,56c5.109-0.093,23.467-16.036,23.467-31.353C50.467,18.163,45.209,12,38.723,12z"/>
														</svg>	</a>	
														<a class="float-right mr-2 retweettweet" target="_blank" href="https://twitter.com/intent/retweet?tweet_id=<?php echo $data['data']['id']; ?>"><svg width="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 72" > <path  fill="#AAB8C2" d="M70.676 36.644C70.166 35.636 69.13 35 68 35h-7V19c0-2.21-1.79-4-4-4H34c-2.21 0-4 1.79-4 4s1.79 4 4 4h18c.552 0 .998.446 1 .998V35h-7c-1.13 0-2.165.636-2.676 1.644-.51 1.01-.412 2.22.257 3.13l11 15C55.148 55.545 56.046 56 57 56s1.855-.455 2.42-1.226l11-15c.668-.912.767-2.122.256-3.13zM40 48H22c-.54 0-.97-.427-.992-.96L21 36h7c1.13 0 2.166-.636 2.677-1.644.51-1.01.412-2.22-.257-3.13l-11-15C18.854 15.455 17.956 15 17 15s-1.854.455-2.42 1.226l-11 15c-.667.912-.767 2.122-.255 3.13C3.835 35.365 4.87 36 6 36h7l.012 16.003c.002 2.208 1.792 3.997 4 3.997h22.99c2.208 0 4-1.79 4-4s-1.792-4-4-4z"/></svg></a>

														<a class="float-right mr-2 replytweet" target="_blank" href="https://twitter.com/intent/tweet?in_reply_to=<?php echo $data['data']['id']; ?>"><svg width="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 72"><path fill="#AAB8C2" d="M41 31h-9V19c0-1.14-.647-2.183-1.668-2.688-1.022-.507-2.243-.39-3.15.302l-21 16C5.438 33.18 5 34.064 5 35s.437 1.82 1.182 2.387l21 16c.533.405 1.174.613 1.82.613.453 0 .908-.103 1.33-.312C31.354 53.183 32 52.14 32 51V39h9c5.514 0 10 4.486 10 10 0 2.21 1.79 4 4 4s4-1.79 4-4c0-9.925-8.075-18-18-18z"/></svg></a>												

													</div>

													<?php 

													if (isset($data['includes']['media']) )
													{

														echo '<div class="image mt-2">';
														$counter = 0;

														foreach ($data['includes']['media'] as $key => $media)
														{
															if ($media['type'] == 'video')
															{
																continue;
															}

															if (isset($media['url']) and $media['type'] == 'photo')
															{

															//calculte width when max-height is 200

																if (isset($media['width']) and isset($media['height']))
																{

																	$ratio = $media['height'] / 200;
																	$width = $media['width']/ $ratio;

																	echo '<a href="' . $media['url']  . '" target="_blank">';
																	echo '<img class="image-child mx-auto d-block lazyload img-fluid" data-src="' . $media['url'] . '" height="200" >';
																	echo '</a>';
																}
																else
																{
																	echo '<a href="' . $media['url']  . '" target="_blank">';
																	echo '<img class="image-child mx-auto d-block lazyload img-fluid" data-src="' . $media['url'] . '">';
																	echo '</a>';
																}


															}
															else if (isset($media['preview_image_url']))
															{

																if (isset($media['width']) and isset($media['height']))
																{
																	$ratio = $media['height'] / 200;
																	$width = $media['width']/$ratio;

																	echo '<a href="' . $media['preview_image_url']  . '" target="_blank">';
																	echo '<img class="image-child mx-auto d-block lazyload " data-src="' . $media['preview_image_url'] . '" height="200" width="' . $width . '"">';
																	echo '</a>';
																}
																else
																{
																	echo '<a href="' . $media['preview_image_url']  . '" target="_blank">';
																	echo '<img class="image-child mx-auto d-block lazyload" data-src="' . $media['preview_image_url'] . '" >';
																	echo '</a>';
																}
															}

															$counter++;
														}

														echo '</div>';

													}

													if (isset($data['extended_entities']))
													{
														foreach ($data['extended_entities'] as $extended_entry)
														{

															foreach ($extended_entry['media'] as $key_media => $extended_entry_media)
															{



															//check if vido
																if ($extended_entry_media['type'] == 'video' and isset($extended_entry_media['video_info']))
																{

																	$highest_bitrate = 0;
																	$best_key = 0;

																	foreach ($extended_entry_media['video_info']['variants'] as $key => $variant)
																	{

																		if (isset($variant['bitrate']) and $variant['bitrate'] > $highest_bitrate)
																		{
																			$highest_bitrate = $variant['bitrate'];
																			$best_key = $key;
																		}

																	}

																	$video_url = $extended_entry_media['video_info']['variants'][$best_key]['url'];

																	if (isset($extended_entry_media['video_info']['aspect_ratio']))
																	{
																		$width = $extended_entry_media['video_info']['aspect_ratio'][0];
																		$height = $extended_entry_media['video_info']['aspect_ratio'][1];
																		$height = $height/$width;
																	}

																	$rand = mt_rand(0,99999999);
																	?>

																	<div style="width: 100%;" class="my-2" id="player<?php echo $extended_entry_media['id'] . $type . $rand; ?>"></div>
																	<script>

																		var width_div = document.getElementById("kolonnebredde_breaking").clientWidth;

																		//console.log('Parent width users er ' + width_div);
																		var height_video = <?php echo $height;?> * width_div;

																		if (height_video > 349)
																		{
																			height_video = 350;
																		}

																		if (height_video == 0)
																		{
																			height_video = 350;
																		}

																		//console.log('Parent height users er ' + height_video);
																		<?php

																		if ($GLOBALS['video_js_loaded'] == false)
																		{
																			echo '<script type="text/javascript" src="js/clappr.min.js"></script>';
																			$GLOBALS['video_js_loaded'] = true;
																		}

																		?>
																		var player = new Clappr.Player({width: "100%", height: height_video, source: "<?php echo $video_url;?>", parentId: "#player<?php echo $extended_entry_media['id'] . $type . $rand; ?>"});
																	</script>


																	<?php
																	

																}

																else if ($extended_entry_media['type'] == 'photo')
																{
																	if (isset($extended_entry_media['media_url_https']))
																	{
																		$image_url = $extended_entry_media['media_url_https'];
																	}
																	else if (isset($extended_entry_media['media_url']))
																	{
																		$image_url = $extended_entry_media['media_url'];
																	}

																	if (isset($extended_entry_media['expanded_url']))
																	{
																		$expanded_url = $extended_entry_media['expanded_url'];
																	}
																	else if (isset($extended_entry_media['media_url']))
																	{
																		$expanded_url = $extended_entry_media['media_url'];
																	}

																	echo '<a target="_blank" href="' . $expanded_url   . '">';
																	echo '<img class="lazyload" data-src="' . $image_url. '" width="100%">';
																	echo '</a>';


																}

															}

														}

													}


													?>

												</div>

											</div>
										</div>
									</div>
								</div>

								<?php			 
							}?>

						</div>
						<div id="seeall" class="text-right mt-1" style=""><button type="button" class="btn btn-light">Show more</button>
						</div>
						<div style="display: none;" id="to_breaking_view" class="text-right mt-1 mb-5" style=""><a href="index.php?mode=breaking" class="btn btn-light" role="button">To breaking view</a><br>
						</div>
		<!--
		<div class="bg-success mt-3 p-4 text-white" style="height:200px;">annonse
		</div>-->

	</div>
</div>
</div>
</div>
<?php echo include 'footer.html';?>

<script type="text/javascript">
	var name =  '<?php echo $name; ?>';
	var numberofrows = <?php echo $show; ?>;
	var startnumber = <?php echo $start; ?>;
	var numFound = <?php echo 1000; ?>;
	var menu_type = '<?php echo $type;?>';
	var mode = '<?php echo $mode;?>';

</script>

<script src="js/customjs.js"></script>


</body>
</html>


<?php

function time_elapsed_string($datetime, $full = false) {
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'y',
		'm' => 'm',
		'w' => 'w',
		'd' => 'd',
		'h' => 'h',
		'i' => 'm',
		's' => 's',
		);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . '' . $v . ($diff->$k > 1 ? '' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) . '' : 'just now';
}

function time_elapsed_string_breaking($datetime, $full = false) {
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'year',
		'm' => 'month',
		'w' => 'week',
		'd' => 'd',
		'h' => 'h',
		'i' => 'm',
		's' => 'second',
		);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . '' . $v . ($diff->$k > 1 ? '' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) . '' : 'just now';
}




function display_tweet($user_data, $data, $show, $type, $parent_data, $is_referenced_tweet, $user_list, $source)
{
	$is_retweet = 0;
	
	//@todo, fjern $parent_data

	if (!isset($data['data']['text']))
	{
		//echo 'return, missing text';
		//echo '$is_referenced_tweet er ' . $is_referenced_tweet . '<br>';
		//var_dump($data);
		return;
	}

	$text = $data['data']['text'];
	
	//remove link in text 
	if (isset($data['data']['entities']['urls']))
	{
		foreach ($data['data']['entities']['urls'] as $entry)
		{
			$url = $entry['url'];

			$text = str_replace($url, '', $text);
		}

	}

	if (empty($user_data))
	{
		$user_data = [];
		$user_data['data']['0']['profile_image_url'] = 'img/dummy_profile.jpg';

		$author_id = $data['data']['author_id'];

		$user_data['data']['0']['username'] = '';
		$user_data['data']['0']['name'] = '';

		if(isset($data['includes']['users']))
		{

			foreach ($data['includes']['users'] as $user)
			{
				if ($user['id'] == $author_id)
				{
					$user_data['data']['0']['username'] = $user['username'];
					$user_data['data']['0']['name'] = $user['name'];
				}
			}

		}

		$external_user = 1;
	}
	else
	{
		$external_user = 0;
	}


	if ($is_referenced_tweet == 'yes')
	{
		?>
		<div class="border preview-corners-referenced px-1 px-sm-3 pb-1 pb-sm-3 pt-1 mt-3">
			<?php
		}

		?>
		<div class="d-flex flex-column mb-1<?php
		if ($is_referenced_tweet == 'no')
		{
			echo ' border-bottom pb-4';
		}
		?>">
		<div class="d-flex justify-content-between">
			<div class="d-flex mt-2 w-100">
				<?php
				if ($is_referenced_tweet == 'no')
				{
					?>
					<div class="mr-2 d-none d-sm-inline">			
						<?php
						echo '<a href="https://twitter.com/' .  $user_data['data']['0']['username'] . '">';

						if ($GLOBALS['users_runner'] < $GLOBALS['users_runner_limit'])
						{
							echo '<img class="rounded-circle profile_image" src="' . $user_data['data']['0']['profile_image_url'] . '" >'; 
							$GLOBALS['users_runner']++;
						}
						else
						{
							echo '<img class="rounded-circle lazyload profile_image" data-src="' . $user_data['data']['0']['profile_image_url'] . '" >'; 
						}
						?>
					</a>
				</div>	
				<?php } ?>

				<div class="d-flex flex-column w-100 kolonnebredde" id="kolonnebredde">
					<div class="d-flex flex-wrap"><?php

						if ($is_referenced_tweet == 'yes')
						{
							echo '<div class="mr-2 ">';
							echo '<a href="https://twitter.com/' .  $user_data['data']['0']['username'] . '">'; 

							if ($GLOBALS['users_runner'] < $GLOBALS['users_runner_limit'])
							{
								echo '<img class="rounded-circle" src="' . $user_data['data']['0']['profile_image_url'] . '" width="30px" height="30px">'; 
								$GLOBALS['users_runner']++;
							}
							else
							{
								echo '<img class="rounded-circle lazyload" data-src="' . $user_data['data']['0']['profile_image_url'] . '" width="30px" height="30px">'; 
							}
							?>
						</a>
					</div>	
					<?php 
					} 
					else
					{
							echo '<div class="mr-2 d-inline d-sm-none">';
							echo '<a href="https://twitter.com/' .  $user_data['data']['0']['username'] . '">'; 

							if ($GLOBALS['users_runner'] < $GLOBALS['users_runner_limit'])
							{
								echo '<img class="rounded-circle" src="' . $user_data['data']['0']['profile_image_url'] . '" width="30px" height="30px">'; 
								$GLOBALS['users_runner']++;
							}
							else
							{
								echo '<img class="rounded-circle lazyload" data-src="' . $user_data['data']['0']['profile_image_url'] . '" width="30px" height="30px">'; 
							}
							?>
						</a>
					</div>


					<?php
					}
					?>


					<div class="mr-2 my-auto">
						<span class="font-weight-bold text"><a class="text-dark" href="https://twitter.com/<?php echo $user_data['data']['0']['username'];?>"><?php echo $user_data['data']['0']['name'] ; ?></a></span>
					</div>
					<div class="mr-2 my-auto">
						<span class="text">
							<a href="https://twitter.com/
							<?php 
							echo $user_data['data']['0']['username'];
							?>
							">
							@<?php echo $user_data['data']['0']['username']; ?>
						</a>
					</span>
				</div>
				<div class="mr-2 text tweettime my-auto" data-toggle="tooltip" title="<?php echo date_format(date_create($data['data']['created_at']),"Y/m/d H:i:s"); ?>">
					<a href="<?php echo 'https://twitter.com/' . $user_data['data']['0']['username'] . '/status/' . $data['data']['id']; ?>">
						<div class="tweet-user text-dark" created_at="<?php echo $data['data']['created_at']; ?>">
							<?php																						
							echo time_elapsed_string($data['data']['created_at']);
							?>
						</div> 
					</a>
				</div>
				<?php
				if ($is_referenced_tweet == 'no')
				{
					?>
					<div class="ml-auto ">
						<a class="mr-2" data-toggle="tooltip" title="Show all from <?php echo $user_data['data']['0']['name'] . '"';
						?> href="index.php?<?php echo 'start=' . 0 . '&show=' . $show . '&name=' . $user_data['data']['0']['username'];?>"><svg xmlns="http://www.w3.org/2000/svg" height="20" fill="#212529" class="bi bi-card-list" viewBox="0 0 16 16">
						<path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
						<path d="M5 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 5 8zm0-2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0 5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-1-5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zM4 8a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm0 2.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
					</svg></a>
					<img src="img/Twitter_logo_blue_32.png" width="24">	
				</div>
				<?php
			}
			?>
		</div>
		<?php 

		if (isset($data['data']['referenced_tweets']))
		{	
			echo '<div class="text mt-1">';

			$type_reference = '';
			$type_local = '';
			$referenced_users_string = '';

			foreach ($data['data']['referenced_tweets'] as $referenced_local)
			{
				$type_reference = $referenced_local['type'];

				if(!isset($data['referenced_tweets_download']))
				{
					continue;
				}

				foreach ($data['referenced_tweets_download'] as $downloaded_tweet)
				{
					if (isset($downloaded_tweet['data']['id']) and $referenced_local['id'] == $downloaded_tweet['data']['id'])
					{

						$author_id = $downloaded_tweet['data']['author_id'];

						foreach ($downloaded_tweet['includes']['users'] as $downloaded_user)
						{

							if ($author_id == $downloaded_user['id'])
							{

								if ($type_reference == 'replied_to')
								{
									$type_local = 'Reply to ' . make_user_link($user_list, $downloaded_user['username'], $show);
									$type_icon = 'fa-retweet';
								}
								else if ($type_reference == 'quoted')
								{
									$type_local = 'Quote of ' . make_user_link($user_list, $downloaded_user['username'], $show);
									$type_icon = 'fa-retweet';
								}
								else if ($type_reference == 'retweeted')
								{
									$type_local = 'Retweet from ' . make_user_link($user_list, $downloaded_user['username'], $show);
									$type_icon = 'fa-retweet';
								}

								$retweet_icon = '<svg width="22px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 72" > <path  fill="#000" d="M70.676 36.644C70.166 35.636 69.13 35 68 35h-7V19c0-2.21-1.79-4-4-4H34c-2.21 0-4 1.79-4 4s1.79 4 4 4h18c.552 0 .998.446 1 .998V35h-7c-1.13 0-2.165.636-2.676 1.644-.51 1.01-.412 2.22.257 3.13l11 15C55.148 55.545 56.046 56 57 56s1.855-.455 2.42-1.226l11-15c.668-.912.767-2.122.256-3.13zM40 48H22c-.54 0-.97-.427-.992-.96L21 36h7c1.13 0 2.166-.636 2.677-1.644.51-1.01.412-2.22-.257-3.13l-11-15C18.854 15.455 17.956 15 17 15s-1.854.455-2.42 1.226l-11 15c-.667.912-.767 2.122-.255 3.13C3.835 35.365 4.87 36 6 36h7l.012 16.003c.002 2.208 1.792 3.997 4 3.997h22.99c2.208 0 4-1.79 4-4s-1.792-4-4-4z"/></svg> ' ;

								$referenced_users_string .=  $retweet_icon . $type_local;


							}

						}


					}
				}


			}

			echo $referenced_users_string;
			echo '</div>';
		}

		?>					

		<?php 

		if (isset($data['data']['entities']['mentions']))
		{
			$original_text = $data['data']['text'];

			foreach ($data['data']['entities']['mentions'] as $mention)
			{

									if (isset($user_list[$mention['username']])) //in our list? 
									{
										
										$mention_link = '<a target="_blank" href="' . 'index.php?' . 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($mention['username']) . '">' .'@' . $mention['username'] . '</a>';
										$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
									}
									else
									{
										$substring = $mention['username'];
										$mention_link = '<a target="_blank" href="' . 'https://twitter.com/' . $mention['username'] . '">' .'@' . $substring . '</a>';
										$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
									}

								}
							}

							if (isset($data['data']['entities']['hashtags']))
							{

								foreach ($data['data']['entities']['hashtags'] as $hashtag)
								{

									$link = '<a target="_blank" href="' . 'https://twitter.com/search?q=' . $hashtag['tag'] . '">#' . $hashtag['tag'] . '</a>';
									$text = str_ireplace('#' . $hashtag['tag'], $link, $text);

								}

							}


							if ($is_referenced_tweet == 'no' and isset($type_reference) and $type_reference == 'retweeted')
							{
							//echo $text; 
							}
							else if ($is_referenced_tweet == 'yes')
							{
								$text = str_replace("\n", "<br>", $text);
								echo '<div class="text my-2 word-wrap">';
								echo "$text"; 
								echo '</div>';

							}
							else
							{
								$text = str_replace("\n", "<br>", $text);
								echo '<div class="text my-2 word-wrap">';
								echo $text;
								echo '</div>'; 
							}


							if (isset($data['data']['attachments']['poll_ids']))
							{
								if (isset($data['data']['id']))
								{
									echo '<div class="text my-2 word-wrap">';
									echo ' <a target="_blank" href="' . 'https://twitter.com/' . $user_data['data']['0']['username'] . '/status/' . $data['data']['id'] . '">';
									echo 'Poll ';
									echo '</a>';
									echo '</div>'; 
								}
							}

							$preview_switch = 'off';

							if (isset($data['data']['entities']['urls']))
							{

								$url_already_used = [];

								foreach ($data['data']['entities']['urls'] as $urlrunner => $url)
								{

									if (!isset($url['title']) or !isset($url['description']) or (!isset($url['expanded_url']) and !isset($url['unwound_url']) ))
									{
							//echo 'Url preview not set, will add to list...<br>';

										$skip_link = 0;

										if (file_exists('../code/previews/users/' . $data['data']['id'] . '.json'))
										{

											if ($previews = readJSON('../code/previews/users/' . $data['data']['id'] . '.json') and !isset($url_already_used[$data['data']['id']]))
											{

												$url_already_used[$data['data']['id']] = 1;

												foreach ($previews as $key => $preview)
												{
													if (isset($preview['title']) and $preview['title'] != '')
													{
														echo '<div class="d-flex flex-column mt-2 mb-0 mb-md-2 preview-corners bg-secondary border">';
														$width = 1;
														$best_key = 0;

														if (isset($preview['cover']) and $preview['cover'] != '')
														{
															echo '<div class="preview-image-holder ">';
															echo '<a target="_blank" href="' . $preview['url_source'] . '">';
															echo '<img class="preview-corners-image lazyload " width="100%" data-src="' . $preview['cover'] . '">' ;
															echo '</a>';
															echo '</div>';
														}

														echo '<div class="pt-sm-3 pt-2 px-sm-3 px-2 "> ';
														echo '<a target="_blank" href="' . $preview['url_source'] . '"><h4 class="text-dark preview-title word-wrap">' . $preview['title'] . '</h4></a>';
														echo '</div>';

														if (isset($preview['description']) and $preview['description'] != '')
														{
															echo '<div class="pb-0 px-2 px-sm-3">';
															echo '<a target="_blank" href="' . $preview['url_source'] . '"><p class="text-dark word-wrap">' . $preview['description'] . '</p></a>';
															echo '</div>';
														}

														echo '</div>';

														$skip_link = 1;
													}
												}

											}

										}

										if ($skip_link == 1)
										{
											continue;
										}

									}


									if (isset($url['images']))
									{
										echo '<div class="d-flex flex-column mt-2 mb-0 mb-md-2 pt-sm-3 pt-2 px-sm-3 px-2 preview-corners bg-secondary border">';

										$width = 1;
										$best_key = 0;

										foreach ($url['images'] as $key =>  $image)
										{

											if ($image['width'] > $width)
											{
												$width = $image['width'];
												$best_key = $key;
											}

										}

										echo '<div class="d-flex flex-column flex-sm-row">';
										echo '<div class="preview-image-holder " style="width: 200px;">';

										if (isset($url['url']))
										{

											echo '<a target="_blank" href="' . $url['url'] . '" title='. $url['expanded_url'] . '>';

											if ($GLOBALS['users_runner'] < $GLOBALS['users_runner_limit'])
											{
												echo '<img title="" class=" " width="100%" src="' . $url['images'][$best_key]['url'] . '">' ;
												$GLOBALS['users_runner']++;
											}
											else
											{
												echo '<img title="" class=" lazyload" width="100%" data-src="' . $url['images'][$best_key]['url'] . '">' ;
											}

											echo '</a>';
										}

										echo '</div>';

										$preview_switch = 'on';

										echo '<div class="d-flex flex-column">';

										if (isset($url['title']))
										{
											echo '<div class="px-2" title='. $url['expanded_url'] . '>';
											echo '<a target="_blank" href="' . $url['url'] . '"><h4 class="text-dark preview-title word-wrap">' . $url['title'] . '</h4></a>';
											echo '</div>';
										}

										if (isset($url['description']))
										{
											echo '<div class="pb-0 px-sm-3 px-2" title='. $url['expanded_url'] . '>';
											echo '<a target="_blank" href="' . $url['url'] . '"><p class="text-dark word-wrap">' . $url['description'] . '</p></a>';
											echo '</div>';
										}

										echo '</div>';
										echo '</div>';
										echo '</div>';
										continue;

									}
									else
									{

										if (isset($url['url']) and !strpos($url['expanded_url'], 'twitter.com') !== false)
										{

											echo '<div class="mt-2 text ">';

											if (isset($url['display_url']))
											{
												echo '<div class="pb-0" title='. $url['display_url'] . '>';
												echo '<a class="text-primary" target="_blank" href="' . $url['url'] . '"><p class="word-wrap">' . $url['display_url'] . '</p></a>';
												echo '</div>';
											}
											else if (isset($url['expanded_url']))
											{
												echo '<div class="pb-0" title='. $url['expanded_url'] . '>';
												echo '<a class="text-primary" target="_blank" href="' . $url['url'] . '"><p class="word-wrap">' . $url['expanded_url'] . '</p></a>';
												echo '</div>';
											}
											else
											{
												echo '<div class="pb-0 " title='. $url['url'] . '>';
												echo '<a class="text-primary" target="_blank" href="' . $url['url'] . '"><p class="word-wrap">' . $url['url'] . '</p></a>';
												echo '</div>';

											}

											echo '</div>';
										}								
									}						

								}


							}

							?>

							<?php 

							$type_reference = '';

							if (isset($data['data']['referenced_tweets']))
							{	

								foreach ($data['data']['referenced_tweets'] as $referenced_local)
								{
									$type_reference = $referenced_local['type'];
									break;
								}

							}

							if (isset($data['includes']['media']) and ($type_reference != 'retweeted'))
							{

						//var_dump($data['includes']['media']);


								$counter = 0;


								foreach ($data['includes']['media'] as $key => $media)
								{

									if ($media['type'] == 'video')
									{
										continue;
									}


									if (isset($media['url']) and $media['type'] == 'photo')
									{

										if ($preview_switch == 'off')
										{
											echo '<div class="image mb-1">';
											echo '<a target="_blank" href="' . $media['url']  . '" target="_blank">';

									//var_dump($media);

											if ($GLOBALS['users_runner'] < $GLOBALS['users_runner_limit'])
											{

												if (isset($media['width']) and isset($media['height']))
												{

													$ratio = $media['height'] / 350;
													$width = $media['width']/ $ratio;

													echo '<a href="' . $media['url']  . '" target="_blank">';
													echo '<img class="image-child mx-auto d-block  img-fluid" src="' . $media['url'] . '" height="350" >';
													echo '</a>';
												}
												else
												{
													echo '<a href="' . $media['url']  . '" target="_blank">';
													echo '<img class="image-child mx-auto d-block  img-fluid" src="' . $media['url'] . '">';
													echo '</a>';
												}


										//echo '<img class="image-child  mx-auto d-block img-fluid" src="' . $media['url'] . '" >';
											}
											else
											{
												if (isset($media['width']) and isset($media['height']))
												{

													$ratio = $media['height'] / 350;
													$width = $media['width']/ $ratio;

													echo '<a href="' . $media['url']  . '" target="_blank">';
													echo '<img class="image-child mx-auto d-block lazyload img-fluid" data-src="' . $media['url'] . '" width="' . $width . '" >';
													echo '</a>';
												}
												else
												{
													echo '<a href="' . $media['url']  . '" target="_blank">';
													echo '<img class="image-child mx-auto d-block lazyload img-fluid" data-src="' . $media['url'] . '">';
													echo '</a>';
												}

											}

											echo '</a>';
											echo '</div>';
										}					

									}
									else if (isset($media['preview_image_url']))
									{
										echo '<div class="image">';
										echo '<a target="_blank" href="' . $media['preview_image_url']  . '" target="_blank">';
										echo '<img class="image-child lazyload mx-auto d-block img-fluid"  data-src="' . $media['preview_image_url'] . '" >';
										echo '</a>';
										echo '</div>';
									}	
								}


								if (isset($data['extended_entities']))
								{
									foreach ($data['extended_entities'] as $extended_entry)
									{


										foreach ($extended_entry['media'] as $extended_entry_media)
										{

									//check if vido
											if ($extended_entry_media['type'] == 'video' and isset($extended_entry_media['video_info']))
											{

												$highest_bitrate = 0;
												$best_key = 0;

												foreach ($extended_entry_media['video_info']['variants'] as $key => $variant)
												{

													if (isset($variant['bitrate']) and $variant['bitrate'] > $highest_bitrate)
													{
														$highest_bitrate = $variant['bitrate'];
														$best_key = $key;
													}

												}

												$video_url = $extended_entry_media['video_info']['variants'][$best_key]['url'];

												if (isset($extended_entry_media['video_info']['aspect_ratio']))
												{
													$width = $extended_entry_media['video_info']['aspect_ratio'][0];
													$height = $extended_entry_media['video_info']['aspect_ratio'][1];
													$height = $height/$width;
												}

												$rand = mt_rand(0,99999999);

												?>

												<div style="width: 100%;" class="my-2" id="player<?php echo $extended_entry_media['id'] . $type . $rand; ?>"></div>
												<script>
												//var width_div = $(".kolonnebredde").width();
												//console.log('Parent width users er ' + width_div);
												var width_div = document.getElementById("kolonnebredde").clientWidth;

												var height_video = <?php echo $height;?> * width_div;

												if (height_video > 349)
												{
													height_video = 350;
												}

												if (height_video == 0)
												{
													height_video = 350;
												}

												//console.log('Parent height users er ' + height_video main thread);
												<?php

												if ($GLOBALS['video_js_loaded'] == false)
												{
													?></script>
													<script type="text/javascript" src="js/clappr.min.js"></script>
													<script>
														<?php 
														$GLOBALS['video_js_loaded'] = true;
													}

													?>
													var player = new Clappr.Player({width: "100%", height: height_video, source: "<?php echo $video_url;?>", parentId: "#player<?php echo $extended_entry_media['id'] . $type . $rand; ?>"});
												</script>


												<?php


											}

											else if ($extended_entry_media['type'] == 'photo')
											{
												if (isset($extended_entry_media['media_url_https']))
												{
													$image_url = $extended_entry_media['media_url_https'];
												}
												else if (isset($extended_entry_media['media_url']))
												{
													$image_url = $extended_entry_media['media_url'];
												}

												if (isset($extended_entry_media['expanded_url']))
												{
													$expanded_url = $extended_entry_media['expanded_url'];
												}
												else if (isset($extended_entry_media['media_url']))
												{
													$expanded_url = $extended_entry_media['media_url'];
												}
												echo '<div class="image">';
												echo '<a target="_blank" href="' . $expanded_url   . '">';
												echo '<img class="lazyload" data-src="' . $image_url. '" width="100%">';
												echo '</a>';
												echo '</div>';


											}



										}


									}
								}


								$counter++;




								if ($is_retweet == 1)
								{

						//https://twitter.com/MeidasTouch/status/1418310387368296454
									$id = $data['data']['conversation_id'];

									$link = 'https://twitter.com/' . $retweet_name . '/status/' . $id ;
									?>
									<div class="mt-2">
										<a target="_blank" href="<?php echo $link ;?>">Show this thread</a>
									</div>
									<?php 
								}


							}



							if (isset($data['referenced_tweets_download'])) 
							{
						//var_dump($data['referenced_tweets']);
								foreach ($data['referenced_tweets_download'] as $data_downloaded)
								{

								//get username
									$tweet = false;

									if (isset($data_downloaded['includes']['users']) and isset($data['data']['author_id']))
									{
										$author_id = $data_downloaded['data']['author_id'];

										foreach ($data_downloaded['includes']['users'] as $user)
										{
											if ($user['id'] == $author_id)
											{

												$tweet = [0,$user['username']];
											}
										}

									}

									$user_data = get_user_data($tweet, $source);

									display_tweet($user_data, $data_downloaded, $show, $type, $data, 'yes', $user_list, $source);

								}

							}

							if ($is_referenced_tweet == 'no')
							{
								?>
								<div class="mt-2">
									<a class="mr-2 replytweet" target="_blank" href="https://twitter.com/intent/tweet?in_reply_to=<?php echo $data['data']['id']; ?>"><svg width="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 72"><path fill="#AAB8C2" d="M41 31h-9V19c0-1.14-.647-2.183-1.668-2.688-1.022-.507-2.243-.39-3.15.302l-21 16C5.438 33.18 5 34.064 5 35s.437 1.82 1.182 2.387l21 16c.533.405 1.174.613 1.82.613.453 0 .908-.103 1.33-.312C31.354 53.183 32 52.14 32 51V39h9c5.514 0 10 4.486 10 10 0 2.21 1.79 4 4 4s4-1.79 4-4c0-9.925-8.075-18-18-18z"/></svg></a>
									<a class="mr-2 retweettweet" target="_blank" href="https://twitter.com/intent/retweet?tweet_id=<?php echo $data['data']['id']; ?>"><svg width="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 72" > <path  fill="#AAB8C2" d="M70.676 36.644C70.166 35.636 69.13 35 68 35h-7V19c0-2.21-1.79-4-4-4H34c-2.21 0-4 1.79-4 4s1.79 4 4 4h18c.552 0 .998.446 1 .998V35h-7c-1.13 0-2.165.636-2.676 1.644-.51 1.01-.412 2.22.257 3.13l11 15C55.148 55.545 56.046 56 57 56s1.855-.455 2.42-1.226l11-15c.668-.912.767-2.122.256-3.13zM40 48H22c-.54 0-.97-.427-.992-.96L21 36h7c1.13 0 2.166-.636 2.677-1.644.51-1.01.412-2.22-.257-3.13l-11-15C18.854 15.455 17.956 15 17 15s-1.854.455-2.42 1.226l-11 15c-.667.912-.767 2.122-.255 3.13C3.835 35.365 4.87 36 6 36h7l.012 16.003c.002 2.208 1.792 3.997 4 3.997h22.99c2.208 0 4-1.79 4-4s-1.792-4-4-4z"/></svg></a>
									<a class="liketweet" target="_blank" href="https://twitter.com/intent/like?tweet_id=<?php echo $data['data']['id']; ?>"><svg width="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 72"><path fill="#AAB8C2" d="M38.723,12c-7.187,0-11.16,7.306-11.723,8.131C26.437,19.306,22.504,12,15.277,12C8.791,12,3.533,18.163,3.533,24.647 C3.533,39.964,21.891,55.907,27,56c5.109-0.093,23.467-16.036,23.467-31.353C50.467,18.163,45.209,12,38.723,12z"/>
									</svg>
								</a>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
		if ($is_referenced_tweet == 'yes')
		{
			?>
		</div>
		<?php
	}

	?>
	
	<?php			 
}


function get_tweet_data($tweet, $source)
{


	if (!isset($tweet[1]))
	{
		return false;
	}

	if ($source == 'users')
	{

		if (!$data = readJSON(mb_strtolower('../code/users/' . $tweet[1] . '/tweets/' . $tweet[0] . '.json')))
		{
		//echo 'Error1';
			return false;
		}
	}
	else if ($source == 'breaking')
	{
		if (!$data = readJSON('../code/breaking/' . mb_strtolower($tweet[1] . '/tweets/' . $tweet[0] . '.json')))
		{
		//echo 'Error2 ' . '../code/breaking/' . mb_strtolower($tweet[1] . '/tweets/' . $tweet[0] . '.json');
			return false;
		}
	}
	else
	{
		return false;
	}

	return $data;
}



function get_user_data($tweet, $source)
{

	if(!isset($tweet[1]))
	{
		return false;
	}


	if ($source == 'users')
	{
		if (!$user_data = readJSON('../code/users_info/users/' . mb_strtolower($tweet[1]) . '.json'))
		{
	//echo 'Error';
			return false;
		}
	}
	else if ($source == 'breaking')
	{
		if (!$user_data = readJSON('../code/users_info/breaking/' . mb_strtolower($tweet[1]) . '.json'))
		{
		    //echo 'Error';
			return false;
		}
	}									


	return $user_data;

}

function tweet_reader($file, $start, $stop)
{

	$diff = $stop - $start;

	$user_tweets_list = [];

	if ($file and !$file->eof()) 
	{

		if ($start > 0)
		{
			$file->seek($start-1);
		}

		$read_counter = 0; 

		while($stop > $read_counter)
		{

			$user_tweets_list[] = $file->fgetcsv();
			$read_counter++;

			if ($file->eof() or $read_counter > $diff)
			{

				break;
			}

		}

		return $user_tweets_list;

	}
	else
	{
		return false;
	}

}

function make_user_link($user_list, $username, $show)
{

	$mention_link = '<a target="_blank" href="' . 'https://twitter.com/' . $username . '">'  . '@' . $username . '</a> ';
	
	return $mention_link;
}

?>

<script type="text/javascript" 
twttr.widgets.load();</script>

