<?php
include 'header.html';
?>

<div class="container mt-4">
<div clasS="row">
<div class="col-12">
<h1>About us</h1>
<p>
Wallstreet100.com is a site dedicated to both financial news and opinions from influential investors, journalists, CEOs and economists. 
<br><br>
We follow 100 of the most important Twitter accounts in finance. Selection is based on a set of criterias including content quality, tweet frequency, number of followers, career/position, celebrity factor, diversity in opinions/area of interest and language (English only). 
<br><br>
We are always trying to improve the list of the 100 most important accounts. Feel free to contact us on adress "contact" at domain name "wallstreet100.com" if you have any suggestions.  
   <br><br>
In addition, we also follow a wide range of Twitter accounts in the financial media, to bring you breaking news as fast as possible. <br>
The content of all tweets are the copyright of Twitter and/or their users.
<br><br>
<i>The Wallstreet100.com team</i>
</p>

</div>
</div>
</div>
<?php include 'footer.html';?>
</body>
</html>
