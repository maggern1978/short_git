<?php

//https://developer.twitter.com/en/docs/twitter-api/rate-limits#v2-limits
//https://developer.twitter.com/en/docs/projects/faq

require_once('twitter-api-php-master/TwitterAPIExchange.php');

//wallstreet100
    $settings = array(
      'oauth_access_token' => "1430827600809799684-K7iJXqqP1XaTfDcBB8ZGx4ek6iiWb9",
      'oauth_access_token_secret' => "J6nChnJp6QPQmfXyQvFKH2WLi8TTVGuJcwQNU3YhDVqeS",
      'consumer_key' => "5TuFX0ZAcnE9lnx4Jg7MTMzMz",
      'consumer_secret' => "xvHx13lKBdqN7mfgnDFCxskbiyUuDlmXWECYRboCUTuyElTokJ"
      );

//shortnordic
//$settings = array(
//	'oauth_access_token' => "1058139584113098752-6o779K154M8loaVx75oG5i2YoKnrsZ",
	//'oauth_access_token_secret' => "V9TON0QuCpR7TbfL7uHCvIUQDlXMZj5jMBNY6mZHW1mAC",
	//'consumer_key' => "9iYS641QFf9GeDD8tNJV3qg1j",
	//'consumer_secret' => "xfBC8K0rVEEIl3KXmq2Jcw0qwRHtZGILCZ5nSSr2jImKbbEWcN"
	//);

include_once 'functions.php';
include_once 'functions_flashtweets.php';

$develop = 0;
$get_number_of_tweets = 10;
$only_download_name = 'ritholtz';
$only_download_switch = 'off';
//$mode = 'users';
//$mode = 'breaking';


if (!isset($top_users))
{
	$top_users = 'no';
}

if (!isset($mode))
{
	$mode = 'users';
}

$users = [];

if ($mode == 'users')
{
	$handle = fopen("data/user_list.csv", "r");
	$savedirectory = 'users/';
}
else if ($mode == 'breaking')
{
	$handle = fopen("data/breaking_list.csv", "r");
	$savedirectory = 'breaking/';
}
else
{
	return('No mode set!!');

}

successecho("Mode er $mode: <br>");
successecho("top_users er $top_users: <br>");

if ($handle) 
{
	while (($line = fgets($handle)) !== false) 
	{
		$users[] = trim($line);    
	}

	fclose($handle);
} 
else 
{
  errorecho('Error opening user file!');  // error opening the file.
  exit('');
}



  //https://twitteroauth.com/

  //$statuses = $connection->get("search/tweets", ["q" => "shortnordic"]);
	$tweets = [];

	foreach ($users as $key => $username)
	{

		//check if rate was exceeded last round

		$the_time_now = time();
		$one_minute_ago = $the_time_now - 60;
		$two_minutes_ago = $the_time_now - 120;

		$one_minute_ago = date('Y-m-d_h.i', $one_minute_ago);
		$two_minutes_ago = date('Y-m-d_h.i', $two_minutes_ago);

		if (file_exists('errors/rate_limit_exceeded_' . $one_minute_ago  . '.json') or file_exists('errors/rate_limit_exceeded_' . $two_minutes_ago  . '.json'))
		{
			errorecho('Rate limit file exists withing 2 minutes, returning...<br>');
			return; 
		}


		if ($top_users == 'yes')
		{

			if ($key > 18)
			{
				echo 'Breaking because of top_user = yes and key is over 18. ';
				break;
			}

		}

		echo $username . ' | ';
		if (mb_strtolower($username) != $only_download_name and $only_download_switch == 'on')
		{
			continue;
		}

		echo $username;

    	//make dir if not existing
		if (!check_if_dir_exists($savedirectory . $username))
		{
			erorecho('Error creating user directory<br>');
			continue;
		}

		if (!check_if_tweets_dir_exists($savedirectory . $username))
		{
			erorecho('Error creating tweets directory<br>');
			continue;
		}

    	//get user id
		if (!$id = readJSON('users_info/' . $mode . '/' . mb_strtolower($username) . '.json'))
		{
			errorecho('Could not read user info! ' . mb_strtolower($username) . ', continuing...<br>');
			continue;
		}

		echo ' | Username is ' . $username . '<br>';

		if (!isset($id['data'][0]['id']))
		{
			errorecho('id not set, skipping!<br>');
			continue;
		}
		
		$id = $id['data'][0]['id'];

	    //get previous id
		if (!file_exists(mb_strtolower($savedirectory . $username . '/' . $username . '_replies.csv')) or !$previous = readCSVkomma(mb_strtolower($savedirectory . $username . '/' . $username . '_replies.csv')))
		{
			$previous = [];
		}

		if (empty($previous[0][0]))
		{
			errorecho('Getting statuses WITHOUT since_id<br>');
			
			//trigger exception in a "try" block
			try 
			{
				$statuses = $output = returnTweet($id, "");
			}

			//catch exception

			catch(Exception $e) 
			{
				echo 'Message: ' . $e->getMessage();
				continue;
			}

    
		}
		else
		{
			echo 'Getting statuses WITH since_id<br>';
			
			$since_id = str_replace("_reply", "", $previous[0][0]);
			$since_id = str_replace("_media", "", $since_id );

			//trigger exception in a "try" block
			try 
			{
				$statuses = $output = returnTweet($id, $since_id);
			}

			//catch exception

			catch(Exception $e) 
			{
				echo 'Message: ' . $e->getMessage();
				continue;
			}


		}

		$headers = [];
		$output = rtrim($output);
		$data = explode("\n",$output);
		$headers['status'] = $data[0];
		array_shift($data);

		foreach($data as $part){

		    //some headers will contain ":" character (Location for example), and the part after ":" will be lost, Thanks to @Emanuele
		    $middle = explode(":",$part,2);

		    //Supress warning message if $middle[1] does not exist, Thanks to @crayons
		    if ( !isset($middle[1]) ) { $middle[1] = null; }

		    $headers[trim($middle[0])] = trim($middle[1]);
		}

		// Print all headers as array
		echo "<pre>";
		foreach ($headers as $headerkey => $header)
		{
			if ($headerkey == 'x-rate-limit-limit')
			{
				echo $headerkey . ' | ' . $header . '<br>';
			}
			else if ($headerkey == 'x-rate-limit-remaining')
			{
				echo $headerkey . ' | ' . $header . '<br>';

				//saveJSON($output, 'errors/' . $header . '_time_is_' . date('H.i.s') . '_mode_is_' . $mode . '_top_is_' . $top_users  . '_' . $username . '.json');

				if ($header == 0)
				{
					errorecho('Limit reached, returning...<br>');
					saveJSON($headers, 'errors/rate_limit_exceeded_' . date('Y-m-d_h.i') . '.json');
					return;
				}
			}
			else if ($headerkey == 'x-rate-limit-reset')
			{
				echo $headerkey . ' | ' . date('Y-m-d H:i:s', $header) . '<br>';
			}					


		}

		//var_dump($headers);

		echo "</pre>";

		//$statuses = $data['18'];

		$statuses = end($data);

		//var_dump($data);
		//data

		if ($develop == 0)
		{

		

			$original = $statuses;

			if (!$statuses = json_decode($statuses, true))
			{
				errorecho('Could not parse json reply from twitter.<br>');

				var_dump($statuses);

				exit();
				
				if (strpos($original, 'limit') !== false)
				{
					//echo $original . '<br>';
					saveJSON($original, 'errors/rate_limit_exceeded_' . date('Y-m-d_h.i') . '.json');
					return;
				}
				else
				{
					//echo $original . '<br>';
					saveJSON($original, 'errors/could_not_parse_reply_' . date('Y-m-d_h.i') . '.json');
				}

				continue; 
			}

			saveJSON($statuses, 'users/raw_dowload.json');

			//var_dump($statuses);

			//exit();

			if (isset($statuses['status']))
			{
				
				if ($statuses['status'] == 429)
				{
					echo '<h1>Error message is 429. Returned when a request cannot be served due to the apps rate limit having been exhausted for the resource. Returning to not overload system!<br></h1>';
					saveJSON($statuses, 'errors/' . date('Y-m-d,H.i.s') . $username . '.json');
					return;
				}

				if ($statuses['status'] == 420)
				{
					echo '<h1>Error message is 420. Returned when an app is being rate limited for making too many requests.. Returning to not overload system!<br></h1>';
					saveJSON($statuses, 'errors/' . date('Y-m-d,H.i.s') . $username . '.json');
					return;
				}				
			}

		}
		else
		{
			echo '<h1>Developer mode!</h1>';
			$statuses = readJSON('users/raw_dowload.json');
		}
		
		if (!empty($statuses))
		{
			if (isset($statuses['meta']['result_count']))
			{
				echo 'Downloaded ' . $statuses['meta']['result_count'] . ' tweets.<br>';

				if ($statuses['meta']['result_count'] == 0)
				{
					echo 'result_count is zero, continueing...<br>';
					echo '<br>----------------------<br><br>';
					continue;
				}

			}
			else
			{
				echo '$statuses["meta"]["result_count"] not set, continueing...<br>';
			}

		}

		echo '---------------------------------<br>';
	

		if (isset($statuses['data']))
		{
			foreach ($statuses['data'] as $index => $tweet)
			{
				$tweet_export = [];
				$tweet_export['data'] = $tweet;

				//data
				//includes media x
				//includes users/mentions x 
				//includes tweets

				$found_switch = 0; 

				echo $index . '. Doing data id ' . $tweet['id'] . '<br>';

				if(isset($tweet['attachments']['media_keys']))
				{
					echo 'Attachements Media(s) found!<br>';
					$image_urls = [];

					foreach ($tweet['attachments']['media_keys'] as $key)
					{
						if (isset($statuses['includes']['media']))
						{

							foreach ($statuses['includes']['media'] as $media)
							{

								if ($media['media_key'] == $key)
								{
									$tweet_export['includes']['media'][] = $media; 
									$found_switch = 1;
									break;
								}

							}

						}

					}

				}
			
				if (isset($tweet['entities']['mentions']))
				{
					
					$mentions_box = [];

					foreach ($tweet['entities']['mentions'] as $mention)
					{
						//echo '$mention["id"] er ' . $mention['id'] . '<br>';
						foreach ($statuses['includes']['users'] as $user)
						{
							
							if ($mention['id'] == $user['id'])
							{
								//echo 'Found user!<br>';
								$mentions_box[] = $user;
							}

						}
					}

					if (!empty($mentions_box))
					{
						$tweet_export['includes']['users'] = $mentions_box;
					}
					
				}				


				//lookup references tweets
				if (isset($tweet['referenced_tweets']))
				{
					$found_switch_referenced = 0;

					$referenced_tweets_box = [];

					foreach ($tweet['referenced_tweets'] as $reference)
					{
						
						$found_reference = 0;

						foreach ($statuses['includes']['tweets'] as $includes_tweets)
						{
							if ($reference['id'] == $includes_tweets['id'])
							{
								
								$tweet_export['includes']['tweets'] = $includes_tweets;

								//echo "referenced_tweets id found in data['includes']['tweets']<br>" ;
								$found_reference = 1;
								break;

							}
						}


						if ($found_reference == 1)
						{
							//continue;
						}

						//echo 'Referert tweet ikke funnet i includes | tweets, laster ned med get_single_tweet($reference["id"]' . '<br>';

						echo 'Laster ned get_single_tweet() for id ' . $reference['id'] .'<br>';


						if (!$status_referenced = json_decode(get_single_tweet($reference['id']), true))
						{
							errorecho('Could not parse json referenced reply from twitter, continuing..<br>');
							saveJSON($statuses, 'errors/could_not_parse_referenced_reply_' . date('Y-m-d_h.i.s') . '.json');
							continue; 
						}
						else
						{
							$tweet_export['referenced_tweets_download'][] = $status_referenced;
							echo ' ok!<br>';
						}
		
						
					}

			
				}


				if (isset($tweet_export['referenced_tweets_download']) and $mode == 'users')
				{
					echo 'Checking referenced_tweets_download for secondary tweets to download.<br>';

					foreach ($tweet_export['referenced_tweets_download'] as $key_teller => $referenced_tweet)
					{
						//var_dump($referenced_tweet);

						if (isset($referenced_tweet['data']['referenced_tweets']))
						{

							foreach ($referenced_tweet['data']['referenced_tweets'] as $reference)
							{

								if (!$status_referenced = json_decode(get_single_tweet($reference['id']), true))
								{
									errorecho('Could not parse json referenced reply from twitter, continuing..<br>');
									saveJSON($statuses, 'errors/could_not_parse_referenced_reply_' . date('Y-m-d_h.i.s') . '.json');
									continue; 
								}
								else
								{
									$tweet_export['referenced_tweets_download'][$key_teller]['referenced_tweets_download'][] = $status_referenced;
									echo 'Secondary download ok, must put data into $tweet_export';
								}

							}

						}

					}

				}

				//sjekk om video og last ned ekstra data i så fall
				//nivå 1
				if ($tweet_export = check_for_video($tweet_export))
				{

					echo 'Check for video ok<br>';
				}
				else
				{
					echo 'Check for video fails.<br>';
				}
									
				$file_ending = '';

				if (isset($tweet_export['data']['referenced_tweets']))
				{
					foreach ($tweet_export['data']['referenced_tweets'] as $referenced_twat)
					{
						if ($referenced_twat['type'] == 'replied_to' or $referenced_twat['type'] == 'retweeted')
						{
							$file_ending .= '_reply';
							break;
						}
					}
				}
			
		
				if (isset($tweet_export['includes']['media']))//nok? 
				{
					$file_ending .= '_media';
				}
		


				if (!file_exists(mb_strtolower($savedirectory . $username . '/tweets/' . $tweet['id'] . $tweet['id'] . $file_ending . '.json')))
				{
					saveJSON($tweet_export, mb_strtolower($savedirectory . $username . '/tweets/' . $tweet['id'] . $file_ending . '.json'));
				}
				else
				{
					echo 'Already saved file' .  $savedirectory . mb_strtolower($username) . '/tweets/' . $tweet['id'] . $file_ending .  '.json... will update.<br>';

					saveJSON($tweet_export, mb_strtolower($savedirectory . mb_strtolower($username . '/tweets/' . $tweet['id'] . $file_ending . '.json')));

				}

				echo '<br>------local-round----------------<br><br>';

			}
		}

		user_display_list_maker($savedirectory, $username);


		if (!empty($statuses)
)		{
			saveJSON($statuses, mb_strtolower($savedirectory . $username . '/' . $username . '_latest_download.json'));
		}
		else
		{
			errorecho('Data is empty for user ' . $username . '. Probably no new updates. <br><br>');
		}

		echo '<br>----------------------<br><br>';

	
	}


function buildBaseString($baseURI, $method, $params) {
    $r = array();
    ksort($params);
    foreach($params as $key=>$value){
        $r[] = "$key=" . rawurlencode($value);
    }
    return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
}

function buildAuthorizationHeader($oauth) {
    $r = 'Authorization: OAuth ';
    $values = array();
    foreach($oauth as $key=>$value)
        $values[] = "$key=\"" . rawurlencode($value) . "\"";
    $r .= implode(', ', $values);
    return $r;
}

//https://api.twitter.com/2/users/44196397/tweets

function returnTweet($user_id, $since_id){
    


    $oauth_access_token         = "1430827600809799684-K7iJXqqP1XaTfDcBB8ZGx4ek6iiWb9";
    $oauth_access_token_secret  = "J6nChnJp6QPQmfXyQvFKH2WLi8TTVGuJcwQNU3YhDVqeS";
    $consumer_key               = "5TuFX0ZAcnE9lnx4Jg7MTMzMz";
    $consumer_secret            = "xvHx13lKBdqN7mfgnDFCxskbiyUuDlmXWECYRboCUTuyElTokJ";

    //shortnordic
    //$oauth_access_token         = "1058139584113098752-6o779K154M8loaVx75oG5i2YoKnrsZ";
    //$oauth_access_token_secret  = "V9TON0QuCpR7TbfL7uHCvIUQDlXMZj5jMBNY6mZHW1mAC";
    //$consumer_key               = "9iYS641QFf9GeDD8tNJV3qg1j";
    //$consumer_secret            = "xfBC8K0rVEEIl3KXmq2Jcw0qwRHtZGILCZ5nSSr2jImKbbEWcN";


    if ($since_id != '')
    {
        //create request
        $request = array(
            'max_results'       => 5,
            'tweet.fields'      => 'attachments',
            'since_id'       => $since_id,
            'expansions'       => 'attachments.poll_ids,attachments.media_keys,author_id,entities.mentions.username,geo.place_id,in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id',
            'media.fields'       => 'duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics',
            'tweet.fields'       => 'attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,text,withheld'
        );       
    }
    else
    {
        //create request
        $request = array(
            'max_results'       => 25,
            'tweet.fields'      => 'attachments',
            'expansions'       => 'attachments.poll_ids,attachments.media_keys,author_id,entities.mentions.username,geo.place_id,in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id',
            'media.fields'       => 'duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics',
            'tweet.fields'       => 'attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,text,withheld'
        );  
    }


    $oauth = array(
        'oauth_consumer_key'        => $consumer_key,
        'oauth_nonce'               => time(),
        'oauth_signature_method'    => 'HMAC-SHA1',
        'oauth_token'               => $oauth_access_token,
        'oauth_timestamp'           => time(),
        'oauth_version'             => '1.0'
    );

    //  merge request and oauth to one array
        $oauth = array_merge($oauth, $request);

    //  do some magic
        $base_info              = buildBaseString("https://api.twitter.com/2/users/$user_id/tweets", 'GET', $oauth);
        $composite_key          = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature            = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature']   = $oauth_signature;

    //  make request
        $header = array(buildAuthorizationHeader($oauth), 'Expect:');
        $options = array( CURLOPT_HTTPHEADER => $header,
                          CURLOPT_HEADER => false,
                          CURLOPT_URL => "https://api.twitter.com/2/users/$user_id/tweets?" . http_build_query($request),
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_SSL_VERIFYPEER => false);

        $feed = curl_init();
        curl_setopt_array($feed, $options);

        //enable headers
        curl_setopt($feed, CURLOPT_HEADER, 1);

        $json = curl_exec($feed);
        curl_close($feed);

    return $json;
}


?>


