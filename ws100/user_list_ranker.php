<?php

include_once 'functions.php';
include_once 'functions_flashtweets.php';

//$mode = 'users';
//$mode = 'breaking';

$users = [];


$handle = fopen("data/user_list.csv", "r");
$savedirectory = 'users/';


if ($handle) 
{
	while (($line = fgets($handle)) !== false) 
	{
		$users[] = trim($line);    
	}

	fclose($handle);
} 
else 
{
  errorecho('Error opening user file!');  // error opening the file.
  exit('');
}


$result_array = [];

foreach ($users as $username)
{

	$csv = readCSVkomma('../code/users/' . mb_strtolower($username) . '/' . mb_strtolower($username) . '_replies.csv');

	
	$sum = 0;
	
	foreach ($csv as $key => $line)
	{

		$line[0] = str_replace("_reply", '', $line[0]);
		$line[0] = str_replace("_media", '', $line[0]);

		$sum += $line[0];

		//var_dump($line[0]);

		if ($key > 8)
		{
			break;
		}
	}

	$average = $sum / (($key + 1)*10000000);
	
	//echo 'Average is ' . $average . '<br>';

	$result_array[] = ['name' => $username, 'average' => $average];
}



usort($result_array, function ($item1, $item2) {
    return $item2['average'] <=> $item1['average'];
});

$export = [];

foreach ($result_array as $line)
{
	$export[] = $line['name'] . "\n";
}

echo 'Saving data/user_list_test.csv <br>';
file_put_contents('data/user_list.csv', $export);

?>