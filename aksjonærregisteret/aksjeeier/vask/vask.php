<?php

///// solr syntax
////D:\solr\solr-8.9.0\bin>solr create -c aksjeeier
////D:\solr\solr-8.9.0\example\exampledocs>java -Dc=aksjeeier -Dtype=text/csv -jar post.jar d:\solr\cleaned.csv
// se også https://stackoverflow.com/questions/30616707/simpleposttool-fatal-specifying-either-url-or-core-collection-is-mandatory/31848591



set_time_limit(200);

$counter = 0;
$runner = 0;
$file = fopen('aksjeeierbok.csv', 'r');
$errors = 0;


//open target file
$targetfile = fopen("cleaned.csv","w");
$shortfile = fopen("cleaned_short.csv","w");



//'Orgnr;Selskap;Aksjeklasse;Navn aksjonær;Fødselsår/orgnr;Postnr/sted;Landkode;Antall aksjer;Antall aksjer selskap' 

echo "//'Orgnr;Selskap;Aksjeklasse;Navn aksjonær;Fødselsår/orgnr;Postnr/sted;Landkode;Antall aksjer;Antall aksjer selskap'<br>"; 

$first_line = explode(";", "orgnr;selskap;aksjeklasse;navn_eier;fodselsaar_orgnr;postnr;poststed;landkode;antall aksjer;antall aksjer selskap");
fputcsv($targetfile, $first_line);

while (($line = fgetcsv($file)) !== FALSE) 
{

	$runner++;

	if ($runner == 1)
	{
		continue;
	}

	if (count($line) > 1) //fix input
	{
		//merge first entry in every entry over first to previous 

		$count_array = count($line);

		//echo $runner . '. Error. Count er ' . $count_array . '<br>';

		$new_array = explode(";", $line[0]);

		$rounds = 0;

		//var_dump($line);

		while($rounds < $count_array)
		{
			if ($rounds == 0)
			{
				$rounds++;
				continue;
			}
			
			$add_array = explode(";", $line[$rounds]);

			//var_dump($add_array);

			$insertion_point = count($new_array);

			//merge first to last entry in new_array
			$new_array[$insertion_point - 1] .= $add_array[0];

			//add remaining
			$add_array_count = count($add_array);

			for ($i = 1; $i < $add_array_count; $i++)
			{
				$new_array[$insertion_point + $i] = $add_array[$i];

			}
			
			$new_array = array_values($new_array);

			//var_dump($new_array);

			$rounds++;
		}

		$line_array = $new_array;
		

	}
	else
	{
		$line_array = explode(";", $line[0]);
	}


	if (!checkline($line_array))
	{

		echo $runner . '.-------------------<br>';

		$errors++;
			
		

		var_dump($line);
		var_dump($line_array);
			
		//read next line and try to repair
		//$next = fgetcsv($file);
		//$next_array = explode(";", $next[0]);
		//var_dump($next_array);
		
	}
	else
	{	
  		$line_array = splittpostnummer($line_array);
 

  		if (!checkline10($line_array))
  		{
  			var_dump($line_array);
  			exit();
  		}

  		fputcsv($targetfile, $line_array);


		if ($counter < 2000)
		{
			fputcsv($shortfile, $line_array);
		
		}  		
	}


	if ($counter > 90000)
	{
		//echo 'Breaking at ' . $counter . '<br>'; 
		//break;
	}

	$counter++;
}

echo 'Number of entries: ' . $counter . '<br>';
echo 'Number of errors: ' . $errors . '<br>';

fclose($targetfile);
fclose($file);
fclose($shortfile);



function splittpostnummer($array)
{

	$add_on = explode(" ", $array[5]);
	
	if (!isset($add_on[1]))
	{
		$add_on[0] = '';
		$add_on[1] = '';
	}

	$new_array = [];

	$new_array[0] = $array[0];
	$new_array[1] = $array[1];
	$new_array[2] = $array[2];
	$new_array[3] = $array[3];
	$new_array[4] = $array[4];		
	$new_array[5] = $add_on[0];
	$new_array[6] = $add_on[1];
	$new_array[7] = $array[6];
	$new_array[8] = $array[7];
	$new_array[9] = $array[8];

	return $new_array;

}


function checkline ($array) // 9
{

	$length = count($array);

	if ($length != 9)
	{
		return false; 
	}

	for ($i = 0; $i < 9; $i++)
	{

		if (!isset($array[$i]))
		{
			return false; 
		}

	}

	return true; 

}



function checkline10 ($array) //10
{

	$length = count($array);

	if ($length != 10)
	{
		return false; 
	}

	for ($i = 0; $i < 10; $i++)
	{

		if (!isset($array[$i]))
		{
			return false; 
		}

	}

	return true; 

}

?>
