<?php 

include 'functions.php';



if (isset($_GET["search"]))
{
	$searchstring = htmlspecialchars($_GET["search"]);
}
else
{
	$searchstring = '';
}

if (isset($_GET["option"]))
{
	$option = htmlspecialchars($_GET["option"]);
}
else
{
	$option = 'all';
}

if (isset($_GET["nr"]))
{
	$nr = htmlspecialchars($_GET["nr"]);
}
else
{
	$nr = '';
}

if (isset($_GET["postnr"]))
{
	$postnr = htmlspecialchars($_GET["postnr"]);
}
else
{
	$postnr = '';
}

if (isset($_GET["orgnr"]))
{
	$orgnr = htmlspecialchars($_GET["orgnr"]);
}
else
{
	$orgnr = '';
}

if (isset($_GET["rows"]))
{
	$rows = htmlspecialchars($_GET["rows"]);
}
else
{
	$rows = 10;
}

if ($rows > 200)
{
	$rows = 200;
}

if (isset($_GET["start"]))
{
	$start = htmlspecialchars($_GET["start"]);
}
else
{
	$start = '0';
}


$end = '&rows=' . $rows . '&start=' . $start . '&wt=json';

//build search query
if ($option == 'all')
{

	$url = 'http://localhost:8983/solr/aksjeeier/select?q.op=AND&q=';
	if ($searchstring == '')
	{
		$searchstring = 'kjell inge røkke';
	}

	$search_array = explode(" ", $searchstring);
	$searchstring = $url;

	//FELTER
	$navn_eier = 'navn_eier:';
	$navn_eier = return_search_string($search_array, $navn_eier);

	$navn_selskap = 'selskap:';
	$navn_selskap = return_search_string($search_array, $navn_selskap);

	$navn_poststed = 'poststed:';
	$navn_poststed = return_search_string($search_array, $navn_poststed);

	$navn_postnr = 'postnr:';
	$navn_postnr = return_search_string($search_array, $navn_postnr);

	$navn_orgnr = 'orgnr:';
	$navn_orgnr = return_search_string($search_array, $navn_orgnr);	


	$searchstring = $searchstring . urlencode($navn_eier) . urlencode(' OR ') . urlencode($navn_selskap) . urlencode(' OR ') . urlencode($navn_poststed) . urlencode(' OR ') . urlencode($navn_orgnr) . $end;
}
else if ($option == 'company')
{

		$searchstring = 'http://localhost:8983/solr/aksjeeier/select?q.op=AND&q=orgnr:' . urlencode($orgnr); 
	
		$searchstring .= $end;

}
else if ($option == 'owner')
{
	
	if ($nr != '')
	{
		$searchstring = 'http://localhost:8983/solr/aksjeeier/select?q.op=AND&q=(navn_eier:"' . urlencode($searchstring) . '"' . urlencode(' AND fodselsaar_orgnr:' . $nr); 
		
		if (!empty($postnr))
		{
			$searchstring .= urlencode(' AND postnr:' . $postnr . ')');
		}
		else
		{
			$searchstring .= ')';
		}
		
		$searchstring .= $end;
						 
	}
	else
	{
		$searchstring = 'http://localhost:8983/solr/aksjeeier/select?q.op=AND&q=navn_eier:' . $nr . '"' . urlencode($searchstring) . '"' . $end . $nr;
	}
	
	
}





echo download($searchstring);


function return_search_string($array, $category)
{


	if (!is_array($array))
	{
		return false;
	}

	if (!$count = count($array))
	{
		
		
		return false;
	}

	if ($count > 25)
	{
		return false; 
	}

	$newstring = $array[0];

	for ($i = 1; $i < $count; $i++)
	{
		$newstring .= '+' . $array[$i];

	}

	return $category . $newstring;

}


?>
