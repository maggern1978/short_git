<!DOCTYPE html>
<html lang="no">
<head>
  <title>Søk i Aksjonærregisteret</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
</head>
<style type="text/css">
  * {
    outline: 0px solid blue;
  }

  .capitalize {
    text-transform: capitalize;
  }
  .minimumsbredde110 {
    min-width: 110px;
  }
  .minimumsbredde80 {
    min-width: 80px;
  }
  .minimumsbredde60 {
    min-width: 60px;
    padding-right: 20px;
  }    


  h1 { font-size: 2.0rem; }

  @media (min-width: 576px) {
    h1 { font-size: 2.5rem; }
  }
  @media (min-width: 768px) {
    h1 { font-size: 2.5rem; }
  }
  @media (min-width: 992px) {
    h1 { font-size: 2.5rem; }
  }
  @media (min-width: 1200px) {
    h1 { font-size: 2.5rem; }
  }

</style>
<body>
  <div class="container-lg">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
        <div class="d-flex flex-column justify-content-center">
          <div class="text-center"><h1>Søk i aksjonærregisteret</h1>
          </div>
          <div class="input-group mt-3 mb-3">
            <div class="input-group-prepend">
              <button id="year" type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown">
                År 2020
              </button>
              <div class="dropdown-menu">
               <?php 
               $start = 2020;

               for ($i = 0; $i < 6; $i++)
               {

                ?>
                <a class="dropdown-item" href="#">År <?php echo $start - $i;?></a>
                <?php 
              }
              ?>
            </div>
          </div>
          <input id="searchfield" type="text" class="form-control" placeholder="Søk etter navn, selskap, organisasjonsnummer, postnummer eller -sted">
          <div class="input-group-append">
            <button id="sendbutton" class="btn btn-success" type="submit">Søk</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-lg">
  <div class="row">
    <div id="result" class="col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
      <script>$("#result").hide();</script>
      <p id="numberofhits"></p>            
      <table class="table table-striped table-responsive-sm table-responsive-md table-responsive-lg  table-sm">
        <thead>
          <tr>
            <th>Aksjonær</th>
            <th class="text-right">Født/org.nr.</th>
            <th class="text-right" style="padding-left: 30px;">Postnr</th>
            <th class="" style="padding-right: 40px;">Poststed</th>
            <th>Selskap</th>
            <th class="text-right minimumsbredde80">Antall&nbspaksjer</th>
            <th class="text-right minimumsbredde80">Andel</th>
            <th class="text-right minimumsbredde80">År</th>
          </tr>
        </thead>
        <tbody id="resultbody">
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="container-lg">
  <div class="row">
    <div id="pagination" class="col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
      <script>$("#pagination").hide();</script>
  
  </div>
</div>


<script type="text/javascript">

  var trigger = 0;

  <?php

  $trigger_search = 0; 
  $search_string = 0;

  if (isset($_GET["search"]))
  {
    $search_string = htmlspecialchars($_GET["search"]);
    $trigger_search = 1; 
    echo 'var trigger = 1;';

  }

  if (isset($_GET["option"]))
  {
    $option = htmlspecialchars($_GET["option"]);
    echo 'var optionvalue = "' . $option . '"; ';

  }
  else
  {
    echo 'var optionvalue = "all";';
  }


  if (isset($_GET["year"]))
  {
    $search_year = htmlspecialchars($_GET["year"]);

  }
  else
  {
    $search_year = '2020';
  }

  if (isset($_GET["nr"]))
  {
    $nr = htmlspecialchars($_GET["nr"]);

  }
  else
  {
    $nr = '';
  }

  if (isset($_GET["postnr"]))
  {
    $postnr = htmlspecialchars($_GET["postnr"]);

  }
  else
  {
    $postnr = '';
  }

  if (isset($_GET["orgnr"]))
  {
    $organisasjonsnummer = htmlspecialchars($_GET["orgnr"]);
  }
  else
  {
    $organisasjonsnummer = '';
  }

  if (isset($_GET["start"]))
  {
    $start = htmlspecialchars($_GET["start"]);
  }
  else
  {
    $start = '0';
  }

  if (isset($_GET["rows"]))
  {
    $rows = htmlspecialchars($_GET["rows"]);
  }
  else
  {
    $rows = '25';
  }


  ?>


var year = "<?php echo $search_year; ?>"; //newest year, default
var born = "<?php echo $nr; ?>"; 
var postnummer = "<?php echo $postnr; ?>"; 
var organisasjonsnummer = "<?php echo $organisasjonsnummer; ?>"; 
var startnummer = "<?php echo $start; ?>"; 
var rader = "<?php echo $rows; ?>"; 



$(function(){

  $(".dropdown-menu > a").click(function(){
    $("#year").text($(this).text());
    $("#year").val($(this).text());

    year = $(this).text();
    year = year.substr(3, 7);  

  });

})


// Get the input field
var input = document.getElementById("searchfield");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) 
{
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) 
  {

    goToURL();
    // Trigger the button element with a click

  }
});


</script>
<script type="text/javascript">

  $("#sendbutton").click(function(event)
  {
   goToURL();

 });

  function goToURL() 
  {

    var value = $("#searchfield").val();
    location.href = 'index.php?search=' + value;

  }


  function dosearch() 
  {
        // Stop form from submitting normally

        var value = $("#searchfield").val();

        console.log(value);
        
        // Send the form data using post
        $.get("search_receive.php", {search:value, option:optionvalue, time:year, nr:born, postnr:postnummer,orgnr:organisasjonsnummer,start:startnummer,rows:rader }, function(data){
            // Display the returned data in browser


            var jsonresult = JSON.parse(data);

            console.log(jsonresult);

            if (jsonresult.response.numFound == 0 || jsonresult.response.numFound === 'undefined')
            {

              $('#result').html('<div class="text-center">Ingen treff for "' + value + '"</div>');
              $("#result").show();  
              return;
            }
            else
            {
              
              var numberofrows = parseInt(jsonresult.responseHeader.params.rows);
              var startnumber = parseInt(jsonresult.responseHeader.params.start);
              var numFound = jsonresult.response.numFound;
              var end = startnumber + numberofrows;

              if (end > numFound)
              {
                end = numFound;
              }


              var qtime = jsonresult.responseHeader.QTime;

              var html = '<div class="d-flex justify-content-between">';
              html += '<div>';
              html += 'Søkeresultat: ' + number_format(numFound,0,",",".") + ' treff';
              html += '</div>';
              console.log(jsonresult.responseHeader);
              html += '<div>Viser ' +  number_format (startnumber, 0, ",", ".");
              html += ' til ' + number_format (end, 0, ",", ".");
              html += '</div>';
              
              $('#numberofhits').html(html);

            }


            var jsonresult = jsonresult.response.docs;

            jsonresult.sort(function(a,b)
            {
             return a.navn_eier[0] > b.navn_eier[0] ? 1 : -1;
            });


            var jsonlength = jsonresult.length;
            var tbody = $('#resultbody');

            console.log(jsonresult);

            
            $('#resultbody').html('');

            for (i = 0; i < jsonlength; i++)
            {
              var celler = '';

              var celle = '<td class="capitalize">';


              celle += '<a href="index.php?search=';
              celle +=  jsonresult[i].navn_eier[0].toLowerCase() + '&option=owner&time=' + year;
              celle += '&nr=';

              if (typeof jsonresult[i].fodselsaar_orgnr !== 'undefined')
              {
                celle += '&nr=' +jsonresult[i].fodselsaar_orgnr;
              }

              if (typeof jsonresult[i].postnr  !== 'undefined')
              {
                celle += '&postnr=' + jsonresult[i].postnr;;

              }

              celle += '">';

              jsonresult[i].navn_eier[0] = jsonresult[i].navn_eier[0].replace(" ", "&nbsp");

              celle += jsonresult[i].navn_eier[0].toLowerCase();

              celle += '</a>';
              celle += '</td>';

              celler = celler + celle;


              if (typeof jsonresult[i].fodselsaar_orgnr !== 'undefined' && jsonresult[i].fodselsaar_orgnr !== '')
              {
                var celle = '<td class="text-right">' + jsonresult[i].fodselsaar_orgnr + '</td>';
                celler = celler + celle;
              }
              else
              {
                var celle = '<td class="text-right">-</td>';
                celler = celler + celle;
              }


              if (typeof jsonresult[i].postnr !== 'undefined' && jsonresult[i].postnr !== '')
              {
                var celle = '<td class="text-right">' + jsonresult[i].postnr + '</td>';
                celler = celler + celle;
              }
              else
              {
                var celle = '<td class="text-right">-</td>';
                celler = celler + celle;
              }

              if(typeof jsonresult[i].poststed !== 'undefined')
              {
                var celle = '<td class="capitalize">' + jsonresult[i].poststed[0].toLowerCase() + '</td>';
                celler = celler + celle;
              }
              else
              {
                var celle = '<td>-</td>';
                celler = celler + celle;
              }

              var selskaper = jsonresult[i].selskap[0].toLowerCase();
              var n = selskaper.lastIndexOf(" as");

              if (n == selskaper.length - 3)
              {

                var last = selskaper.substr(n, selskaper.length);
                last = last.toUpperCase();
                var first = selskaper.substr(0, n);
                var selskaper = first + last;

              }

              var celle = '<td class="capitalize" data-toggle="tooltip" title="Org.nr.: ' + jsonresult[i].orgnr + '">';   
              celle += '<a href="index.php?search=';
              celle +=  jsonresult[i].selskap[0] + '&option=company&time=' + year;
              celle += '&orgnr=' + jsonresult[i].orgnr ;          
              celle += '">';
              
              celle += selskaper;
              celle += '</a>' + '</td>';; 

              celler = celler + celle;

              //number_format (number, decimals, dec_point, thousands_sep) 

              var celle = '<td class="text-right" data-toggle="tooltip" title="Totalt i selskapet: ' + number_format(jsonresult[i].antall_aksjer_selskap,0,",",".") + ' aksjer" >' + number_format(jsonresult[i].antall_aksjer,0,",",".") + '</td>';
              celler = celler + celle;

              var andel = (jsonresult[i].antall_aksjer/jsonresult[i].antall_aksjer_selskap)*100;
              andel_to_desimaler = roundToTwo(andel);

              var celle = '<td class="text-right" data-toggle="tooltip" title="' + number_format(roundToSix(andel),6,",",".")  + ' %">' + number_format(andel_to_desimaler,2,",",".") + '%</td>';
              celler = celler + celle;

              var celle = '<td class="text-right">' + year + '</td>';
              celler = celler + celle;

              var row = $('<tr>').addClass('bar').html(celler);

              $('#resultbody').append(row);
            } 

            $("#result").show(); 
            $('[data-toggle="tooltip"]').tooltip();             


            //build navigation

            nav = '<div class="d-flex flex-column flex-sm-column flex-md-row justify-content-between">';
            nav += '<div>';
           
            
            var pagination_big = '';

             var totalPages = Math.ceil(numFound/numberofrows); //total pages
             var currentPage = Math.floor(startnumber/numberofrows)+1; //this is shouldn't be static, since it's the current page the user is at

             console.log('current page is ' + currentPage );

             console.log(getPageList(totalPages, currentPage, 7));

             var page_list = getPageList(totalPages, currentPage, 7);

             /**
             * If the user is on page 1 to 5, the first page will be 1. If he's not, then the page will be the current page minus 5.
             * This is to stop it from starting the loop from a negative number, since you don't have a -4 page.
             */
         
             //collect all values 

             var pagination_url = 'index.php?search=' + value + '&option=' + optionvalue + '&time=' + year + '&nr=' + born + '&postnr=' + postnummer + '&orgnr=' + organisasjonsnummer + '&rows=' + rader;

             if (currentPage != 1)
             {

             // I changed the for to a while, since the i parameter had to get out of the for. You can still use a for and give the i as a parameter. Whatever.
             var pagination_url_local = pagination_url  + '&start=' + (currentPage - 2)* numberofrows;
             pagination_big += ' <li class="page-item"><a class="page-link" href="' + pagination_url_local +'" aria-label="Forrige"><span aria-hidden="true">&laquo;</span><span class="sr-only">Forrige</span></a></li>';

             }

             for (i = 0; i < page_list.length; i++)
             {
                   
               if (currentPage  == page_list[i] )
               {
                pagination_big += '<li class="page-item active"><a class="page-link" href="#">' + page_list[i] +'</a></li>';
               }
               else if (page_list[i] == 0 )
               {
                //nav += '<li class="page-item"><a class="page-link" href="#">...</a></li>';
               }               
               else
               {
              
                var pagination_url_local = pagination_url  + '&start=' + (page_list[i] - 1)* numberofrows;

                pagination_big += '<li class="page-item"><a href="' + pagination_url_local + '" class="page-link" >' + page_list[i] + '</a></li>';
               }
        
             }

             if (currentPage != totalPages)
             {

              if (totalPages > 10)
              {
                var pagination_url_local = pagination_url  + '&start=' + (totalPages-1) * numberofrows;
                pagination_big += ' <li class="page-item"><a class="page-link" href="' + pagination_url_local +'" aria-label="Siste"><span aria-hidden="true">Siste</span><span class="sr-only">Siste</span></a></li>';                
              }

             var pagination_url_local = pagination_url  + '&start=' + currentPage * numberofrows;
             pagination_big += ' <li class="page-item"><a class="page-link" href="' + pagination_url_local +'" aria-label="Neste"><span aria-hidden="true">&raquo</span><span class="sr-only">Neste</span></a></li>';

             }
             nav += '<div class=" d-none d-sm-block">';
             nav += '<ul class="pagination">';
             nav += pagination_big;
             nav += '</ul>';
             nav += '</div>';

             nav += '<div class=" d-block d-sm-none">';
             nav += '<ul class="pagination pagination-sm ">';
             nav += pagination_big;
             nav += '</ul>';
             nav += '</div>';

             nav += '</div>';
             nav += '<div class="my-auto">';
             nav += '<span class="font-weight-bold">Antall sider:</span> ' + number_format (totalPages, 0, ",", ".")   + '. Søket tok ' + number_format (qtime, 0, ",", ".")  + ' ms';
             nav += '</div>';
             nav += '</div>';

              $('#pagination').html(nav);
              $("#pagination").show(); 

          });

};



$(document).ready(function()
{

  if (trigger == 1)
  {
    console.log(trigger);



    $("#searchfield").val("<?php
      
      if (empty($organisasjonsnummer))
      {
        echo $search_string;
      }
      else
      {
        echo $organisasjonsnummer;
      }
      
      
      ?>");

    dosearch();

  }



});

function roundToTwo(num) {    

  return (Math.round(num * 100) / 100).toFixed(2);

}

function roundToSix(num) {    

  return (Math.round(num * 1000000) / 1000000).toFixed(6);

}

function number_format (number, decimals, dec_point, thousands_sep) 
{
      // Strip all characters but numerical ones.
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec);
        return '' + Math.round(n * k) / k;
      };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    }

  </script>

<script type="text/javascript">



function getPageList(totalPages, page, maxLength) 
{
    if (maxLength < 5) throw "maxLength must be at least 5";

    function range(start, end) {
        return Array.from(Array(end - start + 1), (_, i) => i + start); 
    }

    var sideWidth = 0;
    var leftWidth = (maxLength - sideWidth*2 - 3) >> 1;
    var rightWidth = (maxLength - sideWidth*2 - 2) >> 1;
    if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength - sideWidth - 0)
            .concat(0, range(totalPages - sideWidth + 1, totalPages));
    }
    if (page >= totalPages - sideWidth - 0 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
            .concat(0, range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }



    return range(page - leftWidth - 1, page + rightWidth + 1);
}



</script>


</body>
</html>
