<?php

include_once '../production_europe/functions.php';
include_once '../production_europe/logger.php';

$destination_folder = '../production_europe/json/infront_export/';
$source_folder = '../shorteurope-com.luksus.no/dataraw/';

$countries = get_countries();

foreach ($countries as $land) //copy from source files
{
	
	$filename = $source_folder . $land . '/' . $land . '_current.json';

	if (file_exists($filename))
	{
		if (copy($filename, $destination_folder . '/' . $land . '_current.json'))
		{
			echo 'Copy success for ' . $filename . '<br>';
		}
		else
		{
			logger('Feil kopiering i infront_exporter. ', 'Land er ' . $land . '. Fil er ' . $filename);
		}
	}
	else
	{
		logger('Feil kopiering i infront_exporter. ', 'Land er ' . $land . '. Fil er ' . $filename);
	}
}




?>