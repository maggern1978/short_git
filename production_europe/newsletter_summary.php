<?php


if (!$data = readJSON('../production_europe/json/events/company/' . $land . '.events.company.current.json'))
{
	//echo 'Error reading json, returning.';
	return;
}

if (empty($data))
{
	return;
}

$changescount = 0;
$companyNameBox = [];
$playerNameBox = [];
$newestDate = '2000-01-01';
$numberOfChanges = 0;

foreach ($data as $key => $change)
{
	
	if ($change['change'] != 0)
	{
		$numberOfChanges++;
	}

	if ($newestDate < $change['LastChange'])
	{
		$newestDate = $change['LastChange'];
	}

	if ($change['change'] == 0)
	{
		unset($data[$key]);
	}

}

$publishedate = date( "Y-m-d", strtotime("+1 Weekday", strtotime($newestDate)));
if ($publishedate > date('Y-m-d')) //never newer than today
{
	$publishedate = date('Y-m-d');
}


if ($round % 2 == 1)
{
	$background_color = ' background-color: #e9f8ff;';
	$padding = 'padding: 8px; padding-top: 3px;  padding-bottom: 3px;';
}
else
{
	$background_color = '';
	$padding = 'padding: 0px;';
}

if ($land == 'united_kingdom')
{
	$land_navn = 'United Kingdom';
}
else
{
	$land_navn = ucwords($land);
}

?>

<div class="" style=" <?php echo $padding; ?>; <?php echo $background_color; ?>">
	<div class="col-12" style=" <?php echo $background_color; ?>">
		<a style="color: black;  text-decoration: none; " href="https://
		<?php 

		if ($mode == 'shortnordic')
		{
			echo $mode . '.com/index_' . $land . '.php';
		}
		else
		{
			echo $mode . '.com/' . $land . '.php';
		}

		?>

		"><h3 style="margin-top: 5px; margin-bottom: 5px; font-size: 1.5rem; font-family: Helvetica, arial, sans-serif;"><?php echo $land_navn; ?> </h3></a>
	</div>



	<?php

	if ($newestDate >= date( "Y-m-d", strtotime("-1 Weekday")))
	{

	}
	else
	{
		?>
		<div class="col-12" style="font-family: Helvetica, arial, sans-serif;">
			<p style="margin-top: 5px; margin-bottom: 5px;">
				<?php

				echo '(Positions published ';
				echo strtolower(date( "l", strtotime(($publishedate)))) . ' ' . date( "Y-m-d", strtotime(($publishedate)));
				echo ')';
				?>	

			</p>
		</div>
<?php }	?>
