<?php

require '../production_europe/functions.php';
require '../production_europe/logger.php';

$mode = '';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	if (isset($_GET["mode"]))
	{
		$mode = test_input($_GET["mode"]);
	}
	else
	{
		 $mode = 'shorteurope';
	}
}
else
{
    $mode = 'shorteurope';
}


date_default_timezone_set('Europe/Oslo');

include '../production_europe/newsletter_header.html';

if ($mode == 'shortnordic')
{
	$countries = ['sweden', 'norway', 'denmark', 'finland'];
}
else if($mode == 'shorteurope')
{
	$countries = get_countries();
}
else
{
	return;
}


?>
<div class="col-12" >
<p style="color:grey">Newsletter from <a href="https://<?php echo $mode .'.com'; ?>">
<?php

	if ($mode == 'shortnordic')
	{
		echo 'ShortNordic.com';
	}
	else
	{
		echo 'ShortEurope.com';
	}

?>


</a></p>
</div>
<div class="col-12">
<h3>Changes to short positions</h3>
<p>Generated <?php echo date('l', strtotime("now")) . ' ' . date('Y-m-d') . ' at ' . date('H:i'); ?>.</p>

<!--<p>Pulished <?php echo mb_strtolower(date('l', strtotime("now"))) . ' ' .  date('Y-m-d'); ?>.</p>-->

</div>

<?php

foreach ($countries as $key => $land)
{
	?>

	<?php
	include '../production_europe/newsletter_summary.php';
	include '../production_europe/newsletter_company.php';
}

include '../production_europe/newsletter_footer.html';

?>