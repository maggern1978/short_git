<?php 

//$land = 'sweden';
//$land = 'united_kingdom';

include '../production_europe/options_set_currency_by_land.php';
include '../production_europe/functions.php';

$playerliste = '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
$saveUrlWinner = '../shorteurope-com.luksus.no/datacalc/toplist/' . $land . '/winner/';
$saveUrlLoser = '../shorteurope-com.luksus.no/datacalc/toplist/' . $land . '/loser/';

$date = date('Y-m-d');


if (!$json = readJSON($playerliste))
{
	errorecho('Error reading playerpositions<br>');
	return;
}

$keepContainer = [];

foreach ($json as $key => $player)
{
	$object = new stdClass;
	$object->playerName =  $player['playerName'];
	$object->numberOfPositions =  count($player['positions']);
	$object->basecurrency =  $player['positions'][0]['basecurrency'];

	$change = 0;

	foreach ($player['positions'] as $position)
	{
		$change += $position['positionValueChange'] * $position['base_currency_multiply_factor'];
	}

	$object->change = number_format(round($change,2),2);
	$keepContainer[] = (array)$object;
}

//split in two

$losers = [];
$winners = [];

foreach ($keepContainer as $entry)
{

	if ($entry['change'] > 0)
	{
		$winners[] = $entry;
	}
	else if ($entry['change'] < 0)
	{
		$losers[] = $entry;
	}	

}

usort($winners,function($first,$second){
    return $first['change'] < $second['change'];
});

usort($losers,function($first,$second){
    return $first['change'] > $second['change'];
});


//merge header array 
//var_dump($file_export);  

$url = $saveUrlWinner . 'winner.' . $land . '.current' . '.json';

$fp = fopen($url, 'w');
fwrite($fp, json_encode($winners, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
fclose($fp);

$url = $saveUrlLoser . 'loser.' . $land . '.current' . '.json';
$fp = fopen($url, 'w');
fwrite($fp, json_encode($losers, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
fclose($fp);

  ?>
