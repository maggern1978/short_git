<?php
echo '<br>';

include '../production_europe/functions.php';

$filePath = '../shorteurope-com.luksus.no/dataraw/sweden.history/historypositions.ods';
$savePath = '../shorteurope-com.luksus.no/dataraw/sweden.history/sweden.history_current.csv';

require_once '../vendor/box/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

$reader = ReaderEntityFactory::createReaderFromFile($filePath);

$positionsBox = [];

$reader->open($filePath);

foreach ($reader->getSheetIterator() as $key => $sheet) {
    foreach ($sheet->getRowIterator() as $index => $row) {
        
    	if ($index < 6)
    	{
    		continue;
    	}

    	$temparray = [];

        // do stuff with the row
        $cells = $row->getCells();

        $key = key($cells[0]);
        $temparray[] = $cells[0]->$key;
        //echo $cells[0]->$key;
        //echo ',';

        $key = key($cells[1]);
        $temparray[] = $cells[1]->$key;
        //echo $cells[1]->$key;
        //echo ',';

        $key = key($cells[2]);
        $temparray[] = $cells[2]->$key;
        //echo $cells[2]->$key;
        //echo ',';

        $key = key($cells[3]);
        $percent = $cells[3]->$key;
        $percent = str_replace(',', '.', $percent);

        $temparray[] = $percent;
        //echo $cells[3]->$key;
        //echo ',';

        $key = key($cells[4]);


        if ($cells[4]->$key instanceof DateTime) {
          
          $newDate = (string)$cells[4]->$key->format('Y-m-d');
         
        }
        else
        {
           $newDate = $cells[4]->$key;
        }


        $temparray[] = $newDate;
        //echo $cells[4]->$key;
      
        if (isset($cells[5]))
        {
	        $key = key($cells[5]);
	        $temparray[] = $cells[5]->$key;
    	    //echo $cells[5]->$key;
        	//echo '<br>';
        }
        else
        {
        	//echo '<br>';
        }

        $positionsBox[] = $temparray;
           
    }
}

$reader->close();
saveCSVx($positionsBox, $savePath);

?>