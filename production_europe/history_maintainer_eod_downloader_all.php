<?php
//this files is for maintaince and must be startet manually

if (!function_exists('reformatcsv')) 
{
	function reformatcsv($data, $ticker)
	{

		$count = count($data);
		$allholder = [];

	//var_dump($data);

		for ($i = 2; $i < $count; $i++)
		{

			if(!isset($data[$i-1][4]) or !isset($data[$i][6]))
			{
				continue;
			}

			$object = new stdClass;
			$object->previousClose = (float)$data[$i-1][4];
			$object->ticker = $ticker;
			$object->volume = (int)$data[$i][6];
			$object->close = $data[$i][4];
			$object->change = (float)$object->close - (float)$object->previousClose;
			$object->change = round($object->change,5);

			if ($object->close == 0 or $object->close == 'null' or $object->close == null or $object->previousClose == 0 or $object->previousClose == 'null' )
			{
			//var_dump($object);
			//echo '|';
				continue;
			}

			$allholder[][$data[$i][0]] = (array)$object;
		}

		$count = count($allholder);


		if ($count > 2)
		{
			return array_reverse($allholder);
		}
		else
		{
			return false;
		}

	}	
	
}


if (!function_exists('reformatdata')) 
{

	function reformatdata($data, $ticker)
	{

		$count = count($data);

		$allholder = [];

		for ($i = 0; $i < $count-1; $i++)
		{

			$object = new stdClass;
			$object->previousClose = (float)$data[$i+1]['close'];
			$object->ticker = $ticker;
			$object->volume = (int)$data[$i]['volume'];
			$object->close = $data[$i]['close'];
			$object->change = $data[$i]['close'] - $data[$i+1]['close'];
			$object->change = round($object->change,5);

			$allholder[][$data[$i]['date']] = (array)$object;
		}
		return $allholder;
	}
}


echo '<br>';
set_time_limit(19000);

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('Europe/Oslo');

include('../production_europe/logger.php');
require '../production_europe/functions.php';


$isinlist = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv');
$isincount = count($isinlist);

echo 'Getting ' . $isincount . ' number of stocks <br>';


$now = time();
//$two_weeks_ago = strtotime('-3 weeks');
$two_weeks_ago = 0;

//https://query1.finance.yahoo.com/v7/finance/download/IBM?period1=0&period2=1595932959&interval=1d&events=history&crumb=jIIyoewBT2o
//$urlstart = 'https://query1.finance.yahoo.com/v7/finance/download/';
//$urlend = '&interval=1d&events=history&crumb=jIIyoewBT2o';

$urlstart = 'https://eodhistoricaldata.com/api/eod/';
$urlend = '?from=2010-01-05&api_token=5da59038dd4f81.70264028&period=d&fmt=json';

//https://eodhistoricaldata.com/api/eod/CARL-B.CO?from=2000-01-05&api_token=5da59038dd4f81.70264028&period=d&fmt=json

$skipscounter = 0;

for ($i = 0; $i < $isincount; $i++) {

	if (isset($isinlist[$i][1]) and isset($isinlist[$i][8])) {
		$ticker = $isinlist[$i][1] . '.' . $isinlist[$i][8];
		$ticker = str_replace(' ','-',$ticker);
		$tickerending = substr($ticker,-2,2);
		$folder = '../production_europe/history/';

		//echo 'Tickerending is ' . $tickerending . '<bR>';
		//&outputsize=full&apikey=demo

		flush_start();

        $filename = $ticker . '.json';
        $filename = strtolower($filename);
    	
        $filenameurl = $folder . $filename;
       
		if (file_exists($filenameurl))
		{

			$filetime_file_seconds = filemtime($filenameurl);
			$filetime_now_seconds = time();

			$time_difference_seconds = $filetime_now_seconds  - $filetime_file_seconds;
			$time_difference_hours = $time_difference_seconds/3600; 

			if ($time_difference_hours < 22)
			{
        		//echo $i . '. ' . $filenameurl . ' already updated within 22 hours, skipping<br>';
				$skipscounter++;
				continue; 
			}

		}
      
		$url = $urlstart . $ticker . $urlend;
        

		if ($data = json_decode(download($url), true))
		{
			$data = array_reverse($data);
			$data = reformatdata($data, $ticker);
			
			//echo 'Saving ' . $folder . $filename . '<br>';
			echo $i . '. Saving ' . $url .' ' . date('Y-m-d H:i:s') . '<br>';

			saveJSONsilent($data, $folder . $filename);
			//file_put_contents('../production_europe/history/eod_download_json/'. $filename , $data);
		}
		else 
		{
			errorecho ('Ticker download error from EOD: ' . $ticker . '<br>'); 
			echo 'Doing yahoo download';

			$urlstartalt = 'https://query1.finance.yahoo.com/v7/finance/download/';
			$urlendalt = '&interval=1d&events=history&crumb=jIIyoewBT2o';

			$url = $urlstartalt . $ticker . '?period1=' . $two_weeks_ago . '&period2=' . time() . $urlendalt;
	        echo $url .' ' . date('Y-m-d H:i:s') . '<br>';

			if (!$data = download($url))
			{
				errorecho ('Ticker download error from Yahoo: ' . $ticker . '<br>'); 
				include '../production_europe/flush_end.php';
				continue;
			}
			
			$data = array_map('str_getcsv', explode("\n", $data));
			if ($data = reformatcsv($data, $ticker))
			{
				//successecho ('Saving ' . $folder . $filename . '<br><br>');
				saveJSON($data, strtolower($folder . $filename));
			}
			else
			{
				echo '<strong>To few values, skipping save</strong><br>';
			}

		}

		flush_end();
	}
	else
	{
		errorecho($i . '. Error!<br>');
	}

}



?>