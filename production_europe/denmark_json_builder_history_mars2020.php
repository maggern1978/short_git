<?php
date_default_timezone_set('Europe/Oslo');

//obs! this files needs include('../production_europe/simplehtmldom_1_7/simple_html_dom.php'); to run alone!
echo '<br>Starting...<br>';
include('../production_europe/simplehtmldom_1_7/simple_html_dom.php');
include('../production_europe/functions.php');

set_time_limit(320);

//sorteringsfunksjon
if (!function_exists('comp')) {
    function comp($a, $b)
    {
        if ($a->ShortingDate == $b->ShortingDate) {
            return 0;
        }
        return ($a->ShortingDate > $b->ShortingDate) ? -1 : 1;
    }
}

if (!function_exists('datetotime')) {
    function datetotime ($date, $format = 'DD.MM.YYYY') {
        if ($format == 'YYYY-MM-DD') list($year, $month, $day) = explode('-', $date);
        if ($format == 'YYYY/MM/DD') list($year, $month, $day) = explode('/', $date);
        if ($format == 'YYYY.MM.DD') list($year, $month, $day) = explode('.', $date);

        if ($format == 'DD-MM-YYYY') list($day, $month, $year) = explode('-', $date);
        if ($format == 'DD/MM/YYYY') list($day, $month, $year) = explode('/', $date);
        if ($format == 'DD.MM.YYYY') list($day, $month, $year) = explode('.', $date);

        if ($format == 'MM-DD-YYYY') list($month, $day, $year) = explode('-', $date);
        if ($format == 'MM/DD/YYYY') list($month, $day, $year) = explode('/', $date);
        if ($format == 'MM.DD.YYYY') list($month, $day, $year) = explode('.', $date);

        return mktime(0, 0, 0, $month, $day, $year);
    }
}


include('../production_europe/logger.php');

set_time_limit(5000);

if (!function_exists('readCSV_tickerliste')) {
 //obs, dataene må ha semikolon som skilletegn
    function readCSV_tickerliste($file_location_and_name) {

        $csvFile = file($file_location_and_name);

        //Les in dataene 
        $name = [];
        foreach ($csvFile as $line) {
            $name[] = str_getcsv($line, ',');
        }

        //ordne utf koding
        $counter = 0;
        foreach ($name as &$entries) {
            $data[$counter] = array_map("utf8_encode", $entries);
            $counter++;
        }
        return $name;
    }
}

if (!function_exists('readCSVsemikolon')) {
 //obs, dataene må ha semikolon som skilletegn
    function readCSVsemikolon($file_location_and_name) {

        $csvFile = file($file_location_and_name);

        //Les in dataene 
        $name = [];
        foreach ($csvFile as $line) {
            $name[] = str_getcsv($line, ';');
        }

        //ordne utf koding
        $counter = 0;
        foreach ($name as &$entries) {
            $data[$counter] = array_map("utf8_encode", $entries);
            $counter++;
        }
        return $name;
    }
}

//Lagre
if (!function_exists('saveCSV')) {
    function saveCSV($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');
          fputcsv($fp, $input);
    fclose($fp);
    }
}

$path = '../shorteurope-com.luksus.no/dataraw/denmark/download_history/html/';
$files = array_diff(scandir($path), array('.', '..'));

//sort by time downloaded
usort($files, function($a, $b) {
    return filemtime('../shorteurope-com.luksus.no/dataraw/denmark/download_history/html/' . $a) > filemtime('../shorteurope-com.luksus.no/dataraw/denmark/download_history/html/' . $b);
});

//var_dump($files);

$positionsBox = [];
foreach ($files as $key => $file) 
{
    $arr = [];
    //echo 'File is ' . $path . $file . '<br>';
    $html = file_get_html($path . $file);

    if (empty($html))
    {
        errorecho($key . '. ' . $path . $file . 'File is empty! Continuing...<br>');
        continue;

    }

    foreach($html->find('table tr td') as $e){
        //echo $e->plaintext . '<br>';
        $arr[] = trim($e->plaintext);
    }

    //var_dump($arr);

    $count = count($arr);
    //echo 'Count er :' . $count . '<br>';

    for ($i = 3; $i < $arr; $i = $i + 7) {

        if (!isset($arr[$i])) {
            break;
        }

        if (strpos($arr[$i+2],"CANCELLED")) {
            //echo 'CANCELLED';
            continue;
        }
        else if (strpos($arr[$i+2],"ANNULLERET")) {
            //echo 'CANCELLED';
            continue;
        }
       
        $position=new stdClass();
        $position->ShortingDate = $arr[$i+0];
        $position->description = $arr[$i+2];
        $position->Name = $arr[$i+3];
        $positionsBox[] = $position;
        //var_dump($position);
        //echo '<br><br>';
    }
}



$count = count($positionsBox);
echo 'Antall posisjoner registrert: ' . $count . '<br>';
$activeCounter = 0;

//var_dump($positionsBox);

for ($i = 0; $i < $count; $i++) {

    //snu datoen og legg til to sifre til år
    $dateArray = explode('.', $positionsBox[$i]->ShortingDate);
    $newdate = '20'. $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
    
    //trekk fra en arbeidsdag
    $newdate = date('Y-m-d', strtotime($newdate . '-1 Weekday'));
    
    $positionsBox[$i]->ShortingDate = $newdate;

     if (strpos($positionsBox[$i]->description,"%")) {
        $positionsBox[$i]->type = 'percent';
    }
    else if (strpos($positionsBox[$i]->description,"reduced") or 
        strpos($positionsBox[$i]->description,"nedbragt")) {
        $positionsBox[$i]->type = 'shares';
    }
    else {
        $positionsBox[$i]->type = 'unknown';
    }
    
    $positionsBox[$i]->Name = strstr($positionsBox[$i]->Name," A/S",true);

    if ($positionsBox[$i]->Name == false) {
        //alternative name finding 
        echo '<br> Name not found by finding A/S, doing alternative method: '  . $positionsBox[$i]->description . '<br>';

        $namex = strstr($positionsBox[$i]->description,'issued by '); 
        $namex = str_replace('issued by ', '', $namex);
        echo 'New name found is ' . $namex . '<br>';
        $positionsBox[$i]->Name = $namex;

    }

    if (strpos($positionsBox[$i]->description,"Historic")) {
      $positionsBox[$i]->isActive = 'no';
    }
    else {
         $positionsBox[$i]->isActive = 'yes';
         $activeCounter++;
    }

    //remove historic
    $positionsBox[$i]->description = str_replace('(Historic) ', '', $positionsBox[$i]->description);


    if ($positionsBox[$i]->type == 'shares') {
        $positionsBox[$i]->PositionHolder = strstr($positionsBox[$i]->description," has", true);

        if ($positionsBox[$i]->PositionHolder == false) {
            $positionsBox[$i]->PositionHolder =  strstr($positionsBox[$i]->description," har nedbragt", true);
        }

        $descriptionString = $positionsBox[$i]->description;

        $descriptionString = str_replace($positionsBox[$i]->Name, "", $descriptionString);
        $descriptionString = str_replace($positionsBox[$i]->PositionHolder, "", $descriptionString);

        $positionsBox[$i]->NetShortPosition = preg_replace('/[^0-9]+/', '', $descriptionString); 
        $positionsBox[$i]->ShortPercent = ''; 
    }
    else if ($positionsBox[$i]->type == 'percent') { 
        $positionsBox[$i]->PositionHolder =  strstr($positionsBox[$i]->description," holds", true);

        if ($positionsBox[$i]->PositionHolder == false) {
            $positionsBox[$i]->PositionHolder =  strstr($positionsBox[$i]->description," har en", true);
        }

        $descriptionString = $positionsBox[$i]->description;

        $descriptionString = str_replace($positionsBox[$i]->Name, "", $descriptionString);
        $descriptionString = str_replace($positionsBox[$i]->PositionHolder, "", $descriptionString);

        $positionsBox[$i]->ShortPercent = preg_replace('/[^0-9]+/', '', $descriptionString); 
        $positionsBox[$i]->ShortPercent = $positionsBox[$i]->ShortPercent/100;
        $positionsBox[$i]->NetShortPosition = ''; 
    }
    else {
        $positionsBox[$i]->NetShortPosition = 'error'; 
        $positionsBox[$i]->ShortPercent = 'error'; 
    }

}

//Group positions on companies

echo 'Number of active positions: ' . $activeCounter . '<br>';


$allCompanyNames = []; 

$count = count($positionsBox);
echo 'Number of positions: ' . $count . '<br>';

if  ($count == 0) {
    echo 'Count is zero, this is an error!';
    logger('Number of companies in denmark is zero', ' in denmark_json_builder_history.php. Returning!');
    return;
}


for ($i = 0; $i < $count; $i++) {
        
    //fiks selskaper som ikke har blittmed
    if ($positionsBox[$i]->Name =='') {
        //echo '2!';
        if (strpos($positionsBox[$i]->description,"Østasiatiske")) {
            $positionsBox[$i]->Name = 'Santa Fe Group';
        }
        else if (strpos($positionsBox[$i]->description,"Landbobank")) {
            $positionsBox[$i]->Name = 'Ringkjøbing Landbobank, Aktieselskab';   
        }
    }
    $allCompanyNames[] = $positionsBox[$i]->Name;
   
}

$allCompanyNames = array_unique($allCompanyNames);
//var_dump($allCompanyNames);

$selskapsBox = [];

foreach ($allCompanyNames as $name) {
    $company = new stdClass();
    $company->ISIN = '';
    $company->Name = $name;
    $company->ShortPercent = '';
    $company->ShortedSum = '';
    $company->numberOfStocks = '';
    $company->LastChange = '';
    $company->ticker = '';
        
    //samle posisjonene med samme navn
    $holder = [];
        for ($i = 0; $i < $count; $i++) {
            if (isset($positionsBox[$i]->Name) and $positionsBox[$i]->Name == $name) {
                $holder[] = $positionsBox[$i];
                unset($positionsBox[$i]);
            }
        }
     $company->NumPositions = count($holder);   
    $company->Positions = $holder;
    $selskapsBox[] = $company;
}

//var_dump($selskapsBox);

$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );


$isinlisteCount = count($isin_list);
$selskapsBoxCount = count($selskapsBox);
$missBox = [];

//første runde for å finne de som mangler

echo '<br><br>Searching in isin list:<br>';

for ($i = 0; $i < $selskapsBoxCount; $i++) {

    //ta navnet og søk i isinlisten
    $nameTarget = $selskapsBox[$i]->Name;

    //fiks navnene manuelt

    if ($nameTarget == 'Solar' ) {
        $selskapsBox[$i]->Name = 'Solar B'; // annet navn
        $nameTarget = 'Solar B';
    }
    else if ($nameTarget == 'NKT Holding' ) {
        $selskapsBox[$i]->Name = 'NKT'; // annet navn
        
    }
    else if ($nameTarget == 'TDC' ) {
        unset ($selskapsBox[$i]); // konkurs
        continue;
    }
    else if ($nameTarget == 'Novozymes' ) {
        $selskapsBox[$i]->Name = 'Novozymes B'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0060336014'; //sett isin
       
    }
    else if ($nameTarget == 'H. Lundbeck' ) {
        $selskapsBox[$i]->Name = 'Lundbeck'; // annet navn
        $nameTarget = 'Lundbeck';
    }
    else if ($nameTarget == 'William Demant Holding' ) {
        $selskapsBox[$i]->Name = 'Demant'; // annet navn
        $nameTarget = 'Demant';
    }
    else if ($nameTarget == 'H. Lundbeck' ) {
        $selskapsBox[$i]->Name = 'Lundbeck'; // annet navn
        $nameTarget = 'Lundbeck';
    }
    else if ($nameTarget == 'Harboes Bryggeri' ) { //posisjon annulert, hopp over
        continue;
    }
    else if ($nameTarget == 'Dampskibsselskabet Norden' ) {
        $selskapsBox[$i]->Name = 'D/S Norden'; // annet navn
        $nameTarget = 'D/S Norden';
    }
    else if ($nameTarget == 'Coloplast' ) {
        $selskapsBox[$i]->Name = 'Coloplast B'; // annet navn
        $nameTarget = 'Coloplast B';
    }
    else if ($nameTarget == 'OW Bunker' ) {
        continue;
    }
    else if ($nameTarget == 'Carlsberg' ) {
        $selskapsBox[$i]->Name = 'Carlsberg B'; // annet navn
        $nameTarget = 'Carlsberg B';
    }
    else if ($nameTarget == 'Rockwool International' ) {
        $selskapsBox[$i]->Name = 'Rockwool Int. B'; // annet navn
        $nameTarget = 'Rockwool Int. B';
    }
    else if ($nameTarget == 'Auriga Industries' ) {
        continue; // ikke notert
    }
    else if ($nameTarget == 'ALK-Abello' ) {
        $selskapsBox[$i]->Name = 'ALK-Abelló B'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0060027142'; //sett isin 
      
    }
    else if ($nameTarget == 'Ringkjøbing Landbobank, Aktieselskab' ) {
        $selskapsBox[$i]->Name = 'Ringkjøbing Landbobank'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0060854669'; //sett isin 
   
    }
    else if ($nameTarget == 'A.P. Møller - Mærsk') {
        $selskapsBox[$i]->Name = 'A.P. Møller - Mærsk B'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0010244508'; //sett isin 
 
    }

    $nameTarget = $selskapsBox[$i]->Name;

    $success = 0;

    if ($row = binary_search_multiple_hits('', $nameTarget, $isin_list, $name_list, 'denmark'))
    {
        $selskapsBox[$i]->ISIN = $row[0];
         
        if (isset($row[9]))
        {
            $selskapsBox[$i]->numberOfStocks =  (int) $row[9];
            $success = 1;
            echo $i . '. ' . $nameTarget . '. ' . $selskapsBox[$i]->numberOfStocks . ' aksjer funnet<br>';
            //var_dump($selskapsBox[$i]->numberOfStocks);
          
        }
        else
        {
            echo 'Number of stocks not found. <br>';
        }

    }
    
    if ($success == 0) 
    {
        echo $i . '. ' . 'Fant IKKE: ' . $selskapsBox[$i]->Name . ' OBS: Dette må trolig rettes manuelt!<br>';
        $missBox[] = $selskapsBox[$i]->Name;
    }
}


foreach ($selskapsBox as $key => $selskap)
{
    echo $key . '. '; 
    echo $selskap->Name  . '. ' . $selskap->numberOfStocks . '<br>';
}


//var_dump($selskapsBox[1]->Positions);

for ($i = 0; $i < $count; $i++) {
    //echo $selskapsBox[$i]->Name . '<br>';

    if (isset($selskapsBox[$i]->Positions)) {
        $positionsCount = count($selskapsBox[$i]->Positions);
        $target = $selskapsBox[$i]->Name;
        for ($x = 0; $x < $positionsCount ; $x++) {
            
            $selskapsBox[$i]->Positions[$x]->Name =  $target;
            
            //hvis ikke isin er satt
            if ($selskapsBox[$i]->ISIN == '') {
                
                //fjern feilmelding av isin som er sjekket opp at er ok
                if ($target == 'Harboes Bryggeri') {
                	
                }
                else {
                	echo "The following company's ISIN was not found: ";
                	echo $target . ' ' . ' <br>';
                }


                unset ($selskapsBox[$i]);
                break;
            }
        }
    }
}

$selskapsBox = array_values($selskapsBox);
$count = count($selskapsBox);


for ($i = 0; $i < $count; $i++) {
     

    //find stock target
    $isinTarget = $selskapsBox[$i]->ISIN;

    $success = 0;

    if (isset($selskapsBox[$i]->numberOfStocks))
    {
        $numberOfStocks = (int)$selskapsBox[$i]->numberOfStocks;
        $success = 1;
    }
    else
    {
        errorecho('Number of stocks not found or zero for ' . $selskapsBox[$i]->ISIN . ' | '  . $selskapsBox[$i]->Name . '<br>');
        var_dump($selskapsBox[$i]);
        continue;
    }

    echo $i . '. ' . $selskapsBox[$i]->Name . ' | ' . $isinTarget . '<br>';

    if (isset($selskapsBox[$i]->Positions)) 
    {
        $selskapsBox[$i]->NumPositions = count($selskapsBox[$i]->Positions);
        $positionsCount = $selskapsBox[$i]->NumPositions;

        $ShortedSum = 0;
        $ShortPercent = 0;
        $NetShortPosition = 0;

        for ($x = 0; $x < $positionsCount ; $x++) {

            //trim spaces and other stuff

            $selskapsBox[$i]->Positions[$x]->PositionHolder = trim($selskapsBox[$i]->Positions[$x]->PositionHolder);

            //remove is name has "Point72" in it
            if (\strpos($selskapsBox[$i]->Positions[$x]->PositionHolder, 'Point72') !== false) {
                unset ($selskapsBox[$i]->Positions[$x]);
                //echo 'Unset posisjon ' . $i . '->' . $x . ' pga Point72-navn';
                continue;
            }

            if ($selskapsBox[$i]->Positions[$x]->ShortPercent == '') {
                //Antall aksjer er satt, sett inn procent

                if ($selskapsBox[$i]->numberOfStocks == 0) {
                    echo 'Number of stocks for company is zero! <br>';
                    continue;
                }

                $percent = (((float)$selskapsBox[$i]->Positions[$x]->NetShortPosition)/(int)$selskapsBox[$i]->numberOfStocks)*100;
                
                $percent = number_format($percent,4,".","");
                $selskapsBox[$i]->Positions[$x]->ShortPercent = (float)$percent;

                 //echo $i . '. ' . $selskapsBox[$i]->Name .  ' percent is missing, filling in ' . $selskapsBox[$i]->Positions[$x]->ShortPercent . '.<br>';
            }
            else if ($selskapsBox[$i]->Positions[$x]->NetShortPosition == '') 
            {
                //echo 'hit!';
                    
                if ($selskapsBox[$i]->numberOfStocks == '')
                {
                    errorecho('Number of stock missing for ' . $selskapsBox[$i]->Name  . '<br>');

                }
                else
                {
                    //Antall aksjer er satt, sett inn procent
                    $NetShortPosition = (($selskapsBox[$i]->Positions[$x]->ShortPercent)*$selskapsBox[$i]->numberOfStocks);

                    $NetShortPosition = round($NetShortPosition,4);
                    $selskapsBox[$i]->Positions[$x]->NetShortPosition = $NetShortPosition/100;
                    //echo $i . '. ' . $selskapsBox[$i]->Name . ' netshortposition is missing, filling in ' . $selskapsBox[$i]->Positions[$x]->NetShortPosition . ' .<br>';
                }
            }
        }
    }


}

$companyCount = count($selskapsBox);

//sort positions from newest to oldest
for ($i = 0; $i < $companyCount; $i++) {
    $netShortTemp = 0;
    (float)$ShortPercentTemp = 0;
    $positionCount = count($selskapsBox[$i]->Positions);
    
    //sorter posisjonene fra nyest til eldst
    usort($selskapsBox[$i]->Positions, "comp");

    for ($z = 0; $z < $positionCount; $z++) {
        //Rund av beregnignen av antall aksjer shortet i hver posisjon
        $selskapsBox[$i]->Positions[$z]->NetShortPosition = 
        (int)number_format((float)$selskapsBox[$i]->Positions[$z]->NetShortPosition,0,"","");
        
        //tildel ISIN til hver posisjon
        $selskapsBox[$i]->Positions[$z]->ISIN = $selskapsBox[$i]->ISIN;

        //finn selskapene med aktive posisjoner og legg til temp verdi
        if ($selskapsBox[$i]->Positions[$z]->isActive == 'yes') {
            $netShortTemp += (float)$selskapsBox[$i]->Positions[$z]->NetShortPosition;
            $ShortPercentTemp += (float)$selskapsBox[$i]->Positions[$z]->ShortPercent;
        }
    //remove description of position and type
        unset($selskapsBox[$i]->Positions[$z]->description);
        unset($selskapsBox[$i]->Positions[$z]->type);
    }
    //tildel selskapet aggregerte verdier
    $selskapsBox[$i]->ShortedSum = $netShortTemp;
    $selskapsBox[$i]->ShortPercent = $ShortPercentTemp;
    $selskapsBox[$i]->LastChange = $selskapsBox[$i]->Positions[0]->ShortingDate; 

}

//legg til tickere
foreach ($selskapsBox as $key => $selskap)
{

    if ($row = binary_search_multiple_hits($selskap->ISIN, $selskap->Name, $isin_list, $name_list, 'denmark'))
    {
         $selskapsBox[$key]->ticker =  $row[1] . '.' . $row[8]; 
         $selskapsBox[$key]->ticker = mb_strtolower($selskapsBox[$key]->ticker);
         $selskapsBox[$key]->ticker= str_replace(' ', '-', $selskapsBox[$key]->ticker);

         $count = count($selskapsBox[$key]->Positions);

         for ($i = 0; $i < $count; $i++)
         {
            $selskapsBox[$key]->Positions[$i]->ticker = $selskapsBox[$key]->ticker;
         }
    }
    else
    {
        errorecho($nameTarget . ' not found<br>');
    }
}


//save file
$string =  '../shorteurope-com.luksus.no/dataraw/denmark.history/denmark.history_current' . '.json';
saveJSON($selskapsBox, $string);


?>