<?php 

include '../production_europe/functions.php';

//isinlisten må rebaseres, da det er duplictate navn. 
$land = 'france';

$historyJson = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json';

//les inn alle posisjoner
$selskapsposisjoner = file_get_contents($historyJson);
$selskapsposisjoner = json_decode($selskapsposisjoner, true);
$success = 0;

$countSelskapsposisjoner = count($selskapsposisjoner);

echo ' Antall selskaper: ' . $countSelskapsposisjoner .  '<br>';  
echo '<br>';

//gå igjennom og sett alt til inaktive
foreach ($selskapsposisjoner as $key => $selskap)
{
  foreach ($selskap['Positions'] as $index => $posisjon)
  {
    $selskapsposisjoner[$key]['Positions'][$index]['isActive'] = 'no';

  }
}

$newAllPositionsHolder = [];
$idArrayToSetActive = [];

//gå igjennom posisjonene
$countSelskapsposisjoner = count($selskapsposisjoner);
echo ' Antall selskaper: ' . $countSelskapsposisjoner .  '<br>';  

for ($i = 0; $i < $countSelskapsposisjoner; $i++) {

  echo 'Selskap: ' . $i . '. ' . $selskapsposisjoner[$i]['Name'] . '<br>';

  $posCount = count($selskapsposisjoner[$i]['Positions']);


  //collect all player names for compay positions
  $allPlayerNameBox = [];

  for ($x = 0; $x < $posCount; $x++) 
  {
      //Collekt all player names for company
    if (isset($selskapsposisjoner[$i]['Positions'][$x])) 
    {
      $allPlayerNameBox[] = $selskapsposisjoner[$i]['Positions'][$x]['PositionHolder'];

      //apply company name to each position
      $selskapsposisjoner[$i]['Positions'][$x]['Name'] = $selskapsposisjoner[$i]['Name'];

      //set all to not active
      //$selskapsposisjoner[$i]['Positions'][$x]['isActive'] = 'no';

      //also fixing the timestamp
      $newestdate = date('Y-m-d', (strtotime($selskapsposisjoner[$i]['Positions'][$x]['ShortingDate'])));
      $selskapsposisjoner[$i]['Positions'][$x]['ShortingDate'] = $newestdate;
    }
  }

 //sort the positons
  usort($selskapsposisjoner[$i]['Positions'], function ($item1, $item2) {
    return $item2['ShortingDate'] > $item1['ShortingDate'];
  });

  $allPlayerNameBox = array_unique($allPlayerNameBox);
  $allPlayerNameBox  = array_values($allPlayerNameBox);
  $allPlayerNameBoxCount = count($allPlayerNameBox);

  echo 'Antall playere i selskap: ' . $allPlayerNameBoxCount . '<br>';
    //var_dump($allPlayerNameBox);

  $selskapsposisjoner[$i]['Positions'] = array_values($selskapsposisjoner[$i]['Positions']);

  $posCount = count($selskapsposisjoner[$i]['Positions']);

  //alle playernavn
  for ($z = 0; $z < $allPlayerNameBoxCount; $z++) 
  {

    $nameTarget = strtolower($allPlayerNameBox[$z]);
    echo $z . '. Nametarget er: ' . $nameTarget . "<br>";

    $foundfirst = 0;

    for ($x = 0; $x < $posCount; $x++) 
    {
      if ($nameTarget == strtolower($selskapsposisjoner[$i]['Positions'][$x]['PositionHolder'])) 
      {
        $foundfirst++;

        if ($foundfirst > 1)
        {
          //unset all but first position
          //unset($selskapsposisjoner[$i]['Positions'][$x]);
          $selskapsposisjoner[$i]['Positions'][$x]['isActive'] = 'no';
          continue;
        }

        //check first position of player
        if ($selskapsposisjoner[$i]['Positions'][$x]['ShortPercent'] > 0 and $foundfirst == 1)
        {
          echo 'Found first position of ' . $selskapsposisjoner[$i]['Positions'][$x]['PositionHolder'] . ' in ' . $selskapsposisjoner[$i]['Positions'][$x]['Name'] . '<br>';
          $selskapsposisjoner[$i]['Positions'][$x]['isActive'] = 'yes';
          
        }       
      }
    }

    $selskapsposisjoner[$i]['Positions'] = array_values($selskapsposisjoner[$i]['Positions']);

  }
  echo '<br>';
}


foreach ($selskapsposisjoner as $key => $selskap)
{

  $numPositions = 0;
  $lastChange = '2001-01-01';
  $shortedSum = 0;
  $shortedPercent = 0;

  foreach ($selskap['Positions'] as $index => $posisjon)
  {
    $numPositions++;

    if ($posisjon['isActive'] == 'yes')
    {
      $shortedSum += $posisjon['NetShortPosition'];
      $shortedPercent += $posisjon['ShortPercent'];
    }

    if ($lastChange < $posisjon['ShortingDate'])
    {
      $lastChange = $posisjon['ShortingDate'];
    }
  }

  $selskapsposisjoner[$key]['ShortPercent'] = $shortedPercent;
  $selskapsposisjoner[$key]['ShortedSum'] = $shortedSum;
  $selskapsposisjoner[$key]['LastChange'] = $lastChange;
  $selskapsposisjoner[$key]['NumPositions'] = $numPositions;

}

$delisted_list = [];
$delisted_list[] = 'Ingenico Group - GCS';
$delisted_list[] = 'PEUGEOT S.A.';
$delisted_list[] = 'PEUGEOT S.A';

for ($i = 0, $count = count($delisted_list); $i < $count; $i++)
{
  $delisted_list[$i] = mb_strtolower($delisted_list[$i]);
}

echo '<strong>Setter selskapsposisjoner til null for selskaper som er tatt av børs:</strong><br>';

//resett positions for Peugeot S.A
for ($i = 0, $count = count($selskapsposisjoner); $i < $count; $i++) 
{

  if (in_array(mb_strtolower($selskapsposisjoner[$i]['Name']), $delisted_list))
  {
    $isactive_box = [];

    echo $i . '. ' . $selskapsposisjoner[$i]['Name'] . ' posisjoner inn: ' . count($selskapsposisjoner[$i]['Positions']) . '.<br>';

    foreach ($selskapsposisjoner[$i]['Positions'] as $key => $position)
    { 

      if ($position['isActive'] == 'yes')
      {
        $selskapsposisjoner[$i]['Positions'][$key]['isActive'] = 'no';
        $isactive_box[] = $position;
      }
      else if ($position['isActive'] == '')
      {
        $selskapsposisjoner[$i]['Positions'][$key]['isActive'] = 'no';
      }

    }

    echo '$isactive_box  count er ' . count($isactive_box) . '<br>';

    $enddate = date('Y-m-d');

    //adding end positons
    foreach ($isactive_box as $index => $position)
    {
      $isactive_box[$index]['ShortingDate'] = $enddate;
      $isactive_box[$index]['ShortPercent'] = 0;
      $isactive_box[$index]['NetShortPosition'] = 0;
      $isactive_box[$index]['isActive'] = 'no';

      $selskapsposisjoner[$i]['Positions'][] = $isactive_box[$index];
    }

    usort($selskapsposisjoner[$i]['Positions'], function ($item1, $item2) 
    {
      return $item2['ShortingDate'] > $item1['ShortingDate'];
    });

    echo 'Posisjoner ut: ' . count($selskapsposisjoner[$i]['Positions']) . '<br>'; //var_dump($selskaper[$i]->Positions);
    
  }

}

echo '<br>';

//checking for correct data

//var_dump($selskapsposisjoner[0]['Positions']);



///add up statistikken på selskapsnivå og lagre i history
echo 'Saving historical positions...<br>';
$historyfilename = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json';
saveJSON($selskapsposisjoner, $historyfilename);


echo 'Removeing all inactive positions.<br>';

//slett alt som ikke er aktive posisjoner
foreach ($selskapsposisjoner as $key => $selskap)
{
  echo $key .'. ' . $selskap['Name'] . '. ' . $selskap['ShortPercent'] . '<br>';
  $shortedssum = 0;

  foreach ($selskap['Positions'] as $index => $position)
  {
    if ($position['isActive'] == 'yes')
    {
      continue;
    }
    else
    {
      unset($selskapsposisjoner[$key]['Positions'][$index]);
    }
  }

  $selskapsposisjoner[$key]['Positions'] = array_values($selskapsposisjoner[$key]['Positions']);

}


//fix stats after deleting positons
foreach ($selskapsposisjoner as $key => $selskap)
{

  $numPositions = 0;
  $lastChange = '2001-01-01';
  $shortedSum = 0;
  $shortedPercent = 0;

  foreach ($selskap['Positions'] as $index => $posisjon)
  {
    $numPositions++;

    if ($posisjon['isActive'] == 'yes')
    {
      $shortedSum += $posisjon['NetShortPosition'];
      $shortedPercent += $posisjon['ShortPercent'];
    }

    if ($lastChange < $posisjon['ShortingDate'])
    {
      $lastChange = $posisjon['ShortingDate'];
    }
  }

  $selskapsposisjoner[$key]['ShortPercent'] = $shortedPercent;
  $selskapsposisjoner[$key]['ShortedSum'] = $shortedSum;
  $selskapsposisjoner[$key]['LastChange'] = $lastChange;
  $selskapsposisjoner[$key]['NumPositions'] = $numPositions;

}

//delte companies with zero positions
echo '<br>Deleting companies with zero active positions...<br>';
foreach ($selskapsposisjoner as $key => $selskap)
{

  if (!count($selskap['Positions']) > 0)
  {
    echo $key . '. Zero count, unsetting...' . $selskap['Name'] . '<br>'; 
    unset($selskapsposisjoner[$key]);
  }

}
$selskapsposisjoner = array_values($selskapsposisjoner);


saveJSON($selskapsposisjoner, '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json');

$date = date('Y-m-d');

$url = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date .  '.' . $land . '.json';

saveJSON($selskapsposisjoner, $url );


?>

