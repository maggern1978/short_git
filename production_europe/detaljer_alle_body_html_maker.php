<?php

require('../production_europe/functions.php');
require('../production_europe/namelink.php');

//$land = 'united_kingdom';

//include '../production_europe/options_set_currency_by_land.php';
$string =  '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';

echo 'Reading ' . $string;

if (!$json = readJSON($string))
{
	echo 'Read error, returning';
	return;
}

$date = date('Y-m-d');

$companiesNameBox = [];
$positionCounter = 0;

foreach ($json as $player)
{
	foreach ($player['positions'] as $position)
	{
		$companiesNameBox[] = $position['companyName'];
		$positionCounter++;
	}
}

$companiesNameBox = array_unique($companiesNameBox);
$companiesNameBox = array_values($companiesNameBox);
$number = count($companiesNameBox);


$numberOfPlayers = count($json);

date_default_timezone_set('Europe/Oslo');
$now = date('Y-m-d H:i',strtotime("now"));

ob_start();

echo '<h5>' . $numberOfPlayers . ' players, holding ' . $positionCounter . ' positions in ' . $number . ' companies.';

?>
</h5>
</div>
</div>
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			Companies with missing market data are excluded.
			<?php echo 'Updated: ' . $now;?>. 
			<br><br>
		</div>
	</div>
</div>

<?php

foreach ($json as $key => $player) {

	$total_verdiendring = 0;
	$total_eksponering = 0;
	$total_positionValueChangeSinceStart = 0;

	$total_verdiendring_base_currency = 0;
	$total_eksponering_base_currency = 0;
	$total_positionValueChangeSinceStartBaseCurrency = 0;
	$multicurrency_mode = false;

	$basecurrency = '';

	foreach ($player["positions"] as $index => $posisjon) 
	{
		
		$basecurrency = $posisjon['basecurrency'];

		if ($posisjon['currency'] != $posisjon['basecurrency'])
		{
			$multicurrency_mode = true;
			break;
		}
	}

	$positioncount = count($player["positions"]);

	foreach ($player["positions"] as $index => $posisjon) {
		if ($index == 0) {
			?>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2>
							<?php
							$selskapNew  = ucwords($player['playerName']);
							$singleCompany   = nametolink($selskapNew);
							$singleCompany = strtoupper($singleCompany);
							$historyLink = 'details.php?player=' . $singleCompany .
							'' . '&land=' . $land;
							echo '<a class="text-dark" data-toggle="tooltip" title="See all active positions" href="' . $historyLink  . '">';
							$selskapNew = strtolower($selskapNew);
							echo ucwords($selskapNew);
							echo '</a>';

							?>

							<span class="updated pull-right">
								<?php 

								if ($multicurrency_mode == true and $positioncount > 1)
								{
									echo '*Sums in ' . $basecurrency;
								}
								
								?>
							</span>
						</h2>
						<table class="table table-bordered table-sm">
							<thead>
								<tr>
									<th class="">#</th>
									<th>Company</th>
									<th class='text-right'>
										<span class="">Short</span>
									</th>
									<th class='text-right'>
										<span class="" data-toggle="tooltip" title="Number of stocks shorted multiplied by current stock price">Position value</span>
									</th>
									<th class='text-right'>Stock price</th>
									<th class="text-right" data-toggle="tooltip" title="Stock price change in percent the latest trading day">
										<span class="">Latest price change</span>
									</th>

									<th class="text-right">
										<span class="" data-toggle="tooltip" title="Position value change latest trading day">Pos. value change</span>
									</th>
									<th class="text-right" data-toggle="tooltip" title="Price at short position start (end of day value)">
										<span class="">Startdate stock price</span>
									</th>

									<th class="text-right" data-toggle="tooltip" title="Value change since the short position started">
										<span class="">Total value change</span>
									</th>
								</tr>
							</thead>
							<tbody>

							   <?php //noname end
							} 
							
							$selskapNew  = strtolower($posisjon["companyName"]);
							$selskapNew  = ucwords($selskapNew);
							$singleCompany   = nametolink($selskapNew);
							$singleCompany = strtoupper($singleCompany);
							$historyLink = 'details_company.php?company=' . $singleCompany .
							'&land=' . $land;

							echo '<tr>';
							echo '<td class="">';
							$runner = $index + 1;
							echo $runner;
							echo '.</td>';
							echo '<td>';
							echo '<a class="text-dark" data-toggle="tooltip" title="See active positions 
							for this company" href="' . $historyLink  . '">';
								echo ucwords($selskapNew );
							echo '</a>';
							echo '</td>';
							echo '<td class="text-right shortpercentcell" data-toggle="tooltip" title="' . number_format($posisjon['numberOfStocks'],0,".",",") . ' stocks">';
							echo number_format($posisjon['shortPercent'],2,".",",") . ' %';
							echo '</td>';

							echo '<td class="text-right">';
							$posisjonsverdi = round($posisjon['marketValue'],2);

							echo number_format($posisjonsverdi,2,".",",") . ' M ' . $posisjon['currency'];
							echo '</td>';
							echo '<td class="text-right ">';
							echo number_format((float)$posisjon['stockPrice'],2,".",",");
							echo '</td>';
							echo '<td class="text-right ">';
							echo round($posisjon['stockPriceChange'],2) . ' (' . round($posisjon['valueChangePercent'],2) . ' %)';
							echo '</td>';

							echo '<td class="text-right">';
							$verdiendring = round($posisjon['positionValueChange'],2);
							echo number_format($verdiendring,2,".",",") . ' M ';
							echo '</td>';

							echo '<td class="text-right" data-toggle="tooltip" title="Position started ' . $posisjon['positionStartDate'] . '.">';
							if (!isset($posisjon['startStockPrice']) or $posisjon['startStockPrice'] == '' or $posisjon['startStockPrice'] == 0) 
							{
								echo '-';
							}
							else 
							{
								echo round($posisjon['startStockPrice'],2);
							}
							echo '</td>';

							echo '<td class="text-right">';

							if (!isset($posisjon['positionValueChangeSinceStart']) or $posisjon['positionValueChangeSinceStart'] == '' or $posisjon['startStockPrice'] == '' or $posisjon['startStockPrice'] == 0) 
							{
								echo '-';
							}
							else 
							{
								$change = round($posisjon['positionValueChangeSinceStart']/1000000,2);
								echo number_format($change,2,".",",") . '';
							}
							echo ' M</td>';
							echo '</tr>';


							$total_verdiendring += $posisjon['positionValueChange'];
							$total_eksponering += $posisjon['marketValue'];

							if ($multicurrency_mode == true)
							{
								$total_verdiendring_base_currency += $posisjon['positionValueChange']*$posisjon['base_currency_multiply_factor'];
								$total_eksponering_base_currency += $posisjon['marketValue']*$posisjon['base_currency_multiply_factor'];
								
								if (isset($posisjon['positionValueChangeSinceStartBaseCurrency']) and $posisjon['positionValueChangeSinceStartBaseCurrency'] != '' and $posisjon['positionValueChangeSinceStartBaseCurrency'] != NULL  and $posisjon['positionValueChangeSinceStartBaseCurrency'] != '-') 
								{
									$total_positionValueChangeSinceStartBaseCurrency += (float)$posisjon['positionValueChangeSinceStartBaseCurrency'];
								}								
							}


							if (isset($posisjon['positionValueChangeSinceStart']) and $posisjon['positionValueChangeSinceStart'] != '' and $posisjon['positionValueChangeSinceStart'] != NULL and $posisjon['positionValueChangeSinceStartBaseCurrency'] != '-') {
								$total_positionValueChangeSinceStart += (float)$posisjon['positionValueChangeSinceStart'];
							}
						}			

						if ($positioncount> 1)
						{
							if ($multicurrency_mode == false)
							{
								echo '<tr>';
								echo '<td>';
								echo '</td>';
								echo '<td class="font-weight-bold">';
								echo 'Sum:';
								echo '</td>';
								echo '<td class="text-right">';
								echo '</td>';
								echo '<td class="text-right font-weight-bold">';
								$total_eksponering = round($total_eksponering,2);
								echo number_format($total_eksponering,2,".",",")  . ' M ' . $basecurrency ;
								echo '</td>';
								echo '<td class="text-right">';
								echo '</td>';
								echo '<td class="text-right">';
								echo '</td>';
								echo '<td class="text-right font-weight-bold ' . getcolor($total_verdiendring) . ' ">';
								$total_verdiendring = round($total_verdiendring,2);
								echo number_format($total_verdiendring,2,".",",") . ' M';
								echo '</td>';
								echo '<td class="text-right">';
								echo '</td>';
								echo '<td class="text-right ' . getcolor($total_positionValueChangeSinceStart) . ' font-weight-bold">';
								$total_positionValueChangeSinceStart = round($total_positionValueChangeSinceStart/10000000,2);
								echo  number_format($total_positionValueChangeSinceStart,2,".",",") . ' M';
								echo '</td>';
								echo '</tr>';
							}
							else
							{
								echo '<tr>';
								echo '<td>';
								echo '</td>';
								echo '<td class="font-weight-bold">';
								echo 'Sum:';
								echo '</td>';
								echo '<td class="text-right">';
								echo '</td>';
								echo '<td class="text-right font-weight-bold">';
								$total_eksponering_base_currency = round($total_eksponering_base_currency,2);
								echo number_format($total_eksponering_base_currency,2,".",",")  . ' M ' . $basecurrency ;
								echo '</td>';
								echo '<td class="text-right">';
								echo '</td>';
								echo '<td class="text-right">';
								echo '</td>';
								echo '<td class="text-right font-weight-bold ' . getcolor($total_verdiendring_base_currency) . ' ">';
								$total_verdiendring_base_currency = round($total_verdiendring_base_currency,2);
								echo number_format($total_verdiendring_base_currency,2,".",",") . ' M ' . $basecurrency;
								echo '</td>';
								echo '<td class="text-right">';
								echo '</td>';
								echo '<td class="text-right ' . getcolor($total_positionValueChangeSinceStartBaseCurrency) . ' font-weight-bold" ';
								echo 'data-toggle="tooltip" title="';
								echo 'Calculated. For exchange rates, see footer">';
								$total_positionValueChangeSinceStartBaseCurrency = round($total_positionValueChangeSinceStartBaseCurrency/10000000,2);
								echo  number_format($total_positionValueChangeSinceStartBaseCurrency,2,".",",") . ' M ' . $basecurrency;
								echo '</td>';
								echo '</tr>';
							}
						}

						?>
					</tbody>
				</table>
				<div class="mb-4">
				</div>

			</div>
		</div>
	</div>
	<?php 

} 


//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 

// Write final string to file
$nameFirst = '../shorteurope-com.luksus.no/detaljer_alle_';
$nameLast = '.html';
$nameFull = $nameFirst . $land. $nameLast;

file_put_contents($nameFull, $htmlStr);


?>