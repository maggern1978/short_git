<?php 

//this should run a few times a day only
//echo '<br>Main round for ' . $country . '<br><br>';
//$land = 'germany';
//$land = 'norway';
set_time_limit(20);
$country = $land;

echo '<br>';

include '../production_europe/options_set_currency_by_land.php';

$activePositions = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
$graphData = '../production_europe/history_shorting/' . $land;

require '../production_europe/namelink.php';
require '../production_europe/functions.php';

//$pageTitle = 'Newly shorted companies ' . ucwords($country);

if (!$jsonx = readJSON($activePositions))
{
  errorecho('Read error of jsonx<br>');
  return;
}

$jsonCount = count($jsonx);
$positionsBox = [];
$nameBox = [];

//take alle companies that have at least one active position
for ($i = 0; $i < $jsonCount; $i++) {

  $positionCount = count($jsonx[$i]['positions']);
  for ($x = 0; $x < $positionCount; $x++) {

    $z['companyName'] = $jsonx[$i]['positions'][$x]['companyName'];
    $z['ticker'] = $jsonx[$i]['positions'][$x]['ticker'];
    $positionsBox[] = $z;
    $nameBox[] = $jsonx[$i]['positions'][$x]['companyName'];
  }
}

$nameBox = array_unique($nameBox);
$nameBox = array_values($nameBox);
$nameBoxCount = count($nameBox);

//make graphdata for the companies

//$mode = 'justactive';
//include '../production_europe/graphdata_generator.php';

$zeroDayBoxHolder = [];

//var_dump($positionsBox);

$positionsBoxCount =  count($positionsBox);

//find all tickers 
foreach ($nameBox as $key => $companyName) {

  //find ticker 
  //echo $companyName . ' <br>';

  $ticker = '';

  for ($x = 0; $x < $positionsBoxCount; $x++) {

    if ($positionsBox[$x]['companyName'] == $companyName) {
      $ticker = $positionsBox[$x]['ticker'];
      //echo $key . '. Found ' . $companyName . ' with ticker ' . $ticker . '<br>';
      break;
    }
  }

  if ($ticker == '')  
  {
    errorecho('Did not find ticker for ' . $companyName . ' ' . $positionsBox[$x]['ticker']);
    continue;
  }

  $url = $graphData . '/' .  strtolower($ticker) . '.json';

  if (file_exists($url)) 
  {
    //echo($key . '. File found: ' . $url . '<br>');
    $shortinfo = readJSON($url);
  }
  else 
  {
    errorecho ($key . '. File not found: ' . $url . '<br>');
    continue;
  }

  $day = date('Y-m-d');
  
  while (in_array(date("l", strtotime($day)), ["Saturday", "Sunday"]))
  {
    echo 'Today is a weekend, setting today to previous day...<br>';
    $day = date('Y-m-d', strtotime("- 1 day", strtotime($day)));    

  }
  
  $lastday = date('Y-m-d', strtotime("- 8 years", strtotime($day)));    //8 years
  $successfirstday = false;
  $successlastday = false;
  //find first zero short day
  $zerorunner = 0;
  $dayrunner = 0;

  $firstday = $day;

  $maxround = 10000;
  $round = 0;

  //find first day in data
  while($firstday >= $lastday)
  {

    if ($round++ > $maxround)
    {
      errorecho($companyName . '. Break because of maxround!<br>');
      break;
    }

   if (!isset($shortinfo[$firstday]))
    {
      //echo 'Day is ' . $day . '<br>';
      $firstday = date('Y-m-d', strtotime("- 1 weekday", strtotime($day)));    
      $dayrunner++;
      continue;
    }
    else
    {
      //echo 'First day in data is ' .   $firstday . '<br>';
      break;
    }

  }

  $day = $firstday;

  while($day >= $lastday)
  {

    if (isset($shortinfo[$day]))
    {
      //echo 'Day is ' . $day . '<br>';
      $day = date('Y-m-d', strtotime("- 1 weekday", strtotime($day)));    
      $dayrunner++;
      continue;
    }
    else
    {
      //echo 'first zerodate in data after first entry is ' . $day . '<br>';
      $successfirstday = true;
      $zeroday = $day;

      //finding out how many days with short
      while($zeroday >= $lastday)
      {

        if (!isset($shortinfo[$zeroday]))
        {
          //echo 'Zeroday is ' . $day . '<br>';
          $zeroday = date('Y-m-d', strtotime("- 1 weekday", strtotime($zeroday))); 
          $zerorunner++;     
          continue;
        }
        else
        {
          //echo 'Next zeroday is ' . $zeroday . ' (' . $zerorunner . ' days)<br>';
          $successlastday = true;
          break;
        }
      }
      break; 
    }
  }

  $company=new stdClass();
  $company->name = $companyName;
  

  $zeroDayBoxHolder[] = $company;


  if ($successfirstday == true)
  {
    $company->firstZeroDate = $day;
  }
  else
  {
    $company->firstZeroDate = '-';
  }


  if ($successlastday == true)
  {
      $company->daysWithZero =   $zerorunner;
  }
  else
  {
      $company->daysWithZero = '' . $zerorunner;
  }

}


$count = count($zeroDayBoxHolder);

usort($zeroDayBoxHolder, function($a, $b)
{
  return strcmp($b->firstZeroDate, $a->firstZeroDate);
});

//var_dump($zeroDayBoxHolder);

//function saveJSON($data, $filename)

$url = '../short_data/datacalc/newpositions/' . $country . '/newpositions_' . $country . '.json';

//var_dump($zeroDayBoxHolder);

saveJSON($zeroDayBoxHolder, $url);

?>

<?php //include 'header.html'; ?>

<?php //include 'footer.php'; ?>
<?php //include 'footer_about.html'; ?>
</body>