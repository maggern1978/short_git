<?php

include '../production_europe/functions.php';

set_time_limit(1200);

//read isin list
$data = readCSV('../shorteurope-com.luksus.no/dataraw/isin/history_sublist.csv');

echo '<br>';

//checking if file exists in both directories
foreach ($data as $key => $row)
{
	flush_start();

	echo 'Tid: ';
	timestamp();

	if (!isset($row[8]))
	{
		flush_end();
		continue; 
	}

	$tickerending = strtolower($row[8]);
	$ticker = $row[1];
	$ticker = str_replace(' ', '-', $ticker);

	$filenameBulk = '../production_europe/history_bulk/' . $tickerending . '/' . $tickerending . '_current.json';
	$filenameHistoryJson = '../production_europe/history/' . strtolower($ticker) . '.' . $tickerending . '.json';

	if (!file_exists($filenameBulk) or !file_exists($filenameHistoryJson))
	{
		errorecho("One of the filenames ($filenameBulk or $filenameHistoryJson) does not exists, will continue<br>");
		flush_end();
		continue; 
	}

	if (!$bulkdata = readJSON($filenameBulk))
	{
		errorecho($key . '. Error reading bulk file<br>');
		flush_end();
		continue;
	}

	if (!$historydata = readJSON($filenameHistoryJson))
	{
		errorecho($key . '. Error reading stock history json file<br>');
		flush_end();
		continue; 
	}

	//search for ticker in stockdata
	$tickersuccess = 0;
	$tickerdata = array();

	foreach ($bulkdata as $company)
	{
		if ($company['code'] == $ticker)
		{
			echo $key . '. Ticker found: ' . strtolower($ticker) . '.' . $tickerending . '. ';
			$tickerdata = $company;
			$tickersuccess = 1;
			break;
		}
	}

	if ($tickersuccess == 0)
	{
		$ticker = strtolower($ticker);
		errorecho($key . ". Ticker $ticker.$tickerending not found in bulk data. ");
		echo 'Trying to download single ticker... ';

		if ($singleticker = ticker_download_eodhistorical($ticker . '.' . $tickerending))
		{
			$today = date('Y-m-d');

			if ($singleticker['date'] == $today)
			{
				$tickerdata = $singleticker;
				successecho(' Success. ');
			}
			else
			{
				errorecho('Date in downloaded single ticker do not match today, skipping.<br>');
				var_dump($singleticker);
				flush_end();
				continue; 
			}

		}
		else
		{
			errorecho('fails. ');
			flush_end();
			continue;
		}

		
	}

	$targetdate = $tickerdata['date'];

	echo 'Searching for ' . $targetdate . ' in historyjson. ';
	$foundsuccess = 0;

	foreach ($historydata as $index => $day)
	{
		$stockdate = array_keys($day);
		$stockdate = $stockdate[0];

		if ($stockdate == $targetdate)
		{
			//compare values

			$closeHistory = round($day[$stockdate]['close'],2);
			$closeBulk = round($tickerdata['close'],2);

			if ($closeHistory == $closeBulk)
			{
				successecho('Targetdate and close values are equal in historyjson, skipping!<br>');
				$foundsuccess = 1;
				break;
			}
			else
			{
				echo ' Close values are NOT equal, continuing to add! ';
				unset($historydata[$index]);
			}
			
		}

	}

	if ($foundsuccess == 1)
	{
		flush_end();
		continue;
	}

	$object = new stdClass;
	$object->ticker = strtoupper($row[1] . '.' . $row[8]);
	$object->volume = $tickerdata['volume'];	
	$object->close = $tickerdata['close'];

	$adder = [];
	$adder[$targetdate] = (array)$object;

	array_unshift($historydata, $adder);
	successecho(' Success ');	
	saveJSON($historydata, $filenameHistoryJson);

	echo '<br>';
	flush_end();

}

echo '<br>DONE!<br>';

?>