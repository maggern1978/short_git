<?php 

set_time_limit(5000);

$production = 1;
//nedlasting av aksjer ferdig!
include_once '../production_europe/functions.php';
require '../production_europe/logger.php';
require('../production_europe/run.php');

$land = 'spain';
echo date('H:i:s') . '<br>';
run('../production_europe/spain_download_active_and_historical_positions.php');
echo date('H:i:s') . '<br>';
run('../production_europe/spain_excelreader_historical_positions.php');
echo '<br><strong>Execl converting of ' . $land . ' done... </strong><br>';

echo date('H:i:s') . '<br>';
echo '<br><strong>spain_csv_fill_in_isin.php starter... </strong><br>';
$inputFileName = '../shorteurope-com.luksus.no/dataraw/spain.history/spain.history_current.csv';
$mode = 'history';
include '../production_europe/spain_csv_fill_in_isin.php'; //gjør om csv-filen til en json-fil med alle aktive posisjoner
echo '<br>spain_csv_fill_in_isin.php done <br>';
echo date('H:i:s') . '<br>';

$inputFileName = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.csv';
//echo '<br><strong>csv_duplicates_finder.php starter... </strong><br>';
//include '../production_europe/csv_duplicates_finder.php'; //
//echo '<br>csv_duplicates_finder.php done <br>';
//echo date('H:i:s') . '<br>';

//Konverter nedlastet fil til csv
// Ta alle de aktive posisjonene og legg til de er aktive i csv-filen

run('../production_europe/setActivePositions.php');
run('../production_europe/mergeCSV.php');

$inputFileName = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_merged.csv';
//run('../production_europe/csv_duplicates_finder.php'); //IKKE KJØR, HENGER?
run('../production_europe/csv_to_json_builder_history.php');
run('../production_europe/csv_to_json_history_positions_cleaner.php');

//gjør om navnene fra myndigheter til det som står i hovedlisten for selskaper. 
$mode = 'spainhistory';
$inputFile = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json';
include('../production_europe/json_set_names_to_isinlist.php');

$land = 'spain';
run('../production_europe/verdiutregning_selskap_sweden.php'); //lager all_positions_$land.json

run('../production_europe/json_splitter.php'); //splitter ut stock.prices verdiene

$land = 'spain';
run('../production_europe/graph_short_generator.php'); //lager grafdata

echo '<br>History ' . $land  . ' completed! :D';

?>