
<?php 

//denne filen trenger inputs: $land og $selskapsnavn

switch ($land) {
    case "norway":
    $indexData = '../production_europe/json/ticker/osebx.indx.history.json';
    $stockExchangeName = 'Oslo Stock Exchange';
    break;
    case "sweden":
    $indexData = '../production_europe/json/ticker/omxspi.indx.history.json';
    $stockExchangeName = 'Nasdaq Stockholm OMXSPI';
    break;
    case "denmark":
    $indexData = '../production_europe/json/ticker/omxc25.indx.history.json';
    $stockExchangeName = 'Nasdaq Copenhagen OMXC25';
    break;
    case "finland":
    $indexData = '../production_europe/json/ticker/omxh25.indx.history.json';
    $stockExchangeName = 'Nasdaq Helsinki OMXHPI';
    break;
    case "germany":
    $indexData = '../production_europe/json/ticker/gdaxi.indx.history.json';
    $stockExchangeName = 'DAX';
    break;
    case "spain":
    $indexData = '../production_europe/json/ticker/ibex.indx.history.json';
    $stockExchangeName = 'IBEX 35';
    break;
    case "france":
    $indexData = '../production_europe/json/ticker/fchi.indx.history.json';
    $stockExchangeName = 'CAC 40';
    break;
    case "italy":
    $indexData = '../production_europe/json/ticker/spmib.indx.history.json';
    $stockExchangeName = 'FTSE MIB';
    break;        
    case "united_kingdom":
    $indexData = '../production_europe/json/ticker/ftse.indx.history.json';
    $stockExchangeName = 'FTSE 100';
    break;    
}

$land_navn = ucwords($land);
$stockdata = '../production_europe/history/';
$shortdata = '../production_europe/history_shorting/'. $land .'/';

include '../production_europe/options_set_currency_by_land.php';
require '../production_europe/namelink.php';

if (!isset($selskapsnavn))
{
    return;
}

$selskapsnavn = linktoname($selskapsnavn);
$selskapsnavn = strtolower($selskapsnavn);
$selskapsnavn = ucwords($selskapsnavn);
$dateContainer = array();
$indexContainer = array();

$count = 0;

if (isset($indexData)) {

    $indexBox = file_get_contents($indexData);
    if (!$indexData = json_decode($indexBox, true))
    {
        return;
        echo 'Error<br>';
    } 
}

if (!isset($ticker)) {
    //echo 'Company not found.';
    return;
}

$ticker = str_replace(" ","-",strtolower($ticker),$count);


if (!$shortdata = readJSON($shortdata . $ticker . '.json'))
{
   $shortdata = false;
}


if (!$stockdata = readJSON($stockdata . $ticker . '.json'))
{
    //'(No graphdata available)';
    $stockdata = false;
}

if ($stockdata == false and $shortdata == false)
{
    //echo 'returning';
    return;
}

//find first date
if ($shortdata)
{
   $shortkeys = array_keys($shortdata);
   $oldestdate  = $shortkeys[0];
}
else
{
    $oldestdate = date('Y-m-d', strtotime("-5 years"));
}


?>

<script>

    datesCollection = []
    var today = '<?php echo date('Y-m-d'); ?>'

    var MyDate = new Date('<?php echo $oldestdate; ?>');
    var MyDateString;

    //console.log('Today is ' + today);
    var rounds = 0;

    var d1 = Date.parse(today);

    while (today != MyDateString) 
    {
        MyDate.setDate(MyDate.getDate() + 1);

        var day = MyDate.getDay(MyDate);
        var isWeekend = (day === 6) || (day === 0);

        if (isWeekend)
        {
            continue;
        }

        MyDateString =  MyDate.getFullYear() + '-'
        + ('0' + (MyDate.getMonth()+1)).slice(-2) + '-'
        + ('0' + MyDate.getDate()).slice(-2) ;


        var d2 = Date.parse(MyDateString);
        if (d1 <= d2) 
        {
            break;
        }

        datesCollection.push(MyDateString)

        //console.log(MyDateString);
    }



    dato = [];
    dato = datesCollection;

    var length = datesCollection.length;

    <?php 

    if (isset($indexData)) 
    {
        echo 'var indexData = ' . json_encode($indexData, true) . ';';
    }

    if (isset($stockdata)) 
    {
        echo ' var stockdataOriginal = ' . json_encode($stockdata, true) . ';';
    }

    if (isset($shortdata)) 
    {
        echo ' var shortdata = ' . json_encode($shortdata, true) . ';';
    }

    ?>

//rebase the stockdata
var stockdataOriginallength = stockdataOriginal.length;
var stockdataNew = [];

for (x = 0; x < stockdataOriginallength; x++)
{
 var keys = Object.keys(stockdataOriginal[x]);
 var key = keys[0];
 stockdataNew[key] = stockdataOriginal[x][key]['close'];
}
stockdataOriginal = 0;

//console.log('stockdataNew er ');
//console.log(stockdataNew);


//rebase the shortdata
var data = []; //shortdata
var datesCollectionLength = datesCollection.length;
var temp;

var firsthit = 0;
var firsthitnumber = 0;

//find last date in short data, to avoid missing data making the short to zero.
for (i = datesCollectionLength-1; i >= 0; i--) //start from behind
{

    var targetdate = datesCollection[i];

    if (shortdata[targetdate] !== undefined ) 
    {

        //console.log('Last value is ' + i + ' ('  + shortdata[targetdate] + ')');
        var lastvaluei = i;
        break;

    }

}


for (i = 0; i <= datesCollectionLength; i++) 
{
    var targetdate = datesCollection[i];

    if (shortdata[targetdate] !== undefined ) 
    {

        temp = shortdata[targetdate];
        data[i] = round(temp,3);
        firsthit = 1;

        if (firsthitnumber == 0)
        {
            firsthitnumber = i;
        }

    //console.log('Missing: ' + targetdate);
    }
    else
    {
        //data is undefined, so no data

        //set to zero only if there is value before and after
        if (firsthit == 0) //not if before first
        {
             data[i] = '';
        }
        else
        {

            if (i == datesCollectionLength-1 || i > lastvaluei) //not if last
            {
                
                data[i] = '';
            }
            else
            {
               
               data[i] = '0';
            }
        }
       
        //console.log('Found: ' + targetdate + '. Value is ' + data[i] + '. i er ' + i); //679 643 644
    }
}


if (datesCollectionLength > 365*5)
{
    var zoomlevelpercent = 60;
}
else if (datesCollectionLength > 365*4)
{
    var zoomlevelpercent = 50;
}
else if (datesCollectionLength > 365*3)
{
    var zoomlevelpercent = 33;
}
else if (datesCollectionLength < 365)
{
    var zoomlevelpercent = 0;
}
else
{
    var zoomlevelpercent = (firsthitnumber/datesCollectionLength)*100;
}

if (datesCollectionLength < 365)
{
    var zoomlevelpercent = 0;
}


//rebase the index
var indexDataNew = []; 
var indexDatalength = indexData.length;

for (x = 0; x < indexDatalength; x++)
{
    var date = indexData[x]['date'];
    
    if (indexData[x]['close'] > 0) //no zeros!
    {
        indexDataNew[date] = indexData[x]['close'];
    }
    else
    {
        indexDataNew[date] = '';
    }

}
indexData = 0;

//console.log('indexDataNew er ');
//console.log(indexDataNew);


//------------------------
//del verdiene opp i tre slik at de går inn i grafen

var stockdata = [];
//fill inn stockdata
for (i = 0; i < datesCollectionLength; i++) 
{
    var targetdate = datesCollection[i];

    //first array
    //console.log('Target is ' + targetdate);

    if(stockdataNew[targetdate] === undefined ) 
    {
        stockdata[i] = '';
    }
    else
    {
       stockdata[i] = stockdataNew[targetdate];
   }  

}

//console.log('stockdata er ');
//console.log(stockdata);


var indexdata = [];
//fill inn indexdata
for (i = 0; i < datesCollectionLength; i++) 
{
    var targetdate = datesCollection[i];

    if(indexDataNew[targetdate] === undefined ) 
    {
        indexdata[i] = '';
    }
    else
    {
       indexdata[i] = indexDataNew[targetdate];
   }  
}

//console.log('indexdata er ');
//console.log(indexdata);

//if todays values exists already, replace the values already there. 

var enddate = <?php echo date('Y-m-d');?>;

var length = data.length;

if (typeof(endprice) !== 'undefined') 
{
    stockdata[length-1] = endprice;
    datesCollection[length-1] = today;
}
else
{
    //console.log('Endprice er undefined');
}

if (typeof(endshortpercent) !== 'undefined') 
{
    data[length-1] = endshortpercent;
     //console.log('endshortpercent er ' + endshortpercent);
     datesCollection[length-1] = today;
}
else
{
    //console.log('endshortpercent er undefined');
}



//console.log(datesCollection);
//console.log(data);

//normalize data to first data point of stock data
var indexholderafterhits = [];
indexholderafterhits = indexdata;
var indexholderrafterhitsrelative = [];

var startvaluestock = stockdata[0];
var startvalueindex = indexdata[0]; 
//console.log('startvaluestock: ' + startvaluestock);
//console.log('startvalueindex: ' + startvalueindex);

lengthindexdata = indexholderafterhits.length; 

for (k = 0; k < lengthindexdata; k++) {
    //echo 'console.log(indexholderafterhits[k]);';

    if (indexholderafterhits[k] == '')
    {
        continue;
    }
   
    indexholderrafterhitsrelative[k] = ((indexholderafterhits[k]/indexholderafterhits[0]))*startvaluestock;
    indexholderrafterhitsrelative[k] = round(indexholderrafterhitsrelative[k],3);
    //console.log(indexholderrafterhitsrelative[k]);
    //break;

}

//console.log('Her er rebasert index:');
//console.log(indexholderrafterhitsrelative);

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

var exeptions = [];
length = stockdata.length;

//simulate dots when there is missing data
for (i = 0; i < length; i++) {    
    if (stockdata[i] == "") {
        var x = i;
        while(stockdata[x] == "") {
            x++;
        }

        var first = stockdata[i-1];
        var last = stockdata[x];
        //console.log("Første er " + first);
        //console.log("siste er " + last);
        var numberOfMissingValues = x - i;
        //console.log("numberOfMissingValues er " + numberOfMissingValues);
        
        if (first < last) {
            var difference = first-last;
            var differenceStep = -difference/(numberOfMissingValues+1);
        }
        else {
            var difference = last-first;
            var differenceStep = difference/(numberOfMissingValues+1);
        }
        //console.log("Difference er " + difference);
        //console.log("differenceStep er " + differenceStep);

        for (z = 1; z < numberOfMissingValues+1; z++) {
            var w = i-1 + z;
            var y = z+0;
            stockdata[w] = parseFloat(stockdata[i-1]) + parseFloat((y)*differenceStep);
            stockdata[w] = round(stockdata[w],2);
            exeptions[w] += " (missing data, price estimate)";
             //console.log(stockdata[i+z]);
            }
        }
    }

</script>


<div class="mb-2" id="nokkelhoyre<?php echo $land; ?>" style="width: 100%;height:450px;"></div>

<script>
    function roundToTwo(num) {    
        return +(Math.round(num + "e+2")  + "e-2");
    }
        // based on prepared DOM, initialize echarts instance
        var myChart6<?php echo $land; ?> = echarts.init(document.getElementById('nokkelhoyre<?php echo $land; ?>'));
        // var myChart1 = echarts.init(document.getElementById('5storste'), {renderer: 'svg', devicePixelRatio: '2'});


        var w;
        var h;
        var left;
        var right;
        var headingFontSize;
        var toppen;

        function setparameters() {

            w = window.innerWidth;
            h = window.innerHeight;

            if (w >= 1200) {
             left = '4%';
             right = '4%';
             headingFontSize = 18;
             toppen = '70';
         }
         else if (w >= 992 ) {
             left = '4%';
             right = '4%';
             headingFontSize = 16;
             toppen = '70';
         }
         else if (w >= 768) {
             left = '5%';
             right = '6%';
             headingFontSize = 18;
             toppen = '70';
         }
         else if (w >= 576) {
             left = '7%';
             right = '8%';
             headingFontSize = 18;
             toppen = '90';
         }
         else if (w >= 440) {
            left = '9%';
            right = '10%';
            headingFontSize = 18;
            toppen = '90';
            //console.log('440');
        }
        else if (w >= 340) {
            left = '12%';
            right = '10%';
            headingFontSize = 18;
            toppen = '90';
            //console.log('340');
        }
        else {
            left = '12%';
            right = '10%';
            headingFontSize = 18;
            toppen = '900';
            //console.log('last');
        }
    }

    setparameters();

    option = {
        textStyle: {
            fontFamily: ['Encode Sans','sans-serif'],
        },

        toolbox: {
            show : true,
            itemSize: 15,
            feature : {
                mark : {show: false},
                dataZoom : {show: false},
                magicType : {show: false, type: ['line', 'bar']},
                restore : {show: false},
                dataView : {
                    show: true,
                    title: 'Data',
                    lang: ['Short history', 'Exit', 'Refresh'],

                }
            }
        },    
        grid: {
            top:    toppen,
            bottom: 100,
            left:   left,
            right:  right,
        },
        legend: {
            top: 35,
            itemWidth: 35,
            itemHeight: 15,
            borderWidth: 0,
            textStyle: {
                fontSize: 16,
            },
            <?php
            if (isset($indexData)) {
                echo 'selected: {';
                echo "    '" . $stockExchangeName . " (indexed)': false,";
                echo '},';}
                ?>

                data: [{
                  name: 'Short in % (right axis)',
                  icon: 'rect',
              },
              {
                  name: 'Stock price',
                  icon: 'rect',
              },
              <?php 
              if (isset($indexData)) {
                echo '{';
                echo "name: '" . $stockExchangeName . " (indexed)',";
                echo "  icon: 'rect',";
                echo "}],";
            }
            else {
                echo "],";
            }
            ?>

        },
        animation: false,
        backgroundColor: '#f6f6f6',
        title: {
        //backgroundColor: '#000000',
        left: 'center',
        padding: [10,300,10,300],
        text: 'History <?php echo $selskapsnavn; ?> ',
        textStyle: {
            color: '#000000',
            fontSize: headingFontSize,
            width: 400,
        },
        subtextStyle: {
            fontSize: 14,
            color: '#000000',
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            crossStyle: {
                color: '#999'
            }
        },
        backgroundColor: '#000000',
        confine: true,
        //formatter: '{a}<br> Date: {b} <br>{c}% ',

        formatter: function (params,ticket,callback) {
            //console.log(params)

            var res = '';
            for (var i = 0, l = params.length; i < l; i++) {

                //var start = params[i].seriesName.substr(0, 5);
                var startlong = params[i].seriesName;
                //console.log(startlong);


                //console.log('STart er ' + start);

                if (startlong == '<?php echo $stockExchangeName . ' (indexed)'; ?>') {
                    //var prosent = ((params[i].value-startvalue/startvalue
                    //params[i].dataIndex
                    //indexholderafterhits[params[i].value]
                    indexprice  = roundToTwo(indexholderafterhits[params[i].dataIndex]);
                    sign = '';
                    if ((roundToTwo(((indexprice/indexholderafterhits[0])-1)*100)) > 0) {
                        sign = '+';
                    }


                    res += '<?php echo $stockExchangeName . ' (indexed)'; ?>: <BR>' + indexprice + '  (' + sign + roundToTwo(((indexprice/indexholderafterhits[0])-1)*100) + ' %) <br/> ';
                }
                else if (startlong == 'Stock price') {
                    stockprice  = roundToTwo(params[i].value);
                    firststockprice = stockdata[0];
                    sign = '';
                    if (roundToTwo(((stockprice/stockdata[0])-1)*100) > 0) {
                        sign = '+';
                    }


                    res += params[i].seriesName + ' : ' + stockprice + ' (' + sign + roundToTwo(((stockprice/stockdata[0])-1)*100) + ' %)<br/>';  
                }

                else {
                  //res += params[i].dataIndex;
                  res += params[i].seriesName + ' : ' + params[i].value + '%' + '<br/>';

              }


          }
          setTimeout(function (){
            callback(ticket, res);
        }, 0)
          return 'loading';
      }
  },

  xAxis: [
  {
    name: 'Date',
    type: 'category',
    data: dato,
    axisPointer: {
        type: 'shadow'
    },
    nameTextStyle: {
        fontSize: 16,
        padding: [15,0,100,0],
        align: 'bottom',

    },
    nameRotate: '0',
    nameLocation: 'middle',
}
],
yAxis: [
{
    type: 'value',
    name: 'Price',
    axisLabel: {
        formatter: '{value}',
    },
    nameTextStyle: {
      fontSize: 16,
      padding: [15,20,0,30],
      align: 'right',
  },

  nameLocation: 'start',

},        
{
    type: 'value',
    name: 'Short in %',           
    nameTextStyle: {
        fontSize: 16,
        padding: [15,-20,100,-40],
        align: 'right',
    },
    nameRotate: '0',
    nameLocation: 'start', 
    axisLabel: {
        formatter: '{value}%',

    },
},

],
dataZoom: [
{
    show: true,
    type: 'slider',
    bottom: 10,
    start: zoomlevelpercent,
    end: 100,
    handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
    handleSize: '105%',
    handleStyle: {
      color: 'black',
  },
},
],
series: [
{
    name:'Short in % (right axis)',
    type:'line',
    connectNulls: true,
    data: data, 
    lineStyle: {
      width: 3, 
  },
  showSymbol: false,
  yAxisIndex: 1,
},
{
    name:'Stock price',
    type:'line',
    connectNulls: true,
    data: stockdata,
    showSymbol: false,
    lineStyle: {
      width: 3, 
  },
  yAxisIndex: 0,
},

<?php
if (isset($indexData)) {
    echo '{';
    echo "    name:'" . $stockExchangeName . " (indexed)',";
    echo "    type:'line',";
    echo "    connectNulls: true,";
    echo "    data: indexholderrafterhitsrelative, ";

    echo "    lineStyle: {";
    echo "      width: 3, ";
    echo "    },";
    echo "    showSymbol: false,";
    echo "     yAxisIndex: 0,";
    echo " },";

}
?>
],

};
       // use configuration item and data specified to show chart

       myChart6<?php echo $land; ?>.setOption(option);

       window.addEventListener('resize', function(event){
        setparameters();

        option = {
            grid: {
                top:    toppen,
                bottom: 100,
                left:   left,
                right:  right,
            },
        }  
        myChart6<?php echo $land; ?>.setOption(option);
        myChart6<?php echo $land; ?>.resize();
    });

</script>


