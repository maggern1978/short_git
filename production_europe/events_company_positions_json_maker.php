<?php 

//$land = 'denmark';

//$land = 'italy';

require '../production_europe/namelink.php';
require '../production_europe/logger.php';
require '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

echo '<br>';

$limit_date_previous = '2020-01-01';

$date = date('Y-m-d');

while (!file_exists('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
		//echo $date . '<br>';
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

	if ($date < $limit_date_previous)
	{
		echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
		return;
	}
}	


echo 'Reading new positions: '. '../shorteurope-com.luksus.no/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json</strong><br>';

//read todays positions
if (!$currentdata = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
	errorecho('Error reading json ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
	return;
}

//find the newest position date
$newestdate = '2001-01-01';

foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $key => $position)
	{
		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}
}


//echo 'Previous working day of that is ' . $previousday . '<br>';
if ($newestdate == date('Y-m-d', (strtotime ('-1 weekday', strtotime ($date)))))
{
	echo 'Dates match. File has positions with timestamp from previous weekday (' . $newestdate .'). <br>';
}
else
{
	echo 'Newest date in current positions is ' . $newestdate . '.<br>';
	echo('Dates DOES NOT match. Filename does not have positions with timestamp from previous weekday. Probably no new positions?<br>');
}

echo '<br><strong>Finding file with newest position that is older than newestedate found for current.json:</strong> <br>';


$previous_newest_date = $newestdate;

while ($previous_newest_date == $newestdate)
{

	//finding the previous file
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	echo 'File is ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'. '<br>';

	while (!file_exists('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		echo $date . '<br>';
		$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

		if ($date < $limit_date_previous)
		{
			echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
			return;
		}
	}	


	//read previous positions
	if (!$previousdata = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		errorecho('Error reading previous json ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
		continue;
	}


	//finding newest date
	//find the newest position date
	
	$previous_newest_date = '2001-01-01';

	foreach ($previousdata as $key => $company)
	{
		foreach ($company['Positions'] as $key => $position)
		{
			if ($position['ShortingDate'] > $previous_newest_date)
			{
				$previous_newest_date = $position['ShortingDate'];
			}
		}
	}

	if ($previous_newest_date != $newestdate)
	{
		successecho('Newest date in current positions is ' . $previous_newest_date . '.<br>');
	}

}



echo 'Reading previous positions: ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json </strong>because that file has positions from the day before.<br> ';

echo '<br>';
//collect all changes names from new list

$changesCompanyBox = [];

foreach ($currentdata as $key => $currentcompany)
{
	
	//find same company in previous and check for changes
	$targetName = strtolower($currentcompany['Name']);
	$targetIsin = strtolower($currentcompany['ISIN']);

	$success = 0;
	$foundcount = 0;
	foreach ($previousdata as $index => $previouscompany)
	{

		if ($targetName == strtolower($previouscompany['Name']) or $targetIsin == $previouscompany['ISIN'])
		{
			$success = 1;
			$foundcount++;
			//echo $key . '. ' . $targetName . ' exists in both current and previous. Checking for differences: ';

			$currentcompany['ShortPercent'] = round($currentcompany['ShortPercent'],4);
			$previouscompany['ShortPercent'] = round($previouscompany['ShortPercent'],4);

			if ($currentcompany['ShortPercent'] == $previouscompany['ShortPercent'] and $currentcompany['NumPositions'] == $previouscompany['NumPositions'])
			{
				//echo 'Shortpercent and number of positions are identical.';
			}
			else
			{
				echo $key . '. ' . $targetName  . '. ';
				echo 'Shortpercent previous: ' . $previouscompany['ShortPercent'] . ' and current ' . $currentcompany['ShortPercent']  . '<br>';
				echo 'Positions previous: ' . $previouscompany['NumPositions'] . ' and positions current ' . $currentcompany['NumPositions']  . '<br>';

				echo 'Shortpercent and number of positions are <strong>NOT</strong> identical. Adding previous data to current. ';
				echo '<br><br>';

				$currentdata[$key]['previousShortPercent'] = $previouscompany['ShortPercent'];
				$currentdata[$key]['previousNumPositions'] = $previouscompany['NumPositions'];
				$currentdata[$key]['status'] = 'update';
				$changesCompanyBox[] = $currentdata[$key];
			}
			if ($foundcount > 1)
			{
				errorecho('Findcount er ' . $foundcount . ' meaning erorr with names<br>');
			}			
			
		}

	}

	if ($success == 0)
	{
		$currentdata[$key]['status'] = 'new';
		$currentdata[$key]['previousShortPercent'] = 0;
		$currentdata[$key]['previousNumPositions'] = 0;
		$changesCompanyBox[] = $currentdata[$key];
		echo $key . '. <strong>' . $targetName . ' not found in previouspositions</strong><br>';
	}

}

//now find positions that are ended, meaning previousposition companies that are not in current. 
echo '<br>';
echo 'Finding companies that are in previous, but not in current, meaning short has gone to zero. <br>';
$endedcount = 0;

foreach ($previousdata as $key => $previouscompany)
{

	$targetName = strtolower($previouscompany['Name']);
	$targetIsin = $previouscompany['ISIN'];

	$success = 0;
	foreach ($currentdata as $index => $currentcompany)
	{

		if ($targetName == strtolower($currentcompany['Name']) or $targetIsin == $currentcompany['ISIN'])
		{
			//echo $key . '. ' . $targetName . ' from previous also exists in current, skipping to next. ';
			$success = 1;
			break;
		}
	}

	if ($success == 0)
	{
		echo $key . '. ' . $targetName . ' from previous is <strong>NOT</strong> found in current positions, adding to changesCompanyBox <br>';
		$previousdata[$key]['status'] = 'ended';
		$previousdata[$key]['previousShortPercent'] = $previousdata[$key]['ShortPercent'];
		$previousdata[$key]['previousNumPositions'] = $previousdata[$key]['NumPositions'];
		$previousdata[$key]['ShortPercent'] = 0;
		$previousdata[$key]['NumPositions'] = 0;
		$changesCompanyBox[] = $previousdata[$key];
		$endedcount++;

		//var_dump($previousdata[$key]);

	}

 	//echo '<br>';

}

echo 'Positions that ended: ' . $endedcount . '<br>';
echo '<br>';

//set lastchange date

$count = count($changesCompanyBox);

for ($i = 0; $i < $count; $i++)
{
	$newestdate = '2001-01-01';

	foreach ($changesCompanyBox[$i]['Positions'] as $position)
	{

		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}

	$changesCompanyBox[$i]['LastChange'] = $newestdate;
}


//remove positions as they are not needed

$count = count($changesCompanyBox);

for ($i = 0; $i < $count; $i++)
{
	unset($changesCompanyBox[$i]['Positions']);
}

//set change

for ($i = 0; $i < $count; $i++)
{
	if (!isset($changesCompanyBox[$i]['previousShortPercent']))
	{
		errorecho('Error! missing previousShortPercent<br>');
		var_dump($changesCompanyBox[$i]);
		continue;
	}

	$changesCompanyBox[$i]['change'] = $changesCompanyBox[$i]['ShortPercent'] - $changesCompanyBox[$i]['previousShortPercent'];
	$changesCompanyBox[$i]['change'] = round($changesCompanyBox[$i]['change'],2);

}

//sort the positons
usort($changesCompanyBox, function ($item1, $item2) {
	return $item2['change'] > $item1['change'];
});

foreach ($changesCompanyBox as $key => $company)
{
	$changesCompanyBox[$key] = float_format($company);
}




saveJSON($changesCompanyBox, '../production_europe/json/events/company/' . $land . '.events.company.current.json');

//var_dump($changesCompanyBox);


?>