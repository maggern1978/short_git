<?php

//delete contents of file 
$saveurl = '../shorteurope-com.luksus.no/dataraw/united_kingdom.history/united_kingdom.history_current.csv';

include 'functions.php';

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
//use PhpOffice\PhpSpreadsheet\Cell\Cell;

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');

//$writer = new Xlsx($spreadsheet);
//$writer->save('hello world.xlsx');

$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("../shorteurope-com.luksus.no/dataraw/united_kingdom/united_kingdom_download.xlsx");
$sheetnames = $spreadsheet->getSheetNames();
$spreadsheet->setActiveSheetIndex(1);

$dataArray = $spreadsheet->getActiveSheet()
    ->ToArray(
           // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
    );

$success = 0;
if (!isset($dataArray[8]['B'])) {
	echo 'Data is not set! Download of active positions not working!';
	logger('Klarte ikke laste ned britiske active posisjoner!', '');
	return;
}
else {
	$success = 1;
}

$holderBox = [];

$counter = 0;
if ($success = 1) {
	foreach ($dataArray as $key => $Row) {

			if ($counter > 1 and isset($Row['A'])) {
			//var_dump($Row);
			$rad1 = $Row['A'];
			$rad1 = rtrim($rad1);

			$rad2 = rtrim($Row['B']);
			
			//trim vekk rare verdier
			$rad2 = rtrim($rad2);
			//echo $rad3;
			$rad2 = str_replace( chr( 194 ) . chr( 160 ), '', $rad2);
			//echo $rad3;
			$rad2 = strtoupper($rad2);
			
			//fjerne space i isin
			$rad3 = preg_replace('/\s+/', '', $Row['C']);
			//$rad4 = $Row[3];
			
			//formater bort kommaer
			$rad4 = number_format((float)$Row['D'],2,'.','.');

						
			$rad5 = (string)$Row['E'];

			//ta bort "/" i dato
			
			$old_date = $Row['E'];        
			$old_date_timestamp = strtotime($old_date);
			$new_date = date('Y-m-d', $old_date_timestamp);   

			$rad5 = $new_date;

					
			$lagre_array = array($rad1, $rad2, $rad3, $rad4, $rad5);
			//echo $counter . '<br>';
	    	//saveCSV($Row, 'testcsv.csv');
	    	
			$holderBox[] = $lagre_array;
			}
			//echo $counter . ' ' ;

			$counter++;
	}
}
//sortere

usort($holderBox, function($a, $b) {
    return $a[4] < $b[4];
});

//Lagre
saveCSVx($holderBox, $saveurl);
$holderBox = null;
$dataArray = null;

?>