<?php 

//echo isopen('norway');
//https://www.tradinghours.com/exchanges/omxh-helsinki/market-holidays/2020

if (!function_exists('isopen')) {
    function isopen($country) {

        $today = date("Y-m-d");
        $doNotRunDates = array();

        switch ($country) {
            case "norway":
                $doNotRunDates[] = '2019-04-18';
                $doNotRunDates[] = '2019-04-19';
                $doNotRunDates[] = '2019-04-22';
                $doNotRunDates[] = '2019-05-01';
                $doNotRunDates[] = '2019-05-17';
                $doNotRunDates[] = '2019-05-30';
                $doNotRunDates[] = '2019-06-10';
                $doNotRunDates[] = '2019-12-24';
                $doNotRunDates[] = '2019-12-25';
                $doNotRunDates[] = '2019-12-26';
                $doNotRunDates[] = '2019-12-31';
                $doNotRunDates[] = '2020-01-01';
                $doNotRunDates[] = '2020-01-01';
                $doNotRunDates[] = '2020-04-09';
                $doNotRunDates[] = '2020-04-10';
                $doNotRunDates[] = '2020-04-13';
                $doNotRunDates[] = '2020-12-24';
                $doNotRunDates[] = '2020-12-25';
                $doNotRunDates[] = '2020-12-31';

                break;
            case "sweden":
                $doNotRunDates[] = '2019-04-19';
                $doNotRunDates[] = '2019-04-22';
                $doNotRunDates[] = '2019-05-30';            
                $doNotRunDates[] = '2019-06-06';
                $doNotRunDates[] = '2019-06-21';
                $doNotRunDates[] = '2019-12-24';
                $doNotRunDates[] = '2019-12-25';
                $doNotRunDates[] = '2019-12-26';
                $doNotRunDates[] = '2019-12-31';
                $doNotRunDates[] = '2020-01-01';
                $doNotRunDates[] = '2020-01-06';
                $doNotRunDates[] = '2020-04-13';
                $doNotRunDates[] = '2020-05-01';
                $doNotRunDates[] = '2020-05-21';
                $doNotRunDates[] = '2020-06-01';
                $doNotRunDates[] = '2020-06-09';
                $doNotRunDates[] = '2020-06-19';
                $doNotRunDates[] = '2020-12-24';
                $doNotRunDates[] = '2020-12-25';
                $doNotRunDates[] = '2020-12-31';
                break;
            case "denmark":
                $doNotRunDates[] = '2019-04-18';
                $doNotRunDates[] = '2019-04-19';
                $doNotRunDates[] = '2019-04-22';
                $doNotRunDates[] = '2019-05-17';
                $doNotRunDates[] = '2019-05-30';
                $doNotRunDates[] = '2019-05-31';
                $doNotRunDates[] = '2019-06-05';
                $doNotRunDates[] = '2019-06-10';
                $doNotRunDates[] = '2019-12-24';
                $doNotRunDates[] = '2019-12-25';
                $doNotRunDates[] = '2019-12-26';
                $doNotRunDates[] = '2019-12-31';
                $doNotRunDates[] = '2020-04-09';
                $doNotRunDates[] = '2020-04-10';
                $doNotRunDates[] = '2020-05-08';
                $doNotRunDates[] = '2020-05-21';
                $doNotRunDates[] = '2020-05-22';
                $doNotRunDates[] = '2020-06-01';
                $doNotRunDates[] = '2020-06-05';
                $doNotRunDates[] = '2020-12-24';
                $doNotRunDates[] = '2020-12-25';
                $doNotRunDates[] = '2020-12-31';

                break;
            case "finland":
                $doNotRunDates[] = '2019-12-06';
                $doNotRunDates[] = '2019-12-24';
                $doNotRunDates[] = '2019-12-25';
                $doNotRunDates[] = '2019-12-26';
                $doNotRunDates[] = '2019-12-31';
                $doNotRunDates[] = '2020-04-10';
                $doNotRunDates[] = '2020-04-13';
                $doNotRunDates[] = '2020-05-01';
                $doNotRunDates[] = '2020-06-19';
                $doNotRunDates[] = '2020-12-24';
                $doNotRunDates[] = '2020-12-25';
                $doNotRunDates[] = '2020-12-31';
                break;
        }

        $countDays = count($doNotRunDates);
        $hit = 0;
        for ($i = 0; $i < $countDays; $i++) {
            if ($doNotRunDates[$i] == $today) {
               $hit = 1;
               break;
            }
        }

       if ($hit == 0) {
            return true;
       }
       else {
            return false;
       }
    }
}
?>