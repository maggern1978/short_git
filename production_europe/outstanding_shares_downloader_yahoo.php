<?php 

include '../production_europe/functions.php';
include '../production_europe/logger.php';

$placement = 'shorteurope-com.luksus.no';
set_time_limit(6000);

echo '<br>Starter...<br>';

$filenameIsin = '../' . $placement . '/dataraw/isin/hovedlisten_landkoder.csv';

$csvFile = file($filenameIsin );            
$isinData = [];
foreach ($csvFile as $line) {
    $isinData[] = str_getcsv($line, ";");
} 

//fiks enkoding
foreach ($isinData as $counter => $entries) {
    $isinData[$counter] = array_map("utf8_encode", $entries);
    $counter++;
}

$filenames = [];
$urls = [];

$urlFirst = 'https://query1.finance.yahoo.com/v7/finance/quote?symbols=';
$urlLast = '&fields=sharesOutstanding';
$isinArray = [];

$yahooData = [];
$bommelisten = [];
$success_counter = 0;

$isinData = array_reverse($isinData);

foreach ($isinData as $key => $line) {
    
    flush_start();

    //skip header
    if ($key == 0) {
        continue;
    }
    if (!isset($line[8]) or $line[8] == '' or $line[1] == '' ) 
    {
        flush_end();
        continue;
    }

    if (strtolower('XETRA') == strtolower($line[8]))
    {
        //flush_end();
        //continue;

        echo 'xetra!';
    }

    $ticker = $line[1];
    $ticker = str_replace(" ", "-", $ticker, $count);
    $ticker .= '.' . $line[8];
    $isinData[$key][1] = $ticker;
    $url = $urlFirst . $ticker . $urlLast;

    $savepath = '../production_europe/json/outstandingstocks_yahoo/'. mb_strtolower($ticker) . '.json';
    $savepath = mb_strtolower($savepath);

    //only save files if files are 2 workdays or more old
    $today = date('Y-m-d');
    $yesterday = date('Y-m-d',(strtotime ( '-1 weekday' , strtotime ($today))));

    $success = 0;
    
    if (file_exists($savepath))
    {
        $filetime = filemtime($savepath);
        $filedate = date('Y-m-d', $filetime);
        
        if ($yesterday <= $filedate)
        {
            echo $key . '. Skipping download of ' . $savepath . '. File date is ' . $filedate . '<br>';
            flush_end();
            continue;
        }
        else
        {
            $success = 1;
        }
        
    }
    else
    {
        $success = 1;
    }
    

    if ($data = download_outstanding($url))
    {
        $array['SharesOutstanding'] = $data['quoteResponse']['result'][0]['sharesOutstanding'];
        $array['isinlistdata'] = $line;

        if (!is_numeric($array['SharesOutstanding']))
        {
            errorecho($key . '. ' . $ticker . ' ' . $url . ' fails shares_test. Timestamp: ' . date('Y-m-d H:i:s') . '<br>');
            continue;
        }

        $url = mb_strtolower('../production_europe/json/outstandingstocks_yahoo/' . mb_strtolower($ticker) . '.json');
        saveJSONsilent($array, $url);
        successecho($key . '. Downloaded ' . mb_strtolower($ticker) . ' ' . ' Timestamp: ' . date('Y-m-d H:i:s') .'<br>' );
        $success_counter++;
    }
    else
    {
        errorecho($key . '. Could not download ' . $ticker . ' ' . $url . ' Timestamp: ' . date('Y-m-d H:i:s') . '<br>');
        $bommelisten[] = $url;
    }
 
    flush_end();

}

echo 'Trefflisten: '. count($yahooData) . '<br>';
echo 'Bommelisten: ' . count($bommelisten) . '<br>';


function download_outstanding($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLVERSION,1);
    $data = curl_exec ($ch);
    $error = curl_error($ch); 
    curl_close ($ch);
    
    sleep(2);

    if ($error != '') {
        echo '<BR>ERROR DOWNLOADING OUSTANDING SHARES!<BR>';
        var_dump($error);
        return false;

    }
    else {
        $data = json_decode($data, true); 
        if (isset($data['quoteResponse']['result'][0]['sharesOutstanding'])) {
            return $data;
        }
        else {
            return false;
        }
        
    }
}


function saveCSVenkel($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');
        fputcsv($fp, $input);
    fclose($fp);
}

?>

