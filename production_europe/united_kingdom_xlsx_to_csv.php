<?php

if (!isset($mode))
{
	$mode = 'active';
}

echo 'Mode is: ' . $mode . '<br>';

include_once 'functions.php';

if ($mode == 'active')
{
	$saveurl = '../shorteurope-com.luksus.no/dataraw/united_kingdom/united_kingdom_current.csv';
	$keyword_to_process = 'current';
}
else if ($mode == 'history')
{
	$saveurl = '../shorteurope-com.luksus.no/dataraw/united_kingdom.history/united_kingdom.history_current.csv';
	$keyword_to_process = 'historic';
}
else
{
	errorecho('Error with mode, must be active or history. Returning...<br>');
	return;
}

require '../vendor/autoload.php';

timestamp();

$filename = "../shorteurope-com.luksus.no/dataraw/united_kingdom/united_kingdom_download.xlsx";

//https://opensource.box.com/spout/docs/#fluent-interface
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

$reader = ReaderEntityFactory::createReaderFromFile($filename);
//$reader->setShouldFormatDates(true);

$reader->open($filename);

$csv_rows = [];
gc_collect_cycles();
print_mem();

//foreach ($reader->getSheetIterator() as $sheet) {
    //$sheetName = $sheet->getName();
    //$isSheetVisible = $sheet->isVisible();
    //$isSheetActive = $sheet->isActive(); // active sheet when spreadsheet last saved
    //echo $sheetName . '<br>';
//}


foreach ($reader->getSheetIterator() as $sheet) 
{
	$sheetName = $sheet->getName();

	if (stripos($sheetName, $keyword_to_process) !== false) 
	{
    	echo 'Doing sheetname: ' . $sheetName . ' based on keyword ' . $keyword_to_process . '<br>';
	}
	else
	{
		continue;
	}

    foreach ($sheet->getRowIterator() as $row) 
    {
        // do stuff with the row
        $cells = $row->getCells();
        
        $single_row = [];

        foreach ($cells as $cell) 
        {
            
        	$var = $cell->getValue();

            if ($var instanceof DateTime) {
  				$var = date_format($var,"Y-m-d");
			}

            $single_row[] = $var;

        }
        $csv_rows[] = $single_row;
       
    }
    break;
}

$reader->close();

//delete først row
unset($csv_rows[0]);

usort($csv_rows, function($a, $b) //sorter på data
{
	return $a[4] < $b[4];
});

//Lagre
saveCSVx($csv_rows, $saveurl);

timestamp();

//helps memory?
$csv_rows = 0;

?>