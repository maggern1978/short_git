<?php

include '../production_europe/functions.php';
$countries = get_countries();
$directoryList = [];

echo '<br>';

foreach ($countries as $country)
{
	echo '<strong>' . $country . '</strong><br>';
	$prefix = '../shorteurope-com.luksus.no/dataraw/';
	$files = listfiles($prefix . $country);
	deletefiles($files, $prefix . $country . '/');

	$prefix = '../shorteurope-com.luksus.no/dataraw/';
	$files = listfiles($prefix . $country . '.history/');
	deletefiles($files, $prefix . $country . '.history/');

	$prefix = '../shorteurope-com.luksus.no/datacalc/players/';
	$files = listfiles($prefix . $country . '/');
	deletefiles($files, $prefix . $country . '/');

}

$directoryList[] = '../shorteurope-com.luksus.no/dataraw/';

function deletefiles($array, $prefix)
{

	foreach ($array as $file)
	{

		//strpos(string,find,start)
		if ((is_file($prefix . $file)))
		{

			$tendaysago = date('Y-m-d', strtotime("-9 day"));
			$filetime = filemtime($prefix . $file);
			$filedate = date('Y-m-d', $filetime);

			if ($filedate < $tendaysago)
			{
				echo $prefix . $file . ': ' . $filedate.  ' older then ' . $tendaysago . '<br>';
				unlink($prefix . $file);
			}
		}
		else 
		{
			echo 'Skipping ' . $file . '<br>';
		}

	}

}


?>