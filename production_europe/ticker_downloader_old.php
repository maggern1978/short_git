<?php 

require '../production_europe/logger.php';

$tickerArray[0] =  ['^FTSE', '^GDAXI', '^DJI', '^GSPC', '^IXIC', 'NIY=F', '^HSI'];
$tickerArray[1] = ['FTSE 100', 'DAX', 'Dow Jones', 'S&P 500', 'NASDAQ Comp.', 'Nikkei', 'Hang Seng'];
$locationArray = [];
$locationArray[0] = $locationArray[1] = $locationArray[2] = $locationArray[3] = $locationArray[4] = $locationArray[5] = $locationArray[6] = '../shorteurope-com.luksus.no/dataraw/tickere/international/';
$locationArray[] = '../shorteurope-com.luksus.no/dataraw/tickere/';

//var_dump($tickerArray);
$filnameEnding = '.json';
$urlStart = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=';
$urlEnd = '&apikey=4XI39Z4NYH3C983R';
$path = '../shorteurope-com.luksus.no/dataraw/tickere/international/';

foreach ($tickerArray[0] as $key => $ticker){
    
    $filename = $tickerArray[1][$key] . $filnameEnding;
    $url = $urlStart . $ticker . $urlEnd;
    $NUM_OF_ATTEMPTS = 3;
    $attempts = 0;

    do {
        
        try
        {
           download_jsonx($url, $filename, $locationArray[$key]);
        } catch (Exception $e) {
            echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
            $attempts++;
            continue;
        }

        break;

    } while($attempts < $NUM_OF_ATTEMPTS);

} 




$yahooTickere = ['^OMXSPI', '^OMXC20', '^OMXHPI','OSEBX.OL'];
$filenames = ['stockholm_current.json', 'copenhagen_current.json', 'helsinki_current.json','oslobors_current.json'];

foreach ($yahooTickere as $key => $ticker) {

    $NUM_OF_ATTEMPTS = 3;
    $attempts = 0;

    $filename = $filenames[$key];
    $url = 'https://query1.finance.yahoo.com/v7/finance/quote?symbols=' . $ticker;
    $path = '../shorteurope-com.luksus.no/dataraw/tickere/';

    do {
        
        try
        {
           download_jsonx($url, $filename, $path);
        } catch (Exception $e) {
            echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
            $attempts++;
            continue;
        }

        break;

    } while($attempts < $NUM_OF_ATTEMPTS);


$string = $path . $filename;
$string = file_get_contents($string);
$helsinkiJson = json_decode($string, true);  

//var_dump($helsinkiJson['quoteResponse']['result']);

if (!isset($helsinkiJson['quoteResponse']['result']['0']['regularMarketPrice'])) {
    logger('Error, empty value in downloaded file', 'in ticker.php');
}

    $latestClose = $helsinkiJson['quoteResponse']['result']['0']['regularMarketPrice'];  
    $previousClose = $helsinkiJson['quoteResponse']['result']['0']['regularMarketPreviousClose'];



    //echo 'Stockholm latest is: ' . $latestClose;
    //echo 'Stockholm previous is: ' . $previousClose;

$change = $latestClose - $previousClose;
//echo '<br>Stockholm change is: ' . $change ;

$percent = (1-($previousClose/$latestClose))*100;
//echo '<br>Percent change is: ' . $percent ;

$percent = round($percent ,4);

$percent = (string)$percent . '%';

//echo '<br>Percent change is: ' . $percent ;

$day = date("d") ;
$month = date("m") ;
$year = date("Y") ;

//Build string
$latestTradingDay = $year . '-' . $month . '-' . $day;

$dataNew = array("Global Quote" => 
           array(
            "08. previous close" => round($previousClose,4),
            "07. latest trading day" => $latestTradingDay,
            "05. price" => round($latestClose,4),
            "10. change percent" => $percent,
            "09. change" => round($change,4),
            ));

$dataNew = json_encode($dataNew, JSON_PRETTY_PRINT);
$destination = $path . $filename;
//$file = fopen($destination, "w+");
file_put_contents($destination, $dataNew);
//fclose($file);

}


function download_jsonx($url, $filename, $path) {
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSLVERSION,1);
$data = curl_exec ($ch);
$error = curl_error($ch); 
if (curl_error($ch)) {
    $error_msg = curl_error($ch);
    echo $error_msg;
}
curl_close ($ch);

$destination = $path . $filename;
$file = fopen($destination, "w+");
fputs($file, $data);
fclose($file);
}

function download_json_oslo($url, $filename, $path) {
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSLVERSION,1);
$data = curl_exec ($ch);
$error = curl_error($ch); 
if (curl_error($ch)) {
    $error_msg = curl_error($ch);
    echo $error_msg;
}
curl_close ($ch);

//Get last trading day value from index, since alpha vantage gives wrong


    $indexBox = file_get_contents('../shorteurope-com.luksus.no/datacalc/graphdata/index/osebx.json');
    $indexData = json_decode($indexBox, true); 
    //var_dump($indexData);
    $count = count($indexData);
    $yesterday = date('Y-m-d',strtotime("today -1 Weekday"));
    //echo $yesterday;
    $yesterdayValue = '0';
    //var_dump($yesterday);

    for ($x = $count-1; $x > 0; $x--) {
       if ($indexData[$x][0] == $yesterday) {
         $yesterdayValue = $indexData[$x][1];
         //echo 'Found date' . ' Yesterday value er ' .  $yesterdayValue ;
         break;
       } 
    }

$data = json_decode($data,true);

$data["Global Quote"]["08. previous close"] = $yesterdayValue;

if ($yesterdayValue != 0) {



$percent = (($data["Global Quote"]['05. price']/$yesterdayValue)-1)*100;

$percent = round($percent,4);

$percent = (($data["Global Quote"]['05. price']/$yesterdayValue)-1)*100;

$data["Global Quote"]["09. change"] = $data["Global Quote"]['05. price'] - $yesterdayValue;

$data["Global Quote"]["09. change"] = (string)$data["Global Quote"]["09. change"];

$data["Global Quote"]["10. change percent"] = (string)$percent . '%';

//$data = json_encode($data, JSON_PRETTY_PRINT);

//var_dump($data);

$destination = $path . $filename;
//var_dump($destination);



$file = fopen('../shorteurope-com.luksus.no/dataraw/tickere/oslobors_current.json', "w+");
fwrite($file, json_encode($data, JSON_PRETTY_PRINT)); 
fclose($file);
}
}

?>