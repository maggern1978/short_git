<?php 

//laster ned både historiske og nåværende kurser

require '../production_europe/logger.php';
require '../production_europe/functions.php';

//dax

$tickere = ['GDAXI','OSEBX','OMXSPI','OMXC25','OMXH25', 'OMXC20', 'OMXC20CAP', 'FCHI','IBEX', 'FCHI', 'SPMIB', 'FTSE'];

//OMX Copenhagen 25 Index
//OMX Stockholm
//OSE Benchmark
//DAX Index
//OMX Helsinki 25

//https://eodhistoricaldata.com/api/real-time/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json

foreach ($tickere as $ticker)
{
    //continue;

    $url = $ticker . '.INDX';

    if ($data = ticker_download_eodhistorical($url))
    {
        $url = strtolower($url);
        
        saveJSON($data, '../production_europe/json/ticker/' . $url . '.json');

    }
    else
    {
        //logger('download error in ticker_downloader.php ', $url);
    }
}

foreach ($tickere as $ticker)
{

    //continue;

    $url = $ticker . '.INDX';
    $fromdate = '2010-01-01';

    if ($data = download_ticker_history($url, $fromdate))
    {
        $url = strtolower($url);
        
        saveJSON($data, '../production_europe/json/ticker/' . $url . '.history.json');

    }
    else
    {
        //logger('download error in ticker_downloader.php ', $url);
    }
}

//last ned oslo

$yahooTicker = 'OSEBX.OL';
$filename = 'oslobors_current.json';
$url = 'https://query1.finance.yahoo.com/v7/finance/quote?symbols=' . $yahooTicker;

if ($data = json_decode(download($url),true))
{  
    saveJSON($data, '../production_europe/json/ticker/osebx.ol_yahoo.json');
}  

//kovert the norwegian one 

$osloyahoo = $data;

if (!isset($osloyahoo['quoteResponse']['result']['0']['regularMarketPrice'])) {
    return;
}

$latestClose = $osloyahoo['quoteResponse']['result']['0']['regularMarketPrice'];  
$previousClose = $osloyahoo['quoteResponse']['result']['0']['regularMarketPreviousClose'];

$change = $latestClose - $previousClose;
echo 'Oslo change is: ' . $change . '. Close is ' . $latestClose ;

$percent = (1-($latestClose/$previousClose))*-100;
echo '<br>Percent change is: ' . $percent ;

$percent = round($percent ,2);


$timestamp = $osloyahoo['quoteResponse']['result']['0']['regularMarketTime'] + ($osloyahoo['quoteResponse']['result']['0']['gmtOffSetMilliseconds']/1000);
        
$timestamp = (new DateTime("@$timestamp"))->format('Y-m-d H:i:s');
$time = date('H:i:s', strtotime($timestamp));
$tradingDay= date('Y-m-d', strtotime($timestamp));
$timestamp = date('Y-m-d H:i:s', strtotime($timestamp));


$object = new stdClass;
$object->code = 'OSEBX.INDX';
$object->change = $change;
$object->change_p = $percent;
$object->date = $tradingDay;
$object->close = $latestClose;
$object->previousClose = $previousClose;

var_dump($object);

if ($data = json_decode(download($url),true))
{  
    saveJSON($object, '../production_europe/json/ticker/osebx.ol.json');
} 

?>