<?php

if (!isset($mode))
{
	$mode = 'active';
}

echo 'Mode is: ' . $mode . '<br>';

include_once 'functions.php';

if ($mode == 'active')
{
	$saveurl = '../shorteurope-com.luksus.no/dataraw/sweden/sweden_current.csv';
    $filename = "../shorteurope-com.luksus.no/dataraw/sweden/currentpositions.ods";
}
else if ($mode == 'history')
{
	$saveurl = '../shorteurope-com.luksus.no/dataraw/sweden.history/sweden.history_current.csv';
    $filename = '../shorteurope-com.luksus.no/dataraw/sweden.history/historypositions.ods';
}
else
{
	errorecho('Error with mode, must be active or history. Returning...<br>');
	return;
}

require '../vendor/autoload.php';

timestamp();



//https://opensource.box.com/spout/docs/#fluent-interface
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

$reader = ReaderEntityFactory::createReaderFromFile($filename);
//$reader->setShouldFormatDates(true);

$reader->open($filename);

$csv_rows = [];
gc_collect_cycles();
print_mem();

//foreach ($reader->getSheetIterator() as $sheet) {
    //$sheetName = $sheet->getName();
    //$isSheetVisible = $sheet->isVisible();
    //$isSheetActive = $sheet->isActive(); // active sheet when spreadsheet last saved
    //echo $sheetName . '<br>';
//}


foreach ($reader->getSheetIterator() as $sheet) 
{
	$sheetName = $sheet->getName();
    echo 'Sheet name is: ' . $sheetName . '<br>';

    foreach ($sheet->getRowIterator() as $line => $row) 
    {
        
        if ($line < 6)
        {
            continue; 
        }

        // do stuff with the row
        $cells = $row->getCells();
        
         //echo 'Line ' . $line . ': ' . $cells[0]->getValue() . '<br>';

        $single_row = [];

        foreach ($cells as $cell) 
        {
            
        	$var = $cell->getValue();

            if ($var instanceof DateTime) {
  				$var = date_format($var,"Y-m-d");
			}

            $single_row[] = $var;

        }

        if (isset($single_row[3]))
        {
            $single_row[3] = str_replace(',', '.', $single_row[3]);

            if ($single_row[3] == '<0.5' or $single_row[3] == '<0,5')
            {
                $single_row[3] = 0; 
            }

        }

        $csv_rows[] = $single_row;
       
    }
    break;
}

$reader->close();

//delete først row
unset($csv_rows[0]);

usort($csv_rows, function($a, $b) //sorter på data
{
	return $a[4] < $b[4];
});

echo 'Antall oppføringer: ' . count($csv_rows) . '<br>';

//Lagre
saveCSVx($csv_rows, $saveurl);

timestamp();

//helps memory?
$csv_rows = 0;

?>