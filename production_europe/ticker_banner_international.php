
<?php 

date_default_timezone_set('Europe/Oslo');
$now = date('l j. F H:i',strtotime("now"));
$now = strtolower($now);

   //start html catch
   ob_start();
   ?>

<div class="container my-2">
  <div class="row ticker-padding justify-content-center ">
    <div class="col-lg-12 bg-dark ">
      <div class="mt-1"><h5 class="text-white text-center">International markets <?php echo $now; ?></h5></div>
    </div>
  </div>
</div>
<div class="container my-2">
  <div class="row ticker-padding justify-content-center">
   <?php

   //Hentilene
   $path = 'dataraw/tickere/international/';
   $files = array_diff(scandir($path), array('.', '..'));

   //ta bort json-filen
   $filesWithoutJson = array();
   foreach ($files as $key => $file) {
    $filesWithoutJson[] = str_replace(".json","",$file);
   }

   foreach ($filesWithoutJson as $key => $file) {
    //Last inn nyeste data 
   $string = file_get_contents($path .  $file . '.json');
   $current = json_decode($string, true);    

        //echo $oslobors_current['Global Quote']['01. symbol'];

   if (isset($current['Global Quote']['10. change percent'])) {

       $endring = $current['Global Quote']['10. change percent'];
       $hovedindeksen = $current['Global Quote']['05. price'];

       $hovedindeksen = number_format((float)$hovedindeksen,2,",",".");
       $endring = number_format((float)$endring,2,",",".");

            //lag logikk for å sette farge på utviklingen
        $switch = '';
        if ($current['Global Quote']['09. change'] >= 0)
        {
            if ($current['Global Quote']['09. change'] == 0)
            {
                $switch = 'exchange-header-neutral';
            }
            else 
            {
                $switch = 'exchange-header-up';
            }

        }
        else 
        {
            $switch = 'exchange-header-down';
        }
    }
    else {
         $switch = 'exchange-header-neutral';
         $endring = '-';
         $hovedindeksen = '-';
        }

?>             

            <div class="pl-3 text-center">
                <div class="py-0 pt-1">
                    <span class="exchange-header <?php echo $switch; ?>">
                    <?php echo $endring; ?>%</span>
                </div>
                <div class="py-0 pb-1" data-toggle="tooltip" title="<?php echo $file; ?>">
                    <span class="d-none d-sm-inline"> <strong><?php echo $file; ?></strong> </span>
                    <span class="d-inline d-sm-none exchange-index"> <strong><?php echo $file; ?></strong> </span>                            
                    <span class="exchange-index"><?php echo $hovedindeksen; ?></span>
                </div>
            </div>

            <?php } 

            

            //  Return the contents of the output buffer
            $htmlStr = ob_get_contents();
            // Clean (erase) the output buffer and turn off output buffering
            ob_end_clean(); 
            // Write final string to file
            file_put_contents('banner_international.html', $htmlStr);

            ?>
            


