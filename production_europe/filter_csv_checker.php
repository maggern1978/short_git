<?php


include '../production_europe/functions.php';

$data = readCSVkomma($inputFileName);
$cellcount = 5;

foreach ($data as $key => $row)
{

	if (count($row) != 5)
	{
		echo $key . '. Cell count (5) is not correct, unsetting<br>';
		var_dump($row);
		unset($data[$key]);
		continue;
	}

	foreach ($row as $index => $cell)
	{

		$info = str_replace("\n", '', $cell);
		$info = str_replace('"', '', $cell);
		$info = trim($info);
		$data[$key][$index] = $info;

	}
}

$data = array_values($data);

foreach ($data as $key => $row)
{
	//var_dump($row);
}

saveCSVx($data, $inputFileName);


?>