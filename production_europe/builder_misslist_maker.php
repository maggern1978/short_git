<?php 

include('../production_europe/logger.php');
include('../production_europe/functions.php');

$date = date('Y-m-d');
set_time_limit(5000);

//$land = 'germany';

$isinlisten = '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv';
$isinlisten_isinsortert = '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv';

if (!$isinlisten = readCSV($isinlisten))
{
	echo 'could not read hovedlisten';
}

if (!$isinlisten_isinsortert = readCSV($isinlisten_isinsortert))
{
	echo 'could not read hovedlisten $isinlisten_isinsortert';
}

$countries = get_countries();

foreach ($countries as $land)
{

	echo $land  . '. <br>';

	$fileCurrent = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';

	$current = readJSON($fileCurrent);

	foreach ($current as $company)
	{

		$isintarget = $company['ISIN'];
		$nametarget = $company['Name'];

		$found = 0;

		if ($indexhit = binary_search_isinliste($nametarget, $isinlisten))
		{
			$found = 1;
			$row = $isinlisten[$indexhit]; 
		}
		else if ($indexhit = binary_search_isinliste_isin($isintarget, $isinlisten_isinsortert))
		{
			$found = 1;
			$row = $isinlisten_isinsortert[$indexhit]; 
		}

		if ($found == 0)
		{
			echo 'Company not found: ' . $nametarget . ' ' . $isintarget . '<br>';
		}

	}

}


?>

