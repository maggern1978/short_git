				    <div class="container">
			           				          
				           <?php

				           require '../production_europe/namelink.php';

				           if ($mode == 'multi' or $mode2 == 'singledetails') {
				           		echo '<div class="d-flex ">';
								echo '	<div class="d-none d-sm-block">';
				           		echo '<h2>';

			    				$selskapNew  = ucwords($companyName);
			    				$selskapNew = strtolower($selskapNew);
					            $singleCompany   = nametolink($selskapNew);
					            $singleCompany = strtoupper($singleCompany);
					            $historyLink = 'history_company.php?player=x&selskapsnavn=' . $singleCompany  . '&land=' . $land;
					            echo '<a class="text-dark"  data-toggle="tooltip" title="' . "See company's full history" . '" href="' . $historyLink  . '">';

					            //fix ticker 
					            $ticker = $obj_user->ticker;
					            $position = strpos($ticker,'.',0);
					            $ticker = substr($ticker,0,$position);
					            $tickertradingview = $ticker;
					            $companynametradingview = ucwords($selskapNew);
					            $ticker = str_replace("-", " ", $ticker);

 
							    echo ucwords($selskapNew) . ' <span class="detaljer-selskap-change text-muted">(' . $ticker . ')</span>';
							    echo '</a>';
							    echo '</div>';

							    if (isset($headerSwitch)) {
								
									echo '<div class="pl-3 pt-1 ml-auto d-none d-sm-block text-dark">';
							    	echo '';
							        echo '<span class="detaljer-selskap-price">';
							        echo ' ';  
							        echo number_format($aksjekurs,2) . ' ' . $currency_ticker;
							        echo '</span>';
							        echo '<span class="pl-2 detaljer-selskap-change ';
							        echo getcolor($kursendring);

							        echo '">';
							        if ($kursendring > 0) {
							        	echo '+';
							        }
							        echo $kursendring . ' (';

							        if ($kursendring > 0) {
							        	echo '+';
							        }
							        echo $kursendring_prosent_formatert . ' %)';
							        echo '</span>';
							        echo '</div>';
							        echo '<div class="d-block d-sm-none mb-2">';
							        echo '<div class="mb-0">';
							        echo '<span class="h2">';
							        echo ucwords($selskapNew) ;
							        echo '</span>';
							    	echo '</div>';
							        echo '<span class="detaljer-selskap-price text-dark">';
							        echo '' . $aksjekurs . ' ' . $currency_ticker;
							        echo '</span>';

					        		echo '<span class="pl-2 detaljer-selskap-change ';
							        echo getcolor($kursendring);

							        echo '">';
							        if ($kursendring > 0) {
							        	echo '+';
							        }
							        echo $kursendring . '';

					
							        echo ' (';
							        if ($kursendring > 0) {
							        	echo '+';
							        }
							        echo $kursendring_prosent_formatert . ' %)</span>';
							        
							        echo '</div>';
							    }

							   echo '</div>';

							}
				           ?>
				         
				           </div>
				          <div class="container">
				          <div class="row">
				          <div class="col-12">
				 			<table class="table table-bordered table-sm">
							    <thead>
							      <tr>
							        <th class="">#</th>
							        <th>Player</th>
							        <th class='text-right'>
								        <span class="">Short</span>
							        </th>
							        <th class='text-right'>
								        <span data-toggle="tooltip" title="Number of stocks shorted multiplied by current stock price">Position value</span>
								        	
							        </th>
							        <?php
							        	if (!isset($headerSwitch)) {
							        ?>
							        <th class='text-right'>Stock price</th>
							        <th class="text-right" data-toggle="tooltip" title="Stock price change in percent the latest trading day">
								        Latest price change
							        </th>
							        <?php
							        	}
							        ?>
							        <th class="text-right">
								        <span data-toggle="tooltip" title="Position value change latest trading day, for the player. A positive value means the player hass earned (stock price falls) on the position.">Latest win/loss</span>
							        </th>

							        
							        <th class="text-right" data-toggle="tooltip" title="Price at short position start (end of day value)">
								        Startprice position
							        </th>
							        
							       	<th class="text-right" data-toggle="tooltip" title="Value change since the short position started, for the position holder.  A positive value means the player has earned (stock price falls) on the position.">
								        Total value change
							        </th>
							      </tr>
							    </thead>
							    <tbody>