<?php

switch ($land) {
    case "norway":
        $currency_ticker = 'NOK';
        break;
    case "sweden":
        $currency_ticker = 'SEK';
        break;
    case "denmark":
        $currency_ticker = 'DKK';
        break;
    case "finland":
        $currency_ticker = 'EUR';
        break;
    case "germany":
        $currency_ticker = 'EUR';
        break;
    case "united_kingdom":
        $currency_ticker = 'GBP';
        break;
    case "france":
        $currency_ticker = 'EUR';
        break;                 
    case "italy":
        $currency_ticker = 'EUR';
        break; 
    case "spain":
        $currency_ticker = 'EUR';
        break;     
    default:
    echo 'Not found';
    return;
}


?>