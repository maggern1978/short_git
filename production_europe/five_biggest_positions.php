
<?php 
//$land = 'sweden';
require '../production_europe/logger.php';

include '../production_europe/options_set_currency_by_land.php';

$landnavn = ucwords($land);
$playerliste = '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';



if (!function_exists('cmp')) {

    function cmp($a, $b)
    {
        return strnatcmp($a->marketValueBaseCurrency, $b->marketValueBaseCurrency);
    }
}

//Last inn nyeste shorttabell 
$string = file_get_contents($playerliste);
$playerposisjoner = json_decode($string, true);        

//var_dump($playerposisjoner[0]);
$posisjonsholder = array();

foreach ($playerposisjoner as $key => $player) {
    foreach ($player['positions'] as $index => $posisjon) {
        $obj_posisjoner = new stdClass;
        $obj_posisjoner->playerName = $player['playerName'];
        $obj_posisjoner->marketValueBaseCurrency = $posisjon['marketValueBaseCurrency'];
        $obj_posisjoner->marketValue = $posisjon['marketValue'];
        $obj_posisjoner->currency = $posisjon['currency'];
        $obj_posisjoner->companyName = $posisjon['companyName'];
        $posisjonsholder[] = $obj_posisjoner;
    }
}

usort($posisjonsholder, "cmp");
$posisjonsholder = array_reverse($posisjonsholder);

//var_dump($posisjonsholder);

$marketValueArray = array();
$playerArray = array();
$companyArray = array();
$marketValueArrayFormated = array();
$counter = 0;

foreach ($posisjonsholder as $posisjon) {
    $marketValueArray[] = number_format($posisjon->marketValue,1,".","");
    $marketValueArrayFormated[] = number_format($posisjon->marketValue,1,".",",");
    $temp_navn_player = $posisjon->playerName;
    $temp_navn = $posisjon->companyName;
    
    $currencyarray[] = $posisjon->currency;
    $playerArray[] = $temp_navn_player;
    $companyArray[] = $temp_navn;
    if ($counter > 3) {
        break;
    }
    $counter++;
}

if (!isset($playerArray[0])) {
    logger('playerarray not set, returning ', 'in five_biggest_positions.php. Land: ' . $land);
    return;
}

// Turn on output buffering
ob_start();
?>

<div id="storste<?php echo $land; ?>" style="width: 100%;height:280px;"></div>
<script>
    var names = <?php echo json_encode($playerArray); ?>;
    var marketvalue = <?php echo json_encode($marketValueArray); ?>;
    var company = <?php echo json_encode($companyArray); ?>;

</script>


<script type="text/javascript">

       //var style = window.getComputedStyle(5storste);
       //var width = parseFloat(style.width);

       //var x = width;
       //var y = 'px';
       //var z = x + y;
       //document.getElementById('5storste').style.width=z;

        // based on prepared DOM, initialize echarts instance
        var myChartStorste<?php echo $land; ?> = echarts.init(document.getElementById('storste<?php echo $land; ?>'));
        // var myChart1 = echarts.init(document.getElementById('5storste'), {renderer: 'svg', devicePixelRatio: '2'});

        var w = window.innerWidth;
        var h = window.innerHeight;

        if (w < 600) {
            var position = 'inside';
            var padding = 0;
            var linecolor = '#ffffff';
            var radius = '69%';

        }
        else {
            var position = 'outside';
            var padding = 0;
            var linecolor = '#000000';
            var radius = '69%';
        }

        var itemStyle = {
            normal: {
                opacity: 1,
                borderWidth: 3,
                borderColor: 'white'
            },
            emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                borderWidth: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        };
        option = {
            toolbox: {
                show : true,
                zlevel: 5,
                itemSize: 20,


                feature : {
                    dataView : {
                        show: true,
                        title: ' ',
                        icon: 'image://img/dataicon.png',
                        iconStyle : {
                            color: 'ccc',
                        },

                        top: 'bottom',
                        lang: ['Five biggest bets', 'Exit', 'Refresh'],
                    },
                },
            },
            textStyle: {
                fontFamily: ['Encode Sans','sans-serif'],
            },
            animation: false,
            backgroundColor: '#f6f6f6',
            title: {
                backgroundColor: '#000000',
                zlevel: 0,
                left: 'center',
                padding: [7,300,10,300],

                text: '5 biggest bets by value in <?php echo ucwords($landnavn); ?>',
                textStyle: {
                    color: '#ffffff',
                    fontSize: 18,
                    width: 400,
                }
            },
        //legend: {
        //orient: 'horizontal',
        //bottom: '0',
        //data: [company[0], company[1], company[2], company[3], company[4]],
        
    //},
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {names} {c}"
    },
    series: [{
        name: 'Top 5 positions by value in <?php echo ucwords($landnavn); ?>:',
        type: 'pie',
        selectedMode: 'single',
        selectedOffset: 10,
        clockwise: true,
        center: ['50%', '55%'],
        radius: [0, radius],
        label: {
            padding: padding,
            position: position,
            formatter: "{b}",           
            textStyle: {
                fontSize: 14,
                color: '#000000', 
                backgroundColor: '#f9f9f9',
                padding: [4,4,4,4],
                avoidLabelOverlap: true,
            }
        },
        labelLine: {
            normal: {
                lineStyle: {
                    color: linecolor,
                }
            }
        },
        data:[
        {
            value:marketvalue[0], 
            name:company[0],
            tooltip: {
                backgroundColor: '#000',
                formatter: "{b}:<br /> <?php echo $marketValueArrayFormated[0]; ?>M <?php echo $currencyarray[0]; ?> <br /> Position holder:<br /> <?php echo $playerArray[0]?>",
            }
        },
        {
            value:marketvalue[1], 
            name:company[1],
            tooltip: {
                backgroundColor: '#000',
                formatter: "{b}:<br /> <?php echo $marketValueArrayFormated[1]; ?>M <?php echo $currencyarray[1]; ?> <br /> Position holder:<br /> <?php echo $playerArray[1]?>",
            }
        },
        {
            value:marketvalue[2], 
            name:company[2],
            tooltip: {
                backgroundColor: '#000',
                formatter: "{b}:<br /> <?php echo $marketValueArrayFormated[2]; ?>M <?php echo $currencyarray[2]; ?> <br /> Position holder:<br /> <?php echo $playerArray[2]?>",
            }
        },
        {
            value:marketvalue[3], 
            name:company[3],
            tooltip: {
                backgroundColor: '#000',
                formatter: "{b}:<br /> <?php echo $marketValueArrayFormated[3]; ?>M <?php echo $currencyarray[3]; ?> <br /> Position holder:<br /> <?php echo $playerArray[3]?>",
            }
        },
        {
            value:marketvalue[4], 
            name:company[4],
            tooltip: {
                backgroundColor: '#000',
                formatter: "{b}:<br /> <?php echo $marketValueArrayFormated[4]; ?>M <?php echo $currencyarray[4]; ?> <br /> Position holder:<br /> <?php echo $playerArray[4]?>",
            }
        },
        ],
        itemStyle: itemStyle
    }]
};
        // use configuration item and data specified to show chart
        myChartStorste<?php echo $land; ?>.setOption(option);    
        window.addEventListener('resize', function(event){   
          myChartStorste<?php echo $land; ?>.resize();
      });

  </script>

  <?php 
  $filenameStart = '../shorteurope-com.luksus.no/five_biggest_positions_';
  $filenameEnd = '.html';

  $filename = $filenameStart . $land . $filenameEnd;

//  Return the contents of the output buffer
  $htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
  ob_end_clean(); 
// Write final string to file
  file_put_contents($filename , $htmlStr);
  ?>