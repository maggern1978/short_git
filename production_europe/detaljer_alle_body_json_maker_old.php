<?php

require('../production_europe/functions.php');
require('../production_europe/namelink.php');

//$land = 'norway';

switch ($land) {
	case "norway":
	$landShort = 'nor';
	$fil = '../shorteurope-com.luksus.no/dataraw/ssr.finanstilsynet/finanstilsynet_current.json';
	$shortliste = '../shorteurope-com.luksus.no/datacalc/shortlisten_nor_current.csv';
	$currency_ticker = 'NOK';
	break;
	case "sweden":
	$landShort = 'swe';
	$fil = '../shorteurope-com.luksus.no/dataraw/fi.se/fi.se_current.json';
	$shortliste = '../shorteurope-com.luksus.no/datacalc/shortlisten_swe_current.csv';
	$currency_ticker = 'SEK';
	break;
	case "denmark":
	$landShort = 'dkk';
	$fil = '../shorteurope-com.luksus.no/dataraw/denmark/denmark_current.json';
	$shortliste = '../shorteurope-com.luksus.no/datacalc/shortlisten_dkk_current.csv';
	$currency_ticker = 'DKK';
	break;
}

require '../production_europe/settings_country.php';

$graphdatafolder = '../production_europe/history/' . $land;

$string = file_get_contents($fil);

$json = json_decode($string, true, 512, JSON_UNESCAPED_UNICODE);

//var_dump($json);

$total_verdiendring = 0;
$total_eksponering = 0;
$total_shortandel = 0;
$total_positionValueChangeSinceStart = 0;
$counter = 1;

//last inn shortlisten med aksjekurser
//$shortliste = 'datacalc/shortlisten_nor_current.csv';
$csvFile = file($shortliste);            
$shortliste = [];

$number = 0;
foreach ($csvFile as $key => $line) {
	$shortliste[] = $data[] =  str_getcsv($line); 
	$number = $key;
};
//var_dump($shortliste[0]);
//var_dump($shortliste[21]);

$count = count($shortliste);

//read date from generated file to set date
$date = date("Y.m.d");

$player_array = array();
$positionCount = 0;

foreach ($json as $key => $selskap) {

	//echo 'Selskap: ' . $selskap['Name'] . '<br>';

	foreach ($selskap['Positions'] as $index => $posisjon) {
		//var_dump($posisjon);
		if (isset($posisjon["isActive"]) and $posisjon["isActive"] == 'yes') {
			//echo 'Found ';
			$player_array[] = $posisjon["PositionHolder"];

			$positionCount++;
		}
		else
		{
			unset($json[$key]['Positions'][$index]);
			//echo '|';
		}
	}
	$json[$key]['Positions'] = array_values($json[$key]['Positions']);

	//echo '<br>';
}

//sort out zero positions
foreach ($json as $key => $selskap) {

	//echo 'Selskap: ' . $selskap['Name'] . '<br>';
	if ($selskap['ShortPercent'] == 0)
	{
		unset($json[$key]);
		continue;
	}

	$count = count($selskap['Positions']);

	if ($count > 0)
	{
		//echo 'Keeping ' . $json[$key]['Name'] . '<br>';
		//var_dump($selskap);
	}
	else
	{
		//echo 'Nullcount, unsetting...' . $json[$key]['Name'] . '<br>';
		unset($json[$key]);
	}
}

$json = array_values($json);


//fill inn stock info
foreach ($json as $key => $selskap) {

	echo $key . '. ' . $selskap['Name'] . '<br>';
	$targetname = $selskap['Name'];
	$ticker = '';
	$stockPrice = '';
	$stockPriceChange = '';

	$success = 0;

	foreach ($shortliste as $index => $row)
	{
		if (strtolower($targetname) == strtolower($row[0]))
		{
			$json[$key]['ticker'] = $row[2];
			$json[$key]['stockPrice'] = $row[6];
			$json[$key]['stockPriceChange'] = $row[7];

			$success = 1;
			break;
		}

	}

	if ($success == 0)
	{
		echo ' Not found <br>';
		var_dump($selskap);
	}
	
}


//legg inn kursdata i posisjonene før de blir delt opp på playere

$jsoncount = count($json);

for ($i = 0; $i < $jsoncount; $i++)
{

	$positionscount = count($json[$i]['Positions']);
	//echo $positionscount;
	//echo '<br>';

	for ($x = 0; $x < $positionscount; $x++)
	{
		$json[$i]['Positions'][$x]['stockPrice'] = $json[$i]['stockPrice'];
		$json[$i]['Positions'][$x]['stockPriceChange'] = $json[$i]['stockPriceChange'];
		$json[$i]['Positions'][$x]['ticker'] = $json[$i]['ticker'];
		$json[$i]['Positions'][$x]['positionStartDate'] = date("Y-m-d", strtotime( $json[$i]['Positions'][$x]['ShortingDate'])); 
		$json[$i]['Positions'][$x]['companyName'] = $json[$i]['Positions'][$x]['Name'];
		$json[$i]['Positions'][$x]['shortPercent'] = $json[$i]['Positions'][$x]['ShortPercent'];
		$json[$i]['Positions'][$x]['numberOfStocks'] = $json[$i]['Positions'][$x]['NetShortPosition'];
		$json[$i]['Positions'][$x]['marketValue'] = ($json[$i]['Positions'][$x]['numberOfStocks'] * $json[$i]['Positions'][$x]['stockPrice']);
		$json[$i]['Positions'][$x]['marketValue']= $json[$i]['Positions'][$x]['marketValue']/1000000;
		$json[$i]['Positions'][$x]['companyName'] = $json[$i]['Positions'][$x]['Name'];

		//percent
		$previousStockPrice = $json[$i]['Positions'][$x]['stockPrice'] - $json[$i]['Positions'][$x]['stockPriceChange'];
		$json[$i]['Positions'][$x]['valueChangePercent'] = (($json[$i]['Positions'][$x]['stockPrice']/$previousStockPrice)-1)*100;

		$json[$i]['Positions'][$x]['positionValueChange'] = ($json[$i]['Positions'][$x]['numberOfStocks']) *  $json[$i]['Positions'][$x]['stockPriceChange'] * -1;
		$json[$i]['Positions'][$x]['positionValueChange'] = $json[$i]['Positions'][$x]['positionValueChange']/1000000;



		
		//$json[$i]['Positions'][$x]['stockPrice'] = 
		//$json[$i]['Positions'][$x]['stockPrice'] =
		//$json[$i]['Positions'][$x]['stockPrice'] =

		unset($json[$i]['Positions'][$x]['Id']);
		unset($json[$i]['Positions'][$x]['EntityId']);
		unset($json[$i]['Positions'][$x]['ShortingDate']);
		unset($json[$i]['Positions'][$x]['ShortPercent']);
		unset($json[$i]['Positions'][$x]['ISIN']);
		unset($json[$i]['Positions'][$x]['Status']);
		unset($json[$i]['Positions'][$x]['NetShortPosition']);
		unset($json[$i]['Positions'][$x]['Name']);
		unset($json[$i]['Positions'][$x]['isActive']);

	}
	$json[$i]['positions'] = $json[$i]['Positions'];

	unset($json[$i]['Positions']);

}

//var_dump(($json[0]['positions']));

//add inn graph start data
echo '<br><br>Adding in historic data, positionStartDate<br>';

for ($i = 0; $i < $jsoncount; $i++)
{

	$positionscount = count($json[$i]['positions']);
	//echo $positionscount;
	//echo '<br>';

	for ($x = 0; $x < $positionscount; $x++)
	{
		//echo '-> ' . $x . '. ';
		$ticker = $graphdatafolder . '/' . strtolower($json[$i]['positions'][$x]['ticker']) . '.json';


		if (file_exists($ticker)) {
			$graphJson = file_get_contents($ticker);
			
			if ($graphJson = json_decode($graphJson, true))
			{
			}
			else
			{
				echo 'Error reading, will continue.';
				continue;
			}

			$graphJsonCount = count($graphJson);
			$targetdate = $json[$i]['positions'][$x]['positionStartDate'];

			$foundsuccess = 0;

			//echo 'Targetdate er ' . $targetdate . '<br> ';

			for ($j = 0; $j < $graphJsonCount; $j++) {

				$keys = array_keys($graphJson[$j]);
				$keys = $keys[0];

				if ($keys == $targetdate) {

					$graphday = $graphJson[$j][$keys];

					//echo $targetdate . ' ' . $graphJson['price'] . '<br>';
					//exit();
					$json[$i]['positions'][$x]['startStockPrice'] = (float)$graphday['close'];
					$json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] = $json[$i]['positions'][$x]['startStockPrice'] - (float)$json[$i]['positions'][$x]['stockPrice'];
					$json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] = $json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] *-1;
					$json[$i]['positions'][$x]['positionValueChangeSinceStart'] = $json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] * $json[$i]['positions'][$x]['numberOfStocks'] * -1;
					
					$foundsuccess = 1;
					break;
				}
			}
		

			if ($foundsuccess == 0)
			{
				echo $i . '. ' . $json[$i]['Name'] . ' <br>';
				echo 'Startdato og kurs ikke funnet! ' . $ticker . '<br>';
			}
			else
			{
				//echo ' ok <br>';
			}

		}
		else
		{
			echo $i . '. ' . $json[$i]['Name'] . ' <br>';
			echo 'File does not exist ' .  '' .  '<br>'; 
		}
	}
}


//var_dump($json);

//lagre playernavn
$playerNameBox = [];

foreach ($json as $selskap)
{
	foreach ($selskap['positions'] as $posisjon)
	{
		$playerNameBox[] = $posisjon['PositionHolder'];
	}
}

$playerNameBox = array_unique($playerNameBox);
$playerNameBox = array_values($playerNameBox);

$allholder = [];

//tildel posisjoner til playere
foreach ($playerNameBox as $key => $playername)
{
	$object = new stdClass;
	$object->playerName = $playername;
	$object->date = date('Y-m-d');
	$object->totalValueChange = 0;
	$object->totalValueChangePercent = 0;
	$object->totalMarketValue = 0;

	$tempholder = [];
    
    echo $key . '. ' . $playername . '<br>';

	foreach ($json as $selskap)
	{
		foreach ($selskap['positions'] as $posisjon)
		{
			if ($posisjon['PositionHolder'] == $playername)
			{
				$tempholder [] = $posisjon;
				
				$object->totalMarketValue += $posisjon['marketValue'];
				$object->totalValueChange  += $posisjon['positionValueChange'];

			}
		}
	}

	$startvalue = $object->totalMarketValue + $object->totalValueChange;
	
	if ($object->totalMarketValue > 0)
	{
	    	$percent = (($startvalue/$object->totalMarketValue)-1)/100;
	    	
	}
	else
	{
	    errorecho('totalmarketvalue er negativ<br>');
	    $percent = '0';
	}

	$object->totalValueChangePercent = $percent;
	$object->positions = $tempholder;
	
	$allholder[] = (array)$object;
	
}

//var_dump($allholder);

foreach ($allholder as $player)
{
	//var_dump($player);
}


//sort by name
usort($allholder, function($a, $b) {
    return $a['playerName'] <=> $b['playerName'];
});

//save to json
$string =  '../shorteurope-com.luksus.no/datacalc/players/' . $landShort . '/playerpositions.' . $date . '.' . $landShort . '.json';
echo 'Saving ' . $string . '<br>';

$saveJson = json_encode($allholder, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
$fp = fopen($string, 'w');
fwrite($fp, $saveJson);
fclose($fp);

$string =  '../shorteurope-com.luksus.no/datacalc/players/' . $landShort . '/playerpositions.' . $landShort . '.current.json';
echo 'Saving ' . $string . '<br>';

//write current
$fp = fopen($string, 'w');
fwrite($fp, $saveJson);
fclose($fp);



?>