<?php

include_once('../production_europe/logger.php');
include_once('../production_europe/functions.php');

$date = date('Y-m-d');
set_time_limit(5000);

//$land = 'sweden';
//$land = 'united_kingdom';

$csvlocation = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.csv';
$fileCurrent = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';
//les inn datane
$csvdata = readCSVkomma($csvlocation);

require '../production_europe/logger.php';

if (!class_exists('selskap')) {

	class selskap {
	  //Creating properties
		public $ISIN;
		public $Name;
		public $ShortPercent;
		public $ShortedSum;
		public $LastChange;
		public $NumPositions;
		public $currency;
		public $ticker;
		public $Positions;


	    //assigning values
		public function __construct($isin, $name, $shortpercent, $shortedsum, $lastchange, $numpositions, $positions, $currency, $ticker) 
		{
			$this->ISIN = $isin;
			$this->Name = $name;
			$this->ShortPercent = $shortpercent;
			$this->ShortedSum = $shortedsum;
			$this->LastChange = $lastchange;
			$this->NumPositions = $numpositions;
			$this->currency = $currency;
			$this->ticker = $ticker;
			$this->Positions = $positions;


		}
	}
}

if (!class_exists('posisjon')) {
	class posisjon {
	  //Creating properties
	    //public $Id;
	    //public $EntityId;
		public $ShortingDate;
		public $PositionHolder;
		public $ShortPercent;
		public $NetShortPosition;
		public $ISIN;
		public $Name;
		public $isActive;
		public $currency;
		
	    //public $Status;

	    //assigning values
		public function __construct($ShortingDate, $PositionHolder, $ShortPercent, $NetShortPosition, 
			$ISIN, $Name, $isActive, $currency, $ticker) 
		{
	          //$this->Id = $Id;
	          //$this->EntityId = $EntityId;
			$this->ShortingDate = $ShortingDate;
			$this->PositionHolder = $PositionHolder;
			$this->ShortPercent = $ShortPercent;
			$this->NetShortPosition = $NetShortPosition;
			$this->ISIN = $ISIN;
			$this->Name = $Name;
			$this->isActive = $isActive;
			$this->currency = $currency;
	        $this->ticker = mb_strtolower(str_replace(" ", '-', $ticker));
		}
	}
}


$playernamebox = [];
$companyIsinBox = [];

//gå igjennom slett der oppføringene ikke stemmer
$csvdata_count = count($csvdata);

for ($i = 0; $i < $csvdata_count; $i++)
{

	$removed = 0;

	for ($x = 0; $x < 5; $x++)
	{
		//removing komma in company and player names

		if (!isset($csvdata[$i][$x]))
		{
			errorecho('Data not set at ' . $i . ' and ' . $x . ' Unsettting<br> <br> ');
			unset($csvdata[$i]);
			$removed = 1;
			break;
		}
	
		$csvdata[$i][$x] = trim($csvdata[$i][$x]);
		$csvdata[$i][$x]  = trim($csvdata[$i][$x] , " \t\n\r\0\x0B\xc2\xa0"); //whitespaces

	}

	if ($removed == 0)
	{
		$csvdata[$i][0] = str_replace(",", "", $csvdata[$i][0]);
		$csvdata[$i][1] = str_replace(",", "", $csvdata[$i][1]);
		$playernamebox[] = mb_strtolower($csvdata[$i][0]);
	}
}

//finding duplicate values
$csvdata = array_values($csvdata);
$count = count($csvdata);

$playernamebox = array_unique($playernamebox);
$playernamebox = array_values($playernamebox);
$playernameboxCount = count($playernamebox);

for ($i = 0; $i < $playernameboxCount; $i++)
{
	//ta et playernavn
	$targetname = $playernamebox[$i];
	$companynamesBoxlocal = [];
	$positionsboxlocal = [];
	$companyIsinBoxLocal = [];

	$count = count($csvdata);

	//samle inn alle selskapsnavnene hvor player har shortposisjoner
	for ($x = 0; $x < $count; $x++)
	{

		if ($targetname == mb_strtolower($csvdata[$x][0]))
		{
			$companynamesBoxlocal[] = mb_strtolower($csvdata[$x][1]);
			$companyIsinBoxLocal[] = mb_strtolower($csvdata[$x][2]);
			$positionsboxlocal[] = $csvdata[$x];
		}

	}

	if (count(array_unique($companynamesBoxlocal)) < count($companynamesBoxlocal) or count(array_unique($companyIsinBoxLocal)) < count($companyIsinBoxLocal))
	{
		
		echo '<strong>' . $i . '. ' . $targetname  . ' </strong>';
		echo 'Duplicate values! Going through positions to remove oldest of each: <br>';

		//go through all values
		$positionsboxlocalCount = count($positionsboxlocal);
		
		$removepositionsBox = [];

		//by company name
		for ($z = 0; $z < $positionsboxlocalCount; $z++)
		{

			$companynametarget = $positionsboxlocal[$z][1];
			$hits = 0;

			foreach ($positionsboxlocal as $key => $row)
			{

				if ($companynametarget == $row[1])
				{

					if ($hits == 0)
					{
						$firstrow = $row;
						$hits++;
					}
					else
					{
						//echo 'Second position !++<br>';
						if ($firstrow[4] > $row[4])
						{
							//var_dump($row);
							$removepositionsBox[] = $row;

						}
						
					}

				}

			}

		}

		//by company isin
		for ($z = 0; $z < $positionsboxlocalCount; $z++)
		{

			$companyisintarget = $positionsboxlocal[$z][2];
			$hits = 0;

			foreach ($positionsboxlocal as $key => $row)
			{

				if ($companyisintarget == $row[2])
				{

					if ($hits == 0)
					{
						//echo 'First!';
						//var_dump($row);
						//echo '----------------------------<br>';
						$firstrow = $row;
						$hits++;
					}
					else
					{
						//echo 'Second position isin!++<br>';
						if ($firstrow[4] > $row[4])
						{
							//var_dump($row);
							$removepositionsBox[] = $row;

						}

						
					}

				}

			}

		}

		$removepositionsBox = array_unique($removepositionsBox,SORT_REGULAR);
		//var_dump($removepositionsBox);

		$removepositionsBoxCount = count($removepositionsBox);
		$removepositionsBox = array_values($removepositionsBox);

		for ($p = 0; $p < $count; $p++)
		{

			for ($o = 0; $o < $removepositionsBoxCount; $o++)
			{

				if ($removepositionsBox[$o] == $csvdata[$p])	
				{

					echo 'Unsetting position ' . $p . ' for being a duplicate for player ' . $csvdata[$p][0] . ' in ' . $csvdata[$p][1] . ' ' . $csvdata[$p][2] . ' ' . $csvdata[$p][3] . ' ' . $csvdata[$p][4] . '<br>';
					unset($csvdata[$p]);
					break;

				}
			}
		}

		$csvdata = array_values($csvdata);
	}
}

//check if players have two positions in any company, should only be one!
if ($land == 'germany')
{
	
	//clean the data
	$count = count($csvdata);
	for ($i = 1; $i < $count; $i++) {

		//fixing the komma in number
		$csvdata[$i][3] = str_replace(",", ".", $csvdata[$i][3]);
	}
	unset($csvdata[0]);
	$csvdata = array_values($csvdata);
}

//lag alle posisjonene som objekt
//$used_list_companies = array();

$used_list_isin = array();
$used_list_companies = array();
$posisjoner = array();
$errorholder = array();

//finn sist oppdatert ved å ta første dato fra fi.se_current.csv
if (!isset($csvdata[0][4]))
{
	errorecho('Data not set! Returning!');
	//return;
	$last_updated = date('Y-m-d');
}
else
{
	$last_updated = $csvdata[0][4];
}

echo 'Newest/first position: ' . $last_updated . '<br>';

//les inn hovedlisten for å match isin mot ticker
//les inn tickerliste 
$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

$foundcache = [];
$foundcacheOutstanding = [];
$miss_box = [];
$miss_box_outstanding = [];

foreach ($csvdata as $key => $position) {

		//hopp over de som er mer enn fire år gamle!
	$fourYearsAgo = date('Y-m-d', strtotime("-4 year"));

	if ($position[4] < $fourYearsAgo)
	{
		continue;
	}

		//bygger to arrays med alle børsselskaper og isin, som senere brukes til å lage selskapslisten
	if (isset($position[1])) 
	{
		$used_list_companies[] = $position[1];
		$used_list_isin[] = $position[2];
	}
	else 
	{
		continue;
	}

		//finner isin, alternativt navn, ticker i isinlisten
	$targetname = mb_strtolower($position[1]);
	$targetisin = $position[2];

	$found_in_isin_list = 0;

	//function binary_search_multiple_hits ($isin, $name, $isin_sorted_list, $name_sorted_list, $land)

	//$firstrow = binary_search_isinliste($name, $name_liste);
	if ($row = binary_search_multiple_hits($targetisin, $targetname, $isin_list, $name_list, $land))
	{
		$altname = $row[2];
		$currency = $row[3];
		$tickerselskap = $row[1] . '.' . $row[8];
		$found_in_isin_list = 1;

		if (isset($row[9]))
		{
			$number_of_stocks = (float)$row[9] * ((float)$position[3]/100);
			$number_of_stocks = round($number_of_stocks,0);
			//echo 'Number of stocks: ' . $number_of_stocks . '<br>';
			$success = 1;
		}
		else
		{
			$obj = new stdClass;
			$obj->name = $position[1];
			$obj->ISIN = $position[2];
			$miss_box_outstanding[] = (array)$obj;		
			continue; 
		}
	}
	else
	{
		$obj = new stdClass;
		$obj->name = $position[1];
		$obj->ISIN = $position[2];
		$miss_box[] = (array)$obj;
		continue;
		
	}		

	if ($success == 1) {

		//successecho(' success! ');
		//bytt slash-strek med bindestrek i datoene
		$date_temp = $position[4];
		$dateNew = date('Y-m-d', strtotime($date_temp));

		$isActive = 'yes';
		$posisjoner[] = new posisjon($dateNew , $position[0], $position[3], $number_of_stocks, $position[2], $position[1], $isActive, $currency, $tickerselskap);
	}

}

//var_dump($miss_box_outstanding);

//var_dump($miss_box_outstanding);
echo '<br>';
$miss_box_outstanding = remove_duplicates_in_array($miss_box_outstanding);

echo '<strong><br>Outstanding shares misses:<br></strong>';
$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
$file = fopen($filename,"a");
foreach ($miss_box_outstanding as $key => $position)
{
	fputs($file, "\n" . $position['name']);
	echo $key . '. ' . $position['name'] . ' ' . $position['ISIN']  . '. Adding to misslist.csv ';
	$position['land'] = $land;
	saveJSON($position, '../production_europe/isin_adder/misslist_active/outstanding_active@' . $land . '@' . $position['name'] .  '.json');
	
}
fclose($file);

echo '<br>';
$miss_box = remove_duplicates_in_array($miss_box);

echo '<strong>Isin / name misses:<br></strong>';
$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
$file = fopen($filename,"a");
foreach ($miss_box as $key => $position)
{
	fputs($file, "\n" . $position['name']);
	echo $key . '. ' . $position['name'] . ' ' . $position['ISIN']  . '. Adding to misslist.csv ';
	$position['land'] = $land;
	saveJSON($position, '../production_europe/isin_adder/misslist_active/isin_active_csv_to_json_builder@' . $land . '@' . $position['name'] .  '.json');

}
fclose($file);


$used_list_isin = array_unique($used_list_isin);
$used_list_companies = array_unique($used_list_companies);

//filter du dupletter med ulike case på navnene!

foreach ($used_list_companies as $key => $navn)
{
	$hitcounter = 0;
	foreach ($used_list_companies as $index => $subname)
	{
		if (mb_strtolower($navn) == mb_strtolower($subname))
		{
			if ($hitcounter == 0)
			{
				$hitcounter++;
				continue;
			}
			echo 'Unsetting duplicate name: ' . $used_list_companies[$index] . '<br>';
			unset($used_list_companies[$index]);
		}
	}
}

$used_list_companies = array_values($used_list_companies);

$selskaper = array();
$posisjonsholder_unik = $used_list_companies;
$poskopi = $posisjoner;

$maincounter = 0;

//ikke match mot navn, match heller mot isin

//behandle hver unike navn
foreach ($posisjonsholder_unik as $key => $posisjon) {
	//build company 

	//Ta selskapnavnet
	$selskapnavnet = $posisjon;

	//Finn isin
	$posisjonsholder = array();

	//ta player-navnet

	$isin ='';
	$counter = 0;
	$last_updated;
	
	//var_dump($posisjon);

	foreach ($posisjoner as $index => $pos)  {

		//var_dump($pos);
		if (strtolower($pos->Name) == mb_strtolower($selskapnavnet)) {

			//echo ' hit! ' . $counter . ' ' . $selskapnavnet . ' ' ;
			$isin = $pos->ISIN;
			$currency = $pos->currency;
			//echo $isin . '<br>';
			$posisjonsholder[] = $pos;	
			$counter++;
		}

	}

	$maincounter += $counter;

	//ECHO '<BR>Hitcounter: ' . $maincounter . '<br>';

	$antall_posisjoner = count($posisjonsholder);

	//Nå har vi alle aktive posisjoner, og må regne ut summen av alle prosentene og av alle aksjene.
	(float)$accumulated_short_percent = '0';
	(float)$accumulated_short_stocks = '0';

	$ticker = '';

	foreach ($posisjonsholder as $index => $posisjon) {

		if ($index == 0)
		{
			$last_updated = $posisjon->ShortingDate;
		}
		else
		{
			if ($last_updated < $posisjon->ShortingDate)
			{
				//set new latest updated date
				//echo ' | Setting new newest position to ' . $posisjon->ShortingDate . '<br>';
				$last_updated = $posisjon->ShortingDate;
			}
		}

		(float)$accumulated_short_percent += $posisjon->ShortPercent;
		(float)$accumulated_short_stocks += $posisjon->NetShortPosition;
		$ticker = $posisjon->ticker;

	}

	$accumulated_short_percent = round($accumulated_short_percent,2);
	$accumulated_short_percent = number_format($accumulated_short_percent,2,".",",");
	//Skal selskap og posisjoner, men bare hvis det er shorting i selskapet, og bare aktive posisjoner innad i selskapet

	$selskaper[] = new selskap($isin, $selskapnavnet, $accumulated_short_percent, $accumulated_short_stocks, $last_updated, $antall_posisjoner, $posisjonsholder, $currency, $ticker);

}


//remove positions with zero positions
$count = count($selskaper);

for ($i = 0; $i < $count; $i++)
{
	//var_dump($selskaper[$i]);

	$positionscount = count($selskaper[$i]->Positions);


	if (!$positionscount > 0)
	{
		echo $i . '. Deleting position for zero positions <br>';
		unset($selskaper[$i]);
	}
}

$selskaper = array_values($selskaper);

//write current
//echo 'Writing file ' . $fileCurrent . '<br>';
saveJSON($selskaper, $fileCurrent);

//$fp = fopen($fileCurrent, 'w');
//fwrite($fp, json_encode($selskaper, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
//fclose($fp);

?>