<?php 

set_time_limit(5000);

//$land = "denmark";

//for just doing a few companies
//$land = 'norway';
require '../production_europe/functions.php';
require '../production_europe/logger.php';

//$removedates = ['2020-07-31', '2020-07-30', '2020-07-29', '2020-07-28'];
//$removedates = ['2020-08-05'];
$removedates = [];

$removeEndDate = date('Y-m-d');
$removeStartDate = '2020-08-06';

echo 'Removing dates from ' . $removeStartDate . ' til ' . $removeEndDate . '<br>';

while ($removeStartDate < $removeEndDate)
{

  //echo 'Adding ' . $removeEndDate . ' to array to be removed <br>';
  $removedates[] = $removeEndDate;
  $removeEndDate = date('Y-m-d', strtotime("-1 day", strtotime($removeEndDate)));

}

$mode = 'cleanoutzeros';
$mode = 'removedates';

$countries = ['norway', 'sweden', 'finland', 'denmark', 'germany'];

foreach ($countries as $land)
{

   $savefolder = '../shorteurope-com.luksus.no/datacalc/graphdata/' . $land . '/';



  $files = array_diff(scandir($savefolder), array('.', '..'));
 

  //gå igjennom filene og fjern datoene

  foreach ($files as $key => $file)
  {
    echo $savefolder . $file;
    $data = readJSON($savefolder . $file);

    foreach ($data as $index => $day)
    {

      if ($mode = 'removedates')
      {

        foreach ($removedates  as $date)
        {
          if ($day['date'] == $date)
          {
            unset($data[$index]);
          }
        } 
      } 
     
      if ($mode = 'cleanoutzeros')
      {

        if ($day['price'] == '')
        {
          echo 'Removing ' . $day['date'] . '<br>';
          unset($data[$index]);
        }

      }
    }

    echo  '...saved' . '<br>';

    $data = array_values($data);
    saveJSON($data, $savefolder . $file);

  }

}



?>

