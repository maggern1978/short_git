<?php

//$inputFileName = '../shorteurope-com.luksus.no/dataraw/spain.history/spain.history_current.csv';

include_once '../production_europe/functions.php';
$land = 'spain';


echo '<br>Starter...<br>';
echo 'Inputfile er ' . $inputFileName . '<br>';

$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

if (!$csvdata = readCSVkomma($inputFileName))
{
	errorecho('Read error of ' . $inputFileName . '<br>');
	logger('Read error of ' . $inputFileName, ' in spain_csv_fill_in_isin.php. Exiting!');
	return;	
}


echo 'Positions in is ' . count($csvdata) . '<br>';

$miss_box_outstanding = [];

foreach ($csvdata as $key => $csvrow)
{
	//adjust names
	if ($csvrow[2] != '')
	{
	//continue;
	}

	$targetname = strtolower($csvrow[1]);
	$targetname = str_replace(',', '', $targetname);


	if (substr($targetname,-1) == '.')
	{
		//echo 'Target name is ' . $targetname . '<br>';
		$targetname = substr($targetname,0,strlen($targetname)-1);
		//echo 'Target name is ' . $targetname . '<br>';
	}

	if ($targetname == 'solaria energia y medioambiente s.a' )
	{
		$targetname = 'Solaria Energia y Medio Ambiente S.A';
		$targetname = strtolower($targetname);
	}

	$success = 0;

	//check misscache
	$missfound = 0;

	if ($row = binary_search_multiple_hits('', $targetname, $isin_list, $name_list, 'spain'))
	{
		$csvdata[$key][2] = $row[0];
		$success = 1;
	}

	if ($success == 1)
	{
		//successecho($key . '. Found ' . $targetname . '<br>');
	}
	else
	{
		flush_start();

		$obj = new stdClass;
		$obj->name = $targetname;
		$obj->ISIN = '';
		$miss_box_outstanding[] = (array)$obj;	
		//echo $key .  '. ' . $targetname;
		//errorecho('Not found. Adding to miss_box_outstanding and unsetting. <br>');
		unset($csvdata[$key]);
		flush_end();
	}
}

//set mode
if ($mode = 'active')
{
	$prefix = '_active';
}
else
{
	$prefix = '';
}

$miss_box_outstanding = remove_duplicates_in_array($miss_box_outstanding);

echo '<strong><br>Shares misses:<br></strong>';
$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
$file = fopen($filename,"a");

foreach ($miss_box_outstanding as $key => $position)
{
	fputs($file, "\n" . $position['name']);
	echo $key . '. ' . $position['name'] . ' ' . $position['ISIN']  . '. Adding to misslist-folders ';
	$position['name'] = str_replace('/', '', $position['name']);
	
	saveJSON($position, '../production_europe/isin_adder/misslist' . $prefix . '/name@' . $land . '@' . $position['name'] .  '.json');
}

fclose($file);


$csvdata = array_values($csvdata);
echo 'Saving ' . $inputFileName . '<br>';
echo 'Positions out is ' . count($csvdata) . '<br>';

if (count($csvdata) < 3)
{
	logger('Problem i spain_csv_fill_in_isin.php. Antall posisjoner ut er 2 eller mindre. Trolig en feil med regnearket. Avbryter lagring av csv-fil.', ' Trolig feil. Exit, avbryter hele spain_builder.php ');
	exit();
	return;
}

saveCSVx($csvdata, $inputFileName);

function stripAccents($stripAccents){
  return strtr($stripAccents,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

?>