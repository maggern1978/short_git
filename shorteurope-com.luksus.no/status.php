<!DOCTYPE html>
<html lang="en">
<head>
	<title>Status Shorteurope.com</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<style>
	
	* {
		outline: 0px solid blue;  
	}	

</style>

<body>

	<div class="container">
		<div class="col-12">
			<h2 id="feiltittel">Feilmeldinger</h2>
			<div style="color: red;" id="feilbox">
			</div>
		</div>
	</div>
	<?php
	$errorBox = [];
	include '../production_europe/functions.php';
	$countries = get_countries();
	$statscounter = 0;

	?>
	<div class="container">
		<script>var errors<?php echo $statscounter; ?> = 0; </script>
		<div class="col-12">
			<div><span class="h4" id="tittel<?php echo $statscounter; ?>">Builder: </span><button class="ml-2 btn btn-secondary btn-sm" id="button<?php echo $statscounter; ?>">Vis</button></div>
			<h6>Nedlasting av posisjoner og produksjon av current.json i /dataraw/$land/. Grense: 24 timer. </h6>
			<table id="table<?php echo $statscounter; ?>" class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist oppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($countries as $key => $land)
					{
						$file = '../short_data/dataraw/' . $land . '/' . $land . '_current.json';
						$mdate = date("Y-m-d H:i:s", filemtime($file));
						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$seconds = floor($seconds_diff/60);

						if ($seconds < 24*60)
						{
							$color = 'green';
							$status = 'OK';
							echo '<script>';
							//echo 'errors0++';
							echo '</script>';							
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
							echo '<script>';
							echo 'errors0++';
							echo '</script>';
						}
						?>

						<tr>
							<td><?php echo $key; ?> </td>
							<td><?php echo $land . '_current.json';?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									$hours = floor($seconds/60);
									$minutes = $seconds % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}
								else
								{

									$hours = floor($seconds/60);
									$minutes = $seconds % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?> 
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="container mb-3">
		<div class="col-12">

			<h2>Active positions</h2>
			<h6>Shortlisten - kursoppdateringer med alpha.php. Grense: 60 minutter.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist oppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php

					foreach ($countries as $key => $land)
					{
						$file = '../short_data/datacalc/' . 'shortlisten_' . $land . '_current.csv';
						$mdate = date("Y-m-d H:i:s", filemtime($file));
						$today  = date("Y-m-d");

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$time = floor($seconds_diff/60);

						if ($time < 60)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo 'shortlisten_' . $land . '_current.csv';?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									echo $time . ' minutter siden'; 
								}
								else
								{

									$hours = floor($time/60);
									$minutes = $time % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?>
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="container">
		<div class="col-12">
			<h2>Graphdata</h2>
			<h6>Generator til production_europe/history_shorting/$land</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Land</th>
						<th class=" text-right">I dag</th>
						<th class=" text-right">I går</th>
						<th class=" text-right">-2 dager</th>
						<th class=" text-right">Eldre</th>
					</tr>
				</thead>
				<tbody>

					<?php

					foreach ($countries as $key => $land)
					{
						
						$path = '../production_europe/history_shorting/' . $land . '/';
						
						$files = array_diff(scandir($path), array('.', '..'));
						
						$today  = $today  = date("Y-m-d");
						$yesterday = date("Y-m-d", strtotime("-1 day"));
						$todaysago = date("Y-m-d", strtotime("-2 day"));
						//echo $yesterday;
						
						$todaycounter = 0;
						$yesterdaycounter = 0;
						$todaysagocounter = 0;
						$oldercounter = 0;
						$errorcount = 0;
						$totalcount = 0;
						
						
						foreach ($files as $file)
						{
							$mdate = date("Y-m-d", filemtime($path . '/' . $file));

							if ($mdate == $today)
							{
								$todaycounter++;
							}
							else if ($mdate == $yesterday)
							{
								$yesterdaycounter++; 
							}
							else if ($mdate == $todaysago)
							{
								$todaysagocounter++; 
							}
							else
							{
								$oldercounter++;
							}
							$totalcount++;
						}

						if ($todaycounter > $totalcount*0.7)
						{
							$color = 'green';
						}
						else
						{
							$color = 'red';
						}

						?>

						<tr>
							<td><?php echo ucwords($land); ?> </td>
							<td class=" text-right font-weight-bold" style="color: <?php echo $color; ?>"><?php echo $todaycounter;?> </td>
							<td class=" text-right"><?php echo $yesterdaycounter; ?></td>
							<td class=" text-right"><?php echo $todaysagocounter; ?></td>
							<td class=" text-right" style="color: red;"><?php echo $oldercounter; ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="container">
		<div class="col-12">
			<h2>Kursdata - historiske </h2>
			<h6>Nedlasting til production/history/ og outstanding aksjer</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Mappe</th>
						<th class=" text-right">I dag</th>
						<th class=" text-right">I går</th>
						<th class=" text-right">-2 dager</th>
						<th class=" text-right">Eldre</th>
					</tr>
				</thead>
				<tbody>

					<?php


					$path = '../production_europe/history/';

					$files = array_diff(scandir($path), array('.', '..'));

					$today  = $today  = date("Y-m-d");
					$yesterday = date("Y-m-d", strtotime("-1 day"));
					$todaysago = date("Y-m-d", strtotime("-2 day"));
						//echo $yesterday;

					$todaycounter = 0;
					$yesterdaycounter = 0;
					$todaysagocounter = 0;
					$oldercounter = 0;
					$errorcount = 0;
					$totalcount = 0;


					foreach ($files as $file)
					{
						$mdate = date("Y-m-d", filemtime($path . '/' . $file));

						if ($mdate == $today)
						{
							$todaycounter++;
						}
						else if ($mdate == $yesterday)
						{
							$yesterdaycounter++; 
						}
						else if ($mdate == $todaysago)
						{
							$todaysagocounter++; 
						}
						else
						{
							$oldercounter++;
						}
						$totalcount++;
					}


					$color = 'green';




					?>

					<tr>
						<td>/production_europe/history/</td>
						<td class=" text-right font-weight-bold" style="color: <?php echo $color; ?>"><?php echo $todaycounter;?> </td>
						<td class=" text-right"><?php echo $yesterdaycounter; ?></td>
						<td class=" text-right font-weight-bold" style="color: red;"><?php echo $todaysagocounter; ?></td>
						<td class=" text-right font-weight-bold" style="color: red;"><?php echo $oldercounter; ?></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>

	<div class="container">
		<div class="col-12">
			
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Mappe</th>
						<th class=" text-right">I dag</th>
						<th class=" text-right">I går</th>
						<th class=" text-right">-2 dager</th>
						<th class=" text-right">Eldre</th>
					</tr>
				</thead>
				<tbody>
					<?php

					$path = '../production_europe/json/outstanding_shares_part/';
					$files = array_diff(scandir($path), array('.', '..'));
					$today  = $today  = date("Y-m-d");
					$yesterday = date("Y-m-d", strtotime("-1 day"));
					$todaysago = date("Y-m-d", strtotime("-2 day"));
						//echo $yesterday;

					$todaycounter = 0;
					$yesterdaycounter = 0;
					$todaysagocounter = 0;
					$oldercounter = 0;
					$errorcount = 0;
					$totalcount = 0;


					foreach ($files as $file)
					{
						$mdate = date("Y-m-d", filemtime($path . '/' . $file));

						if ($mdate == $today)
						{
							$todaycounter++;
						}
						else if ($mdate == $yesterday)
						{
							$yesterdaycounter++; 
						}
						else if ($mdate == $todaysago)
						{
							$todaysagocounter++; 
						}
						else
						{
							$oldercounter++;
						}
						$totalcount++;
					}

					$color = 'green';
					$statscounter++;
					?>

					<tr>
						<td>json/outstanding_shares_part/</td>
						<td class=" text-right font-weight-bold" style="color: <?php echo $color; ?>"><?php echo $todaycounter;?> </td>
						<td class=" text-right"><?php echo $yesterdaycounter; ?></td>
						<td class=" text-right font-weight-bold" style="color: red;"><?php echo $todaysagocounter; ?></td>
						<td class=" text-right font-weight-bold" style="color: red;"><?php echo $oldercounter; ?></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>


	<div class="container">
		<div class="col-12">
			<h2>Events</h2>
			<h6>Eventes for selskaper i json/events/. Grense: 24 timer.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist oppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php
					$path = '../production_europe/json/events/company/';
					$files = array_diff(scandir($path), array('.', '..'));

					foreach ($files as $key => $file)
					{
						$file = $path . $file;
						$mdate = date("Y-m-d H:i:s", filemtime($file));

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$minutes = floor($seconds_diff/60);

						if ($minutes < 24*60)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo str_replace('../production_europe/json/events/company/', '', $file) ;?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									$hours = floor($minutes/60);
									$minutes = $minutes % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}
								else
								{
									$hours = floor($minutes/60);
									$minutes = $minutes % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?> 
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
				</tbody>
			</table>
		</div>
	</div>	

	<div class="container">
		<div class="col-12">
			<h6>Eventes for playere i json/events/. Grense: 24 timer.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist oppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php
					$path = '../production_europe/json/events/player/';
					$files = array_diff(scandir($path), array('.', '..'));

					foreach ($files as $key => $file)
					{
						$file = $path . $file;
						$mdate = date("Y-m-d H:i:s", filemtime($file));

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$minutes = floor($seconds_diff/60);

						if ($minutes < 24*60)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo str_replace('../production_europe/json/events/player/', '', $file) ;?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									$hours = floor($minutes/60);
									$minutes = $minutes % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}
								else
								{
									$hours = floor($minutes/60);
									$minutes = $minutes % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?> 
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
				</tbody>
			</table>
		</div>
	</div>	

	<div class="container">
		<div class="col-12">
			<h2>History</h2>
			<h6>Nedlasting og produksjon av history_current.json i dataraw $land . history. Grense: 24 timer.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist oppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php

					foreach ($countries as $key => $land)
					{
						$file = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.json';
						$mdate = date("Y-m-d H:i:s", filemtime($file));
						$today  = date("Y-m-d");

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$time = floor($seconds_diff/60);

						if ($time < 24*60)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo $land . '.history_current.json';?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									$hours = floor($time/60);
									$minutes = $time % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}
								else
								{

									$hours = floor($time/60);
									$minutes = $time % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?> 
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
				</tbody>
			</table>
		</div>
	</div>	


	<div class="container">
		<div class="col-12">
			<h2>Playerpositions</h2>
			<h6>Produksjon av playerpositions.json i /datacalc/players/playerpositions.$land.current.json. Grense: 60 minutter.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist opppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php

					foreach ($countries as $key => $land)
					{
						$file = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
						$mdate = date("Y-m-d H:i:s", filemtime($file));
						$today  = date("Y-m-d");

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$time = floor($seconds_diff/60);

						if ($time < 62)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo 'playerpositions.' . $land . '_current.json';?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									echo $time . ' minutter siden'; 
								}
								else
								{

									$hours = floor($time/60);
									$minutes = $time % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?>
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
					
				</tbody>
			</table>
		</div>
	</div>	

	<div class="container">
		<div class="col-12">
			<h2>Most shorted companies</h2>
			<h6>Produksjon av shorteurope-com.luksus.no/mostShortedCompanies_$land.html. Grense: 60 minutter.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist opppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php

					foreach ($countries as $key => $land)
					{
						

						$fileNameFirst = '../shorteurope-com.luksus.no/mostShortedCompanies_';
						$fileNameMiddle = $land;
						$fileNameLast = '.html';

						$file = $fileNameFirst . $fileNameMiddle  . $fileNameLast;

						$mdate = date("Y-m-d H:i:s", filemtime($file));
						$today  = date("Y-m-d");

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$time = floor($seconds_diff/60);

						if ($time < 62)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo 'mostShortedCompanies_.' . $land . '.html';?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									echo $time . ' minutter siden'; 
								}
								else
								{

									$hours = floor($time/60);
									$minutes = $time % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?>
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
					
				</tbody>
			</table>
		</div>
	</div>	


	<div class="container">
		<div class="col-12">
			<h2>O-meter companies</h2>
			<h6>Produksjon av shorteurope-com.luksus.no/o-meter_$land.html. Grense: 60 minutter.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist opppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php

					foreach ($countries as $key => $land)
					{
						

						$fileNameFirst = '../shorteurope-com.luksus.no/o-meter_';
						$fileNameMiddle = $land;
						$fileNameLast = '.html';

						$file = $fileNameFirst . $fileNameMiddle  . $fileNameLast;

						$mdate = date("Y-m-d H:i:s", filemtime($file));
						$today  = date("Y-m-d");

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$time = floor($seconds_diff/60);

						if ($time < 62)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo 'o-meter_' . $land . '.html';?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									echo $time . ' minutter siden'; 
								}
								else
								{

									$hours = floor($time/60);
									$minutes = $time % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?>
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
					
				</tbody>
			</table>
		</div>
	</div>	

	<div class="container">
		<div class="col-12">
			<h2>Tabeller</h2>
			<h6>Produksjon av tabell_$land.html . Grense: 60 minutter.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist opppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php
					foreach ($countries as $key => $land)
					{
						$fileNameFirst = '../shorteurope-com.luksus.no/o-meter_';
						$fileNameMiddle = $land;
						$fileNameLast = '.html';

						$file = $fileNameFirst . $fileNameMiddle  . $fileNameLast;

						$mdate = date("Y-m-d H:i:s", filemtime($file));
						$today  = date("Y-m-d");

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$time = floor($seconds_diff/60);

						if ($time < 62)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo 'o-meter_' . $land . '.html';?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									echo $time . ' minutter siden'; 
								}
								else
								{

									$hours = floor($time/60);
									$minutes = $time % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?>
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					?>
					
				</tbody>
			</table>
		</div>
	</div>	

	<div class="container">
		<div class="col-12">
			<h2>Detaljer_alle_body_html.html</h2>
			<h6>Produksjon av detaljer_alle_$land.html . Grense: 60 minutter.</h6>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>#</th>
						<th>Fil</th>
						<th>Status</th>
						<th>Sist opppdatert</th>
						<th>Timestamp</th>
						<th>Filstørrelse</th>
					</tr>
				</thead>
				<tbody>

					<?php
					foreach ($countries as $key => $land)
					{
						$fileNameFirst = '../shorteurope-com.luksus.no/';
						$fileNameMid = 'detaljer_alle_';
						$fileNameMiddle = $land;
						$fileNameLast = '.html';

						$file = $fileNameFirst . $fileNameMid . $fileNameMiddle  . $fileNameLast;

						$mdate = date("Y-m-d H:i:s", filemtime($file));
						$today  = date("Y-m-d");

						$ts1 = filemtime($file);
						$ts2 = strtotime("now");     
						$seconds_diff = $ts2 - $ts1;                            
						$time = floor($seconds_diff/60);

						if ($time < 62)
						{
							$color = 'green';
							$status = 'OK';
						}
						else
						{
							$color = 'red';
							$status = 'Feil';
							$errorBox[] = $file;
						}
						?>

						<tr>
							<td><?php echo $key;?> </td>
							<td><?php echo $fileNameMid . $land . '.html';?> </td>
							<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
							<td>
								<?php 

								if ($status == 'OK')
								{
									echo $time . ' minutter siden'; 
								}
								else
								{

									$hours = floor($time/60);
									$minutes = $time % 60;
									echo $hours . ' timer og ' . $minutes . ' minutter siden';
								}

								?>
							</td>
							<td><?php echo $mdate; ?></td>
							<td><?php echo human_filesize(filesize($file)); ?></td>
						</tr>
						<?php 
					}
					$statscounter++;
					    //$feilBox[] = 'feil 1';
                        //$feilBox[] = 'feil 2';
					?>
					
				</tbody>
			</table>
		</div>
	</div>	


	<div class="container">
		<div class="col-12">

		<?php 

		if (file_exists('../production_europe/error_log'))
		{



		}


		$handle = fopen('../production_europe/error_log','r');
		$requestsCount = 0;
		$num404 = 0;

		while (!feof($handle)) 
		{
		    $dd = fgets($handle);
		    $requestsCount++;   
		    $parts = explode('"', $dd);
		    $statusCode = substr($parts[2], 0, 4);
		    if (hasRequestType($statusCode, '404')) $num404++;
		}

		echo "Total 404 Requests: " . $num404 . "<br />";
		fclose($handle);

		function hasRequestType($l,$s) {
		        return substr_count($l,$s) > 0;
		}

		?>

		</div>
	</div>		


	<br><br><br>	
</body>
</html>
<script>
	var feilbox = <?php echo json_encode($feilBox);?>;
	console.log(feilbox);
	if (feilbox === null) {
    // the variable is defined
    $('#feiltittel').html('Feilmeldinger: Ingen');

}       
else
{

	if (feilbox.length > 0)
	{
		var message = '';

		for (i =0; i < feilbox.length; i++)
		{
			message += feilbox[i] + '<br>';
		}
		$('#feiltittel').html('Feilmeldinger: ' + feilbox.length);
		$('#feilbox').html(message + '<br>');
	}    
}


$(document).ready(function(){
	$("#button0").click(function(){
		$("#table0").toggle();
	});
});



if (errors0 > 0)
{
	$('#tittel0').append('<span class="text-danger"> ' + errors0 + ' feil' + '</span>');
}
else
{
	$('#tittel0').append('<span class="text-success"> ok</span>'); 
	$("#table0").hide();

}
</script>

<?php


function human_filesize($size, $precision = 2) {
	for($i = 0; ($size / 1024) > 0.9; $i++, $size /= 1024) {}
		return round($size, $precision). ' ' . ['B','kB','MB','GB','TB','PB','EB','ZB','YB'][$i];
}

?>