<?php include 'header.html'; ?>

<?php 

$country = $type = "";

require '../production_europe/namelink.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
}

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';

$landnavn = ucwords($land);
$filename = '../short_data/datacalc/shortlisten_' . $land . '_current.csv';

$csvFile = file($filename);            
$data = [];
foreach ($csvFile as $key => $line) 
{
    if ($key == 0)
    {
      continue; 
    }

    $row = str_getcsv($line); 
    $row[16] = $row[4]*$row[6]*$row[15];
    $data[] =  $row;
} 

usort($data,function($first,$second){
    return $first[16] < $second[16] ;
});

//var_dump($data);

$pageTitle = 'Most shorted companies' . ' - ' . $landnavn;
$description = 'Short positions in ' . $landnavn . ' ranked by value. ';

$runner = 0;
$totalsum = 0;


?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row ">
<div class="col-12">

<h1>Most shorted companies in <?php echo ucwords($landnavn); ?> by value</h1> 
    <a href="most_shorted_companies_all.php?country=<?php echo strtolower($landnavn); ?>" class="btn mb-3 btn-primary" role="button">Most shorted by percent 
    </a>

<table class="table table-bordered table-sm">
    <thead class="thead-dark">
      <tr>
        <th>#</th>
        <th>Name</th>
        <th class="text-right">Value</th>
        <th class="text-right">%</th>
      </tr>
    </thead>
    <tbody>
   
    <?php $counter = 1;
    foreach ($data as $key => $selskap) 
    {
   
    echo '<tr>';
    echo '<td>' . $counter  . '.</td>';
    $singleCompany   = nametolink($selskap[0]);

    echo '<td>';
    echo '<a href="details_company.php?company=' . $singleCompany  . '&land=' . $land . '">';
    echo  $selskap[0]  . '</td>';

    $value = ($selskap[4] * $selskap[6])/1000000;
    $totalsum += ($value * $selskap[15]);

    $value  = number_format(round($value,2),2,".",",");

    echo '<td class="text-right">' . $value . ' M ' . $selskap[12] .  '</td>';
  
    echo '<td class="text-right ">' . number_format(round($selskap[5],2),2,".",".") . '%</td>';
    echo '</tr>';
    $counter++;
}

echo '<tr class="font-weight-bold">';
echo '<td class="text-right">' . '</td>';
echo '<td>Sum: ' . '</td>';
echo '<td class="text-right">' . number_format($totalsum,2,".",",")  . ' M ' . $selskap[13] . '</td>';
echo '<td class="text-right">' . '</td>';
echo '</tr>';

$description = $description . 'A total of ' . $counter . ' companies are currently shorted for a value of ' . number_format($totalsum,0,".",",") . ' million ' . $currency_ticker ;

?>

    </tbody>
  </table>

    <div class="container">
        <div class="row">
           <?php
               echo "Updated: ".date("Y-m-d H:i",filemtime($filename));
            ?>
        </div>
    </div>

</div>

</div>

<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>
 </main>
  </body>
</html>