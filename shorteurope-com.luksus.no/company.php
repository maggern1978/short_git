<?php 

//https://www.investopedia.com/articles/trading/07/adx-trend-indicator.asp

require_once('../technical/kode/functions.php');

include 'header.html';
if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (!isset($_GET["country"]) or !isset($_GET["ticker"])) {
    echo 'Input error';
    return;
  }
  $country = test_input($_GET["country"]);
  $ticker = test_input($_GET["ticker"]);
}
else
{
    echo 'Input error';
    return;
}

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

    $filename = '../technical/data/rsi/' . $country . '/' . $ticker . '.rsi.json';

    if (!$data = readJSON($filename))
    {
        echo 'Error reading json';
        return;
    }

    //ADX 
    $filename_adx = '../technical/data/adx/'. $country . '/' . $ticker . '.adx.json';
    
    if(!file_exists($filename_adx))
    {
        echo 'File ' . $filename . ' adx does not exist, continuing...<br>';
        //return;
    }

    if (!$adxdata = readJSON($filename_adx)) 
    {
        echo 'File ' . $filename . ' error reading adx json, continuing...<br>';
        return;
    }

    //miks in adx
    $adxcount = count($adxdata['adx']);

        ?>

    <br>  <br>
    <div class="container mt-3">
    <div class="row">
    <div class="mx-auto">    
    <?php
	  echo '<div class="my-2 ">';
    $ticker = $data['ticker'];
    
	  include 'technical/company_bit.php';
    echo '</div>';
    
    ?>
    </div>
    </div>
    </div>
    <br>

  <?php $pageTitle = 'ShortEurope - ' . $data['name'] ; ?>
  <?php include 'footer.php'; ?>
  <?php include 'footer_about.html'; ?>

