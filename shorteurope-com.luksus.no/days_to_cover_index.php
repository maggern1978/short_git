<?php include 'header.html'; 

$playernavn = $selskapsnavn  = $land = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
}

require '../production_europe/namelink.php';

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';

$landnavn = ucwords($land);
$filename = '../short_data/datacalc/shortlisten_' . $land . '_current.csv';
$pageTitle = 'Days to cover' . ' - ' . ucwords($landnavn);

///sørg for custom id til echarts!

//$filename = 'datacalc/shortlisten_nor_current.csv';
$csvFile = file($filename);            
$data = [];
foreach ($csvFile as $line) {
    $data[] = str_getcsv($line); 
}

$selskapsnavn = array();
$days_to_cover = array();
$volume = array();
$counter = 0; 
$shortede_aksjer = array();
$short_prosent = array();

//push navn og finn days_to_cover for hvert selskap

//var_dump($data);

foreach ($data as $key => $entries) {
    if ($counter > 0 and $entries[10] != 0) {
    // echo ' ' . $key;
    // echo $entries[4] . ' ' . $entries[10] . ' <br> <br>';
    $selskapsnavn[] = $entries[0];
    $days_to_cover[] = number_format(((float)$entries[4]/(float)$entries[10]),1,".","");
    $volume[] = $entries[10];
    $shortede_aksjer[] = $entries[4];
    $short_prosent[] = $entries[5];;
    }
    //
    $counter++;
}

//sorter dataene:

array_multisort($days_to_cover, SORT_DESC, 
                $selskapsnavn,
                $volume,
                $shortede_aksjer,
                $short_prosent);

?>

<?php include 'ads/banner_720.html'; ?>

<div class="container">
  <div class="row ">
<div class="col-12">

<h1>Days-to-cover-ratio for <?php echo ucwords($landnavn); ?></h1> 
<table class="table table-bordered table-sm">
    <thead class="thead-dark">
      <tr>
        <th>#</th>
        <th>Name</th>
        <th class="text-right">Days-to-cover</th>
        <th class="text-right">Avg. volume 20 days</th>
        <th class="text-right">Stocks short</th>
        <th class="text-right">% short</th>
      </tr>
    </thead>
    <tbody>
   
    <?php $counter = 1;
    $descriptionTemp = '';

    foreach ($selskapsnavn as $key => $selskap) {
   


    echo '<tr>';
    echo '<td>' . $counter  . '.</td>';
    $singleCompany   = nametolink($selskap);

    //$singleCompany = strtoupper($singleCompany);
    $singleCompany = strtolower($singleCompany);
    $singleCompany = ucwords($singleCompany);
    echo '<td>';
    echo '<a href="details_company.php?company=' . $singleCompany  . '&land=' . $land . '">';
    
    $singleCompany = linktoname($selskap);

    echo $singleCompany . '</td>';
    echo '<td class="text-right">' . $days_to_cover[$key]  . '</td>';  
    echo '<td class="text-right">' . number_format($volume[$key],0,".",",")  . '</td>';
    echo '<td class="text-right">' . number_format($shortede_aksjer[$key],0,".",",")  . '</td>';
    echo '<td class="text-right">' . number_format($short_prosent[$key],2,".",",") . '%</td>';   
    echo '<tr>';

    if ($counter < 5) {
    $descriptionTemp = $descriptionTemp . $selskap . ', ';
    }
    if ($counter == 5) {
    $descriptionTemp = $descriptionTemp . 'and ' . $selskap . '.';
    }
    $counter++;


}//var_dump($hitsarray);

$description = 'List of days to cover for ' . $landnavn . '. A total of ' . $counter . ' companies are currently shorted. Top five companies with the highest day to cover are ' . $descriptionTemp . '';

$description = $description;

?>
    </tbody>
  </table>
  <br>
  <h3>About days-to-cover-ratio</h3>
  <p>The days-to-cover-ratio (also called short interest ratio) represents the number of days it takes short sellers on average to cover their positions, that is repurchase all of the borrowed shares. It is calculated by dividing the number of shares sold short by the average daily trading volume, generally over the last 20 trading days. The ratio is used by both fundamental and technical traders to identify trends.</p>
  <p>The days-to-cover ratio can also be calculated for an entire exchange to determine the sentiment of the market as a whole. If an exchange has a high days-to-cover ratio of around five or greater, this can be taken as a bearish signal, and vice versa.</p>
  <h3>Short squeeze (a.k.a. Bear Squeeze)</h3>
  <p>A short squeeze can occur if the price of stock with a high short interest begins to have increased demand and a strong upward trend. To cut their losses, short sellers may add to demand by buying shares to cover short positions, causing the share price to further escalate temporarily. Short squeezes are more likely to occur in stocks with small market capitalization and a small public float.</p>
  <p>Source: Wikipedia</p>
  
</div>
</div>
<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>
 </main>
  </body>
</html>
