<?php include 'header.html'; 

$country = $type = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
}

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';
$landnavn = ucwords($land);
$filename = '../short_data/heatmap_' . $land . '.html';

$pageTitle = 'Shorting analysis over time for ' . $landnavn;

$description = 'See up to the last 40 changes in short positions for all activly shorted companies in ' . $landnavn . '.' ;

include 'ads/banner_720.html';
include $filename;

?>

    <div class="container">
        <div class="row">
           <?php
               echo "Updated: ".date("Y-m-d H:i",filemtime($filename));
            ?>
        </div>
    </div>

</div>
</div>

<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>
 </main>
  </body>
</html>