<?php

include 'header.html';
require '../production_europe/logger.php';
require '../production_europe/functions.php';
require_once('../production_europe/isopen.php');
require '../production_europe/namelink.php';
$development = 1;

date_default_timezone_set('Europe/Oslo');

$development = 1;
$land = 'sweden';

if (!isopen($land))
{
	echo $land . ' is closed today, returning.<br> ';
	return;
}

echo '<br>';

switch ($land) {
    case "norway":
        break;
    case "sweden":
        break;
    case "denmark":
        break;
    case "finland":
        break;
  	default:
    return;
}

if ($json = readJSON('../production_europe/json/events/' . $land . '/events.' . $land . '.current.json'))
{
	echo 'File ' . '../production_europe/json/events/' . $land . '/events.' . $land . '.current.json' . ' read successfully<br>';
}
else 
{
	errorecho('File ' . '../production_europe/json/events/' . $land . '/events.' . $land . '.current.json' . ' failed reading, returning...<br>');
	return;
}

if (count($json) == 0)
{
	echo 'Count is zero, returning...';
	return;
}

//Get all the company names
$companyNameContainer = [];
for ($i = 0, $jsoncount = count($json); $i < $jsoncount; $i++)
{
	$companyNameContainer[] = $json[$i]['companyName'];
}

$companyNameContainer = array_unique($companyNameContainer);
$companyNameContainer = array_values($companyNameContainer);

$allContainer = [];

for ($i = 0, $count = count($companyNameContainer); $i < $count; $i++)
{
	$localPositionsHolder = [];

	for ($x = 0; $x < $jsoncount; $x++)
	{

		if ($companyNameContainer[$i] == $json[$x]['companyName'])
		{
			$localPositionsHolder[] = $json[$x];
		}
	}

	$allContainer[] = $localPositionsHolder;

}

$rankingcontainer = [];
$shortDate = '';

foreach ($allContainer as $key => $company)
{
	$shortpositionChangePercent = 0;
	$companyName = '';
	$companyShortPercentTotal = 0;
	$positionStartDate = '';

	foreach ($company as $position)
	{

		if ($position['status'] == 'new')
		{
			$shortpositionChangePercent += $position['shortPercent'];
		}
		else if ($position['status'] == 'updated')
		{
			$shortpositionChangePercent += $position['shortPercent'] - $position['oldposition']['shortPercent'];
		}
		else if ($position['status'] == 'deleted')
		{
			$shortpositionChangePercent -= $position['shortPercent'];
		}
		
		$companyName = $position['companyName'];
		$companyShortPercentTotal = $position['shortSumCompany'];
		$positionStartDate = $shortDate = $position['positionStartDate'];
	}

	$object = new stdClass;
	$object->companyName = $companyName;
	$object->companyShortChangeTotal = $shortpositionChangePercent;
	$object->companyShortPercentTotal = $companyShortPercentTotal;
	$object->positionStartDate = $positionStartDate;

	$rankingcontainer[] = (array)$object;

	//echo $key . '. ' . $companyName . ': ' . $shortpositionChangePercent . '% (' .  $companyShortPercentTotal .'% total)<br>';
}

//sorting list before save
usort($rankingcontainer, function($a, $b) {
    return $a['companyShortChangeTotal'] < $b['companyShortChangeTotal'];
});

//var_dump($rankingcontainer);

echo '<br>';

//check if date is minus 1 working day from today
$publisheddate = date( "Y-m-d", strtotime("+1 Weekday", strtotime($shortDate)));
echo 'Position dates = ' . $shortDate . ', which means published date ' . $publisheddate . '<br>';
echo 'If publisheddate is today, then continue<br>';

//dev! todo
//$publisheddate = '2020-07-19';

if (date('Y-m-d') != $publisheddate)
{
	errorecho('Publisheddate is not today, returning....<br>');
	
	//dev todo
	//return;
}
else 
{
	successecho('Publisheddate is today, continuing....<br>');
}


if ($development == 0)
{
	ob_start();
}

?>

<div class="container mb-3">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-6 col-lg-6 mb-2">
		<h3>Newest short changes to companies</h3>
		<table class="table table-striped table-hover table-bordered table-sm table-most-shorted">
		    <thead class="thead">
		      <tr>
		        <th>#</th>
		        <th>Company name</th>
		        <th class="text-right most-shorted-table-heading-share">Change</th>
		        <th class="text-right most-shorted-table-heading-a-week-ago" data-toggle="tooltip" data-placement="top" title="" data-original-title="">Total short</th>
		      </tr>
		    </thead>
		    <tbody>
		    <?php 

		    foreach ($rankingcontainer as $key => $company)
		    {
		    	echo '<tr>';
		    	echo '<td>';
		    	echo $key + 1;
		    	echo '.</td>';
		    	echo '<td>';

		    	$linkcompany = nametolink($company['companyName']);
		    	echo '<a data-toggle="tooltip" title="See all active positions for company" href="';
				echo 'details_company.php?company=' . $linkcompany . '&land=' . $land;
		    	echo '">';
		    	echo $company['companyName'];
		    	echo '</a>';
		    	echo '</td>';
		    	echo '<td class="text-right">';

		    	if ($company['companyShortChangeTotal'] > 0)
		    	{
		    		echo '+';
		    	}

		    	echo $company['companyShortChangeTotal'];
		    	echo ' %</td>';
		    	echo '<td class="text-right">';
		    	echo $company['companyShortPercentTotal'];
		    	echo ' %</td>';
		    	echo '</tr>';
		    }
		    ?>
		    </tbody>
		    </table>
		    <span class="float-right">Updated with positions published <?php echo $publisheddate; ?> </span>
		</div>
	</div>
</div>

<?php 


if ($development == 0)
{

	$htmlStr = ob_get_contents();

	// Clean (erase) the output buffer and turn off output buffering
	ob_end_clean(); 

	// Write final string to file
	file_put_contents('../production_europe/html/events.companychanges/' . $land . '.events.companychanges.' . 'current.html', $htmlStr);
}

?>