<?php include 'header.html'; 

//$playernavn = $fil_teller = $i = "";
$country = "";

require '../production_europe/namelink.php';
require '../production_europe/functions.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $country = $land = test_input($_GET["country"]);
}

$land_navn = ucwords($land);
$allPositions = '../short_data/dataraw/stock.prices/' . $land . '/players/players.index.json';

$string = file_get_contents($allPositions);
if (!$selskapsposisjoner = json_decode($string, true))
{
  echo 'Read error.<br>';
  return;
}

include '../production_europe/input_check_country.php';

$pageTitle = 'All short positions, active and historical, sorted by players in ' . ucwords($land_navn);
$description = 'Look into thousands of short positions by player and by company. Se excatly how much a player has earned over several years. '

?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row">
    <div class="col-12">
      <?php 
      echo '<h1>All short players in '. ucwords($land_navn) .'</h1>';
      echo '<p>Current and historical positions</p>';
      ?>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-12 col-sm-6">
    
        <?php
        $count = count($selskapsposisjoner)/2;

        $turn = 0;
        foreach ($selskapsposisjoner as $key => $selskap) {
         $selskapsnavnNew  = strtolower($selskap['PositionHolder']);
         $selskapsnavnNew  = ucwords($selskapsnavnNew);
         $nameNew = nametolink($selskapsnavnNew);

         echo '<div class="mb-1"><a href="' . 'history_player.php?player=' . 
         $nameNew . '&land=' . $land . '">';
         echo $selskapsnavnNew  . '</a> ';

         if ($selskap['numPositions'] == 1)
         {
           echo '(' . $selskap['numPositions'] . ' position)' ;
         }
         else if ($selskap['numPositions'] > 1)
         {
           echo '(' . $selskap['numPositions'] . ' positions)' ;
         }

         echo '</div>';
         if ($count < $key || $turn == 0) 
         {
          //echo '</p>';
          echo '</div>';
          echo '<div class="col-6">';
          //echo '<p>';
        }
      }

      ?>
  
  </div>


</div>
</div>
</div>

<?php include 'footer.php'; ?>