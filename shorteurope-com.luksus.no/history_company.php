<?php include 'header.html'; 

//$land = 'nor';

//$selskapsnavn = $fil_teller = $i = "";
$selskapsnavn  = $land = "";

require '../production_europe/namelink.php';
require '../production_europe/functions.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $selskapsnavn = test_input($_GET["selskapsnavn"]);
  $land = test_input($_GET["land"]);
}

include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';
include '../production_europe/logger.php';

$graphFolder = '../short_data/datacalc/graphdata/' . $land . '/';

set_time_limit(20);

//$allPositions = '../short_data/dataraw/stock.prices/' . $land . '/all_positions_' . $land . '.json';

$selskapsnavn = linktoname($selskapsnavn);
//get file location
$firsttwoletters = getFirstTwoLetters($selskapsnavn);
$filename = $firsttwoletters . '.json';

if (!isset($firsttwoletters[0]) or !isset($firsttwoletters[1]) or !$selskapsposisjoner = readJSON('../short_data/dataraw/stock.prices/' . $land . '/companies/' . $firsttwoletters[0] . '/' . $firsttwoletters[1] . '/' . $filename))
{
  ?>
<div class="container">
  <div class="row">
    <div class="col-12">
    <?php  
  echo 'No positions found for ' . ucwords($selskapsnavn) .  ' in ' . ucwords($land) . '.<br>';
  ?>
  </div>
  </div>
  </div>
  <?php
  return;
}


$land_navn = ucwords($land);

$pageTitle = $selskapsnavn . ' - ' . $land_navn;
$success = 0;

$land_navn = str_replace('_',' ', $land);
$land_navn = ucwords($land_navn);

?>

<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row">
    <div class="col-12">
      <?php 
      $selskapsnavnNew  = strtolower($selskapsnavn);
      $selskapsnavnNew  = ucwords($selskapsnavnNew);

      ?>
     

      <?php  $totalSum = 0;
      $positionsBox = array();
      $ticker = '';
      $countPos = 0;

      $totalmeta = 0;

      foreach ($selskapsposisjoner as $key => $selskap) 
      {

        if (strtoupper($selskap['StockName']) == strtoupper($selskapsnavn))  
        {
          ?>

           <h2><?php echo $selskapsnavnNew; ?> - history</h2>
      Country: <?php echo ucwords($land); ?>. Ticker: <?php echo mb_strtoupper($selskap['ticker']); ?><br><br> Summary for all available positions:<br>

          <?php


          $ticker = $selskap['ticker'];
          
          foreach ($selskap['summary'] as $player)
          {

            $totalSum += $player['SumOfAllValueChangeForPlayer'];

            $selskapNew  = strtolower($player['PositionHolder']);
            $selskapNew  = ucwords($selskapNew);

            $singleCompany   = nametolink($selskapNew);
            $singleCompany = strtoupper($singleCompany);

            $historyLink = 'history_player.php?player=' . $singleCompany . '&land=' . $land;

            $link = 'details.php?player=' . $singleCompany .  '&land=' . $land;

            if (!isset($selskapsposisjoner[$key]['positions'][0]))
            {
              logger('Error in history_company.php' ,  $singleCompany . ' ' . $land . ' ' . $selskap['StockName']);
              continue;
            }


            $basecurrency = $selskapsposisjoner[$key]['positions'][0]['basecurrency'];
            $currency = $selskapsposisjoner[$key]['positions'][0]['currency'];
            $base_currency_multiply_factor = $selskapsposisjoner[$key]['positions'][0]['base_currency_multiply_factor'];                  

            if ($player['SumOfAllValueChangeForPlayer'] > 0) 
            {
             echo '<a href="' . $link . '">';
             echo $selskapNew . '</a>';
             echo ' earned <span class="text-success"><strong> ' . number_format(($player['SumOfAllValueChangeForPlayer']/1000000),2,".",",");
             echo ' million ' .  $currency . '</strong></span>';

           }
           elseif ($player['SumOfAllValueChangeForPlayer'] == 0) 
           {
             echo '<a href="' . $link . '">';
             echo $selskapNew . '</a>';
             echo 'Result is <strong> ' . number_format($player['SumOfAllValueChangeForPlayer']/1000000,2,".",",");
             echo ' million ' . $currency . '</strong>';
           }
           else 
           {
             echo '<a href="' . $link . '">';
             echo $selskapNew . '</a>';
             echo ' lost <span class="text-danger"><strong> ' . number_format($player['SumOfAllValueChangeForPlayer']/1000000,2,".",",");
             echo ' million ' . $currency . '</strong></span>';
           }
           echo '<a href="' . $historyLink. '">' . ' (full history)' . '</a><br>';
         }

        //echo $name . ' ' . $selskap['SumOfAllValueChangeForPlayer'] . '<br>';
         foreach ($selskap['positions'] as $index => $position) 
         {
          $positionsBox[] = $position;
          $countPos++;
        }
      }
    }

    $totalmeta = $totalSum;

    if(!isset($currency))
    {
      $currency = '';
    }

    if ($totalSum > 0) {
      echo '<br><h5>Total sum: ' . number_format($totalSum/1000000,2,".",",") . ' million ' . 
      $currency. ' earned in '. $countPos . ' positions.</h5>';

    }
    else {
      echo '<br><h5>Total sum: ' . abs(number_format($totalSum/1000000,2,".",",")) . ' million ' . 
      $currency . ' lost in '. $countPos . ' positions.</h5>';
    }

    ?>
  </div>
</div>
</div>
<?php

$description = 'Historical short positions for ' . $selskapsnavnNew . '. ' ;

if ($totalSum > 0) {
 $description = $description . 'In total shorters earned ' . abs(number_format($totalSum/1000000,2,".",",")) . ' millon ' . $currency . ' in '  . $countPos . ' positions. ';

}
else {
  $description = $description . 'In total shorters lost ' . abs(number_format($totalSum/1000000,2,".",",")) . ' millon ' . $currency . ' in '  . $countPos . ' positions. ';
}

$ticker  = str_replace(" ","-",$ticker,$count);

if ($ticker == '') {
  //echo'Graph data not found'; 
  return;
}


?>

<div class="container">
  <div class="row">
    <div class="col-12">
      <?php include '../production_europe/company_graph.php'; ?>
    </div>
  </div>
</div>
</div>

<div class="container my-4">
  <div class="row">
    <div class="col-12">
      <table class="table table-striped">
        <thead class= "thead-dark">
          <tr>
            <th scope="">#</th>
            <th scope="col" style="min-width: 110px;">From</th>
            <th scope="col" style="min-width: 110px;">To</th>
            <th scope="col" class="text-right" style="min-width: 90px;">Short-%</th>
            <th scope="col" class="text-right">StartPrice</th>
            <th scope="col" class="text-right">EndPrice</th>
            <th scope="col" class="">Player</th>
            <th scope="col" class="text-right">Change</th>
            
          </tr>
        </thead>

        <?php 

        echo '<tbody>';
        $counter = 1;

        foreach ($positionsBox as $key => $posisjon) {

          $switch = 0;
          if ($posisjon['isActive'] == 'yes') {
            echo '<tr class="bg-yellow">';
            $switch = 1;
          }
          else {
            echo '<tr>';
            $switch = 0;
          }

          echo '<td>' . $counter . '.</td>';
          echo '<td>' . $posisjon['ShortingDateStart'] . '</td>';
          echo '<td>' . $posisjon['ShortingDateEnd'];

          if ($switch == 1) {
            echo ' (ongoing)';
                  //var_dump($posisjon);
          }
          echo '<td class="text-right">' . $posisjon['ShortPercent']. '</td>';
          echo '</td>';
          
          if (is_numeric($posisjon['ShortingDateStartStockPrice']))
          {
            echo '<td class="text-right">' . number_format($posisjon['ShortingDateStartStockPrice'],2,".",","). '</td>';
          }
          else
          {
            echo '<td class="text-right">-</td>';
          }
          
          if (is_numeric($posisjon['ShortingDateEndStockPrice']))
          {
              echo '<td class="text-right">' . number_format($posisjon['ShortingDateEndStockPrice'],2,".",","). '</td>';
          }
          else
          {
            echo '<td class="text-right">-</td>';
          }

        
          
          $navnNew  = $posisjon['PositionHolder'] ;
          $navnNew = strtolower($navnNew);
          $navnNew  = ucwords($navnNew );
          echo '<td class="">' . $navnNew . '</td>';

          if (is_numeric($posisjon['valueChange']))
          {
            echo '<td class="text-right text-nowrap">' . number_format($posisjon['valueChange']*-1/1000000,2,".",","). ' M ' . $posisjon['currency'] . '</td>';
          }
          else
          {
             echo '<td class="text-right text-nowrap">-</td>';
          }
         

          echo '</tr>';
          $counter++;

          
          ?>

          <?php } 
          echo '</tbody>';
          echo '</table>';
          ?>
        </div>
      </div>
    </div>

    <?php include 'footer.php'; ?>
    <?php include 'footer_about.html'; ?>

  </main>

