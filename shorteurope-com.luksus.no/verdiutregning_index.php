<?php include 'header.html'; 

?>

<?php 

//$playernavn = $fil_teller = $i = "";
$country = "";

require '../production_europe/namelink.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $country = test_input($_GET["country"]);
}

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

set_time_limit(500);

switch ($country) {
    case "norway":
        $fil = '../short_data/dataraw/ssr.finanstilsynet/finanstilsynet_current.json';
        $shortliste = '../short_data/datacalc/shortlisten_nor_current.csv';
        $currency_ticker = 'NOK';
        $land_navn = 'Norway';
        $land = 'nor';
        $stockPricesUrl = '../short_data/dataraw/stock.prices/nor/nor.json/';
        $saveUrl = '../short_data/dataraw/stock.prices/nor/';
        $mapUrl = '../short_data/dataraw/stock.prices/nor/positions_map_nor.json';
        $allPositions = '../short_data/dataraw/stock.prices/nor/all_positions_nor.json';
        break;
    case "sweden":
        $fil = 'dataraw/fi.se/fi.se_current.json';
        $shortliste = 'datacalc/shortlisten_swe_current.csv';
        $currency_ticker = 'SEK';
        $land_navn = 'Sweden';
        $land = 'swe';
        $stockPricesUrl = './dataraw/stock.prices/swe/';
        $saveUrl = './dataraw/stock.prices/swe/';
        $mapUrl = './dataraw/stock.prices/swe/positions_map_swe.json';
        $allPositions = './dataraw/stock.prices/swe/all_positions_swe.json';
        break;
    case "denmark":
        $fil = 'dataraw/denmark/denmark_current.json';
        $shortliste = 'datacalc/shortlisten_dkk_current.csv';
        $currency_ticker = 'DKK';
        $land_navn = 'Denmark';
        $land = 'dkk';
        $stockPricesUrl = './dataraw/stock.prices/dkk/';
        $saveUrl = './dataraw/stock.prices/dkk/';
        $mapUrl = './dataraw/stock.prices/dkk/positions_map_dkk.json';
        $allPositions = './dataraw/stock.prices/dkk/all_positions_dkk.json';
        break;
    case "fin":
        $land = "fin";
        break;
    case "is":
        //echo "is";
        break;
    default:
        echo 'Land not set!';
        return;
}

$string = file_get_contents($mapUrl);
$map = json_decode($string, true);

$pageTitle = 'All short positions, active and historical, sorted by players in ' . $land_navn;
$description = 'Look into thousands of short positions by player and by company. Se excatly how much a player has earned over several years. '

?>

<?php include 'header_ad.html'; ?>

<div class="container">
    <div class="row">
        <div class="col-12">
<?php 
echo '<h1>All short players in '. $land_navn .'</h1>';
echo '<p>Current and historical positions</p>';
?>
</div>
</div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-6">
<?php

$map_keys = array_keys($map);

$antall =  count($map_keys);
echo '<p>';
foreach ($map_keys as $index => $key) {
	
	$x =  count($map[$key]);
	
	$playernavn = $key;

	$playernavnx = nametolink($playernavn);

	echo  '<a href="verdiutregning_vis.php?player=' . strtoupper($playernavnx) . '&selskapsnavn=x&land=' . $country . '">';
	
	$playernavn  = strtolower($playernavn);
	$playernavn  = ucwords($playernavn);

	if ($x > 1) {
		echo '<span class="font-weight-bold">' . $playernavn . ':</span> ' ;
		echo $x . ' companies<br>';
	}
	else {
		echo '<span class="font-weight-bold">' . $playernavn . ':</span> ' ;
		echo $x . ' company<br>';
	} 

	

	$index_holder = $index;
	if ($antall/2 <= $index )
	{
		break;
	}

}
echo '</p>';

?>
</div>

        <div class="col-12 col-sm-6">
<?php 

//echo $antall . ' ' . $index_holder;

$restantall = $antall - $index_holder;

//echo ' ' . $restantall;

//echo '----------';
echo '<p>';
for ($i = $restantall+2; $antall > $i; $i++) {
	
	//var_dump($map);
	$x =  count($map[$map_keys[$i]]);
	
	$playernavn = $map_keys[$i];

	$playernavnx = nametolink($playernavn);

	echo  '<a href="verdiutregning_vis.php?player=' . strtoupper($playernavnx) . '&selskapsnavn=x&land=' . $country . '">';

	$playernavn  = strtolower($playernavn);
	$playernavn  = ucwords($playernavn);

	if ($x > 1) {
		echo '<span class="font-weight-bold">' . $playernavn . ':</span> ' ;
		echo $x . ' companies<br>';
	}
	else {
		echo '<span class="font-weight-bold">' . $playernavn . ':</span> ' ;
		echo $x . ' company<br>';
	} 


}
	echo '</p>';



//var_dump($map_keys);
?>
</div>
</div>
</div>

<?php include 'footer.php'; ?>