<?php

include_once('../code/functions.php');

$listfiles = listfiles('../code/data/ticker_list/');

if (count($listfiles) > 1)
{
	//echo 'Error, too many files in data/ticker_list. Returning!';
	return;
}

if (!$tickers = readCSVtab('../code/data/ticker_list/' . $listfiles[0]))
{
	echo 'Error reading ticker list!';
	exit();
}

$bulkTickerData = [];

$all_count = count($tickers);

//Kode,Navn,Kallenavn
foreach ($tickers as $key => $ticker)
{
	

	$length = count($ticker);


	for ($i = 0; $i < $length; $i++)
	{
		$ticker[$i] = trim($ticker[$i]);
		
	}

	$bulkTickerData[] = $ticker;
}

//echo separate bonds

$bondsTickerData = [];

foreach ($bulkTickerData as $key => $ticker)
{
	
	if (isset($ticker[2]) and strripos($ticker[0], 'GBOND') !== false )
	{
		$bondsTickerData[] = $ticker;
		unset($bulkTickerData[$key]);
	}

}

$bulkTickerData = array_values($bulkTickerData);

include '../public_html/header.html';

?>
<div class="container mt-3">
	<div class="row">
		<div class="col-12 text-center border-bottom d-none d-md-block">
			<h1>Markets</h1>
		</div>
		<div class="col-12 d-block d-md-none">
			<h1>Markets</h1>
		</div>		
	</div>
</div>


<div class="container-lg ">
<div class="mx-auto">
	<div class="row">
	
		<div clasS="col-12 col-sm-12 col-md-6 col-lg-5 ml-auto">
			<div class="d-flex flex-column">
				<?php

				$column_switch = 0;
				$column_switch_done = 0;

				//var_dump($bulkTickerData);

				foreach ($bulkTickerData as $key => $ticker)
				{
					
					//echo $key . '<br>';

					$anwer = displayticker_list($ticker, $bulkTickerData);

					//echo $anwer . '<br>';

					if ($column_switch == 1 and $anwer == 'heading' and $column_switch_done == 0)
					{
						$column_switch_done = 1;

						echo '</div>';
						echo '</div>';
						echo '<div clasS="col-12 col-sm-12 col-md-6 col-lg-5 mr-auto"><div class="d-flex flex-column">';

					}

					if ($key > $all_count/2 and $column_switch_done == 0)
					{

						$column_switch = 1;

					}
				}

				foreach ($bondsTickerData as $ticker)
				{
					displayticker_new_bonds($ticker,2);
				}

				?>

			</div>
		</div>
	</div>
	</div>
</div>
</div>		
	<br><br><br>
</body>
</html>

<?php


function displayticker_new_bonds($target, $decimals)
{

	if(!file_exists('../code/data/tickers/' . mb_strtolower($target[0])  . '.json'))
	{
		return;
	}

	if(!$tickers = readJSON('../code/data/tickers/' . mb_strtolower($target[0])  . '.json'))
	{
		return;
	}

	$count = count($tickers);

	$latest = $tickers[$count-1];
	$previous = $tickers[$count-2];
	
	$diff = $latest['close'] - $previous['close'];

	if ( $diff > 0)
	{
		$subfix = 'plus';
	}
	else if ($diff == 0)
	{
		$subfix = 'zero';
	}	
	else
	{
		$subfix = 'minus';
	}		

	$diff_percent = ($diff/$previous['close']);

	?>
	<div class=" <?php echo '' ?> border-bottom">
		<div class="d-flex justify-content-between">
			<div style="min-width: 100px;"  class="ticker-text ticker-name mr-2 mt-1 mb-1 font-weight-bold"><span data-placement="top" data-toggle="tooltip" title="<?php echo $target[1]; ?>"><?php echo $target[2];?></span>
			</div>
			<div class="d-flex justify-content-end">
				<div style="min-width: 59px;"  class="ticker-text text-right ticker-close mr-2 mt-1 mb-1 "><span data-html="true" data-toggle="tooltip" title="Updated <?php echo $latest['date'];?>"><?php echo numb_format(round($latest['close'],3),3);?></span>
				</div>			
				<div style="min-width: 55px;"  class="ticker-text text-right ticker-close mr-2 mt-1 mb-1 "><span data-html="true" data-toggle="tooltip" title="<?php echo $diff . '. Updated:<br>'. $latest['date'];?>"><?php echo numb_format(round($diff,3),3);?></span>
				</div>
				<div class="d-flex" style="min-width: 55px;">	
					<div  class="text-right ticker-change mt-1 mb-1 ticker-change-<?php echo $subfix;?> "><span data-html="true" data-toggle="tooltip" title="<?php echo round($diff_percent,4) . '%. Updated:<br>'. $latest['date'];?>"><?php echo numb_format(round($diff_percent ,2),$decimals);?>%</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php 
	
}

function displayticker_new($target, $decimals)
{

	if(!file_exists('../code/data/tickers/' . mb_strtolower($target[0]) . '.json'))
	{
		return;
	}

	if(!$ticker = readJSON('../code/data/tickers/' . mb_strtolower($target[0]) . '.json'))
	{
		return;
	}

	
	if ($ticker['change_p'] > 0)
	{
		$subfix = 'plus';
	}
	else if ($ticker['change_p'] < 0)
	{
		$subfix = 'minus';
	}		
	else if ($ticker['change_p'] == 0)
	{
		$subfix = 'zero';
	}		

	if ($ticker['close'] > 10000)
	{
		$decimals = 0;
	}
	else if ($ticker['close'] > 5000)
	{
		$decimals = 1;
	}

	?>
	<div class=" <?php echo '' ?> border-bottom">
		<div class="d-flex justify-content-between">
			<div style="min-width: 100px;"  class="ticker-text ticker-name mr-2 mt-1 mb-1 font-weight-bold"><span data-placement="top" data-toggle="tooltip" title="<?php echo $target[1]; ?>"><?php echo $target[2];?></span>
			</div>
			<div class="d-flex justify-content-end">
				<div style="min-width: 59px;"  class="ticker-text text-right ticker-close mr-2 mt-1 mb-1 "><span  data-toggle="tooltip" title="Updated <?php echo $ticker['timestamp'];?>"><?php echo numb_format(round($ticker['close'],$decimals),$decimals);?></span>
				</div>
				<div style="min-width: 55px;"  class="ticker-text text-right ticker-close mr-2 mt-1 mb-1 "><span data-html="true"  data-toggle="tooltip" title="<?php echo $ticker['change'] . '. Updated:<br>' . $ticker['timestamp'];?>"><?php echo numb_format(round($ticker['change'],$decimals),$decimals); ?></span>
				</div>					
				<div class="d-flex" style="min-width: 55px;">
					<div class="text-right ticker-change mt-1 mb-1 ticker-change-<?php echo $subfix;?> "><span data-html="true" data-toggle="tooltip" title="<?php echo $ticker['change_p'] . '%. Updated:<br>' . $ticker['timestamp'];?>"><?php echo round($ticker['change_p'],2);?>%</span>
					</div>	
				</div>
			</div>
		</div>
	</div>
	
	<?php 
	
}

function numb_format($number, $decimals)
{

	return number_format($number,$decimals, '.',',');
}

function displayticker_list($ticker, $list)
{

	if ($ticker[0] == '')
	{
		return 'heading';
	}
	
	if (!isset($ticker[2]) or !isset($ticker[1]) or !isset($ticker[0]) or mb_strtolower($ticker[0]) == 'kode' or mb_strtolower($ticker[1]) == 'navn' or mb_strtolower($ticker[2]) == 'kallenavn' )
	{
		return 'heading';	
	}

	if (!isset($ticker[1]) or $ticker[1] == '' or !isset($ticker[2]) or $ticker[2] == '')
	{

		if (isset($ticker[0]) and $ticker[0] != '')
		{
			display_title($ticker);
			return 'title';
		}


	}


	displayticker_new($ticker,2);
	return 'ticker';
	
}

function display_title($entry)
{
	echo '<div class="mt-3"></div>';
	
	if ($pos = strripos($entry[0], "USA") !== false)
	{
		$string = ucwords(mb_strtolower($entry[0]));
		$string = str_replace("Usa", "USA", $string);
		echo '<h2>' . $string   . '</h2>';

	}
	else if ($pos = strripos($entry[0], "U.S.") !== false)
	{
		$string = ucwords(mb_strtolower($entry[0]));
		$string = str_replace("U.s.", "U.S.", $string);
		echo '<h2>' . $string   . '</h2>';

	}	
	else
	{
		echo '<h2>' . ucwords(mb_strtolower($entry[0])) . '</h2>';
	}

	
}


function readCSVtab($file_location_and_name) {

	$csvFile = file($file_location_and_name);

        //Les in dataene 
	$name = [];
	foreach ($csvFile as $line) {
		$name[] = str_getcsv($line, '	');
	}

        //ordne utf koding
	$counter = 0;
	foreach ($name as &$entries) {
		$data[$counter] = array_map("utf8_encode", $entries);
		$counter++;
	}
	return $name;
}

?>