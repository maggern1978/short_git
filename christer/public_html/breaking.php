<?php 

//christer
//christer2021
//@todo hent mer info fra tweet med video så video kan integreres i siten. 
//@todo hent inn full tekst fra refrerte tweeter
//todo legg inn linker der det finnes
//hvis video ikke laster, lag preview i stedet
//http://localhost/christer/public_html/index.php?start=00&show=10&name=sjosephburns

include_once '../code/functions.php';

if (isset($_GET['start']))
{
	$start = htmlspecialchars($_GET["start"]);
}
else
{
	$start = 0;
}

$url_string = 'start=' . $start; 

if (isset($_GET['show']) and is_numeric($_GET['show']))
{
	$show = htmlspecialchars($_GET["show"]);
}
else
{
	$show = 25;
}

if (isset($_GET['type']))
{
	$type = htmlspecialchars($_GET["type"]);
}
else
{
	$type = 'tweets';
}

$url_string .= '&show=' . $show;

$source = 'breaking';


	if (!$tweets_file = new SplFileObject('../code/data/displaylist/breaking_display_list_tweets.csv'))
	{
		echo 'Could not read user tweets list<br>';
		$file = false;
	}

	if (!$replies_file = new SplFileObject('../code/data/displaylist/breaking_display_list_replies.csv'))
	{
		echo 'Could not read user replies list<br>';
		$file = false;
	}

	if (!$media_file = new SplFileObject('../code/data/displaylist/breaking_display_list_media.csv'))
	{
		echo 'Could not read media tweets list<br>';
		$file = false;
	}		




$stop = $start + $show; 

$tweets_data = tweet_reader($tweets_file, $start, $stop);
$replies_data = tweet_reader($replies_file, $start, $stop);
$media_data = tweet_reader($media_file, $start, $stop);

//var_dump($tweets_data);
//var_dump($replies_data);
//var_dump($media_data);

$user_list = [];

if (!$user_list = readJSON('../code/data/allusers_search_array.json'))
{
	//echo 'Could not read breaking_display_list<br>'; //will continue without
	//$user_list = [];
}

if (!$file = new SplFileObject('../code/data/displaylist/user_display_list_tweets.csv'))
{
	//echo 'Could not read user_display_list<br>';
	$file = false;
}


if ($file != false)
{
	$breaking_tweets_list = tweet_reader($file, 0, 100);
}
else
{
	//tweet_reader($file, $start, $stop, $show);
	$breaking_tweets_list = [];
}

include 'header.html';
include 'ticker_line.html';
?>

<div class="container-fluid mt-3">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-xl-7 mx-auto">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-8 col-md-8 col-xl-7">
					<div class="d-flex justify-content-between ">
						<div class="menu-font menu-div flex-grow-1 text-center">
							<div class="d-flex ">
								<div id='menu_tweets' class="p-2 mx-auto <?php if ($type == 'tweets') { echo 'menu-active';} ?> "><span class="font-weight-bold">Tweets</span></div>
							</div>
						</div>
						<div class="menu-font menu-div flex-grow-1 text-center">
							<div class="d-flex">
								<div  id='menu_tweetsandreplies' class="p-2 mx-auto <?php if ($type == 'reply') { echo 'menu-active';} ?>"><span class="font-weight-bold">Tweets & replies</span></div>
							</div>
						</div>
						<div class="menu-font menu-div flex-grow-1 text-center">
							<div class="d-flex">
								<div id='menu_media' class="p-2 mx-auto <?php if ($type == 'media') { echo 'menu-active';} ?>"><span class="font-weight-bold">Media</span></div>
							</div>
						</div>
					</div>

					<br>
					<div class="d-flex flex-column"><?php $stylestring = ' style="display: none;"'; ?>
						<div class="tweets " <?php if ($type != 'tweets') { echo $stylestring;}else { echo ' style="display: block"';} ?> >

							<?php


							$user_info_cache = [];

							foreach ($tweets_data as $key => $tweet)
							{

								$data = get_tweet_data($tweet, $source);
								$user_data = get_user_data($tweet, $source, $user_info_cache);

								$data = get_tweet_data($tweet, $source);

								$type_local = 'tweets';

								display_tweet($user_data, $data, $show, $type_local);

							}
							?>

						</div>
						<div class="container-lg mt-5 mb-5 tweets" <?php if ($type != 'tweets') { echo $stylestring;}else { echo ' style="display: block"';} ?>>
							<div class="row">
								<div id="pagination-tweet" class=" col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
								</div>
							</div>
						</div>							
						<div class="reply" <?php if ($type != 'reply') { echo $stylestring;} ?>>
							<?php

							foreach ($replies_data as $key => $tweet)
							{

								$data = get_tweet_data($tweet, $source);
								$user_data = get_user_data($tweet, $source, $user_info_cache);

								$data = get_tweet_data($tweet, $source);
								$type_local = 'reply';

								display_tweet($user_data, $data, $show, $type_local);
							}

							?>
						</div>
						<div class="container-lg mt-5 mb-5 reply " <?php if ($type != 'reply') { echo $stylestring;}else { echo 'style="display:block;"';} ?>>
							<div class="row">
								<div id="pagination-reply" class=" col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
								</div>
							</div>
						</div>

						<div class="media" <?php if ($type != 'media') { echo $stylestring;}else { echo ' style="display: block"';} ?>>
							<?php

							foreach ($media_data as $key => $tweet)
							{

								$data = get_tweet_data($tweet, $source);
								$user_data = get_user_data($tweet, $source, $user_info_cache);

								$data = get_tweet_data($tweet, $source);

								$type_local = 'media';

								display_tweet($user_data, $data, $show, $type_local);

							}							
							?>
						</div>
						<div class="container-lg mt-5 mb-5 media" <?php if ($type != 'media') { echo $stylestring;} else { echo ' style="display: block"';} ?>>
							<div class="row">
								<div id="pagination-media" class=" col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
								</div>
							</div>
						</div>	
					</div>
				</div>

				<div class="col-12 col-sm-4 col-md-4 border-left pr-1">
					<div class=" breaking-height">
						<h3 id="breaking_news"><a href="index.php">Users tweets</a></h3>
						<?php

						$user_info_cache = [];

						foreach ($breaking_tweets_list as $key => $tweet)
						{
							if (!isset($tweet[1]))
							{
								continue;
							}

							if (!$data = readJSON('../code/users/' . $tweet[1] . '/tweets/' . $tweet[0] . '.json'))
							{
							echo 'Error';
								continue;
							}

							//cache user info for later use. 

							if (!isset($user_info_cache[$tweet[1]]))
							{
								if (!$user_data = readJSON('../code/users_info/users/' . $tweet[1] . '.json'))
								{
							//echo 'Error';
									continue;
								}

								$user_info_cache[$tweet[1]] = $user_data;

							}
							else
							{
								$user_data = $user_info_cache[$tweet[1]];
							}

							if ($tweet[1] != 'elonmusk')
							{
									//continue;
							}


							$text = $data['text'];

							if (isset($data['entities']['urls']))
							{
								foreach ($data['entities']['urls'] as $entry)
								{
									$url = $entry['url'];

									$text = str_replace($url, '', $text);
								}

							}

							?>
							<div class="">
								<div class="d-flex  flex-column pb-2 mb-1 border-bottom">
									<div class="d-flex justify-content-between">
										<div class="d-flex mt-2 w-100">
											<div class="d-flex flex-column w-100 ">
												<div class="d-flex flex-wrap">
													<div class="mr-2 text text tweettime text-breaking-header" data-toggle="tooltip" title="<?php echo date_format(date_create($data['created_at']),"Y/m/d H:i:s"); ?>">
														<div class="tweet-user" created_at="<?php echo $data['created_at']; ?>">
															<?php																						
															echo time_elapsed_string($data['created_at']);
															?></div> 
														</div>
														<div class="mr-2">
															<span class="text text-breaking-header">
																<a href="index.php?
																<?php

																echo 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($user_data['data']['0']['username']);

																?>
																">
																@<?php echo $user_data['data']['0']['username']; ?>
															</a>
														</span>
													</div>

												</div>
												<div class="text-breaking mt-1">
													<?php 

													if(isset($data['entities']['mentions']))
													{
														foreach ($data['entities']['mentions'] as $mention)
														{

															if (strripos($text, '@' . $mention['username']))
															{
													if (isset($user_list[$mention['username']])) //in our list? 
													{
														$mention_link = '<a href="' . 'index.php?' . 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($user_data['data']['0']['username']) . '">' .'@' . $mention['username'] . '</a>';
														$text = str_ireplace('@' . $mention['username'], $mention_link, $text);

													}
													else
													{
														$mention_link = '<a href="' . 'https://twitter.com/' . $mention['username'] . '">' .'@' . $mention['username'] . '</a>';
														$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
													}

													
												}
												//echo 'Text now: ' . $text . '<br>-----------<br>'; 
											}
										}

										echo $text . ' ';
										//var_dump($data['entities']); 

										if(isset($data['entities']['urls']))
										{
											foreach ($data['entities']['urls'] as $url)
											{


												if (isset($url['expanded_url']) and !strripos($url['expanded_url'], 'twitter.com') and !strripos($url['expanded_url'], 't.co'))
												{
													echo '| <a href="' . $url['expanded_url'] . '"> Link</a>';
													continue;
												}

												else if (isset($url['url']) and !strripos($url['url'], 'twitter.com') and !strripos($url['url'], 't.co'))  
												{
													echo '| <a href="' . $url['url'] . '"> Link</a>';
													continue;
												}
											}


										}

										?>

									</div>

									<?php 
									if(isset($data['media']) )
									{

										echo '<div class="image mt-2">';
										$counter = 0;

										foreach ($data['media'] as $key => $media)
										{
											if (isset($media['url']) and $media['type'] == 'photo')
											{


												echo '<a href="' . $media['url']  . '">';
												echo '<img class="lazyload" data-src="' . $media['url'] . '" width="100%">';
												echo '</a>';
											}

											if (isset($media['type']) and $media['type'] == 'video')
											{
												$preview_image_url = $media['preview_image_url'];

												if (isset($data['extended_entities']['media']))
												{
													foreach ($data['extended_entities']['media'] as $extended_entry)
													{

														if ($extended_entry['media_url_https'] == $preview_image_url)
														{

															$highest_bitrate = 0;
															$best_key = 0;

															foreach ($extended_entry['video_info']['variants'] as $key => $variant)
															{

																if (isset($variant['bitrate']) and $variant['bitrate'] > $highest_bitrate)
																{
																	$highest_bitrate = $variant['bitrate'];
																	$best_key = $key;
																}

															}

															$video_url = $extended_entry['video_info']['variants'][$best_key]['url'];

															if (isset($extended_entry['video_info']['aspect_ratio']))
															{
																$width = $extended_entry['video_info']['aspect_ratio'][0];
																$height = $extended_entry['video_info']['aspect_ratio'][1];
																$height = $height/$width;
															}

															?>

															<div style="width: 100%;" class="my-2" id="player<?php $key . $data['id']; ?>"></div>

															<script>
																var width_div = $("#breaking_news").width();
																//console.log('breaking: ' + width_div);
																var height_video = <?php echo $height;?> * width_div;
																//console.log('breaking: ' + height_video);

																var player = new Clappr.Player({width: "100%", height: height_video, source: "<?php echo $video_url;?>", parentId: "#player<?php echo $key . $data['id']; ?>"});
															</script>


															<?php
														}

													}
												}

											}
											$counter++;
										}

										echo '</div>';

									}

									?>

								</div>

							</div>
						</div>
					</div>
				</div>

				<?php			 
			}?>

		</div>
		<div id="seeall" class="text-right mt-1" style=""><button type="button" class="btn btn-light">Show all</button>
		</div><!--
		<div class="bg-success mt-3 p-4 text-white" style="height:200px;">annonse
		</div>-->

	</div>
</div>
</div>
</div>

<script type="text/javascript">
	
	var numberofrows = <?php echo $show; ?>;
	var startnumber = <?php echo $start; ?>;
	var numFound = <?php echo 1000; ?>;
	var menu_type = '<?php echo $type;?>';

</script>

<script src="js/customjs.js"></script>

</body>
</html>


<?php

function time_elapsed_string($datetime, $full = false) {
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'year',
		'm' => 'month',
		'w' => 'week',
		'd' => 'd',
		'h' => 'h',
		'i' => 'm',
		's' => 'second',
		);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . '' . $v . ($diff->$k > 1 ? '' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) . '' : 'just now';
}

function time_elapsed_string_breaking($datetime, $full = false) {
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'year',
		'm' => 'month',
		'w' => 'week',
		'd' => 'd',
		'h' => 'h',
		'i' => 'm',
		's' => 'second',
		);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . '' . $v . ($diff->$k > 1 ? '' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) . '' : 'just now';
}

function number_abbr($number)
{
	$abbrevs = [12 => 'T', 9 => 'B', 6 => 'M', 3 => 'K', 0 => ''];

	foreach ($abbrevs as $exponent => $abbrev) {
		if (abs($number) >= pow(10, $exponent)) {
			$display = $number / pow(10, $exponent);
			$decimals = ($exponent >= 3 && round($display) < 100) ? 1 : 0;
			$number = number_format($display, $decimals).$abbrev;
			break;
		}
	}

	return $number;
}


function display_tweet($user_data, $data, $show, $type)
{
	if (!isset($data['text']))
	{
		return;
	}
	$text = $data['text'];

	//remove link in text 
	if (isset($data['entities']['urls']))
	{
		foreach ($data['entities']['urls'] as $entry)
		{
			$url = $entry['url'];

			$text = str_replace($url, '', $text);
		}

	}

	?>
	
	<div class="d-flex  flex-column pb-4 mb-1 border-bottom">
		<div class="d-flex justify-content-between">
			<div class="d-flex mt-2 w-100">
				<div class="mr-2 ">
					<?php echo '<img class="rounded-circle lazyload" data-src="' . $user_data['data']['0']['profile_image_url'] . '" width="58px">'; ?>
				</div>	
				<div class="d-flex flex-column w-100 kolonnebredde">
					<div class="d-flex flex-wrap">
						<div class="mr-2">

							<span class="font-weight-bold text"><a class="text-dark"href="https://twitter.com/<?php echo $user_data['data']['0']['username'];?>"><?php echo $user_data['data']['0']['name'] ; ?></a></span>
						</div>
						<div class="mr-2">
							<span class="text">
								<a href="index.php?
								<?php

								echo 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($user_data['data']['0']['username']);

								?>
								">
								@<?php echo $user_data['data']['0']['username']; ?>
							</a>
						</span>
					</div>
					<div class="mr-2 text tweettime" data-toggle="tooltip" title="<?php echo date_format(date_create($data['created_at']),"Y/m/d H:i:s"); ?>">
						<div class="tweet-user" created_at="<?php echo $data['created_at']; ?>">
							<?php																						
							echo time_elapsed_string($data['created_at']);
							?></div> 
						</div>										
					</div>
					<div class="text mt-2">

						<?php 
						if(isset($data['entities']['mentions']))
						{
							foreach ($data['entities']['mentions'] as $mention)
							{

																if (isset($user_list[$mention['username']])) //in our list? 
																{
																	$mention_link = '<a href="' . 'index.php?' . 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($mention['username']) . '">' .'@' . $mention['username'] . '</a>';
																	$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
																}
																else
																{

																	$length = $mention['end']-$mention['start'];
																	$substring = substr($data['text'], $mention['start'],$length);

																	$mention_link = '<a href="' . 'https://twitter.com/' . $mention['username'] . '">' .'' . $substring . '</a>';
																	$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
																}

															}
														}


														echo $text; 

														?>

													</div>
													<?php 

													$preview_switch = 'off';

													if(isset($data['entities']['urls']))
													{


														foreach ($data['entities']['urls'] as $urlrunner => $url)
														{


															if ( !isset($url['title']) or !isset($url['description']) or ( !isset($url['expanded_url']) and !isset($url['unwound_url']) ))
															{
															//echo 'Url preview not set, will add to list...<br>';

															//var_dump($data['entities']['urls']);

																if (isset($url['expanded_url']) and !strripos($url['expanded_url'], 'twitter.com') and !strripos($url['expanded_url'], 't.co'))
																{
																	echo '<div class="word-wrap">';
																	echo '<a href="' . $url['expanded_url'] . '">' . $url['expanded_url'] . '</a>';
																	echo '</div>';
																	continue;
																}

																else if (isset($url['url']) and !strripos($url['url'], 'twitter.com')  and !strripos($url['url'], 't.co')) 
																{
																	echo '<div class="word-wrap">';
																	echo '<a  href="' . $url['url'] . '">' . $url['url'] . '</a>';
																	echo '</div>';
																	continue;
																}


															}


															if (isset($url['images']))
															{
																echo '<div class="d-flex flex-column mt-2 preview-corners bg-secondary border">';
																$width = 1;
																$best_key = 0;

																foreach ($url['images'] as $key =>  $image)
																{

																	if ($image['width'] > $width)
																	{
																		$width = $image['width'];
																		$best_key = $key;
																	}

																}
																echo '<div class="preview-image-holder ">';

																if (isset($url['expanded_url']))
																{
																	echo '<a href="' . $url['expanded_url'] . '">';
																	echo '<img class="preview-corners-image lazyload" width="100%" data-src="' . $url['images'][$best_key]['url'] . '">' ;
																	echo '</a>';
																}
																else
																{
																	echo '<img class="preview-corners-image lazyload" width="100%" data-src="' . $url['images'][$best_key]['url'] . '">' ;

																}


																echo '</div>';

																$preview_switch = 'on';


																if (isset($url['expanded_url']))
																{
																	echo '<div class="pt-3 pl-3 pr-3"> ';
																	echo '<a href="' . $url['expanded_url'] . '"><h4 class="text-dark preview-title">' . $url['title'] . '</h4></a>';
																	echo '</div>';
																}


																if (isset($url['description']))
																{
																	echo '<div class="pb-0 pl-3 pr-3">';
																	echo '<a href="' . $url['expanded_url'] . '"><p class="text-dark">' . $url['description'] . '</p></a>';
																	echo '</div>';
																}

																echo '</div>';
															}



														}

													}
													?>


													<?php 
													if(isset($data['media']) )
													{

														echo '<div class="image mt-2">';
														$counter = 0;



														foreach ($data['media'] as $key => $media)
														{
															if (isset($media['url']) and $media['type'] == 'photo')
															{

															//only if there is no preview!

																if ($preview_switch == 'off')
																{
																	echo '<a href="' . $media['url']  . '">';
																	echo '<img class="lazyload" data-src="' . $media['url'] . '" width="100%">';
																	echo '</a>';
																}


															}

															if (isset($media['type']) and $media['type'] == 'video')
															{
																$preview_image_url = $media['preview_image_url'];

																if (isset($data['extended_entities']['media']))
																{
																	foreach ($data['extended_entities']['media'] as $extended_entry)
																	{

																		if ($extended_entry['media_url_https'] == $preview_image_url)
																		{


																			$highest_bitrate = 0;
																			$best_key = 0;

																			foreach ($extended_entry['video_info']['variants'] as $key => $variant)
																			{

																				if (isset($variant['bitrate']) and $variant['bitrate'] > $highest_bitrate)
																				{
																					$highest_bitrate = $variant['bitrate'];
																					$best_key = $key;
																				}

																			}

																			$video_url = $extended_entry['video_info']['variants'][$best_key]['url'];

																			if(isset($extended_entry['video_info']['aspect_ratio']))
																			{
																				$width = $extended_entry['video_info']['aspect_ratio'][0];
																				$height = $extended_entry['video_info']['aspect_ratio'][1];
																				$height = $height/$width;
																			}

																			?>
																			
																			<div style="width: 100%;" class="my-2" id="player<?php echo $key . $data['id'] . $type; ?>"></div>
																			<script>
																				var width_div = $(".kolonnebredde").width();
																				//console.log('Parent width users er ' + width_div);
																				var height_video = <?php echo $height;?> * width_div;

																				if (height_video > 349)
																				{
																					height_video = 350;
																				}

																				if (height_video == 0)
																				{
																					height_video = 350;
																				}

																				//console.log('Parent height users er ' + height_video);

																				var player = new Clappr.Player({width: "100%", height: height_video, source: "<?php echo $video_url;?>", parentId: "#player<?php echo $key . $data['id']  . $type; ?>"});
																			</script>


																			<?php
																		}

																	}
																}

															}
															$counter++;
														}

														echo '</div>';

													}

													?>


												</div>

											</div>
										</div>
									</div>

									<?php			 
								}



								function get_tweet_data($tweet, $source)
								{


									if (!isset($tweet[1]))
									{
										return false;
									}

									if ($source == 'users')
									{
										if (!$data = readJSON(mb_strtolower('../code/users/' . $tweet[1] . '/tweets/' . $tweet[0] . '.json')))
										{
										//echo 'Error1';
											return false;
										}
									}
									else if ($source == 'breaking')
									{
										if (!$data = readJSON('../code/breaking/' . mb_strtolower($tweet[1] . '/tweets/' . $tweet[0] . '.json')))
										{
										//echo 'Error2 ' . '../code/breaking/' . mb_strtolower($tweet[1] . '/tweets/' . $tweet[0] . '.json');
											return false;
										}
									}
									else
									{
										return false;
									}

									return $data;
								}

						//cache user info for later use. 

								function get_user_data($tweet, $source)
								{

									if(!isset($tweet[1]))
									{
										return false;
									}

									if (!isset($user_info_cache[$tweet[1]]))
									{

										if ($source == 'users')
										{
											if (!$user_data = readJSON('../code/users_info/users/' . mb_strtolower($tweet[1]) . '.json'))
											{
										//echo 'Error';
												return false;
											}
										}
										else if ($source == 'breaking')
										{
											if (!$user_data = readJSON('../code/users_info/breaking/' . mb_strtolower($tweet[1]) . '.json'))
											{
											    //echo 'Error';
												return false;
											}
										}									

										$user_info_cache[$tweet[1]] = $user_data;

									}
									else
									{
										$user_data = $user_info_cache[$tweet[1]];
									}

									return $user_data;

								}

								function tweet_reader($file, $start, $stop)
								{

									$diff = $stop - $start;

									$user_tweets_list = [];

									if ($file and !$file->eof()) 
									{

										if ($start > 0)
										{
											$file->seek($start-1);
										}

										$read_counter = 0; 

										while($stop > $read_counter)
										{

											$user_tweets_list[] = $file->fgetcsv();
											$read_counter++;

											if ($file->eof() or $read_counter > $diff)
											{
 				//echo 'No more tweets<br>';
												break;
											}

										}

										return $user_tweets_list;

									}
									else
									{
										return false;
									}

								}

								?>



