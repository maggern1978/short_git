<?php 

//christer
//christer2021
//@todo hent mer info fra tweet med video så video kan integreres i siten. 
//@todo hent inn full tekst fra refrerte tweeter
//todo legg inn linker der det finnes

include_once '../code/functions.php';

if (isset($_GET['start']))
{
	$start = htmlspecialchars($_GET["start"]);
}
else
{
	$start = 0;
}

$url_string = 'start=' . $start; 

if (isset($_GET['show']) and is_numeric($_GET['show']))
{
	$show = htmlspecialchars($_GET["show"]);
}
else
{
	$show = 100;
}

$url_string .= '&show=' . $show;

$source = 'users';

if (isset($_GET['name']))
{
	$temp_name = htmlspecialchars($_GET["name"]);

	if (file_exists('../code/users/' . $temp_name . '/' . $temp_name . '_list.csv'))
	{
		$name = $temp_name;
		$url_string .= '&name=' . $name;
		$source = 'users';
	}
	else if (file_exists('../code/breaking/' . $temp_name . '/' . $temp_name . '_list.csv'))
	{
		
		$name = $temp_name;
		$url_string .= '&name=' . $name;
		$source = 'breaking';
	}	
	else 
	{
		$name = false;
	}
}
else
{
	$name = false;
}

if ($name == false)
{

	if (!$file = new SplFileObject('../code/data/displaylist/user_display_list.csv'))
	{
		echo 'Could not read user tweets list<br>';
		$file = false;
	}

}
else
{	
	if ($source == 'users')
	{

		if (!$file = new SplFileObject('../code/users/' . $temp_name . '/' . $temp_name . '_list.csv'))
		{
			echo 'Could not read user tweets list<br>';
			$file = false;
		}
	}
	if ($source == 'breaking')
	{

		if (!$file = new SplFileObject('../code/breaking/' . $temp_name . '/' . $temp_name . '_list.csv'))
		{
			echo 'Could not read user tweets list<br>';
			$file = false;
		}
	}	
}

$stop = $start + $show; 
$user_tweets_list = [];

if ($file and !$file->eof()) 
{

	if ($start > 0)
	{
		$file->seek($start-1);
	}

	$read_counter = 0; 

	while($show > $read_counter)
	{

		$user_tweets_list[] = $file->fgetcsv();
		$read_counter++;

		if ($file->eof() or $read_counter > $show)
		{
 				//echo 'No more tweets<br>';
			break;
		}

	}

}
else
{
	echo 'No more tweets<br>';
}
//var_dump($user_tweets_list);


$user_list = [];

if (!$user_list = readJSON('../code/data/allusers_search_array.json'))
{
	//echo 'Could not read breaking_display_list<br>';
	//$user_list = [];
}



if (!$file = new SplFileObject('../code/data/displaylist/breaking_display_list.csv'))
{
	echo 'Could not read breaking_display_list<br>';
	$file = false;
}


$breaking_tweets_list = [];

if ($file and !$file->eof()) 
{

	$read_counter = 0; 

	while($show > $read_counter)
	{

		$breaking_tweets_list[] = $file->fgetcsv();
		$read_counter++;

		if ($file->eof() or $read_counter > 200)
		{
 				//echo 'No more tweets<br>';
			break;
		}

	}

}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>flashtweets.com</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custom.css">
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@clappr/player@latest/dist/clappr.min.js"></script>
	<script src="js/lazysizes.min.js" async=""></script>
</head>
<body>
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
	<style type="text/css">
	</style>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
		<div class="container">
			<a class="navbar-brand logo font-weight-bold" href="index.php">Flashtweets.com</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarColor02">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home
							<span class="sr-only">Home</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Who we follow</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Key indicators</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="https://flippet.no/christer/code/control_all.php">About us</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0">
					<input class="form-control mr-sm-2" type="text" placeholder="Search">
					<button class="btn btn-secondary my-2 my-sm-0" type="submit">Go</button>
				</form>
			</div>
		</div>
	</nav>
	<div class="container-fluid mt-3">
		<div class="row">
			<div class="col-12 col-sm-8 col-md-6 mx-auto">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
						<div class="d-flex justify-content-between ">
							<div class="menu-font menu-div flex-grow-1 text-center">
								<div class="d-flex ">
									<div id='menu_tweets' class="p-2 mx-auto menu-active"><span class="font-weight-bold">Tweets</span></div>
								</div>
							</div>
							<div class="menu-font menu-div flex-grow-1 text-center">
								<div class="d-flex">
									<div  id='menu_tweetsandreplies' class="p-2 mx-auto"><span class="font-weight-bold">Tweets & replies</span></div>
								</div>
							</div>
							<div class="menu-font menu-div flex-grow-1 text-center">
								<div class="d-flex">
									<div id='menu_media' class="p-2 mx-auto"><span class="font-weight-bold">Media</span></div>
								</div>
							</div>
						</div>
						<script type="text/javascript">
							$(".menu-div").click(function()
							{
								$(this).siblings().find(".p-2").removeClass("menu-active");
								$(this).find(".p-2").addClass("menu-active");

								if ($(this).find(".p-2").attr('id') == "menu_tweets") 
								{
									console.log('menu_tweets');
									$(".tweets").css("display", 'block');
									$(".reply").css("display", 'none');
									
								}
								else if ($(this).find(".p-2").attr('id') == "menu_tweetsandreplies") 
								{
									console.log('menu_tweetsandreplies');
									$(".reply").css("display", 'block');
								}
								else if ($(this).find(".p-2").attr('id') == "menu_media") 
								{
									console.log('menu_media');
									$(".tweets").css("display", 'none');
									$(".tweets.media").css("display", 'block');
									$(".reply").css("display", 'none');
									$(".reply.media").css("display", 'block');
								}    								

							});
						</script>						
						<br>
						<div class="d-flex flex-column">
							<?php

							$user_info_cache = [];



							foreach ($user_tweets_list as $key => $tweet)
							{
								if (!isset($tweet[1]))
								{
									continue;
								}

								if ($source == 'users')
								{
									if (!$data = readJSON(mb_strtolower('../code/users/' . $tweet[1] . '/tweets/' . $tweet[0] . '.json')))
									{
										//echo 'Error1';
										continue;
									}
								}
								else if ($source == 'breaking')
								{
									if (!$data = readJSON('../code/breaking/' . mb_strtolower($tweet[1] . '/tweets/' . $tweet[0] . '.json')))
									{
										//echo 'Error2 ' . '../code/breaking/' . mb_strtolower($tweet[1] . '/tweets/' . $tweet[0] . '.json');
										continue;
									}
								}
								else
								{
									continue;
								}

								
								//cache user info for later use. 

								if (!isset($user_info_cache[$tweet[1]]))
								{
									
									if ($source == 'users')
									{
										if (!$user_data = readJSON('../code/users_info/users/' . $tweet[1] . '.json'))
										{
									//echo 'Error';
											continue;
										}
									}
									else if ($source == 'breaking')
									{
										if (!$user_data = readJSON('../code/users_info/breaking/' . $tweet[1] . '.json'))
										{
									//echo 'Error';
											continue;
										}
									}									
									
									$user_info_cache[$tweet[1]] = $user_data;
									
								}
								else
								{
									$user_data = $user_info_cache[$tweet[1]];
								}

								if ($tweet[1] != 'elonmusk')
								{
									//continue;
								}


								$text = $data['text'];

								//remove link in text 
								if (isset($data['entities']['urls']))
								{
									foreach ($data['entities']['urls'] as $entry)
									{
										$url = $entry['url'];

										$text = str_replace($url, '', $text);
									}

								}


								//var_dump($data);
								$type = 'tweets';

								if (isset($data['in_reply_to_user_id']) or isset($data['referenced_tweets']))
								{
									$type = 'reply';
								}

								if (isset($data['media']) )
								{
									$media = 'media';
								}
								else
								{
									$media = '';
								}


								?>
								<div class="<?php echo $type . ' ' . $media;?>">
									<div class="d-flex  flex-column pb-4 mb-1 border-bottom">
										<div class="d-flex justify-content-between">
											<div class="d-flex mt-2 w-100">
												<div class="mr-2 ">
													<?php echo '<img class="rounded-circle lazyload" data-src="' . $user_data['data']['0']['profile_image_url'] . '" width="58px">'; ?>
												</div>	
												<div class="d-flex flex-column w-100 ">
													<div class="d-flex flex-wrap">
														<div class="mr-2">

															<span class="font-weight-bold text"><a class="text-dark"href="https://twitter.com/<?php echo $user_data['data']['0']['username'];?>"><?php echo $user_data['data']['0']['name'] ; ?></a></span>
														</div>
														<div class="mr-2">
															<span class="text">
																<a href="index.php?
																<?php

																echo 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($user_data['data']['0']['username']);

																?>
																">
																@<?php echo $user_data['data']['0']['username']; ?>
															</a>
														</span>
													</div>
													<div class="mr-2 text" data-toggle="tooltip" title="<?php echo date_format(date_create($data['created_at']),"Y/m/d H:i:s"); ?>">
														<?php																						
														echo time_elapsed_string($data['created_at']);
														?> 
													</div>										
												</div>
												<div class="text mt-2">

													<?php 
													if(isset($data['entities']['mentions']))
													{
														foreach ($data['entities']['mentions'] as $mention)
														{

															if (strripos($text, '@' . $mention['username']))
															{
																if (isset($user_list[$mention['username']])) //in our list? 
																{
																	$mention_link = '<a href="' . 'index.php?' . 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($user_data['data']['0']['username']) . '">' .'@' . $mention['username'] . '</a>';
																	$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
																}
																else
																{
																	$mention_link = '<a href="' . 'https://twitter.com/' . $mention['username'] . '">' .'@' . $mention['username'] . '</a>';
																	$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
																}

																
															}

														}
													}



													echo $text; 


													?>

												</div>
												<?php 

												$preview_switch = 'off';

												if(isset($data['entities']['urls']))
												{
													

													foreach ($data['entities']['urls'] as $urlrunner => $url)
													{
														

														if ( !isset($url['title']) or !isset($url['description']) or ( !isset($url['expanded_url']) and !isset($url['unwound_url']) ))
														{
															//echo 'Url preview not set, will add to list...<br>';
															
															if ((isset($url['display_url']) and strripos($url['display_url'], "twitter.com")))
															{
																//echo 'case 1';
															}

															else if ((isset($url['expanded_url']) and strripos($url['expanded_url'], "twitter.")))
															{
																//echo 'case 2';
															}

															else
															{

																//var_dump($url);

																if (file_exists('../code/previews/users/' . $tweet[0] . '.json'))
																{
																	echo $key . '. Already downloaded...<br>';
																	//continue;
																	if (!$preview_data = readJSON('../code/previews/users/' . $tweet[0] . '.json'))
																	{
																		continue;
																	}

																}
																else if (file_exists( '../code/previews/breaking/' . $tweet[0] . '.json'))
																{
																	echo $key . '. Already downloaded...<br>';

																	if (!$preview_data = readJSON('../code/previews/breaking/' . $tweet[0] . '.json'))
																	{
																		continue;
																	}																	
																}
																else
																{

																$fp = fopen('../code/previews/previews.csv', 'a'); //opens file in append mode  
																fwrite($fp, $tweet[0] . ',' . $user_data['data']['0']['username'] . ',index' . "\n");  
																fclose($fp);
																continue;
															}

															var_dump($preview_data);

															foreach ($preview_data as $preview)
															{


																echo '<div class="d-flex flex-column mt-2 preview-corners bg-secondary border">';

																if (!empty($preview['cover']) and $preview['cover'] != '')
																{

																	echo '<div class="preview-image-holder ">';
																	echo '<img class="preview-corners-image lazyload" width="100%" data-src="' . $preview['cover'] . '">' ;
																	echo '</div>';

																}
																else
																{
																	echo '<div class="preview-image-holder ">';
																	echo '<img class="preview-corners-image lazyload" width="100%" data-src="img/dummy.jpg">' ;
																	echo '</div>';
																}

																if (isset($preview['url_source']))
																{
																	echo '<div class="pt-3 pl-3 pr-3"> ';
																	echo '<a href="' . $preview['url_source'] . '"><h4 class="text-dark preview-title">' . $preview['title'] . '</h4></a>';
																	echo '</div>';
																


																	if (isset($preview['description']) and $preview['description'] != '')
																	{
																		echo '<div class="pb-0 pl-3 pr-3">';
																		echo '<a href="' . $preview['url_source'] . '"><p class="text-dark">' . $preview['description'] . '</p></a>';
																		echo '</div>';
																	}

																}

																echo '</div>';

															}


															
														}


													}

													if ( !isset($url['images']) or !isset($url['unwound_url']) or !isset($url['description']) )
													{
															//echo 'Url preview not set, will add to list...<br>';

														continue;

													}

													echo '<div class="d-flex flex-column mt-2 preview-corners bg-secondary border">';

													if (isset($url['images']))
													{

														$width = 1;
														$best_key = 0;

														foreach ($url['images'] as $key =>  $image)
														{

															if ($image['width'] > $width)
															{
																$width = $image['width'];
																$best_key = $key;
															}

														}
														echo '<div class="preview-image-holder ">';
														echo '<img class="preview-corners-image lazyload" width="100%" data-src="' . $url['images'][$best_key]['url'] . '">' ;
														echo '</div>';

														$preview_switch = 'on';
													}

													if (isset($url['unwound_url']))
													{
														echo '<div class="pt-3 pl-3 pr-3"> ';
														echo '<a href="' . $url['unwound_url'] . '"><h4 class="text-dark preview-title">' . $url['title'] . '</h4></a>';
														echo '</div>';
													}


													if (isset($url['description']))
													{
														echo '<div class="pb-0 pl-3 pr-3">';
														echo '<a href="' . $url['unwound_url'] . '"><p class="text-dark">' . $url['description'] . '</p></a>';
														echo '</div>';
													}


													echo '</div>';

												}

											}
											?>


											<?php 
											if(isset($data['media']) )
											{

													//$keys = key($data['media']);

//													and $data['media']['type'] == 'photo'
													//var_dump($data['media']);

												echo '<div class="image mt-2">';
												$counter = 0;



												foreach ($data['media'] as $key => $media)
												{
													if (isset($media['url']) and $media['type'] == 'photo')
													{

															//only if there is no preview!

														if($preview_switch == 'off')
														{
															echo '<a href="' . $media['url']  . '">';
															echo '<img class="lazyload" data-src="' . $media['url'] . '" width="100%">';
															echo '</a>';
														}


													}

													if (isset($media['type']) and $media['type'] == 'video')
													{
														$preview_image_url = $media['preview_image_url'];

														if (isset($data['extended_entities']['media']))
														{
															foreach ($data['extended_entities']['media'] as $extended_entry)
															{

																if ($extended_entry['media_url_https'] == $preview_image_url)
																{


																	$highest_bitrate = 0;
																	$best_key = 0;

																	foreach ($extended_entry['video_info']['variants'] as $key => $variant)
																	{

																		if (isset($variant['bitrate']) and $variant['bitrate'] > $highest_bitrate)
																		{
																			$highest_bitrate = $variant['bitrate'];
																			$best_key = $key;
																		}

																	}

																	$video_url = $extended_entry['video_info']['variants'][$best_key]['url'];

																	if(isset($extended_entry['video_info']['aspect_ratio']))
																	{
																		$width = $extended_entry['video_info']['aspect_ratio'][0];
																		$height = $extended_entry['video_info']['aspect_ratio'][1];
																		$height = $height/$width;
																	}

																	?>

																	<div style="width: 100%;" class="my-2" id="player<?php echo $key . $data['id']; ?>"></div>
																	<script>
																		var width_div = $("#player<?php echo $key . $data['id']; ?>").parent().width();
																		console.log('width er ' + width_div);
																		var height_video = <?php echo $height;?> * width_div;

																		if (height_video > 349)
																		{
																			height_video = 350;
																		}
																		console.log(height_video);

																		var player = new Clappr.Player({width: "100%", height: height_video, source: "<?php echo $video_url;?>", parentId: "#player<?php echo $key . $data['id']; ?>"});
																	</script>


																	<?php
																}

															}
														}

													}
													$counter++;
												}

												echo '</div>';

											}

											?>
												<!--
												<div class="mt-2 d-flex text ">
													<div class="mr-5">
														<div class="d-flex align-items-center icon-holder">
															<div class=" d-flex rounded-circle icons justify-content-center text-center align-items-center">
																<i class="fa fa-comment-o" aria-hidden="true"></i>
															</div>
															<div class="icons-text ml-1">
																<?php //echo number_abbr($data['public_metrics']['reply_count']);?>
															</div>													
														</div>
													</div>
													<div class="mr-5">
														<div class="d-flex icons-text align-items-center icon-holder">
															<div class=" d-flex rounded-circle icons justify-content-center text-center align-items-center  ">
																<i class="fa fa-retweet" aria-hidden="true"></i>
															</div>
															<div class="icons-text  ml-1">
																<?php //echo number_abbr($data['public_metrics']['retweet_count']);?>
															</div>													
														</div>
													</div>
													<div class="mr-5">
														<div class="d-flex icons-text align-items-center icon-holder">
															<div class=" d-flex rounded-circle icons justify-content-center text-center align-items-center icons-text ">
																<i class="fa fa-heart-o" aria-hidden="true"></i>
															</div>
															<div class="icons-text  ml-1">
																<?php //echo number_abbr($data['public_metrics']['like_count']);?>
															</div>													
														</div>
													</div>
													<div class="">
														<div class="d-flex icons-text align-items-center icon-holder">
															<div class=" d-flex rounded-circle icons justify-content-center text-center align-items-center icons-text ">
																<i class="fa fa-quote-right" aria-hidden="true"></i>
															</div>
															<div class="icons-text  ml-1">
																<?php //echo number_abbr($data['public_metrics']['quote_count']);?>
															</div>													
														</div>
													</div>
												</div>	
											-->

										</div>

									</div>
								</div>
							</div>
						</div>
						<?php			 
					}

					?>

				</div>
				<div class="container-lg mt-5 mb-5">
					<div class="row">
						<div id="pagination" class="col-12 col-sm-12 col-md-12 col-lg-12 mx-auto">
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-4 border-left">
				<h3 id="breaking_news">Breaking news</h3>
				<?php

				$user_info_cache = [];

				foreach ($breaking_tweets_list as $key => $tweet)
				{
					if (!isset($tweet[1]))
					{
						continue;
					}

					if (!$data = readJSON('../code/breaking/' . $tweet[1] . '/tweets/' . $tweet[0] . '.json'))
					{
						//echo 'Error';
						continue;
					}


					//cache user info for later use. 

					if (!isset($user_info_cache[$tweet[1]]))
					{
						if (!$user_data = readJSON('../code/users_info/breaking/' . $tweet[1] . '.json'))
						{
							//echo 'Error';
							continue;
						}

						$user_info_cache[$tweet[1]] = $user_data;

					}
					else
					{
						$user_data = $user_info_cache[$tweet[1]];
					}

					if ($tweet[1] != 'elonmusk')
					{
									//continue;
					}


					$text = $data['text'];

					if (isset($data['entities']['urls']))
					{
						foreach ($data['entities']['urls'] as $entry)
						{
							$url = $entry['url'];

							$text = str_replace($url, '', $text);
						}

					}

					?>
					<div class="">
						<div class="d-flex  flex-column pb-2 mb-1 border-bottom">
							<div class="d-flex justify-content-between">
								<div class="d-flex mt-2 w-100">
									<div class="d-flex flex-column w-100 ">
										<div class="d-flex flex-wrap">
											<div class="mr-2 text text-breaking-header" data-toggle="tooltip" title="<?php echo date_format(date_create($data['created_at']),"Y/m/d H:i:s"); ?>">
												<?php																						
												echo time_elapsed_string_breaking($data['created_at']);
												?> 
											</div>
											<div class="mr-2">
												<span class="text text-breaking-header">
													<a href="index.php?
													<?php

													echo 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($user_data['data']['0']['username']);

													?>
													">
													@<?php echo $user_data['data']['0']['username']; ?>
												</a>
											</span>
										</div>

									</div>
									<div class="text-breaking mt-1">
										<?php 
										
										if(isset($data['entities']['mentions']))
										{
											foreach ($data['entities']['mentions'] as $mention)
											{

												if (strripos($text, '@' . $mention['username']))
												{
													if (isset($user_list[$mention['username']])) //in our list? 
													{
														$mention_link = '<a href="' . 'index.php?' . 'start=' . 0 . '&show=' . $show . '&name=' . mb_strtolower($user_data['data']['0']['username']) . '">' .'@' . $mention['username'] . '</a>';
														$text = str_ireplace('@' . $mention['username'], $mention_link, $text);

													}
													else
													{
														$mention_link = '<a href="' . 'https://twitter.com/' . $mention['username'] . '">' .'@' . $mention['username'] . '</a>';
														$text = str_ireplace('@' . $mention['username'], $mention_link, $text);
													}

													
												}
												//echo 'Text now: ' . $text . '<br>-----------<br>'; 
											}
										}

										echo $text . ' ';
										//var_dump($data['entities']); 

										if(isset($data['entities']['urls']))
										{
											foreach ($data['entities']['urls'] as $url)
											{
												if (isset($url['unwound_url']))
												{
													echo '| <a href="' . $url['unwound_url'] . '"> Link</a>';
												}
												else if (isset($url['expanded_url']))
												{
													echo '| <a href="' . $url['expanded_url'] . '"> Link</a>';
												}
											}


										}

										?>

									</div>

									<?php 
									if(isset($data['media']) )
									{

										echo '<div class="image mt-2">';
										$counter = 0;

										foreach ($data['media'] as $key => $media)
										{
											if (isset($media['url']) and $media['type'] == 'photo')
											{


												echo '<a href="' . $media['url']  . '">';
												echo '<img class="lazyload" data-src="' . $media['url'] . '" width="100%">';
												echo '</a>';
											}

											if (isset($media['type']) and $media['type'] == 'video')
											{
												$preview_image_url = $media['preview_image_url'];

												if (isset($data['extended_entities']['media']))
												{
													foreach ($data['extended_entities']['media'] as $extended_entry)
													{

														if ($extended_entry['media_url_https'] == $preview_image_url)
														{

															$highest_bitrate = 0;
															$best_key = 0;

															foreach ($extended_entry['video_info']['variants'] as $key => $variant)
															{

																if (isset($variant['bitrate']) and $variant['bitrate'] > $highest_bitrate)
																{
																	$highest_bitrate = $variant['bitrate'];
																	$best_key = $key;
																}

															}

															$video_url = $extended_entry['video_info']['variants'][$best_key]['url'];

															if (isset($extended_entry['video_info']['aspect_ratio']))
															{
																$width = $extended_entry['video_info']['aspect_ratio'][0];
																$height = $extended_entry['video_info']['aspect_ratio'][1];
																$height = $height/$width;
															}

															?>

															<div style="width: 100%;" class="my-2" id="player<?php $key . $data['id']; ?>"></div>
															<script>
																var width_div = $("#breaking_news").width();
																console.log(width_div);
																var height_video = <?php echo $height;?> * width_div;
																console.log(height_video);

																var player = new Clappr.Player({width: "100%", height: height_video, source: "<?php echo $video_url;?>", parentId: "#player<?php echo $key . $data['id']; ?>"});
															</script>


															<?php
														}

													}
												}

											}
											$counter++;
										}

										echo '</div>';

									}

									?>

								</div>

							</div>
						</div>
					</div>
				</div>
				<?php			 
			}?>



		</div>
	</div>
</div>
</div>
</div>
<script type="text/javascript">
	$(".icon-holder").hover(function()
	{

		$(this).css("color", 'green');
		$(this).children(".icons").css("background-color","#cbe1ba");
		$(this).children(".icons").css("color","green");
	}, function()
	{

		$(this).css("color", '#212529');
		$(this).children(".icons").css("background-color","white");
		$(this).children(".icons").css("color","#212529");
	});
</script>

<script type="text/javascript">

     //build pagination            
     var pagination_big = '';

     var numberofrows = <?php echo $show; ?>;
     var startnumber = <?php echo $start; ?>;
     var numFound = <?php echo 1000; ?>;

    var totalPages = Math.ceil(numFound/numberofrows); //total pages
    var currentPage = Math.floor(startnumber/numberofrows)+1; //this is shouldn't be static, since it's the current page the user is at
    var page_list = getPageList(totalPages, currentPage, 7);

    var pagination_url = 'index.php?';

    if (currentPage != 1)
    {

    // I changed the for to a while, since the i parameter had to get out of the for. You can still use a for and give the i as a parameter. Whatever.
    var pagination_url_local = pagination_url  + '&start=' + (currentPage - 2)* numberofrows + '&show=' + 
    <?php 
    echo $show . ';';

    if ($name != false)
    {
    	echo 'pagination_url_local += "&name=' . $name . '";';
    }
    ?>;

    pagination_big += ' <li class="page-item"><a class="page-link" href="' + pagination_url_local +'" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>';

}

for (i = 0; i < page_list.length; i++)
{

	if (currentPage  == page_list[i])
	{
		pagination_big += '<li class="page-item active"><a class="page-link" href="#">' + page_list[i] +'</a></li>';
	}
	else if (page_list[i] == 0 )
	{
                //nav += '<li class="page-item"><a class="page-link" href="#">...</a></li>';
            }               
            else
            {
            	var pagination_url_local = pagination_url  + 'start=' + (page_list[i] - 1)* numberofrows + '&show=' + 
            	<?php 
            	echo $show . ';';

            	if ($name != false)
            	{
            		echo 'pagination_url_local += "&name=' . $name . '";';
            	}
            	?>;
            	pagination_big += '<li class="page-item"><a href="' + pagination_url_local + '" class="page-link" >' + page_list[i] + '</a></li>';
            }
        }

        if (currentPage != totalPages)
        {

        	if (totalPages > 10)
        	{
        		var pagination_url_local = pagination_url  + '&start=' + (totalPages-1) * numberofrows +  '&show=' + 
        		<?php 
        		echo $show . ';';

        		if ($name != false)
        		{
        			echo 'pagination_url_local += "&name=' . $name . '";';
        		}
        		?>;
        		pagination_big += ' <li class="page-item"><a class="page-link" href="' + pagination_url_local +'" aria-label="Siste"><span aria-hidden="true">Last</span><span class="sr-only">Siste</span></a></li>';                
        	}

        	var pagination_url_local = pagination_url  + '&start=' + currentPage * numberofrows+ '&show=' + 
        	<?php 
        	echo $show . ';';

        	if ($name != false)
        	{
        		echo 'pagination_url_local += "&name=' . $name . '";';
        	}
        	?>;
        	pagination_big += ' <li class="page-item"><a class="page-link" href="' + pagination_url_local +'" aria-label="Next"><span aria-hidden="true">&raquo</span><span class="sr-only">Neste</span></a></li>';

        }

        nav = '<div class="d-flex flex-column flex-sm-column flex-md-row justify-content-between">';

        if (totalPages > 0)
        {
        	nav += '<div class=>'; 
        	nav += '<div class=" d-none d-sm-block">';
        	nav += '<ul class="pagination">';
        	nav += pagination_big;
        	nav += '</ul>';
        	nav += '</div>';

        	nav += '<div class=" d-block d-sm-none">';
        	nav += '<ul class="pagination pagination-sm ">';
        	nav += pagination_big;
        	nav += '</ul>';
        	nav += '</div>';
        	nav += '</div>';
        }
        else
        {
        	nav += '<div class="d-none d-sm-block"> ';
        	nav += '</div>';
        }

        nav += '<div class="my-auto text-right">';
        nav += 'Showing ' + (startnumber+1) + ' to ' + (startnumber + numberofrows);
        nav += '</div>';
        nav += '</div>';

        //console.log( pagination_big);
        //console.log(nav);

        $('#pagination').html(nav);
        $("#pagination").show();



        function getPageList(totalPages, page, maxLength) 
        {
        	if (maxLength < 5) throw "maxLength must be at least 5";

        	function range(start, end) {
        		return Array.from(Array(end - start + 1), (_, i) => i + start); 
        	}

        	var sideWidth = 0;
        	var leftWidth = (maxLength - sideWidth*2 - 3) >> 1;
        	var rightWidth = (maxLength - sideWidth*2 - 2) >> 1;
        	if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength - sideWidth - 0)
        .concat(0, range(totalPages - sideWidth + 1, totalPages));
    }
    if (page >= totalPages - sideWidth - 0 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
        .concat(0, range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }

    return range(page - leftWidth - 1, page + rightWidth + 1);
}

function number_format (number, decimals, dec_point, thousands_sep) 
{
      // Strip all characters but numerical ones.
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function (n, prec) 
      {
      	var k = Math.pow(10, prec);
      	return '' + Math.round(n * k) / k;
      };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
      	s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
      	s[1] = s[1] || '';
      	s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
  }

</script>


</body>
</html>


<?php

function time_elapsed_string($datetime, $full = false) {
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'year',
		'm' => 'month',
		'w' => 'week',
		'd' => 'd',
		'h' => 'h',
		'i' => 'm',
		's' => 'second',
		);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . '' . $v . ($diff->$k > 1 ? '' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function time_elapsed_string_breaking($datetime, $full = false) {
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'year',
		'm' => 'month',
		'w' => 'week',
		'd' => 'd',
		'h' => 'h',
		'i' => 'm',
		's' => 'second',
		);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . '' . $v . ($diff->$k > 1 ? '' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) . '' : 'just now';
}

function number_abbr($number)
{
	$abbrevs = [12 => 'T', 9 => 'B', 6 => 'M', 3 => 'K', 0 => ''];

	foreach ($abbrevs as $exponent => $abbrev) {
		if (abs($number) >= pow(10, $exponent)) {
			$display = $number / pow(10, $exponent);
			$decimals = ($exponent >= 3 && round($display) < 100) ? 1 : 0;
			$number = number_format($display, $decimals).$abbrev;
			break;
		}
	}

	return $number;
}


?>



