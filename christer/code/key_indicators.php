<?php

include_once('../code/functions.php');

$files = listfiles('../code/data/tickers/');

$alltickers = [];

foreach ($files as $file)
{
	$alltickers[] = readJSON('../code/data/tickers/' . $file);
}

include '../public_html/header.html';

?>
<div class="container mt-3">
<div class="row">
<div class="col-12">
<h2>Key indicators</h2>
<p>Here are some important financial indexes and prices that we follow. </p>
</div>
</div>
</div>

<div class="container">

<div class="d-flex flex-column">
<h2>Equity indices</h2>
<?php displayticker($alltickers, 'S&P 500', 1,true, 2); ?>
<?php displayticker($alltickers, 'Nasdaq', 1,false, 0); ?>
<?php displayticker($alltickers, 'Dow Jones', 1,false,2); ?>
<?php displayticker($alltickers, 'FTSE-London', 2, false,2); ?>
<?php displayticker($alltickers, 'CAC-Paris', 2, false,2); ?>
<?php displayticker($alltickers, 'DAX-Frankfurt', 2, false,2); ?>
<?php displayticker($alltickers, 'Nikkei', 2, false,2); ?>
<?php displayticker($alltickers, 'Hang Seng', 2, false,2); ?>
<h2 class="mt-3">Currency</h2>
<?php displayticker($alltickers, 'EUR/USD', 3, false,3); ?>
<?php displayticker($alltickers, 'USD/JPY', 3, false,3); ?>
<?php displayticker($alltickers, 'Bitcoin', 4, false,0); ?>
<h2 class="mt-3">Bonds</h2>
<?php displayticker($alltickers, 'US 10-YR', 3, false,2); ?>
<h2 class="mt-3">Commodities</h2>
<?php displayticker($alltickers, 'Crude Oil', 2,false,2); ?>
<?php displayticker($alltickers, 'Gold', 2, false,2); ?>
<?php displayticker($alltickers, 'Silver', 2, false,2); ?>
<?php displayticker($alltickers, 'Copper', 2, false,2); ?>




</div>
</div>
</div>
</body>
</html>

<?php



function displayticker($alltickers, $target, $rank, $first, $decimals)
{
	foreach ($alltickers as $ticker) 
	{
		if ($target != $ticker['name'])
		{
			continue;
		}
		
		if ($ticker['change_p'] > 0)
		{
			$subfix = 'plus';
		}
		else if ($ticker['change_p'] < 0)
		{
			$subfix = 'minus';
		}		
		else if ($ticker['change_p'] == 0)
		{
			$subfix = 'zero';
		}		


		if ($rank == 1)
		{
			$visibility = '';
		}
		else if ($rank == 2)
		{
			$visibility = 'd-none d-sm-block';
		}
		else if ($rank == 3)
		{
			$visibility = 'd-none d-sm-none d-md-block';
		}
		else if ($rank == 4)
		{
			$visibility = 'd-none d-sm-none d-md-none d-lg-block';
		}
		else if ($rank == 5)
		{
			$visibility = 'd-none d-sm-none d-md-none d-lg-none d-xl-block';
		}							
		else
		{
			$visibility = '';
		}

	?>
	<div class=" <?php echo '' ?>">
	<div class="d-flex">
		<div style="min-width: 100px;" data-toggle="tooltip" title="Updated <?php echo $ticker['timestamp'];?>" class="ticker-text ticker-name mr-2 mt-1 mb-1 font-weight-bold"><?php echo $ticker['name'];?>
		</div>
		<div style="min-width: 80px;" data-toggle="tooltip" title="Updated <?php echo $ticker['timestamp'];?>" class="ticker-text text-right ticker-close mr-2 mt-1 mb-1"><?php echo round($ticker['close'],$decimals);?>
		</div>	
		<div  data-toggle="tooltip" title="Updated <?php echo $ticker['timestamp'];?>" class="ticker-change mr-4 mt-1 mb-1 ticker-change-<?php echo $subfix;?> "><?php echo round($ticker['change_p'],2);?>%
		</div>	
	</div>
	</div>
	
	<?php 
	break;
	}
	
}


?>