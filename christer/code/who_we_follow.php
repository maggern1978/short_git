<?php


include 'header.html';
include_once '../code/functions.php';

if (!$users = readCSV('../code/data/user_list.csv'))
{
	//echo 'Could not read user tweets list<br>';
	
}

usort($users, function ($item1, $item2) {
    
    return $item1[0] <=> $item2[0];

});


if (!$breaking = readCSV('../code/data/breaking_list.csv'))
{
	//echo 'Could not read user tweets list<br>';
	
}

usort($users, function ($item1, $item2) {
    
    return $item1[0] <=> $item2[0];

});

$count = count($users);
$half = ceil($count/2)-1;

?>
<div class="container mt-3">
<div clasS="row">
<div class="col-12">

<h2>Who we follow</h2>
<p>We follow between 50 and 100 of the most important twitter accounts in finance. Who we follow is based on a set of criterias, whereas the most important is quality of the content these users share. <br><br>
In addition, we also follow a wide range of twitter accounts in the financial media, with the intent of bringing you breaking news as fast as possible.<p>
<h4>Users</h4>
</div>
</div>
</div>

<div class="container mt-2">
<div clasS="row">
<div class="col-12 col-sm-6">
<?php

foreach ($users as $key => $user)
{
	//http://localhost/christer/public_html/index.php?start=0&show=25&name=business
	echo '<div class=""><a href="index.php?start=0&show=25&name=' . $user[0] . '">@' . $user[0] . '</a></div>';

	if ($key == $half) 
	{
		echo '</div><div class="col-12 col-sm-6">';
	}

}

?>
</div>
</div>
</div>

<div class="container mt-4">
<div clasS="row">
<div class="col-12">
<h4>News sources</h4>
</div>
</div>
</div>

<div class="container mt-2">
<div clasS="row">
<div class="col-12 col-sm-6">
<?php
$count = count($breaking);
$half = ceil($count/2) - 1;


foreach ($breaking as $key => $user)
{
	//http://localhost/christer/public_html/index.php?start=0&show=25&name=business
	echo '<div class=""><a href="index.php?start=0&show=25&name=' . mb_strtolower($user[0]) . '">@' . $user[0] . '</a></div>';

	if ($key == $half) 
	{
		echo '</div><div class="col-12 col-sm-6">';
	}

}
?>
</div>
</div>
</div>
<br>
<br>
<br>

</body>
</html>
