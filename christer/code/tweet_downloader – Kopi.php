<?php

//https://developer.twitter.com/en/docs/twitter-api/rate-limits#v2-limits
//https://developer.twitter.com/en/docs/projects/faq

require_once('twitter-api-php-master/TwitterAPIExchange.php');

$settings = array(
	'oauth_access_token' => "1058139584113098752-6o779K154M8loaVx75oG5i2YoKnrsZ",
	'oauth_access_token_secret' => "V9TON0QuCpR7TbfL7uHCvIUQDlXMZj5jMBNY6mZHW1mAC",
	'consumer_key' => "9iYS641QFf9GeDD8tNJV3qg1j",
	'consumer_secret' => "xfBC8K0rVEEIl3KXmq2Jcw0qwRHtZGILCZ5nSSr2jImKbbEWcN"
	);

include_once 'functions.php';
include_once 'functions_flashtweets.php';

$develop = 0;
$get_number_of_tweets = 10;
$only_download_name = 'ritholtz';
$only_download_switch = 'off';
//$mode = 'users';
//$mode = 'breaking';

if (!isset($mode))
{
	$mode = 'users';
}

$users = [];

if ($mode == 'users')
{
	$handle = fopen("data/user_list.csv", "r");
	$savedirectory = 'users/';
}
else if ($mode == 'breaking')
{
	$handle = fopen("data/breaking_list.csv", "r");
	$savedirectory = 'breaking/';
}
else
{
	return('No mode set!!');

}

successecho("Mode er $mode: <br>");

if ($handle) 
{
	while (($line = fgets($handle)) !== false) 
	{
		$users[] = trim($line);    
	}

	fclose($handle);
} 
else 
{
  errorecho('Error opening user file!');  // error opening the file.
  exit('');
}


  //https://twitteroauth.com/

  //$statuses = $connection->get("search/tweets", ["q" => "shortnordic"]);
	$tweets = [];

	foreach ($users as $key => $username)
	{
		echo $username . ' | ';
		if (mb_strtolower($username) != $only_download_name and $only_download_switch == 'on')
		{
			continue;
		}

		echo $username;

    	//make dir if not existing
		if (!check_if_dir_exists($savedirectory . $username))
		{
			erorecho('Error creating user directory<br>');
			continue;
		}

		if (!check_if_tweets_dir_exists($savedirectory . $username))
		{
			erorecho('Error creating tweets directory<br>');
			continue;
		}

    	//get user id
		if (!$id = readJSON('users_info/' . $mode . '/' . mb_strtolower($username) . '.json'))
		{
			errorecho('Could not read user info! ' . mb_strtolower($username) . ', continuing...<br>');
			continue;
		}

		echo 'Username is ' . $username . '<br>';

		if (!isset($id['data'][0]['id']))
		{
			errorecho('id not set, skipping!<br>');
			continue;
		}
		
		$id = $id['data'][0]['id'];

	    //get previous id
		if (!file_exists(mb_strtolower($savedirectory . $username . '/' . $username . '_replies.csv')) or !$previous = readCSVkomma(mb_strtolower($savedirectory . $username . '/' . $username . '_replies.csv')))
		{
			$previous = [];
		}

		if (empty($previous[0][0]))
		{
			errorecho('Getting statuses WITHOUT since_id<br>');
    	//$statuses = $connection->get("statuses/user_timeline", array('count' => 30, 'trim_user' => false, 'exclude_replies' => true, 'screen_name' => $username));
			$url = 'https://api.twitter.com/2/users/' . $id . '/tweets';
			$getfield = '?max_results=' . $get_number_of_tweets;
			$getfield .= '&expansions=attachments.poll_ids,attachments.media_keys,author_id,entities.mentions.username,geo.place_id,in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id';	
			$getfield .= '&media.fields=duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics';
			$getfield .= '&tweet.fields=attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,text,withheld';
			$requestMethod = 'GET';
			
		}
		else
		{
			echo 'Getting statuses WITH since_id<br>';
			$since_id = $previous[0][0];
			$since_id = str_replace('.json', '', $since_id);
			$since_id = str_replace('_media', '', $since_id);
			$since_id = str_replace('_reply', '', $since_id);
			echo 'Since_id er "' . $since_id . '"<br>';

			$url = 'https://api.twitter.com/2/users/' . $id . '/tweets';
			$getfield = '?max_results=' . $get_number_of_tweets . '&since_id=' . $since_id;
			$getfield .= '&expansions=attachments.poll_ids,attachments.media_keys,author_id,entities.mentions.username,geo.place_id,in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id';	
			$getfield .= '&media.fields=duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics';
			$getfield .= '&tweet.fields=attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,text,withheld';
	//$getfield .= '&user.fields=created_at,description,entities,id,location,name,pinned_tweet_id,profile_image_url,protected,public_metrics,url,username,verified,withheld';

			$requestMethod = 'GET';
		}

		if ($develop == 0)
		{

			//trigger exception in a "try" block
			try {
				$twitter = new TwitterAPIExchange($settings);

				$statuses = $twitter->setGetfield($getfield)
				->buildOauth($url, $requestMethod)
				->performRequest();
			}

			//catch exception

			catch(Exception $e) 
			{
				echo 'Message: ' . $e->getMessage();
				continue;
			}

			if (!$statuses = json_decode($statuses, true))
			{
				errorecho('Could not parse json reply from twitter, continuing..<br>');
				continue; 
			}

			saveJSON($statuses, 'users/raw_dowload.json');

		}
		else
		{
			echo '<h1>Developer mode!</h1>';
			$statuses = readJSON('users/raw_dowload.json');
		}
	
		
		if (!empty($statuses))
		{
			if (isset($statuses['meta']['result_count']))
			{
				echo 'Downloaded ' . $statuses['meta']['result_count'] . ' tweets.<br>';

				if ($statuses['meta']['result_count'] == 0)
				{
					echo '<br>----------------------<br><br>';
					continue;
				}

			}

		}


		if (isset($statuses['data']))
		{
			foreach ($statuses['data'] as $index => $tweet)
			{
				$tweet_export = [];
				$tweet_export['data'] = $tweet;

				//data
				//includes media x
				//includes users/mentions x 
				//includes tweets



				$found_switch = 0; 

				echo $index . '. Doing data id ' . $tweet['id'] . '<br>';

				if(isset($tweet['attachments']['media_keys']))
				{
					echo 'Attachements Media(s) found!<br>';
					$image_urls = [];

					foreach ($tweet['attachments']['media_keys'] as $key)
					{
						if (isset($statuses['includes']['media']))
						{

							foreach ($statuses['includes']['media'] as $media)
							{

								if ($media['media_key'] == $key)
								{
									$tweet_export['includes']['media'][] = $media; 
									$found_switch = 1;
									break;
								}

							}

						}

					}

				}
			
				if (isset($tweet['entities']['mentions']))
				{
					
					$mentions_box = [];

					foreach ($tweet['entities']['mentions'] as $mention)
					{
						//echo '$mention["id"] er ' . $mention['id'] . '<br>';
						foreach ($statuses['includes']['users'] as $user)
						{
							
							if ($mention['id'] == $user['id'])
							{
								//echo 'Found user!<br>';
								$mentions_box[] = $user;
							}

						}
					}

					if (!empty($mentions_box))
					{
						$tweet_export['includes']['users'] = $mentions_box;
					}
					
				}				


				//lookup references tweets
				if (isset($tweet['referenced_tweets']))
				{
					$found_switch_referenced = 0;

					$referenced_tweets_box = [];

					foreach ($tweet['referenced_tweets'] as $reference)
					{
						
						$found_reference = 0;

						foreach ($statuses['includes']['tweets'] as $includes_tweets)
						{
							if ($reference['id'] == $includes_tweets['id'])
							{
								
								$tweet_export['includes']['tweets'] = $includes_tweets;

								//echo "referenced_tweets id found in data['includes']['tweets']<br>" ;
								$found_reference = 1;
								break;

							}
						}


						if ($found_reference == 1)
						{
							//continue;
						}

						//echo 'Referert tweet ikke funnet i includes | tweets, laster ned med get_single_tweet($reference["id"]' . '<br>';

						echo 'Laster ned get_single_tweet() for id ' . $reference['id'] .'<br>';


						if (!$status_referenced = json_decode(get_single_tweet($reference['id']), true))
						{
							errorecho('Could not parse json referenced reply from twitter, continuing..<br>');
							continue; 
						}
						else
						{
							$tweet_export['referenced_tweets_download'][] = $status_referenced;
							echo ' ok!<br>';
						}
		
						
					}

			
				}


				if (isset($tweet_export['referenced_tweets_download']) and $mode == 'users')
				{
					echo 'Checking referenced_tweets_download for secondary tweets to download.<br>';

					foreach ($tweet_export['referenced_tweets_download'] as $key_teller => $referenced_tweet)
					{
						//var_dump($referenced_tweet);

						if (isset($referenced_tweet['data']['referenced_tweets']))
						{

							foreach ($referenced_tweet['data']['referenced_tweets'] as $reference)
							{

								if (!$status_referenced = json_decode(get_single_tweet($reference['id']), true))
								{
									errorecho('Could not parse json referenced reply from twitter, continuing..<br>');
									continue; 
								}
								else
								{
									$tweet_export['referenced_tweets_download'][$key_teller]['referenced_tweets_download'][] = $status_referenced;
									echo 'Secondary download ok, must put data into $tweet_export';
								}

							}

						}

					}

				}

				//sjekk om video og last ned ekstra data i så fall
				//nivå 1
				if ($tweet_export = check_for_video($tweet_export))
				{

					echo 'Check for video ok<br>';
				}
				else
				{
					echo 'Check for video fails.<br>';
				}
									
				$file_ending = '';

				if (isset($tweet_export['data']['in_reply_to_user_id']))
				{
					$file_ending .= '_reply';
				}
		
				if (isset($tweet_export['includes']['media']))//nok? 
				{
					$file_ending .= '_media';
				}
		


				if (!file_exists(mb_strtolower($savedirectory . $username . '/tweets/' . $tweet['id'] . $tweet['id'] . $file_ending . '.json')))
				{
					saveJSON($tweet_export, mb_strtolower($savedirectory . $username . '/tweets/' . $tweet['id'] . $file_ending . '.json'));
				}
				else
				{
					echo 'Already saved file' .  $savedirectory . mb_strtolower($username) . '/tweets/' . $tweet['id'] . $file_ending .  '.json... will update.<br>';

					saveJSON($tweet_export, mb_strtolower($savedirectory . mb_strtolower($username . '/tweets/' . $tweet['id'] . $file_ending . '.json')));

				}

				echo '<br>------local-round----------------<br><br>';

			}
		}

		user_display_list_maker($savedirectory, $username);

		

		if (!empty($statuses))
		{
			saveJSON($statuses, mb_strtolower($savedirectory . $username . '/' . $username . '_latest_download.json'));
		}
		else
		{
			errorecho('Data is empty for user ' . $username . '. Probably no new updates. <br><br>');
		}

		echo '<br>----------------------<br><br>';

		

	}


?>


