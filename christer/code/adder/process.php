<?php


//include_once '../../code/functions.php';


if (isset($_POST['breaking']))
{
	$array =  filter_var($_POST['breaking'], FILTER_SANITIZE_STRING);

	file_put_contents('../../code/data/breaking_list.csv', $array);

	$date = (string)date("Y-m-d H.i");

	file_put_contents('log/' . $date . '_breaking_list.csv', $array);

}

if (isset($_POST['users']))
{
	$array =  filter_var($_POST['users'], FILTER_SANITIZE_STRING);

	file_put_contents('../../code/data/user_list.csv', $array);

	$date = (string)date("Y-m-d H.i");

	file_put_contents('log/' . $date . '_user_list.csv', $array);
}

if (!isset($_POST['users']) and !isset($_POST['breaking']))
{
	$errors[] = 'No data set!';
}

	if (!empty($errors)) {
		$data['success'] = false;
		$data['errors'] = $errors;
		$data['message'] = $message;
	} else {
		$data['success'] = true;
		$data['message'] = 'Success!';
	}

	echo json_encode($data);


return;

$errors = [];
$data = [];
$message = [];

if (!empty($_POST['breaking'])) {
	$breaking = $_POST['breaking'];
	$message[] = $breaking;
}
else
{
	$message[] = 'No breaking data set!';
}


if (!empty($_POST['users'])) {
	//$users = filter_var($_POST['users'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
}
else
{
	$message[] = 'No user data set!';
}

if (!isset($users) and !isset($breaking))
{
	$errors[] = 'No data set!';
}



if (isset($users) and !empty($users))
{
	
	$fp = fopen('user_list.csv', 'w');

	foreach ($users as $user)
	{

		$data = $user.PHP_EOL;
		fwrite($fp, $data);

	}

	fclose($fp);
}

if (isset($breaking) and !empty($breaking))
{
	
	$fp = fopen('breaking_list.csv', 'w');

	foreach ($breaking as $user)
	{

		$data = $user .PHP_EOL;
		fwrite($fp, $data);

	}

	fclose($fp);
}


	if (!empty($errors)) {
		$data['success'] = false;
		$data['errors'] = $errors;
		$data['message'] = $message;
	} else {
		$data['success'] = true;
		$data['message'] = 'Success!';
	}

	echo json_encode($data);




//$current_users = readCSV('../../code/data/user_list.csv');
//$current_breaking = readCSV('../../code/data/breaking_list.csv');


