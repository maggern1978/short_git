<?php

include_once '../../code/functions.php';

$current_users = readCSV('../../code/data/user_list.csv');
$current_breaking = readCSV('../../code/data/breaking_list.csv');


?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Flashtweets.com</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
  <body>
    <div class="container">
      <div class="col-sm-6 col-sm-offset-3">
        <h2>Oppdatering av brukerlistene</h2>
        <p>Post inn hele listen, nåværende liste erstattes av det som er i tekstfeltene. Ett navn pr linje.</p>

        <form id="form" class="mb-5" action="process.php" method="POST">
         <div id="name-group" class="form-group">
          <h3>Vanlige brukere i hovedtråden</h3>
          <textarea style="height:300px;"
          type="text"
          class="form-control"
          id="users"
          name="users"
          placeholder=""
          ></textarea>
        </div>

          <button type="submit" class="btn btn-success">
          Oppdater brukere
        </button>
        <div id="result" class="mt-3 mb-3"></div>
      </form>


      <form id="form2" action="process.php" method="POST">
        <div id="email-group" class="form-group">
          <h3>Breaking-brukere</h3>
          <textarea style="height:300px;"
          type="text"
          class="form-control"
          id="breaking"
          name="breaking"
          placeholder=""
          ></textarea>

        </div>
        <button type="submit" class="btn btn-success">
          Oppdater breaking
        </button>
        <div id="result2" class="mt-3"></div>
      </form>

    </div>
  </div>    
</body>
<script>

var breaking_current = <?php echo json_encode($current_breaking); ?>;
var users_current = <?php echo json_encode($current_users); ?>;

var breaking_length = breaking_current.length;
var breaking_text = '';

for (i= 0; i < breaking_length; i++)
{
  breaking_text += breaking_current[i][0] + "\n";
} 

var users_length = users_current.length;
var users_text = '';

for (i= 0; i < users_length; i++)
{
  users_text += users_current[i][0] + "\n";
} 

$("#breaking").val(breaking_text);
$("#users").val(users_text);

  //https://www.digitalocean.com/community/tutorials/submitting-ajax-forms-with-jquery
  $(document).ready(function () {
    $("#form").submit(function (event) {
      var formData = {
        users: $("#users").val(),
      };
     
      console.log(formData);
      $.ajax({
        type: "POST",
        url: "process.php",
        data: formData,
        dataType: "json",
        encode: true,
      }).done(function (data) {
        console.log(data);

        if (data.success === true) 
        {
          $("#result").html('Oppdatert suksess. ' + new Date());
        }
        else
        {

          var message = 'Oppdatering feilet. ' + new Date();

    
          $("#result").html(message + new Date());
        }

      });

      event.preventDefault();
    });


    $("#form2").submit(function (event) {
      var formData = {
        breaking: $("#breaking").val(),
      };
     
      console.log(formData);
      $.ajax({
        type: "POST",
        url: "process.php",
        data: formData,
        dataType: "json",
        encode: true,
      }).done(function (data) {
        console.log(data);

        if (data.success === true) 
        {
          $("#result2").html('Oppdatert suksess.' + new Date() );
        }
        else
        {

          var message = 'Oppdatering feilet. ';

          $("#result2").html(message + new Date());
        }

      });

      event.preventDefault();
    });


  });


</script>
</html>