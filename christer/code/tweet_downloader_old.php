<?php

//https://developer.twitter.com/en/docs/twitter-api/rate-limits#v2-limits
//https://developer.twitter.com/en/docs/projects/faq

require_once('twitter-api-php-master/TwitterAPIExchange.php');

$settings = array(
	'oauth_access_token' => "1058139584113098752-6o779K154M8loaVx75oG5i2YoKnrsZ",
	'oauth_access_token_secret' => "V9TON0QuCpR7TbfL7uHCvIUQDlXMZj5jMBNY6mZHW1mAC",
	'consumer_key' => "9iYS641QFf9GeDD8tNJV3qg1j",
	'consumer_secret' => "xfBC8K0rVEEIl3KXmq2Jcw0qwRHtZGILCZ5nSSr2jImKbbEWcN"
	);

include_once 'functions.php';
include_once 'functions_flashtweets.php';


//$mode = 'users';
//$mode = 'breaking';

if (!isset($mode))
{
	$mode = 'users';
}

$devmode = 0; 
$users = [];

if ($mode == 'users')
{
	$handle = fopen("data/user_list.csv", "r");
	$savedirectory = 'users/';
}
else if ($mode == 'breaking')
{
	$handle = fopen("data/breaking_list.csv", "r");
	$savedirectory = 'breaking/';
}
else
{
	return('No mode set!!');

}

successecho("Mode er $mode: <br>");

if ($handle) 
{
	while (($line = fgets($handle)) !== false) 
	{
		$users[] = trim($line);    
	}

	fclose($handle);
} 
else 
{
  errorecho('Error opening user file!');  // error opening the file.
  exit('');
}

if ($devmode == 0)
{

  //https://twitteroauth.com/


  //$statuses = $connection->get("search/tweets", ["q" => "shortnordic"]);
	$tweets = [];

	foreach ($users as $key => $username)
	{


		echo $username . ' | ';
		if (mb_strtolower($username) != 'scaramucci')
		{
			continue;
		}


		echo $username;


    //make dir if not existing
		if (!check_if_dir_exists($savedirectory . $username))
		{
			erorecho('Error creating user directory<br>');
			continue;
		}

		if (!check_if_tweets_dir_exists($savedirectory . $username))
		{
			erorecho('Error creating tweets directory<br>');
			continue;
		}

    	//get user id
		if (!$id = readJSON('users_info/' . $mode . '/' . mb_strtolower($username) . '.json'))
		{
			errorecho('Could not read user info! ' . mb_strtolower($username) . ', continuing...<br>');
			continue;
		}

		echo 'Username is ' . $username . '<br>';

		if (!isset($id['data'][0]['id']))
		{
			errorecho('id not set, skipping!<br>');
			continue;
		}
		
		$id = $id['data'][0]['id'];

	    //get previous id
		if (!file_exists(mb_strtolower($savedirectory . $username . '/' . $username . '_replies.csv')) or !$previous = readCSVkomma(mb_strtolower($savedirectory . $username . '/' . $username . '_replies.csv')))
		{
			$previous = [];
		}

		if (empty($previous[0][0]))
		{
			errorecho('Getting statuses WITHOUT since_id<br>');
    	//$statuses = $connection->get("statuses/user_timeline", array('count' => 30, 'trim_user' => false, 'exclude_replies' => true, 'screen_name' => $username));
			$url = 'https://api.twitter.com/2/users/' . $id . '/tweets';
			$getfield = '?max_results=15';
			$getfield .= '&expansions=attachments.poll_ids,attachments.media_keys,author_id,entities.mentions.username,geo.place_id,in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id';	
			$getfield .= '&media.fields=duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics';
			$getfield .= '&tweet.fields=attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,text,withheld';
			$requestMethod = 'GET';
			
		}
		else
		{
			echo 'Getting statuses WITH since_id<br>';
			$since_id = $previous[0][0];
			$since_id = str_replace('.json', '', $since_id);
			$since_id = str_replace('_media', '', $since_id);
			$since_id = str_replace('_reply', '', $since_id);
			echo 'Since_id er "' . $since_id . '"<br>';

			$url = 'https://api.twitter.com/2/users/' . $id . '/tweets';
			$getfield = '?max_results=15&since_id=' . $since_id;
			$getfield .= '&expansions=attachments.poll_ids,attachments.media_keys,author_id,entities.mentions.username,geo.place_id,in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id';	
			$getfield .= '&media.fields=duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics';
			$getfield .= '&tweet.fields=attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,text,withheld';
	//$getfield .= '&user.fields=created_at,description,entities,id,location,name,pinned_tweet_id,profile_image_url,protected,public_metrics,url,username,verified,withheld';

			$requestMethod = 'GET';
		}



		//trigger exception in a "try" block
		try {
			$twitter = new TwitterAPIExchange($settings);

			$statuses = $twitter->setGetfield($getfield)
			->buildOauth($url, $requestMethod)
			->performRequest();
		}

		//catch exception

		catch(Exception $e) 
		{
			echo 'Message: ' . $e->getMessage();
			continue;
		}


		if (!$statuses = json_decode($statuses, true))
		{
			errorecho('Could not parse json reply from twitter, continuing..<br>');
			continue; 
		}


	




		if (!empty($statuses))
		{
			if (isset($statuses['meta']['result_count']))
			{
				echo 'Downloaded ' . $statuses['meta']['result_count'] . ' tweets.<br>';

				if ($statuses['meta']['result_count'] == 0)
				{
					echo '<br>----------------------<br><br>';
					continue;
				}


			}

		}


		if (isset($statuses['data']))
		{
			foreach ($statuses['data'] as $index => $tweet)
			{
				$found_switch = 0; 

				if(isset($tweet['attachments']['media_keys']))
				{
					echo 'Media(s) found!<br>';
					$image_urls = [];

					foreach ($tweet['attachments']['media_keys'] as $key)
					{
						if (isset($statuses['includes']['media']))
						{

							foreach ($statuses['includes']['media'] as $media)
							{

								if ($media['media_key'] == $key)
								{
									$image_urls[$key] = $media;
									$found_switch = 1; 
									break;
								}

							}

						}

					}

				}

				//go through media and look for video
				if ($found_switch == 1)
				{
					foreach ($image_urls as $key => $media)
					{

						if ($media['type'] == 'video')
						{

							echo 'Video found<br>';
							$id = $tweet['id'];


							//trigger exception in a "try" block
							try {
								if ($media_data = single_tweet_media_downloader($id))
								{

									$tweet['extended_entities'] = $media_data['extended_entities'];
								}
								else
								{
									errorecho('Error downloading extended media.<br>');
								}
							}

							//catch exception

							catch(Exception $e) 
							{
								echo 'Message: ' . $e->getMessage();
							}							



						}

					}
				}


				

				if ($found_switch == 1)
				{
					$tweet['media'] = $image_urls;
				}


				//look for urls and check if preview needs to be generated
				$file_ending = '';

				if (isset($tweet['in_reply_to_user_id']) or isset($tweet['referenced_tweets']))
				{
					$file_ending .= '_reply';

				}

				//lookup references tweets
				if (isset($tweet['referenced_tweets']))
				{
					$found_switch_referenced = 0;

					$referenced_tweets_box = [];
					foreach ($tweet['referenced_tweets'] as $reference)
					{
						
						if (!$status_referenced = json_decode(get_single_tweet($reference['id']), true))
						{
							errorecho('Could not parse json referenced reply from twitter, continuing..<br>');
							continue; 
						}

						if(isset($status_referenced['data']['attachments']['media_keys']))
						{
							echo 'Media(s) found in referenced tweets!<br>';
							var_dump($reference);
							$image_urls_referenced = [];

							foreach ($status_referenced['data']['attachments']['media_keys'] as $key)
							{
								if (isset($status_referenced['includes']['media']))
								{

									foreach ($status_referenced['includes']['media'] as $media)
									{

										if ($media['media_key'] == $key)
										{
											$image_urls_referenced[$key] = $media;
											$found_switch_referenced = 1; 
											
										}

									}

								}

							}

						}
						else
						{
							echo 'Media keys not set in referenced tweet<br>';
							
						}

						//go through media and look for video
						if ($found_switch_referenced == 1)
						{
							foreach ($image_urls_referenced as $key => $media)
							{

								if ($media['type'] == 'video')
								{

									echo 'Video found<br>';
									

									if ($media_data = single_tweet_media_downloader($reference['id']))
									{

										$status_referenced['extended_entities'] = $media_data['extended_entities'];
									}
									else
									{
										errorecho('Error downloading extended media.<br>');
									}

								}

							}
						}

						if ($found_switch_referenced == 1)
						{
							$status_referenced['media'] = $image_urls_referenced;
						}


						$referenced_tweets_box[$reference['id']] = $status_referenced;				

						https://twitter.com/Scaramucci/status/1419039869444182023/photo/1
					}

					$tweet['referenced_tweets_download'] = $referenced_tweets_box;

				}


				if (isset($tweet['media']) and !isset($tweet['in_reply_to_user_id']))//nok? 
				{
					$file_ending .= '_media';
				}

			

				if(isset($data['referenced_tweets']))
				{
					echo 'isset!';
					foreach ($data['referenced_tweets'] as $ref_tweet)
					{
						echo 'here - ';
						foreach ($statuses['tweets'] as $status_tweet)
						{
							echo 'there';
							if ($status_tweet['id'] == $ref_tweet['id'])
							{
								echo 'found one!';
							}


						}
					}

					

				}	



				if (!file_exists(mb_strtolower($savedirectory . $username . '/tweets/' . $tweet['id'] . $tweet['id'] . $file_ending . '.json')))
				{
					saveJSON($tweet, mb_strtolower($savedirectory . $username . '/tweets/' . $tweet['id'] . $file_ending . '.json'));
				}
				else
				{
					echo 'Already saved file' .  $savedirectory . mb_strtolower($username) . '/tweets/' . $tweet['id'] . $file_ending .  '.json... will update.<br>';

					saveJSON($tweet, mb_strtolower($savedirectory . mb_strtolower($username . '/tweets/' . $tweet['id'] . $file_ending . '.json')));

				}

			}
		}

//make main list of tweets
		$tweets_list = listfiles($savedirectory . mb_strtolower($username) . '/tweets/');

//var_dump($tweets_list);



		foreach ($tweets_list as $key => $tweet)
		{
			$tweets_list[$key] = str_replace('.json', '', $tweet); 
		}

		if (!empty($tweets_list))
		{

			$tweets = [];
			$media = [];
			$replies = [];

			foreach ($tweets_list as $index => $file)
			{

				$array = [];
				$array[] = $tempfile = $file;
				$array[] = $username; 

				$replies[] = $array;

				if (strpos($tempfile, '_reply') !== false) //if found true
				{
					
				}
				else
				{
					$tweets[] = $array;
				}

				if (strpos($tempfile, '_media') !== false) //if found true
				{
					$media[] = $array;
				}


			}	


			usort($tweets, function ($item1, $item2)  
			{
				$temp1 = str_replace("_media,", "", $item1[0]);
				$temp2 = str_replace("_media,", "", $item2[0]);

				if ((int)$temp1 < (int)$temp2)
				{
					return true;
				}
				else
				{
					return false;
				}

			});

			usort($replies, function ($item1, $item2) 
			{
				$temp1 = str_replace("_reply,", "", $item1[0]);
				$temp2 = str_replace("_reply,", "", $item2[0]);

				$temp1 = str_replace("_media,", "", $temp1);
				$temp2 = str_replace("_media,", "", $temp2);    

				if ((int)$temp1 < (int)$temp2)
				{
					return true;
				}
				else
				{
					return false;
				}

			});

			usort($media, function ($item1, $item2) 
			{				

				$temp1 = str_replace("_reply,", "", $item1[0]);
				$temp2 = str_replace("_reply,", "", $item2[0]);

				$temp1 = str_replace("_media,", "", $temp1);
				$temp2 = str_replace("_media,", "", $temp2);   

				if ((int)$temp1 < (int)$temp2)
				{
					return true;
				}
				else
				{
					return false;
				}

			});

			saveCSVx($tweets, mb_strtolower($savedirectory . $username . '/' . $username . '_tweets.csv'));
			saveCSVx($replies, mb_strtolower($savedirectory . $username . '/' . $username . '_replies.csv'));
			saveCSVx($media, mb_strtolower($savedirectory . $username . '/' . $username . '_media.csv'));

			//file_put_contents(mb_strtolower($savedirectory . $username . '/' . $username . '_list.csv'), implode(',' . $username . PHP_EOL, $tweets_list));

		}
		else
		{
			errorecho('empty!');
		}

		if (!empty($statuses))
		{
			saveJSON($statuses, mb_strtolower($savedirectory . $username . '/' . $username . '_latest_download.json'));
		}
		else
		{
			errorecho('Data is empty for user ' . $username . '. Probably no new updates. <br><br>');
		}

		echo '<br>----------------------<br><br>';

	}

}





?>


