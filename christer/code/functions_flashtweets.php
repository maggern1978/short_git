<?php 

function check_if_dir_exists($name)
{
  if (!file_exists(mb_strtolower($name))) 
  {
    if (mkdir(mb_strtolower($name), 0777, true))
    {
      return true;
    }
    else
    {
      return false; 
    }

  }
  else
  {
    return true; 
  }
}

function check_if_tweets_dir_exists($name)
{
  if (!file_exists(mb_strtolower($name) . '/tweets')) 
  {
    if (mkdir(mb_strtolower($name) . '/tweets', 0777, true))
    {
      return true;
    }
    else
    {
      return false; 
    }

  }
  else
  {
    return true; 
  }
}

function check_if_media_dir_exists($name)
{
  if (!file_exists(mb_strtolower($name) . '/media')) 
  {
    if (mkdir(mb_strtolower($name) . '/media', 0777, true))
    {
      return true;
    }
    else
    {
      return false; 
    }

  }
  else
  {
    return true; 
  }
}

function check_if_video_dir_exists($name)
{
  if (!file_exists(mb_strtolower($name) . '/video')) 
  {
    if (mkdir(mb_strtolower($name) . '/video', 0777, true))
    {
      return true;
    }
    else
    {
      return false; 
    }

  }
  else
  {
    return true; 
  }
}

function single_tweet_media_downloader($id)
{
  $settings = array(
    'oauth_access_token' => "1058139584113098752-6o779K154M8loaVx75oG5i2YoKnrsZ",
    'oauth_access_token_secret' => "V9TON0QuCpR7TbfL7uHCvIUQDlXMZj5jMBNY6mZHW1mAC",
    'consumer_key' => "9iYS641QFf9GeDD8tNJV3qg1j",
    'consumer_secret' => "xfBC8K0rVEEIl3KXmq2Jcw0qwRHtZGILCZ5nSSr2jImKbbEWcN"
    );
  $url = 'https://api.twitter.com/1.1/statuses/show.json';

  $getfield = '?id= ' . $id . '&include_entities=true&tweet_mode=extended';
  $requestMethod = 'GET';
  $twitter = new TwitterAPIExchange($settings);

  $statuses2 = $twitter->setGetfield($getfield)
  ->buildOauth($url, $requestMethod)
  ->performRequest();

  if (!$statuses2 = json_decode($statuses2, true))
  {
    errorecho('Could not parse media json reply from twitter, continueing..<br>');
    return false;

  }

  saveJSON($statuses2, 'test.json');

  return $statuses2;


}

function get_tweet($id) {

  $url = 'https://api.twitter.com/2/tweets/' . $id;

  $getfield = '&expansions=attachments.poll_ids,attachments.media_keys,author_id,entities.mentions.username,geo.place_id,in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id';  
  $getfield .= '&media.fields=duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics';
  $getfield .= '&tweet.fields=attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,text,withheld';
  //$getfield .= '&user.fields=created_at,description,entities,id,location,name,pinned_tweet_id,profile_image_url,protected,public_metrics,url,username,verified,withheld';

  $requestMethod = 'GET';

  $settings = array(
    'oauth_access_token' => "1058139584113098752-6o779K154M8loaVx75oG5i2YoKnrsZ",
    'oauth_access_token_secret' => "V9TON0QuCpR7TbfL7uHCvIUQDlXMZj5jMBNY6mZHW1mAC",
    'consumer_key' => "9iYS641QFf9GeDD8tNJV3qg1j",
    'consumer_secret' => "xfBC8K0rVEEIl3KXmq2Jcw0qwRHtZGILCZ5nSSr2jImKbbEWcN"
    );



  $twitter = new TwitterAPIExchange($settings);

  $statuses = $twitter->setGetfield($getfield)
  ->buildOauth($url, $requestMethod)
  ->performRequest();
  
}


function get_single_tweet($id) {

  try 
  {

  //echo 'Getting single tweet!<br>';

  $url = 'https://api.twitter.com/2/tweets/' . $id;

  $getfield = '&expansions=attachments.poll_ids,attachments.media_keys,author_id,entities.mentions.username,geo.place_id,in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id';  
  $getfield .= '&media.fields=duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics';
  $getfield .= '&tweet.fields=attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,text,withheld';
  //$getfield .= '&user.fields=created_at,description,entities,id,location,name,pinned_tweet_id,profile_image_url,protected,public_metrics,url,username,verified,withheld';

  $requestMethod = 'GET';

  $settings = array(
    'oauth_access_token' => "1058139584113098752-6o779K154M8loaVx75oG5i2YoKnrsZ",
    'oauth_access_token_secret' => "V9TON0QuCpR7TbfL7uHCvIUQDlXMZj5jMBNY6mZHW1mAC",
    'consumer_key' => "9iYS641QFf9GeDD8tNJV3qg1j",
    'consumer_secret' => "xfBC8K0rVEEIl3KXmq2Jcw0qwRHtZGILCZ5nSSr2jImKbbEWcN"
    );


  $twitter = new TwitterAPIExchange($settings);

  $statuses = $twitter->setGetfield($getfield)
  ->buildOauth($url, $requestMethod)
  ->performRequest();
  
  }

  catch(Exception $e) 
  {
    echo 'Message: ' .$e->getMessage();
     
    return false;
  }

  return $statuses;
  
}


function include_try($urlforincludetry, $set_mode)
{

  try 
  {
    $starttime_includetry = time();
    echo '<h3 style="color: green;">Starter ' . $urlforincludetry . ' at <span style="color:black;">' . date('Y-m-d H:i:s') . '</span></h3>';
    $mode = $set_mode;
    include $urlforincludetry;
  }

  catch(Exception $e) 
  {
    echo 'Message: ' .$e->getMessage();
    $endtime_includetry = time();
    $difference_includetry = $endtime_includetry - $starttime_includetry;

    echo '<h3 style="color: red;">FEIL: ' . $urlforincludetry . ' at <span style="color:black;">' . date('Y-m-d H:i:s') . '</span> after '  . $difference_includetry . ' seconds </h3>';
    return false;
  }
    $endtime_includetry = time();
    $difference_includetry = $endtime_includetry - $starttime_includetry;
  echo '<h3 style="color: green;">Fullført: ' . $urlforincludetry . ' at <span style="color:black;">' . date('Y-m-d H:i:s') . '</span> after '  . $difference_includetry . ' seconds </h3>';
  return true;
}



function check_for_video($single_tweet)
{

  if (isset($single_tweet['referenced_tweets_download']))
  {
    echo 'Going into referenced_tweets_download<br>';
    foreach ($single_tweet['referenced_tweets_download'] as $index => $downloaded_tweet)
    {
      $single_tweet['referenced_tweets_download'][$index] = check_for_video($downloaded_tweet);
    }
    
  }


  if (isset($single_tweet['includes']['media']))
  {

    foreach ($single_tweet['includes']['media'] as $media)
    {

      if ($media['type'] == 'video')
      {
        echo 'Video found in includes | media. Will download info. ';

        //trigger exception in a "try" block

        $id = $single_tweet['data']['id'];

        try {
          if ($video_data = single_tweet_media_downloader($id))
          {

            $single_tweet['extended_entities'][] = $video_data['extended_entities'];
            
          }
          else
          {
            errorecho('Error downloading extended media.<br>');
          }
        }

        //catch exception

        catch(Exception $e) 
        {
          echo 'Message: ' . $e->getMessage();
        } 



      }

    }

  }



  return $single_tweet;

}



function user_display_list_maker($directory, $username)
{
    //$savedirectory . mb_strtolower($username) . '/tweets/'
    //make main list of tweets
    $tweets_list = listfiles($directory . mb_strtolower($username) . '/tweets/');

    //var_dump($tweets_list);

    foreach ($tweets_list as $key => $tweet)
    {
      $tweets_list[$key] = str_replace('.json', '', $tweet); 
    }

    if (!empty($tweets_list))
    {

      $tweets = [];
      $media = [];
      $replies = [];

      foreach ($tweets_list as $index => $file)
      {

        $array = [];
        $array[] = $tempfile = $file;
        $array[] = $username; 

        $replies[] = $array;

        if (strpos($tempfile, '_reply') !== false) //if found true
        {
          
        }
        else
        {
          $tweets[] = $array;
        }

        if (strpos($tempfile, '_media') !== false) //if found true
        {
          $media[] = $array;
        }


      } 


      usort($tweets, function ($item1, $item2)  
      {
        $temp1 = str_replace("_media,", "", $item1[0]);
        $temp2 = str_replace("_media,", "", $item2[0]);

        if ((int)$temp1 < (int)$temp2)
        {
          return true;
        }
        else
        {
          return false;
        }

      });

      usort($replies, function ($item1, $item2) 
      {
        $temp1 = str_replace("_reply,", "", $item1[0]);
        $temp2 = str_replace("_reply,", "", $item2[0]);

        $temp1 = str_replace("_media,", "", $temp1);
        $temp2 = str_replace("_media,", "", $temp2);    

        if ((int)$temp1 < (int)$temp2)
        {
          return true;
        }
        else
        {
          return false;
        }

      });

      usort($media, function ($item1, $item2) 
      {       

        $temp1 = str_replace("_reply,", "", $item1[0]);
        $temp2 = str_replace("_reply,", "", $item2[0]);

        $temp1 = str_replace("_media,", "", $temp1);
        $temp2 = str_replace("_media,", "", $temp2);   

        if ((int)$temp1 < (int)$temp2)
        {
          return true;
        }
        else
        {
          return false;
        }

      });

      saveCSVx($tweets, mb_strtolower($directory . $username . '/' . $username . '_tweets.csv'));
      saveCSVx($replies, mb_strtolower($directory . $username . '/' . $username . '_replies.csv'));
      saveCSVx($media, mb_strtolower($directory . $username . '/' . $username . '_media.csv'));
    }
    else
    {
      errorecho('empty!');
    }
}


function readCSVtab($file_location_and_name) {

  $csvFile = file($file_location_and_name);

        //Les in dataene 
  $name = [];
  foreach ($csvFile as $line) {
    $name[] = str_getcsv($line, "\t");
  }

        //ordne utf koding
  $counter = 0;
  foreach ($name as &$entries) {
    $data[$counter] = array_map("utf8_encode", $entries);
    $counter++;
  }
  return $name;
}



?>