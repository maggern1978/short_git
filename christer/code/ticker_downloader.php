<?php 

include_once('functions.php');
include_once('functions_flashtweets.php');

if (!isset($mode))
{
	$mode = 'forside'; //normal // bonds //forside
}

$listfiles = listfiles('data/ticker_list/');

if (count($listfiles) > 1)
{
	echo 'Error, too many files in data/ticker_list!!';
}

if (!$tickers = readCSVtab('data/ticker_list/' . $listfiles[0]))
{
	echo 'Error reading ticker list!';
	exit();
}

echo '<h3>Mode er ' . $mode . '</h3>';

$bulkTickerData = [];

//Kode,Navn,Kallenavn
foreach ($tickers as $key => $ticker)
{
	
	if (!isset($ticker[1]) or $ticker[1] == '' or !isset($ticker[2]) or $ticker[2] == '')
	{
		continue;
	}

	if(mb_strtolower($ticker[0]) == 'kode' or mb_strtolower($ticker[1]) == 'navn' or mb_strtolower($ticker[2]) == 'kallenavn' )
	{
		continue;
	}

	if ($mode == 'forside')
	{
		
		if (!isset($ticker[4]) or $ticker[4] == '')
		{
			//echo 'Mode er forside, skipping ' . $ticker[0] . '<br>';
			continue;
		}
		
	}


	$ticker[1] = trim($ticker[1]);
	$ticker[2] = trim($ticker[2]);
	$ticker[0] = trim($ticker[0]);

	$tickers[$key] = $ticker;

	$bulkTickerData[] = $ticker[0];
}



//echo separate bonds

$bondsTickerData = [];

foreach ($bulkTickerData as $key => $ticker)
{
	
	if (strripos($ticker, 'BOND') !== false )
	{
		$bondsTickerData[] = $ticker;
		unset($bulkTickerData[$key]);
	}

}

echo 'Getting ' . count($bulkTickerData) . ' tickers or ' .  count($bondsTickerData)  .  ' bonds<br>';


if ($mode == 'bonds')
{

	if ($download_bonds = ticker_download_bonds($bondsTickerData,''))
	{
		saveJSON($download_bonds,'json/tickers_bonds_all.json');
	}

	foreach ($download_bonds as $key => $bond)
	{
		saveJSON($bond, 'data/tickers/' . mb_strtolower($key) . '.json');
	}

}
else if ($mode == 'normal')
{


	if ($download = ticker_download_eodhistorical($bulkTickerData))
	{
		saveJSON($download,'json/tickers_all.json');
	}


	foreach ($tickers as $key => $ticker)
	{

		if (!isset($ticker[1]) or $ticker[1] == '' or !isset($ticker[2]) or $ticker[2] == '')
		{
			continue;
		}

		if(mb_strtolower($ticker[0]) == 'kode' or mb_strtolower($ticker[1]) == 'navn' or mb_strtolower($ticker[2]) == 'kallenavn' )
		{
			continue;
		}


		$found = 0; 
		foreach ($download as $entry)
		{
			if ($ticker[0] == $entry['code'])
			{
				$entry['name'] = $ticker[2];
				saveJSON($entry, 'data/tickers/' . mb_strtolower($entry['code']) . '.json');
				$found = 1;
				break;
			}
		}

		if ($found == 0 and $mode != 'forside')
		{
			echo '<strong>Error, not found: ' . $ticker[0] . '</strong><br>';
		}
		
	}

}

else if ($mode == 'forside')
{

	if ($download = ticker_download_eodhistorical($bulkTickerData))
	{
		saveJSON($download,'json/tickers_forside_all.json');
	}

	foreach ($tickers as $key => $ticker)
	{

		if (!isset($ticker[1]) or $ticker[1] == '' or !isset($ticker[2]) or $ticker[2] == '')
		{
			continue;
		}

		if (mb_strtolower($ticker[0]) == 'kode' or mb_strtolower($ticker[1]) == 'navn' or mb_strtolower($ticker[2]) == 'kallenavn' )
		{
			continue;
		}


		$found = 0; 
		foreach ($download as $entry)
		{
			if ($ticker[0] == $entry['code'])
			{
				$entry['name'] = $ticker[2];
				saveJSON($entry, 'data/tickers/' . mb_strtolower($entry['code']) . '.json');
				$found = 1;
				break;
			}
		}

		if ($found == 0 and $mode != 'forside')
		{
			echo '<strong>Error, not found: ' . $ticker[0] . '</strong><br>';
		}
		
	}
}



function ticker_download_bonds($tickers, $date)
{

	if ($date == '')
	{
		$date = '&from=2020-01-01';
	}
	else
	{
		$date = '&from=' . $date;
	}

	$starturl = 'https://eodhistoricaldata.com/api/eod/';
	$undurl = '?api_token=5da59038dd4f81.70264028&fmt=json' . $date;

	$holder = [];

	foreach ($tickers as $ticker)
	{
		$url = $starturl . $ticker . $undurl;

		if ($dataticker = (array)json_decode(download($url)))
		{

			$holder[$ticker] = $dataticker;

		}
		else
		{
			echo $ticker . ' download error! Will continue...<br>';
		}
	}

	return $holder;

}

function ticker_download_eodhistorical($ticker)
{
	$starturl = 'https://eodhistoricaldata.com/api/real-time/';
	$undurl = '?api_token=5da59038dd4f81.70264028&fmt=json';


            //find out if one or several tickers are requested

	if (is_array($ticker))
	{

		$ticker = array_unique($ticker);
		$ticker = array_values($ticker);

                //https://eodhistoricaldata.com/api/real-time/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json&s=VTI,EUR.FOREX
		$count = count($ticker);

		$maxNumbersDownload = 20;

		$downloadrounds = ceil($count/$maxNumbersDownload);
		echo '<br>';
		echo 'Multi download of ' . $count  . ' tickers. '; 
		echo 'Will download ' . $maxNumbersDownload . ' tickers at the time, in ' . $downloadrounds . ' round(s).<br>';

		$x = 0;

		$urlarray = [];

		while($downloadrounds > $x)     
		{

			$tickernumberstart = $maxNumbersDownload * $x;
			$tickernumberend = ($maxNumbersDownload * $x) + $maxNumbersDownload;


			if ($tickernumberend > $count)
			{
				$tickernumberend = $count;
			}

                //echo 'Start: ' . $tickernumberstart . '. End: ' . $tickernumberend . ' (minus 1)<br>';

                    //building the url
			for ($z = $tickernumberstart; $z < $tickernumberend; $z++)
			{

				if (!isset($ticker[$z]))
				{
                        //echo 'Z is ' . $z . '<br>';
					continue;
				}
				else
				{
                        //echo 'Z is ' . $z . '<br>';
				}

				if ($z == $tickernumberstart)
				{
					$url = 'https://eodhistoricaldata.com/api/real-time/' . $ticker[$tickernumberstart] . '?api_token=5da59038dd4f81.70264028&fmt=json&s=';
				}
				else if ($z == $tickernumberend - 1)
				{
					$url .= $ticker[$z];
				}
				else
				{
					$url .= $ticker[$z] . ',';
				}

			}

			$urlarray[] = $url;
                    //echo $url . '<br>';

			$x++;
		}

		$dataholdertemp = [];

		foreach ($urlarray as $key => $url)
		{

			echo $url . '<br>';

			if ($datatickers = (array)json_decode(download($url)))
			{

				if (isset($datatickers[0]))
				{

					foreach ($datatickers as $tickercompany)
					{

						$data = (array)$tickercompany;

						if ($data = ticker_download_eodhistorical_tester($data))
						{
							$dataholdertemp[] = $data;   
						}
					}
				}
				else
				{

					$data = $datatickers;

					if ($data = ticker_download_eodhistorical_tester($data))
					{
						$dataholdertemp[] = $data;   
					}

				}   

			}  
		}

		return $dataholdertemp;

	}

	else 
	{
                //single quoute 
		$url = $starturl . $ticker . $undurl;

		if ($data = (array)json_decode(download($url)))
		{

			$data = ticker_download_eodhistorical_tester($data);
			return $data;

		}
		else
		{
			echo('Error downloading from eodhistorical<br>');
			return false;
		}
	}

}



function ticker_download_eodhistorical_tester($datainput)
{   

	if ('NA' === $datainput['timestamp'] || 'NA' === $datainput['volume'] || 'NA' === $datainput['close'] || 'NA' === $datainput['previousClose'])
	{
		echo('Error in ticker_download_eodhistorical_tester, data is missing. Ticker: ' . $datainput['code'] . '<br>');
        //var_dump($datainput);

		$ticker = $datainput['code'];
		return false;

	}
	else
	{
            //format to correct timezone
		$datainput['timestamp'] = date("Y-m-d H:i:s", $datainput['timestamp']);
            // create a $dt object with the UTC timezone
		$dt = new DateTime($datainput['timestamp'], new DateTimeZone('UTC'));
            // change the timezone of the object without changing it's time
		//$dt->setTimezone(new DateTimeZone('Europe/Oslo'));
            // format the datetime
		$dt->format('Y-m-d H:i:s');
		$datainput['timestamp'] = $datainput['timestamp'];
		$datainput['date'] = (string)$dt->format('Y-m-d');
		$datainput['time'] = (string)$dt->format('H:i:s');

		return $datainput;
	}
}


function saveJSON($data, $filename) {

	if ($fp = fopen($filename, 'w'))
	{
		fwrite($fp, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		fclose($fp); 
		echo "Saved file " . $filename . "<br>";
	}
	else
	{
		return false;
	}
}


function readCSVkomma($file_location_and_name) {

	$csvFile = file($file_location_and_name);

        //Les in dataene 
	$name = [];
	foreach ($csvFile as $line) {
		$name[] = str_getcsv($line, ',');
	}

        //ordne utf koding
	$counter = 0;
	foreach ($name as &$entries) {
		$data[$counter] = array_map("utf8_encode", $entries);
		$counter++;
	}
	return $name;
}



function download($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSLVERSION,1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 200);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$data = curl_exec($ch);
	$error = curl_error($ch); 

	if ($error != '') {
		var_dump($error);
		return false;
	}
	curl_close ($ch);
	return $data;
}

    function listfiles($path)
    {
        if (file_exists($path))
        {
          $files = array_diff(scandir($path), array('.', '..'));
          $files = array_values($files);
          return $files;
      }
      else
      {
        return false;
        
    }
}

?>