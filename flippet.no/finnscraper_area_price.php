<?php

require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

set_time_limit(1000);

$date = date('Y-m-d');

while (!file_exists('json/joined/'. $date . '.json'))
{

	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	echo 'Trying ' . 	'json/joined/'. $date . '.json' . '<br>';
	if ($date < '2020-01-01')
	{
		echo 'Json files not found, returning...<br>';
		return;
	}
}

if (!$json = readJSON('json/joined/' . $date . '.json'))
{
	echo 'json/joined/' . $date . '.json is not set<br>';
	return;
}

echo 'Processing file ' . 'json/joined/' . $date. '.json' . '<br>';

$filename = "download/areas/" . $date . "/";

if (!file_exists($filename)) 
{
	mkdir("download/areas/" . $date, 0777);
	echo "The directory download/areas/ $date was successfully created.<br>";

} 

//clean the others
$files = listfiles('download/areas/');

foreach ($files as $file)
{
	if ($file == $date)
	{
		continue;
	}
	else
	{

		$localfiles = delete('download/areas/' . $file);

		
	}
	
}


$areas_box = [];
$property_type_box = [];

foreach ($json as $key => $apartment)
{

	if (!isset($apartment['area']))
	{
		continue; 
	}

	if ($apartment['area'] == 'Oslo') //should not be oslo...
	{
		continue; 
	}

	$areas_box[] = $apartment['area'];

	if (isset($apartment['property type']))
	{
		$property_type_box[] = $apartment['property type'];
	}
	
}

$areas_box = array_unique($areas_box);
$areas_box = array_values($areas_box);

$property_type_box = array_unique($property_type_box);
$property_type_box = array_values($property_type_box);


//bygg opp en rekke id'er som du vil sjekke

foreach ($property_type_box as $property_type)
{
	

	$filename = "download/areas/" . $date . "/" . $property_type . '/';

	if (!file_exists($filename)) 
	{
		mkdir($filename, 0777);
		echo "The directory $filename was successfully created.<br>";
	} 

	echo 'Doing ' . $property_type . '<br>';

	foreach ($areas_box as $key => $area)
	{

		//finn en leilighet med areas
		
		echo '<br>------------------------------<br>';
		echo '<strong>'. $key . '. Trying to find ' . $area . ' | ' . $property_type . ': </strong><br>';

		$success = 0;
		$foundcount = 0;

		foreach ($json as $index => $apartment)
		{
			

		if ($success == 1 and $foundcount > 2) //do a few of each area to make sure that data for area is captured.
		{
			break;
		}		

		if (isset($apartment['property type']) and isset($apartment['area']) and $apartment['area'] == $area and mb_strtolower($apartment['property type']) == $property_type)
		{
			//echo '<br>Area found nr: ' . $index . ' '; 
			//check if file already is downloaded
			$filename = 'download/areas/' . $date . '/' . $property_type . '/' . $apartment['id'] . '.html';

			if (file_exists($filename))
			{
				echo $index . '. '. $filename . ' already exists, skipping to next...<br>';
				$foundcount++;
				$success = 1;
				continue;
			}
			else
			{
				//check if download is fine 
				$starturl = 'https://www.finn.no/prisstatistikk/';
				$url = $starturl . $apartment['id'];

				echo $url;

				if ($data = download($url))
				{
					
					if (!stripos($data, 'Beklager, nå er det rusk i maskineriet') and !stripos($data, 'Ingen treff akkurat nå'))
					{
						$filename = 'download/areas/' . $date . '/' . $property_type . '/' . $apartment['id'] . '.html';
						echo 'Saving ' . $filename . ' (' . $area .  ')'. '. ';

						file_put_contents($filename, $data);
						$random = random_int(3, 10);
						echo ' | Sleeping for ' . $random . ' seconds.<br>';
						sleep($random);
						$success = 1;
						$foundcount++;
					}
				}
			}

		}
	}

	if ($success == 0)
	{
		errorecho('Error downloading any files for area ' . $area . ' and property type ' . $property_type . '<br>');
	}

	


}

}





?>