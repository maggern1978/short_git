<?php

include 'bit_head.html';

?>

<div class="container">
	<div class="row">
		<div class="col-12 mt-2">
			<h2>Visninger - legg til</h2>
			<form id="my_form_id" action="showings_form_receiver.php" method="post">
				<div class="d-flex flex-column">

					Link til annonse:* <input id="id" type="text" name="link"><br>
					Salgspris etter budrunde: <input id="price" type="text" name="price"><br>
					Kommentarer: <input id="comment" type="text" name="comment"><br>
					<input type="submit" value="Send inn">
				</form>
				*obligatorisk. Hele URL'en.
			</div>
		</div>
	</div>
	<div id='result' class=""></div>
	<p id="timer"></p>
	<div id='result2' class=""></div>

	<script>
		$(document).ready(function(){
			$('#my_form_id').on('submit', function(e){
                    //Stop the form from submitting itself to the server.
                    e.preventDefault();
                    var id = $('#id').val();

                    if (id.length < 2)
                    {
                    	$('#result').html('Error, must contain id');
                    	javascript_abort();
                    }

                    var comment = $('#comment').val();
                    var price = $('#price').val();

                    $.ajax({
                    	type: "POST",
                    	url: 'showings_form_receiver.php',
                    	data: {
                    		id: id,
                    		comment: comment,
                    		price: price
                    	},
                    	success: function(data)
                    	{
                    		$('#result').html('<h3 class="mt-3">Laster ned, vennligst vent da dette kan ta et par minutter...</h3><br>');
                    		$('#result2').html('');

							// Set the date we're counting down to
							var counter = 0;

								// Update the count down every 1 second
								var x = setInterval(function() {

							  // Get today's date and time
							  counter++;


							  // Display the result in the element with id="demo"
							  document.getElementById("timer").innerHTML = '<h3>Tid: ' + counter + ' sekunder</h3>';

							  // If the count down is finished, write some text
							  if (counter < 0) {
							  	
							  	document.getElementById("timer").innerHTML = "EXPIRED";
							  }
							}, 1000);


                    		$.ajax({
                    			type: "GET",
                    			url: 'finnscraper_dl_single_details.php',
                    			data: {
                    				id: data,
                    			},
                    			success: function(data)
                    			{
                    				
                    				$('#result2').html('<h3 class="mt-3">Nedlasting ferdig. Resultat:</h3><br>' + data);
                    				clearInterval(x);

                    			}
                    		}); 

                    	}
                    });




                });
		});
		function javascript_abort()
		{
			throw new Error('Error. Must contain id!');
		}
	</script>

	<!-- Display the countdown timer in an element -->


	<script>



</script>

</body>
</html>

