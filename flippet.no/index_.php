<!DOCTYPE html>
<html lang="en">
<head>
	<title>Leiligheter | flippet.no</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" />
</head>
<style type="text/css">
	
	.badgetext 
	{
		font-size: 20px;
	}

</style>
<body>

	<!-- Grey with black text -->
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<div class="container">
			<a class="navbar-brand" href="index_.php"><span class="font-weight-bold h2">FLIPPET.no </span><span clasS="d-none d-sm-none d-md-inline">- Fine oppussinger og raske penger</span></a>
			<ul class="navbar-nav">
				<li class="nav-item text-white" data-toggle="tooltip"> 
					<a class="nav-link" href="om.php">Om</a>
				</li>
			</ul>
		</div>
	</nav>

	<?php

	require_once('functions.php');	

	if (!empty($_GET["start"])) 
	{
		$start_offset = test_input($_GET["start"]);
		//echo $date;
	}
	else
	{
		$start_offset = 0;
	}

	if (!empty($_GET["show"])) 
	{
		$show_number = test_input($_GET["show"]);
		//echo $date;
	}
	else
	{
		$show_number = 10;
	}

	//get latest average prices
	$average_date = date('Y-m-d');


	if (!$average_prices = readJSON('json/areas/average_prices.json'))
	{
		//echo 'Read error of' . 'json/areas/average_prices_' .  $average_date . '.json' . '<br>';
	}

	//reading index data



	if (!$start_count = readJSON('published/start_count.json'))
	{
		//$start_offset = 0;
		echo 'Could not read start index<br>';
		return;
	}
	else
	{
		$start_count = $start_count['nr'];
	}

	//leser inn leilighetene

	$indexdata = [];

	$starting_point = $start_count - $start_offset;

	$rounds = 0;

	for ($i = $show_number; $i > 0; $i--)
	{
		$file = $starting_point - ++$rounds ;

		if ($file < 0)
		{
			break;
		}

		$filename = 'published/apartments/' . $file . '.json';
		//echo $filename . '<br>';

		if (file_exists($filename) and $data = readJSON($filename))
		{
			$indexdata[] = $data;
		}

	}

	$next_number = $start_count - ($starting_point - $show_number);
	$previous_number = $start_count - ($starting_point + $show_number);


	//regn ut sidetall
	$number_of_pages = ceil($start_count / $show_number);
	$display_start = (($start_offset / $show_number) * $show_number) + 1 ;
	$display_end = ((($start_offset / $show_number) * $show_number)) + $show_number;

	if ($display_end > $start_count)
	{
		$display_end = $start_count;
	}

	if ($display_start > $start_count)
	{
		$display_start = $start_count;
	}
	

	?>

	<div class="container mt-3 pb-3">
		<div class="row ">
			<div class="col-12 mb-1 ">
				<div class="d-flex justify-content-between border-bottom">
					<ul class="pagination justify-content-center" style="margin:20px 0">
						<?php 

						if ($previous_number < 0)
						{
							?>
							<li class="page-item disabled"><a class="page-link" href="<?php echo 'index_.php?start=' . $previous_number . '&show=' . $show_number; ?>">Forrige</a></li>

							<?php

						}
						else
						{
							?>
							<li class="page-item"><a class="page-link" href="<?php echo 'index_.php?start=' . $previous_number . '&show=' . $show_number; ?>">Forrige</a></li>
							<?php
						}

						?>

						<?php 
						if ($start_count > $next_number )
						{
							?>
							<li class="page-item"><a class="page-link" href="<?php echo 'index_.php?start=' . $next_number . '&show=' . $show_number; ?>">Neste</a></li>

							<?php
						}
						else
						{

							?>

							<li class="page-item disabled "><a class="page-link" href="#">Neste</a></li>

							<?php
						}
						?>
						<?php

						?>
						<li class="my-auto ml-3 d-none d-sm-none d-md-block">Viser <?php echo $display_start; ?>  til <?php echo $display_end; ?> av totalt <?php echo $start_count; ?> oppføringer</li>
					</ul>
					<div class="my-auto ">
						<div class="d-flex">

							<div class="mr-2">Vis:
							</div>
							<div class="mr-2"><a href="index_.php?start=<?php echo 0 . '&show=10' ?>">10</a>
							</div>
							<div class="mr-2"><a href="index_.php?start=<?php echo 0 . '&show=20' ?>">20</a>
							</div>														
							<div class="mr-2"><a href="index_.php?start=<?php echo 0 . '&show=50' ?>">50</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php 
	foreach ($indexdata as $key => $leilighet) 
	{ 
		?><div class="container mb-4">
		<div class="row ">
			<?php

			$index = $key + 1;

			if (isset($leilighet['property type']) and $leilighet['property type'] == 'garasje/parkering')
			{
				continue;
			}

			if (!isset($leilighet['totalprice'])) 	
			{
				$leilighet['totalprice'] = $leilighet['price'];
			}

			if (!isset($leilighet['common costs'])) 	
			{
				$leilighet['common costs'] = '-';
			}

			if (!isset($leilighet['property type'])) 	
			{
				$leilighet['property type'] = '-';
			}

			if ($start = strpos($leilighet['price'], '-'))
			{
				$leilighet['price'] = substr($leilighet['price'], $start+1);
			}

			if ($start = strpos($leilighet['totalprice'], '-'))
			{
				$leilighet['totalprice'] = substr($leilighet['totalprice'], $start+1);
			}

			if (!isset($leilighet['size']))
			{
					//var_dump($leilighet);
					//continue;
			}

			if (isset($leilighet['size']))
			{

				if ($start = strpos($leilighet['size'], '-'))
				{
					continue;
				}

			}

			$today = date('Y-m-d');
				//$timelimit = date('Y-m-d', strtotime($limit, strtotime($today)));
				//var_dump($timelimit);



			if ($leilighet['property type'] == 'Garasje/Parkering' or $leilighet['property type'] == 'andre')
			{
				continue;
			}

				$average = $leilighet['totalprice'] / $leilighet['size']; //brukes under

				//find out if part of apartment building
				if ($leilighet['ownership_repeat'] > 1) //mer enn en bør stemme
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}

				else if ($leilighet['ownership_repeat'] == 1 and $leilighet['ownership_count'] == 1) //to like oppføringer, bør stemme
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}
				else if ($leilighet['ownership_repeat'] == 0 and $leilighet['ownership_count'] == 1) //bare en oppføring, bør stemme				{
					{
						$badgecolor = 'success';
						$badgetext = 'Flippet';
					}												
					else if ((int)$leilighet['ownership_repeat'] == 1 and (int)$leilighet['ownership_count'] > 4)
					{

						$price_limit = 1.1;

						if (isset($average_prices[$leilighet['property type']][$leilighet['area']]))
						{

							$ratio = (float)$average / (float)$average_prices[$leilighet['property type']][$leilighet['area']];

							$price_limit_percent = round(($ratio -1)*100,1);

						if ($ratio > $price_limit) //only include if at least 10% over average price in area pr square meter
						{
							$badgecolor = 'primary';
							$badgetext = 'Ukjent, del av boligkompleks ('. $price_limit_percent .  '% over snittpris)';
						}
						else
						{
							continue;
						}
					}
					else
					{
						continue;
					}

				}
				else if (($leilighet['ownership_repeat'] / $leilighet['ownership_count']) > 0.7)
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}				
				else
				{
					
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}

				//$badgetext .= ' -> ' . $leilighet['ownership_count'];

				

				?>

				<div class="col-12 col-sm-12 col-md-4 "><a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $leilighet['id']; ?>"> 
					<img src="<?php echo $leilighet['imgurl'] ;?>" width="100%" class="img-responsive">
				</a>
			</div>
			<div class="col-12 col-sm-12 col-md-8 ">
				<div class="d-flex flex-column ">
					<div class="d-flex justify-content-between">
						<div class="mb-auto">
							<span class="font-weight-bold">Adresse:</span>
							<?php 
							echo $leilighet['adress']; 

							if (isset($leilighet['area']))
							{
								echo ' | ' . $leilighet['area']; 
							}
							
							if (isset($leilighet['fylke']))
							{
								echo ' | ' . $leilighet['fylke']; 
							} 

							?>
						</div>
						<div class="d-none d-sm-none d-md-block">
							<?php 

							if (isset($leilighet['first_spotted']))
							{
								?>

								<div class="" data-toggle="tooltip" title="Først sett" ><i class="fa fa-clock-o mr-1" aria-hidden="true"></i><?php echo $leilighet['first_spotted']; ?></div>		
								<?php 
							}
							?>
						</div>
					</div>

					<div class="mt-1">
						<h4><a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $leilighet['id']; ?>"> <?php echo $leilighet['title']; ?></a></h4>
					</div>

					<div class="d-flex justify-content-between mb-2 mt-2 ">
						<div class="d-flex flex-column">
							<div class="">	
							<span class="d-none d-sm-inline">Pris pr. kvadrat:</span>
							<span class="d-inline d-sm-none">Pris pr. m²:</span>	
								
							</div>						
							<div class="">	
								<span class="h5" data-toggle="tooltip" title="Snittpris basert på totalpris / antall kvadrat.">
									<?php

									echo number_format($average,0,'.','.')  . ',-';
									?>
								</span>
							</div>
							<?php
							if (isset($leilighet['area']) and isset($average_prices[$leilighet['property type']][$leilighet['area']]))
							{
								?>
								<div class="text-secondary" data-toggle="tooltip" title="Snittpris pr. m² i området <?php echo $leilighet['area'] . ' for ' . $leilighet['property type']; ?>">
									<?php 

									if ($average_prices[$leilighet['property type']][$leilighet['area']] > $average)
									{
										echo '<i class="fa fa-arrow-circle-down mr-1" aria-hidden="true"></i>';
									}
									else
									{
										echo '<i class="fa fa-arrow-circle-up mr-1" aria-hidden="true"></i>';
									}

									echo number_format($average_prices[$leilighet['property type']][$leilighet['area']],0,'.','.') . ',- ' ; 
									?>
								</div>
								<?php
							}
							?>

						</div>
						<div class="d-flex flex-column">
						<div>Størrelse:
						</div>
						<div class="text-right">
							<h5>
								<?php

								echo number_format($leilighet['size'],0,'.','.')  . ' m²';
								?>
							</h5>
						</div>
						</div>								
						<div class="d-flex flex-column">
						<div class="text-right">
						Prisantydning: 
						</div>
						<div class="text-right">

							<h5>
								<?php

								echo number_format($leilighet['price'],0,'.','.')  . ',-';
								?>
							</h5>
							<?php
							$badgefontsize = '';

							if ($badgetext == 'Flippet')
							{
								$addedprice = $leilighet['price'] - $leilighet['ownership']['tinglysninger'][0]['pris'];

								$addedprice = number_format($addedprice,0,'.','.');

								if ($addedprice > 0)
								{
									$badgetext = ' +' . $addedprice . ',-';
								}
								else if ($addedprice == 0)
								{
									$badgecolor = 'secondary';
									$badgetext = '';
									$badgetext .= '';
								}
								else
								{
									$badgecolor = 'danger';
									$badgetext = '';
									$badgetext .= ' ' . $addedprice . ',-';
								}
								$badgefontsize = 'badgetext';
							}

							?>
							<span data-toggle="tooltip" title="Prisendring. Forrige salgspris var <?php echo ''; ?>" class="badge  <?php echo $badgefontsize; ?>  text-white bg-<?php echo $badgecolor; ?>"><?php echo $badgetext; ?></span>							
						</div>
						</div>
					</div>

					<div class=""><span class="font-weight-bold">Totalpris:</span> <?php echo number_format($leilighet['totalprice'],0,'.','.') . ' kroner';
						echo ' | ' . ucwords($leilighet['property type']) . ' | ' . ucwords($leilighet['legal type']);
						?>
					</div>
					<div class="d-flex justify-content-between">
						<div class="">
							<span class="font-weight-bold"><a data-toggle="modal" data-target="#modal_<?php echo $leilighet['id']; ?>" href="">Siste tinglysning:</a></span>

							<!-- Modal -->
							<div class="modal" id="modal_<?php echo $leilighet['id']; ?>">
								<div class="modal-dialog" >
									<div class="modal-content">

										<!-- Modal Header -->
										<div class="modal-header">
											<h4 class="modal-title">Eierskiftehistorikk - <?php echo $leilighet['adress']; ?></h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<?php

											foreach ($leilighet['ownership'] as $key => $value)
											{
												if ($key == 'tinglysninger')
												{
													break;
												}
												echo ucwords($key) . ': ' . $value . '<br>';
											}


											?>

											<table class="table mt-2">
												<thead>
													<tr>
														<?php
														echo '<th>';
														echo 'Tinglyst: '; 
														echo '</th>';
														echo '<th>';
														echo 'Boligtype: '; 
														echo '</th>';
														echo '<th class="text-right">';
														echo 'Andelsnummer: '; 
														echo '</th>';
														echo '<th class="text-right">';
														echo 'Pris: '; 
														echo '</th>';
														?>

													</tr>
												</thead>										
												<tbody>

													<?php

													foreach ($leilighet['ownership']['tinglysninger'] as $key => $tinglysning)
													{
														?>
														<tr>

															<?php

															if (isset($tinglysning['tinglyst']))
															{
																echo '<td style="white-space: nowrap;">';
																echo '' . $tinglysning['tinglyst']; 
																echo '</td>';
															}
															else
															{
																echo '<td>';														
																echo '</td>';
															}

															if (isset($tinglysning['boligtype']))
															{
																echo '<td>';
																echo '' . $tinglysning['boligtype']; 
																echo '</td>';
															}
															else
															{
																echo '<td>';														
																echo '</td>';
															}														

															if (isset($tinglysning['andelsnummer']))
															{
																echo '<td class="text-right">';
																echo '' . $tinglysning['andelsnummer']; 
																echo '</td>';
															}
															else
															{
																echo '<td>';														
																echo '</td>';
															}

															if (isset($tinglysning['pris']))
															{
																echo '<td class="text-right">';
																echo '' . number_format($tinglysning['pris'],0,',','.'); 
																echo '</td>';
															}
															else
															{
																echo '<td>';														
																echo '</td>';
															}	
															?>

														</tr>
														<?php
													}

													?>

												</tbody>
											</table>

										</div>
										<!-- Modal footer -->
										<div class="modal-body border-top">
											<div class="d-flex justify-content-between">
											<div class="my-auto">
												<a href="https://www.finn.no/realestate/ownershiphistory.html?finnkode=<?php echo $leilighet['id']; ?>">Link</a> til Finn.no's eierskiftehistorikk
											</div>
											<div>
											<button type="button" class="btn btn-danger" data-dismiss="modal">Lukk</button>
											</div>
											
										</div>
										</div>
									</div>
								</div>
							</div>	

							<?php 
							$earlier = new DateTime("today");
							$later = new DateTime($leilighet['tinglyst_newest']);

							$interval = $later->diff($earlier);

							$datestring = '';

							if ($interval->format('%y') > 0)
							{
								$datestring .= $interval->format('%y') . ' år, ';

								if ($interval->format('%m') == 1)
								{
									$datestring .= $interval->format('%m'). ' måned og ';

								}
								else if ($interval->format('%m') == 0)
								{


								}									
								else
								{
									$datestring .= $interval->format('%m'). ' måneder og ';

								}

								if ($interval->format('%d') == 1)
								{

									$datestring .= $interval->format('%d') . ' dag';
								}
								else
								{

									$datestring .= $interval->format('%d') . ' dager';								
								}
							}

							else if ($interval->format('%m') > 0)
							{
								if ($interval->format('%m') == 1)
								{
									$datestring .= $interval->format('%m'). ' måned og ';

								}
								else
								{
									$datestring .= $interval->format('%m'). ' måneder og ';

								}

								if ($interval->format('%d') == 1)
								{

									$datestring .= $interval->format('%d') . ' dag';
								}
								else
								{

									$datestring .= $interval->format('%d') . ' dager';								
								}

							}
							else
							{
								if ($interval->format('%d') == 1)
								{

									$datestring .= $interval->format('%d') . ' dag';
								}
								else
								{

									$datestring .= $interval->format('%d') . ' dager';								
								}


							}

							echo $leilighet['tinglyst_newest'] . ' (' .  $datestring . ' siden)';

							?>
						</div>

					</div>
				</div>
			</div>

			
		</div>
	</div>
	<?php } //var_dump($indexdata[0]); ?>
	<ul class="pagination justify-content-center" style="margin:20px 0">
		<?php 

		
		if ($previous_number < 0)
		{
			?>
			<li class="page-item disabled"><a class="page-link" href="<?php echo 'index_.php?start=' . $previous_number ; ?>">Forrige</a></li>

			<?php

		}
		else
		{
			?>
			<li class="page-item"><a class="page-link" href="<?php echo 'index_.php?start=' . $previous_number . '&show=' . $show_number; ?>">Forrige</a></li>

			<?php
		}

		?>

		<?php

		//display three boxes each way, if possible
		$number_of_pages = $start_count / $show_number;

		//current page is:
		$current_position = ceil(($start_offset + 2) / $show_number); 

		$skipcounter = 0;

		//find out if current positions needs adding in front

		if ($number_of_pages % 2 == 0)
		{
			$remaining_pages = $number_of_pages - $current_position;
		}
		else
		{
			$remaining_pages = $number_of_pages + 1 - $current_position;
		}
		

		//echo $remaining_pages;	

		$start_offset = 4; //number in front of middle

		if ($remaining_pages < 3)
		{
			$start_offset = 7 - $remaining_pages ;
		}

		for ($x = $current_position - $start_offset; $x < $current_position; $x++)
		{
			
			$x = ceil($x);

			$runner = $x + 1;

			if ($runner < 1)
			{
				$skipcounter++;
				continue;
			}

			?>
			
			<?php
			
			if ($runner == $current_position)
			{
				break;
			}
			
			//$next_local =  ceil(($runner  * $show_number));

			$next_local =  ($x * $show_number);

			//echo $x;

			$runner = floor($runner);

			?>
			<li class="page-item"><a class="page-link" href="<?php echo 'index_.php?start=' . $next_local . '&show=' . $show_number;  ?>"><?php echo $runner ; ?></a></li>
			<?php 
			
		}

		?>

		<?php 
		if ($current_position != 0)
		{
			?>

			<li class="page-item active font-weight-bold bg-primary"><a class="page-link disabled" href="#"><?php echo $current_position; ?></a></li>

			<?php

		}
		else
		{
			?>
			<li class="page-item active font-weight-bold bg-success text-white"><a class="page-link disabled" href="#"><?php echo $current_position; ?></a></li>
			<?php
		}

		$round = 1;

		for ($x = $current_position; $x < $current_position + 3 + $skipcounter; $x++)
		{
			
			$runner = $x + 1;

			?>
			
			<?php
			
			if ($runner == $current_position)
			{
				break;
			}
			

			$next_local =  ($x * $show_number);

			//echo $x;

			if ($next_local >= $start_count)
			{
				break;
			}

			?>
			<li class="page-item"><a class="page-link" href="<?php echo 'index_.php?start=' . $next_local . '&show=' . $show_number; ?>"><?php echo $runner; ?></a></li>
			<?php 
			
		}

		?>

		<?php 
		if ($start_count > $next_number )
		{
			?>
			<li class="page-item"><a class="page-link" href="<?php echo 'index_.php?start=' . $next_number . '&show=' . $show_number;  ?>">Neste</a></li>

			<?php
		}
		else
		{

			?>

			<li class="page-item disabled "><a class="page-link" href="#">Neste</a></li>

			<?php
		}
		?>


	</ul>
	<div class="container mt-3 mb-3">
		<div class="row">
			<div class="col-12 text-center">Sist oppdatert: <?php echo date('Y-m-d | H.i', filemtime('published/start_count.json')) ;?>
			</div>
		</div>
	</div>

	<script type="text/javascript">

// Select all elements with data-toggle="tooltips" in the document
$('[data-toggle="tooltip"]').tooltip();

// Select a specified element
$('#myTooltip').tooltip();

</script>
</body>
</html>