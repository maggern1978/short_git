<?php 
require_once('functions.php');	

if (!empty($_GET["show"])) 
{
  $time = ($_GET["show"]);
  //echo $date;
}
else
{
	$time = 24;
}

if (!empty($_GET["sort"])) 
{
  $sort = ($_GET["sort"]);
  //echo $date;
}
else
{
	$sort = 'pulished';
}

if (!empty($_GET["dato"])) 
{
	$date = test_input($_GET["dato"]);
	//echo $date;
}
else
{
	$date = date('Y-m-d');
}

$limit_date_future = date('Y-m-d', strtotime("+90 days", strtotime($date)));
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

if (!empty($_GET["modus"])) 
{
	$mode = test_input($_GET["modus"]);
	//echo $date;
}
else
{
	$mode = 'bakover';
}


?>

<?php include 'bit_head.html'; ?>
<?php include 'bit_kontrollpanel.html'; ?>
<body>

	<?php



	

	if (!$mode == 'bakover' or !$mode == 'fremover' )
	{
		$mode = 'bakover';
	}


	if ($mode == 'bakover')
	{
		while (!file_exists('json/joined/'. $date . '.json'))
		{
			//echo $date . '<br>';
			$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

			if ($date < $limit_date_previous)
			{
				echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
				return;
			}
		}		
	}

	if ($mode == 'fremover')
	{
		while (!file_exists('json/joined/'. $date . '.json'))
		{

			$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));

			if ($date > $limit_date_future)
			{
				echo 'Json files not found, returning... Ingen tidligere oppføringer.<br>';
				return;
			}
		}		
	}

	///echo $mode . ' | ' . $date . '<br>';


	if (!$indexdata = readJSON('json/joined/' .  $date . '.json'))
	{
		echo 'Read error of ' . 'json/joined/' .  $date . '.json' . '<br>';
	}

	$objectsforsale = '';

	if (!$indexdataRipped = readJSON('json/index/' .  $date . '.json'))
	{
		echo 'Notice: Read error of ' . 'json/index/' .  $date . '.json' . '<br>';
	}
	else
	{
		$objectsforsale = count($indexdataRipped);
	}

	//get latest average prices

	if (!$average_prices = readJSON('json/areas/average_prices.json'))
	{
		echo 'Read error of' . 'json/areas/average_prices.json' . '<br>';
	}


	//$time = 24;
	$limit = '-' . $time . ' months';

	$updated = date('Y-m-d | H:i', filemtime('json/joined/'. $date . '.json'));
	$updated2 = date('Y-m-d', filemtime('json/joined/'. $date . '.json'));

	$nextdate = date('Y-m-d', strtotime("+1 day", strtotime($date)));
	$previousdate = date('Y-m-d', strtotime("-1 day", strtotime($date)));


	//set sorting, published er default

	if ($sort == 'price_descending')
	{
		
		//sort by biggest flips
		usort($indexdata, function ($item1, $item2) 
		{
			return $item2['price_change'] < $item1['price_change'];
		});
	}
	else if ($sort == 'price_ascending')
	{
	
		//sort by biggest flips
		usort($indexdata, function ($item1, $item2) 
		{
			return $item2['price_change'] > $item1['price_change'];
		});
	}
	?>

	<div class="container mt-4">
		<div class="row">
			<div class="col-12 mb-3">
				<h3>Leiligheter på Finn.no i Oslo til salgs nå - Dato: <?php echo $date; ?></h3>
				<p>Utvalg: Til salgs nå, bruktbolig, Oslo. Sortering: Nyeste annonsedato. Tanken er at tinglysninger under 1 år indikerer en flippet leilighet.<br>
					<?php 
					echo 'Utvalget består av ' . $objectsforsale . ' eiendommer, hvorav ' . count($indexdata) . ' har eierinfo. ' ;
					echo 'Tidsgrense tinglysning: ' . $time . ' måneder. ';
					echo  'Det er ingen justeringer for boligprisstigning.';
					?>
				</p>
			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-12 mb-3 ">
				<div class="d-flex flex-row">
					<div class="mr-3 border-right  pr-3">
						<a href="<?php echo 'flipped_panel.php?dato=' . $nextdate . '&modus=fremover'; ?>&show=<?php echo $time; ?>&sort=<?php echo $sort; ?>">Nyere dato</a>
					</div>
					<div class="">
						<a href="<?php echo 'flipped_panel.php?dato=' . $previousdate . '&modus=bakover'; ?>&show=<?php echo $time; ?>&sort=<?php echo $sort; ?>">Eldre dato</a> 
					</div>

				</div>
				<?php

				?>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<?php 
			foreach ($indexdata as $key => $leilighet) 
			{ 
				$index = $key + 1;

				if (isset($leilighet['property type']) and $leilighet['property type'] == 'garasje/parkering')
				{
					continue;
				}

				if (!isset($leilighet['totalprice'])) 	
				{
					$leilighet['totalprice'] = $leilighet['price'];
				}

				if (!isset($leilighet['common costs'])) 	
				{
					$leilighet['common costs'] = '-';
				}

				if (!isset($leilighet['property type'])) 	
				{
					$leilighet['property type'] = '-';
				}

				if ($start = strpos($leilighet['price'], '-'))
				{
					$leilighet['price'] = substr($leilighet['price'], $start+1);
				}

				if ($start = strpos($leilighet['totalprice'], '-'))
				{
					$leilighet['totalprice'] = substr($leilighet['totalprice'], $start+1);
				}

				if (!isset($leilighet['size']))
				{
					//var_dump($leilighet);
					//continue;
				}

				if (isset($leilighet['size']))
				{

					if ($start = strpos($leilighet['size'], '-'))
					{
					//continue;
					}

				}

				$today = date('Y-m-d');
				$timelimit = date('Y-m-d', strtotime($limit, strtotime($today)));


				if ($leilighet['tinglyst_newest'] < $timelimit)
				{
					continue;
				}

				if ($leilighet['property type'] == 'Garasje/Parkering' or $leilighet['property type'] == 'andre')
				{
					continue;
				}

				$average = $leilighet['totalprice'] / $leilighet['size']; //brukes under

				//find out if part of apartment building
				if ($leilighet['ownership_repeat'] > 1) //mer enn en bør stemme
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}

				else if ($leilighet['ownership_repeat'] == 1 and $leilighet['ownership_count'] == 1) //to like oppføringer, bør stemme
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}
				else if ($leilighet['ownership_repeat'] == 0 and $leilighet['ownership_count'] == 1) //bare en oppføring, bør stemme				{
					{
						$badgecolor = 'success';
						$badgetext = 'Flippet';
					}												
					else if ((int)$leilighet['ownership_repeat'] == 1 and (int)$leilighet['ownership_count'] > 4)
					{

						$price_limit = 1.1;

						if (isset($average_prices[$leilighet['property type']][$leilighet['area']]))
						{

							$ratio = (float)$average / (float)$average_prices[$leilighet['property type']][$leilighet['area']];

							$price_limit_percent = round(($ratio -1)*100,1);

						if ($ratio > $price_limit) //only include if at least 10% over average price in area pr square meter
						{
							$badgecolor = 'primary';
							$badgetext = 'Ukjent, del av boligkompleks ('. $price_limit_percent .  '% over snittpris)';
						}
						else
						{
							continue;
						}
					}
					else
					{
						continue;
					}

				}
				else if (($leilighet['ownership_repeat'] / $leilighet['ownership_count']) > 0.7)
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}				
				else
				{
					$badgecolor = 'seconday';
					$badgetext = 'Uklassifisert (Antall funnet ' . $leilighet['ownership_count'] . ')';
				}

				//$badgetext .= ' -> ' . $leilighet['ownership_count'];

				?>

				<div class="col-12 col-sm-12 col-md-4 mb-4"><a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $leilighet['id']; ?>"> 
					<img data-src="<?php echo $leilighet['imgurl'] ;?>" width="100%" class="img-responsive lazyload">
				</a>
			</div>
			<div class="col-12 col-sm-12 col-md-8 mb-4">
				<div class="d-flex flex-column ">
					<div class="d-flex justify-content-between">
						<div class="mb-auto">
							<span class="font-weight-bold">Adresse:</span>
							<?php 
							echo $leilighet['adress']; 

							if (isset($leilighet['area']))
							{
								echo ' | ' . $leilighet['area']; 
							}
							
							if (isset($leilighet['fylke']))
							{
								echo ' | ' . $leilighet['fylke']; 
							} 

							?>
						</div>
						<div>
							<?php 

							if (isset($leilighet['first_spotted']))
							{
								?>

								<div class="badge bg-secondary text-white">Først sett: <?php echo $leilighet['first_spotted']; ?></div>		
								<?php 
							}
							?>
						</div>
					</div>

					<div class="mt-1">
						<h4><a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $leilighet['id']; ?>"> <?php echo $leilighet['title']; ?></a></h4>
					</div>

					<div class="d-flex justify-content-between mb-2 mt-2 ">
						<div class="d-flex flex-column">
							<div class="">	
								<span class="h5">
									<?php

									echo number_format($average,0,'.','.')  . ',- pr. m²';
									?>
								</span>
							</div>
							<?php
							if (isset($leilighet['area']) and isset($average_prices[$leilighet['property type']][$leilighet['area']]))
							{
								?>
								<div class="text-secondary" data-toggle="tooltip" title="Snittpris pr. m² i området <?php echo $leilighet['area'] . ' for ' . $leilighet['property type']; ?>">
									<?php 

									if ($average_prices[$leilighet['property type']][$leilighet['area']] > $average)
									{
										echo '<i class="fa fa-arrow-circle-down mr-1" aria-hidden="true"></i>';
									}
									else
									{
										echo '<i class="fa fa-arrow-circle-up mr-1" aria-hidden="true"></i>';
									}

									echo number_format($average_prices[$leilighet['property type']][$leilighet['area']],0,'.','.') . ',- ' ; 
									?>
								</div>
								<?php
							}
							?>

						</div>
						<div class="text-right">
							<h5>
								<?php

								echo number_format($leilighet['size'],0,'.','.')  . ' m²';
								?>
							</h5>
						</div>								
						<div class="text-right">

							<h5>
								<?php

								echo number_format($leilighet['price'],0,'.','.')  . ' kroner';
								?>
							</h5>
						</div>
					</div>
					<div class="mt-1"><span class="font-weight-bold">Totalpris:</span> <?php echo number_format($leilighet['totalprice'],0,'.','.') . ' kroner';
						echo ' | ' . $leilighet['property type'] . ' | ' . $leilighet['legal type'];
						?>
					</div>
					<div class="d-flex justify-content-between">
						<div class="">
							<span class="font-weight-bold"><a data-toggle="modal" data-target="#modal_<?php echo $leilighet['id']; ?>" href="">Siste tinglysning:</a></span>

							<!-- Modal -->
							<div class="modal" id="modal_<?php echo $leilighet['id']; ?>">
								<div class="modal-dialog" >
									<div class="modal-content">

										<!-- Modal Header -->
										<div class="modal-header">
											<h4 class="modal-title">Tinglysninger</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<?php

											foreach ($leilighet['ownership'] as $key => $value)
											{
												if ($key == 'tinglysninger')
												{
													break;
												}
												echo $key . ': ' . $value . '<br>';
											}


											?>

											<table class="table mt-2">
												<thead>
													<tr>
														<?php
														echo '<th>';
														echo 'Tinglyst: '; 
														echo '</th>';
														echo '<th>';
														echo 'Boligtype: '; 
														echo '</th>';
														echo '<th class="text-right">';
														echo 'Andelsnummer: '; 
														echo '</th>';
														echo '<th class="text-right">';
														echo 'Pris: '; 
														echo '</th>';
														?>

													</tr>
												</thead>										
												<tbody>

													<?php

													foreach ($leilighet['ownership']['tinglysninger'] as $key => $tinglysning)
													{
														?>
														<tr>

															<?php

															if (isset($tinglysning['tinglyst']))
															{
																echo '<td>';
																echo '' . $tinglysning['tinglyst']; 
																echo '</td>';
															}
															else
															{
																echo '<td>';														
																echo '</td>';
															}

															if (isset($tinglysning['boligtype']))
															{
																echo '<td>';
																echo '' . $tinglysning['boligtype']; 
																echo '</td>';
															}
															else
															{
																echo '<td>';														
																echo '</td>';
															}														

															if (isset($tinglysning['andelsnummer']))
															{
																echo '<td class="text-right">';
																echo '' . $tinglysning['andelsnummer']; 
																echo '</td>';
															}
															else
															{
																echo '<td>';														
																echo '</td>';
															}

															if (isset($tinglysning['pris']))
															{
																echo '<td class="text-right">';
																echo '' . number_format($tinglysning['pris'],0,',','.'); 
																echo '</td>';
															}
															else
															{
																echo '<td>';														
																echo '</td>';
															}	
															?>

														</tr>
														<?php
													}

													?>

												</tbody>
											</table>
										</div>

										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
										</div>

									</div>
								</div>
							</div>	

							<?php 
							$earlier = new DateTime("today");
							$later = new DateTime($leilighet['tinglyst_newest']);

							$interval = $later->diff($earlier);

							$datestring = '';

							if ($interval->format('%y') > 0)
							{
								$datestring .= $interval->format('%y') . ' år, ';

								if ($interval->format('%m') == 1)
								{
									$datestring .= $interval->format('%m'). ' måned og ';

								}
								else if ($interval->format('%m') == 0)
								{


								}									
								else
								{
									$datestring .= $interval->format('%m'). ' måneder og ';

								}

								if ($interval->format('%d') == 1)
								{

									$datestring .= $interval->format('%d') . ' dag';
								}
								else
								{

									$datestring .= $interval->format('%d') . ' dager';								
								}
							}

							else if ($interval->format('%m') > 0)
							{
								if ($interval->format('%m') == 1)
								{
									$datestring .= $interval->format('%m'). ' måned og ';

								}
								else
								{
									$datestring .= $interval->format('%m'). ' måneder og ';

								}

								if ($interval->format('%d') == 1)
								{

									$datestring .= $interval->format('%d') . ' dag';
								}
								else
								{

									$datestring .= $interval->format('%d') . ' dager';								
								}

							}
							else
							{
								if ($interval->format('%d') == 1)
								{

									$datestring .= $interval->format('%d') . ' dag';
								}
								else
								{

									$datestring .= $interval->format('%d') . ' dager';								
								}


							}

							echo $leilighet['tinglyst_newest'] . ' (' .  $datestring . ' siden)';

							$badgefontsize = '';

							if ($badgetext == 'Flippet')
							{
								$addedprice = $leilighet['price'] - $leilighet['ownership']['tinglysninger'][0]['pris'];

								$addedprice = number_format($addedprice,0,'.','.');

								if ($addedprice > 0)
								{
									$badgetext = ' (+' . $addedprice . ')';
								}
								else if ($addedprice == 0)
								{
									$badgecolor = 'secondary';
									$badgetext = 'Lik pris';
									$badgetext .= '';
								}
								else
								{
									$badgecolor = 'danger';
									$badgetext = 'Tap';
									$badgetext .= ' (' . $addedprice . ')';
								}

								$badgefontsize = 'badgetext';
							}

							?>
							<span class="badge  <?php echo $badgefontsize; ?>  text-white bg-<?php echo $badgecolor; ?>"><?php echo $badgetext; ?></span>
						</div>
						<?php
						//sjekk om allerede nedlasted

						if (!file_exists('download/details/' . $leilighet['id']))
						{

							?>
							<div class="d-flex">

								<div class="mr-2" data-toggle="tooltip" title="Last ned og publiser">
									<a target="_blank" href="finnscraper_details.php?id=
									<?php 
									echo $leilighet['id']; 
									echo '&dato=' . $date;
									echo '&publiser=yes';

									?>">
									<i class="fa text-secondary fa-arrow-circle-o-up fa-2x" aria-hidden="true"></i></a>
								</div>

								<div class="" data-toggle="tooltip" title="Last ned detaljer-siden hos Finn.no">
									<a target="_blank" href="finnscraper_details.php?id=
									<?php 
									echo $leilighet['id']; 
									echo '&dato=' . $date;

									?>">
									<i class="fa text-secondary fa-download fa-2x" aria-hidden="true"></i></a>
								</div>
							</div>
							<?php 
						}
						else
						{
							?>
							<div class="d-flex">

								<?php 
								if (!file_exists('download/details/' . $leilighet['id'] . '/publiser.json'))
								{

									?>

									<div class="mr-2" data-toggle="tooltip" title="Last ned detaljer og publiser">
										<a href="finnscraper_details.php?id=
										<?php 
										echo $leilighet['id']; 
										echo '&dato=' . $date;
										echo '&publiser=yes';

										?>">
										<i class="fa text-secondary fa-arrow-circle-o-up fa-2x" aria-hidden="true"></i></a>
									</div>

									<?php 
								}
								else 
								{
									?>
									<div class="mr-2" data-toggle="tooltip" title="Allerede publisert">
										<a href="finnscraper_details.php?id=
										<?php 
										echo $leilighet['id']; 
										echo '&dato=' . $date;
										echo '&publiser=yes';

										?>">
										<i class="fa text-success fa-arrow-circle-o-up fa-2x" aria-hidden="true"></i></a>
									</div>


									<?php

								}
								?>

								<div class="" data-toggle="tooltip" title="Detaljer allerede lastet ned hos Finn.no. Trykke for å se detaljer!">

									<a href="finnscraper_details_show.php?id=
									<?php 
									echo $leilighet['id']; 

									?>">
									<i class="text-success fa fa-eye fa-2x" aria-hidden="true"></i></a>
								</div>

								<div class="ml-2" data-toggle="tooltip" title="Trykk for å slette!">
									<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal">
										Slett
									</button>


								</div>

								<!-- The Modal -->
								<div class="modal" id="myModal">
									<div class="modal-dialog">
										<div class="modal-content">

											<!-- Modal Header -->
											<div class="modal-header">
												<h4 class="modal-title">Sletting av leilighet</h4>
												<button type="button" class="close" data-dismiss="modal">&times;</button>
											</div>

											<!-- Modal body -->
											<div class="modal-body">Bekreft for å slette id: <?php echo $leilighet['id']; ?><br>Dette vil både slette og avpublisere.<br> <br>
												<a href="deleter.php?id=
												<?php 
												echo $leilighet['id']; 

												?>" class="btn btn-danger" role="button">Ja, jeg vil slette</a>
												
											</div>

											<!-- Modal footer -->
											<div class="modal-footer">
												<button type="button" class="btn btn-info" data-dismiss="modal">Lukk</button>
											</div>

										</div>
									</div>
								</div>



							</div>
							<?php
						} 
						?>
					</div>
				</div>
			</div>

			<?php } //var_dump($indexdata[0]); ?>
		</div>
	</div>
	<script type="text/javascript">

// Select all elements with data-toggle="tooltips" in the document
$('[data-toggle="tooltip"]').tooltip();

// Select a specified element
$('#myTooltip').tooltip();

</script>

<script src="js/lazysizes.min.js" ></script>
<script>

  window.lazySizesConfig = window.lazySizesConfig || {};
  lazySizesConfig.loadMode = 1;

</script>

</body>
</html>