<?php

include_once 'functions.php';

//get adresses for point of interests!

if (!$adresses = readJSON('json/point_of_interest/point_of_interest_list.json'))
{
  echo 'Read error of json/point_of_interest/point_of_interest_list.json <br>';
}


//get adresses from todays ads

$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/maps/'. $date . '.json'))
{
  //echo $date . '<br>';
  $date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

  if ($date < $limit_date_previous)
  {
    echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
    return;
  }
}

if (!$indexdata = readJSON('json/maps/' .  $date . '.json'))
{
  echo 'Read error of ' . 'json/maps/' .  $date . '.json' . '<br>';
}

//echo 'Count: ' . count($indexdata) . '<br>';


foreach ($adresses as $key => $saved_apartment)
{
  echo '<strong>';
  echo $key . '. ' . $saved_apartment['adress'] . '';
  echo '</strong>';
  echo '<br>';

  $proximity_box = [];

  foreach ($indexdata as $index => $todays_apartment)
  {

    if (isset($todays_apartment['property type']) and $todays_apartment['property type'] == 'garasje/parkering')
    {
      continue;
    }


    //$distance = computeDistance($saved_apartment['lat'], $saved_apartment['lng'], $todays_apartment['location']['adresser']['0']['representasjonspunkt']['lat'], $todays_apartment['location']['adresser']['0']['representasjonspunkt']['lon']);
    if (!isset($todays_apartment['location']['adresser']['0']))
    {
      //var_dump($todays_apartment);
      continue;
    }

    $saved_lat = $saved_apartment['lat'];
    $saved_lon = $saved_apartment['lon'];

    $today_lat = $todays_apartment['location']['adresser']['0']['representasjonspunkt']['lat'];
    $today_lon = $todays_apartment['location']['adresser']['0']['representasjonspunkt']['lon'];

  
    $distance = vincentyGreatCircleDistance($saved_lat, $saved_lon, $today_lat, $today_lon);
    //echo $todays_apartment['adress'] . '. ';
    //echo 'Distance is ' . $distance . '<br>';
 
    $distance = round($distance,1);

    if ($distance < 300)
    {
      echo '---| ' . $index . '. ' . $todays_apartment['adress'] . ' | ' . $distance . ' meter';
      echo '<a href="https://www.finn.no/realestate/homes/ad.html?finnkode=' . $todays_apartment['id']  . ' ">Link</a>';
      echo '<br>';
      //var_dump($todays_apartment);
      $todays_apartment['distance'] = $distance;
      $proximity_box[] = $todays_apartment;
    }

  }
  if (empty($proximity_box))
  {
    unset($adresses[$key]);
  }
  else
  {
     $adresses[$key]['proximity'] = $proximity_box;
  }

}

$adresses = array_values($adresses);

saveJSON($adresses, 'json/point_of_interest/point_of_interest_found_list.json');




?>