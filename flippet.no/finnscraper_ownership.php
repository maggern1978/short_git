<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

//https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC
//https://www.finn.no/realestate/ownershiphistory.html?finnkode=204954811
//flippet.no

require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

set_time_limit(6000);

$date = date('Y-m-d');

while (!file_exists('json/index/'. $date . '.json'))
{

	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	echo 'Trying ' . 	'json/index/'. $date . '.json' . '<br>';
	if ($date < '2020-01-01')
	{
		echo 'Json files not found, returning...<br>';
		return;
	}
}

if (!$json = readJSON('json/index/' . $date . '.json'))
{
	echo 'json/index/' . $date . '.json is not set<br>';
	return;
}

echo 'Processing file ' . 'json/index/' . $date. '.json' . '<br>';
echo 'Number of entries: ' . count($json) . '<br>';

$urls = [];

$download_counter = 0; 
$runner = 0; 

foreach ($json as $key => $entry)
{
	
	flush_start();
	echo $runner++ . '. ';

	//check if file already exists, if yes then continue if file is newer than 14 days. 

	$filedir = 'data/ownership/' . $entry["id"];

	//check if already downloaded 
	$url = 'https://www.finn.no/realestate/ownershiphistory.html?finnkode=' . $entry['id'];

	$filename = 'download/ownership/' . $entry['id'] . '.html';

	if (file_exists($filename))
	{
		echo $filename . ' already downloaded, skipping...<br>';
		flush_end();
		continue;
	}

	$html = download($url);

	if (!stripos($html, 'Beklager, nå er det rusk i maskineriet') and !stripos($html, 'Ingen treff akkurat nå'))
	{
		echo 'Saved ' . $filename . ' from ' . $url . '. ';
		file_put_contents($filename, $html);
	}
	else
	{
		echo 'Fant ' . '"Beklager, nå er det rusk i maskineriet" eller "Ingen treff akkurat nå", stopping download... ';
		break;
	}
	
	timestamp();

	$rand = random_int(2,10);
	
	$download_counter++;

	if ($download_counter  > 700)
	{
		flush_end();
		break;
	}

	echo 'Sleeping for ' . $rand . ' seconds.<br>';
	flush_end();
	sleep($rand);
	
}





?>