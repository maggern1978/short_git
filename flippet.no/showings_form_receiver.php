<?php

require_once "functions.php";

if (!empty($_POST["id"])) 
{
	$id = test_input($_POST["id"]);
}
else
{
	exit('Error. No id! Please go back and try again.<br>');
}

if (!empty($_POST["comment"])) 
{
	$comment = test_input($_POST["comment"]);
}
else
{
	$comment = false;
}

if (!empty($_GET["price"])) 
{
	$price = test_input($_GET["price"]);
}
else
{
	$price = false;
}

$logdate = date('Y-m-d_H.i.s');


//find id
$lastpos = strrpos($id, '=');
$id = substr($id, $lastpos+1);

//save log
$obj = new stdClass;
$obj->id = $id;
$obj->price = $price;
$obj->comment = $comment;
$obj->date = $logdate;

echo $obj->id;
saveJSONsilent((array)$obj, 'data/showings_log/' . $id . '.json');


?>