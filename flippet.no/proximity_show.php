<?php

include 'functions.php';


$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/proximity/'. $date . '.json'))
{
  //echo $date . '<br>';
  $date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

  if ($date < $limit_date_previous)
  {
    echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
    return;
  }
}   

if (!$data = readJSON('json/proximity/' .  $date . '.json'))
{
  echo 'Read error of ' . 'json/proximity/' .  $date . '.json' . '<br>';
}


//read the points of interest

if (!$point_of_interest = readJSON('json/point_of_interest/point_of_interest_found_list.json'))
{
  echo 'Read error of ' . 'json/point_of_interest/point_of_interest_found_list.json<br>';
  exit();
}

//get latest average prices

if (!$average_prices = readJSON('json/areas/average_prices.json'))
{
  echo 'Read error of' . 'json/areas/average_prices.json' . '<br>';
}



//count 
$point_of_interest_count = count($point_of_interest);
$point_of_interst_subcount = 0;
foreach ($point_of_interest  as $adress)
{
  $point_of_interst_subcount += count($adress['proximity']);
}

//count 
$data_count = count($data);
$data_subcount = 0;
foreach ($data  as $adress)
{
  $data_subcount += count($adress['proximity']);
}

include 'bit_head.html';

?>
<body>
  <div class="container ">
    <div class="row">
      <div class="col-12">
        <h1>Boliger til salgs nå nær interessepunkter og lagrede boliger</h1>  
        <p><?php echo 'Antall lagrede boliger: ' . $data_count . ', med ' . $data_subcount . ' treff i nærheten.'; ?> <br>
          <?php echo 'Antall interessepunkt: ' . $point_of_interest_count . ', med ' . $point_of_interest_count . ' treff i nærheten.'; ?> 
        </p>
        <div class="d-flex">
          <div class="mr-2"><a href="#point_of_interest" class="btn btn-secondary" role="button">Gå til interessepunktene</a></div>
          <div class="mr-2"><a href="proximity_map.php" class="btn btn-secondary" role="button">Se på kartet</a></div>
          <div class="mr-2"><a href="flipped_panel.php" class="btn btn-secondary" role="button">Gå til kontrollpanel</a></div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <?php
  foreach ($data as $key => $entry)
  {

    //find map
    $col_middle = 9;
    $display_map = false; 
    $image_url = 'download/details/' . $entry['id'] . '/images/map.png';

    if (file_exists($image_url))
    {
      $col_middle = 6;
      $display_map = true; 
    }

    if (isset($average_prices[$entry['property_type']][$entry['area']]))
    {
      $area_message = 'Område: ' . $entry['area'];
      $price_message = 'Snittpris: ' . money($average_prices[$entry['property_type']][$entry['area']]) . ' kroner pr. kvadrat';
    }
    else
    {
      $area_message = '';
      $price_message = '';
    }

    ?>

    <div class="container" style="height: 184px;">
      <div class="row no-gutters bg-bluelight border">
        <div class="col-3 ">
          <a href="<?php echo $entry['link']; ?>">
            <img style="max-height: 184px;" src="<?php echo $entry['imgurl']; ?>" width="100%">
          </a>
        </div>
        <div class="col-<?php echo $col_middle; ?> px-3 pt-2">
          <div class="d-flex flex-column text-white">
            <a href="<?php echo $entry['link']; ?>">
              <h4 class="text-dark"><?php echo $entry['title']; ?></h4>
            </a>
          </div>
          <div class="d-flex flex-column">
          <div class="d-flex justify-content-between">
            <div>
             Adresse: <?php echo $entry['adress']; ?>
           </div>
           <div class="text-right text-secondary">
             Denne boligen er lagret.
           </div>
           </div>           
            <div>
            <?php echo $area_message . '. ' . $price_message; ?> 
           </div>           
 
         </div>          
       </div>
       <?php 

       if ($display_map)
       {
         ?>
         <div class="col-3 ">
          <a href="<?php echo $entry['link']; ?>">
            <img src="<?php echo $image_url; ?>" width="100%" height="184px">
          </a>
        </div> 
        <?php
      }
      ?>
    </div>
  </div>
</div>
<div class="container mt-3">
  <div class="row">
    <div class="col-12">
      <h5><?php echo count($entry['proximity']); ?> boliger til salgs innenfor 100 meter:  </h5>      
    </div>
  </div>
</div> 
<?php
foreach ($entry['proximity'] as $index => $new_entry)
{

  $message = '';
  $difference = 0;

  if(isset($new_entry['area']))
  {
    

    if(isset($average_prices[$new_entry['property type']][$new_entry['area']]))
    {

      if (isset($new_entry["size"]))
      {
        $new_entry['price_per_square_meter'] = (float)$new_entry['totalprice']/(float)$new_entry['size'];
      }


      if (isset($new_entry["area"]))
      {

       if (isset($new_entry['area']) and isset($average_prices[$new_entry['property type']][$new_entry['area']]) and isset($new_entry['price_per_square_meter']))
       {
         $message = ' Snittpris i ' . $new_entry['area'] . ': ' . money($average_prices[$new_entry['property type']][$new_entry['area']]) . ' kroner';

         if (isset($new_entry['price_per_square_meter']))
         {
           $difference = $new_entry['price_per_square_meter'] - $average_prices[$new_entry['property type']][$new_entry['area']];

           if ($difference > 0)
           {
            $prefix = '+';
          }
          else
          {
            $prefix = '';
          }

          if  ($difference < 0)
          {
            $message = "Under snittpris i området: " . money($average_prices[$new_entry['property type']][$new_entry['area']]) . " kroner (<span class='text-success font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
          }
          else
          {
           $message =  "Over snittpris i området: " . money($average_prices[$new_entry['property type']][$new_entry['area']]) . " kroner (<span class='text-danger font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
         }

       } 
     }
   } 

 }

}

?>

<div class="container mt-3 ">
  <div class="row no-gutters ">
    <div class=" bg-primary p-2 ml-4" style="width:50px;">
      <h1 class="text-center text-white"><?php $runner = $index + 1; echo $runner ?></h1>
    </div>  
    <div class="col-2">
      <a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $new_entry['id']; ?>">
        <img src="<?php echo $new_entry['imgurl']; ?>" width="100%" height="100%">
      </a>
    </div>
    <div class="col-8 px-3 pt-2 border">
      <div class="d-flex flex-column text-white">
        <a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $new_entry['id']; ?>">
          <h5 class="text-dark"><?php echo $new_entry['title']; ?></h5>
        </a>
      </div>
      <div class="d-flex flex-column">
        <div>
         Adresse: <?php echo $new_entry['adress']; ?>
       </div>
        <div>
         Pris: <?php echo money($new_entry['price']); ?> kroner
       </div>       
       <div>
         <?php //var_dump($new_entry); ?>
         Kvadratmeterpris: 
         <?php 
         if (is_numeric($new_entry['size']))
         {
           $average_price = $new_entry['totalprice'] / $new_entry['size']; 
         }
         else
         {
           $average_price = 0; 
         }


         $difference = '';
         //$message = '';

         if (isset($new_entry['area']) and isset($average_prices[$new_entry['property type']][$new_entry['area']]))
         {
           //$message = ' Snittpris i ' . $new_entry['area'] . ': ' . money($average_prices[$new_entry['area']]) . ' kroner';
           $difference = $average_price - $average_prices[$new_entry['property type']][$new_entry['area']];

           if ($difference > 0)
           {
            $prefix = '+';
          }
          else
          {
            $prefix = '';
          }

          echo money($average_price) . ' kroner' ;
        }
        else
        {
          echo money($average_price) . ' kroner' ;
        }
        
        ?> 
        </div>
        <div>
         <?php
         echo $message;
         
         ?>
       </div>
     </div>
   </div>          
 </div>
</div>
</div>
</div>

<?php
//var_dump($new_entry['location']);

}

?> <br>
<br>
<br>
<?php

}

?>

<div class="container" id="point_of_interest">
</div>

<?php
foreach ($point_of_interest as $key => $entry)
{
  ?>

  <div class="container" >
    <div class="row no-gutters bg-bluelight border">
      <div class="col-12 px-3 pt-2">
        <div class="d-flex flex-column text-white">
          <h2 class="text-dark">Interessepunkt: <?php echo $entry['adress']; ?></h2>
        </div>
        <div class="d-flex flex-column">
          <div>
           Lagt til: <?php echo $entry['added_date']; ?>
         </div>
         <div class="text-right text-secondary">
           Radius er 300 meter.
         </div> 
       </div>          
     </div>
   </div>
 </div>
</div>
<div class="container mt-3">
  <div class="row">
    <div class="col-12">
      <h5><?php echo count($entry['proximity']); ?> boliger til salgs innenfor 200 meter:  </h5>      
    </div>
  </div>
</div> 
<?php
foreach ($entry['proximity'] as $index => $new_entry)
{

  $message = '';
  $difference = 0;

  if(isset($new_entry['area']))
  {

    if(isset($average_prices[$new_entry['property type']][$new_entry['area']]))
    {

      if (isset($new_entry["size"]))
      {
        $new_entry['price_per_square_meter'] = (float)$new_entry['totalprice']/(float)$new_entry['size'];
      }


      if (isset($new_entry["area"]))
      {

       if (isset($new_entry['area']) and isset($average_prices[$new_entry['property type']][$new_entry['area']]) and isset($new_entry['price_per_square_meter']))
       {
         $message = ' Snittpris i ' . $new_entry['area'] . ': ' . money($average_prices[$new_entry['property type']][$new_entry['area']]) . ' kroner';

         if (isset($new_entry['price_per_square_meter']))
         {
           $difference = $new_entry['price_per_square_meter'] - $average_prices[$new_entry['property type']][$new_entry['area']];

           if ($difference > 0)
           {
            $prefix = '+';
          }
          else
          {
            $prefix = '';
          }

          if  ($difference < 0)
          {
            $message = "Under snittpris i området: " . money($average_prices[$new_entry['property type']][$new_entry['area']]) . " kroner (<span class='text-success font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
          }
          else
          {
           $message =  "Over snittpris i området: " . money($average_prices[$new_entry['property type']][$new_entry['area']]) . " kroner (<span class='text-danger font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
         }

       } 
     }
   } 

 }

}

 ?>

 <div class="container mt-3 " >
  <div class="row no-gutters " >
    <div class=" bg-primary p-2 ml-4" style="width:50px;">
      <h1 class="text-center text-white"><?php $runner = $index + 1; echo $runner ?></h1>
    </div>  
    <div class="col-2">
      <a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $new_entry['id']; ?>">
        <img src="<?php echo $new_entry['imgurl']; ?>" width="100%" height="100%">
      </a>
    </div>
    <div class="col-8 px-3 pt-2 border">
      <div class="d-flex flex-column text-white">
        <a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $new_entry['id']; ?>">
          <h5 class="text-dark"><?php echo $new_entry['title']; ?></h5>
        </a>
      </div>
      <div class="d-flex flex-column">
        <div>
         Adresse: <?php echo $new_entry['adress']; ?>
       </div>
        <div>
         Pris: <?php echo money($new_entry['price']); ?> kroner
       </div>       
        <div>
         <?php echo $message;?>
       </div>       
     </div>          
   </div>
 </div>
</div>
</div>

<?php

}

?> 
<br>
<br>
<br>
<?php

}

?>

</body>
</html>