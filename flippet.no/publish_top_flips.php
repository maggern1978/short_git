<?php
echo '<br>';

require_once('functions.php');	

$date = date('Y-m-d');

while (!file_exists('json/joined/'. $date . '.json'))
{

	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

	if ($date < '2021-01-01')
	{
		echo 'Json files not found, returning... Ingen tidligere oppføringer.<br>';
		return;
	}
}

//find out if anything is published today manually

$files = listfiles('download/details');
$today = date('Y-m-d');
$from_today_box = [];

foreach ($files as $file)
{

	$filetime = filemtime('download/details/' . $file);

	$filedate = date('Y-m-d', filemtime('download/details/' . $file));

	if ($filedate == $today and file_exists('download/details/' . $file . '/publiser.json'))
	{
		$from_today_box[] = $file;
	}

}

//get latest average prices
if (!$average_prices = readJSON('json/areas/average_prices.json'))
{
	echo 'Read error of' . 'json/areas/average_prices.json' . '<br>';
}

$json = readJSON('json/joined/'. $date . '.json');

echo 'Antall leiligheter inn: ' . count($json) . '<br>';
echo 'Filtrerer ut leiligheter...<br>';

$time = 12;
$limit = '-' . $time . ' months';
$today = date('Y-m-d');
$timelimit = date('Y-m-d', strtotime($limit, strtotime($today)));

foreach ($json as $key => $leilighet)
{

	if ($leilighet['first_spotted'] != $date)
	{
		unset($json[$key]);
		continue;

	}

	if ($leilighet['tinglyst_newest'] < $timelimit)
	{
		unset($json[$key]);
		continue;
	}


	if (!isset($leilighet['property type']) or $leilighet['property type'] == 'Garasje/Parkering' or $leilighet['property type'] == 'andre')
	{
		unset($json[$key]);
		continue;
	}

				//find out if part of apartment building
	if ($leilighet['ownership_repeat'] < 1)
	{

	}
	else if (($leilighet['ownership_repeat'] / $leilighet['ownership_count']) > 0.7)
	{

	}									
	else if ($leilighet['ownership_repeat'] == 1 and $leilighet['ownership_count'] == 1)
	{

	}								
	else if ($leilighet['ownership_repeat'] == 1 and $leilighet['ownership_count'] > 4)
	{
		unset($json[$key]);
		continue;
	}
	else
	{
		unset($json[$key]);
		continue;
	}

	if (!isset($leilighet['ownership']['tinglysninger'][0]['pris']))
	{
		unset($json[$key]);
		continue;
	}

	$flip_ratio = $leilighet['price'] / $leilighet['ownership']['tinglysninger'][0]['pris'];
	$json[$key]['price_change'] = $leilighet['price'] - $leilighet['ownership']['tinglysninger'][0]['pris'];
	$json[$key]['price_added_in_percent'] = ($flip_ratio-1)*100 ;

}

$json_count = count($json);

echo 'Gjenværende leiligheter: ' . $json_count . '<br><br>';

if ($json_count == 0)
{
	echo 'Zero left, returning...<br>';
	return;
}

echo 'Legger til to mesttjenende og høyest prosent<br>';

$publish_box = [];

//sort by biggest flips
usort($json, function ($item1, $item2) {
	return $item2['price_added_in_percent'] <=> $item1['price_added_in_percent'];
});

foreach ($json as $key => $leilighet)
{
	$publish_box[] = $leilighet;
	echo '--> La til 1 leilighet basert på prisoppgang i prosent.<br>';
	unset($json[$key]);

	if ($key > 0) //take top two
	{
		break;
	}
}

//sort by biggest flips
usort($json, function ($item1, $item2) {
	return $item2['price_change'] <=> $item1['price_change'];
});

$json = array_values($json);

//var_dump($json);

foreach ($json as $key => $leilighet)
{
	$publish_box[] = $leilighet;
	echo '--> La til 1 leilighet basert på prisendring. <br>';
	unset($json[$key]);

	if ($key > 0) //take top two
	{
		break;
	}
}



//add in manuel adds
echo 'Adding ' . count($from_today_box) . ' manual published apartments...<br>';

foreach ($from_today_box as $id)
{

	if (!$data = readJSON('download/details/' . $id . '/' . $id .'.json'))
	{
		continue;
	}

	$flip_ratio = $data['price'] / $data['ownership']['tinglysninger'][0]['pris'];

	$data['price_change'] = $data['price'] - $data['ownership']['tinglysninger'][0]['pris'];
	$data['price_added_in_percent'] = ($flip_ratio-1)*100 ;

	$publish_box[] = $data;

}




//filter out too small values!

$earn_limit = 700000;

echo '<br>';
echo 'Filtrerer ut alt som ikke har tjent ' . $earn_limit . ' eller  er priset til over snittprisen i området.<br>';

foreach ($publish_box as $index => $leilighet)
{
	if ($leilighet['price_change'] < $earn_limit)
	{
		echo $index . '. Under earn limit, unsetting...<br>';
		unset($publish_box[$index]);
		continue;
	}

	if ($leilighet['price_added_in_percent'] < 18)
	{
		echo $index . '. Under percentage limit, unsetting...<br>';
		unset($publish_box[$index]);
		continue;
	}
	
	//sort out under average price apartments
	foreach ($average_prices[$leilighet['property type']] as $key => $area)
	{
		
		if (!isset($leilighet['area']))
		{
			echo 'Area not set, breaking...';
			break;
		}

		if (mb_strtolower($key) == mb_strtolower($leilighet['area']))
		{
			
			$price_per_square_meter = $leilighet['totalprice'] / $leilighet['size'];

			if ($area > $price_per_square_meter)
			{
				echo $index . '. Under snittpris! Unsetting. Områdepris: ' . $area . ' | Leilighet: ' . $price_per_square_meter . '<br>';
				unset($publish_box[$index]);
			}
		}
	}
}

echo 'Publiserer ' . count($publish_box) . ' leiligheter. <br>';

$publish_box = array_values($publish_box);


if (!$runner = readJSON('published/start_count.json'))
{

	errorecho('Could not read start_count.json');
	return;

}

$runner = (int)$runner['nr'];


foreach ($publish_box as $key => $leilighet)
{

	$filename = "published/log_published/" . $leilighet['id'] . "/";

	if (!file_exists($filename)) 
	{
		mkdir($filename, 0777);
	} 
	else
	{
		echo $key . '. ' . $filename . ' already published, will skip.<br>';
		continue;
	}

	$leilighet['publisert'] = date('Y-m-d');

	saveJSON($leilighet, 'published/apartments/' . $runner .'.json');
	
	$runner++;

	$tester['nr'] = $runner;

	saveJSONsilent($tester, 'published/start_count.json');
	
}

echo 'publish_top_flips.php ferdig!<br>';


?>