<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

//https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC
//flippet.no


echo '<br>';
echo '<h1>Oppdaterer indeks...</h1><br>';

$files = listfiles('download/showings/');

if (!count($files) > 0)
{
	echo 'Error reading files in download/showings/<br>';
	return;
}

$answer = [];

foreach ($files as $index => $file)
{

	$dom = file_get_html("download/showings/" . $file . '/' . $file . '.html', false);

	if(!empty($dom)) 
	{

		$answer[$index]["id"] = $file;	
		$date_added = date("Y-m-d", filemtime("download/showings/" . $file . '/' . $file . '.html'));

		echo '<strong>' . $index . '. Fil: ' . "download/showings/" . $file . '/' .  $file . ".html. Content last changed: " . $date_added . '</strong><br>';
		$answer[$index]["date"] = $date_added;

		foreach($dom->find("section") as $key => $adsunit) 
		{
			$hit = 0; 

			foreach($adsunit->find(".u-t3") as $title) 
			{
				if (strlen($title->plaintext) > 2)
				{
					echo '<br><br>';
					echo $key . '. ';
					$answer[$index]["area"] = $title->plaintext;
					echo '"' . $title->plaintext . '"<br>';

					$hit = 1;
					break;
				}
			}

			if ($hit == 1)
			{
				break;
			}

		}
		echo 'Round 1 ends <br>';


		foreach($dom->find("section") as $key => $adsunit) 
		{
			$hit = 0; 

			foreach($adsunit->find("h1") as $title) 
			{
				if (strlen($title->plaintext) > 2)
				{
					echo '<br><br>';
					echo $key . '.<strong>';
					$answer[$index]["title"] = $title->plaintext;
					echo '"' . $title->plaintext . '"<br>';
					echo '</strong>';		
					$hit = 1;
					break;
				}
			}

			if ($hit == 1)
			{
				break;
			}

		}
		echo 'Round 2 ends <br>';

		foreach($dom->find(".u-mt32") as $key => $adsunit) 
		{
			$hit = 0; 

			foreach($adsunit->find(".u-mh16") as $title) 
			{

				if (strlen($title->plaintext) > 2)
				{
					echo '<br><br>';
					echo $key . '.<strong>';
					$answer[$index]["adress"] = $title->plaintext;
					echo '"' . $title->plaintext . '"<br>';
					echo '</strong>';		
					$hit = 1;
					break;
				}
				
			}

			if ($hit == 1)
			{
				break;
			}

		}
		echo 'Round 3 ends <br>';

		foreach($dom->find(".panel") as $key => $adsunit) 
		{
			$hit = 0; 
			$takenext = false;

			foreach($adsunit->find("span") as $teller => $title) 
			{

				if ($takenext == true)
				{
					echo '<br><br>';
					echo $key . '.<strong>';

					$price = trim(($title->plaintext)); 

					$price = str_replace(" kr", "", $price);
					$price = str_replace(' ', "", $price);

					$length = strlen($price);

						$newnumber = []; //remove spaces
						for ($i = 0; $i < $length ; $i++)
						{
							if (is_numeric($price[$i]))
							{
								$newnumber[] = $price[$i];
							}

						}

						$price = implode("", $newnumber);

						$answer[$index]["price"] = $price;
						echo '"' . $price . '"<br>';
						echo '</strong>';
						break;	
					}	

					
					if (mb_strtolower($title->plaintext) == 'prisantydning')
					{

						$takenext = true;
						continue;

					}


				}

			}
			echo 'Round 4 ends <br>';

			$takenr = 0;
			foreach($dom->find(".definition-list") as $key => $adsunit) 
			{
				

				$temp_holder = [];

				foreach($adsunit->find("dt") as $teller => $title) 
				{
					
					$temp_holder[$teller] = $title->plaintext;

				}

				$temp_holder_dd = [];

				foreach($adsunit->find("dd") as $teller => $title) 
				{
					
					$temp_holder_dd[] = $title->plaintext;
				}

				//joinin dd dt
				foreach ($temp_holder as $teller => $entry)
				{
					$input = trim($temp_holder_dd[$teller]);
					$input = str_replace(" m²", "", $input);
					$input = str_replace(" kr", "", $input);
					$input = str_replace(" ", "", $input);
					$answer[$index][$entry] = trim($input);
				}

			}

			echo 'Round 5 ends <br>';

			foreach($dom->find(".panel") as $key => $panel) 
			{

				$images = [];
				$images_text = [];

				foreach($panel->find("a") as $a) 
				{

					foreach($a->find("img") as $img) 
					{

						$url = $img->{'data-src'};

						if (!empty($img->{'data-src'}))
						{
							//find filename
							$lastpos = strrpos($url, '/');

							$filename = substr($url, $lastpos+1);

							$images[] = $filename;
						}

					}


					foreach($a->find(".image-carousel__caption") as $img) 
					{

						if (!empty($img->plaintext ))
						{
							echo $img->plaintext . '<br>';

							$images_text[] =  trim(str_replace("  ", "", $img->plaintext));
						}

					}

				}

				$answer[$index]["images"] = $images;
				$answer[$index]["images_text"] = $images_text;

				break;

			}

			unset($answer[$index]["images_text"][0]);
			$answer[$index]["images_text"] = array_values($answer[$index]["images_text"]);

			foreach ($answer[$index]["images"] as $key => $image)
			{
				$img_key = $key + 0  ;
			//echo $key . '. ' . $image . '. ' . $answer[$index]["images_text"][$img_key] . '<br>';
			//echo '<img width="200" src="download/showings/' . $file . '/images/' . $image .'"><br><br><br>';
			}

			echo 'Round 5 ends<br>';


		}

	}

	//sort by date
	usort($answer, function ($item1, $item2) {
		return $item1['date'] < $item2['date'];
	});


	echo '<br>Saving ' . count($answer) . ' entries<br>';
	saveJSON($answer, 'json/showings/index.json');

	?>