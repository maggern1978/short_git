<?php

require_once "functions.php";

if (!empty($_GET["id"])) 
{
	$id = ($_GET["id"]);
	//echo $date;
}
else
{
	exit('Ingen id satt');
	return;
	//$id = 211854314;
}

if (!file_exists('download/showings/' . $id . '/images')) 
{
	echo 'No images to show, folder does not exist.<br>';
	return;
} 

if (!$files = listfiles('download/showings/' . $id . '/images'))
{
	exit('Kan ikke liste bildemappe');
}

if (!$showings_all = readJSON('json/showings/index.json'))
{
	exit('Kan ikke lese detaljer.');
}

$entry = '';

foreach ($showings_all as $apartment)
{
	if ($id == $apartment['id'])
	{
		$entry = $apartment;
		break;
	}
}

$filename = 'data/showings_log/' . $entry['id'] . '.json';

if (!$form_data = readJSON($filename))
{
	$comment = '-';
	$date = '-';
	$price = '-';

}
else
{
	$comment = $form_data['comment'];
	$date = $form_data['date'];
	$price = $form_data['price'];
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Detaljer - <?php echo $id; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<style type="text/css">

	* {
		outline: 0px solid blue;
	}

	.thumbnail {
		width: 350px; /* You can set the dimensions to whatever you want */
		height: 233px;
		object-fit: cover;
	}
	/* Make the image fully responsive */
	.carousel-inner img {
		max-height: 90vh;
		min-height: 90vh;
	}

	/* On screens that are 992px or less, set the background color to blue */
	@media screen and (max-width: 600px) {
		.carousel-inner img {
			width: 100%;
			min-height: 0vh;
		}
	}

	.carousel-inner .carousel-item {
		transition: -webkit-transform 2s ease;
		transition: transform 0s ease;
		transition: transform 0s ease, -webkit-transform 0s ease;
	}
	.modal-full {
		min-width: 100%;
		margin: 0;
		margin: 0;
		top: 0;
		left: 0;
		right: 0;
	}


	.carousel-control-prev-icon {
		box-shadow: 2px 2px 4px #000000;
		background-color: blue;
	}
	.carousel-control-next-icon {
		box-shadow: 2px 2px 4px #000000;
		background-color: blue;
	}
	.modal {
		padding-right: 0px !important;
	}

</style>
<body>
	<div class="container mt-3">
		<div class="row">
			<div class="col-12">

				<h2 class="text-secondary">Detaljer og bildegalleri for id <?php echo $entry['id']; ?></h2>
				<!-- Button to Open the Modal -->
				<div class="d-flex flex-wrap">
				<button type="button" class="btn btn-primary mr-2 mb-2" data-toggle="modal" data-target="#myModal">
						Vis bilder
					</button>
					<a href="download/showings/<?php echo $entry['id'] . '/' . $entry['id'] . '.html'; ?>" type="button" class="btn btn-primary  mr-2 mb-2" >
						Lokalt lagret annonse
					</a>				
					<a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $entry['id'];?>" type="button" class="btn btn-primary  mr-2 mb-2" >
						Annonsen hos Finn.no
					</a>
					<a href="https://www.finn.no/realestate/ownershiphistory.html?finnkode=<?php echo $entry['id'];?>" type="button" class="btn btn-primary  mr-2 mb-2" >
						Eierskiftehistorikk hos Finn.no
					</a>
				</div>								
			</div>
		</div>
	</div>
	<div class="container mt-1">
		<div class="row">
			<div class="col-12">
				<h4 class="text-secondary">Detaljinformasjon:</h4>
			</div>
		</div>
	</div>
	<div class="container my-2 mb-4">
		<div class="row">
			<div class="col-12">
				<?php

				foreach ($entry as $key => $row)
				{

					if ($key != 'title')
					{
						continue;
					}
					else
					{
						

						echo '<span class="font-weight-bold">' . ucwords($key) . '</span>: ' . ucwords($row);


					}
				}

				?>

			</div>
		</div>
	</div>

	<div class="container my-2 mb-4">
		<div class="row">


			<?php
			$counter = 0; 

			foreach ($entry as $key => $row)
			{

				if ($key == 'images' or $key == 'images_text' or $key == 'id' or $key == 'title' or $key == 'Mobil')
				{
					continue;
				}
				else
				{
					if ($counter % 3 == 0 and $counter != 0)
					{
						echo '</div>';
						echo '<div class="row mt-2">';

					}
					echo '<div class=" col-12 col-sm-6 col-md-4">';
					echo '<span class="font-weight-bold">' . ucwords($key) . '</span>: ' . ucwords($row);
					echo '</div>';
					$counter++;

				}
			}

			?>

			
		</div>
	</div>


	<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" >
		<div class="modal-dialog modal-full mx-auto" role="document">
			<div class="modal-content modal-fullscreen"">

				<div id="demo" class="carousel slide align-items-center " data-ride="carousel">

					<!-- The slideshow -->
					<div class="carousel-inner">

						<?php
						foreach ($entry['images'] as $key => $filename)
						{
							if ($key != 0)
							{
								break;
							}
							?>
							<div class="carousel-item active align-items-center ">
								<div class="container-fluid">
									<img  src="download/showings/<?php echo  $entry['id'] . '/images/' . $filename; ?>" class="mx-auto d-block"  alt="testet">
								</div>
								<div class="text-center mt-1 bildetekst">
									<?php echo $entry['images_text'][$key]; ?>
								</div>

							</div>
							<?php 
						}?>

						<?php
						foreach ($entry['images'] as $key => $filename)
						{
							if ($key == 0)
							{
								continue;
							}
							?>
							<div class="carousel-item align-items-center flex-column">
								<div class="container-fluid">
									<img  src="download/showings/<?php echo  $entry['id'] . '/images/' . $filename; ?>" class="mx-auto d-block img-fluid"  alt="testet">
								</div>
								<div class="text-center mt-1 bildetekst">
									<?php echo $entry['images_text'][$key]; ?>
								</div>

							</div>
							<?php 
						}?>



						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#demo" data-slide="prev">
							<span class="carousel-control-prev-icon"></span>
						</a>
						<a class="carousel-control-next" href="#demo" data-slide="next">
							<span class="carousel-control-next-icon"></span>
						</a>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Lukk</button>
				</div>
			</div>
		</div>

	</div>

	

	<div class="container mt-3">
		<div class="row">
			<div clasS="col-12">
				<h4><?php echo count($entry['images']) . ' bilder:';?></h4>
			</div>
		</div>
	</div>

	<div class="container mt-3 border">
		<div class="row">
			<div class="d-flex flex-wrap justify-content-center">
				<?php 
				foreach ($entry['images'] as $key => $filename)
				{
					?>
					<div class="mr-3 mt-3 text-center" style="height:150px; width: 225px; background-color: #d8d8d8;">

						<img id="bilde<?php echo $key;?>" class="bilde" data-toggle="modal" data-target="#myModal" data-slide-to="<?php echo $key;?>" height="100%" src="download/showings/<?php echo  $entry['id'] . '/images/' . $filename; ?>">
						
					</div>
					<?php }?>	
				</div>
			</div>
		</div>

		<script type="text/javascript">
			$('.carousel').carousel({
				interval: false,
				ride: false,
				keyboard: true,
			});

			$(document).ready(function() {
	jQuery.fn.carousel.Constructor.TRANSITION_DURATION = 00  // 2 seconds
});

			$(".bilde").click(() => $(".carousel").carousel("next"));


			$('.bilde').click(function(e) {

				var goTo = parseInt($(this).data('slide-to'));
				console.log(goTo)

				$('#demo').carousel(goTo);
			});


			$(document).bind('keyup', function(e) {
				if(e.which == 39){
					$('.carousel').carousel('next');
				}
				else if(e.which == 37){
					$('.carousel').carousel('prev');
				}
			})

		</script>

	</body>
	</html>