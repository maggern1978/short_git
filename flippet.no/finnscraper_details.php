<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

if (!empty($_GET["id"])) 
{
	$id = test_input($_GET["id"]);
	//echo $date;
}
else
{
	exit('Ingen id satt');
}

if (!empty($_GET["dato"])) 
{
	$date = test_input($_GET["dato"]);
	//echo $date;
}
else
{
	exit('Ingen dato satt');
}

if (!empty($_GET["publiser"])) 
{
	$publiser = test_input($_GET["publiser"]);
	//echo $date;
}
else
{
	$publiser = false;
}


//---

set_time_limit(240);

//$date = date('Y-m-d');

$filename = "download/details/" . $id . "/";

if (!file_exists($filename)) 
{
	mkdir("download/details/" . $id, 0777);
	echo "The directory download/details/$id was successfully created.<br>";  
} 
$filedir = 'download/details/' . $id;

//finn kilden og lagre som json

if (!$source = readJSON('json/joined/' . $date . '.json'))
{
	echo('Kildejson ikke funnet: ' . 'json/joined/' . $date . '.json. Continuing to rip...');
}
else
{
	foreach ($source as $key => $entry)
	{

		if ($id == $entry['id'])
		{
			saveJSON($entry, $filedir . '/' . $id . '.json');
			break;
		}

	}

	if ($publiser)
	{
		saveJSON($publiser, $filedir . '/publiser.json');
	}
}


$do_update = 1;

if (file_exists($filedir . '/' . $id. '.html'))
{
	echo "Fil allerede nedlastet. ";
	$do_update = 0;
}

if ($do_update == 1)
{
	$url = 'https://www.finn.no/realestate/homes/ad.html?finnkode=' . $id;
	$html = download($url);

	if (!stripos($html, 'Beklager, nå er det rusk i maskineriet') and !stripos($html, 'Ingen treff akkurat nå') and !stripos($html, 'Her gikk det unna!'))
	{
		
		$filename = "download/details/" . $id . "/" . $id . '.html';
		
		if (file_put_contents($filename, $html))
		{
			successecho ('Saved ' . $filename . ' from ' . $url . '. ');
		}
		else
		{
			errorecho ('Failed to save ' . $filename . ' from ' . $url . '. ');
		}
	}
	else
	{
		echo 'Fant ' . '"Beklager, nå er det rusk i maskineriet" eller "Ingen treff akkurat nå", aborting file save... ';
		return;
	}
}

if (!file_exists("download/details/" . $id . "/images")) 
{
	mkdir("download/details/" . $id . '/images', 0777);
	echo "The directory download/details/$id/images was successfully created.<br>";  
} 

$source = $date;

echo 'Saving source date file: ';
saveJSON($source, "download/details/" . $id . '/source.json');



echo '<br>';
echo 'Trying to get images...<br>';

$dom = file_get_html($filedir . '/' . $id. '.html', false);

$urls_to_download = [];

$coordinates = [];

if(!empty($dom)) 
{

	foreach($dom->find(".panel") as $key => $panel) 
	{
		//echo '<br><br>';
		//echo $key . '.<strong>';
		foreach($panel->find("a") as $a) 
		{
			//echo $a->plaintext;
			//echo '<br>';
			//echo $a->src;
			//echo $a->href;

			foreach($a->find("img") as $img) 
			{
				
				if (!empty($img->src))
				{
					$urls_to_download[] = $img->src;
				}
				
			}

			foreach($a->find("img") as $img) 
			{
				
				if (!empty($img->{'data-src'}))
				{
					$urls_to_download[] = $img->{'data-src'};
				}

			}	
		}

		
	}
}

echo 'Identified ' . count($urls_to_download) . ' images to download.<br>';


foreach ($urls_to_download as $key => $url)
{

	//find filename
	$lastpos = strrpos($url, '/');

	$filename = substr($url, $lastpos+1);

	flush_start();
	echo $key . '. Filename: ' . $filename . '. ';
	echo $url;

	if (file_exists('download/details/' . $id . '/images/' . $filename))
	{
		echo ' | Already exists, will skip. <br>';
		flush_end();
		continue;

	}

	if ($data = download($url))
	{
		
		if (!stripos($data, 'Beklager, nå er det rusk i maskineriet') and !stripos($data, 'Ingen treff akkurat nå') and !stripos($data, 'Her gikk det unna!'))
		{
					

			if (strpos($url, 'cmap') !== false) //just for map
			{
				$coordinates['location'] = $filename;
				saveJSON($coordinates, 'download/details/' . $id . '/location.json');
				$filename = 'map.png';
			}

			file_put_contents('download/details/' . $id . '/images/' . $filename, $data);
			echo ' | Saving. ';

			$random = random_int(1, 3);
			echo ' | Sleeping for ' . $random . ' seconds to not download too fast.<br>';
			sleep($random);
			
			
		}
	}
	flush_end();
}

//publish 
if ($publiser)
{
	include ('publish_top_flips.php');
}



?>