<?php

include_once 'functions.php';

//https://maps.googleapis.com/maps/api/geocode/json?address=odinsvei+4b+1413,+t%C3%A5rn%C3%A5sen+norge&key=AIzaSyDIDFDKvAJqNw1zx04g3JA1ymxWRfXp0Y8
//urlencode

//finner locationdata for lagrede boliger

set_time_limit(15);

$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/index/'. $date . '.json'))
{
  echo $date . '<br>';
  $date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

  if ($date < $limit_date_previous)
  {
    echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
    return;
  }
}

if (!$indexdata = readJSON('json/index/' .  $date . '.json'))
{
  echo 'Read error of ' . 'json/index/' .  $date . '.json' . '<br>';
}


$adresses = [];
$files = listfiles('download/details/');

foreach ($files as $index => $file)
{

  if (!$data = readJSON('download/details/' . $file . '/' . $file . '.json'))
  {
    echo 'Read error of ' . $file . '<br>';
    continue; 
  }

  //find position!

  if (!$locationdata = readJSON('download/details/' . $file . '/location.json'))
  {
    echo 'Read error of ' . $file . '<br>';
    continue; 
  }
  
  $last_pos = strrpos($locationdata['location'], 'lat=');
  $lat_string = substr($locationdata['location'], $last_pos + 4, 8);
  $lat_string = cuttext($lat_string);

  $last_pos = strrpos($locationdata['location'], 'lng=');
  $lng_string = substr($locationdata['location'], $last_pos + 4, 8);
  $lng_string = cuttext($lng_string);

  $obj = new stdClass;
  $obj->lat = $lat_string;
  $obj->lng = $lng_string;
  $obj->link = 'https://www.finn.no/realestate/homes/ad.html?finnkode=' . $file;
  $obj->title = $data['title'];
  $obj->adress = $data['adress'];
  $obj->price = $data['price'];
  $obj->totalpris = $data['totalprice'];
  $obj->size = $data['size'];
  $obj->property_type = $data['property type'];
  $obj->id = $file;
  $obj->legal_type = $data['legal type'];
  $obj->price_per_square_meter = (float)$data['totalprice'] / (float)$data['size'];
  $obj->previous_price = $data['ownership']['tinglysninger'][0]['pris'];
  $obj->price_change = $data['price'] - $obj->previous_price;
  
  
  $obj->area = '';
  if (isset($data['area']))
  {
    $obj->area = $data['area'];
  }

  $obj->first_spotted = '';
  if (isset($data['first_spotted']))
  {
    $obj->first_spotted = $data['first_spotted'];
  }
  
  $obj->imgurl = $data['imgurl'];
  
  $adresses[] = (array)$obj;

}



//get adresses from todays ads

$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/maps/'. $date . '.json'))
{
  //echo $date . '<br>';
  $date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

  if ($date < $limit_date_previous)
  {
    echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
    return;
  }
}

if (!$indexdata = readJSON('json/maps/' .  $date . '.json'))
{
  echo 'Read error of ' . 'json/maps/' .  $date . '.json' . '<br>';
}

//echo 'Count: ' . count($indexdata) . '<br>';


foreach ($adresses as $key => $saved_apartment)
{
  echo '<strong>';
  echo $key . '. ' . $saved_apartment['adress'] . '';
  echo '</strong>';
  echo '<a href="https://www.finn.no/realestate/homes/ad.html?finnkode=' . $saved_apartment['id']  . ' ">Link</a>';
  echo '<br>';

  $proximity_box = [];

  foreach ($indexdata as $index => $todays_apartment)
  {

    if (isset($todays_apartment['property type']) and $todays_apartment['property type'] == 'garasje/parkering')
    {
      continue;
    }

    //$distance = computeDistance($saved_apartment['lat'], $saved_apartment['lng'], $todays_apartment['location']['adresser']['0']['representasjonspunkt']['lat'], $todays_apartment['location']['adresser']['0']['representasjonspunkt']['lon']);
    if (!isset($todays_apartment['location']['adresser']['0']))
    {
      //var_dump($todays_apartment);
      continue;
    }

    $distance = vincentyGreatCircleDistance($saved_apartment['lat'], $saved_apartment['lng'], $todays_apartment['location']['adresser']['0']['representasjonspunkt']['lat'], $todays_apartment['location']['adresser']['0']['representasjonspunkt']['lon']);
    //echo $todays_apartment['adress'] . '. ';
    //echo 'Distance is ' . $distance . '<br>';

    $distance = round($distance,1);

    if ($distance < 100 and $todays_apartment['id'] != $saved_apartment['id'])
    {
      echo '---| ' . $index . '. ' . $todays_apartment['adress'] . ' | ' . $distance . ' meter';
      echo '<a href="https://www.finn.no/realestate/homes/ad.html?finnkode=' . $todays_apartment['id']  . ' ">Link</a>';
      echo '<br>';
      //var_dump($todays_apartment);
      $todays_apartment['distance'] = $distance;
      $proximity_box[] = $todays_apartment;

    }

  }
  if(empty($proximity_box))
  {
    unset($adresses[$key]);
  }
  else
  {
   $adresses[$key]['proximity'] = $proximity_box;
 }

}

$adresses = array_values($adresses);

saveJSON($adresses, 'json/proximity/' . $date . '.json');




?>