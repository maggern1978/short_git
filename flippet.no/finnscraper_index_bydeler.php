<?php



require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

//https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC
//flippet.no

set_time_limit(3000);

$date = date('Y-m-d');

$filename = "download/bydel/" . $date  . "/";

if (!file_exists($filename)) 
{
	mkdir($filename, 0777);
	echo "The directory $filename was successfully created.<br>"; 
} 
else 
{
    //echo "The directory $dirname exists.<br>";
}



$bydeler = [];
$bydeler['Bjerke'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20528&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Bygdøy - Frogner'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20507&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Bøler'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20519&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Ekeberg - Bekkelaget'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20515&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Furuset'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20524&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Gamle Oslo'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20512&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Grefsen - Kjelsås'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20529&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Grorud'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20527&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Grünerløkka - Sofienberg'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20511&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Hellerud'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20523&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Helsfyr - Sinsen'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20522&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Lambertseter'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20518&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Manglerud'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20520&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Nordstrand'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20516&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Romsås'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20526&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Røa'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20532&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Sagene - Torshov'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20510&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Sentrum'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20513&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Sogn'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20530&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['St.Hanshaugen - Ullevål'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20509&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Stovner'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20525&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Søndre Nordstrand'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20517&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Ullern'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20533&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Uranienborg - Majorstuen'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20508&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Vinderen'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20531&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';
$bydeler['Østensjø'] = 'https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20521&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC';



//to pages
//https://www.finn.no/realestate/homes/search.html?is_new_property=false&lifecycle=1&location=1.20061.20510&page=2&property_type=4&property_type=1&property_type=3&property_type=2&sort=PUBLISHED_DESC

$download_list = [];

foreach ($bydeler as $key => $bydel)
{

	$urls = [];

	for ($i = 1; $i < 100; $i++)

	{

		$urls[$i] = $bydel . '&page=' . $i;
	}

	$obj = new stdClass;
	$obj->bydel = $key;
	$obj->urls = $urls;

	$download_list[] = (array)$obj; 	


}

$download_counter = 0; 

foreach ($download_list as $key => $bydel)
{

	foreach ($bydel['urls'] as $index => $url)
	{

		$filename = 'download/bydel/' . $date . '/' . $bydel['bydel'] . '/';

		if (!file_exists($filename)) 
		{
			mkdir($filename, 0777);
			echo "The directory $filename was successfully created.<br>"; 
		}

		$filename = 'download/bydel/' . $date . '/' . $bydel['bydel'] . '/' . $index . '.html';

		if (file_exists($filename))
		{
			$mod_date =  filemtime($filename);
		}
		else
		{
			$mod_date = 0;
		}

		$now_time = time();

		if (file_exists($filename) and $mod_date < $now_time - (12*60*60)) //12 timer
		{
			echo $filename . ' is not from today (' . $mod_date . ' / ' . $now_time . '), deleting...<br>';
			$difference = $mod_date - $now_time;
			var_dump($difference);

			unlink($filename)		;
		}

	

		if (file_exists($filename))
		{
		    echo $filename . ' already exists, skipping...<br>';
			continue;
		}
		
		echo $key . '. ' . $filename . '. ';
		//echo $url;
		timestamp();


		$html = download($url);

		if (!stripos($html, 'Beklager, nå er det rusk i maskineriet') and !stripos($html, 'Ingen treff akkurat nå'))
		{
			file_put_contents($filename, $html);
		}
		else
		{
			echo 'Fant ' . '"Beklager, nå er det rusk i maskineriet" eller "Ingen treff akkurat nå", stopping download...<br>';
			break;
		}

		$rand = random_int(0,10);
		sleep($rand);

		$download_counter++;

		if ($download_counter  > 50)
		{
			break;
		}

		echo 'Sleeping for ' . $rand . ' seconds.<br>';

	}


}

//$dom = file_get_html("https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC", false);
//$dom = file_get_html("data/side.html", false);

//file_put_contents('data/side.html', $dom);




?>