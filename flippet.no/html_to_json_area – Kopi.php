<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

//https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC
//flippet.no

$date = date('Y-m-d');

while (!file_exists('download/areas/'. $date))
{

	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	echo 'Trying ' . 	$date . '.json<br>';
	if ($date < '2020-01-01')
	{
		echo 'Json files not found, returning...<br>';
		return;
	}
}

$files = listfiles('download/areas/'. $date . '/');

$allbox = [];

$runneren = 0;

foreach ($files as $key => $file)
{

	$id = str_replace('.html', '', $file);

	$answer = array();

	//$dom = file_get_html("download/ownership/" . $date . '/' . $file, true);
	echo $runneren++  . '. Processing file download/areas/' . $file . '.  ';

	$dom = str_get_html(file_get_contents("download/areas/" . $date . '/' . $file)); 

	if(!empty($dom)) 
	{

		foreach($dom->find('script') as $runner => $definition) 
		{

			$localjson = json_decode($definition->innertext, true);

			//var_dump($localjson);
			//var_dump($localjson['props']);


			if (!isset($localjson['props']) or empty($localjson))
			{
				//echo 'props not set or empty<br>';
				continue;
			}

			//var_dump($localjson['props']['pageProps']['locationHistory']);
			//var_dump($localjson['props']['pageProps']['ad']);
			//var_dump($localjson['props']['pageProps']);

			$localjson = $localjson['props']['pageProps']['locationHistory'][0];

			if (!isset($localjson['propertyType']))
			{
				continue;
			}

			//only apartments

			if ($localjson['propertyType'] != 'Leilighet')
			{
				echo 'Not apartment, will skip! <br>';
				continue;
			}

			$allbox[] = $localjson;

		}
	}
	else
	{
		errorecho('Empty: ' . "download/areas/" . $date . '/' . $file . '<br>');
	}

	echo '<br>';

}
echo 'Out: ' . count($allbox);

echo '<br>';
saveJSON($allbox, 'json/areas/area_prices.json');

echo '<br><br>Finding values pr area<br>';

//$average_box = [];
$average_test = [];

foreach ($allbox as $area)
{

	$sum = 0;
	$sumapartments = 0;

	foreach ($area['histData'] as $key => $number_of_apartmens)
	{
		$keyint = (int)$key;
		$sumapartments += $number_of_apartmens;
		$sum += $keyint  * $number_of_apartmens;
		//echo $key . ' ' . $number_of_apartmens . ' (' . $keyint  * $number_of_apartmens . ') ' . $sum . ' <br>';
	}

	$average = round($sum / $sumapartments);

	//var_dump($area['histData']);
	$areaname = $area['locationDetails'][0]['name'];

	echo $area['locationDetails'][0]['name'];
	echo ' ' . $average . '<br>';
	
	if (!empty($area['histData'])) // only if many hits
	{
			//$average_box[$areaname] = (int)$average;
			$average_test[$areaname][] = (int)$average;
			//$average_test[$areaname]['type'][] = $area['propertyType'];
	}
	else
	{
		echo 'Empty, will skip!';
		continue;
	}


}
echo '<br><br>Checking for duplicated prices!<br>';
//when multiple prices, take the one that is most frequent

foreach ($average_test as $key => $area)
{

	$area_local = array_unique($area);

	if (count($area_local) > 1)
	{
		echo $key . ' : ';
		errorecho ('More than one value!<br>');
	

		$counted = array_count_values($area);
		arsort($counted);

		$keys = array_keys($counted);

		$average_test[$key] = $keys[0]; //take the one with most
		echo 'Keeping ' . $keys[0] . '<br>';

	}
	else
	{
		$average_test[$key] = $area[0]; 
	}


}

uasort($average_test,"descending");
saveJSON($average_test, 'json/areas/average_prices.json');

$today = date('y-m-d');

saveJSON($average_test, 'json/areas/history/average_prices_' . $today . '.json');

//var_dump($average_test);

?>