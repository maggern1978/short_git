<?php

include 'bit_head.html';
include 'functions.php';
$data = readJSON('json/showings/index.json');

?>
	<div class="container mt-3">
		<div class="row">
		<h3>Visninger - oversikt</h3>
		</div>
		</div>

<?php


foreach ($data as $key => $entry)
{
	$filename = 'data/showings_log/' . $entry['id'] . '.json';
	
	if (!$form_data = readJSON($filename))
	{
		$comment = '-';
		$date = '-';
		$price = '-';

	}
	else
	{
		$comment = $form_data['comment'];
		$date = $form_data['date'];
		$price = $form_data['price'];
	}

	?>

	<div class="container mt-3 border">
		<div class="row">
			<div class="d-flex justify-content-between">
				<div>
					<img width="230" src="download/showings/<?php echo  $entry['id'] . '/images/' . $entry['images'][0]; ?>">
				</div>
				<div class="p-2 ml-1">
					<div class="d-flex justify-content-between">
						<div><span class="font-weight-bold">Adresse: </span><?php echo $entry['adress']; ?></div>
						<div><?php echo $entry['area']; ?></div>
					</div>
					<div class="d-flex">
						<div class="h4"><a href="<?php echo 'showings_display_details.php?id=' . $entry['id']; ?>"><?php echo $entry['title']; ?></a></div>
					</div>					
					<div class="d-flex justify-content-between">
						<div class="h5"><span class="font-weight-bold">Prisantydning:</span> <?php echo number_format($entry['price'],0,'.','.') . ',- ' ; ?></div>
						<div class="h5"><span class="font-weight-bold"><?php if (!empty($price)) { echo 'Salgspris: '; } ; ?> </span> <?php if (!empty($price)) { echo $price; } else { echo 'Salgspris: Ikke oppgitt'; }; ?></div>
					</div>
					<div class="d-flex justify-content-between">
						<div class="h5"><span class="font-weight-bold">Lagt inn:</span> <?php echo $entry['date']; ?></div>
						<div class="h5"><span class="font-weight-bold"><?php if (!empty($comment)) { echo 'Kommentar: '; } ; ?> </span> <?php if (!empty($comment)) { echo $comment; } ; ?></div>
					</div>												
				</div>
			</div>
		</div>
	</div>

	<?php } ?>


</body>
</html>

