<?php

require_once('functions.php');

$date = date('Y-m-d');

while (!file_exists('json/index/'. $date . '.json'))
{

	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	//echo 'Trying ' . 	$date . '.json<br>';

	if ($date < '2020-01-01')
	{
		echo 'Json files not found, returning...<br>';
		return;
	}
}

if (!$indexdata = readJSON('json/index/'. $date . '.json'))
{
	echo 'Error reading json for index data.<br>';
	return; 
}

echo 'Indexdata in: ' . count($indexdata) . '<br>';

foreach ($indexdata as $key => $apartment)
{

	//set ownership
	$ownership_filename = 'data/ownership/' . $apartment['id'] . '/' . $apartment['id'] . '.json';

	echo $key . '. ' . $ownership_filename . '<br>';

	if (file_exists($ownership_filename))
	{

		//get first spotted date
		$filetime = filemtime($ownership_filename);

		if ($first_spotted = date('Y-m-d', $filetime))
		{
			$indexdata[$key]['first_spotted'] = $first_spotted;
		}
		else
		{
			$indexdata[$key]['first_spotted'] = false;
		}

		if ($ownershipdata = readJSON($ownership_filename))
		{
			
			if (isset($ownershipdata['tinglysninger']))
			{
				$indexdata[$key]['tinglyst_newest'] = $ownershipdata['tinglysninger'][0]['tinglyst'];
			    $indexdata[$key]['ownership_count'] = count($ownershipdata['tinglysninger']);
			}
			else
			{
				$indexdata[$key]['tinglyst_newest'] = false;
			    $indexdata[$key]['ownership_count'] = false;
			}
		

			//var_dump($ownershipdata);

			if (isset($ownershipdata['seksjonsnummer']))
			{
				$type = 'seksjonsnummer';
			}
			else if (isset($ownershipdata['seksjonsnummer']))
			{
				$type = false;
			}

			if (isset($ownershipdata['tinglysninger'][0]['andelsnummer']) and $indexdata[$key]['ownership_count'] > 1)
			{
				//echo 'andel. ';

				$firstkey = $ownershipdata['tinglysninger'][0]['andelsnummer'];

				$firstkeycount = 0; 

				foreach ($ownershipdata['tinglysninger'] as $ownerkey => $entry)
				{
					if ($entry['andelsnummer'] == $firstkey)
					{
						$indexdata[$key]['ownership_repeat'] = ++$firstkeycount; 
					}
					else
					{
						$indexdata[$key]['ownership_repeat'] = $firstkeycount;
						break;
					}

				}

			}
			else if (isset($ownershipdata['seksjonsnummer']) and $indexdata[$key]['ownership_count'] > 1)
			{

				//echo 'Seksjon. ';

				$firstkey = $ownershipdata['seksjonsnummer'];

				$firstkeycount = 0; 

				foreach ($ownershipdata['tinglysninger'] as $ownerkey => $entry)
				{
					if ($entry['seksjonsnummer'] == $firstkey)
					{
						$indexdata[$key]['ownership_repeat'] = ++$firstkeycount; 
					}
					else
					{
						$indexdata[$key]['ownership_repeat'] = $firstkeycount;
						break;
					}

				}

			}			
			else if ($indexdata[$key]['ownership_count'] == 1)
			{
				$indexdata[$key]['ownership_repeat'] = 1; 
			}
			else
			{
				$indexdata[$key]['ownership_repeat'] = 0; 
			}


			if (strpos($indexdata[$key]['tinglyst_newest'], '.'))
			{
				$datearray = explode(".", $indexdata[$key]['tinglyst_newest']);
				$indexdata[$key]['tinglyst_newest'] = $datearray[2] . '-' . $datearray[1] . '-' . $datearray[0];
			}

			$indexdata[$key]['ownership'] = $ownershipdata;

		}
		else
		{	
			unset($indexdata[$key]);
		}

	}
	else
	{
		echo 'Does not exist...unsetting<br>';
		unset($indexdata[$key]);
	}
}

foreach ($indexdata as $key => $data)
{
	echo $key . '<br>';
	if (isset($data['ownership']['tinglysninger'][0]['pris']) and isset($data['price']))
	{
		$indexdata[$key]['price_change'] = $data['price'] - $data['ownership']['tinglysninger'][0]['pris']; 
	}
	else
	{
		$indexdata[$key]['price_change'] = false;
	}
	

}


$indexdata = array_values($indexdata);
echo '<br><br>Indexdata out: ' . count($indexdata) . '<br>';

usort($indexdata, function($a, $b) {
	return $a['first_spotted'] < $b['first_spotted'];
});

saveJSON($indexdata, 'json/joined/' .  $date . '.json');

?>
