<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

//https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC
//flippet.no

$eiertyper = ['Leilighet', 'Enebolig','Tomannsbolig','Rekkehus','Garasje/Parkering','Andre','Kontor'];
$eierform = ['Aksje', 'Andel', 'Selveier', 'Annet', 'Obligasjon'];

$date = date('Y-m-d');

while (!file_exists('download/bydel/'. $date))
{

	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	echo 'Trying ' . 	$date . '.json<br>';
	if ($date < '2020-01-01')
	{
		echo 'Json files not found, returning...<br>';
		return;
	}
}

$files = listfiles('download/bydel/'. $date . '/');

if (!count($files) > 2)
{
	echo 'Error reading files in download/bydel/'. $date . '/' . '<br>';
	return;
}



$allbox = [];

foreach ($files as $index => $dir)
{

	echo '<h1>' . $dir . '</h1>';

	$files = listfiles('download/bydel/'. $date . '/' . $dir);

	foreach ($files as $key => $file)
	{

		$answer = array();

		$dom = file_get_html("download/bydel/" . $date . '/' .  $dir . '/' . $file, false);

		if(!empty($dom)) 
		{

			foreach($dom->find(".ads__unit") as $key => $adsunit) 
			{
				echo '<br><br>';
				echo $key . '.<strong>';
				foreach($adsunit->find("h2") as $title) 
				{
					$answer[$key]["title"] = $title->plaintext;
					echo $title->plaintext . '<br>';
				}
				echo '</strong>';


				foreach($adsunit->find(".ads__unit__img") as $imgbox) 
				{

					foreach($imgbox->find("img") as $bilde) 
					{
						$answer[$key]["imgurl"] = $bilde->src;
					}	

				}			

				foreach($adsunit->find(".ads__unit__link") as $link) 
				{

					$string = $link->href;
					$linkposisjon = stripos($string, "=");
					$string = substr($string, $linkposisjon+1);

					$i = 0; 
					$filename = '';

					while(isset($string[$i]) and is_numeric($string[$i]))
					{

					//echo $string[$i];
						$filename .= $string[$i];
						$i++;
					}

					if (strlen($filename) > 5)
					{
						$answer[$key]["id"] = $filename;
						echo 'id er ' .  $filename . '<br>';
					}
					else 
					{
						$answer[$key]["id"] = $string;
						echo 'id er (string)' .  $filename . '<br>';
					}

				}


				foreach($adsunit->find(".ads__unit__content__details") as $details) 
				{
					$array = explode(',',$details->plaintext);

					if (isset($array[0]))
					{
						$answer[$key]["adress"] = trim($array[0]);
					}

					if (isset($array[1]))
					{
						$answer[$key]["fylke"] = trim($array[1]);
					}

					if (isset($array[2]))
					{
						$answer[$key]["area"] = trim($array[2]);
					}


					echo $details->plaintext . '<br>';
				}

				foreach($adsunit->find(".ads__unit__content__keys") as $keys) 
				{

					$pricebox = [];

					foreach($keys->find("div") as $div) 
					{
						$string = $div->plaintext;

						if (strpos($string, 'm²',0))
						{
							$answer[$key]["size"] = trim(str_ireplace("m²", "", $div->plaintext));
						}
						else if (strpos($string, ' kr',0))
						{
							$string = str_ireplace ('kr', '', $string);
						$string  =  preg_replace('/\s+/u', '', $string); // remove spaces

						echo $string . '<br>';
						$answer[$key]["price"] = $string;
					}
					
				}

				//$answer[$key]["size"] = $keys->plaintext;
				//echo $details->plaintext . '<br>';
			}

			foreach($adsunit->find(".ads__unit__content__list") as $list) 
			{
				$string = $list->plaintext;

				echo $string . ' | test <br>';

				if (stripos($string, 'otalpris'))
				{

					//To verdier må hentes ut
					//Totalpris: 5 647 142 kr • Fellesutg.: 3 215 kr

					$kronerposisjon = stripos($string, ' kr',0); //totalpris, finn posisjoner
					$string_totalpris = substr($string, 0, $kronerposisjon); //kutt

					$string_totalpris = str_ireplace("Totalpris:", "", $string_totalpris);
					$string_totalpris = trim($string_totalpris);
					$answer[$key]["totalprice"] = stripspaces($string_totalpris);

					$string_fellesutgifter = substr($string, $kronerposisjon); //kutt
					$kolonposisjon = stripos($string_fellesutgifter, ': ',0); //totalpris, finn posisjoner
					$string_fellesutgifter = substr($string_fellesutgifter, $kolonposisjon);

					$string_fellesutgifter = str_ireplace('kr', '', $string_fellesutgifter);
					$string_fellesutgifter = str_ireplace(':', '', $string_fellesutgifter);
					$string_fellesutgifter = trim($string_fellesutgifter);

					$answer[$key]["common costs"] = stripspaces($string_fellesutgifter);
					
				}
				else if ($dotposisjoner = strpos($string, '•',0))
				{
					
					foreach ($eiertyper as $eiertype)
					{
						if (stripos($string, $eiertype))
						{
							$answer[$key]["property type"] = strtolower($eiertype);
							echo $eiertype . '-----------<br>';
							break;
						}
					}

					foreach ($eierform as $form)
					{
						$a = $string;
						$search = $form;

						if (preg_match("/{$search}/i", $a)) 
						{

							$answer[$key]["legal type"] = strtolower($form);
							echo $form . ' found!<br>';
							break;

						}
						
					}

				}
				else
				{
					$answer[$key]["broker"] = mb_strtolower($string);
				}			

				//$answer[$key]["list"] = $string;
				//echo $list->plaintext . '<br>';
			}
			


		}
		echo '';
	}
	else
	{
		echo 'empty!';
	}

	foreach ($answer as $runner => $input)
	{
		$answer[$runner]['area'] = $dir;
	}


	$answer = array_values($answer);

	foreach ($answer as $input)
	{
		
		if (isset($allbox[$input['id']]))
		{
			$count_existing = count($allbox[$input['id']]);
			$count_new = count($input);

			if ($count_new > $count_existing)
			{
				$allbox[$input['id']] = $input;
				echo 'Saving, new data is more than old...<br>';
			}
			else
			{
				echo 'Skipping save, existing data ('. $count_existing . ') is more than new (' . $count_new . ') ...<br>';
			}
		}
		else
		{
			$allbox[$input['id']] = $input;
		}

	}

}

}

echo '<br>Saving ' . count($allbox) . ' entries<br>';

saveJSON($allbox, 'json/index/' . $date . '.json');

?>