<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

if (!empty($_GET["id"])) 
{
	$id = test_input($_GET["id"]);
	//echo $date;
}
else
{
	exit('Ingen id satt');
}

set_time_limit(240);


$filename = "download/showings/" . $id . "/";

if (!file_exists($filename)) 
{
	mkdir("download/showings/" . $id, 0777);
	echo "The directory download/showings/$id was successfully created.<br>";  
} 
$filedir = 'download/showings/' . $id;

//finn kilden og lagre som json

$do_update = 1;

if (file_exists($filedir . '/' . $id. '.html'))
{
	echo "HTML-filen er allerede nedlastet. ";
	$do_update = 0;
}

//last ned detaljer-siden
if ($do_update == 1)
{
	$url = 'https://www.finn.no/realestate/homes/ad.html?finnkode=' . $id;
	$html = download($url);

	if (!stripos($html, 'Beklager, nå er det rusk i maskineriet') and !stripos($html, 'Ingen treff akkurat nå') and !stripos($html, 'Her gikk det unna!'))
	{
		
		$filename = "download/showings/" . $id . "/" . $id . '.html';
		
		if (file_put_contents($filename, $html))
		{
			successecho ('Saved ' . $filename . ' from ' . $url . '. ');
		}
		else
		{
			errorecho ('Failed to save ' . $filename . ' from ' . $url . '. ');
		}
	}
	else
	{
		echo 'Fant ' . '"Beklager, nå er det rusk i maskineriet" eller "Ingen treff akkurat nå", aborting file save... ';
		return;
	}
}

//last ned eierskap-detaljer
//$url = 'https://www.finn.no/realestate/ownershiphistory.html?finnkode=' . $entry['id'];
if ($do_update == 1)
{
	$url = 'https://www.finn.no/realestate/ownershiphistory.html?finnkode=' . $id;
	$html = download($url);

	if (!stripos($html, 'Beklager, nå er det rusk i maskineriet') and !stripos($html, 'Ingen treff akkurat nå') and !stripos($html, 'Her gikk det unna!'))
	{
		
		$filename = "download/showings/" . $id . "/" . $id . '.ownership.html';
		
		if (file_put_contents($filename, $html))
		{
			successecho ('Saved ' . $filename . ' from ' . $url . '. ');
		}
		else
		{
			errorecho ('Failed to save ' . $filename . ' from ' . $url . '. ');
		}
	}
	else
	{
		echo 'Fant ' . '"Beklager, nå er det rusk i maskineriet" eller "Ingen treff akkurat nå", aborting file save... ';
		return;
	}
}


if (!file_exists("download/showings/" . $id . "/images")) 
{
	mkdir("download/showings/" . $id . '/images', 0777);
	echo "The directory download/showings/$id/images was successfully created.<br>";  
} 

echo '<br>';
echo 'Trying to get images...<br>';

$dom = file_get_html($filedir . '/' . $id. '.html', false);

$urls_to_download = [];

$coordinates = [];

if(!empty($dom)) 
{

	foreach($dom->find(".panel") as $key => $panel) 
	{
		//echo '<br><br>';
		//echo $key . '.<strong>';
		foreach($panel->find("a") as $a) 
		{
			//echo $a->plaintext;
			//echo '<br>';
			//echo $a->src;
			//echo $a->href;

			foreach($a->find("img") as $img) 
			{
				
				if (!empty($img->src))
				{
					$urls_to_download[] = $img->src;
				}
				
			}

			foreach($a->find("img") as $img) 
			{
				
				if (!empty($img->{'data-src'}))
				{
					$urls_to_download[] = $img->{'data-src'};
				}

			}	
		}
		break;
		
	}

}

echo 'Identified ' . count($urls_to_download) . ' images to download.<br>';

foreach ($urls_to_download as $key => $url)
{

	//find filename
	$lastpos = strrpos($url, '/');

	$filename = substr($url, $lastpos+1);

	flush_start();
	echo $key . '. Filename: ' . $filename . '. ';
	echo $url;

	if (file_exists('download/showings/' . $id . '/images/' . $filename))
	{
		echo ' | Already exists, will skip. <br>';
		flush_end();
		continue;

	}

	if ($data = download($url))
	{
		
		if (!stripos($data, 'Beklager, nå er det rusk i maskineriet') and !stripos($data, 'Ingen treff akkurat nå') and !stripos($data, 'Her gikk det unna!'))
		{
					

			if (strpos($url, 'cmap') !== false) //just for map
			{
				$coordinates['location'] = $filename;
				saveJSON($coordinates, 'download/showings/' . $id . '/location.json');
				$filename = 'map.png';
			}

			file_put_contents('download/showings/' . $id . '/images/' . $filename, $data);
			echo ' | Saving. ';

			$random = random_int(1, 3);
			echo ' | Sleeping for ' . $random . ' seconds to not download too fast.<br>';
			sleep($random);
			
			
		}
	}
	flush_end();
}

echo 'Download ended.<br>';
echo 'Updating index: <br>';


?>