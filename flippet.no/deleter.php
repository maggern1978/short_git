<?php

require_once "functions.php";

if (!empty($_GET["id"])) 
{
	$id = ($_GET["id"]);
	//echo $date;
}
else
{
	exit('Ingen id satt');
	//return;
	//$id = 211854314;
}

//$id = '210194930';

//find out if is published

$filename_found = '';

if (file_exists('published/log_published/' . $id)) 
{
	echo 'Id: ' . $id . ' is published, will try to unpublish.<br>';
	
	//find correct json
	$jsonfiles = listfiles('published/apartments/');

	foreach ($jsonfiles as $file)
	{
		$data = readJSON('published/apartments/' . $file);

		if ($data['id'] == $id)
		{
			echo 'Found id ' . $id . ' in file ' . $file . '<br>';
			$filename_found = $file;
			break;
		}

	}

	//delete json file 
	echo 'Deleting directory ' . 'published/log_published/' . $id . '<br>';
	if (!rmdir('published/log_published/' . $id))
	{
		exit('Delete failed!');
	}

	//delete directory 
	echo 'Deleting file ' . 'published/apartments/' . $file . '<br>';
	unlink('published/apartments/' . $file);

	//adjust start_count 1 down 
	$count = readJSON('published/start_count.json');

	echo 'Adjusting start_count.json: <br>';
	echo '--Value in: ' . $count['nr']  . '<br>';
	$count['nr'] -= 1; 
	echo '--Value out: ' . $count['nr']  . '<br>';
	
	saveJSON($count, 'published/start_count.json');

	//rename files in front of deleted file

	foreach ($jsonfiles as $jsonfile)
	{

		$name = (int)str_replace(".json", '', $jsonfile);

		if ($name > (int)$filename_found)
		{
			$newname = $name - 1;
			$newname = $newname . '.json';

			echo 'renaming ' . $jsonfile . ' to '. $newname . '<br>';

			//rename(old, new, context)
			rename('published/apartments/' . $jsonfile, 'published/apartments/' . $newname);

		}


	}

echo '<br>';
echo '<br>';
}
else
{
	echo 'Id: ' . $id . ' is NOT published.<br>';
} 


echo 'Unpublishing secton DONE. <br>Moving on to delete "download/details/' . $id . '"<br>';


if (file_exists('download/details/' . $id . '/images')) 
{
	
	$imagefiles = listfiles('download/details/' . $id . '/images');

	foreach ($imagefiles as $imagefile)
	{
		echo 'Deleting in image-folder:' . $imagefile . ' <br>';
		unlink('download/details/' . $id . '/images/' . $imagefile);
	}
	
	if (rmdir('download/details/' . $id . '/images'))
	{
		echo 'Deleted director ' . 'download/details/' . $id . '/images' . '<br>';
	}
	else
	{
		errorecho('Delete of image folder failed! Continuing...');
	}

} 
else
{
	echo 'download/details/' . $id . '/images does not exist...<br>';
}


if (file_exists('download/details/' . $id)) 
{
	
	$files = listfiles('download/details/' . $id . '/');

	foreach ($files as $file)
	{
		echo 'Deleting in folder: download/details/' . $id . '/' . $file . ' <br>';
		unlink('download/details/' . $id . '/' . $file);
	}
	
	if (rmdir('download/details/' . $id . ''))
	{
		echo 'Deleted directory ' . 'download/details/' . $id . '' . '<br>';
	}
	else
	{
		errorecho('Delete of image folder failed! Continuing...');
	}
	
}
else
{
	echo 'download/details/' . $id . ' does not exist...<br>';
}

echo 'Deleter.php DONE!';

?>