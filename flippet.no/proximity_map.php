<?php

include 'functions.php';

//read the points of interest

if (!$point_of_interest = readJSON('json/point_of_interest/point_of_interest_found_list.json'))
{
  echo 'Read error of ' . 'json/point_of_interest/point_of_interest_found_list.json<br>';
  exit();
}


$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/proximity/'. $date . '.json'))
{
  //echo $date . '<br>';
  $date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

  if ($date < $limit_date_previous)
  {
    echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
    return;
  }
}   

if (!$adresses = readJSON('json/proximity/' .  $date . '.json'))
{
  echo 'Read error of ' . 'json/proximity/' .  $date . '.json' . '<br>';
  exit();
}

if (!$average_prices = readJSON('json/areas/average_prices.json'))
{
  echo 'Read error of' . 'json/areas/average_prices.json' . '<br>';
}


?>


<!DOCTYPE html>
<html>
<head>
  <title>Flippet.no - kart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
</head>
<style type="text/css">
  /* Set the size of the div element that contains the map */
  #map {
    height: 1000px;
    /* The height is 400 pixels */
    width: 100%;
    /* The width is the width of the web page */
  }
  .maptitle {
    font-size: 20px;
    font-weight: 700;
  }
  .maptext {
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
  }
  .maparea {
    font-size: 12px;
    font-weight: 300;

  }

</style>
<script>

      // Initialize and add the map
      function initMap() {
        // The location of Uluru
        const oslo = { lat: 59.90168, lng: 10.76113 };

        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), 
        {
          zoom: 13,
          center: oslo,
        });

        const image =
        "images/icon_nearby.png";

        const image_green =
        "images/icon_nearby_green.png";

        const image_gray =
        "images/icon_nearby_gray.png";

        const image_red =
        "images/icon_nearby_red.png";

        const icon_point =
        "images/icon_point2.png";

        <?php //downoaded details
        foreach ($adresses as $key => $house)
        {

        //saved apartments
        $image_pos = strrpos($house['imgurl'], '/');
        $house['imgurl'] = 'download/details/' . $house['id'] . '/images/' . substr($house['imgurl'], $image_pos+1);

        ?>

        const location<?php echo $key; ?> = { lat: <?php echo $house['lat']; ?>, lng: <?php echo $house['lng']; ?> };

        // The marker, positioned at Uluru
        const marker<?php echo $key; ?> = new google.maps.Marker
        ({
          position: location<?php echo $key; ?>,
          map: map,
        });

        marker<?php echo $key; ?>.addListener("mouseover", () => {
          infowindow<?php echo $key; ?>.open(map, marker<?php echo $key; ?>);
        });

        // assuming you also want to hide the infowindow when user mouses-out
        marker<?php echo $key; ?>.addListener('mouseout', function() {
          infowindow<?php echo $key; ?>.close();
        });

        marker<?php echo $key; ?>.addListener('click', function() {
          window.location.href = '<?php echo 'finnscraper_details_show.php?id=' . $house['id']; ?>';
        });

        const contentString<?php echo $key; ?> =
        '<div style="width: 300px;" id="content">' +
        '<center><img  class="lazyload" width="300px" data-src="<?php echo $house['imgurl']; ?>"></center>' +
        '<div id="" class="maptitle" style="margin-bottom: 6px; margin-top: 5px;"><?php
        echo $house['adress']; 

        if (isset($house["area"]))
        {
          echo '<span class="maparea float-right">' . $house['area'] . '</span>';
        }

        ?></div>' +
        '<div id="bodyContent">' +
        "<span class='maptext'>" +
        "<b>Pris:</b> <?php echo money($house['price']) . ' kroner (+' . money($house['price_change']) . ')' ; ?><br>" +
        "<b>Pris pr. kvadrat: </b> <?php echo money($house['price_per_square_meter']); ?> kroner<br>" +
        "<b>Størrelse: </b><?php echo $house['size']; ?> kvadratmeter<br>" +
        "</span>" +

        "<?php 

        if (isset($house['first_spotted']))
        {
         echo "<div class='maparea mt-2'>Først sett: " . $house['first_spotted'] . '</div>';
       }           

       ?>" +
       "</div>" +
       "</div>";

       const infowindow<?php echo $key; ?> = new google.maps.InfoWindow({
        content: contentString<?php echo $key; ?>,
      });

       <?php 

        //and then the apartmens close to the saved 

         foreach ($house['proximity'] as $runner => $nearby_house)
         {

          //finn over ller under snittpris

          $message = '';
          $difference = 0;

          if (isset($nearby_house["size"]))
          {
            $nearby_house['price_per_square_meter'] = (float)$nearby_house['totalprice']/(float)$nearby_house['size'];
          }


          if (isset($nearby_house["area"]))
          {

           if (isset($nearby_house['area']) and isset($average_prices[$nearby_house['property type']][$nearby_house['area']]) and isset($nearby_house['price_per_square_meter']))
           {
             $message = ' Snittpris i ' . $nearby_house['area'] . ': ' . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . ' kroner';

             if (isset($nearby_house['price_per_square_meter']))
             {
               $difference = $nearby_house['price_per_square_meter'] - $average_prices[$nearby_house['property type']][$nearby_house['area']];

               if ($difference > 0)
               {
                $prefix = '+';
              }
              else
              {
                $prefix = '';
              }

              if  ($difference < 0)
              {
                $message = "Under snittpris i området: " . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . " kroner (<span class='text-success font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
              }
              else
              {
                 $message =  "Over snittpris i området: " . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . " kroner (<span class='text-danger font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
              }

            } 
          }
        } 


      if (isset($nearby_house['location']['adresser']['0']['representasjonspunkt']['lat']) and isset($nearby_house['location']['adresser']['0']['representasjonspunkt']['lon'] ))
      {
        $nearby_house['lat'] = $nearby_house['location']['adresser']['0']['representasjonspunkt']['lat'] ;
        $nearby_house['lon'] = $nearby_house['location']['adresser']['0']['representasjonspunkt']['lon'] ;
        $index = $key . '_' . $runner;

        ?>  

        const location<?php echo $index; ?> = { lat: <?php echo $nearby_house['lat']; ?>, lng: <?php echo $nearby_house['lon']; ?> };

                    // The marker, positioned at Uluru
                    const marker<?php echo $index; ?> = new google.maps.Marker
                    ({
                      position: location<?php echo $index; ?>,
                      icon: <?php
                      if ($difference < 0)
                      {
                        echo 'image_green,';
                        echo 'zIndex:3000,';
                      }
                      else if ($difference > 0)
                      {
                        echo 'image_red,';
                        echo 'zIndex:1000,';
                      }
                      else
                      {
                        echo 'image_gray,';
                        echo 'zIndex:2000,';
                      }
                      ?>
                      map: map,
                    });

                    marker<?php echo $index; ?>.addListener("mouseover", () => {
                      infowindow<?php echo $index; ?>.open(map, marker<?php echo $index; ?>);
                    });

                    // assuming you also want to hide the infowindow when user mouses-out
                    marker<?php echo $index; ?>.addListener('mouseout', function() {
                      infowindow<?php echo $index; ?>.close();
                    });

                    marker<?php echo $index; ?>.addListener('click', function() {
                      window.location.href = 'https://www.finn.no/realestate/homes/ad.html?finnkode=' + '<?php echo $nearby_house['id']; ?>';
                    });

                    const contentString<?php echo $index; ?> =
                    '<div style="width: 300px;" id="content">' +
                    '<center><img class="lazyload" width="300px" data-src="<?php echo $nearby_house['imgurl']; ?>"></center>' +
                    '<div id="" class="maptitle" style="margin-bottom: 6px; margin-top: 5px;"><?php
                    echo $nearby_house['adress']; 

                    if (isset($nearby_house["area"]))
                    {
                      echo '<span class="maparea float-right">' . $nearby_house['area'] . '</span>';
                    }

                    ?></div>' +
                    '<div id="bodyContent">' +
                    "<span class='maptext'>" +
                    "<b>Pris:</b> <?php echo money($nearby_house['price']) . ' kroner' ; ?><br>" +
                    "<b>Pris pr. kvadrat: </b>" +
                    "<?php 

                    
                    echo money($nearby_house['price_per_square_meter']); 
                    ?> kroner<br>" +
                    "<b>Størrelse: </b><?php echo $nearby_house['size']; ?> kvadratmeter<br>" +
                    "</span>" +
                    "<?php
                    echo  $message;
                    ?>" +                    
                    "</div>" +
                    "</div>";

                    const infowindow<?php echo $index; ?> = new google.maps.InfoWindow({
                      content: contentString<?php echo $index; ?>,
                    });

                    <?php 
                  }


                }



              }

              ?>



        <?php //points of interest!
        foreach ($point_of_interest as $key => $house)
        {

          $key = $key . '_interest';
        //saved apartments
          ?>

          const location<?php echo $key; ?> = { lat: <?php echo $house['lat']; ?>, lng: <?php echo $house['lon']; ?> };

        // The marker, positioned at Uluru
        const marker<?php echo $key; ?> = new google.maps.Marker
        ({
          position: location<?php echo $key; ?>,
          map: map,
          icon: icon_point,
        });

        marker<?php echo $key; ?>.addListener("mouseover", () => {
          infowindow<?php echo $key; ?>.open(map, marker<?php echo $key; ?>);
        });

        // assuming you also want to hide the infowindow when user mouses-out
        marker<?php echo $key; ?>.addListener('mouseout', function() {
          infowindow<?php echo $key; ?>.close();
        });

        const contentString<?php echo $key; ?> =
        '<div style="width: 300px;" id="content">' +
        '<div id="" class="maptitle" style="margin-bottom: 6px; margin-top: 5px;">Interessepunkt: <?php
        echo $house['adress']; 
        ?></div>' +
        '<div id="bodyContent">' +
        "<span class='maptext'>" +
        "</span>" +
        "<?php 

        if (isset($house['added_date']))
        {
         echo "<div class='maparea mt-2'>Lagt til: " . $house['added_date'] . '</div>';
       }           

       ?>" +
       "</div>" +
       "</div>";

       const infowindow<?php echo $key; ?> = new google.maps.InfoWindow({
        content: contentString<?php echo $key; ?>,
      });

       <?php 

        //and then the apartmens close to the point of interest 

       foreach ($house['proximity'] as $runner => $nearby_house)
       {
      
          $message = '';
          $difference = 0;

          if (isset($nearby_house["size"]))
          {
            $nearby_house['price_per_square_meter'] = (float)$nearby_house['totalprice']/(float)$nearby_house['size'];
          }

           

          if (isset($nearby_house["area"]))
          {

           if (isset($nearby_house['area']) and isset($average_prices[$nearby_house['property type']][$nearby_house['area']]) and isset($nearby_house['price_per_square_meter']))
           {
             $message = ' Snittpris i ' . $nearby_house['area'] . ': ' . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . ' kroner';

             if (isset($nearby_house['price_per_square_meter']))
             {
               $difference = $nearby_house['price_per_square_meter'] - $average_prices[$nearby_house['property type']][$nearby_house['area']];

               if ($difference > 0)
               {
                $prefix = '+';
              }
              else
              {
                $prefix = '';
              }

              if  ($difference < 0)
              {
                $message = "Under snittpris i området: " . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . " kroner (<span class='text-success font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
              }
              else
              {
                 $message =  "Over snittpris i området: " . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . " kroner (<span class='text-danger font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
              }

            } 
          }
        } 

        if (isset($nearby_house['location']['adresser']['0']['representasjonspunkt']['lat']) and isset($nearby_house['location']['adresser']['0']['representasjonspunkt']['lon'] ))
        {
          $nearby_house['lat'] = $nearby_house['location']['adresser']['0']['representasjonspunkt']['lat'] ;
          $nearby_house['lon'] = $nearby_house['location']['adresser']['0']['representasjonspunkt']['lon'] ;
          $index = $key . '_' . $runner;

          ?>  

          const location<?php echo $index; ?> = { lat: <?php echo $nearby_house['lat']; ?>, lng: <?php echo $nearby_house['lon']; ?> };

                    // The marker, positioned at Uluru
                    const marker<?php echo $index; ?> = new google.maps.Marker
                    ({
                      position: location<?php echo $index; ?>,
                      icon: <?php
                      if ($difference < 0)
                      {
                        echo 'image_green,';
                        echo 'zIndex:3000,';
                      }
                      else if ($difference > 0)
                      {
                        echo 'image_red,';
                        echo 'zIndex:1000,';
                      }
                      else
                      {
                        echo 'image_gray,';
                        echo 'zIndex:2000,';
                      }
                      ?>                     
                      map: map,
                    });

                    marker<?php echo $index; ?>.addListener("mouseover", () => {
                      infowindow<?php echo $index; ?>.open(map, marker<?php echo $index; ?>);
                    });

                    // assuming you also want to hide the infowindow when user mouses-out
                    marker<?php echo $index; ?>.addListener('mouseout', function() {
                      infowindow<?php echo $index; ?>.close();
                    });

                    marker<?php echo $index; ?>.addListener('click', function() {
                      window.location.href = 'https://www.finn.no/realestate/homes/ad.html?finnkode=' + '<?php echo $nearby_house['id']; ?>';
                    });

                    const contentString<?php echo $index; ?> =
                    '<div style="width: 300px;" id="content">' +
                    '<center><img width="300px" src="<?php echo $nearby_house['imgurl']; ?>"></center>' +
                    '<div id="" class="maptitle" style="margin-bottom: 6px; margin-top: 5px;"><?php
                    echo $nearby_house['adress']; 

                    if (isset($nearby_house["area"]))
                    {
                      echo '<span class="maparea float-right">' . $nearby_house['area'] . '</span>';
                    }

                    ?></div>' +
                    '<div id="bodyContent">' +
                    "<span class='maptext'>" +
                    "<b>Pris:</b> <?php echo money($nearby_house['price']) . ' kroner' ; ?><br>" +
                    "<b>Pris pr. kvadrat: </b>" +
                    "<?php 
                    if (isset($nearby_house['size']))
                    {
                      $nearby_house['price_per_square_meter'] = (float)$nearby_house['totalprice']/(float)$nearby_house['size'];
                      echo money($nearby_house['price_per_square_meter']);                       
                    }

                    ?> kroner<br>" +
                    "<b>Størrelse: </b><?php 
                    if (isset($nearby_house['size']))
                    {
                      echo $nearby_house['size']; 
                    }
                    ?> kvadratmeter<br>" +
                    "</span>" +
                    "<?php
                    echo $message; 
                    ?>" +

                  "</div>" +
                  "</div>";

                  const infowindow<?php echo $index; ?> = new google.maps.InfoWindow({
                    content: contentString<?php echo $index; ?>,
                  });

                  <?php 
                }
              }
            }
            ?>
          }

        </script>

        <body>

          <!--The div element for the map -->
          <div id="map"></div>

          <!-- Async script executes immediately and must be after any DOM elements used in callback. -->
          <script
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIDFDKvAJqNw1zx04g3JA1ymxWRfXp0Y8&callback=initMap&libraries=&v=weekly"
          async
          ></script>
          <script src="js/lazysizes.min.js" ></script>
          <script>

            window.lazySizesConfig = window.lazySizesConfig || {};
            lazySizesConfig.loadMode = 1;

          </script>          
        </body>
        </html>
        <?php



        ?>