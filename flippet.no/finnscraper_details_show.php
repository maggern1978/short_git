<?php

require_once "functions.php";

if (!empty($_GET["id"])) 
{
	$id = ($_GET["id"]);
	//echo $date;
}
else
{
	exit('Ingen id satt');
	return;
	//$id = 211854314;
}


if (!file_exists('download/details/' . $id . '/images')) 
{
	echo 'No images to show, folder does not exist.<br>';
	return;
} 

if (!$files = listfiles('download/details/' . $id . '/images'))
{
	exit('Kan ikke liste bildemappe');
}


if (!$leilighet = readJSON('download/details/' . $id . '/' . $id . '.json'))
{
	exit('Kan ikke leste detaljer');
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Detaljer - <?php echo $leilighet['adress']; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
	.thumbnail {
		width: 350px; /* You can set the dimensions to whatever you want */
		height: 233px;
		object-fit: cover;
	}

</style>
<body>
	<div class="container mt-3">
		<div class="row">
			<div class="col-12">
				<h2 class="text-secondary">Detaljer og bildegalleri for id <?php echo $leilighet['id']; ?></h2>
			</div>
		</div>
	</div>
	<div class="container mt-2">
		<div class="row ">
			<?php 
			
			if (isset($leilighet['property type']) and $leilighet['property type'] == 'garasje/parkering')
			{
					//continue;
			}

			if (!isset($leilighet['totalprice'])) 	
			{
				$leilighet['totalprice'] = $leilighet['price'];
			}

			if (!isset($leilighet['common costs'])) 	
			{
				$leilighet['common costs'] = '-';
			}

			if (!isset($leilighet['property type'])) 	
			{
				$leilighet['property type'] = '-';
			}

			if ($start = strpos($leilighet['price'], '-'))
			{
				$leilighet['price'] = substr($leilighet['price'], $start+1);
			}

			if ($start = strpos($leilighet['totalprice'], '-'))
			{
				$leilighet['totalprice'] = substr($leilighet['totalprice'], $start+1);
			}

			if (!isset($leilighet['size']))
			{
					//var_dump($leilighet);
					//continue;
			}

			if (isset($leilighet['size']))
			{

				if ($start = strpos($leilighet['size'], '-'))
				{
						//continue;
				}

			}

			$today = date('Y-m-d');

			if ($leilighet['property type'] == 'Garasje/Parkering' or $leilighet['property type'] == 'andre')
			{
					//continue;
			}

				$average = $leilighet['totalprice'] / $leilighet['size']; //brukes under
				$average = $leilighet['totalprice'] / $leilighet['size']; //brukes under

				//find out if part of apartment building
				if ($leilighet['ownership_repeat'] > 1) //mer enn en bør stemme
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}

				else if ($leilighet['ownership_repeat'] == 1 and $leilighet['ownership_count'] == 1) //to like oppføringer, bør stemme
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}
				else if ($leilighet['ownership_repeat'] == 0 and $leilighet['ownership_count'] == 1) //bare en oppføring, bør stemme				{
					{
						$badgecolor = 'success';
						$badgetext = 'Flippet';
					}												
					else if ((int)$leilighet['ownership_repeat'] == 1 and (int)$leilighet['ownership_count'] > 4)
					{

						$price_limit = 1.1;

						if (isset($average_prices[$leilighet['area']]))
						{

							$ratio = (float)$average / (float)$average_prices[$leilighet['area']];

							$price_limit_percent = round(($ratio -1)*100,1);

						if ($ratio > $price_limit) //only include if at least 10% over average price in area pr square meter
						{
							$badgecolor = 'primary';
							$badgetext = 'Ukjent, del av boligkompleks ('. $price_limit_percent .  '% over snittpris)';
						}
						else
						{
							//continue;
						}
					}
					else
					{
						//continue;
					}

				}
				else if (($leilighet['ownership_repeat'] / $leilighet['ownership_count']) > 0.7)
				{
					$badgecolor = 'success';
					$badgetext = 'Flippet';
				}				
				else
				{
					$badgecolor = 'seconday';
					$badgetext = 'Uklassifisert (Antall funnet ' . $leilighet['ownership_count'] . ')';
				}




				//find local main image

				if ($start_position = strrpos($leilighet['imgurl'],"/"))
				{
					$filename_first_image = substr($leilighet['imgurl'], $start_position+1);

					if (file_exists('download/details/'. $leilighet['id'] . '/images/' . $filename_first_image))
					{
						$leilighet['imgurl'] ='download/details/'. $leilighet['id'] . '/images/' . $filename_first_image;
					}


				}

				?>

				<div class="col-4 mb-4"><a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $leilighet['id']; ?>"> 
					<img src="<?php echo $leilighet['imgurl'] ;?>" width="100%" class="img-responsive">
				</a>
			</div>
			<div class="col-8 mb-4">
				<div class="d-flex flex-column ">
					<div class="d-flex justify-content-between">
						<div class="mb-auto">
							<span class="font-weight-bold">Adresse:</span>
							<?php 
							echo $leilighet['adress']; 

							if (isset($leilighet['area']))
							{
								echo ' | ' . $leilighet['area']; 
							}
							
							if (isset($leilighet['fylke']))
							{
								echo ' | ' . $leilighet['fylke']; 
							} 

							?>
						</div>
						<div>
							<?php 

							if (isset($leilighet['first_spotted']))
							{
								?>

								<div class="badge bg-secondary text-white">Først sett: <?php echo $leilighet['first_spotted']; ?></div>		
								<?php 
							}
							?>
						</div>
					</div>
					<div class="mt-1">
						<h4><a href="https://www.finn.no/realestate/homes/ad.html?finnkode=<?php echo $leilighet['id']; ?>"> <?php echo $leilighet['title']; ?></a></h4>
					</div>
					
					<div class="d-flex justify-content-between mb-2 mt-2 ">
						<div class="d-flex flex-column">
							<div class="">	
								<span class="h5">
									<?php
									
									echo number_format($average,0,'.','.')  . ',- pr. m²';
									?>
								</span>
							</div>
							<?php
							if (isset($leilighet['area']) and isset($average_prices[$leilighet['area']]))
							{
								?>
								<div class="text-secondary" data-toggle="tooltip" title="Snittpris pr. m² i området <?php echo $leilighet['area']; ?>">
									<?php 

									if ($average_prices[$leilighet['area']] > $average)
									{
										echo '<i class="fa fa-arrow-circle-down mr-1" aria-hidden="true"></i>';
									}
									else
									{
										echo '<i class="fa fa-arrow-circle-up mr-1" aria-hidden="true"></i>';
									}

									echo number_format($average_prices[$leilighet['area']],0,'.','.') . ',- ' ; 
									?>
								</div>
								<?php
							}
							?>

						</div>
						<div class="text-right">
							<h5>
								<?php

								echo number_format($leilighet['size'],0,'.','.')  . ' m²';
								?>
							</h5>
						</div>								
						<div class="text-right">

							<h5>
								<?php

								echo number_format($leilighet['price'],0,'.','.')  . ' kroner';
								?>
							</h5>
						</div>
					</div>
					<div class="mt-1"><span class="font-weight-bold">Totalpris:</span> <?php echo number_format($leilighet['totalprice'],0,'.','.') . ' kroner';
						echo ' | ' . $leilighet['property type'] . ' | ' . $leilighet['legal type'];
						?>

					</div>
					<div class="">
						<span class="font-weight-bold"><a data-toggle="modal" data-target="#modal_<?php echo $leilighet['id']; ?>" href="">Siste tinglysning:</a></span>

						<!-- Modal -->
						<div class="modal" id="modal_<?php echo $leilighet['id']; ?>">
							<div class="modal-dialog" >
								<div class="modal-content">

									<!-- Modal Header -->
									<div class="modal-header">
										<h4 class="modal-title">Tinglysninger</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>

									<!-- Modal body -->
									<div class="modal-body">
										<?php

										if (isset($leilighet['ownership']['eieform']))
										{
											echo 'Eieform: ' . $leilighet['ownership']['eieform'] . '<br>';
										}
										if (isset($leilighet['ownership']['kommunenr']))
										{
											echo 'Kommunenr: ' . $leilighet['ownership']['kommunenr'] . '<br>';
										}						        
										if (isset($leilighet['ownership']['gårdsnr']))
										{
											echo 'Gårdsnr: ' . $leilighet['ownership']['gårdsnr'] . '<br>';
										}
										if (isset($leilighet['ownership']['bruksnr']))
										{
											echo 'Bruksnr: ' . $leilighet['ownership']['bruksnr'] . '<br>';
										}					

										?>

										<table class="table  mt-2">
											<thead>
												<tr>
													<?php
													echo '<th>';
													echo 'Tinglyst: '; 
													echo '</th>';
													echo '<th>';
													echo 'Boligtype: '; 
													echo '</th>';
													echo '<th class="text-right">';
													echo 'Andelsnummer: '; 
													echo '</th>';
													echo '<th class="text-right">';
													echo 'Pris: '; 
													echo '</th>';
													?>

												</tr>
											</thead>										
											<tbody>

												<?php

												foreach ($leilighet['ownership']['tinglysninger'] as $key => $tinglysning)
												{
													?>
													<tr>

														<?php

														if (isset($tinglysning['tinglyst']))
														{
															echo '<td>';
															echo '' . $tinglysning['tinglyst']; 
															echo '</td>';
														}
														else
														{
															echo '<td>';														
															echo '</td>';
														}

														if (isset($tinglysning['boligtype']))
														{
															echo '<td>';
															echo '' . $tinglysning['boligtype']; 
															echo '</td>';
														}
														else
														{
															echo '<td>';														
															echo '</td>';
														}														

														if (isset($tinglysning['andelsnummer']))
														{
															echo '<td class="text-right">';
															echo '' . $tinglysning['andelsnummer']; 
															echo '</td>';
														}
														else
														{
															echo '<td>';														
															echo '</td>';
														}

														if (isset($tinglysning['pris']))
														{
															echo '<td class="text-right">';
															echo '' . $tinglysning['pris']; 
															echo '</td>';
														}
														else
														{
															echo '<td>';														
															echo '</td>';
														}	
														?>

													</tr>
													<?php
												}

												?>

											</tbody>
										</table>
									</div>

									<!-- Modal footer -->
									<div class="modal-footer">
										<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
									</div>

								</div>
							</div>
						</div>				

						<?php 
						$earlier = new DateTime("today");
						$later = new DateTime($leilighet['tinglyst_newest']);

						$interval = $later->diff($earlier);

						$datestring = '';

						if ($interval->format('%y') > 0)
						{
							$datestring .= $interval->format('%y') . ' år, ';

							if ($interval->format('%m') == 1)
							{
								$datestring .= $interval->format('%m'). ' måned og ';

							}
							else if ($interval->format('%m') == 0)
							{

							}									
							else
							{
								$datestring .= $interval->format('%m'). ' måneder og ';

							}

							if ($interval->format('%d') == 1)
							{

								$datestring .= $interval->format('%d') . ' dag';
							}
							else
							{

								$datestring .= $interval->format('%d') . ' dager';								
							}
						}

						else if ($interval->format('%m') > 0)
						{
							if ($interval->format('%m') == 1)
							{
								$datestring .= $interval->format('%m'). ' måned og ';

							}
							else
							{
								$datestring .= $interval->format('%m'). ' måneder og ';

							}

							if ($interval->format('%d') == 1)
							{

								$datestring .= $interval->format('%d') . ' dag';
							}
							else
							{

								$datestring .= $interval->format('%d') . ' dager';								
							}

						}
						else
						{
							if ($interval->format('%d') == 1)
							{

								$datestring .= $interval->format('%d') . ' dag';
							}
							else
							{

								$datestring .= $interval->format('%d') . ' dager';								
							}


						}

						echo $leilighet['tinglyst_newest'] . ' (' .  $datestring . ' siden)';



						$badgefontsize = '';

						if ($badgetext == 'Flippet')
						{
							$addedprice = $leilighet['price'] - $leilighet['ownership']['tinglysninger'][0]['pris'];

							$addedprice = number_format($addedprice,0,'.','.');

							if ($addedprice > 0)
							{
								$badgetext .= ' (+' . $addedprice . ')';
							}
							else if ($addedprice == 0)
							{
								$badgecolor = 'secondary';
								$badgetext = 'Lik pris';
								$badgetext .= '';
							}
							else
							{
								$badgecolor = 'danger';
								$badgetext = 'Tap';
								$badgetext .= ' (' . $addedprice . ')';
							}
							$badgefontsize = 'badgetext';
						}

						?>
						<span class="badge  <?php echo $badgefontsize; ?>  text-white bg-<?php echo $badgecolor; ?>"><?php echo $badgetext; ?></span>
					</div>
					<div class="d-flex justify-content-between">					
						<div class=""><span class="font-weight-bold">Fellesutgifter: </span> 
							<?php 

							if (is_numeric($leilighet['common costs']) or is_float($leilighet['common costs']) or is_int($leilighet['common costs']))
							{
								echo number_format($leilighet['common costs'],0,'.','.')  . ' kroner'; 
							}
							else if (!empty($leilighet['common costs'] ))
							{
								echo $leilighet['common costs']  . ' kroner'; 
							}
							else
							{
								echo '-';
							}
							

							?></div>


							<?php
						//sjekk om allerede nedlasted

							if (!file_exists('download/details/' . $leilighet['id']))
							{
								
								?>

								<div class="" data-toggle="tooltip" title="Last ned detaljer-siden hos Finn.no">
									
									<a href="finnscraper_details.php?id=
									<?php 
									echo $leilighet['id']; 
									echo '&dato=' . $date;
									?>">
									<i class="fa text-secondary fa-download fa-2x" aria-hidden="true"></i></a></div>
									<?php 
								}
								else
								{

									?>

									<div class="" data-toggle="tooltip" title="Detaljer allerede lastet ned hos Finn.no">
										
										<a href="finnscraper_details_show.php?id=
										<?php 
										echo $leilighet['id']; 
							//echo '&dato=' . $date;
										?>">
										<i class="text-success fa fa-check-square fa-2x" aria-hidden="true"></i></a></div>
										<?php
									} 
									?>

								</div>
								
							</div>
						</div>
						<?php  //var_dump($indexdata[0]); ?>
					</div>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-12 mb-2">
							<div class="mr-3">
								<a href="<?php echo 'download/details/' . $id . '/' . $id . '.html'; ?>" class="btn btn-secondary" role="button">
									<div class="d-flex mr-3">
										<div class="mr-2 pt-1 "><i class="fa fa-th-list fa-2x mr-1" aria-hidden="true"></i> </div>
										<div class="my-auto 1">Se lagret finn.no-annonse
										</div>
									</div>
								</a>
							</div>

						</div>
					</div>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-12 mb-2">
							<h4><?php echo count($files); ?> bilder tilgjengelig: </h4>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="">
							<div class="d-flex flex-wrap">
								<?php 
								if (file_exists('download/details/' . $id . '/images/map.png'))
								{
									?>
									<div class="col-4 mb-2 thumbnail"><a href ="download/details/<?php echo $id . '/images/map.png'; ?>"> <img class="img-fluid thumbnail" src="download/details/<?php echo $id . '/images/map.png'; ?>"></a></div>

									<?php
								}


								foreach ($files as $file)
								{
									if ($file == 'map.png')
									{
										continue;
									}
									?>

									<div class="col-4 mb-2 text-center "><a href ="download/details/<?php echo $id . '/images/' . $file; ?>"> <img class="img-fluid thumbnail" style="max-height: 233px;" src="download/details/<?php echo $id . '/images/' . $file; ?>"></a></div>

									<?php
								}

								?>

							</div>
						</div>
					</div>

				</body>
				</html>