<?php

include 'functions.php';


  if (!empty($_GET["min"])) 
  {
    $min_limit = test_input($_GET["min"]);
    //echo $date;
  }
  else
  {
    $min_limit = 0;
  }


  if (!empty($_GET["max"])) 
  {
    $max_limit = test_input($_GET["max"]);
    //echo $date;
  }
  else
  {
    $max_limit = 999999999999;
  }

//inputfiler:
//point_of_interest_list.json
//average_prices.json
//json/maps/dato

//read the points of interest
if (!$point_of_interest = readJSON('json/point_of_interest/point_of_interest_list.json'))
{
  echo 'Read error of ' . 'json/point_of_interest/point_of_interest_found_list.json<br>';
  exit();
}


$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/details/'. $date . '.json'))
{
  //echo $date . '<br>';
  $date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

  if ($date < $limit_date_previous)
  {
    echo 'json/details/$date files not found, returning... Ingen nyere oppføringer.<br>';
    return;
  }
}   

if (!$adresses = readJSON('json/details/' .  $date . '.json'))
{
  echo 'Read error of ' . 'json/details/' .  $date . '.json' . '<br>';
  exit();
}

if (!$average_prices = readJSON('json/areas/average_prices.json'))
{
  echo 'Read error of' . 'json/areas/average_prices.json' . '<br>';
  exit();
}


$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/maps/'. $date . '.json'))
{
  //echo $date . '<br>';
  $date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

  if ($date < $limit_date_previous)
  {
    echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
    return;
  }
}   

if (!$index = readJSON('json/maps/' .  $date . '.json'))
{
  echo 'Read error of ' . 'json/maps/' .  $date . '.json' . '<br>';
  exit();
}

?>


<!DOCTYPE html>
<html>
<head>
  <title>Flippet.no - kart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
  
</head>
<style type="text/css">
  /* Set the size of the div element that contains the map */
  #map {
    height: 570px;
    /* The height is 400 pixels */
    width: 100%;
    /* The width is the width of the web page */
  }
  .maptitle {
    font-size: 20px;
    font-weight: 700;
  }
  .maptext {
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
  }
  .maparea {
    font-size: 12px;
    font-weight: 300;

  }

</style>
<script>

      // Initialize and add the map
      function initMap() {
        // The location of Uluru
        const oslo = { lat: 59.90168, lng: 10.76113 };

        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), 
        {
          zoom: 13,
          center: oslo,
        });

        const image =
        "images/icon_nearby.png";

        const image_green =
        "images/icon_nearby_green.png";

        const image_gray =
        "images/icon_nearby_gray.png";

        const image_red =
        "images/icon_nearby_red.png";

        const icon_point =
        "images/icon_point2.png";

        <?php //downoaded details //saved apartments
        foreach ($adresses as $key => $house)
        {
          details($house, $key);
        }

        //points of interest!
        foreach ($point_of_interest as $key => $house)
        {
          point_of_interest($house, $key);
        }

        foreach ($index as $key => $house)
        { 

          if (isset($house['totalprice']))
          {
            if ($house['totalprice'] <= $max_limit and $house['totalprice'] >= $min_limit)
            {
              index_apartment($house, $key, $average_prices);
            }
          }
          else
          {
            if ($house['price'] <= $max_limit and $house['price'] >= $min_limit)
            {
              index_apartment($house, $key, $average_prices);
            }
          }
         
        }

        ?>
      }

    </script>

    <body>

      <!--The div element for the map -->
      <div id="map"></div>
      <script type="text/javascript">

      var avail_height = $(window).height();

      $("#map").height(avail_height);

        function loadScript() {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDIDFDKvAJqNw1zx04g3JA1ymxWRfXp0Y8&callback=initMap&libraries=&v=weekly';
          document.body.appendChild(script);

          $.getScript( "js/lazysizes.min.js", function( data, textStatus, jqxhr ) 
          {
            console.log( data ); // Data returned
            console.log( textStatus ); // Success
            console.log( jqxhr.status ); // 200
            console.log( "Load was performed." );
          });

          window.lazySizesConfig = window.lazySizesConfig || {};
          lazySizesConfig.loadMode = 1;
        }

        window.onload = loadScript;

      </script>
    </body>
    </html>

    <?php

    
?>


