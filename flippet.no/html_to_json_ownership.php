<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

//https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC
//flippet.no

$date = date('Y-m-d');

$files = listfiles('download/ownership/');

$allbox = [];

$runneren = 0;

foreach ($files as $key => $file)
{

	$id = str_replace('.html', '', $file);

	$answer = array();

	//$dom = file_get_html("download/ownership/" . $date . '/' . $file, true);
	echo $runneren++  . '. ' . $file . '. ';

	$dom = str_get_html(file_get_contents("download/ownership/" . $file)); 

	if(!empty($dom)) 
	{

		successecho(' Download ok <br>');
		$indexarray = [];
		$valuearray = [];

		foreach($dom->find(".definition-list") as $runner => $definition) 
		{

			foreach($definition->find("dt") as $dtkey => $dtname) 
			{
				$name = strtolower($dtname->plaintext);

				if ($name == 'seksjonsnr')
				{

					$name = 'seksjonsnummer';

				}

				$indexarray[] = strtolower($name);
			}

			foreach($definition->find("dd") as $ddkey => $ddname) 
			{

				$valuearray[] = strtolower($ddname->plaintext);
				
			}			

		}

		foreach ($indexarray as $key => $index)
		{
			$allbox[$id][$index] = $valuearray[$key];
		}

		foreach($dom->find(".data-table") as $index => $table) 
		{

			//find header and build indexs
			$indexes = [];

			foreach($table->find("th") as $th) 
			{
				$indexes[] = strtolower($th->plaintext);
			}

			$indexescount = count($indexes);

			foreach($table->find("tbody") as $tbody) 
			{

				foreach($tbody->find("tr") as $index => $tr) 
				{
					$array = [];

					foreach($tr->find("td") as $runner => $td) 
					{

						if ($runner == 0)
						{
							$array[strtolower($indexes[0])] = mb_strtolower($td->plaintext);
						}
						else if ( $runner == 1)
						{
							$array[strtolower($indexes[1])] = mb_strtolower($td->plaintext);
						}
						else if ( $runner == 2)
						{
							$array[strtolower($indexes[2])] = mb_strtolower($td->plaintext);
						}
						else if ($runner == 3)
						{
							$array[strtolower($indexes[3])] = mb_strtolower($td->plaintext);
						}					

					//$allbox[$id]["title"] = $td->plaintext;
					//echo $td->plaintext . '<br>';
					}


					if (isset($array['tinglyst'])) //16.09.1997
					{
						$datearray = explode(".", $array['tinglyst']);
						$array['tinglyst'] = $datearray[2] . '-' . $datearray[1] . '-' . $datearray[0];

						//$date = date_create_from_format('d.m.Y', $array['tinglyst']);
						//echo date_format($date, 'Y-m-d');
						//$array['tinglyst'] = (string)date_format($date, 'Y-m-d');

					}

					if (isset($array['pris']))
					{
						$array['pris'] = str_replace(',−', '', $array['pris']);
						$array['pris'] = stripspaces($array['pris']);
						$array['pris'] = trim($array['pris']);
					}

					if (!empty($array))
					{
						$array['id'] = $id;

						foreach ($array as $key => $line)
						{
							$allbox[$id]['tinglysninger'][$index][$key] = mb_strtolower($line);
						}
						
					}
					else
					{
						echo '| Empty';
					}
					
				}
			}
			echo '</strong>';
		}
	}
	else
	{
		errorecho('Empty: ' . "download/ownership/" . $file . '<br>');
	}
	
	//var_dump($dom);
	//exit();	

}
echo 'Out: ' . count($allbox);

echo '<br>';
saveJSON($allbox, 'json/ownership/' . $date . '.json');

?>