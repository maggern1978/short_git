<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

//https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC
//flippet.no

set_time_limit(6000);

$date = date('Y-m-d');

while (!file_exists('json/ownership/'. $date . '.json'))
{

	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	echo 'Trying download/ownership/' . 	$date . '.json<br>';
	if ($date < '2020-01-01')
	{
		echo 'Json files not found, returning...<br>';
		return;
	}
}

$today = $date;

if (!$json = readJSON('json/ownership/' . $today . '.json'))
{
	echo 'json/ownership/' . $today . '.json is not set<br>';
	return;
}

echo 'Doing ' . 'json/ownership/' . $today . '.json' . '<br>';

$runner = 0; 

foreach ($json as $key => $entry)
{

	echo $runner++ . ' -> ' . $key . '. ';

	//check if id already exists, if not set date to today
	if (file_exists('data/ownership/' . $key))
	{
		echo 'Id directory already exists. ';

		$newestfile = readJSON('data/ownership/' .  $key . '/' . $key . '.json');

		if ($newestfile == $entry)
		{
			echo 'Entry is the same as newest file, going to next. <br>';
			continue;
		}
		else
		{
			echo 'Entry is NOT the same as newest file ' . "data/ownership/" . $key . '/' . $key . '.json <br>'; 

			saveJSON($entry, 'data/ownership/' .  $key  . '/' . $key  . '.json');
			continue; 
		}
	}

	else
	{
		echo "data/ownership/" .  $key  . '/' . 'does not exists, will create <br>.';		
		mkdir('data/ownership/' . $key , 0777);
		saveJSON($entry, "data/ownership/" .  $key  . '/' . $key . '.json');
	}

	echo '<br>';
}







?>