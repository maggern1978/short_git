<?php
require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

//https://www.finn.no/realestate/homes/search.html?location=0.20061&sort=PRICE_SQM_ASC
//flippet.no

set_time_limit(300);

$today = date('Y-m-d');
//$today = '2021-01-24';

$today = date('Y-m-d');

while (!file_exists('json/index/' . $today . '.json'))
{

	$today = date('Y-m-d', strtotime("-1 day", strtotime($today)));
	echo 'Trying ' . 	$today . '.json<br>';

	if ($today < '2020-01-01')
	{
		echo 'Json files not found, returning...<br>';
		return;
	}
}

if (!$json = readJSON('json/index/' . $today . '.json'))
{
	echo 'json/index/' . $today . '.json is not set<br>';
	return;
}

echo 'Doing ' . 'json/index/' . $today . '.json' . '<br>';

$runner = 0; 

foreach ($json as $key => $entry)
{

	echo $runner++ . ' -> ' . $key . '. ';

	//check if id already exists, if not set date to today
	if (file_exists('data/index/' . $entry['id']))
	{
		echo 'Id directory already exists. ';
		saveJSON($entry, 'data/index/' .  $entry['id'] . '/' . $entry['id'] . '.json');

	}
	else
	{
		echo "data/index/" .  $entry['id'] . '/' . ' does not exists, will create <br>.';		
		mkdir('data/index/' . $entry['id'], 0777);
		
		saveJSON($entry, "data/index/" .  $entry['id'] . '/' . $entry['id'] . '.json');
		
		//making fist seen
		$first_seen = $today;

		saveJSON($first_seen, "data/index/" .  $entry['id'] . '/' . 'first_seen.json');
		
	}

	echo '<br>';

}



?>