<?php

include 'functions.php';

//get latest average prices

if (!$average_prices = readJSON('json/areas/average_prices.json'))
{
  echo 'Read error of' . 'json/areas/average_prices.json' . '<br>';
}



include 'bit_head.html';

?>
<body>

  <div class="container ">
    <div class="row">
      <div class="col-12">
        <h1>Snittpriser siste år i Oslo:</h1>  
        <p></p>
        <?php 

        foreach ($average_prices as $index => $type)
        {
          echo '<h4>' . ucwords($index) . '</h4>';

          ?>

          <table class="table table-striped">
            <thead>
              <tr>
                <th>Område</th>
                <th class="text-right">Pris</th>
              </tr>
            </thead>
            <tbody>

              <?php

              foreach ($type as $key => $area)
              {

                ?>

                <tr>
                  <td><?php echo $key; ?></td>
                  <td class="text-right"><?php echo money($area); ?> kroner </td>
                </tr>     

                <?php
              }
              ?>
              </tbody>
              </table>
              <?php
            }

            ?>
     


      </div>
    </div>
  </div>


</body>
</html>