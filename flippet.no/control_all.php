<?php

	include 'finnscraper_index_bydeler.php'; // laster ned alle index

	echo '<strong>-------------------------</strong>';
	include 'html_to_json_bydeler.php'; // rips data from html and stores in json til json/index

	echo '<strong>-------------------------</strong>';
	include 'json_splitter_index.php'; //splits json into folders in data/

	echo '<strong>-------------------------</strong>';
	include 'finnscraper_ownership.php'; // takes /json/index/date.json and downloads ownership info

	echo '<strong>-------------------------</strong>';
	include 'html_to_json_ownership.php'; // rips data from ownership html and stores in json

	echo '<strong>-------------------------</strong>';
	include 'json_splitter_ownership.php'; //makes ownership json

	echo '<strong>-------------------------</strong>';
	include 'jsons_joiner.php'; // makes joined json

	echo '<strong>-------------------------</strong>';
	include 'finnscraper_area_price.php'; //download area html files 

	echo '<strong>-------------------------</strong>';
	include 'html_to_json_area.php'; //makes area average prices json file 

	echo '<strong>-------------------------</strong>';
	include 'details_json_maker.php'; //makes area average prices json file 

	echo '<strong>-------------------------</strong>';
	include 'publish_top_flips.php'; //publish flips

	echo '<strong>-------------------------</strong>';
	include 'proximity_control.php'; //kjører proximity

	echo '<strong>-------------------------</strong>';
	include 'html_to_json_showings.php'; //kjører proximity
	
?>