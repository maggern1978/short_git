<?php

include_once 'functions.php';


$adresses = [];

$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/index/'. $date . '.json'))
{
	//echo $date . '<br>';
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

	if ($date < $limit_date_previous)
	{
		echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
		return;
	}
}		

if (!$indexdata = readJSON('json/index/' .  $date . '.json'))
{
	echo 'Read error of ' . 'json/index/' .  $date . '.json' . '<br>';
}

echo 'Antall boliger inn: ' . count($indexdata) . '<br>';

$error_counter = 0;

foreach ($indexdata as $key => $entry)
{

	$filename = "download/maps/" . $entry['id'] . "/";

	if (!file_exists($filename)) 
	{
		mkdir($filename, 0777);
		echo "The directory $filename was successfully created.<br>";

	} 

	$filename = $filename . $entry['id'] . '.json';

	if (file_exists($filename)) 
	{
		//echo $filename . $entry['id'] . '.json exists. Skipping download...<br>' ;
		
		if ($json = readJSON($filename))
		{
			
			//var_dump($json);

			if (isset($json['adresser']['0']))
			{
				$indexdata[$key]['location'] = $json;
			}
			else
			{
				$error_counter++;

				//try downloading again
				$adress = $entry['adress'];
				$adress = fixadress($adress);

				if ($json = get_position_kartverket($adress))
				{
					
					echo 'Leste filen med id ' . $entry['id'] . ', men fant ingen treff. Prøver nytt søk for ' . $adress . ': ';
					
					if (!isset($json['adresser']['0']))
					{
						errorecho('Feilet igjen. '); //var_dump($entry);
						//var_dump($json);
					}

					saveJSON($json, $filename);
					$indexdata[$key]['location'] = $json;
					

				}
			
			}
		}
	} 
	else
	{
		//build adress string
		$adress = $entry['adress'];
		$adress = fixadress($adress);

		echo $adress . '<br>';
		//get location data for address
		if ($json = get_position_kartverket($adress))
		{
			saveJSON($json, $filename);
			$indexdata[$key]['location'] = $json;
		}		
	}


}

$ok_counter = count($indexdata) - $error_counter;

successecho('<br>Number of adresses found: ' . $ok_counter  . '<br>');

errorecho('Number of adresses not found: ' . $error_counter . '<br><br>');

saveJSON($indexdata, 'json/maps/' . $date . '.json');

echo 'proximity_adress_downloader.php DONE! <br><br>';

function fixadress($string)
{

	$string = str_replace('&#x27;', "'", $string); // remove '
	$string = str_replace(' &amp; ;', '-', $string); // remove '


	//remove parenteses
	if ($parentes_start = strpos($string, "("))
	{
	
		if ($parentes_end = strpos($string, ")"))
		{
			$before = substr($string, 0, $parentes_start);
			$after = substr($string, $parentes_end+1);
			$string = $before . $after;
		}

	}

	//remove parenteses
	if ($parentes_start = strpos($string, "("))
	{
	
		if ($parentes_end = strpos($string, ")"))
		{
			$before = substr($string, 0, $parentes_start);
			$after = substr($string, $parentes_end+1);
			$string = $before . $after;
		}

	}

	//remove ' - '

	if ($found_pos = strpos($string, ' - '))
	{
		$string = substr($string, 0, $found_pos);	
	}

	//fix adresses like this Pilestredet Park 11-13

	if ($found_pos = strpos($string, '-'))
	{
		//substr(string,start,length)
		//get numbers before 
		$count = 1;

		$character = substr($string, $found_pos-$count,1);
		//echo 'tegn er ' . $character . '<br>';

		while(is_numeric($character))
		{
			//echo $count . ' | ';
			$count++;
			$character = substr($string, $found_pos- $count,1);

		}

		$before_numbers = substr($string, $found_pos-$count,$count);
	
		$count = 1;

		$character = substr($string, $found_pos+$count,1);
		//echo 'tegn er ' . $before_numbers . '<br>';

		while(is_numeric($character))
		{
			//echo $count . ' | ';
			$count++;
			$character = substr($string, $found_pos + $count,1);

		}

		$after_numbers = substr($string, $found_pos + 1,$count);
		//echo 'tegn er ' . $after_numbers . '<br>';

		if (is_numeric($before_numbers) and is_numeric($after_numbers ))
		{
			$string = substr($string, 0, $found_pos);
			//echo 'Ut: ' . $string . '<br>'; 
		}

	}


	//echo ($string) . '<br>';
	$lastspace_position = strrpos($string," ");

	if ($lastspace_position == false)
	{
		return false; 
	}

	//$lastspace_position = strrpos($string," ");

	$string_length = strlen($string);

	//echo 'string length: ' . $string_length . '<br>';
	//echo 'Lastspace position: ' . $lastspace_position . '<br>';

	$sub_string = substr($string, $lastspace_position+1);

	if (strlen($sub_string) == 1)
	{

		if (!preg_match("/^[a-zA-Z]$/", $sub_string)) //if letter
		{
    		return $string;
		}

		//echo 'ut: ' . substr($string, 0,$lastspace_position) . $sub_string . '<br>';
		return substr($string, 0,$lastspace_position) . $sub_string;

	}
	else
	{
		return $string;
	}

	//var_dump($sub_string);

	return $string; 




}

function get_position_google($string)
{

	$middleurl = urlencode($string);

	//https://maps.googleapis.com/maps/api/geocode/json?address=odinsvei+4b+1413,+t%C3%A5rn%C3%A5sen+norge&key=AIzaSyDIDFDKvAJqNw1zx04g3JA1ymxWRfXp0Y8

	$starturl = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
	$endurl = '&key=AIzaSyDIDFDKvAJqNw1zx04g3JA1ymxWRfXp0Y8';

	$url = $starturl . $middleurl . $endurl;


	if (!$data = download($url))
	{
		echo 'Error downloading ' . $url . '<br>';
		return false;
	}

	return $data;

}







?>