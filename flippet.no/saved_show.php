<?php

include 'functions.php';


$date = date('Y-m-d');
$limit_date_previous =  date('Y-m-d', strtotime("-90 days", strtotime($date)));

while (!file_exists('json/saved/'. $date . '.json'))
{
  //echo $date . '<br>';
  $date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

  if ($date < $limit_date_previous)
  {
    echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
    return;
  }
}   

if (!$data = readJSON('json/saved/' .  $date . '.json'))
{
  echo 'Read error of ' . 'json/saved/' .  $date . '.json' . '<br>';
}

//get latest average prices
if (!$average_prices = readJSON('json/areas/average_prices.json'))
{
  echo 'Read error of' . 'json/areas/average_prices.json' . '<br>';
}

//count 
$data_count = count($data);
include 'bit_head.html';

?>
<body>
  <div class="container ">
    <div class="row">
      <div class="col-12">
        <h1>Lagrede boliger</h1>  
        <p><?php echo 'Antall lagrede boliger: ' . $data_count; ?> <br>
        </p>
        <div class="d-flex">
          <div class="mr-2"><a href="proximity_map_saved.php" class="btn btn-secondary" role="button">Se på kartet</a></div>
          <div class="mr-2"><a href="flipped_panel.php" class="btn btn-secondary" role="button">Gå til kontrollpanel</a></div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <?php
  foreach ($data as $key => $entry)
  {

    //find map
    $col_middle = 9;
    $display_map = false; 
    $image_url = 'download/details/' . $entry['id'] . '/images/map.png';

    if (file_exists($image_url))
    {
      $col_middle = 6;
      $display_map = true; 
    }

    if (isset($average_prices[$entry['property_type']][$entry['area']]))
    {
      $area_message = 'Område: ' . $entry['area'];
      $price_message = 'Snittpris: ' . money($average_prices[$entry['property_type']][$entry['area']]) . ' kroner pr. kvadrat';
    }
    else
    {
      $area_message = '';
      $price_message = '';
    }

    ?>

    <div class="container mb-3" style="height: 184px;">
      <div class="row no-gutters bg-bluelight border">
        <div class="col-3 ">
          
          <a href="<?php echo 'finnscraper_details_show.php?id=' . $entry['id']; ?>">
            <img class="lazyload" style="max-height: 184px;" data-src="<?php echo $entry['imgurl']; ?>" width="100%">
          </a>
        </div>
        <div class="col-<?php echo $col_middle; ?> px-3 pt-2">
          <div class="d-flex flex-column text-white">
            <a href="<?php echo 'finnscraper_details_show.php?id=' . $entry['id']; ?>">
              <h4 class="text-dark"><?php echo $entry['title']; ?></h4>
            </a>
          </div>
          <div class="d-flex flex-column">
          <div class="d-flex justify-content-between">
            <div>
             Adresse: <?php echo $entry['adress']; ?>
           </div>
   
           </div>           
            <div>
            <?php echo $area_message . '. ' . $price_message; ?> 
           </div>  
           <div> 
            <a href="<?php echo $entry['link']; ?>">
            Annonsen på Finn.no
          </a>        
           </div>
 
         </div>          
       </div>
       <?php 

       if ($display_map)
       {
         ?>
         <div class="col-3 ">
          <a href="<?php echo 'finnscraper_details_show.php?id=' . $entry['id']; ?>">
            <img class="lazyload" data-src="<?php echo $image_url; ?>" width="100%" height="184px">
          </a>
        </div> 
        <?php
      }
      ?>
    </div>
  </div>
</div>

<?php

}

?>

<script src="js/lazysizes.min.js" ></script>
<script>

  window.lazySizesConfig = window.lazySizesConfig || {};
  lazySizesConfig.loadMode = 1;

</script>


</body>
</html>