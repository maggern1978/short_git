<?php

require_once "simple_html_scraper/simple_html_dom.php";
require_once "functions.php";

set_time_limit(1000);

$date = date('Y-m-d');

while (!file_exists('json/joined/'. $date . '.json'))
{

	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	echo 'Trying ' . 	'json/joined/'. $date . '.json' . '<br>';
	if ($date < '2020-01-01')
	{
		echo 'Json files not found, returning...<br>';
		return;
	}
}

if (!$json = readJSON('json/joined/' . $date . '.json'))
{
	echo 'json/joined/' . $date . '.json is not set<br>';
	return;
}

echo 'Processing file ' . 'json/joined/' . $date. '.json' . '<br>';

$filename = "download/areas/" . $date . "/";

if (!file_exists($filename)) 
{
	mkdir("download/areas/" . $date, 0777);
	echo "The directory download/areas/ $date was successfully created.<br>";

} 

//clean the others
$files = listfiles('download/areas/');

foreach ($files as $file)
{
	if ($file == $date)
	{
		continue;
	}
	else
	{

		$localfiles = listfiles('download/areas/' . $file);

		foreach ($localfiles as $localfile)
		{
			unlink('download/areas/' . $file . '/' . $localfile);
			//echo ('Deleting: download/index/' . $file . '/' . $localfile);
			//echo '<br>';
		}

		rmdir('download/areas/' . $file);
		echo 'Deleting: download/areas/' . $file . '<br>';
	
	}
	
}






$areas_box = [];

foreach ($json as $key => $apartment)
{

	if (!isset($apartment['area']))
	{
		continue; 
	}

	if ($apartment['area'] == 'Oslo') //should not be oslo...
	{
		continue; 
	}

	$areas_box[] = $apartment['area'];
	
}

$areas_box = array_unique($areas_box);
$areas_box = array_values($areas_box);

//bygg opp en rekke id'er som du vil sjekke

$idbox = [];

foreach ($areas_box as $key => $area)
{

	//finn en leilighet med areas
	flush_start();
	echo '<br>------------------------------<br>';
	echo '<strong>'. $key . '. Trying to find ' . $area . ': </strong><br>';

	$success = 0;
	$foundcount = 0;

	foreach ($json as $index => $apartment)
	{
		
		if ($success == 1 and $foundcount > 2) //do a few of each area to make sure that data for area is captured.
		{
			break;
		}		

		if (isset($apartment['property type']) and isset($apartment['area']) and $apartment['area'] == $area and mb_strtolower($apartment['property type']) == 'leilighet')
		{
			//echo '<br>Area found nr: ' . $index . ' '; 
			//check if file already is downloaded
			$filename = 'download/areas/' . $date . '/' . $apartment['id'] . '.html';

			if (file_exists($filename))
			{
				echo $index . '. '. $filename . ' already exists, skipping to next...<br>';
				$foundcount++;
				$success = 1;
				continue;
			}
			else
			{
				//check if download is fine 
				$starturl = 'https://www.finn.no/prisstatistikk/';
				$url = $starturl . $apartment['id'];

				echo $url;

				if ($data = download($url))
				{
					
					if (!stripos($data, 'Beklager, nå er det rusk i maskineriet') and !stripos($data, 'Ingen treff akkurat nå'))
					{
						$filename = 'download/areas/' . $date . '/' . $apartment['id'] . '.html';
						echo 'Saving ' . $filename . ' (' . $area .  ')'. '. ';

						file_put_contents($filename, $data);
						$random = random_int(3, 10);
						echo ' | Sleeping for ' . $random . ' seconds.<br>';
						sleep($random);
						$success = 1;
						$foundcount++;
					}
				}
			}

		}



	}

	if ($success == 0)
	{
		errorecho('Error downloading any files for area ' . $apartment['area'] . '<br>');
	}

	flush_end();
}




//$url = 'https://www.finn.no/pulse/pricepersqm?finnkode=206181878';

//$html = download($url);

//file_put_contents('download/areas/test.html', $html);

//collect areas in data




?>