<!DOCTYPE html>
<html lang="en">
<head>
  <title>Legger til adresse</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid mx-2">
<?php

require_once "functions.php";

if (!empty($_GET["adress"])) 
{
  $adress = ($_GET["adress"]);
  //echo $date;
}
else
{
  exit('Ingen adresse satt');
  //return;
  //$id = 211854314;
}

echo 'Adressen er ' . $adress . '<br>';
echo 'Prøver å finne lokalisjonsdata hos Kartverket<br>';

$lokalisjonsdata = get_position_kartverket($adress);

$success = 0;
if (isset($lokalisjonsdata['adresser'][0]))
{
  successecho('<br>Suksess: Fant følgende: <br>');
  echo $lokalisjonsdata['adresser'][0]['adressetekst'];
  echo '<br>';
  echo 'LAT: ' . $lokalisjonsdata['adresser'][0]['representasjonspunkt']['lat'];
  echo '<br>';
  echo 'LON: ' . $lokalisjonsdata['adresser'][0]['representasjonspunkt']['lon'];
  $success = 1;
}
else
{
  errorecho('Ingen treff.');
}

if ($success == 1)
{

  $obj = new stdClass;
  $obj->adress = $adress;
  $obj->lat = $lokalisjonsdata['adresser'][0]['representasjonspunkt']['lat'];
  $obj->lon = $lokalisjonsdata['adresser'][0]['representasjonspunkt']['lon'];
  $obj->added_date = date('Y-m-d');

  $filename = 'json/point_of_interest/point_of_interest_list.json';
  $current_list = readJSON($filename);

  //skip if alredy in list

  foreach ($current_list as $point)
  {
    if (mb_strtolower($point['adress']) == mb_strtolower($adress))
    {
      errorecho ('<br>Adress is aldready in list, exiting...');
      exit();
    }
  }

  $current_list[] = (array)$obj;
  echo '<br>';
  saveJSON($current_list, 'json/point_of_interest/point_of_interest_list.json');
}


?>

<div class="my-2"><a href="flipped_panel.php" class="btn btn-dark" role="button">Tilbake</a></div>
</div>
</body>
</html>
