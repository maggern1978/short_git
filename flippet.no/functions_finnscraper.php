<?php

function money($string)
{
  return number_format((float)$string, 0, ",", ".");
}

function cuttext($string)
{

  while(!is_numeric($string))
  {

    $string = substr($string,0,strlen($string) - 1);

  }

  return $string;

}

function get_position_kartverket($string)
{

	$middleurl = urlencode($string);

	//https://ws.geonorge.no/adresser/v1/#/default/get_punktsok
	//https://ws.geonorge.no/adresser/v1/punktsok?radius=100&utkoordsys=4258&treffPerSide=10&koordsys=4258&asciiKompatibel=true&lon=10.7522&lat=59.9139&side=0

	//https://ws.geonorge.no/adresser/v1/sok?adressetekst=Dalsbergstien%204a&poststed=oslo&postnummer=0170&treffPerSide=10&side=0&asciiKompatibel=true&utkoordsys=4258
	//https://ws.geonorge.no/adresser/v1/sok?adressetekst=Dalsbergstien%204a&poststed=oslo&treffPerSide=10&side=0&asciiKompatibel=true&utkoordsys=4258


	$starturl = 'https://ws.geonorge.no/adresser/v1/sok?adressetekst=';

	$endurl = '&poststed=oslo&treffPerSide=100&side=0&asciiKompatibel=true&utkoordsys=4258';

	$url = $starturl . $middleurl . $endurl;


	if (!$data = download($url))
	{
		echo 'Error downloading ' . $url . '<br>';
		return false;
	}

	if ($data = json_decode($data, true))
	{
			return $data;
	}
	else
	{
		return false;
	}

}


/**
 * Computes the distance between two coordinates.
 *
 * Implementation based on reverse engineering of
 * <code>google.maps.geometry.spherical.computeDistanceBetween()</code>.
 *
 * @param float $lat1 Latitude from the first point.
 * @param float $lng1 Longitude from the first point.
 * @param float $lat2 Latitude from the second point.
 * @param float $lng2 Longitude from the second point.
 * @param float $radius (optional) Radius in meters.
 *
 * @return float Distance in meters.
 */
function computeDistance($lat1, $lng1, $lat2, $lng2, $radius = 6378137)
{
    static $x = M_PI / 180;
    $lat1 *= $x; $lng1 *= $x;
    $lat2 *= $x; $lng2 *= $x;
    $distance = 2 * asin(sqrt(pow(sin(($lat1 - $lat2) / 2), 2) + cos($lat1) * cos($lat2) * pow(sin(($lng1 - $lng2) / 2), 2)));

    return $distance * $radius;
}


/**
 * Calculates the great-circle distance between two points, with
 * the Vincenty formula.
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $lonDelta = $lonTo - $lonFrom;
  $a = pow(cos($latTo) * sin($lonDelta), 2) +
    pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
  $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

  $angle = atan2(sqrt($a), $b);
  return $angle * $earthRadius;
}


function details($house, $key)
    {

      $image_pos = strrpos($house['imgurl'], '/');
      $house['imgurl'] = 'download/details/' . $house['id'] . '/images/' . substr($house['imgurl'], $image_pos+1);

      ?>
      const location<?php echo $key; ?> = { lat: <?php echo $house['lat']; ?>, lng: <?php echo $house['lng']; ?> };

      // The marker, positioned at Uluru
      const marker<?php echo $key; ?> = new google.maps.Marker
      ({
      position: location<?php echo $key; ?>,
      map: map,
    });

    marker<?php echo $key; ?>.addListener("mouseover", () => {
    infowindow<?php echo $key; ?>.open(map, marker<?php echo $key; ?>);
  });

  // assuming you also want to hide the infowindow when user mouses-out
  marker<?php echo $key; ?>.addListener('mouseout', function() {
  infowindow<?php echo $key; ?>.close();
});

marker<?php echo $key; ?>.addListener('click', function() {
window.location.href = '<?php echo $house['link']; ?>';
});

marker<?php echo $key; ?>.addListener('rightclick', function() {
window.open('<?php echo $house['link']; ?>', '_blank'); 
});


const contentString<?php echo $key; ?> =
'<div style="width: 300px;" id="content">' +
'<center><img data-src="<?php echo $house['imgurl']; ?>" class="lazyload" width="300px" ></center>' +
'<div id="" class="maptitle" style="margin-bottom: 6px; margin-top: 5px;"><?php
echo $house['adress']; 

if (isset($house["area"]))
{
  echo '<span class="maparea float-right">' . $house['area'] . '</span>';
}

?></div>' +
'<div id="bodyContent">' +
"<span class='maptext'>" +
"<b>Pris:</b> <?php echo money($house['price']) . ' kroner (+' . money($house['price_change']) . ')' ; ?><br>" +
"<b>Pris pr. kvadrat: </b> <?php echo money($house['price_per_square_meter']); ?> kroner<br>" +
"<b>Størrelse: </b><?php echo $house['size']; ?> kvadratmeter<br>" +
"</span>" +

"<?php 

if (isset($house['first_spotted']))
{
 echo "<div class='maparea mt-2'>Først sett: " . $house['first_spotted'] . '</div>';
}           

?>" +
"</div>" +
"</div>";

const infowindow<?php echo $key; ?> = new google.maps.InfoWindow({
content: contentString<?php echo $key; ?>,
});

<?php
}


function point_of_interest($house, $key)
{
  $key = $key . '_interest';
        //saved apartments
  ?>

  const location<?php echo $key; ?> = { lat: <?php echo $house['lat']; ?>, lng: <?php echo $house['lon']; ?> };

  // The marker, positioned at Uluru
  const marker<?php echo $key; ?> = new google.maps.Marker
  ({
  position: location<?php echo $key; ?>,
  map: map,
  icon: icon_point,
});

marker<?php echo $key; ?>.addListener("mouseover", () => {
infowindow<?php echo $key; ?>.open(map, marker<?php echo $key; ?>);
});

// assuming you also want to hide the infowindow when user mouses-out
marker<?php echo $key; ?>.addListener('mouseout', function() {
infowindow<?php echo $key; ?>.close();
});

const contentString<?php echo $key; ?> =
'<div style="width: 300px;" id="content">' +
'<div id="" class="maptitle" style="margin-bottom: 6px; margin-top: 5px;">Interessepunkt: <?php
echo $house['adress']; 
?></div>' +
'<div id="bodyContent">' +
"<span class='maptext'>" +
"</span>" +
"<?php 

if (isset($house['added_date']))
{
 echo "<div class='maparea mt-2'>Lagt til: " . $house['added_date'] . '</div>';
}           

?>" +
"</div>" +
"</div>";

const infowindow<?php echo $key; ?> = new google.maps.InfoWindow({
content: contentString<?php echo $key; ?>,
});

<?php 

        //and then the apartmens close to the point of interest 
}

function index_apartment($nearby_house, $key, $average_prices)
{

  //echo 'console.log("Area ' . $nearby_house['area'] . '");';

  //and then the apartmens close to the saved 
  //finn over ller under snittpris
  $runner =  $key . '_index';

  $message = '';
  $difference = 0;

  if (isset($nearby_house["size"]) and isset($nearby_house['totalprice']))
  {
    $nearby_house['price_per_square_meter'] = (float)$nearby_house['totalprice']/(float)$nearby_house['size'];
  }
  else
  {
      if (isset($nearby_house["size"]) and isset($nearby_house['price']))
      {
        $nearby_house['price_per_square_meter'] = (float)$nearby_house['price']/(float)$nearby_house['size'];
      }
      else
      {
        $nearby_house['price_per_square_meter'] = 0;
      }   
  }


  if (isset($nearby_house["area"]))
  {

   if (isset($average_prices[$nearby_house['property type']][$nearby_house['area']]) and isset($nearby_house['price_per_square_meter']))
   {
     $message = ' Snittpris i ' . $nearby_house['area'] . ': ' . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . ' kroner';

     if (isset($nearby_house['price_per_square_meter']))
     {
       $difference = $nearby_house['price_per_square_meter'] - $average_prices[$nearby_house['property type']][$nearby_house['area']];

       if ($difference > 0)
       {
        $prefix = '+';
      }
      else
      {
        $prefix = '';
      }

      if  ($difference < 0)
      {
        $message = "Under snittpris for " . $nearby_house['property type'] . " i området: " . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . " kroner (<span class='text-success font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
      }
      else
      {
       $message =  "Over snittpris for " . $nearby_house['property type'] . " i området: " . money($average_prices[$nearby_house['property type']][$nearby_house['area']]) . " kroner (<span class='text-danger font-weight-bold'>" . $prefix .  money($difference) .  '</span>)' ;
      }

      //if zero
      if ($nearby_house['price_per_square_meter'] == 0)
      {
        $message = 'Ukjent pris pr. kvadrat';
        $difference = 0;
      }

   }
   
 }


if (isset($nearby_house['location']['adresser']['0']['representasjonspunkt']['lat']) and isset($nearby_house['location']['adresser']['0']['representasjonspunkt']['lon'] ))
{
  $nearby_house['lat'] = $nearby_house['location']['adresser']['0']['representasjonspunkt']['lat'] ;
  $nearby_house['lon'] = $nearby_house['location']['adresser']['0']['representasjonspunkt']['lon'] ;
  $index = $key . '_' . $runner;

  //echo 'console.log("location is set");';
  ?>  

  const location<?php echo $index; ?> = { lat: <?php echo $nearby_house['lat']; ?>, lng: <?php echo $nearby_house['lon']; ?> };

  // The marker, positioned at Uluru
  const marker<?php echo $index; ?> = new google.maps.Marker
  ({
  position: location<?php echo $index; ?>,
  icon: <?php
  if ($difference < 0)
  {
    echo 'image_green,';
    echo 'zIndex:3000,';
  }
  else if ($difference > 0)
  {
    echo 'image_red,';
    echo 'zIndex:1000,';
  }
  else
  {
    echo 'image_gray,';
    echo 'zIndex:2000,';
  }
  ?>
  map: map,
});

marker<?php echo $index; ?>.addListener("mouseover", () => {
infowindow<?php echo $index; ?>.open(map, marker<?php echo $index; ?>);
});

// assuming you also want to hide the infowindow when user mouses-out
marker<?php echo $index; ?>.addListener('mouseout', function() {
infowindow<?php echo $index; ?>.close();
});

marker<?php echo $index; ?>.addListener('click', function() {
window.location.href = 'https://www.finn.no/realestate/homes/ad.html?finnkode=' + '<?php echo $nearby_house['id']; ?>';
});

marker<?php echo $index; ?>.addListener('rightclick', function() {
window.open('https://www.finn.no/realestate/homes/ad.html?finnkode=' + '<?php echo $nearby_house['id']; ?>', '_blank'); 
});


const contentString<?php echo $index; ?> =
'<div style="width: 300px;" id="content">' +
'<center><img class="lazyload" width="300px" data-src="<?php echo $nearby_house['imgurl']; ?>"></center>' +
'<div id="" class="maptitle" style="margin-bottom: 6px; margin-top: 5px;"><?php
echo $nearby_house['adress']; 

if (isset($nearby_house["area"]))
{
  echo '<span class="maparea float-right">' . $nearby_house['area'] . '</span>';
}

?></div>' +
'<div id="bodyContent">' +
"<span class='maptext'>" +
"<b>Pris:</b> <?php echo money($nearby_house['price']) . ' kroner' ; ?><br>" +
"<b>Pris pr. kvadrat: </b>" +
"<?php 


echo money($nearby_house['price_per_square_meter']); 
?> kroner<br>" +
"<b>Størrelse: </b><?php echo $nearby_house['size']; ?> kvadratmeter<br>" +
"</span>" +
"<?php
echo  $message;
?>" +                    
"</div>" +
"</div>";

const infowindow<?php echo $index; ?> = new google.maps.InfoWindow({
content: contentString<?php echo $index; ?>,
});

<?php 
}

}
}

function delete($dir) {
  $files = array_diff(scandir($dir), array('.','..'));
  foreach ($files as $file) {
    (is_dir("$dir/$file")) ? delete("$dir/$file") : unlink("$dir/$file");
  }
  return rmdir($dir);
}

?>