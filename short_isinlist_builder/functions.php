<?php 
if (!function_exists('utf8ize')) {
    function safe_json_encode($value, $options = 0, $depth = 512, $utfErrorFlag = false) {
        $encoded = json_encode($value, $options, $depth);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $encoded;
            case JSON_ERROR_DEPTH:
                return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_STATE_MISMATCH:
                return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
            case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_UTF8:
                $clean = utf8ize($value);
                if ($utfErrorFlag) {
                    return 'UTF8 encoding error'; // or trigger_error() or throw new Exception()
                }
                return safe_json_encode($clean, $options, $depth, true);
            default:
                return 'Unknown error'; // or trigger_error() or throw new Exception()

        }
    }
}

if (!function_exists('utf8ize')) {
    function utf8ize($mixed) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = utf8ize($value);
            }
        } else if (is_string ($mixed)) {
            return utf8_encode($mixed);
        }
        return $mixed;
    }
}

if (!function_exists('Memory_Usage')) {
function Memory_Usage($decimals = 2)
{
    $result = 0;

    if (!function_exists('memory_get_usage'))
    {
        $result = memory_get_usage() / 1024;
    }

    else
    {
        if (!function_exists('exec'))
        {
            $output = array();

            if (substr(strtoupper(PHP_OS), 0, 3) == 'WIN')
            {
                exec('tasklist /FI "PID eq ' . getmypid() . '" /FO LIST', $output);

                $result = preg_replace('/[\D]/', '', $output[5]);
            }

            else
            {
                exec('ps -eo%mem,rss,pid | grep ' . getmypid(), $output);

                $output = explode('  ', $output[0]);

                $result = $output[1];
            }
        }
    }

    return number_format(intval($result) / 1024, $decimals, '.', '');
}
}

if (!function_exists('readJSON')) {
    function readJSON($file_location_and_name) {
        $string = file_get_contents($file_location_and_name);
        return $data = json_decode($string, true);
    }
}

if (!function_exists('download_json')) {

    function download_json($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLVERSION,1);
    $data = curl_exec($ch);
    $error = curl_error($ch); 
    //var_dump($data);
    if ($error != '') {
        var_dump($error);
        throw new Exception("download error" . $url);
    }
    curl_close ($ch);
    sleep(0.61);
    return $data;
    }
}

if (!function_exists('download')) {

    function download($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLVERSION,1);
    $data = curl_exec($ch);
    $error = curl_error($ch); 

    if ($error != '') {
        var_dump($error);
        throw new Exception("download error" . $url);
    }
    curl_close ($ch);
    return $data;
    }
}

 //obs, dataene må ha semikolon som skilletegn
if (!function_exists('readCSV')) {
     function readCSV($file_location_and_name) {

        $csvFile = file($file_location_and_name);

        //Les in dataene 
        $name = [];
        foreach ($csvFile as $line) {
            $name[] = str_getcsv($line, ';');
        }

        //ordne utf koding
        $counter = 0;
        foreach ($name as &$entries) {
            $data[$counter] = array_map("utf8_encode", $entries);
            $counter++;
        }
        return $name;
    }
}

 //obs, dataene må ha semikolon som skilletegn
if (!function_exists('readCSVkomma')) {
     function readCSVkomma($file_location_and_name) {

        $csvFile = file($file_location_and_name);

        //Les in dataene 
        $name = [];
        foreach ($csvFile as $line) {
            $name[] = str_getcsv($line, ',');
        }

        //ordne utf koding
        $counter = 0;
        foreach ($name as &$entries) {
            $data[$counter] = array_map("utf8_encode", $entries);
            $counter++;
        }
        return $name;
    }
}

if (!function_exists('saveCSVx')) {
    function saveCSVx($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');

    foreach ($input as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
    }
}

if (!function_exists('saveCSVsemikolon')) {
    function saveCSVsemikolon($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');

    foreach ($input as $fields) {
        fputcsv($fp, $fields, ';');
    }

    fclose($fp);
    }
}

if (!function_exists('saveJSON')) {
    function saveJSON($data, $filename) {
        $fp = fopen($filename, 'w');
        fwrite($fp, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        fclose($fp);
    }
}

if (!function_exists('getcolor')) {
    function getcolor($inputvalue) {

        if ($inputvalue == NULL or '') {
            return '';
        }

        if ($inputvalue > 0 ) {
            return 'exchange-header-up';
        }
        elseif ($inputvalue  == 0) {
            return 'exchange-header-neutral';   
        }
        elseif ($inputvalue  < 0) {
            return 'exchange-header-down';          
        }
    }
}

if (!function_exists('date_range')) {

  function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

      $dates = array();
      $current = strtotime($first);
      $last = strtotime($last);

      while( $current <= $last ) {
          //only work days
          if(date("w",$current)!=6 and date("w",$current)!=0) {
            $dates[] = date($output_format, $current);
          }
          $current = strtotime($step, $current);
      }
      return $dates;
  }
}

 //obs, dataene må ha semikolon som skilletegn
if (!function_exists('readCSV_graphdata')) {
   function readCSV_graphdata($file_location_and_name) {

      $csvFile = file($file_location_and_name);

      //Les in dataene 
      $name = [];
      foreach ($csvFile as $line) {
          $name[] = str_getcsv($line, ';');
      }

      //ordne utf koding
      $counter = 0;
      foreach ($name as &$entries) {
        $data[$counter] = array_map("utf8_encode", $entries);
        $counter++;
      }
      return $name;
  }
}


if (!function_exists('isWeekend')) {
  //Weekend function
  function isWeekend($date) {
      return (date('N', strtotime($date)) >= 6);
  } 
} 

if (!function_exists('download_candle_from_finnhub')) {
    function download_candle_from_finnhub($ticker, $starttime, $endtime)
    {
        
        $starturl = 'https://finnhub.io/api/v1/stock/candle?symbol=';
        $starturl .= strtoupper($ticker);
        $starturl .='&resolution=D&from=';
        $starturl .= $starttime;
        $starturl .= '&to=';
        $starturl .= $endtime;
        $endurl = '&token=bp8pnvnrh5racm7obvp0';
        $fullurl = $starturl . $endurl;

        echo $fullurl . '';

        //will try two times
        $NUM_OF_ATTEMPTS = 1;
        $attempts = 0;
        $data = '';

        sleep(1);

        do {    
            try
            {
                $data = json_decode(download_json($fullurl), true);

            } catch (Exception $e) {
                echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
                $attempts++;
                sleep(1);
                continue;
            }
            break;
        } while($attempts < $NUM_OF_ATTEMPTS);

        //test data
        if (!isset($data['c'][0]))
        {
           return false;
        }

        $timecount = count($data['t']);
        for ($i = 0; $i < $timecount; $i++) 
        {
            $data['t'][$i] = date("Y-m-d", $data['t'][$i]);
        }
      
        return $data;
    }
}


if (!function_exists('download_ticker_from_finnhub')) {
    function download_ticker_from_finnhub($ticker)
    {
        
        $starturl = 'https://finnhub.io/api/v1/quote?symbol=';
        $undurl = '&token=bp8pnvnrh5racm7obvp0';
        $fullurl = $starturl . strtoupper($ticker) . $undurl;

        //will try two times
        $NUM_OF_ATTEMPTS = 1;
        $attempts = 0;
        $data = '';

        //echo $fullurl . '<br>';

        do {    
            try
            {
                $data = json_decode(download_json($fullurl), true);

            } catch (Exception $e) {
                echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
                $attempts++;
                sleep(2);
                continue;
            }
            break;
        } while($attempts < $NUM_OF_ATTEMPTS);

        if (isset($data['t']))
        {
            $data['t'] = date("Y-m-d", $data['t']);
        }

        //test data
        if (!isset($data['c']))
        {
           return false;
        }

        sleep(0.5);
        return $data;
    }
}

if (!function_exists('ticker_download_finnhub_and_yahoo')) {
    function ticker_download_finnhub_and_yahoo($ticker)
    {
        
        $starturl = 'https://finnhub.io/api/v1/quote?symbol=';
        $undurl = '&token=bp8pnvnrh5racm7obvp0';
        $fullurl = $starturl . strtoupper($ticker) . $undurl;

        //will try two times
        $NUM_OF_ATTEMPTS = 2;
        $attempts = 0;
        $data = '';

        //echo $fullurl . '<br>';

        do {    
            try
            {
                $data = json_decode(download_json($fullurl), true);

            } catch (Exception $e) {
                echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
                $attempts++;
                sleep(2);
                continue;
            }
            break;
        } while($attempts < $NUM_OF_ATTEMPTS);

        if (isset($data['t']))
        {
            $data['t'] = date("Y-m-d", $data['t']);
            sleep(2);
        }


        //alternative download!
        if (!isset($data['c']) or $data['c'] == 0)
        {
            echo '<br>Price not set in Finnhub data, using alternative download. <br>';

            $data = download_yahoo_ticker($ticker);

            $temp_array = [];
            $temp_array['c'] = $data['05. price'];
            //$temp_array['o'] = $data['02. open'];
            $temp_array['pc'] = $data['08. previous close'];
            $temp_array['t'] = $data['07. latest trading day'];
            $temp_array['timestamp'] = $data['timestamp'];
            $data = $temp_array;
            sleep(1);
           
        }
       
        return $data;
    }
}



if (!function_exists('download_yahoo_ticker')) {
    function download_yahoo_ticker($ticker)
    {
    
        $url = 'https://query1.finance.yahoo.com/v7/finance/quote?symbols=' . $ticker;
        
        echo '<br>Using yahoo download' . $url . '<br>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION,1);
        $data = curl_exec ($ch);
        $error = curl_error($ch); 
        if (curl_error($ch)) {
            $error_msg = curl_error($ch);
            echo $error_msg;
        }
        curl_close ($ch);
    
        $data = (array)json_decode($data);
        if (!$data = (array)($data['quoteResponse']))
        {
            echo 'Error downloading yahoo ticker or response has not the right data<br>';
            //return false;
        }
        //echo '<br><br>';
        //var_dump($data);
        //echo '<br><br>';

        if (!isset($data['result']['0']->regularMarketPrice))
        {
            echo 'Market price is not set!';
            return false;
        }

        $latestClose = $data['result']['0']->regularMarketPrice;  
        $previousClose = $data['result']['0']->regularMarketPreviousClose;
        $timestamp = $data['result']['0']->regularMarketTime + ($data['result']['0']->gmtOffSetMilliseconds/1000);
        
        $timestamp = (new DateTime("@$timestamp"))->format('Y-m-d H:i:s');
        $timestamp = date('H:i:s', strtotime($timestamp));

        $change = $latestClose - $previousClose;
        $percent = (1-($previousClose/$latestClose))*100;
    
        $percent = round($percent ,4);
        $percent = (string)$percent . '%';
    
        $day = date("d") ;
        $month = date("m") ;
        $year = date("Y") ;
    
        //Build string
        $latestTradingDay = $year . '-' . $month . '-' . $day;
    
        $dataNew = 
                   array(
                    "08. previous close" => $previousClose,
                    "07. latest trading day" => $latestTradingDay,
                    "05. price" => round($latestClose,4),
                    "10. change percent" => $percent,
                    "09. change" => round($change,4),
                    "timestamp" => $timestamp
                    );
        
        return $dataNew;
    
    }
}
if (!function_exists('errorecho')) {
    function errorecho($msg) {
        echo '<b>';
        echo '<span style="color: red; font-size: 120%;">';
        echo $msg;
        echo '</span></b>';
    }
}

if (!function_exists('successecho')) {
    function successecho($msg) {
        echo '<b>';
        echo '<span style="color: green; font-size: 120%;">';
        echo $msg;
        echo '</span></b>';
    }
}

if (!function_exists('getfiles')) {
    function getfiles($path) {
        $files = array_diff(scandir($path), array('.', '..'));
        return $files;
    }
}

if (!function_exists('ticker_download_eodhistorical')) {
    function ticker_download_eodhistorical($ticker)
    {
        $starturl = 'https://eodhistoricaldata.com/api/real-time/';
        $undurl = '?api_token=5da59038dd4f81.70264028&fmt=json';

            //find out if one or several tickers are requested

        if (is_array($ticker))
        {

            $ticker = array_unique($ticker);
            $ticker = array_values($ticker);

                //https://eodhistoricaldata.com/api/real-time/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json&s=VTI,EUR.FOREX
            $count = count($ticker);

            $maxNumbersDownload = 10;

            $downloadrounds = ceil($count/$maxNumbersDownload);
            echo '<br>';
            echo 'Multi download of ' . $count  . ' tickers.<br>'; 
            echo 'Will download ' . $maxNumbersDownload . ' tickers at the time, in ' . $downloadrounds . ' round(s).<br>';

            $x = 0;

            $urlarray = [];

            while($downloadrounds > $x)     
            {

                $tickernumberstart = $maxNumbersDownload * $x;
                $tickernumberend = ($maxNumbersDownload * $x) + $maxNumbersDownload;


                if ($tickernumberend > $count)
                {
                    $tickernumberend = $count;
                }

                //echo 'Start: ' . $tickernumberstart . '. End: ' . $tickernumberend . ' (minus 1)<br>';

                    //building the url
                for ($z = $tickernumberstart; $z < $tickernumberend; $z++)
                {

                    if (!isset($ticker[$z]))
                    {
                        //echo 'Z is ' . $z . '<br>';
                        continue;
                    }
                    else
                    {
                        //echo 'Z is ' . $z . '<br>';
                    }

                    if ($z == $tickernumberstart)
                    {
                        $url = 'https://eodhistoricaldata.com/api/real-time/' . $ticker[$tickernumberstart] . '?api_token=5da59038dd4f81.70264028&fmt=json&s=';
                    }
                    else if ($z == $tickernumberend - 1)
                    {
                        $url .= $ticker[$z];
                    }
                    else
                    {
                        $url .= $ticker[$z] . ',';
                    }

                }

                $urlarray[] = $url;
                    //echo $url . '<br>';

                $x++;
            }

            $dataholdertemp = [];

            foreach ($urlarray as $key => $url)
            {

                echo $url . '<br>';

                if ($datatickers = (array)json_decode(download($url)))
                {

                  if (isset($datatickers[0]))
                  {

                      foreach ($datatickers as $tickercompany)
                      {

                        $data = (array)$tickercompany;

                        if ($data = ticker_download_eodhistorical_tester($data))
                        {
                            $dataholdertemp[] = $data;   
                        }
                    }
                }
                else
                {

                    $data = $datatickers;

                    if ($data = ticker_download_eodhistorical_tester($data))
                    {
                        $dataholdertemp[] = $data;   
                    }

                }   

                }  
            }

            return $dataholdertemp;

        }

        else 
        {
                //single quoute 
            $url = $starturl . $ticker . $undurl;

            if ($data = (array)json_decode(download($url)))
            {

                $data = ticker_download_eodhistorical_tester($data);
                return $data;

            }
            else
            {
                errorecho('Error downloading from eodhistorical<br>');
                return false;
            }
        }

    }
}

if (!function_exists('ticker_download_eodhistorical_tester')) {
    function ticker_download_eodhistorical_tester($datainput)
    {   

        if ('NA' === $datainput['timestamp'] || 'NA' === $datainput['volume'] || 'NA' === $datainput['close'] || 'NA' === $datainput['previousClose'])
        {
            errorecho('error! in ticker_download_eodhistorical_tester. Ticker: ' . $datainput['code'] . '<br>');
            var_dump($datainput);
            
            $ticker = $datainput['code'];

            echo 'Trying alternative download of ticker ' . $ticker . '<br>';

            if ($data = ticker_download_finnhub_and_yahoo($ticker))
            {

              var_dump($data);

              $temp_array['code'] = $ticker;
              $temp_array['timestamp'] = $data['timestamp'];
              $temp_array['open'] = $data['o'];
              $temp_array['high'] = $data['h'];
              $temp_array['low'] = $data['l'];
              $temp_array['close'] = $data['c'];
              $temp_array['volume'] = false;
              $temp_array['previousClose'] = $data['pc'];
              $temp_array['change'] = $data['c'] - $data['pc'];

              if ($data['pc'] != 0)
              {
                $temp_array['change_p'] = ($temp_array['change']/$data['pc'])*100;
              }
              else
              {
                $temp_array['change_p'] = 0;
              }
              
              $temp_array['date'] = date("Y-m-d", strtotime($data['timestamp']));
              $temp_array['time'] = date("H:i:s", strtotime($data['timestamp']));

              return($temp_array);
            }
        else
        {
            errorecho('Alternative download failed, returning<br>');
            return false;
        }
           
      
        }
        else
        {
         
            //format to correct timezone
            $datainput['timestamp'] = date("Y-m-d H:i:s", $datainput['timestamp']);

            // create a $dt object with the UTC timezone
            $dt = new DateTime($datainput['timestamp'], new DateTimeZone('UTC'));

            // change the timezone of the object without changing it's time
            $dt->setTimezone(new DateTimeZone('Europe/Oslo'));

            // format the datetime
            $dt->format('Y-m-d H:i:s');

            $datainput['timestamp'] = (string)$dt->format('Y-m-d H:i:s');

            $datainput['date'] = (string)$dt->format('Y-m-d');

            $datainput['time'] = (string)$dt->format('H:i:s');

            return $datainput;
        }
    }
}


?>