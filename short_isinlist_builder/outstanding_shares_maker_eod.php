<?php 

include 'functions.php';

set_time_limit(6000);

echo '<br>';

//read path
$path = 'fundamental_download/';
echo 'Taking files from ' . $path . '<br>';
$files = array_diff(scandir($path), array('.', '..'));
echo 'Number of files: ' . count($files) . '<br>';

$csvContainer = [];
$savepath = '';

$listcontainter = [];
$errorcounter = 0;

foreach ($files as $key => $file)
{
    include 'flush_start.php';
    
    if ($data = readJSON($path . $file))
    {
        //var_dump($data);
    }
    else
    {
        echo $key . '. ';
        echo $file . '. ';
        errorecho('Read error for ' . $file . '<br>');
        $errorcounter++;
        continue;
    }

    if (!isset($data['SharesStats']))
    {
        echo $key . '. ';
        echo $file . '. ';
        echo 'SharesStats not set, continuing<br>';
        $errorcounter++;
        continue; 
    }

    else if ($data['SharesStats']['SharesOutstanding'] == null)
    {
        echo $key . '. ';
        echo $file . '. ';
        echo 'Shares are null, continuing<br>';
        $errorcounter++;
        continue; 
    }
    else if ($data['SharesStats']['SharesOutstanding'] == null)
    {
        echo $key . '. ';
        echo $file . '. ';
        echo 'SharesStats are null, continuing<br>';
        $errorcounter++;
        continue; 
    }


    $ticker = $data['General']['Code'] . '.' . $data['General']['Exchange'];

    $filename = str_replace('.json','', $file);


    //Ticker,"Outstanding shares",isin, name
    $array = [$filename, (string)$data['SharesStats']['SharesOutstanding'], $data['General']['ISIN'], $data['General']['Name']];
    $listcontainer[] = $array;

    include 'flush_end.php';
}

echo '<br>Errorcounter is ' . $errorcounter++ . '<br>';

$savefilename = 'outstanding_shares_current.csv';

echo 'Saving ' . $savefilename . '<br>';
saveCSVsemikolon($listcontainer, $savefilename);



?>

