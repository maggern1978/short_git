
<?php 

//This is based on the main isin list in dataCalc/isin/
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include 'functions.php';
set_time_limit(4000);
	
$filename = 'main_isin_list.csv';

$list = readCSV($filename);

$urlstart = 'https://eodhistoricaldata.com/api/eod/';

$urlstart = 'https://eodhistoricaldata.com/api/fundamentals/';
$urlend = '?api_token=5da59038dd4f81.70264028';

//https://eodhistoricaldata.com/api/shorts/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&from=2000-01-01
//$urlstart = 'https://eodhistoricaldata.com/api/shorts/';
//$urlend = $urlend . '&from=2000-01-01';
	
foreach ($list as $key => $row)
{
	if ($key == 0)
	{
		continue;
	}
	
	include 'flush_start.php';
	$ticker = $row[1] . '.' . $row[8];
	$url = $urlstart . $ticker . $urlend;
	$ticker = strtolower($ticker);

	if (file_exists('fundamental_download/'. $ticker . '.json'))
	{
		continue; 
	}

	echo $key . '. Downloading ' . $url . '<br>';
	
	if ($data = json_decode(download($url)))
	{
		saveJSON($data, 'fundamental_download/'. $ticker . '.json');
	}
	include 'flush_end.php';

	
}
	
	
	
?>