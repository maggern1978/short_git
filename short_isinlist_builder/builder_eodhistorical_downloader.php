<?php
set_time_limit(8000);
include 'functions.php';
echo '<br>';

//read input files

$files = getfiles('buildereodhistorical/inputfiles/');

foreach ($files as $file) {

	echo '<h1>Downloading from input file ' . $file . '</h1><br>';

	$csv = readCSVkomma('buildereodhistorical/inputfiles/' . $file);
	echo '<br>';
	//var_dump($csv[1]);


	//https://eodhistoricaldata.com/api/search/AAPL?api_token=YOUR_API_TOKEN

	$token = '5da59038dd4f81.70264028';
	$starturl = 'https://eodhistoricaldata.com/api/search/';
	$endurl = '?api_token=' . $token;

	$isinContainer = [];

	foreach ($csv as $key => $row) 
	{
		if ($key == 0) 
		{
			continue;
		}

		$temparray = [$row[2],$row[1]];
		$isinContainer[] = $temparray;
	}

	//works as array_uniqe
	$isinContainer = array_map("unserialize", array_unique(array_map("serialize", $isinContainer)));

	//var_dump($isinContainer);

	$errorcounter = 0;
	$errorcontainer = [];

	foreach ($isinContainer as $key => $row)
	{
		if ($key == 0)
		{
			continue;
		}

		if ($key > 200)
		{
			//exit();
		}

		$doAlternate = 0;

		if ($row[0] != '') { //spain does only have names!

			if (file_exists('buildereodhistorical/json/' . $row[0] . '.json'))
			{
				//include 'flush_start.php';
				//echo $key . '. Already exist, skipping.<br>';
				//include 'flush_end.php';
				continue; 
			}

			$url = $starturl . $row[0] . $endurl;
			include 'flush_start.php';
			echo $key . '. Trying ' . $url . ''; 
			include 'flush_end.php';

			if ($data = json_decode(download($url)))
			{

				successecho(' Success!');
				
				//save
				saveJSON($data, 'buildereodhistorical/json/' . $row[0] . '.json');
				echo '<br><br>';
				continue;
			}
			else 
			{
				errorecho(' Isin fails! ');
				$doAlternate = 1;
			}

		}
		else
		{
			include 'flush_start.php';
			echo $key . '. Isin missing, doing name only';
			include 'flush_end.php';
		}


		echo ' | ';
		$url = $starturl . $row[1] . $endurl;
		
		if ($data = json_decode(download($url)))
		{
			successecho(' Name success!<br>');
			
			//var_dump($data);
			//var_dump($row);


			//fill inn isin where missing

			echo '<br><strong>Missing isin check: </strong><br> ';

			foreach ($data as $key => $entry)
			{
				
				if ($data[$key]->ISIN == null) //if not set
				{
					
					echo $key . '. ';

					$targetname = $row[1];

					if ($entry->Name == $targetname) //name mataches 100%, then set isin
					{
						echo ' found exact name hit <br>';
						$data[$key]->ISIN = $row[0];
					}
					else //find similarity
					{
						echo ' doing name compare:  ';
						//echo $targetname . ' vs ' . $entry->Name . '. <br>';

						//remove , Inc
						//echo ' Removing text to match better ';
						$removearray = [' Ltd', ' Plc', 'INC.', ', Inc', ' Inc.', ' Inc', ' Corporation', ' Corp', ' Ord' ,'.', ','];

						$name =  $entry->Name;

						foreach ($removearray as $word)
						{
							$name = str_ireplace($word, '', $name);
							$targetname = str_ireplace($word, '', $targetname);
						}

						echo $targetname . ' vs ' . $name . ' | ';

						$similar = similar_text(strtolower($name), strtolower($targetname), $percent);
						echo 'Name similarity: ' . $percent . '%. Characters: ' . $similar . '<br>';

						if ($percent > 95)
						{
							echo ' Over 95% ';

							if ($row[0] == '')
							{
								if ($data[$key]->ISIN == null)
								{
									//no isin found

									//cheack if any other has isin
									$found = 0;

									foreach ($data as $teller => $enter)
									{
										if ($enter->ISIN != null)
										{
											//echo ' . Found one isin in array, will continue...<br>';
											$found = 1;
											break;
										}
									}

									if ($found == 0)
									{
									$ticker = $entry->Code . '.' . $entry->Exchange;
									errorecho('No isin available. Alternative save as ticker ' . $ticker . '<br>');
									saveJSON($entry, 'buildereodhistorical/jsontickers/' . $ticker . '.json');
									}
								}
								
							}
							else
							{
								$data[$key]->ISIN = $row[0];
								echo ' setting ISIN to ' . $row[0] . ' <br><br>';
							}

						}

					}

				}
			
			}

			//put in first isin to have a filename to save
			$altisin = $key;

			foreach ($data as $key => $entry)
			{
				if ($data[$key]->ISIN != null)
				{
					echo 'Setting isin file name to ' . $data[$key]->ISIN . '<br>';
					$altisin = $data[$key]->ISIN;
					break;
				}
			
			}

			//save
			if ($row[0] == '')
			{
				saveJSON($data, 'buildereodhistorical/json/' . $altisin . '.json');
			}
			else
			{
				saveJSON($data, 'buildereodhistorical/json/' . $row[0] . '.json');
			}

			
			continue;
			
		}
		else 
		{
			errorecho('Name fails! ');
		}

		if ($doAlternate == 1) //only when isin is present
		{
			echo ' Trying finnhub. ';

			$altstarturl = 'https://finnhub.io/api/v1/stock/profile2?isin=';
			$altendurl = '&token=bp8pnvnrh5racm7obvp0';
			$alturl = $altstarturl . $row[0] . $altendurl;
			echo ' Url is ' . $alturl . '. ';

			//var_dump(download($alturl));

			if ($json = json_decode(download_json($alturl), true))
			{
				$build = new stdClass;
				
				if ($pos = strpos($json['ticker'],'.', 0))
				{
					$code = substr($json['ticker'], 0, $pos);
				}
				else
				{
					$code = $json['ticker'];
				}

				if ($json['country'] == 'US')
				{
					$build->Exchange = 'US';
				}
				else
				{
					$build->Exchange = $json['exchange'];
				}

				$build->Code = $code;
				
				$build->Name = $json['name'];
				$build->Type = '';
				$build->Country = '';
				$build->Countrycode = $json['country'];
				$build->Currency = $json['currency'];
				$build->ISIN = $row[0];
				$build->Ticker = $json['ticker'];
				$build->Source = 'finnhub';

				$array = (array)$build;
				saveJSON($array, 'buildereodhistorical/json/' . $row[0] . '.json');
				successecho(' Alt ok, saving file<br>');
				//var_dump($array);
				continue;

				//var_dump($build);
			}
			else
			{
				errorecho(' Finnhub fails<br>');
				$errorcontainer[] = $row;
				$errorcounter++;
				continue;
			}

		}
		else
		{
			$errorcontainer[] = $row;
			$errorcounter++;
		}

		echo '<br>';
	}
	saveCSVx($errorcontainer, 'buildereodhistorical/misses.' . $file);
	echo 'Misses er ' . $errorcounter .' of ' . count($csv). ' lines<br>';
	echo '-----------------------------------------------------------------<br><br>';

}

echo '<h1>Finished, end of file!</h1>';

?>