<?php

include '../short_isinlist_builder/functions.php';

//make a backup
$targetname_addon = date('Y-m-d.H-i-s');
copy("../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv", '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder' . $targetname_addon  . '.csv');

$newlist = readCSV('../short_isinlist_builder/main_isin_list.csv');
$oldlist = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv');
$addcounter = 0;

foreach ($newlist as $key => $newrow)
{

	$success = 0;

	foreach ($oldlist as $index => $oldrow)
	{
		if ($oldrow[0] == $newrow[0])
		{
			echo $key . '. Already in main list<br>';
			$success = 1;
			break;
		}


	}

	//not found, go on and add to list
	if ($success == 0)
	{
		$addcounter++;
		$oldlist[] = $newrow;

	}

}


echo 'A total of ' . $addcounter . ' files was added<br>';

saveCSVsemikolon($oldlist, '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv');


?>