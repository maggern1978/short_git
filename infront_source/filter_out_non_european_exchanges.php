<?php

include '../production_europe/functions.php';
set_time_limit(4000);

$isinlist = readCSV('out/hovedlisten_landkoder_outstanding_temp.csv');

$stock_exchanges_box = [];


//kollekt stock exchanges
foreach ($isinlist as $row)
{
	$stock_exchanges_box[] = $row[7];
}

$stock_exchanges_box = array_unique($stock_exchanges_box);
$stock_exchanges_box = array_values($stock_exchanges_box);
//var_dump($stock_exchanges_box);


$duplicates_box = [];

$countries = get_countries();
//$country_codes = ['DK', 'NO', 'FR', 'DE', 'ES', 'SE', 'FI', '' ];

//var_dump($countries);

$countries[7] = 'uk';

//filter out countries that are not within the countries
$delete_counter = 0; 

foreach ($isinlist as $key => $row)
{
	break; //skip this part
	
	if (!in_array(mb_strtolower($row[6]), $countries))
	{
		echo '|';
		$delete_counter++;
		unset($isinlist[$key]);
	}
}

$isinlist = array_values($isinlist);

echo '<br>Removed ' . $delete_counter . ' entries for not being in the right countries<br>';

//check for duplicates
foreach ($isinlist as $key => $row)
{

	$isintarget = $row[0];
	$foundcounter = 0; 

	foreach ($isinlist as $index => $subrow)
	{

		if ($isintarget == $subrow[0])
		{

			if ($index != $key)
			{
				$foundcounter++;
				echo $key . '. Found duplicate of ' . $isintarget . '<br>';
			}
		}
	}
}


foreach ($isinlist as $key => $row)
{
	if ($row[6] == 'UK')
	{
		$isinlist[$key][6] = 'united_kingdom';
	}

}

//lagre sorted array etter isin
usort($isinlist, function ($item1, $item2) {
    return $item2[0] < $item1[0];
});

saveCSVsemikolon($isinlist, '../out/hovedlisten_landkoder_isinsortert.csv');

//lagre sorted array etter navn
usort($isinlist, function ($item1, $item2) {
    return mb_strtolower($item2[2]) < mb_strtolower($item1[2]);
});

saveCSVsemikolon($isinlist, '../out/hovedlisten_landkoder.csv');

?>