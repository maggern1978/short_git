<?php

set_time_limit(7200);

include_once '../production_europe/functions.php';


$files = readCSVkomma('out/infront_isin_list.csv');

$searchbox = [];

foreach ($files as $key => $data)
{
	

	if (isset($data[1]))
	{
		$companyname = $data[1];
		$companyname = namecleaner($companyname);
		//var_dump($companyname);
	}
	else 
	{
		$companyname = false;
	}

	if (isset($data[0]) and is_isin_new($data[0]))
	{
		$isin = $data[0];
		//var_dump($isin);
	}
	else 
	{
		$isin = false;
	}
	
	$object = new stdClass;
	$object->isin = $isin;
	$object->companyname = $companyname;
	$object->ticker = $data[2];
	$searchbox[] = (array)$object;

}

$resultbox = [];
$searchresult = [];
$missbox = [];

foreach ($searchbox as $key => $entry)
{
	

	if ($entry['isin'])
	{

		$fileurl = 'download/isin/' . $entry['isin'] . '.json';

		if (file_exists($fileurl))
		{
			continue;
		}
		//echo $key . '. ';
		//echo 'Searching for ' . $entry['isin'] . '. ';
		
		$isin_search_result = searchname($entry['isin']);
		
		saveJSON($isin_search_result, $fileurl);

		if ($isin_search_result != '')
		{

			foreach ($isin_search_result as $result)
			{
				$searchresult[] = $result;
			}

		}
		else
		{
			echo $key . '. ';
			errorecho('No results for ' . $entry['isin'] . ' , doing name search! ');

			echo 'Searching for ' . $entry['companyname'] . '. ';
			$name_search_result = searchname($entry['companyname']);

			saveJSON($name_search_result, 'download/name/' . $entry['companyname'] . '.json');

			if ($name_search_result != '')
			{
				foreach ($name_search_result as $result)
				{
					$searchresult[] = $result;
				}
			}
			else
			{
				errorecho('No results when searching for name! <br>');
				$missbox[] = $entry;
			}

		}		
	}
}

saveJSON($missbox , '../infront_source/out/missbox.json');

exit();

$searchresult_finnhub = [];

var_dump($missbox);

foreach ($missbox as $key => $entry)
{
	$result = search_finnhub($entry);
	$searchresult_finnhub[] = $result;
	var_dump($entry);
	var_dump($result);
}





function search_finnhub($string)
{

	//https://finnhub.io/api/v1/search?q=US5949181045&token=bp8pnvnrh5racm7obvp0

	$starturl = 'https://finnhub.io/api/v1/search?q=' . $string;
	$endurl = '&token=bp8pnvnrh5racm7obvp0';

	$url = $starturl . $endurl;

	$download = download($url);

	if ($data = json_decode($download, true))
	{
		return $data;
	}
	else
	{
		//var_dump($download);
		return false; 
	}	

}



function searchname($string)
{

	$token = '5da59038dd4f81.70264028';
	$starturl = 'https://eodhistoricaldata.com/api/search/';
	$endurl = '?api_token=' . $token;

	$url = $starturl . $string . $endurl;

	$download = download($url);

	if ($data = json_decode($download, true))
	{
		return $data;
	}
	else
	{
		//var_dump($download);
		return false; 
	}

}

function getfundamentals($ticker)
{

	$token = '5da59038dd4f81.70264028';
	$starturl = 'https://eodhistoricaldata.com/api/fundamentals/';
	$endurl = '?api_token=' . $token;
	$url = $starturl . $ticker. $endurl;

	if ($data = json_decode(download($url), true))
	{
		return $data;
	}

	else
	{
		return false; 
	}
}

function namecleaner($string)
{

	$array = [', L.P.', ' LP', ', S.A.', ' LLP', ' LP'];

	//$string = strtolower($string);

	foreach ($array as $text)
	{
		$string = str_ireplace($text, '', $string);
	}

	return trim($string);
}



?>