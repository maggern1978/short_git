				    <div class="container">

				    	<?php

				    	$selskapNew  = ucwords($singleCompany);
				    	$selskapNew = strtolower($selskapNew);
				    	$singleCompany   = nametolink($selskapNew);
				    	$singleCompany = strtoupper($singleCompany);
				    	$historyLink = 'history_company.php?player=x&selskapsnavn=' . $singleCompany  . '&land=' . $land;


					    //fix ticker 
				    	$ticker = $tickerholder = $posisjon['ticker'];
				    	$position = strpos($tickerholder,'.',0);
				    	$tickerholder = substr($tickerholder,0,$position);
				    	$tickertradingview = $tickerholder;
				    	$companynametradingview = ucwords($selskapNew);
				    	$tickerholder = str_replace("-", " ", $tickerholder);


				    	echo '<div class="d-flex justify-content-between">';
				    	echo '	<div>';
				    	echo '<h2>';
				    	echo '<a class="text-dark"  data-toggle="tooltip" title="' . "See company's full history" . '" href="' . $historyLink  . '">';
				    	echo ucwords($selskapNew) . ' <span class="detaljer-selskap-change text-muted">(' . $ticker . ')</span>';

				    	echo '</a>';
				    	echo '</div>';

				    	echo '<div class="pl-3 pt-1 ml-auto text-dark">';
				    	echo '';
				    	echo '<span class="detaljer-selskap-price">';
				    	echo ' ';  


				    	echo number_format($posisjon['stockPrice'],2) . ' ';
				    	echo $posisjon['currency'];
				    	echo '</span>';
				    	echo '<span class="pl-2 detaljer-selskap-change ';

				    	echo getcolor($posisjon['stockPriceChange']);

				    	echo '">';
				    	if ($posisjon['stockPriceChange'] > 0) {
				    		echo '+';
				    	}
				    	echo $posisjon['stockPriceChange'] . ' (';

				    	if ($posisjon['stockPriceChange'] > 0) {
				    		echo '+';
				    	}

				    	echo number_format(round($posisjon['valueChangePercent'],2),2) . ' %)';
				    	echo '</span>';
				    	echo '</div>';
				    	echo '</div>';

				    	?>

				    </div>
				    <script>
				    	var endprice = <?php echo $posisjon['stockPrice']; ?>;
				    </script>
				    <div class="container">
				    	<div class="row">
				    		<div class="col-12">
				    			<table class="table table-bordered table-sm">
				    				<thead>
				    					<tr>
				    						<th class="">#</th>
				    						<th>Player</th>
				    						<th class='text-right'>
				    							<span class="">Short</span>
				    						</th>
				    						<th class='text-right'>
				    							<span data-toggle="tooltip" title="Number of stocks shorted multiplied by current stock price">Position value</span>

				    						</th>
				    						<?php
				    						if (!isset($headerSwitch)) {
				    							?>
				    							<th class='text-right'>Stock price</th>
				    							<th class="text-right" data-toggle="tooltip" title="Stock price change in percent the latest trading day">
				    								Latest price change
				    							</th>
				    							<?php
				    						}
				    						?>
				    						<th class="text-right">
				    							<span data-toggle="tooltip" title="Position value change latest trading day, for the player. A positive value means the player hass earned (stock price falls) on the position.">Latest win/loss</span>
				    						</th>


				    						<th class="text-right" data-toggle="tooltip" title="Price at short position start (end of day value)">
				    							Startprice position
				    						</th>

				    						<th class="text-right" data-toggle="tooltip" title="Value change since the short position started, for the position holder.  A positive value means the player has earned (stock price falls) on the position.">
				    							Total value change
				    						</th>
				    					</tr>
				    				</thead>
				    				<tbody>