<?php 
 
//https://www.w3schools.com/tags/ref_urlencode.asp 
//%26 = &
//%2B = +
//require '../production_europe/namelink.php';

if (!function_exists('nametolink')) {
		function nametolink($nametolink) {
		$nametolink   = str_replace('&', '%26', $nametolink);
		$nametolink   = str_replace('+', '%2B', $nametolink);
		$nametolink   = str_replace("'", '%27', $nametolink);
		return $nametolink;
	}
}

if (!function_exists('linktoname')) {
		function linktoname($linktoname) {
		$linktoname   = str_replace('&amp;', '&', $linktoname);
		$linktoname   = str_replace('%2B', '+', $linktoname);
		$linktoname   = str_replace('%27', "'", $linktoname);
		return html_entity_decode($linktoname, ENT_QUOTES) ;
	}
}

?>