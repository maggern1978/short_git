<?php

require '../production_europe/namelink.php';
require '../production_europe/logger.php';
require '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

//$land = 'germany';

if (!$data = readJSON('../production_europe/json/events/player/' . $land . '.events.player.current.json'))
{
	echo 'Error reading json, returning.';
	return;
}

$changescount = 0;
$companyNameBox = [];
$playerNameBox = [];
$newestDate = '2000-01-01';

foreach ($data as $key => $change)
{
	
	$companyNameBox[] = $change['Name'];
	$playerNameBox[] = $change['PositionHolder'];;
	$changescount++;

	if ($newestDate < $change['ShortingDate'])
	{
		$newestDate = $change['ShortingDate'];
	}

}


$publishedate = date( "Y-m-d", strtotime("+1 Weekday", strtotime($newestDate)));

if ($publishedate > date('Y-m-d')) //never newer than today
{
	$publishedate = date('Y-m-d');
}



$companyNameBox = array_unique($companyNameBox);
$playerNameBox = array_unique($playerNameBox);

$numberOfPlayers = count($playerNameBox);
$numberOfCompanies = count($companyNameBox);

//var_dump($companyNameBox);

$numberOfChanges = count($data);

ob_start();

?>

<div class="col-12 m2-3">
	<h3>New positions</h3>

	<p>
	<span class="mt-2 font-weight-bold">Newest update was published  
		<?php 
		echo strtolower(date( "l", strtotime(($publishedate)))) . ' ' . date( "Y-m-d", strtotime(($publishedate))) . '. Comparing positions to ' . strtolower(date( "l", strtotime($newestDate))) . ' ' . date( "Y-m-d", strtotime($newestDate)) . '.'; 
		?> 
	</span>
	<?php
	echo 'Any new updates today? ';
	if ($newestDate >= date( "Y-m-d", strtotime("-1 Weekday")))
	{
		echo '<span class="font-weight-bold exchange-header-up">YES</span>';
	}
	else
	{
		echo '<span class="font-weight-bold exchange-header-down">NO</span>';
	}		

	?>
	<br>
		<?php echo $numberOfChanges; ?> changes in <?php echo $numberOfCompanies; ?> companies by <?php echo $numberOfPlayers; ?> players.
	</p>
</div>

<?php

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 

$filename = '../production_europe/html/events/header/' . $land . '.events.header.current.html';

// Write final string to file
file_put_contents($filename, $htmlStr);

?>