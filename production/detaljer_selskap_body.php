<?php

$playerposistionLocation = '../shorteurope-com.luksus.no/datacalc/players/'. $land . '/playerpositions.'. $land . '.current.json';
$shortliste = '../shorteurope-com.luksus.no/datacalc/shortlisten_' . $land . '_current.csv';

require '../production_europe/namelink.php';

//is this a single view? $singleCompany

$json = readJSON($playerposistionLocation);

$total_verdiendring = 0;
$total_eksponering = 0;
$total_positionValueChangeSinceStart = 0;
$counter = 1;

//last inn shortlisten med aksjekurser
//$shortliste = 'datacalc/shortlisten_nor_current.csv';
$csvFile = file($shortliste);            
$shortliste = [];

$number = 0;
foreach ($csvFile as $key => $line) 
{
    $shortliste[] = $row = str_getcsv($line); 
};

$company_array = array();
$positionCount = 0;
$numberOfPlayers = 0;
$positionsholder = [];

$foundsuccess = 0;

if(!isset($singleCompany))
{
	$singleCompany = 0;
}

//echo $singleCompany . '<br>';


foreach ($json as $player)
{
	foreach ($player['positions'] as $position)
	{
		$target = $position['ISIN'];

		//echo '"' . mb_strtolower($singleCompany)  . '"/"' . mb_strtolower($position["companyName"]) . '"<br>';
		//var_dump(mb_strtolower($singleCompany));
		//var_dump(mb_strtolower($position["companyName"]));

		//$singleCompany = 'next plc';

		if (mb_strtolower($singleCompany) == mb_strtolower($position["companyName"]) or $singleCompany == $position["companyName"])			
		{
			$positionsholder[] = $position;
			$positionCount++;
			$foundsuccess = 1;

		}
		else if ((string)$isin == (string)$position['ISIN'])			
		{
			$positionsholder[] = $position;
			$positionCount++;
			$foundsuccess = 1;
			$singleCompany = $position['companyName'];
		}		
	}
}

if ($foundsuccess == 0)
{
	echo '<div class="text-center">' . 'No active short positions found for company "' . $singleCompany .  '". Try <a href="' . 'history_company_index.php?country=' .  $land . '">history index for ' . ucwords($land) . '.</a></div>';
	return;
}
else
{
	include 'ads/banner_720.html'; 
}

$pageTitle = $singleCompany . ' - ' . $landNavn ;
$singleCompany = strtolower($singleCompany);
$singleCompany = ucwords($singleCompany);
$description = 'Updated short positions for ' . $singleCompany . '';
$keywords = $singleCompany . ', ';
$mode = 'single';


if ($positionCount == 1) {
	$posistionMention = 'position';
}
else {
	$posistionMention = 'positions';
}

?>

<div class="container ">
  <div class="row">
    <div class="col-12">
      <h1>Active positions for
        <?php 
        $selskapsnavn = $singleCompany;
        $tempnavn = strtolower($singleCompany);
        $tempnavn = ucwords($tempnavn);
        echo $tempnavn . ' ';
        echo '(' . ucwords($landNavn) . ')'; 
        $link = 'history_company.php?selskapsnavn=' . nametolink($selskapsnavn) . '&land=' . $land;
        ?>
      </h1>
    </div>
  </div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<a href="details_company_all.php?&land=<?php echo $land;?>" class="btn  my-2 btn-primary text-right " role="button">All active positions <?php echo $landNavn;?> </a>
			<a href="<?php echo $link;?>" 
				class="btn btn-primary text-right my-2 " role="button">
				Full history <?php echo $singleCompany ;?> 
			</a>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-12">
<?php

//echo '<h5>' . $positionCount . ' ' . $posistionMention . '. Currency is ' . $currency_ticker . '. ';
$description = $description . ': ' . $positionCount . ' positions';


?>
 </h5>
    </div>
  </div>
</div>
</div>
<div class="container">
    <div class="row">
       <div class="col-12">
       <?php 
       echo "Updated: " . date("Y-m-d, H:i",filemtime($playerposistionLocation)); ?>
       <br><br>
       </div>
    </div>
</div>

<?php

$headerSwitch = 1;
$descriptionTemp = '';
$total_short = 0;
$total_verdiendring = 0;
$total_eksponering = 0;
$total_winorloss = 0;
$total_positionValueChangeSinceStart = 0;


foreach ($positionsholder as $index => $posisjon) {

	if ($index == 0) {

		include '../production_europe/detaljer_selskap_single_header.php';
	}	

	$selskapNew  = strtolower($posisjon["PositionHolder"]);
	$selskapNew  = ucwords($selskapNew);
	$singleCompany   = nametolink($selskapNew);
	$singleCompany = strtoupper($singleCompany);
	$historyLink = 'details.php?player=' . $singleCompany .
	'&land=' . $land;

	echo '<tr>';
	echo '<td class="">';
	$runner = $index + 1;
	echo $runner;
	echo '.</td>';
	echo '<td>';
	echo '<a class="text-dark" data-toggle="tooltip" title="See active positions 
	for this player" href="' . $historyLink  . '">';
		echo ucwords($selskapNew );
	echo '</a>';
	echo '</td>';
	echo '<td class="text-right shortpercentcell" data-toggle="tooltip" title="' . number_format($posisjon['numberOfStocks'],0,".",",") . ' stocks">';
	echo number_format(round($posisjon['shortPercent'],2),2,".",",") . '&nbsp;%';
	echo '</td>';

	echo '<td class="text-right">';
	$posisjonsverdi = round($posisjon['marketValue'],2);

	echo number_format($posisjonsverdi,2,".",",") . '&nbsp;M ';
	echo '</td>';

	echo '<td class="text-right">';
	$verdiendring = round($posisjon['positionValueChange'],2);
	echo number_format($verdiendring,2,".",",") . '&nbsp;M ';
	echo '</td>';

	echo '<td class="text-right" data-toggle="tooltip" title="Position started ' . $posisjon['positionStartDate'] . '.">';
	if (!isset($posisjon['startStockPrice']) or $posisjon['startStockPrice'] == '' or $posisjon['startStockPrice'] == 0) 
	{
		echo '-';
	}
	else 
	{
		echo round($posisjon['startStockPrice'],2);
	}
	echo '</td>';

	echo '<td class="text-right">';

	if (!isset($posisjon['positionValueChangeSinceStart']) or $posisjon['positionValueChangeSinceStart'] == '' or $posisjon['startStockPrice'] == '' or $posisjon['startStockPrice'] == 0) 
	{
		echo '-';
	}
	else 
	{
		$change = round($posisjon['positionValueChangeSinceStart']/1000000,2);
		echo number_format($change,2,".",",") . '';
	}
	echo '&nbsp;M</td>';
	echo '</tr>';

	$total_short += round($posisjon['shortPercent'],2);
	$total_winorloss += $verdiendring;

	if (isset($posisjon['positionValueChangeSinceStart']) and $posisjon['positionValueChangeSinceStart'] != '-')
	{
		$total_verdiendring += $posisjon['positionValueChangeSinceStart']/1000000;
	}
	
	$total_eksponering += $posisjon['marketValue'];

	if (isset($posisjon['positionValueChangeSinceStart']) and $posisjon['positionValueChangeSinceStart'] != '' and $posisjon['positionValueChangeSinceStart'] != NULL and $posisjon['positionValueChangeSinceStart'] != '-') 
	{
		$total_positionValueChangeSinceStart += $posisjon['positionValueChangeSinceStart'];		
	}
	
}


$count = count($positionsholder);

if ($count > 1)
{
	echo '<tr>';
	echo '<td>';
	
	echo '</td>';

	echo '<td class="font-weight-bold">';
	echo 'Sum:';
	echo '</td>';

	echo '<td class="text-right font-weight-bold">';
	echo $total_short;
	echo '&nbsp;%</td>';

	echo '<td class="text-right font-weight-bold">';

	$total_eksponering = round($total_eksponering,2);
	echo number_format($total_eksponering,2,".",",")  . '&nbsp;M';

	echo '</td>';

	echo '<td class="font-weight-bold text-right ' . getcolor($total_winorloss)  . '">';
	echo $total_winorloss;
	echo '&nbsp;M</td>';

	echo '<td class="text-right">';
	echo '</td>';

	echo '<td class="text-right font-weight-bold ' . getcolor($total_verdiendring) . ' ">';
	$total_verdiendring = round($total_verdiendring,2);
	echo number_format($total_verdiendring,2,".",",") . '&nbsp;M';

	echo '</td>';
	echo '</tr>';
}

?>
</tbody>
</table>
<script>
    var endshortpercent = <?php echo $total_short; ?>;
</script>
<div class="mb-4">
</div>

</div>
</div>
</div>
<?php 

if (isset($description) and $mode != 'multi') {
	$description = $description . 'Shorted by: ' .  $descriptionTemp;
}


?>