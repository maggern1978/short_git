<?php

include '../production_europe/functions.php'; //må rettes
$development = 0; 
$land = 'norway';

$savedir = '..shortnordic-com.luksus.no/dataRaw/ssr.finanstilsynet/finanstilsynet_current.json';
$savedir = '../shorteurope-com.luksus.no/dataraw/norway.history/norway.history_current.json';

//first download

$url = 'https://ssr.finanstilsynet.no/api/v2/instruments/export-json';

if ($development == 0)
{
	//download file
	if (!$keepBox = json_decode(download($url),2))
	{
		errorecho('Error downloading file!');
	}	
}
else
{
	$keepBox = readJSON('shorthistory.json');
}

saveJSON($keepBox, '../shorteurope-com.luksus.no/dataraw/norway.history/norway.history_current_downloaded.json');

$keepBoxcount = count($keepBox);
echo 'Doing ' . count($keepBox) . ' companies.<br>';

for ($i = 0; $i < $keepBoxcount; $i++)
{
	//echo $i . ' ' . $keepBox[$i]['issuerName'] .' | ';
	//echo count($keepBox[$i]['events']) .'<br>';
}

//first, add positions endpositions where needed by comparing events

for ($i = 0; $i < $keepBoxcount; $i++)
{
	
	echo '<strong>' . $i . ' ' . $keepBox[$i]['issuerName'] .'</strong><br>';

	if ($keepBox[$i]['issuerName'] != 'Aker Solutions')
	{
		//continue;
	}

	$keepBoxeventscount = count($keepBox[$i]['events']);

	$previousactivepositions = '';

	for ($x = 0; $x < $keepBoxeventscount; $x++)
	{
		
		//echo '---| Event round ' . $x . '. ';

		if ($x == 0)	
		{
			$activeposcount = count($keepBox[$i]['events'][$x]['activePositions']);
			if ($activeposcount > 0)
			{
				//echo '---| Non-zero short in first position. ' . $activeposcount . ' positions is active.' ;
				//adding in isactive 

				for ($c = 0; $c < $activeposcount; $c++)
				{
					$keepBox[$i]['events'][$x]['activePositions'][$c]['isActive'] = 'yes';
					//echo 'Setting on position active. ';
				}
				//echo '<br>';
			}
			else
			{
				//echo '---| Zero short in first positions. Will add all nextactive with zero stocks.<br>'; //then all positions in next activepositions should be added as zero

				if (isset($keepBox[$i]['events'][$x+1]))
				{

					foreach ($keepBox[$i]['events'][$x+1]['activePositions'] as $key => $position) //adding positions
					{
						$keepBox[$i]['events'][$x]['activePositions'][] = $position;
					}

					foreach ($keepBox[$i]['events'][$x]['activePositions'] as $key => $position) //setting positions short to zero
					{
						$keepBox[$i]['events'][$x]['activePositions'][$key]['date'] = $keepBox[$i]['events'][$x]['date'];
						$keepBox[$i]['events'][$x]['activePositions'][$key]['shares'] = 0;
						$keepBox[$i]['events'][$x]['activePositions'][$key]['shortPercent'] = 0;
					}					
				}
				continue;
			}
				
		}

		if (!isset($keepBox[$i]['events'][$x+1])) //no next
		{
			//echo 'No next, continueing...<br>';
			break;
		}	

		$currentdate = $keepBox[$i]['events'][$x]['date'];	
		$currentpositions = $keepBox[$i]['events'][$x]['activePositions'];
		$nextposistions =  $keepBox[$i]['events'][$x+1]['activePositions'];

		//echo 'Compairing against previous active positions. ';
		//echo 'Current date is ' . $currentdate  . '<br>';

		//look for the player names 
		foreach ($nextposistions as $key => $nextposition)
		{
			$targetname = mb_strtolower($nextposition['positionHolder']);
			$foundname = 0;

			foreach ($currentpositions as $index => $currentposition)
			{

				if ($targetname == mb_strtolower($currentposition['positionHolder']))
				{
					$foundname = 1;
					break;
				}

			}

			if ($foundname == 0) //if not found, add position to 
			{
				//echo '---|---| Nextposition not found in current positions, meaning it has ended. Will zero short and add to currentpositions<br>';
				$nextposition['shortPercent'] = 0;
				$nextposition['shares'] = 0;
				$nextposition['date'] = $currentdate;
				$currentpositions[] = $nextposition;
			}

		}

		$keepBox[$i]['events'][$x]['activePositions'] = $currentpositions;


	}

}

//saveJSON($keepBox, '../shorteurope-com.luksus.no/dataraw/norway.history/norway.history_current_test2.json');


for ($i = 0; $i < $keepBoxcount; $i++)
{
	//echo $i . ' ' . $keepBox[$i]['issuerName'] .'<br>';
	$keepBox[$i]['ISIN'] = $keepBox[$i]['isin'];
	$keepBox[$i]['Name'] = $keepBox[$i]['issuerName'];
	$keepBox[$i]['ShortPercent'] = 0;
	$keepBox[$i]['ShortedSum'] = 0;
	$keepBox[$i]['LastChange'] = '';
	$keepBox[$i]['NumPositions'] = 0;
	$keepBox[$i]['Positions'] = [];

	//collect every position in events
	$keepBoxeventscount = count($keepBox[$i]['events']);

	for ($x = 0; $x < $keepBoxeventscount; $x++)
	{
		$activePositionsCount = count($keepBox[$i]['events'][$x]['activePositions']);
		for ($y = 0; $y < $activePositionsCount; $y++)
		{
			//echo $x . ' ' . $y . '<br>';
			$keepBox[$i]['Positions'][] = $keepBox[$i]['events'][$x]['activePositions'][$y];
		}
		
	}

	unset($keepBox[$i]['events']);

	//sett data for posisjonene
	$poscount = count($keepBox[$i]['Positions']);
	//echo 'Poscount er ' . $poscount . '<br>';

	$lastChange = '2001-01-01';

	for ($x = 0; $x < $poscount; $x++)
	{
		$pos = new stdClass;
		$pos->ShortingDate = $keepBox[$i]['Positions'][$x]['date'];
		$pos->PositionHolder = $keepBox[$i]['Positions'][$x]['positionHolder'];
		$pos->ShortPercent = $keepBox[$i]['Positions'][$x]['shortPercent'];
		$pos->NetShortPosition = $keepBox[$i]['Positions'][$x]['shares'];
		$pos->ISIN = $keepBox[$i]['ISIN'];
		$pos->Name = $keepBox[$i]['Name'];

		if (!isset($keepBox[$i]['Positions'][$x]['isActive']))
		{
			//echo 'isActive is not set! ';
			$pos->isActive = 'no';
		}
		else
		{
			$pos->isActive = 'yes';
			$keepBox[$i]['ShortPercent'] +=	$pos->ShortPercent;
			$keepBox[$i]['ShortedSum'] +=	$pos->NetShortPosition;
		}

		//$pos->isActive = 'yes';
		$keepBox[$i]['Positions'][$x] = (array)$pos;

		if ($pos->ShortingDate > $lastChange)
		{
			$lastChange = $pos->ShortingDate;
		}
	}

	$keepBox[$i]['ShortPercent'] = number_format(round($keepBox[$i]['ShortPercent'],2),2);

	$keepBox[$i]['NumPositions'] = count($keepBox[$i]['Positions']);
	$keepBox[$i]['LastChange'] = $lastChange;

	unset($keepBox[$i]['isin']);
	unset($keepBox[$i]['issuerName']);
	
	//sorter etter dato
	usort($keepBox[$i]['Positions'], function ($item1, $item2) {
	return $item2['ShortingDate'] > $item1['ShortingDate'];
	});

}

saveJSON($keepBox, $savedir);



?> 
