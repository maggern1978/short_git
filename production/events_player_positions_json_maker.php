<?php 

//include 'header.html';


//$land = 'denmark';

require '../production_europe/namelink.php';
require '../production_europe/logger.php';
require '../production_europe/functions.php';
date_default_timezone_set('Europe/Oslo');
echo '<br>';

//$land = 'sweden';

//read todays positions
if (!$currentdata = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json'))
{
	errorecho('Error reading current, returning');
	return;

}

//find the newest position date
$newestdate = '2001-01-01';

foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $key => $position)
	{
		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}
}

//todo format the date
$newestdate = date('Y-m-d', (strtotime($newestdate)));
$previousday = date('Y-m-d', (strtotime ('-1 weekday', strtotime ($newestdate))));

echo 'Newest date in current positions is ' . $newestdate . '<br>';
echo 'Previous working day of that is ' . $previousday . '<br>';


//if neweste date == today, 
echo 'Targetfile is then ' . $land . '/' . $previousday . '.' . $land . '.json' .', because that file has positions from the day before.<br> ';

if (!$previousdata = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $previousday . '.' . $land . '.json'))
{
	errorecho('Error reading previous file ' . $previousday .  ', returning');
	return;
}


$currentBox = [];
//collect positons in two arrays
foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $index => $position)
	{
		
		if (!isset($position['isActive']))
		{
			continue;
		}
		//only take active positions
		if ($position['isActive'] == 'yes')
		{
			//make sure that all positions have a company name, some are missing!
			$currentdata[$key]['Positions'][$index]['Name'] = $company['Name'];
			$currentBox[] = $currentdata[$key]['Positions'][$index];
		}

	}

}



$previousBox = [];
//collect positons in two arrays
foreach ($previousdata as $key => $company)
{
	foreach ($company['Positions'] as $index => $position)
	{
		if (!isset($position['isActive']))
		{
			continue;
		}

		//only take active positions
		if ($position['isActive'] == 'yes') 
		{
			//make sure that all positions have a company name, some are missing!
			$previousdata[$key]['Positions'][$index]['Name'] = $company['Name'];
			$previousBox[] = $previousdata[$key]['Positions'][$index];
		}
	}
}

$new_or_updated_Box = [];

//adding all positions with date to to new_or_updated_Box;
$addcounter = 0;
 foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $key => $position)
	{
		
		$formatted_date = date('Y-m-d', (strtotime($position['ShortingDate'])));

		if ($formatted_date >= $newestdate)
		{
			//$position['mode'] = 'new_in_current';
			$new_or_updated_Box[] = $position;
			$addcounter++;
		}
	}
}

echo 'Added ' . $addcounter . ' positions from currentdata based on position date ' . $newestdate . ' to $new_or_updated_Box<br><br>';

//var_dump($new_or_updated_Box);

//comparing previous positions to current positions
//this is the other way around here

echo 'Finding positions in previousdata that are not in current positions, and hence are changed or ended. <br>';
echo 'Antall er ' . count($previousBox) . '<br>';


$previousadd = 0;

$only_in_previous_box = [];

foreach ($previousBox as $key => $currentposition)
{
	
	include '../production_europe/flush_start.php';
	//echo $key . '<br>';
	$targetPositionHolder = $currentposition['PositionHolder'];
	$targetShortingDate = $currentposition['ShortingDate'];
	$targetShortPercent = $currentposition['ShortPercent'];
	$targetNetShortPosition = $currentposition['NetShortPosition'];
	
	if (!isset($currentposition['Name']))
	{
		eerrorecho('Name not set in currentpositions!');
	}

	$targetName = $currentposition['Name'];

	$targetISIN = $currentposition['ISIN'];
	$success = 0;

	foreach ($currentBox as $index => $previousposition)
	{
		
		if (!isset($previousposition['Name']))
		{
				errorecho('Name not set in previouspositon!');
				var_dump($previousposition);
				
		}
	
		if (strtolower($targetPositionHolder) == strtolower($previousposition['PositionHolder']) and $targetShortingDate == $previousposition['ShortingDate'] and $targetShortPercent == $previousposition['ShortPercent'] and $targetNetShortPosition == $previousposition['NetShortPosition'] and strtolower($targetName) == strtolower($previousposition['Name']) and $targetISIN == $previousposition['ISIN'])
		{
			//echo $key . '. Identisk posisjon funnet';
			$success = 1;
			break;
		}
			
	}

	if ($success == 0)
	{
		//$currentposition['mode'] = 'only_in_previous';
		echo $key . '. ';
		echo 'Previous position not found in current position, adding to only_in_previous_box<br>';
		$previousadd++;
		$only_in_previous_box[] = $currentposition;
	}
	
	include '../production_europe/flush_end.php';
}



echo 'Added ' . $previousadd . ' positions from previousdata.<br>';

//var_dump($new_or_updated_Box);

//ta new_in_current-posisjonene og finn ut om posisjonen er helt ny, eller om det var en tidligere posisjon
echo '<br>';
echo 'Checking if player and company in current positions had previous position, or is new position<br>';
echo 'Antall er ' . count($new_or_updated_Box ) . '<br>';


foreach ($new_or_updated_Box as $key => $position)
{

	$success = 0;

	$targetName = $position['Name'];
	$targetPositionHolder = $position['PositionHolder'];

	foreach ($previousBox as $index => $previousposition)
	{

		if (strtolower($previousposition['Name']) == strtolower($targetName) and strtolower($previousposition['PositionHolder']) == strtolower($targetPositionHolder))
		{
			echo $key. '. Player ' . $previousposition['PositionHolder'] . ' har en tidligere posisjon i selskapet ' .  $previousposition['Name'] . '. Settings status to update.<br>';
			$new_or_updated_Box[$key]['status'] = 'update';
			$new_or_updated_Box[$key]['previousposition'] = $previousposition;
			$success = 1;
			break;
		}
	}

	if ($success == 0)
	{
		//$new_or_updated_Box[$key]['previousposition'] = ['none'];
		echo $key . '. Player ' . $targetPositionHolder . ' har <strong>IKKE</strong> tidligere posisjon i selskapet ' . $targetName . '. Setting status to new.<br>';
		$new_or_updated_Box[$key]['status'] = 'new';
	}
}

//looking for positions in current, if not, position is ended

$endedpositions = [];
echo '<br>';
echo 'Checking if previouspositions has same company and player in current. If not, position is ended. <br>';


foreach ($only_in_previous_box as $key => $position)
{
	$targetName = $position['Name'];
	$targetPositionHolder = $position['PositionHolder'];

	$success = 0;

	foreach ($currentBox as $index => $currentposition)
	{

		if (strtolower($currentposition['Name']) == strtolower($targetName) and strtolower($currentposition['PositionHolder']) == strtolower($targetPositionHolder))
		{
			echo $key . '. Player ' . $currentposition['PositionHolder'] . ' har en posisjon i current i selskapet ' .  $currentposition['Name'] . '<br>';
			//$new_or_updated_Box[$key]['currentposition'] = $currentposition;
			$success = 1;
			break;
		}
	}

	if ($success == 0)
	{
		echo $key . '. ' . $targetName . ' med playernavn ' . $targetPositionHolder . ' <strong>IKKE</strong> funnet. ';
		echo 'Posisjonen legges til med status ended <br>';
		$position['status'] = 'ended';
		$endedpositions[] = $position;
	}
}

//slå sammen arrayene

$allpositions = array_merge($new_or_updated_Box, $endedpositions);
$allpostionsCount = count($allpositions);

//sort the positons
usort($allpositions, function ($item1, $item2) {
  return $item2['PositionHolder'] < $item1['PositionHolder'];
});

echo '<br>';
echo 'Total numer of positions that are changed: ' . $allpostionsCount . '<br><br>';

foreach ($allpositions as $key => $company)
{
	$allpositions[$key] = float_format($company);
}

saveJSON($allpositions, '../production_europe/json/events/player/' . $land . '.events.player.current.json');


?>