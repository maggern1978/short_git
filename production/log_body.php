<?php



switch ($country) {
    case "norway":
        $comment = 'Positions are updated every trading day at 15:30 CET, and reflects the positions at 23:59 the previous day.';
        break;
    case "sweden":
        $comment = 'Positions are updated every trading day at 16:00 CET, and reflects the change in positions the previous day.';
        break;
    case "denmark":
        $comment = 'Positions are updated several times every trading.';
        break;
    case "finland":
        $comment = 'Positions are updated twice a day: 10.00 and after 17.00. ';
        break;
    case "germany":
        $comment = 'Positions are updated working days. ';
        break;
    case "france":
        $comment = 'Positions are updated working days. ';
        break;
    case "spain":
        $comment = 'Positions are updated working days. ';
        break;
    case "italy":
        $comment = 'Positions are updated working days. ';
        break;        
    case "united_kingdom":
        $comment = 'Positions are updated working days. ';
        break;                
    default: 
      echo 'Not found';
    return;
}

include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';

$land_navn = ucwords($land);
$historyfil = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json';
$playerliste = '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';

require '../production_europe/namelink.php';

$land = $country;
$pageTitle = 'Change log short positions ' . ucwords($country);

//var_dump($playerpositions);

$string = file_get_contents($historyfil);
$positions = json_decode($string, true);

//read date from generated file to set date
$date = date('Y-m-d');

//Set latest working day
$dates = array();
$startdate = new DateTime();

//if weekend, choose day before
if ($startdate->format('N') == 6) {
	$startdate =  (new \DateTime())->modify('-1 day');
}
else if ($startdate->format('N') == 7) {
	$startdate =  (new \DateTime())->modify('-2 day');
}

//get index
if ($index > 0) {
	$minusDays = -7*$index . ' day';
	//var_dump($minusDays);
	$startdate->modify($minusDays);
	$enddate = clone $startdate;
	$enddate->modify('-7 days');
	//var_dump($enddate);
}
else {
	$enddate = clone $startdate;
	$enddate->modify('-7 days');
}

//echo $startdate->format('Y-m-d') . ' start <br>';
//echo $enddate->format('Y-m-d') . ' slutt<br>';

$positionsCount = count($positions);
//$targetDate = $date;

//dev
//$targetDate = '2019-02-25';



$hitsBox = array();
	
for ($i = 0; $positionsCount > $i; $i++) {
	//var_dump($positions);
	$localPositionsCount = count($positions[$i]['Positions']);
	//echo $positions[$i]['Name'] .  '<br>';

	for ($x = 0; $localPositionsCount > $x; $x++) {
		
		if (isset($positions[$i]['Positions'][$x]['ShortingDate'])){
			$historyDate = date("Y-m-d", strtotime($positions[$i]['Positions'][$x]['ShortingDate']));
			$historyDate = new DateTime($historyDate);
			//echo $historyDate . '<br>';
			//var_dump($historyDate);
			//var_dump($startdate);
			//var_dump($enddate);
			if (testdate($startdate, $enddate, $historyDate)) {
				$hitsBox[] = $positions[$i]['Positions'][$x];

			}
		}	
	}
}

//var_dump($hitsBox);

usort($hitsBox, 'my_sort_function');

function my_sort_function($a, $b)
{
	$a = date("Y-m-d", strtotime($a['ShortingDate']));
	$b = date("Y-m-d", strtotime($b['ShortingDate']));

    return $a < $b;
}

function testdate($start_date, $end_date, $find) {

if($find->format("Y-m-d") <= $start_date->format("Y-m-d") 
	&& $find->format("Y-m-d") >= $end_date->format("Y-m-d") )

//if($find >= $start_date && $find <= $end_date)
{
	//echo ' True! Date is ' . $find->format("Y-m-d") . '<br>';
   return true;
}
else {
	//echo ' False! ';
	return false;
}
}

$linksBox = array();
$indexShadow = $index +1;
if ($index == 0) {

	$linksBox[0] = 0;
	$linksBox[1] = 1;
}
else {
	$linksBox[0] = $index-1;
	$linksBox[1] = $index+1;
}

//var_dump($linksBox);

?>

<div class="container ">
  <div class="row">
    <div class="col-lg-12">
      <h2>Changelog  
      <?php 
      echo ucwords($country);
      ?>
  </h2>
  <p><span class="font-weight-bold">From <?php echo $startdate->format('Y-m-d'); ?> to <?php echo $enddate->format('Y-m-d') . '. </span>' . $comment; ?></p>
</div>
</div>
</div>

<div class="container">
<table class="table table-hover">
    <thead>

      <tr>
        <th>Posistion Date</th>
        <th>Company</th>
        <th>Short %</th>
        <th>Player</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($hitsBox as $position) { ?>
      <tr>
        <td><?php echo date("Y-m-d", strtotime($position['ShortingDate'])); ?></td>
        <td>
        <?php 
          $companyName = strtolower($position['Name']); 
          $linkname  = nametolink($companyName);

          $link = 'details_company.php?company=' . $linkname . '&land=' . $land;
          //details_company.php?company=INTRUM&land=sweden
          echo '<a href="' . $link .  '"  data-toggle="tooltip" title="Show active positions for company">';
          echo ucwords($companyName);
        ?>
          
        </td>
        <td><?php echo $position['ShortPercent']; ?></td>
        <td>
        <?php 
        
        $playerName = strtolower($position['PositionHolder']);
        $linkname  = nametolink($playerName);

        $link = 'details.php?player=' . $linkname . '&land=' . $land;
        echo '<a href="' . $link .  '"  data-toggle="tooltip" title="Show active positions for player">';
        echo ucwords($playerName); 
        ?>
          
        </td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="log.php?country=<?php echo $country . '&index=' . $linksBox[0];?>
    "><<</a></li>
    <li class="page-item"><?php echo 'Page ' . $indexShadow;?></li>
        <li class="page-item"><a class="page-link" 
    href="log.php?country=<?php echo $country . '&index=' . $linksBox[1];?>
    ">>></a></li>
  </ul>
</nav>