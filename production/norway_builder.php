<?php 

if (!function_exists('isopen')) {
	include '../production_europe/isopen.php';
}
require '../production_europe/logger.php'; //tar inn loggerfunksjonen
require ('../production_europe/functions.php');
require('../production_europe/run.php');
	
	$land  = 'norway';

	if (isopen('norway')) 
	{

		run('../production_europe/norway_active_download.php'); //laster ned json-fil fra finanstilsynet og setter aktive posisjoner
		
		$land  = 'norway';
		echo '<br>../production_europe/set_currency_for_json.php starts <br>';
		$inputFile = '../shorteurope-com.luksus.no/dataraw/' .  $land . '/' .  $land . '_current.json';
		include ('../production_europe/set_currency_for_json.php'); //legger til currency og basecurrency
		echo '<br>../production_europe/set_currency_for_json.php DONE <br>';

		echo '<br><strong>filter_out_old_positions.php starter... </strong><br>';
		include '../production_europe/filter_out_old_positions.php'; //gjør om csv-filen til en json-fil med alle aktive posisjoner
		echo '<br>filter_out_old_positions.php done<br>';	
	
		//gjør om navnene fra  myndigheter til det som står i hovedlisten for selskaper. 
		echo '<br>../production_europe/json_set_names_to_isinlist.php starts <br>';
		$mode = 'norway';
	    $inputFile = '../shorteurope-com.luksus.no/dataraw/' .  $land . '/' .  $land . '_current.json';
	    include '../production_europe/json_set_names_to_isinlist.php';
	    echo '<br>../production_europe/json_set_names_to_isinlist.php done <br>';

	}
	else 
	{
		echo 'Norway is closed!';
	}

echo 'Norway_builder done!';


?>