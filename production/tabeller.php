<?php 

//$land = 'norway';

include '../production_europe/options_set_currency_by_land.php';
include '../production_europe/functions.php';
require '../production_europe/namelink.php';

$filename = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';
$graphlocation = '../shorteurope-com.luksus.no/datacalc/graphdata/' .  $land . '/';

if (!$data_json = readJSON($filename))
{
	errorecho('Error reading ' . $filename . '<br>');
	return;
}

//find gross active positions, number of players and number of companies

$grossnumberofcompanies = count($data_json);
$grossnumberofplayers = 0;
$grossnumberofpositions = 0;
$grossNameContainerPlayers = [];

for ($i = 0; $i < $grossnumberofcompanies; $i++)
{
	$poscount = count($data_json[$i]['Positions']);
	$grossnumberofpositions += $poscount;

	for ($x = 0; $x < $poscount; $x++)
	{
		if ($data_json[$i]['Positions'][$x]['isActive'] == 'yes')
		{
			$grossNameContainerPlayers[] = mb_strtolower($data_json[$i]['Positions'][$x]['PositionHolder']);
		}
	}
}

$grossNameContainerPlayers = array_unique($grossNameContainerPlayers);
$grossNameContainerPlayers = array_values($grossNameContainerPlayers);
$grossnumberofplayers = count($grossNameContainerPlayers);


$playerliste = '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';

echo $playerliste;
$land_navn = ucwords($land);

//last inn shortlisten som inneholder prosent short
//last inn shortlisten med aksjekurser
$filename = '../shorteurope-com.luksus.no/datacalc/shortlisten_' . $land . '_current.csv';
$csvFile = file($filename);            
$data = [];
foreach ($csvFile as $line) {
	$data[] = str_getcsv($line);
} 

//Tabell for oversiktsdata:
$antall_selskaper = count($data);
$total_eksponering = 0;
$antall_playere = 0;
$antall_posisjoner = 0; 
$short_gevinst_eller_tap = 0;
$counter = 0; 
$upCounter = 0;
$downCounter = 0;
$zeroCounter = 0;

foreach ($data as $entry) {
	//hopp over første linje, som er headerteksten
	if ($counter != 0) {
		if (isset($entry[1]))  {
			if ($entry[7]>0) {
				$upCounter++;
			}
			else if ($entry[7] < 0) {
				$downCounter++;
			}
			else {
				$zeroCounter++;
			}

			if ($entry[1] != 0) {
			//Summen av alle tap og gevinster i millioner kroner * base_currency 
				$short_gevinst_eller_tap += ($entry[1]/1000000)*$entry[15];
			}
		}
		else 
		{
			$short_gevinst_eller_tap  += '0';
		}

		$tall = (((float)$entry[4]*(float)$entry[6])/1000000);
		$tall = round($tall,2);
		$tall = number_format($tall,2,".","");
		$total_eksponering += $tall*$entry[15];
	}
	$counter++;
}

$date = date('Y-m-d');

//Last inn nyeste shorttabell 
$selskapsliste = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';
$string = file_get_contents($selskapsliste);

if (!$shortdata = json_decode($string, true))
{
	errorecho('could not read shortdata, returning');
	return;
}		

//Last inn playertabell
$playerpositions = file_get_contents($playerliste);
if (!$playerpositions = json_decode($playerpositions, true))
{
	errorecho('could not read playerpositions, returning');
	return;
}	

//var_dump($playerpositions);
//finn største posisjon

if (!function_exists('cmp')) {
	function cmp($a, $b)
	{
		return strnatcmp($a->marketValue, $b->marketValue);
	}
}

//Last inn nyeste shorttabell 
//var_dump($playerposisjoner[0]);
$posisjonsholder = array();
$totalpositioncount = 0; 

foreach ($playerpositions as $key => $player) {
	foreach ($player['positions'] as $index => $posisjon) {
		$obj_posisjoner = new stdClass;
		$obj_posisjoner->playerName = $player['playerName'];
		$obj_posisjoner->marketValue = $posisjon['marketValueBaseCurrency'];
		$obj_posisjoner->companyName = $posisjon['companyName'];
		$posisjonsholder[] = $obj_posisjoner;
		$totalpositioncount++;
	}
}

usort($posisjonsholder, "cmp");

$posisjonsholder = array_reverse($posisjonsholder);

//var_dump($posisjonsholder[0]);

$biggestBetPlayerName = $posisjonsholder[0]->playerName;
$biggestBetSum = $posisjonsholder[0]->marketValue;
$biggestBetCompany = $posisjonsholder[0]->companyName;


//Finn antall playere
//Først finn de aktive selskapene

$playerUp = 0;
$playerDown = 0;
$playerZero = 0;
$positionsUp = 0;
$positonsDown = 0;
$positionsZero = 0;

foreach ($playerpositions as $key => $player) {
	if ($player['totalValueChange'] > 0) {
		$playerUp++;
	}
	else if (($player['totalValueChange'] < 0) ) {
		$playerDown++;
	}
	else {
		$playerZero++;
	}

	foreach ($player['positions'] as $index => $position) {
		if ($position['stockPriceChange'] > 0) {
			$positonsDown++;
		}
		else if (($position['stockPriceChange'] < 0) ) {
			$positionsUp++;
		}
		else {
			$positionsZero++;
		}
	}
}


$totalplayerarray = array();
$hitarray = array();
$counter = 0;

foreach ($shortdata as $entries) {
	//Hvis posisjonen er større enn null
	if (($entries['ShortPercent']) != 0) {
		$hitarray[] = $counter;

		//Legg til player
		foreach ($entries['Positions'] as $position) {	
			$totalplayerarray[] = $position['PositionHolder'];
		}
	}
	$counter++; 
}

$playerarray_unik = array_unique($totalplayerarray);

//For å finne antall aktive shortere, ser vi etter den første posisjonen vi finner i hvert selskap som shortes, som ikke har null prosent verdi. 

// Check the class exists before trying to use it
if (!class_exists('position')) 
{
	class position 
	{
          //Creating properties
		public $companyName;
		public $positionHolder;
		public $shortPercent;
		public $NetShortPosition;
		public $ShortingDate;
		public $valueChange;
		public $stockChange;
		public $stockPrice;
		public $marketCap;

            //assigning values
		public function __construct($companyName, $positionHolder, $shortPercent, $NetShortPosition, $ShortingDate, $valueChange, $stockChange, $stockPrice, $marketCap) {
			$this->companyName = $companyName;
			$this->positionHolder = $positionHolder;
			$this->shortPercent = $shortPercent;
			$this->NetShortPosition = $NetShortPosition;
			$this->ShortingDate = $ShortingDate;
			$this->valueChange = $valueChange;
			$this->stockChange = $stockChange;
			$this->stockPrice = $stockPrice;
			$this->marketCap = $marketCap;
		}
	}
}

//definer tellere og arrays
$totalt_antall_playere = 0;
$allpositions = array();

//Ta et player navn
foreach ($playerarray_unik as $player) {

	//Let i hvert shortet selskap
	foreach ($shortdata as $entries) {
		
		//Let i alle posisjonen til første hit
		foreach ($entries['Positions'] as $position) {

			if ($player == $position['PositionHolder']) {
				
				
				//fant den første posisjon med riktig navn. Gå videre hvis den ikke er null?
				
				if (!$position['NetShortPosition'] == 0 ) {
					//var_dump($position);
				 //Find value change pr posisjon

					global $valueChange;
					global $stockChange;
					global $stockPrice;
					global $marketCap;

					//gå igjennom listen
					foreach ($data as $company) {
						if ($company[0] == $entries['Name']) {
							
							//var_dump($company);
							//antall aksjer ganget kursendring gir verdiendring
							$valueChange = floatval(-1*(float)$position['NetShortPosition']*(float)$company[7]);
							$stockChange = floatval($company[7]);
							$stockPrice = floatval($company[6]);
							$marketCap = floatval($stockPrice*$position['NetShortPosition']);
						}
					}

				 //push values to object
					$allpositions[] = new position($entries['Name'], $player, $position['ShortPercent'], $position['NetShortPosition'],
						$position['ShortingDate'], $valueChange, $stockChange, $stockPrice, $marketCap);

				}
				break;
			}

		}

	}

}

sort($playerarray_unik);

//regn sammen market cap pr player

$summert_pr_playernavn = array();
$summert_playernavn = array();

foreach ($playerarray_unik as $key => $playernavn) {
	$temp_holder = 0;
	foreach ($allpositions as $index => $position) {
		if ($playernavn == $position->positionHolder) {
			$summert_playernavn[$key] = $playernavn;
			$temp_holder += $position->marketCap;			
		}
	}
	$summert_pr_playernavn[$key] = $temp_holder;
	

}

// Slå sammen gevinst og tap pr posisjon for hver player

$company_array = array();
$value_array = array();
$target_value = 0;
$company_array[] = '';
$counter = 0;

foreach ($allpositions as $position) 
{
	
	//Sjekk om posisjonsholder er i tabell
	if ($position->positionHolder == $company_array[$counter]) 
	{
		$value_array[$counter] = $value_array[$counter] + $position->valueChange;
	}
	else 
	{
		array_push($company_array, $position->positionHolder);
		$counter++;
		$value_array[$counter] = $position->valueChange;
	}
}

//Slett den første verdien i selskaps-arrayen, hvis ikke blir ikke arrayene like når de sorteres
unset ($company_array[0]);

//Sorter navn og summert gevinst/tap-array fra vinner til taper

if (!array_multisort($value_array, SORT_DESC, $company_array))
{
	logger('Error in array_multisort for ' . $land, ' i tabeller.php. ');
}

//finn det mest shortede selskapet

$target = $data[1][5];
$counter= 0;

$money_target = $data[1][4]*$data[1][6];
$money_target_name = $data[1][0];

$name = $data[1][0];

foreach ($data as $selskap) 
{
	if ($counter != 0) 
	{

		//verdi
		$money_target_temp = (float)$selskap[4]*(float)$selskap[6];

		if ($money_target_temp >= $money_target)
		{
			$money_target = $money_target_temp;
			$money_target_name = $selskap[0];
		}	

		//prosentandel
		if ($target <= $selskap[5] )
		{
			

			$target = $selskap[5];
			$name = $selskap[0];
		}
	}
	$counter++;
}



date_default_timezone_set('Europe/Oslo');
$now = date('Y-m-d H:i',strtotime("now"));

?>
<?php ob_start(); //capture start ?>
<h3 class=""><span data-toggle="tooltip" title="Latest trading day">Market summary <?php echo formatname($land); ?> </span><span class="pull-right updated align-middle"></h3> 

<div class="text-white nokkeltall

<?php 
$success = 1;
if ($short_gevinst_eller_tap > 0) {
	echo 'bg-success';	
}
else {
	echo 'bg-danger';
	$success = 0;
} ?> "><span class="panel-header text-white">All positions:
<?php
//abs er absolutt verdi av tall

$short_gevinst_eller_tap = round($short_gevinst_eller_tap,1);

echo (abs($short_gevinst_eller_tap))  . ' million ' . $currency_ticker . ' ';

if ($short_gevinst_eller_tap > 0) {
	echo 'earned ';
	echo '<i class="fa fa-arrow-circle-up pull-right icon_font_awesome" aria-hidden="true"></i></span>';

}
else {
	echo 'lost ';
	echo '<i class="fa fa-arrow-circle-down pull-right icon_font_awesome" aria-hidden="true"></i></span>'	;
}
?></span>
</div>

	<?php //gjør klar til eksport
	$Mestshortetnavn = '';
	$Mestshortetpercent = '';
	$Mestshortetiverdinavn = '';
	$Mestshortetiverdipercent = '';
	$Dagensvinnernavn = '';
	$Dagensvinnerpercent = '';
	$Dagenstapernavn = '';
	$Dagenstaperpercent = '';
	$total_eksponering = round($total_eksponering,0);
	?>

	<div class="">
		<table class="table table-striped table-hover table-bordered table-sm">
			<tbody>
				<tr">
				<td  style="width:150px" scope="row "><span class="font-weight-bold">Total value:</span></td>
				<td><?php echo number_format($total_eksponering,0,".",",") . ' M ' .  $currency_ticker . '<br>'; ?></td>
			</tr>
			<tr>
				<td scope="row"><span class="font-weight-bold">Value change:</span></td>
				<td><?php 

					$opprinnelig_eksponering = $total_eksponering - $short_gevinst_eller_tap;

					if ($opprinnelig_eksponering != 0) {
						$prosent_endring = (($total_eksponering/$opprinnelig_eksponering)*100)-100;
					}
					else {
						$prosent_endring = 'Ukjent';
					}

					if ($prosent_endring != 'Ukjent') {
						$prosent_endring = round($prosent_endring,2);
						echo number_format($prosent_endring,2,".",",") . ' %';
					}
					else {
						echo $prosent_endring;
  						//$prosent_endring = 4;
					}

					?> (last trading day, for the players)</td>
				</tr>
				<tr>
					<td><span class="font-weight-bold">Active positions:</span></td>
					<td>
						<?php
						if ($grossnumberofpositions > $totalpositioncount) 
						{
							echo $grossnumberofpositions . ' ('. $totalpositioncount . ' with market data)';
						}
						else
						{
							echo $grossnumberofpositions;
						}
						?>
					</td>
				</tr>
				<tr >
					<td><span class="font-weight-bold">Companies shorted:</span></td>
					<td>
						<?php 

						if ($grossnumberofcompanies > $antall_selskaper - 1) 
						{
							$tempantall = $antall_selskaper - 1;
							echo $grossnumberofcompanies. ' ('. $tempantall . ')'; 								
							
						}
						else
						{
							echo $antall_selskaper -1 ; 
						}
						
						?>
					</td>
				</tr>
				<tr >
					<td><span class="font-weight-bold">Active players:</span></td>
					<td>
						<?php 
						if ($grossnumberofplayers > count($playerarray_unik)) 
						{
							echo $grossnumberofplayers . ' (' . count($playerarray_unik) . ')';

						}						
						else
						{
							echo $grossnumberofplayers;
						}
						
						?>
					</td>
				</tr>
				<tr>
					<td><span class="font-weight-bold">Most shorted in %:</span></td>
					<td><?php 
						$singleCompany   = nametolink($name);
						$singleCompany = strtoupper($singleCompany);
						echo '<a href="details_company.php?company=' . $singleCompany  . '&land=' . 
						$land . '">';
						$name   = strtolower($name) ;
						$name   = ucwords($name);
						$Mestshortetnavn = $name;
						$Mestshortetpercent = $target;
						$target = round($target, 2);
						echo $name . ' </a>' . '(' . number_format($target,2,".",",") . ' %)';

						?>
					</td>
				</tr>
				<tr>
					<td><span class="font-weight-bold">By value:</span></td>
					<td>
						<?php 
						$singleCompany   = nametolink($money_target_name);
						$singleCompany = strtoupper($singleCompany);
						echo '<a href="details_company.php?company=' . $singleCompany  . '&land=' . 
						$land . '">';
						$money_target_name  = strtolower($money_target_name) ;
						$money_target_name  = ucwords($money_target_name);
						$Mestshortetiverdinavn = $money_target_name;
						$Mestshortetiverdipercent = $money_target;
						echo $money_target_name;
						$money_target = round($money_target,0);
						echo '</a> (' . number_format($money_target/1000000,0,".",",") . ' M ';
						echo $currency_ticker . ') ';?>	
					</td>
				</tr>
				<tr>
					<td><span class="font-weight-bold">Today's winner:</span></td>
					<td><?php

						$singleCompany   = nametolink($company_array[0]);
						echo '<a href="details.php?player=' . $singleCompany  . '&land=';
						echo $land . '">';

						$comp_name  = strtolower($company_array[0]) ;
						$comp_name  = ucwords($comp_name );
						echo $comp_name;
						$Dagensvinnernavn = $comp_name;
						echo '</a>';
						$Dagensvinnerpercent = $value_array[0]/1000000;
						$Dagensvinnerpercent = round($Dagensvinnerpercent);
						echo ' (+' . number_format($Dagensvinnerpercent,1,".",",") . ' M '; 
						echo $currency_ticker . ') ';?>	      	
					</td>
				</tr>
				<tr>
					<td><span class="font-weight-bold">Today's loser:</span></td>
					<td><?php 
						$counter = count($company_array);
						$comp_name_two = strtolower($company_array[$counter-1]) ;
						$comp_name_two = ucwords($comp_name_two);
						$Dagenstapernavn = $comp_name_two;
						$Dagenstaperpercent = $value_array[$counter-1]/1000000;

						$singleCompany   = nametolink($comp_name_two);
						echo '<a href="details.php?player=' . $singleCompany  . '&land=';
						echo $land . '">';

						echo $comp_name_two;
						echo '</a>';
						$newtarget = round(($value_array[$counter-1]/1000000),1);
						echo ' (' . number_format($value_array[$counter-1]/1000000,1,".",",") . ' M ';
						echo $currency_ticker . ') ';?>	
					</td>
				</tr>
				<tr>
					<td><span class="font-weight-bold">Biggest single bet:</span></td>
					<td><?php 

						$singleCompany = $biggestBetPlayerName;
						$singleCompany   = nametolink($singleCompany);
						echo '<a href="details.php?player=' . $singleCompany  . '&land=';
						echo $land . '">';
						$singleCompany = strtolower($singleCompany);
						$singleCompany = ucwords($singleCompany);
						echo $singleCompany;
						echo '</a>';
						$biggestBetSum = round($biggestBetSum,1);
						echo ' (' . number_format($biggestBetSum,1,".",",") . ' M ';
						echo $currency_ticker . ') ';?>	
					</td>
				</tr>

			</tbody>
		</table>

		
		<div class="d-flex justify-content-between">
			<div class="allpositions">Updated: <?php echo $now; ?></div>
			<div class="allpositions">
				<h6>
					<a href="<?php echo 'details_company_all.php?&land=' . $land; ?>">
						See all active positions
					</a>
				</h6>
			</div>
		</div>
	</div>
	<?php

//  Return the contents of the output buffer
	$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
	ob_end_clean(); 
// Write final string to file

	$filname = '../shorteurope-com.luksus.no/tabell_' . $land . '.html';
	file_put_contents($filname, $htmlStr);


//Gjør klare nøkkeldata for eksport

//header

	$header = array(
		array(
			'Utvikling alle posisjoner (mill.)', 
			'Total shorteksponering', 
			'Endring i prosent', 
			'Antall aktive shortposisjoner', 
			'Antall selskaper shortet', 
			'Antall aktive playere',
			'Mest shortet navn',
			'Mest shortet i %',
			'Mest shortet i verdi navn',
			'Mest shortet i verdi',
			'Dagens vinner navn',
			'Dagens vinner verdi',
			'Dagens taper navn',
			'Dagens taper verdi',
			'Selskaper opp',
			'Selskaper ned',
			'Selskaper flat',
			'Playere opp',
			'Playere ned',
			'Playere flat',
			'Posisjoner opp',
			'Posisjoner ned',
			'Posisjoner flat',
			));

	$file_export = array(
		array(
			$short_gevinst_eller_tap, 
			$total_eksponering, 
			number_format($prosent_endring,2,".","."),
			$grossnumberofpositions,
			$grossnumberofcompanies,
			$grossnumberofplayers,
			$Mestshortetnavn, 
			$Mestshortetpercent, 
			$Mestshortetiverdinavn, 
			$Mestshortetiverdipercent, 
			$Dagensvinnernavn, 
			$Dagensvinnerpercent,
			$Dagenstapernavn,
			$Dagenstaperpercent,
			$upCounter,
			$downCounter,
			$zeroCounter,
			$playerUp,
			$playerDown,
			$playerZero, 
			$positionsUp, 
			$positonsDown,
			$positionsZero, 
			));

//merge header array 
	$file_export = array_merge($header, $file_export);
       //var_dump($file_export);

//var_dump($file_export);

//definere funksjon
	if (!function_exists('saveCSV2')) {

	//Lagre nøkkeldata funksjon
		function saveCSV2($input, $file_location_and_name) {
			$fp = fopen($file_location_and_name, 'w');

			foreach ($input as $fields) {
				fputcsv($fp, $fields);
			}

			fclose($fp);
		}
	}
//$production = 1;
//Lagre only if in production, e.g. done from daily.php

	//echo $production . '<br>';
	saveCSV2($file_export, '../shorteurope-com.luksus.no/datacalc/nokkeltall/' . $land . '/nokkeltall_' . $land . '.'. $date . '.csv');
	echo 'Saved: ' . '../shorteurope-com.luksus.no/datacalc/nokkeltall/' . $land . '/nokkeltall_' . $land . '.'. $date . 
	'.csv<br>';
	saveCSV2($file_export, '../shorteurope-com.luksus.no/datacalc/nokkeltall/' . '/nokkeltall_' . $land . '.current' . '.csv');
	echo 'Saved: ' . '../shorteurope-com.luksus.no/datacalc/nokkeltall/' . '/nokkeltall_' . $land . '.current' . '.csv<br>';


	?>


