<?php

//$land = 'spain';

include '../production_europe/options_set_currency_by_land.php';
$selskapsliste = $land . '/' . $land . '_current.json';
$land2 = $land;
$path2 = '../shorteurope-com.luksus.no/datacalc/nokkeltall/' . $land . '/';

if (!function_exists('getNumbers')) {
function getNumbers($filenameAndUrl) 
{

    $csvFile = file($filenameAndUrl);            
    $data = [];
    foreach ($csvFile as $line) 
    {            
        $data[] = str_getcsv($line);
    } 
    return $data;
    }
}

if (!function_exists('roundDownToAny')) {
    function roundDownToAny($n,$x=5) {
        return (floor($n)%$x === 0) ? floor($n) : round(($n-$x/2)/$x)*$x;
    }

    function roundUpToAny($n,$x=5) {
        return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
    }
}

$files = array_diff(scandir($path2), array('.', '..'));


    //build urls
    $counter = 0;
    $valuesContainer = array();
    $dato = array ();
    foreach ($files as $file) {

	//ta kun 10 filverdier
       if ($counter > 549) {
		//break;
       }

	//build url
       $url = $path2 . $file;
       $row = getNumbers($url);

       $valuesContainer[] = $row;
       $dato[] = substr($file, -14, 10);
       $counter++;        

   }

   $antallAktivePosisjoner = array();
   $antallSelskaper = array();
   $antallPlayere = array();
   $shorteksponering = array();
   $utviklingAllePosjoner = array ();
   $prosentvisEndring = array ();
   $totalUtviklingAllePosisjoner = 0;

//sort out items without base currency
   foreach ($valuesContainer as $value) 
   {
    $antallAktivePosisjoner[] = $value[1][3];
    $antallSelskaper[] = $value[1][4];
    $antallPlayere[] = (int)$value[1][5];
    $temp_shorteksponering = ($value[1][1])/1000;
    $shorteksponering[] = number_format($temp_shorteksponering,3,".","");
    $utviklingAllePosjoner[] = number_format($value[1][0],1,".","");
    $prosentvisEndring[] = $value[1][2];
    $totalUtviklingAllePosisjoner += (int)$value[1][0];
   }

$min = min($shorteksponering)-2;

$min = floor($min * 2)/2;
$min = floor($min);

if ($min < 0) {
    $min = 0;
}

$max = max($shorteksponering);
$max = ceil($max * 2)/2;
$max = ceil($max);
$maxpos = max($antallAktivePosisjoner)+1;

$minvaluearray = array();
$minvaluearray[] = min($antallSelskaper);
$minvaluearray[] = min($antallPlayere);
//var_dump($minvaluearray);

$minvaluePlayere = min($minvaluearray)-4;

if ($minvaluePlayere < 0)
{
    $minvaluePlayere  == 0;
}

//echo $minvaluePlayere;

$maxpos = roundUpToAny($maxpos);
$minvaluePlayere = roundDownToAny($minvaluePlayere);

//check that max and min does not give to many steps
$maxmininterval = 1;

//echo "Maks er $max, og min er $min<br>";

while ($max - $min > $maxmininterval * 10)
{
    //echo 'Maksinterval: ' . $maxmininterval . '<br>';
    $maxmininterval++;
}

// Turn on output buffering
ob_start();

?>

<div class="col-12 col-sm-12 cold-md-7 col-lg-8 pb-3"> 

  <div id="nokkelgraf<?php echo $land; ?>" style="width: 100%;height:400px;"></div>
  <script>
    var antallAktivePosisjoner = <?php echo json_encode($antallAktivePosisjoner); ?>;
    var antallSelskaper = <?php echo json_encode($antallSelskaper); ?>;
    var antallPlayere = [<?php echo '"'.implode('","', $antallPlayere).'"' ?>];
    var dato = <?php echo json_encode($dato); ?>;
    var shorteksponering = <?php echo json_encode($shorteksponering); ?>;
    var utviklingAllePosjoner = <?php echo json_encode($utviklingAllePosjoner); ?>;
    var prosentvisEndring = <?php echo json_encode($prosentvisEndring); ?>;
    var minimum = <?php echo $min; ?>;
    //console.log('Minimum er ' + minimum);
    var maximum = <?php echo $max; ?>;
    var maxmininterval = <?php echo $maxmininterval; ?>;

    var maxpos = <?php echo $maxpos; ?>;
    var minposplayer = <?php echo $minvaluePlayere; ?>;
    var totalUtviklingAllePosisjoner = <?php echo $totalUtviklingAllePosisjoner; ?>;

				//window.alert(prosentvisEndring);

                window.onresize = function() {
                 $("#nokkelgraf<?php echo $land; ?>").each(function(){
                     var id = $(this).attr('_echarts_instance_');
                     window.echarts.getInstanceById(id).resize();
                 });
             };

        // based on prepared DOM, initialize echarts instance
        var myChart5<?php echo $land; ?> = echarts.init(document.getElementById('nokkelgraf<?php echo $land; ?>'));
        // var myChart1 = echarts.init(document.getElementById('5storste'), {renderer: 'svg', devicePixelRatio: '2'});

        var w = window.innerWidth;
        var h = window.innerHeight;

        if (w >= 1200) {
            var left = '15%';
            var right = '6%';
            var headingFontSize = 20;
            var fontSizen = '18';
            var toppen = '70';
        }
        else if (w >= 992 && w <= 1199 ) {
            var left = '15%';
            var right = '6%';
            var headingFontSize = 20;
            var fontSizen = '17';
            var toppen = '70';
        }
        else if (w >= 768) {
            var left = '6%';
            var right = '3%';
            var headingFontSize = 20;
            var fontSizen = '18';
            var toppen = '70';
        }
        else if (w >= 576) {
            var left = '6%';
            var right = '3%';
            var headingFontSize = 20;
            var fontSizen = '14';
            var toppen = '70';
        }
        else if (w >= 508) {
            var left = '6%';
            var right = '3%';
            var headingFontSize = 20;
            var fontSizen = '14';
            var toppen = '70';
        }
        else {
            var left = '15%';
            var right = '6%';
            var headingFontSize = 19;
            var fontSizen = '15';
            var toppen = '90';
        }

        var shortEksponering<?php echo $land; ?> = shorteksponering.slice();
        var shortEksponering5d<?php echo $land; ?> = [];
        var shortEksponering1m<?php echo $land; ?> = [];
        var shortEksponering6m<?php echo $land; ?> = [];

        var aktivePos<?php echo $land; ?> = antallAktivePosisjoner.slice();
        var aktivePos5d<?php echo $land; ?> = [];
        var aktivePos1m<?php echo $land; ?> = [];
        var aktivePos6m<?php echo $land; ?> = [];

        var antallSelskaper<?php echo $land; ?> = antallSelskaper.slice();
        var antallSelskaper5d<?php echo $land; ?> = [];
        var antallSelskaper1m<?php echo $land; ?> = [];
        var antallSelskaper6m<?php echo $land; ?> = [];

        var antallPlayere<?php echo $land; ?> = antallPlayere.slice();
        var antallPlayere5d<?php echo $land; ?> = [];
        var antallPlayere1m<?php echo $land; ?> = [];
        var antallPlayere6m<?php echo $land; ?> = [];

        var dato<?php echo $land; ?> = dato.slice();
        var dato5d<?php echo $land; ?> = [];
        var dato1m<?php echo $land; ?> = [];
        var dato6m<?php echo $land; ?> = [];

        length = dato.length;

        for (i = length-6; i < length; i++) {
         aktivePos5d<?php echo $land; ?>.push(aktivePos<?php echo $land; ?>[i]);
         antallSelskaper5d<?php echo $land; ?>.push(antallSelskaper<?php echo $land; ?>[i]);
         antallPlayere5d<?php echo $land; ?>.push(antallPlayere<?php echo $land; ?>[i]);
         dato5d<?php echo $land; ?>.push(dato<?php echo $land; ?>[i]);
         shortEksponering5d<?php echo $land; ?>.push(shortEksponering<?php echo $land; ?>[i]);


     }

     for (i = length-21; i < length; i++) {
         aktivePos1m<?php echo $land; ?>.push(aktivePos<?php echo $land; ?>[i]);
         antallSelskaper1m<?php echo $land; ?>.push(antallSelskaper<?php echo $land; ?>[i]);
         antallPlayere1m<?php echo $land; ?>.push(antallPlayere<?php echo $land; ?>[i]);
         dato1m<?php echo $land; ?>.push(dato<?php echo $land; ?>[i]);   
         shortEksponering1m<?php echo $land; ?>.push(shortEksponering<?php echo $land; ?>[i]);
     }

     var halfyear;
     if (length < 130) {
        var halfyear = length;

    }
    else {
        var halfyear = 130;
    }

    for (i = length-halfyear; i < length; i++) {
        aktivePos6m<?php echo $land; ?>.push(aktivePos<?php echo $land; ?>[i]);
        antallSelskaper6m<?php echo $land; ?>.push(antallSelskaper<?php echo $land; ?>[i]);
        antallPlayere6m<?php echo $land; ?>.push(antallPlayere<?php echo $land; ?>[i]);
        dato6m<?php echo $land; ?>.push(dato<?php echo $land; ?>[i]);   
        shortEksponering6m<?php echo $land; ?>.push(shortEksponering<?php echo $land; ?>[i]);
    }


//console.log('halfyear');
//console.log(halfyear);


option = {
    toolbox: {
        show : true,
        itemSize: 15,
        feature : {
            mark : {show: false},
            dataZoom : {show: false},
            magicType : {show: false, type: ['line', 'bar']},
            restore : {show: false},
            dataView : {
                show: true,
                title: 'Data',
                lang: ['Short history', 'Exit', 'Refresh'],
                
            },
            saveAsImage : {show: false},
            restore: {show: false},
            myTool10: {
                show: true,
                title: ' ',
                icon: 'image://img/favicon5d.png',
                onclick: function (){
                    var option = myChart5<?php echo $land; ?>.getOption();
                    option.series[0].data = shortEksponering5d<?php echo $land; ?>;
                    option.series[1].data = antallPlayere5d<?php echo $land; ?>;
                    option.series[2].data = antallSelskaper5d<?php echo $land; ?>;
                    option.series[3].data = aktivePos5d<?php echo $land; ?>;
                    option.xAxis[0].data = dato5d<?php echo $land; ?>;
                    myChart5<?php echo $land; ?>.setOption(option);
                }
            },
            myTool11: {
                show: true,
                title: ' ',
                icon: 'image://img/1m.png',
                onclick: function (){
                    var option = myChart5<?php echo $land; ?>.getOption();
                    option.series[0].data = shortEksponering1m<?php echo $land; ?>;
                    option.series[1].data = antallPlayere1m<?php echo $land; ?>;
                    option.series[2].data = antallSelskaper1m<?php echo $land; ?>;
                    option.series[3].data = aktivePos1m<?php echo $land; ?>;
                    option.xAxis[0].data = dato1m<?php echo $land; ?>;
                    myChart5<?php echo $land; ?>.setOption(option);
                }
            },
            myTool12: {
                show: true,
                title: ' ',
                icon: 'image://img/6m.png',
                onclick: function (){
                    var option = myChart5<?php echo $land; ?>.getOption();
                    option.series[0].data = shortEksponering6m<?php echo $land; ?>;
                    option.series[1].data = antallPlayere6m<?php echo $land; ?>;
                    option.series[2].data = antallSelskaper6m<?php echo $land; ?>;
                    option.series[3].data = aktivePos6m<?php echo $land; ?>;
                    option.xAxis[0].data = dato6m<?php echo $land; ?>;
                    myChart5<?php echo $land; ?>.setOption(option);
                }
            },
            myTool13: {
                show: true,
                title: ' ',
                icon: 'image://img/reset.png',
                onclick: function (){
                    var option = myChart5<?php echo $land; ?>.getOption();
                    option.series[0].data = shortEksponering<?php echo $land; ?>;
                    option.series[1].data = antallPlayere<?php echo $land; ?>;
                    option.series[2].data = antallSelskaper<?php echo $land; ?>;
                    option.series[3].data = aktivePos<?php echo $land; ?>;
                    option.xAxis[0].data = dato<?php echo $land; ?>;
                    myChart5<?php echo $land; ?>.setOption(option);
                }
            },

        },
    },
    textStyle: {
        fontFamily: ['Encode Sans Condensed','sans-serif'],
    },
    grid: {
        top:  toppen,
        bottom: 90,
        left:   35,
        right:  50,
    },
    animation: false,
    backgroundColor: '#f6f6f6',
    title: {
        //backgroundColor: '#000000',
        left: 'left',
        padding: [10,10,10,10],
        itemGap: 100,
        text: 'Short history <?php echo ucwords($land2); ?>',
        textStyle: {
            color: '#000000',
            fontSize: headingFontSize,
            width: 400,
        },
        subtextStyle: {
            height: 40,
        },
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            crossStyle: {
                color: '#999'
            }
        },
        backgroundColor: '#000000',
        confine: true,
        //formatter: '{a}, {b}, {c}',
    },

    legend: {
        top: 36,
        textStyle: {
            fontSize: fontSizen,
        },
    },
    xAxis: [
    {
        type: 'category',
        data: dato,
        axisPointer: {
            type: 'shadow'
        }
    }
    ],
    yAxis: [
    {
        type: 'value',
        name: '#',
        min: minposplayer,
        max: maxpos,
        interval: 20,
        axisLabel: {
            formatter: '{value}'
        },
        nameTextStyle: {
           fontSize: 16,
           padding: [15,0,100,0],
           align: 'right',

       },
       nameRotate: '0',
       nameLocation: 'start',

   },
   {
    type: 'value',
    name: 'Value in billion <?php echo $currency_ticker; ?>',
    min: minimum,
    max: maximum,
    interval: maxmininterval,
    axisLabel: {
        formatter: '{value} bn.',

    },
    nameTextStyle: {
       fontSize: 16,
       padding: [15,100,0,0],
       align: 'right',

   },
            //nameRotate: '90',
            nameLocation: 'start',
            //nameGap: 25,
        }
        ],
        dataZoom: [
        {
            show: true,
            type: 'slider',
            bottom: 3,
            start: 0,
            end: 100,
            handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
            handleSize: '105%',
            handleStyle: {
              color: 'black',
          },
      }
      ],        
      series: [{
        name:'Market value',
        type:'bar',
        label: {
        },
        data:shorteksponering,
        symbol: 'none',
        yAxisIndex: 1,
        tooltip: {
            backgroundColor: '#00000',
            formatter: '{b}:<br />M <br /> Position holder:<br />',
        }
    },

    {
        name:'Players',
        type:'line',
        yAxisIndex: 0,
        data: antallPlayere,
        symbol: 'none',
        lineStyle: {
         width: 5, 
     }
 },
 {
    name:'Companies',
    type:'line',
    yAxisIndex: 0,
    data: antallSelskaper,
    symbol: 'none',
    lineStyle: {
     width: 5, 
 }
},        
{
    name:'Positions',
    type:'line',
    yAxisIndex: 0,
    symbol: 'none',
    data: antallAktivePosisjoner,
    lineStyle: {
     width: 5, 
 }
},
]
};

       // use configuration item and data specified to show chart
       myChart5<?php echo $land; ?>.setOption(option);
       window.addEventListener('resize', function(event){
          myChart5<?php echo $land; ?>.resize();

      });

  </script>
</div>


<?php

$filenameStart = '../shorteurope-com.luksus.no/nokkeltall_venstre_';
$filenameEnd = '.html';
$filename = $filenameStart . $land2 . $filenameEnd;

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename , $htmlStr);

ob_start();
?>



<div class="col-12 col-sm-12 col-md-12 col-lg-4"> 

  <div id="nokkelhoyre<?php echo $land; ?>" style="width: 100%;height:400px;"></div>
  <script>
    var datoene = <?php echo json_encode($dato); ?>;
    var utviklingAllePosjoner<?php echo $land; ?> = <?php echo json_encode($utviklingAllePosjoner); ?>;
    var prosentvisEndring = <?php echo json_encode($prosentvisEndring); ?>;
    var index;
    var subtext  = 'Total sum in the period: ' + totalUtviklingAllePosisjoner + 'M <?php echo $currency_ticker;?>' ;
    var test;
    var item =  []; 
    var a = utviklingAllePosjoner<?php echo $land; ?>;
    for (index = 0; index <= a.length; ++index) {
        if (a[index] >= 0 ) {
            item[index] = { value: a[index], name: '', 
            itemStyle: { color: '#065b1c', }};
                        //console.log(index);
                    }
                    else {
                        item[index] = {value: a[index], name: '', 
                        itemStyle: { color: '#c41315', }};
                        //console.log(index + 'Index negativ');
                    }
                };

        // based on prepared DOM, initialize echarts instance
        var myChart6<?php echo $land; ?> = echarts.init(document.getElementById('nokkelhoyre<?php echo $land; ?>'));
        // var myChart1 = echarts.init(document.getElementById('5storste'), {renderer: 'svg', devicePixelRatio: '2'});

        var w = window.innerWidth;
        var h = window.innerHeight;

        if (w >= 1200) {
            var left = '17%';
            var right = '6%';
            var headingFontSize = 18;
        }
        else if (w >= 992 && w <= 1199 ) {
            var left = '17%';
            var right = '6%';
            var headingFontSize = 16;
        }
        else if (w >= 768) {
            var left = '7%';
            var right = '3%';
            var headingFontSize = 18;
        }
        else {
            var left = '15%';
            var right = '7%';
            var headingFontSize = 18;
        }


        var date5<?php echo $land; ?> = [];
        var theDates5<?php echo $land; ?> = datoene.slice();

        var item5<?php echo $land; ?> = [];
        var colorItem5<?php echo $land; ?> = item.slice();

        var date1m<?php echo $land; ?> = [];
        var theDates1m<?php echo $land; ?> = datoene.slice();

        var item1m<?php echo $land; ?> = [];
        var colorItem1m<?php echo $land; ?> = item.slice();

        var date1m<?php echo $land; ?> = [];
        var theDates1m<?php echo $land; ?> = datoene.slice();

        var itemx<?php echo $land; ?> = item.slice();
        var datox<?php echo $land; ?> = dato.slice();


        length = item.length;

        for (i = length-6; i < length-1; i++) {
            item5<?php echo $land; ?>.push(colorItem5<?php echo $land; ?>[i]);
            date5<?php echo $land; ?>.push(theDates5<?php echo $land; ?>[i]);
        }

        for (i = length-21; i < length-1; i++) {
            item1m<?php echo $land; ?>.push(colorItem5<?php echo $land; ?>[i]);
            date1m<?php echo $land; ?>.push(theDates5<?php echo $land; ?>[i]);
        }

        var itemx6m<?php echo $land; ?> = [];
        var itemx6m<?php echo $land; ?> = [];

        var item6m<?php echo $land; ?> = item.slice();
        var date6m<?php echo $land; ?> = dato.slice();

        var halfyear;
        if (length < 130) {
            var halfyear = length;

        }
        else {
            var halfyear = 130;
        }

        var item6m<?php echo $land; ?> = [];
        var colorItem6m<?php echo $land; ?> = item.slice();

        var date6m<?php echo $land; ?> = [];
        var theDates6m<?php echo $land; ?> = datoene.slice();

        for (i = length-halfyear; i < length-1; i++) {
            item6m<?php echo $land; ?>.push(colorItem6m<?php echo $land; ?>[i]);
            date6m<?php echo $land; ?>.push(theDates6m<?php echo $land; ?>[i]);
        }


//console.log('halfyear');
//console.log(halfyear);

//console.log('item5');
//console.log(item5<?php echo $land; ?>);
//console.log('date5');
//console.log(date5<?php echo $land; ?>);

option = {
    backgroundColor: 'white',
    toolbox: {
        show : true,
        itemSize: 15,
        feature : {
            dataView : {
                show: true,
                title: 'Data',
                lang: ['Data earnings/loss', 'Exit', 'Refresh'],
            },
            mark : {show: false},
            dataZoom : {show: false},
            magicType : {show: false, type: ['line', 'bar']},
            restore : {show: false},
            saveAsImage : {show: false},
            restore: {show: false},
            myTool2: {
                show: true,
                title: ' ',
                icon: 'image://img/favicon5d.png',
                onclick: function (){
                    var option = myChart6<?php echo $land; ?>.getOption();
                        //option.backgroundColor =  'gray';
                        option.series[0].data = item5<?php echo $land; ?>;
                        option.xAxis[0].data = date5<?php echo $land; ?>;
                        myChart6<?php echo $land; ?>.setOption(option);
                    }
                },
                myTool3: {
                    show: true,
                    title: ' ',
                    icon: 'image://img/1m.png',
                    onclick: function (){
                        var option = myChart6<?php echo $land; ?>.getOption();
                        //option.backgroundColor =  'white';
                        option.series[0].data = item1m<?php echo $land; ?>;
                        option.xAxis[0].data = date1m<?php echo $land; ?>;
                        myChart6<?php echo $land; ?>.setOption(option);
                    }
                },
                myTool5: {
                    show: true,
                    title: ' ',
                    icon: 'image://img/6m.png',
                    onclick: function (){
                        var option = myChart6<?php echo $land; ?>.getOption();
                        //option.backgroundColor =  'white';
                        option.series[0].data = item6m<?php echo $land; ?>;
                        option.xAxis[0].data = date6m<?php echo $land; ?>;
                        myChart6<?php echo $land; ?>.setOption(option);
                    }
                },
                myTool4: {
                    show: true,
                    title: ' ',
                    icon: 'image://img/reset.png',
                    onclick: function (){
                        var option = myChart6<?php echo $land; ?>.getOption();
                        //option.backgroundColor =  'white';
                        option.series[0].data = itemx<?php echo $land; ?>;
                        option.xAxis[0].data = datox<?php echo $land; ?>;
                        myChart6<?php echo $land; ?>.setOption(option);
                    }
                },

            },
        },

        textStyle: {
            fontFamily: ['Encode Sans Condensed','sans-serif'],
        },
        grid: {
            top:    70,
            bottom: 90,
            left:   left,
            right:  right,
        },
        animation: false,
        backgroundColor: '#f6f6f6',
        title: {
        //backgroundColor: '#000000',
        left: 'left',
        padding: [10,10,10,10],
        text: 'Earnings/loss',
        subtext: subtext,
        textStyle: {
            color: '#000000',
            fontSize: headingFontSize,
            width: 400,
        },
        subtextStyle: {
            fontSize: 14,
            color: '#000000',
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            crossStyle: {
                color: '#999'
            }
        },
        backgroundColor: '#000000',
        confine: true,
        formatter: '{a}<br> Date: {b} <br>{c}M ',
    },
    
    xAxis: [
    {
        name: 'Date (MM.DD)',
        type: 'category',
        data: dato,
        axisPointer: {
            type: 'shadow'
        },
        nameTextStyle: {
            fontSize: 14,
            padding: [15,0,100,0],
            align: 'bottom',

        },
        nameRotate: '0',
        nameLocation: 'middle',
    }
    ],
    yAxis: [
    {
        type: 'value',           
        nameTextStyle: {
            fontSize: 16,
            padding: [16,0,100,0],
            align: 'right',
        },
        nameRotate: '0',
        nameLocation: 'start', 
        axisLabel: {
            formatter: '{value}M',

        },
    },
    ],
    dataZoom: [
    {
        show: true,
        type: 'slider',
        bottom: 3,
        start: 0,
        end: 100,
        handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
        handleSize: '105%',
        handleStyle: {
          color: 'black',
      },
  }
  ],    
  series: [
  {
    name:'Earnings/loss: Numbers in million <?php echo $currency_ticker;?>',
    type:'bar',
    data: item, 
},
],
};

       // use configuration item and data specified to show chart
       myChart6<?php echo $land; ?>.setOption(option);
       window.addEventListener('resize', function(event){

          myChart6<?php echo $land; ?>.resize();

      });
  </script>

  <?php 
  $filenameStart = '../shorteurope-com.luksus.no/nokkeltall_hoyre_';
  $filenameEnd = '.html';
  $filename = $filenameStart . $land2 . $filenameEnd;

//  Return the contents of the output buffer
  $htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
  ob_end_clean(); 
// Write final string to file
  file_put_contents($filename , $htmlStr);
  ?>