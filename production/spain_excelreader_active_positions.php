<?php

//delete contents of file 
$saveurl = '../shorteurope-com.luksus.no/dataraw/spain/spain_current.csv';

include_once '../production_europe/functions.php';
require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');

//$writer = new Xlsx($spreadsheet);
//$writer->save('hello world.xlsx');


$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("../shorteurope-com.luksus.no/dataraw/spain/spain_active_and_historical_download.xls");

$sheetnames = $spreadsheet->getSheetNames();
//var_dump($sheetnames);

//$spreadsheet->setActiveSheetIndex(0);

$dataArray = $spreadsheet->getActiveSheet()
    ->ToArray(
           // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
    );

//var_dump($dataArray);

$success = 0;
if (!isset($dataArray[8]['B'])) {
	echo 'Data is not set! Download of active positions not working!';
	logger('Klarte ikke laste ned spanske active posisjoner!', '');
	return;
}
else {
	$success = 1;
}

$holderBox = [];

$counter = 0;
if ($success = 1) {
	foreach ($dataArray as $key => $Row) {

			if ($counter > 3 and isset($Row['B'])) {

			$rad2 = rtrim($Row['B']);
						
			//fjerne space i isin
			$rad3 = $Row['C'];
			//$rad4 = $Row[3];
			
			//formater bort kommaer
			$rad4 = (string)$Row['D'];

			$originalDate = "$rad4";
			$rad4  = date("Y-m-d", strtotime($originalDate));

			//$rad5 = $Row[4];
			$rad5 = (string)$Row['E'];
				
			$lagre_array = array($rad3, $rad2, '', $rad5, $rad4);

	    	
			$holderBox[] = $lagre_array;
			}
			//echo $counter . ' ' ;

			$counter++;
	}
}


//var_dump($holderBox);

//ta alle selskapsnavnene
$companyArray = [];
foreach ($holderBox as $key => $row) {
	$companyArray[] = $row[0]; 
}

//gjør unike
$companyArray = array_unique($companyArray);

//ta et selskapsnavn

$activePositionsBox = [];

foreach ($companyArray as $key => $company) {
	
	//samle inn posisjoner
	echo '<br>Main round for company: ' . $company . '<br>';
	$posisjonsBox = [];
	$playerNameBox = [];

	foreach ($holderBox as $key => $row) {
		if ($company == $row[0]) {
			$posisjonsBox[] = $row;
			$playerNameBox[] = $row[1];
		}
	}

	//var_dump($posisjonsBox);

	$playerNameBox = array_unique($playerNameBox);

	$count = 1;
	//ta hvert navn for hvert selskap
	foreach ($playerNameBox as $key => $playername) {
		//echo 'Looking for ' . $playername . '<br>';
		foreach ($posisjonsBox as $row) {
			if ($playername == $row[1]) {

				echo $count++ . '. Found playername: ' . $playername . '<br>';

				$activePositionsBox[] = $row;
				break;
			}
		}

	}
}


saveCSVx($activePositionsBox, $saveurl);



?>