<?php

//$land = 'norway';

include_once '../production_europe/logger.php';
include_once '../production_europe/functions.php';

set_time_limit(30);
date_default_timezone_set("Europe/Oslo");

//$land = 'norway';

switch ($land) {
  case "norway":
  $stockExchangeNameFormal = 'Norwegian stock exchange';
  $nationalName = 'Norwegian';
  $stockExchange = '../production_europe/json/ticker/osebx.ol.json';
  $stockExchangeName = 'Oslo Stock Exchange';
  break;  
  case "sweden":
  $stockExchange = '../production_europe/json/ticker/omxspi.indx.json';
  $stockExchangeName = 'Nasdaq Stockholm';
  $stockExchangeNameFormal = 'Nasdaq Stockholm';
  $nationalName = 'Swedish';
  break;
  case "denmark":
  $stockExchange = '../production_europe/json/ticker/omxc20.indx.json';
  $stockExchangeName = 'Nasdaq Copenhagen';
  $stockExchangeNameFormal = 'Nasdaq Copenhagen';
  $nationalName = 'Danish';
  break;
  case "finland":
  $stockExchange = '../production_europe/json/ticker/omxh25.indx.json';
  $stockExchangeName = 'Nasdaq Helsinki';
  $stockExchangeNameFormal = 'Nasdaq Helsinki';
  $nationalName = 'Finnish';
  break;
  case "germany":
  $stockExchange = '../production_europe/json/ticker/gdaxi.indx.json';
  $stockExchangeName = 'DAX';
  $stockExchangeNameFormal = 'DAX';
  $nationalName = 'German';
  break;
  case "france":
  $stockExchange =  '../production_europe/json/ticker/fchi.indx.json';
  $stockExchangeName = 'CAC 40';
  $stockExchangeNameFormal = 'CAC 40 index';
  $nationalName = 'French';
  break;      
  case "spain":
  $stockExchange = '../production_europe/json/ticker/ibex.indx.json';
  $stockExchangeName = 'IBEX 35';
  $stockExchangeNameFormal = 'IBEX 35 index';
  $nationalName = 'Spanish';
  break;
  case "united_kingdom":
  $stockExchange = '../production_europe/json/ticker/ftse.indx.json';
  $stockExchangeName = 'FTSE 100';
  $stockExchangeNameFormal = 'FTSE 100 index';
  $nationalName = 'British';
  break; 
  case "italy":
  $stockExchange = '../production_europe/json/ticker/spmib.indx.json';
  $stockExchangeName = 'FTSE MIB';
  $stockExchangeNameFormal = 'FTSE MIB index';
  $nationalName = 'Italian';
  break;                              
}



$intrdayfolder = '../production_europe/intraday/';
//download tickers 

$errorbox = [];
$databox = [];

$data = readJSON($stockExchange);

var_dump($data);

if (!isset($data['close']) or $data['close'] == 0)
{
	$errorbox[] = '$data["close"] er ikke satt i' . $ticker . '. Setting value to zero<br>';
	$data['close'] = 0;
}

if (!isset($data['date']))
{
	echo 'Setting timestamp to ' . date('H:i:s') . '<br>'; 
	$data['date'] = date('H:i:s');
}

$filename = $intrdayfolder . $land . '.json';

//read the current data 
if ($filedata = readJSON($filename))
{

  if (count($filedata) < 1)
  {
    echo 'error! count';
    return;
  }

  $filedata[] = $data;

  $count = count($filedata);
  echo 'Count is ' . $count . '.<br>';

	//$filedata = array_reverse($filedata);
  $temparray = [];

  for ($x = 0; $x < $count; $x++)
  {
    if ($filedata[$x]['date'] == (string)date('Y-m-d'))
    {
      $temparray2[] = $filedata[$x];
    }
  }
  
  if (!isset($temparray2))
  {
    errorecho('Dato er ikke riktig, returning...<br>');
    return;
  }
	//$temparray = array_reverse($temparray);
  $filedata = $temparray2;

  var_dump($filedata);

  //check if there are multiple previous close

  $previousBox = [];
  foreach ($filedata as $entry)
  {
    $previousBox[] = $entry['previousClose'];
  }

  $previousBox = array_unique($previousBox);

  if (count($previousBox) > 1)
  {
    echo 'More than one previous close<br>';
    $count = count($filedata);

    echo 'Count er ' . $count . '. ';

    $target = $filedata[$count-1]['previousClose'];

    echo 'Target er ' . $target . '<br>';

    for ($i = 0; $i < $count; $i++)
    {
      if ($target != $filedata[$i]['previousClose'])
      {
        echo 'Unsetting ' . $i . '<br>';
        unset($filedata[$i]);
      }
    }

    $filedata = array_values($filedata);

  }
  saveJSON($filedata, $filename);
  echo 'Wrote to existing file ' . $filename . '<br>';

}
else 
{
	echo 'Error reading json file, will make new.' . $filename . '<br>';
	$filedata[] = $data;
	saveJSON($filedata, $filename);
	echo 'Wrote to new file file ' . $filename . '<br>';
}



echo '<br><br>';

//now make data for the shortdata, use same timestamp
//build file with shorting results

//Read in the current change
//last inn shortlisten med aksjekurser
if (!$shortingdata = readCSVkomma('../shorteurope-com.luksus.no/datacalc/shortlisten_' . $land . '_current.csv'))
{
  echo 'Read error in ' . '../shorteurope-com.luksus.no/datacalc/shortlisten_' . $land . '_current.csv' . '. Returning';
  var_dump($shortingdata);
  return;
}

$totalMarketValueChange = 0;
$totalMarketValuePrevious = 0;

foreach ($shortingdata as $key => $selskap) {
  if ($key == 0) {
    continue;
  }
  
  $totalMarketValueChange += (float)$selskap[1];
  
  //echo 'test------------------<br>';
  //echo $selskap[4];
  //echo '<br>';
  //echo $selskap[6];
  //echo '<br>';
  //echo $selskap[7];
  //echo '<br>';

  $totalMarketValuePrevious += (float)$selskap[4] * ((float)$selskap[6] + (float)$selskap[7]);
}

$totalMarketValueChangePercent = round(($totalMarketValueChange/$totalMarketValuePrevious)*100,2);
$shortdatafilename = '../production_europe/intraday/' . $land . '_intraday.json';

$object = new stdClass;
$object->close = $data['close'];
$object->previousClose = $data['previousClose'];
$object->indexPercentChange = (($data['close']/$data['previousClose'])-1)*100;
$object->indexPercentChange = round($object->indexPercentChange,4);
$object->date = $data['date'];
$object->time = date('H:i:s');
$object->positionsPercentChange = $totalMarketValueChangePercent;
$object->positionsValueChange = $totalMarketValueChange;
$temparray = [];

echo '<br>------------------------<br>';

var_dump($object);

if (!$shortdataexisting = readJSON($shortdatafilename))
{
  echo 'Read error. Will make new file.';
  $temparray[] = $object;
  saveJSON($temparray, $shortdatafilename);
}
else 
{
  $temparray = [];
  foreach ($shortdataexisting as $entry)
  {

    if ($entry['date'] == date('Y-m-d'))
    {
     $temparray[] = $entry;
   }
   
 }

 //check if last value is equal to what will be added now

 $count = count($temparray);

 if ($count > 1)
 {
  $lastentry = $temparray[$count-1];
}
else
{
  $lastentry['test'] = '';
}


$tempoarray = (array)$object;

  //setting timestamp to 1 because of different ways of setting it in finnhub or yahoo.

$lastentry['time'] = 1;
$tempoarray['time'] = 1;

$array_diff = array_diff($lastentry, $tempoarray);

if (count($array_diff) > 0)
{
 $temparray[] = (array)$object;

  	//sort data by date and time;

 usort($temparray, function($a, $b) {
  return [$a['date'], $a['time']]
  <=>
  [$b['date'], $b['time']];
});

    //$temparrayCount = count($temparray);
    //unset($temparray[$temparrayCount-1]);

    //$temparray = array_values($temparray);

 echo 'Wrote into existing file' . $shortdatafilename . '<br>';
 saveJSON($temparray, $shortdatafilename);
}
else
{
 echo '<br>Notice: Skipping adding new values because they are duplicate of the last. Will not write to ' . $shortdatafilename . ' <br>';
 var_dump($array_diff);
 var_dump($lastentry);
 var_dump($tempoarray);
}

}

loggerarray($errorbox, 'intraday_downloader.php');
echo '<br>intraday_download.php complete for ' . $land . '<br>';


?>