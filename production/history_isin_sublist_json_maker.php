<?php

echo '<br>';
set_time_limit(19000);

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('Europe/Oslo');

include('../production_europe/logger.php');
require '../production_europe/functions.php';

$countries = get_countries();

//add history folders
foreach ($countries as $land)
{
	$countries[] = $land.'.history';
}

$filenames = [];
$baseurl = '../shorteurope-com.luksus.no/dataraw/';

foreach ($countries as $folder)
{
	$filenames[] = $baseurl . $folder . '/' . $folder . '_current.json';

}

foreach ($filenames as $file)
{
	//check if exists
	if (!file_exists($file))
	{
		errorecho('File ' . $file . ' does not exist<br>');
	}	
}

$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

$isincount = count($name_list);

echo 'In isin list: ' . $isincount . ' number of stocks <br>';

$keepBox = [];
//take each file and scan for companies and isin

foreach ($filenames as $key => $filename)
{

	echo $key . '. ' . $filename . '<br>';

	if (!$jsondata = readJSON($filename))
	{
		errorecho('Error reading filename');
	}

	$count = count($jsondata);

	for ($i = 0; $i < $count; $i++)
	{

		$isintarget = $jsondata[$i]['ISIN'];
		$nametarget = strtolower($jsondata[$i]['Name']);

		$success = 0;

		echo '--> ' . $i . '. ' . $nametarget . ' ';

		if ($row = binary_search_multiple_hits($isintarget, $nametarget, $isin_list, $name_list, $land))
		{
			$keepBox[] = $row;
			$success = 1;
		}

		if ($success == 0)
		{
			errorecho(' | Not found');
		}
		else
		{
			successecho(' | found');
		}

		echo '<br>';

	}

echo '<br>';
echo '<br>';

}

$savepath = '../shorteurope-com.luksus.no/dataraw/isin/history_sublist.csv';

saveCSVsemikolon($keepBox, $savepath);


?>