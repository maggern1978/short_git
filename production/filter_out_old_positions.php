<?php

include '../production_europe/functions.php';

$inputFileName = '../shorteurope-com.luksus.no/dataraw/'. $land . '/' . $land . '_current.json';

if (!$data = readJSON($inputFileName))
{
	errorecho ('Read error of til ' . $inputFileName . '<br>');
}

$count = count($data);
$lastdate = date('Y-m-d', strtotime("-4 years"));


echo '<br>';
echo 'Inputfile is ' . $inputFileName . '<br>';
echo 'Searching for positions older than four years (' . $lastdate . ')<br>';

$unsettercounter = 0;
$total_count = 0; 

for ($i = 0; $i < $count; $i++)
{

	$posCount = count($data[$i]['Positions']);

	for ($x = 0; $x < $posCount; $x++)
	{
		if ($data[$i]['Positions'][$x]['ShortingDate'] < $lastdate)
		{
			echo $i . '. ' . $data[$i]['Name'] . ' Unsetting round: ' . $i . ' | Pos. no. ' . $x . '. Date is ' . $data[$i]['Positions'][$x]['ShortingDate']  . '<br>';
			unset($data[$i]['Positions'][$x]);
			$unsettercounter++;
		}
	}

	$data[$i]['Positions'] = array_values($data[$i]['Positions']);

	//slett selskap hvis null selskaper igjen. 
	$posisjonsCount = count($data[$i]['Positions']);
	$total_count += $posisjonsCount;

	if ($posisjonsCount == 0) 
	{
		echo $i . '. ' . $data[$i]['Name'] . ' Zero positions, unsetting company.<br>';
		unset($data[$i]);
		continue;
	}

}

$data = array_values($data);

echo 'Posisjoner ut: ' . $total_count . ' <br>';
successecho($unsettercounter . ' positions removed because of too old.<br>');

saveJSON($data, $inputFileName);

?>