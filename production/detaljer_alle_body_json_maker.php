<?php

require('../production_europe/functions.php');
require('../production_europe/namelink.php');

//$land = 'norway';
//$land = 'sweden';
//$land = 'united_kingdom';
//$land = 'germany';

require_once '../production_europe/options_set_currency_by_land.php';
$shortliste = '../shorteurope-com.luksus.no/datacalc/shortlisten_' . $land . '_current.csv';
$fil = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';

$graphdatafolder = '../production_europe/history/';

$string = file_get_contents($fil);

if (!$json = json_decode($string, true, 512, JSON_UNESCAPED_UNICODE))
{
	errorecho('Read or json_decode error<br>');
}

//var_dump($json);

$total_verdiendring = 0;
$total_eksponering = 0;
$total_shortandel = 0;
$total_positionValueChangeSinceStart = 0;
$counter = 1;

//last inn shortlisten med aksjekurser
//$shortliste = 'datacalc/shortlisten_nor_current.csv';
$csvFile = file($shortliste);            
$shortliste = [];

$number = 0;
foreach ($csvFile as $key => $line) {
	$shortliste[] = $data[] =  str_getcsv($line); 
	$number = $key;
};
//var_dump($shortliste[0]);
//var_dump($shortliste[21]);

$count = count($shortliste);

//read date from generated file to set date
$date = date("Y.m.d");

$player_array = array();
$positionCount = 0;

foreach ($json as $key => $selskap) {

	//echo 'Selskap: ' . $selskap['Name'] . '<br>';

	foreach ($selskap['Positions'] as $index => $posisjon) {
		//var_dump($posisjon);
		if (isset($posisjon["isActive"]) and $posisjon["isActive"] == 'yes') {
			//echo 'Found ';
			$player_array[] = $posisjon["PositionHolder"];
			$positionCount++;
		}
		else
		{
			unset($json[$key]['Positions'][$index]);
			//echo '|';
		}
	}
	$json[$key]['Positions'] = array_values($json[$key]['Positions']);

	//echo '<br>';
}

//sort out zero positions
foreach ($json as $key => $selskap) {

	//echo 'Selskap: ' . $selskap['Name'] . '<br>';
	if ($selskap['ShortPercent'] == 0)
	{
		unset($json[$key]);
		continue;
	}

	$count = count($selskap['Positions']);

	if ($count > 0)
	{
		//echo 'Keeping ' . $json[$key]['Name'] . '<br>';
		//var_dump($selskap);
	}
	else
	{
		//echo 'Nullcount, unsetting...' . $json[$key]['Name'] . '<br>';
		unset($json[$key]);
	}
}

$json = array_values($json);

//echo '<br>Filling in updated stock and isin info<br>';
//fill inn stock info
foreach ($json as $key => $selskap) {

	//echo $key . '. ' . $selskap['Name'] . '';
	$targetname = $selskap['Name'];
	$targetisin = $selskap['ISIN'];
	$ticker = '';
	$stockPrice = '';
	$stockPriceChange = '';

	$success = 0;

	foreach ($shortliste as $index => $row)
	{
		if (strtolower($targetname) == strtolower($row[0]) or strtolower($targetisin) == strtolower($row[3]))
		{
			$json[$key]['ticker'] = $row[2];
			$json[$key]['stockPrice'] = $row[6];
			$json[$key]['stockPriceChange'] = $row[7];
			$json[$key]['currency'] = $row[12];
			$json[$key]['basecurrency'] = $row[13];
			$json[$key]['base_currency_multiply_factor'] = $row[15];

			$success = 1;
			break;
		}

	}

	if ($success == 0)
	{
		echo $key . '. ' . $selskap['Name'] . '';
		echo 'Name not found in shortlisten_country_current.csv, unsetting!<br>';
		unset($json[$key]);
		
	}
	else
	{
		//successecho(' ok ');
	}
	//echo ' <br>';
}

$json = array_values($json);

//legg inn kursdata i posisjonene før de blir delt opp på playere

$jsoncount = count($json);

for ($i = 0; $i < $jsoncount; $i++)
{

	$positionscount = count($json[$i]['Positions']);
	//echo $i . '. ' . $positionscount;

	for ($x = 0; $x < $positionscount; $x++)
	{
		$json[$i]['Positions'][$x]['companyName'] = $json[$i]['Positions'][$x]['Name'];
		$json[$i]['Positions'][$x]['ISIN'] = $json[$i]['ISIN'];
		$json[$i]['Positions'][$x]['stockPrice'] = $json[$i]['stockPrice'];
		$json[$i]['Positions'][$x]['ticker'] = $json[$i]['ticker'];
		$json[$i]['Positions'][$x]['stockPriceChange'] = $json[$i]['stockPriceChange'];
		$json[$i]['Positions'][$x]['shortPercent'] = $json[$i]['Positions'][$x]['ShortPercent'];
		$json[$i]['Positions'][$x]['numberOfStocks'] = $json[$i]['Positions'][$x]['NetShortPosition'];
		$json[$i]['Positions'][$x]['positionStartDate'] = date("Y-m-d", strtotime( $json[$i]['Positions'][$x]['ShortingDate'])); 
		$json[$i]['Positions'][$x]['marketValue'] = (float)$json[$i]['Positions'][$x]['numberOfStocks'] * (float)$json[$i]['Positions'][$x]['stockPrice'];
		$json[$i]['Positions'][$x]['marketValue']= $json[$i]['Positions'][$x]['marketValue']/1000000;
		$json[$i]['Positions'][$x]['marketValueBaseCurrency'] = $json[$i]['Positions'][$x]['marketValue'] * $json[$i]['base_currency_multiply_factor'];
		$json[$i]['Positions'][$x]['currency'] = $json[$i]['currency'];
		$json[$i]['Positions'][$x]['basecurrency'] = $json[$i]['basecurrency'];
		$json[$i]['Positions'][$x]['base_currency_multiply_factor'] = $json[$i]['base_currency_multiply_factor'];

		//var_dump($json[$i]['Positions'][$x]);

		//percent
		$previousStockPrice = (float)$json[$i]['Positions'][$x]['stockPrice'] - (float)$json[$i]['Positions'][$x]['stockPriceChange'];

		if ($previousStockPrice == 0)
		{
			$json[$i]['Positions'][$x]['valueChangePercent'] = '-';
		}
		else
		{
			$json[$i]['Positions'][$x]['valueChangePercent'] = (($json[$i]['Positions'][$x]['stockPrice']/$previousStockPrice)-1)*100;
		}
		
		$json[$i]['Positions'][$x]['positionValueChange'] = (float)$json[$i]['Positions'][$x]['numberOfStocks'] *  (float)$json[$i]['Positions'][$x]['stockPriceChange'] * -1;
		$json[$i]['Positions'][$x]['positionValueChange'] = $json[$i]['Positions'][$x]['positionValueChange']/1000000;

		
		//$json[$i]['Positions'][$x]['stockPrice'] = 
		//$json[$i]['Positions'][$x]['stockPrice'] =
		//$json[$i]['Positions'][$x]['stockPrice'] =

		unset($json[$i]['Positions'][$x]['Id']);
		unset($json[$i]['Positions'][$x]['EntityId']);
		unset($json[$i]['Positions'][$x]['ShortingDate']);
		unset($json[$i]['Positions'][$x]['ShortPercent']);
		//unset($json[$i]['Positions'][$x]['ISIN']);
		unset($json[$i]['Positions'][$x]['Status']);
		unset($json[$i]['Positions'][$x]['NetShortPosition']);
		unset($json[$i]['Positions'][$x]['Name']);
		unset($json[$i]['Positions'][$x]['isActive']);

	}

	$json[$i]['positions'] = array_values($json[$i]['Positions']);

	unset($json[$i]['Positions']);

	//echo '<br>';
	//var_dump($json[$i]['positions']);

}

//var_dump(($json[0]['positions']));

//add inn graph start data
echo '<br><br>Adding in historic data, positionStartDate<br>';

for ($i = 0; $i < $jsoncount; $i++)
{

	echo $i . '. ' . $json[$i]['Name'] . '. ';
	$positionscount = count($json[$i]['positions']);
	//echo $positionscount;
	//echo '<br>';

	for ($x = 0; $x < $positionscount; $x++)
	{
		//echo '-> ' . $x . '. ';
		$ticker = $graphdatafolder . strtolower($json[$i]['positions'][$x]['ticker']) . '.json';


		if (file_exists($ticker)) 
		{
			$graphJson = file_get_contents($ticker);
			
			if ($graphJson = json_decode($graphJson, true))
			{
			}
			else
			{
				echo 'Error reading, will continue.';
				echo '<br>';
				continue;
			}

			$graphJsonCount = count($graphJson);
			$targetdate = $json[$i]['positions'][$x]['positionStartDate'];

			$foundsuccess = 0;

			//echo 'Targetdate er ' . $targetdate . '<br> ';

			for ($j = 0; $j < $graphJsonCount; $j++) {

				$keys = array_keys($graphJson[$j]);
				$keys = $keys[0];

				if ($keys == $targetdate) {

					$graphday = $graphJson[$j][$keys];

					//echo $targetdate . ' ' . $graphJson['price'] . '<br>';
					//exit();
					$json[$i]['positions'][$x]['startStockPrice'] = (float)$graphday['close'];
					$json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] = $json[$i]['positions'][$x]['startStockPrice'] - (float)$json[$i]['positions'][$x]['stockPrice'];
					$json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] = $json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] *-1;
					$json[$i]['positions'][$x]['positionValueChangeSinceStart'] = $json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] * $json[$i]['positions'][$x]['numberOfStocks'] * -1;

					$json[$i]['positions'][$x]['positionValueChangeSinceStartBaseCurrency'] = $json[$i]['positions'][$x]['positionValueChangeSinceStart'] * $json[$i]['positions'][$x]['base_currency_multiply_factor'];
					
					$foundsuccess = 1;
					break;
				}
			}

			if ($foundsuccess == 0)
			{
				echo 'Startdato ' . $targetdate . ' og kurs ikke funnet, legger til "-". Nr ' . $x . '.<br>';
				//unset($json[$i]['positions'][$x]);

				$json[$i]['positions'][$x]['startStockPrice'] = '-';
				$json[$i]['positions'][$x]['stockPriceChangeSinceStartDate'] = '-';
				$json[$i]['positions'][$x]['positionValueChangeSinceStart'] = '-';
				$json[$i]['positions'][$x]['positionValueChangeSinceStartBaseCurrency'] = '-';

			}
			else
			{
				//echo ' ok <br>';
			}

		}
		else
		{
			echo $i . '. ' . $json[$i]['Name'] . ' <br>';
			errorecho('File does not exist, unsettig position' . $x . '. ' .  '<br>'); 
		}
	}
	$json[$i]['positions'] = array_values($json[$i]['positions']);

	
}




//lagre playernavn
$playerNameBox = [];

foreach ($json as $selskap)
{
	foreach ($selskap['positions'] as $posisjon)
	{
		$playerNameBox[] = $posisjon['PositionHolder'];
	}
}

$playerNameBox = array_unique($playerNameBox);
$playerNameBox = array_values($playerNameBox);

$allholder = [];

//tildel posisjoner til playere
foreach ($playerNameBox as $playername)
{
	$object = new stdClass;
	$object->playerName = $playername;
	$object->date = date('Y-m-d');
	$object->totalValueChange = 0;
	$object->totalValueChangePercent = 0;
	$object->totalMarketValue = 0;

	$tempholder = [];

	foreach ($json as $selskap)
	{
		foreach ($selskap['positions'] as $posisjon)
		{
			if ($posisjon['PositionHolder'] == $playername)
			{
				$tempholder [] = $posisjon;
				
				$object->totalMarketValue += $posisjon['marketValueBaseCurrency'];
				$object->totalValueChange  += $posisjon['positionValueChange'] * $posisjon['base_currency_multiply_factor'];

			}
		}
	}

	$yesterdaysvalue = $object->totalMarketValue + $object->totalValueChange;
	
	if ($object->totalMarketValue > 0)
	{
		$percent = (($object->totalMarketValue/$yesterdaysvalue)-1)*100;
        $percent = round($percent,4);
        $percent = number_format($percent,4,".","");		
	}
	else
	{
		$percent = '-';
	}
	
	//var_dump($tempholder);

	$object->totalMarketValue = number_format(round($object->totalMarketValue,7),7,".","");
	$object->totalValueChange  = number_format(round($object->totalValueChange,7),7,".","");

	$object->totalValueChangePercent = $percent;
	$object->positions = (array)$tempholder;
	
	$allholder[] = (array)$object;
	
}


//sort by name
usort($allholder, function($a, $b) {
    return $a['playerName'] <=> $b['playerName'];
});

//save to json

$allholder = array_values($allholder);

foreach ($allholder as $index => $player)
{
	foreach ($player['positions'] as $key => $position)
	{
		$allholder[$index]['positions'][$key] = float_format($position);
	}
}

$string =  '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' .  $date . '.' . $land . '.json';
echo 'Saving ' . $string . '<br>';

saveJSON($allholder, $string);

$string =  '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
echo 'Saving ' . $string . '<br>';

saveJSON($allholder, $string);


?>