
<?php 

//include 'header.html';
echo '<br>';

require '../production_europe/namelink.php';
require '../production_europe/logger.php';
require '../production_europe/functions.php';
include '../production_europe/options_set_currency_by_land.php';

$filename = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';

$graphlocation = '../production_europe/history_shorting/' .  $land . '/';

//last inn shortlisten som inneholder prosent short

if (!$data = readJSON($filename))
{
    errorecho('Error reading ' . $filename . '<br>');
    return;
}


$allBox = [];
//Hent ut relevante data

$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

foreach ($data as $key => $company) 
{
    //getting ticker
    if (!isset($company['Positions'][0]['ISIN']))
    {
        continue;
    }

    $isintarget = $company['Positions'][0]['ISIN'];
    $nametarget =  mb_strtolower($company['Name']);
    $ticker = '';

    if ($row = binary_search_multiple_hits($isintarget, $nametarget, $isin_list, $name_list, $land))
    {
        $ticker = $row[1] . '.' . $row[8];
        $ticker = str_replace(" ", '-', $ticker);  
    }

    $object = new Stdclass;
    $object->selskapsnavn =  $company['Name'];
    $object->shortprosent = $company['ShortPercent'];
    $object->ticker = $ticker;

    $allBox[] = (array)$object;
}

$today = date('Y-m-d');
$a_week_ago = date('Y-m-d');
$a_week_ago = date( "Y-m-d", strtotime("-5 Weekday", strtotime($a_week_ago)));

usort($allBox, function($a, $b) {
    return $b['shortprosent'] > $a['shortprosent'];
});


foreach ($allBox as $index => $company) {

    $tickerLocation = $graphlocation  . strtolower($company['ticker']) . '.json';
    $foundsuccess = 0;

    if (file_exists($tickerLocation)) 
    {
        echo $index . '. Fant: ' . $tickerLocation . ' for ' . $company['selskapsnavn'] . ' | ';

        if (!$stockdata = readJSON($tickerLocation))
        {
            errorecho('Could not read json file ' . $tickerLocation . '<br>');
        }

        if (isset($stockdata[$a_week_ago]))
        {
            echo 'Fant tickerdata for en uke siden på dato ' . $a_week_ago;
            echo ' | adding shortprosent ' . $stockdata[$a_week_ago] . '<br>';
            $allBox[$index]['tidligereShortprosent'] = round($stockdata[$a_week_ago],2);
            $foundsuccess = 1;
        }

        if ($foundsuccess == 0)
        {
            errorecho('Fant ikke dato en uke tidligere!<br>');
            $allBox[$index]['tidligereShortprosent'] = '-';
        }
    }
    else 
    {
        echo $index . '. Fant IKKE TICKER: ' . $tickerLocation . '<br>';
        $allBox[$index]['tidligereShortprosent'] = '-';
    }

    if ($index > 8)
    {
        break;
    }
}


//slett alle innføringer etter verdi nummer 10 slik at det blir lettere med datene i echarts



ob_start();
?>

<div class="col-lg-6">
    <h3>Most shorted companies</h3> 
    <table class="table table-striped table-hover table-bordered table-sm table-most-shorted">
        <thead class="thead-dark">
          <tr>
        <th>#</th>
        <th>Name</th>
        <th class="text-right most-shorted-table-heading-share">Short</th>
        <th class="text-right most-shorted-table-heading-a-week-ago"  data-toggle="tooltip" data-placement="top" title="<?php echo $a_week_ago;?>">A week ago</th>
    </tr>
</thead>
<tbody>

    <?php $counter = 1;
    foreach ($allBox as $key => $selskap) {

       $teller = $key + 1; 

       if ($selskap['tidligereShortprosent'] == '-') 
       {
        $arrow = ' <i class="fa fa-arrow-circle-right "> </i>';
    }
    else if ($selskap['shortprosent'] > $selskap['tidligereShortprosent']) 
    {
        $arrow = ' <span class="text-success"><strong><i class="fa fa-arrow-circle-up "></i>';
    }
    else if ($selskap['shortprosent'] == $selskap['tidligereShortprosent']) 
    {
       $arrow = ' <i class="fa fa-arrow-circle-right "> </i>';
   }
   else 
   {
    $arrow = ' <span class="text-danger"><strong><i class="fa fa-arrow-circle-down "></i>';
}

echo '<tr>';
echo '<td>' .  $teller  . '.</td>';
$singleCompany = nametolink($selskap['selskapsnavn']);
$singleCompany = strtoupper($singleCompany);
echo '<td>';
echo '<a href="details_company.php?company=' . $singleCompany  . '&land=' . $land . '">';
echo $selskap['selskapsnavn'] . '</td>';
echo '<td class="text-right">' . $selskap['shortprosent'] . '%' . $arrow . '</td>';
echo '<td class="text-right">' . $selskap['tidligereShortprosent'] . '%' . '</td>';
echo '</tr>';

    //Begrens til topp 10
if ($teller > 9) {
    break;
}
}
$link = 'most_shorted_companies_all.php?country=' . $land;
$linklog = 'log.php?country=' . $land . '&index=0';
?>

</tbody>
</table>

<a class="float-right" href="<?php echo $link; ?>"><strong>See all</strong></a>
</div>
<?php 

$fileNameFirst = '../shorteurope-com.luksus.no/mostShortedCompanies_';
$fileNameMiddle = $land;
$fileNameLast = '.html';

$filename = $fileNameFirst . $fileNameMiddle . $fileNameLast;
//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename, $htmlStr);
?>