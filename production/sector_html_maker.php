<?php

//$land = 'sweden';

$country = $land;
$positions2 = '../shorteurope-com.luksus.no/datacalc/sector/' . $land . '/sector_' . $land . '.current.json';
$filenamesector = '../shorteurope-com.luksus.no/sector_' . $land . '.html';

include '../production_europe/options_set_currency_by_land.php';
require '../production_europe/namelink.php';
require '../production_europe/functions.php';


$json = readJSON($positions2);
$count = count($json);

//get total value
$totalValue = 0;
for ($i = 0; $count > $i; $i++) {
  $totalValue += round($json[$i]['marketValue'],2);
}

//get highest sector, for making the bar full width
$higest = 0;
$ratio = 1/($json[0]['marketValue']/$totalValue);

//take away a little for looks
//$ratio = $ratio *0.9;


?>

<?php //include 'header.html'; ?>



<?php ob_start();?>
<div class="d-flex flex-column">
  <?php
  $counter = 0;
  for ($i = 0; $i < 3; $i++) {

    if (!isset($json[$i]['sectorname']))
    {
      logger('Error in sector_html_maker.php !isset($json[$i]["sectorname"]',' returning');
      continue;
    }

    if (!isset($json[$i]['marketValue']))
    {
      logger('Error in sector_html_maker.php2',' returning');
      continue;
    }



    if ($i == 0) {
      echo '<div class="mt-0">';
    }
    else {
      echo '<div class="mt-3">';
    }
    
    echo '<div class="px-0 py-2 sector-border tab-border-4-sides d-flex flex-row justify-content-between">';
    echo '<div class="font-weight-bold">';
    echo '<span class="greyish">';
    echo ++$counter . '. ';
    echo '</span>';
    echo '<span class="sector-category">' . $json[$i]['sectorname'] . ' </span>';
    echo '</div>';
    echo '<div class="sector-sum">';
    echo '<span class="">';
    echo number_format(round($json[$i]['marketValue'],2),2,".",",")  . ' M ' . $currency_ticker;
    echo '</span>';
    echo '</div>';
    echo '</div>';
    $metaverdi = ($json[$i]['marketValue']/$totalValue)*100;
    
    if ($metaverdi > 26) {
      ?>
      
      <div class="progress progress-height" data-toggle="tooltip" title="Total short value in the market: <?php echo number_format(round($totalValue,2),2,".",","); ?> million">
        <div class="progress-bar progress-color bg-warning " role="progressbar" style="width: <?php echo $metaverdi; ?>%;" aria-valuenow="<?php echo $metaverdi; ?>" aria-valuemin="0" aria-valuemax="100"><span class=""><?php echo number_format(round($metaverdi,2),2,".",",");?>%</span>
        </div>
      </div>
      
      
      <?php
    } 
    else {
      ?>
      <div class="progress progress-height">
        <div class="progress-bar bg-warning " role="progressbar" style="width: <?php echo $metaverdi; ?>%;" aria-valuenow="<?php echo $metaverdi; ?>" aria-valuemin="0" aria-valuemax="100">
        </div>
        <div class="text-right text-black pl-2"><span class=""><?php echo number_format(round($metaverdi,2),2,".",",");?>%</span>
        </div>
      </div>
      
      <?php
    }
    echo '</div>';
  }

  ?>

  <div class="most-shorted-bottom-spacer">
    
  </div>

  <?php
          //  Return the contents of the output buffer
  $htmlStr = ob_get_contents();
          // Clean (erase) the output buffer and turn off output buffering
  ob_end_clean(); 
          // Write final string to file
  file_put_contents($filenamesector, $htmlStr);
  ?>

