<?php
if (!function_exists('saveCSV')) {

    //Lagre nøkkeldata funksjon
    function saveCSV($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');

    foreach ($input as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
    }
}

if(!function_exists('readCSV_merge')){
 function readCSV_merge($file_location_and_name) {

    $csvFile = file($file_location_and_name);

    //Les in dataene 
    $name = [];
    foreach ($csvFile as $line) {
        $name[] = str_getcsv($line, ',');
    }

    //ordne utf koding
    $counter = 0;
    foreach ($name as &$entries) {
        $data[$counter] = array_map("utf8_encode", $entries);
        $counter++;
    }
    return $name;
 }
}

if(!function_exists('joinFiles')){
function joinFiles(array $files, $result) {
    if(!is_array($files)) {
        throw new Exception('`$files` must be an array');
    }

    $wH = fopen($result, "w+");

    foreach($files as $file) {
        $fh = fopen($file, "r");

        while(!feof($fh)) {
            fwrite($wH, fgets($fh));
        }
        fclose($fh);
        unset($fh);
        fwrite($wH, "\n"); //usually last line doesn't have a newline
    }
    fclose($wH);
    unset($wH);
    }
}

$history = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.csv';
$active = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land  . '.current_active.csv';
$merged = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land  . '.history_merged.csv';


if ($land == 'germany')
{
    echo '<br>Germany has duplicate positions, so active positions must be removed from historic positions<br>';

    //germany has duplicate positions, so active positions must be removed from historic positions
    $historypositions = readCSVkomma($history);
    $activepositions = readCSVkomma($active);

    $activepositionscount = count($activepositions);
    $historypositionscount = count($historypositions);

    $removecounter = 0;

    //take all active positions 

    for ($i = 1; $i < $activepositionscount; $i++)
    {
       
        $targetarray = $activepositions[$i];
        unset($targetarray[5]);

        for ($x = 1; $x < $historypositionscount; $x++)
        {
            if (!isset($historypositions[$x]))
            {
                continue;
            }

            $result = array_diff($targetarray, $historypositions[$x]);

            if ($result)
            {
                
                
            }
            else
            {
                //echo $i . '. ';
                //echo 'Matches, will be removed from historical positions';
                $removecounter++;
                unset($historypositions[$x]);
                break;
            }
        }

        //echo '<br>';
    }
    echo 'Removed ' . $removecounter  . ' positions from german historic positions<br>';

    $historypositions = array_values($historypositions);

    //also convert the kommas in the shortpercent
    $activepositionscount = count($activepositions);
    $historypositionscount = count($historypositions);

    for ($i = 1; $i < $activepositionscount; $i++)
    {
        if (isset($activepositions[$i][3]))
        {
            $activepositions[$i][3] = str_replace(",", ".", $activepositions[$i][3]);
        }

    }

    for ($i = 1; $i < $historypositionscount; $i++)
    {
        $historypositions[$i][3] = str_replace(",", ".", $historypositions[$i][3]);
    }

    saveCSV($historypositions, $history);
    saveCSV($activepositions, $active);

}

joinFiles(array($active, $history), $merged);

$dataArrayA = readCSV_merge($merged);

foreach ($dataArrayA as $key => $row) {
    if ($row[0] == '') {
        unset($dataArrayA[$key]);
    }
}

saveCSV($dataArrayA, $merged);

$dataArrayA = null;
$historypositions = null;
$activepositions = null;

?>