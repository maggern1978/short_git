<?php

$playerposistionLocation = '../shorteurope-com.luksus.no/datacalc/players/'. $land . '/playerpositions.'. $land . '.current.json';
$shortliste = '../shorteurope-com.luksus.no/datacalc/shortlisten_' . $land . '_current.csv';

require '../production_europe/namelink.php';

$positionTotalCount = 0;

if (!$json = readJSON($playerposistionLocation))
{
 	//echo 'Read error';
	return;
}

$companyNameBox = [];
$numberOfPlayers = count($json);
$allpositionsbox = [];

for ($i = 0; $i < $numberOfPlayers; $i++)
{
	$positioncount = count($json[$i]['positions']);
	
	$positionTotalCount += $positioncount;

	//echo $i . ' ' . $positioncount . '<br>';

	for ($x = 0; $x < $positioncount; $x++)
	{
		$companyNameBox[] = $json[$i]['positions'][$x]['companyName'];
		$allpositionsbox[] = $json[$i]['positions'][$x];
	}

}

$companyNameBox = array_unique($companyNameBox);
$companyNameBox = array_values($companyNameBox);

asort($companyNameBox);


$counter = 1;

$date = date('Y-m-d');

$company_array = array();

//56 players, holding 98 positions in 52 companies.
echo '<h5>' . $numberOfPlayers . ' players, holding ' . $positionTotalCount . ' positions in ' .  count($companyNameBox) . ' companies' . '.';
?>
</h5>
</div>
</div>
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
		Companies with missing market data are excluded.
			<?php 
			echo "Updated: " . date("Y-m-d, H:i",filemtime($playerposistionLocation)); ?>. 
			<br><br>
		</div>
	</div>
</div>

<?php

$headerSwitch = 1;
$descriptionTemp = '';



foreach ($companyNameBox as $key => $companyName)
{

	$hitcount = 0;
	
	$total_verdiendring = 0;
	$total_eksponering = 0;
	$total_winorloss = 0;
	$total_positionValueChangeSinceStart = 0;
	$total_short = 0;

	foreach ($allpositionsbox as $index => $posisjon) {

		if (strtolower($companyName) != strtolower($posisjon['companyName']))
		{
			continue;
		}

		if ($hitcount == 0) {
			$singleCompany = $posisjon['companyName'];
			include '../production_europe/detaljer_selskap_single_header.php';
		}	

		$hitcount++;

		$selskapNew  = strtolower($posisjon["PositionHolder"]);
		$selskapNew  = ucwords($selskapNew);
		$singleCompany   = nametolink($selskapNew);
		$singleCompany = strtoupper($singleCompany);
		$historyLink = 'details.php?player=' . $singleCompany .
		'&land=' . $land;

		echo '<tr>';
		echo '<td class="">';
		$runner = $hitcount ;
		echo $runner;
		echo '.</td>';
		echo '<td>';
		echo '<a class="text-dark" data-toggle="tooltip" title="See active positions 
		for this player" href="' . $historyLink  . '">';
			echo ucwords($selskapNew );
		echo '</a>';
		echo '</td>';
		echo '<td class="text-right shortpercentcell" data-toggle="tooltip" title="' . number_format($posisjon['numberOfStocks'],0,".",",") . ' stocks">';
		echo number_format($posisjon['shortPercent'],2,".",",") . ' %';
		echo '</td>';

		echo '<td class="text-right">';
		$posisjonsverdi = round($posisjon['marketValue'],2);

		echo number_format($posisjonsverdi,2,".",",") . ' M ' . $posisjon['currency'];
		echo '</td>';

		echo '<td class="text-right">';
		$verdiendring = round($posisjon['positionValueChange'],2);
		echo number_format($verdiendring,2,".",",") . ' M ';
		echo '</td>';

		echo '<td class="text-right" data-toggle="tooltip" title="Position started ' . $posisjon['positionStartDate'] . '.">';
		if (!isset($posisjon['startStockPrice']) or $posisjon['startStockPrice'] == '' or $posisjon['startStockPrice'] == 0) 
		{
			echo '-';
		}
		else 
		{
			echo round($posisjon['startStockPrice'],2);
		}
		echo '</td>';

		echo '<td class="text-right">';

		if (!isset($posisjon['positionValueChangeSinceStart']) or $posisjon['positionValueChangeSinceStart'] == '' or $posisjon['startStockPrice'] == '' or $posisjon['startStockPrice'] == 0) 
		{
			echo '-';
		}
		else 
		{
			$change = round($posisjon['positionValueChangeSinceStart']/1000000,2);
			echo number_format($change,2,".",",") . '';
		}
		echo ' M</td>';
		echo '</tr>';

		$total_short += round($posisjon['shortPercent'],2);
		$total_winorloss += $verdiendring;
		if (isset($posisjon['positionValueChangeSinceStart']) and $posisjon['positionValueChangeSinceStart'] != '-')  
		{
			$total_verdiendring += $posisjon['positionValueChangeSinceStart']/1000000;
		}
		
		$total_eksponering += $posisjon['marketValue'];

		if (isset($posisjon['positionValueChangeSinceStart']) and $posisjon['positionValueChangeSinceStart'] != '' and $posisjon['positionValueChangeSinceStart'] != NULL and $posisjon['positionValueChangeSinceStart'] != '-') {
			$total_positionValueChangeSinceStart += $posisjon['positionValueChangeSinceStart'];		
		}
		
	}

	if ($hitcount > 1)
	{
		echo '<tr>';
		echo '<td>';

		echo '</td>';

		echo '<td class="font-weight-bold">';
		echo 'Sum:';
		echo '</td>';

		echo '<td class="text-right font-weight-bold">';
		echo $total_short;
		echo ' % </td>';

		echo '<td class="text-right font-weight-bold">';

		$total_eksponering = round($total_eksponering,2);
		echo number_format($total_eksponering,2,".",",")  . ' M';

		echo '</td>';

		echo '<td class="font-weight-bold text-right ' . getcolor($total_winorloss)  . '">';
		echo $total_winorloss;
		echo ' M</td>';

		echo '<td class="text-right">';
		echo '</td>';

		echo '<td class="text-right font-weight-bold ' . getcolor($total_verdiendring) . ' ">';
		$total_verdiendring = round($total_verdiendring,2);
		echo number_format($total_verdiendring,2,".",",") . ' M';

		echo '</td>';
		echo '</tr>';
	}

	?>
</tbody>
</table>
<div class="mb-4">
</div>

</div>
</div>
</div>
<?php 
}

if (isset($description) and $mode != 'multi') {
	$description = $description . 'Shorted by: ' .  $descriptionTemp;
}


?>