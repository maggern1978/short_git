<?php

include '../production_europe/functions.php'; //må rettes
$development = 0; 

$savedir = '..shortnordic-com.luksus.no/dataRaw/ssr.finanstilsynet/finanstilsynet_current.json';
$savedir = '../shorteurope-com.luksus.no/dataraw/norway/norway_current.json';

//first download

$url = 'https://ssr.finanstilsynet.no/api/v2/instruments';

if ($development == 0)
{
	//download file
	if (!$json = json_decode(download($url),2))
	{
		errorecho('Error downloading file!');
	}	
}
else
{
	$json = readJSON('instruments.json');
}

saveJSON($json, '../shorteurope-com.luksus.no/dataraw/norway/norway_current_downloaded_raw.json');

//find newest position date
$newestdateglobal = '2000-01-01';

//keep positions with event newest work day
$jsoncount = count($json);
for ($i = 0; $i < $jsoncount; $i++)
{
	//echo $i . '. ';
	//echo $json[$i]['issuerName'];
	//echo ' ';	
	//echo $json[$i]['isin'];
	//echo '<br>';
	
	$localnewestdate = $json[$i]['events'][0]['date'];
	
	if ($localnewestdate > $newestdateglobal)
	{
		$newestdateglobal = $localnewestdate;
	}
	
}
echo '<br>';
echo 'Newest date in positions is ' . $newestdateglobal . '<br>';

$keepBox = [];

//keep positions with event newest work day
$jsoncount = count($json);
for ($i = 0; $i < $jsoncount; $i++)
{
	
	if ($json[$i]['events'][0]['shortPercent'] > 0)
	{
		$json[$i]['events'] = 	$json[$i]['events'][0];
		$keepBox[] = $json[$i];
	}
	
}

$keepBoxcount = count($keepBox);
echo 'Keeping ' . count($keepBox) . ' companies with newest date.<br>';

for ($i = 0; $i < $keepBoxcount; $i++)
{
	
	$keepBox[$i]['ISIN'] = $keepBox[$i]['isin'];
	$keepBox[$i]['Name'] = $keepBox[$i]['issuerName'];
	$keepBox[$i]['ShortPercent'] = $keepBox[$i]['events']['shortPercent'];
	$keepBox[$i]['ShortedSum'] = $keepBox[$i]['events']['shares'];;
	$keepBox[$i]['LastChange'] = $newestdateglobal;
	$keepBox[$i]['NumPositions'] = count($keepBox[$i]['events']['activePositions']);
	$keepBox[$i]['Positions'] = [];
	
	for ($x = 0; $x < $keepBox[$i]['NumPositions']; $x++)
	{
		$pos = new stdClass;
		$pos->ShortingDate = $keepBox[$i]['events']['activePositions'][$x]['date'];
		$pos->PositionHolder = $keepBox[$i]['events']['activePositions'][$x]['positionHolder'];
		$pos->ShortPercent = $keepBox[$i]['events']['activePositions'][$x]['shortPercent'];
		$pos->NetShortPosition = $keepBox[$i]['events']['activePositions'][$x]['shares'];
		$pos->ISIN = $keepBox[$i]['ISIN'];
		$pos->Name = $keepBox[$i]['Name'];
		$pos->isActive = 'yes';
		$keepBox[$i]['Positions'][] = (array)$pos;
	}

	unset($keepBox[$i]['isin']);
	unset($keepBox[$i]['issuerName']);
	unset($keepBox[$i]['events']);
}

saveJSON($keepBox, $savedir);

//for next day
$date = date('Y-m-d');
$url = '../shorteurope-com.luksus.no/dataraw/norway/' . $date .  '.' . $land . '.json';
saveJSON($keepBox, $url);

?> 
