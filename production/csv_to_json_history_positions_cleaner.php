<?php

//$land = 'germany';
//$land = 'sweden';

include_once '../production_europe/functions.php';
$fileCurrent = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json';

if (!$shortpositions = readJSON($fileCurrent))
{
	errorecho('Read error of positions');
}

echo '<br>';
echo 'Inputfile er ' . $fileCurrent  . '<br>';

//ta hvert selskap og samle sammen playernavnene

$shortpositionscount = count($shortpositions);

$allpositionsbyplayer = [];

for ($i = 0; $i < $shortpositionscount; $i++)
{
	echo '<h1>' . $i . '. Selskap: ' . $shortpositions[$i]['Name'] . '</h1>';
	$poscount = count($shortpositions[$i]['Positions']);
	$playernamebox = [];

	for ($x = 0; $x < $poscount; $x++)
	{
		$playernamebox[] = mb_strtolower($shortpositions[$i]['Positions'][$x]['PositionHolder']);
	}

	$playernamebox = array_unique($playernamebox);
	$playernamebox = array_values($playernamebox);

	//var_dump($playernamebox);

	$playernameboxcount = count($playernamebox);
	$playerpositionbox = [];

	for ($x = 0; $x < $playernameboxcount; $x++)
	{
		$playerslocalpositionsbox = [];

		$playernametarget = $playernamebox[$x];
		//echo 'Playernametarget er ' . $playernametarget . ' ';

		for ($z = 0; $z < $poscount; $z++)
		{

			if ($playernametarget == mb_strtolower($shortpositions[$i]['Positions'][$z]['PositionHolder']))
			{
				//echo '|';
				$playerslocalpositionsbox[] = $shortpositions[$i]['Positions'][$z];
			}
		}	
		$playerpositionbox[] = $playerslocalpositionsbox;
		//echo '<br>';

	}

	$local_box_all = $playerpositionbox;
	$local_box_all_count = count($local_box_all);

	$removecounter = 0; 

	for ($a = 0; $a < $local_box_all_count; $a++)
	{
		$local_box = $local_box_all[$a];
	
		//sjekk at det ikke er noen duplikater, dvs samme player, samme dato og samme shortprosent
		$local_box_count = count($local_box_all[$a]);
		$round = 0; 
		

		for ($x = 0; $x < $local_box_count; $x++)
		{
			if (!isset($local_box_all[$a][$x]))
			{
				continue;
			}
				

			$target_player = mb_strtolower($local_box_all[$a][$x]['PositionHolder']);
			$target_company = mb_strtolower($local_box_all[$a][$x]['Name']);
			$target_shortpercent = $local_box_all[$a][$x]['ShortPercent'];
			$target_shortdate = $local_box_all[$a][$x]['ShortingDate'];

			$found_itself_count = 0;

			for ($z = 0; $z < $local_box_count; $z++)
			{

				if (!isset($local_box_all[$a][$z]))
				{
					continue;
				}

				if ($target_player == mb_strtolower($local_box_all[$a][$z]['PositionHolder']) and $target_company == mb_strtolower($local_box_all[$a][$z]['Name']) and $target_shortpercent == $local_box_all[$a][$z]['ShortPercent'] and $target_shortdate == $local_box_all[$a][$z]['ShortingDate'] and $found_itself_count == 0)
				{
				    //echo 'Found itself';
					$found_itself_count++;
					continue;

				}

				if ($target_player == mb_strtolower($local_box_all[$a][$z]['PositionHolder']) and $target_company == mb_strtolower($local_box_all[$a][$z]['Name']) and $target_shortdate == $local_box_all[$a][$z]['ShortingDate'] and $found_itself_count > 0) //plukker opp samme dato men ulik shortprosent 
				{
					echo $removecounter . '. Found duplicate position. <br>';
					//var_dump($local_box_all[$a][$x]);
					//var_dump($local_box_all[$a][$z]);
					

					//echo '----------------------------------------------------------------------------<br>';
					unset($local_box_all[$a][$z]);
					$found_itself_count++;
					$removecounter++;

				}			

			}
		}

		$local_box_all[$a] = array_values($local_box_all[$a]);

	}

	$playerpositionbox = $local_box_all;


	//ta hver player
	foreach ($playerpositionbox as $index => $player)
	{


		if ($player[0]['PositionHolder'] == '')
		{
			unset($playerpositionbox[$index]);
		}

		//tester for duplikater
		$playerpositionbox_copy = $player;
		$playerpositionbox_copy_count = count($playerpositionbox_copy);

		for ($x = 0; $x < $playerpositionbox_copy_count; $x++) //gjør oppføringer i array helt like
		{
			unset($playerpositionbox_copy[$x]['isActive']);
		}

		echo '---| Posisjoner inn: ' . $playerpositionbox_copy_count . '<br>';

		$playerpositionbox_copy_without_duplicates = remove_duplicates_in_array($playerpositionbox_copy);

		$playerpositionbox_copy_without_duplicates_count = count($playerpositionbox_copy_without_duplicates);

		echo '---| Posisjoner ut: ' . $playerpositionbox_copy_without_duplicates_count . '<br>';

		if ($playerpositionbox_copy_count != $playerpositionbox_copy_without_duplicates_count)
		{
			$diff = $playerpositionbox_copy_count - $playerpositionbox_copy_without_duplicates_count;
			errorecho('Her er det ' . $diff . ' duplikater!<br>');
		}


		echo '---| ' . $index . '. Player: ' . $player[0]['PositionHolder'] . '<br>';


		$foundzero = 0;
		$previous = 0;
		$firstiscorrect = 0;
		$previousiszero = 0;


		foreach($player as $teller => $position)
		{
			
			if ($teller == 0) //da må nyeste posisjon enten være aktiv eller null. 
			{
				if ($position['ShortPercent'] == 0 or $position['isActive'] == 'yes')
				{
					//echo '---|---Første posisjon (' . $teller . ') er ok. isActive: ' . $position['isActive'] . '. Shortprosent: ' . $position['ShortPercent'] .  ' <br>';
					$firstiscorrect = 1;

					if ($position['ShortPercent'] == 0)
					{
						$previousiszero = 1;	
					}		

					//første posisjon er korrekt, da må alle påfølgende settes til ikke aktive

					foreach ($player as $runner => $pos)
					{
						if ($runner == 0) //da må siste posisjon enten være aktiv eller null. 
						{
							continue;
						}

						if ($pos['isActive'] == 'yes')
						{
							errorecho($teller  . '. Fant en posisjon som er satt til aktiv når den ikke burde være det. Setter til ikke aktiv. <br>');
							//var_dump($pos);
							//var_dump($playerpositionbox);
							//exit();
							$playerpositionbox[$index][$runner]['isActive'] = 'no';
						}

						
					}

					continue;
				}
				else
				{
					errorecho('Første posisjon er ikke korrekt. Vil bli satt til 0 i shortprosent.<br>');
					//var_dump($playerpositionbox[$index]);
				}
			}

			if ($firstiscorrect == 1)
			{

				if ($position['ShortPercent'] != 0)
				{
					//echo '---|--- ' . $teller . ' er ok (' . $position['ShortPercent'] . ')<br>';
					$previousiszero = 0;
				}
				else
				{
					if ($previousiszero == 1)
					{
						errorecho($teller . '. To nuller etter hverandre!<br>');
					}
					else
					{
						echo '---|--- ' . $teller . ' er NULL<br>';
						$previousiszero = 1;						
					}

				}

			}
			else if ($firstiscorrect == 0)
			{
				//set position to zero
				$playerpositionbox[$index][$teller]['ShortPercent'] = 0;
				$playerpositionbox[$index][$teller]['NetShortPosition'] = 0;				
				$firstiscorrect = 1;
			}
		}
		echo '<br>----------------------------------------------<br><br>';
		//break;
	}

	//samle posisjonene igjen
	$positionsbox = [];
	$playercount = count($playerpositionbox);
	$collectcounter = 0; 

	for ($x = 0; $x < $playercount; $x++)
	{

		$playerposcount = count($playerpositionbox[$x]);

		for ($z = 0; $z < $playerposcount; $z++)
		{
			$positionsbox[] = $playerpositionbox[$x][$z];
			$collectcounter++;
		}

	}

	echo 'Collected ' . $collectcounter . ' positions <br>';

	//sort by date
	usort($positionsbox, function ($item1, $item2) {
		return $item1['ShortingDate'] < $item2['ShortingDate'];
	});

	$shortpositions[$i]['Positions'] = $positionsbox;

	//var_dump($positionsbox);

}

saveJSON($shortpositions, '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json');

?>