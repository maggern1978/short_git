				    
					<?php if(!isset($noName)) {
						//noname is if the name should be echoed or not
						require '../production_europe/namelink.php';
					?>
				    <div class="container">
				        <div class="row">
				           <div class="col-12">
				           <h2>
				           <?php
				           	$selskapNew  = ucwords($playernavn);
				            $singleCompany   = nametolink($selskapNew);
				            $singleCompany = strtoupper($singleCompany);
				            $historyLink = 'details.php?player=' . $singleCompany .
				                  '' . '&land=' . $land;
				            echo '<a class="text-dark" data-toggle="tooltip" title="See all active positions" href="' . $historyLink  . '">';
				            $selskapNew = strtolower($selskapNew);
						    echo ucwords($selskapNew);
						    echo '</a>';

				           ?>
				           <span class="h4 pull-right"><?php echo $currency_ticker; ?></span></h2>
				             <table class="table table-bordered table-sm">
							    <thead>
							      <tr>
							        <th class="">#</th>
							        <th>Company</th>
							        <th class='text-right'>
								        <span class="">Short</span>
							        </th>
							        <th class='text-right'>
								        <span class="" data-toggle="tooltip" title="Number of stocks shorted multiplied by current stock price">Position value</span>
								        	
							        </th>
							        <th class='text-right'>Stock price</th>
							        <th class="text-right" data-toggle="tooltip" title="Stock price change in percent the latest trading day">
								        <span class="">Latest price change</span>
							        </th>

							        <th class="text-right">
								        <span class="" data-toggle="tooltip" title="Position value change latest trading day">Pos. value change</span>
							        </th>
							        <th class="text-right" data-toggle="tooltip" title="Price at short position start (end of day value)">
								        <span class="">Startdate stock price</span>
							        </th>
							        
							       	<th class="text-right" data-toggle="tooltip" title="Value change since the short position started">
								        <span class="">Total value change</span>
							        </th>
							      </tr>
							    </thead>
							    <tbody>

							   <?php //noname end
							   } 
							   else { 
							   ?>

				              <table class="table table-bordered table-sm">
							    <thead>
							      <tr>
							        <th class="">#</th>
							        <th>Company</th>
							        <th class='text-right'>
								        <span class="">Short</span>
							        </th>
							        <th class='text-right'>
								        <span class="" data-toggle="tooltip" title="Number of stocks shorted multiplied by current stock price">Position value</span>
								        	
							        </th>
							        <th class='text-right'>Stock price</th>
							        <th class="text-right" data-toggle="tooltip" title="Stock price change in percent the latest trading day">
								        <span class="">Latest price change</span>
							        </th>

							        <th class="text-right">
								        <span class="" data-toggle="tooltip" title="Position value change latest trading day">Pos. value change</span>
							        </th>
							        <th class="text-right" data-toggle="tooltip" title="Price at short position start (end of day value)">
								        <span class="">Startdate stock price</span>
							        </th>
							        
							       	<th class="text-right" data-toggle="tooltip" title="Value change since the short position started">
								        <span class="">Total value change</span>
							        </th>
							      </tr>
							    </thead>
							    <tbody>
							   	<?php //else end 
							   	} 
							   	?>