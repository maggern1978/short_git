<?php

include '../production_europe/functions.php';

if (!function_exists('saveCSV')) {

    //Lagre nøkkeldata funksjon
    function saveCSV($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');

    foreach ($input as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
    }
}

//$land = 'germany';

$fil = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.csv';
$historytarget = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.current_active.csv';

$csvdata = readCSVkomma($fil);
$count = count($csvdata);

for ($i = 0; $i < $count; $i++)
{

	$removed = 0;

	for ($x = 0; $x < 5; $x++)
	{
		//removing komma in company and player names

		if (!isset($csvdata[$i][$x]))
		{
			errorecho('Data not set at ' . $i . ' and ' . $x . ' Unsettting<br> <br> ');
			unset($csvdata[$i]);
			$removed = 1;
			break;
		}
	}

	if ($removed == 0)
	{
		$csvdata[$i][0] = str_replace(",", "", $csvdata[$i][0]);
		$csvdata[$i][1] = str_replace(",", "", $csvdata[$i][1]);
	
	
	}
}

usort($csvdata, function ($item1, $item2) {
    return $item2[4] > $item1[4];
});


foreach ($csvdata as $key => $row) {
    $csvdata[$key][5] = 'active';
}

//fjern aktiv markering hvis player har to oppføringer i sammme selskap
foreach ($csvdata as $key => $row)
	{

		$targetcompany = $row[1];
		$targetplayer = $row[0];

		$hitcounter = 0;

		foreach ($csvdata as $index => $subrow)
		{

			if ($targetcompany == $subrow[1] and $targetplayer ==  $subrow[0] )
			{

				
				if ($hitcounter == 0)
				{
					//echo '--> ' . $index . '. ' . $targetcompany . '. First hit skal ikke røres!<br>';
					$hitcounter++; //skip first. 
					continue;

				}
				else
				{

					echo $index . '. Dobbel oppføring for ' . $targetcompany . ' og ' . $targetplayer .'. Hits: ' . $hitcounter . '<br>';
					$csvdata[$index][5] = '';
					//break;

				}


			}

		}



	}





saveCSV($csvdata, $historytarget);

$dataArrayX = null;

?>