<?php

declare(strict_types=1);

require '../production_europe/functions.php';
require '../production_europe/logger.php';

//session_start();    

//echo session_id();

$url = 'https://www.finanssivalvonta.fi/api/shortselling/datatable/all/export';


$data = array("draw"=> 3,

   "columns[0][data]"=> "positionHolder",

   "columns[0][name]" => "", 			  

   "columns[0][searchable]"=> "true",

   "columns[0][orderable]"=>"false",

   "columns[0][search][value]" => "",		  

   "columns[0][search][regex]"=>"false",

   "columns[1][data]"=>"issuerName",

   "columns[1][name]" => "", 

   "columns[1][searchable]"=> "true",

   "columns[1][search][value]" => "true",

   "columns[1][orderable]"=> "false",

   "columns[1][search][regex]"=>"false",

   "columns[2][data]"=>"isinCode",

   "columns[2][searchable]"=> "true",

   "columns[2][orderable]"=>"false",

   "columns[2][search][regex]"=>"false",

   "columns[3][data]"=>"netShortPositionInPercent",

   "columns[3][searchable]"=>"true",

   "columns[3][orderable]"=>"false",

   "columns[3][search][regex]"=> "false",

   "columns[4][data]"=>"positionDate",

   "columns[4][searchable]"=>"true",

   "columns[4][orderable]"=>"false",

   "columns[4][search][regex]"=>"false",

   "start"=> 0,

   "length"=> 99000,

			  "search[value]" => "",

   "search[regex]"=>"false",

   "lang"=> "en",

   "exportOptions[columnData][positionHolder]"=> "Position holder",

   "exportOptions[columnData][issuerName]" =>"Name of the issuer",

   "exportOptions[columnData][isinCode]" => "ISIN",

   "exportOptions[columnData][netShortPositionInPercent]"=>"Net short position (%)",

   "exportOptions[columnData][positionDate]"=>"Date",

   "exportOptions[lang]"=>"en"

			 );



  $postvars = http_build_query($data) . "\n";



  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);

  curl_setopt($ch, CURLOPT_POST, 1);

  curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);



  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



  $server_output = curl_exec ($ch);

//var_dump($server_output);

curl_close ($ch);

file_put_contents('../shorteurope-com.luksus.no/dataraw/finland.history/finland.history_current.csv', $server_output);

$csvlocation = '../shorteurope-com.luksus.no/dataraw/finland.history/finland.history_current.csv';

$csvdata = readCSV($csvlocation);
unset($csvdata[0]);
$csvdata = array_values($csvdata);

$count = count($csvdata);
for ($i = 0; $i < $count; $i++) {
  $csvdata[$i] = str_replace(',', '.', $csvdata[$i]);
  $date = str_replace('.', '-', $csvdata[$i][4]);
  $csvdata[$i][4] = date("Y-m-d", strtotime($date));
}

saveCSVx($csvdata, $csvlocation);




?>