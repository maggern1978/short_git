<?php 

include '../production_europe/logger.php';
include '../production_europe/functions.php';
$placement = 'shorteurope-com.luksus.no';

date_default_timezone_set('Europe/Oslo');
set_time_limit(1400);
$today = date('Y-m-d');

//bygg url 

$path = '../' . $placement . '/dataraw/sweden/currentpositions.ods';

$url = 'https://www.fi.se/en/our-registers/net-short-positions/GetAktuellFile/?_=16020066075';

if ($data = download($url))
{
	save($data, $path);
	echo 'Saved file ' . $path . '<br>';

}
else
{
	echo 'Error!<br>';
	logger('Error in sweden_history_downloader_oktober.php.. ',' Download failed.');
	var_dump($data);
}

echo 'Starter sweden_ods_to_csv_converter_active.php <br>';
include '../production_europe/sweden_ods_to_csv_converter_active.php';
echo 'sweden_ods_to_csv_converter_active.php done!<br>';
?>