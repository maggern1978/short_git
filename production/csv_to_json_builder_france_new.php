<?php

include('../production_europe/logger.php');
include('../production_europe/functions.php');
$date = date('Y-m-d');
set_time_limit(5000);

$land = 'france';
$inputFileName = '../shorteurope-com.luksus.no/dataraw/'. $land . '/' . $land . '_current.csv';
$csvlocation = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.csv';
$fileCurrent = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';

//les inn datane
$csvdata = readCSVkomma($csvlocation);


require '../production_europe/logger.php';

//error_reporting(E_ALL);

if (!class_exists('selskap')) {

	class selskap {
	  //Creating properties
		public $ISIN;
		public $Name;
		public $ShortPercent;
		public $ShortedSum;
		public $LastChange;
		public $NumPositions;
		public $currency;
		public $Positions;


	    //assigning values
		public function __construct($isin, $name, $shortpercent, $shortedsum, $lastchange, $numpositions, $positions, $currency) 
		{
			$this->ISIN = $isin;
			$this->Name = $name;
			$this->ShortPercent = $shortpercent;
			$this->ShortedSum = $shortedsum;
			$this->LastChange = $lastchange;
			$this->NumPositions = $numpositions;
			$this->currency = $currency;
			$this->Positions = $positions;


		}
	}
}

if (!class_exists('posisjon')) {
	class posisjon {
	  //Creating properties
	    //public $Id;
	    //public $EntityId;
		public $ShortingDate;
		public $PositionHolder;
		public $ShortPercent;
		public $NetShortPosition;
		public $ISIN;
		public $Name;
		public $isActive;
		public $currency;
		
	    //public $Status;

	    //assigning values
		public function __construct($ShortingDate, $PositionHolder, $ShortPercent, $NetShortPosition, 
			$ISIN, $Name, $isActive, $currency) 
		{
	          //$this->Id = $Id;
	          //$this->EntityId = $EntityId;
			$this->ShortingDate = $ShortingDate;
			$this->PositionHolder = $PositionHolder;
			$this->ShortPercent = $ShortPercent;
			$this->NetShortPosition = $NetShortPosition;
			$this->ISIN = $ISIN;
			$this->Name = $Name;
			$this->isActive = $isActive;
			$this->currency = $currency;
	          //$this->Status = $Status;
		}
	}
}



//gå igjennom slett der oppføringene ikke stemmer
$count = count($csvdata);

echo 'Posisjoner inn: ' . $count . '<br>';

for ($i = 0; $i < $count; $i++)
{

	for ($x = 0; $x < 5; $x++)
	{
		if (!isset($csvdata[$i][$x]))
		{
			errorecho('Data not set at ' . $i . ' and ' . $x . ' Unsettting<br> <br> ');
			unset($csvdata[$i]);
			break;
		}
	}
}

$csvdata = array_values($csvdata);

//les inn hovedlisten for å match isin mot ticker

//les inn tickerliste 
$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

//exit();

if ($land == 'germany')
{
	
	//clean the data
	$count = count($csvdata);
	for ($i = 1; $i < $count; $i++) {

		//fixing the komma in number
		$csvdata[$i][3] = str_replace(",", ".", $csvdata[$i][3]);
	}
	unset($csvdata[0]);
	$csvdata = array_values($csvdata);
}


//lag alle posisjonene som objekt
//$used_list_companies = array();
$used_list_isin = array();
$used_list_companies = array();
$posisjoner = array();
$miss_box = [];
$miss_box_outstanding = [];


$errorholder = array();

//finn sist oppdatert ved å ta første dato fra fi.se_current.csv
$last_updated = $csvdata[1][4];

echo 'Newest/first position: ' . $last_updated . '<br>';
//echo $last_updated;

foreach ($csvdata as $key => $position) {

		//hopp over de som er mer enn fire år gamle!

	$fourYearsAgo = date('Y-m-d', strtotime("-4 year"));

	if ($position[4] < $fourYearsAgo)
	{
		continue;
	}

		//bygger to arrays med alle børsselskaper og isin, som senere brukes til å lage selskapslisten
	if (isset($position[1])) 
	{
		$used_list_companies[] = $position[1];
		$used_list_isin[] = $position[2];
	}
	else 
	{
		continue;
	}

			//finner isin, alternativt navn, ticker i isinlisten
	$targetname = mb_strtolower($position[1]);
	$targetisin = $position[2];

	$found_in_isin_list = 0;
	$success = 0;

		//function binary_search_multiple_hits ($isin, $name, $isin_sorted_list, $name_sorted_list, $land)
		//$firstrow = binary_search_isinliste($name, $name_liste);

	if ($row = binary_search_multiple_hits($targetisin, $targetname, $isin_list, $name_list, $land))
	{
		$altname = $row[2];
		$currency = $row[3];
		$tickerselskap = $row[1] . '.' . $row[8];
		$found_in_isin_list = 1;

		if (isset($row[9]))
		{
			$number_of_stocks = (float)$row[9] * ((float)$position[3]/100);
			$number_of_stocks = round($number_of_stocks,0);
				//echo 'Number of stocks: ' . $number_of_stocks . '<br>';
			$success = 1;
		}
		else
		{
			$obj = new stdClass;
			$obj->name = $position[1];
			$obj->ISIN = $position[2];
			errorecho($key . '. ' . $targetname . '. Stocks not set!<br>');
				//var_dump($row);

			$miss_box_outstanding[] = (array)$obj;	
		}
	}
	else
	{
		$obj = new stdClass;
		$obj->name = $position[1];
		$obj->ISIN = $position[2];
		$miss_box[] = (array)$obj;
		continue;
		
	}

	if ($success == 1) {

			//successecho(' success! ');
			//bytt slash-strek med bindestrek i datoene
		$date_temp = $position[4];
		$date_temp = str_replace('/','-',$date_temp,$count);

		$active = 'no';

			//$dateMinusOneDay = date('Y-m-d', strtotime('-1 weekday', strtotime($date_temp)));

		if (isset($position[5])) {
			if ($position[5] == 'active') {
				$active = 'yes';
			}
		}

		$playernavn = $position[0];
		$playernavn = str_replace("\n", '', $playernavn);

		$posisjoner[] = new posisjon($date_temp, $playernavn, $position[3], $number_of_stocks, $position[2], $position[1], $active, $currency);
	}


}


//var_dump($miss_box_outstanding);
echo '<br>';
$miss_box_outstanding = remove_duplicates_in_array($miss_box_outstanding);

echo '<strong><br>Outstanding shares misses:<br></strong>';
$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
$file = fopen($filename,"a");
foreach ($miss_box_outstanding as $key => $position)
{
	fputs($file, "\n" . $position['name']);
	echo $key . '. ' . $position['name'] . ' ' . $position['ISIN']  . '. Adding to misslist.csv ';
	$position['land'] = $land;
	saveJSON($position, '../production_europe/isin_adder/misslist_active/outstanding_active@' . $land . '@' . $position['name'] .  '.json');
	
}
fclose($file);

echo '<br>';
$miss_box = remove_duplicates_in_array($miss_box);

echo '<strong>Isin / name misses:<br></strong>';
$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
$file = fopen($filename,"a");
foreach ($miss_box as $key => $position)
{
	fputs($file, "\n" . $position['name']);
	echo $key . '. ' . $position['name'] . ' ' . $position['ISIN']  . '. Adding to misslist.csv ';
	$position['land'] = $land;
	saveJSON($position, '../production_europe/isin_adder/misslist_active/isin_active@' . $land . '@' . $position['name'] .  '.json');

}
fclose($file);

$used_list_isin = array_unique($used_list_isin);
$used_list_companies = array_unique($used_list_companies);

//filter du dupletter med ulike case på navnene!

foreach ($used_list_companies as $key => $navn)
{
	$hitcounter = 0;
	foreach ($used_list_companies as $index => $subname)
	{
		
		if (mb_strtolower($navn) == mb_strtolower($subname))
		{

			if ($hitcounter == 0)
			{
				$hitcounter++;
				continue;
			}
			echo 'Unsetting duplicate name: ' . $used_list_companies[$index] . '<br>';
			unset($used_list_companies[$index]);

		}
	}
}

$used_list_companies = array_values($used_list_companies);
$selskaper = array();
//$selskaper[] = new selskap("BMG0451H1170", "Archer", 0.5, 7444.0, "2018-09-05T00:00:00", 27, '$posisjoner her');	

$posisjonsholder_unik = $used_list_companies;

$poskopi = $posisjoner;

$maincounter = 0;

//ikke match mot navn, match heller mot isin

//behandle hver unike navn
foreach ($posisjonsholder_unik as $key => $posisjon) {
	//build company 

	//echo $key . '. ';

	//Ta selskapnavnet
	$selskapnavnet = $posisjon;

	//Finn isin
	$posisjonsholder = array();

	//ta player-navnet

	$isin ='';
	$counter = 0;
	$last_updated;
	
	//var_dump($posisjon);

	foreach ($posisjoner as $index => $pos)  {

		//var_dump($pos);
		if (strtolower($pos->Name) == strtolower($selskapnavnet)) {

			//echo ' hit! ' . $counter . ' ' . $selskapnavnet . ' ' ;
			$isin = $pos->ISIN;
			$currency = $pos->currency;
			//echo $isin . '<br>';
			$posisjonsholder[] = $pos;	
			$counter++;
		}

	}

	$maincounter += $counter;

	//ECHO '<BR>Hitcounter: ' . $maincounter . '<br>';

	$antall_posisjoner = count($posisjonsholder);

	//Nå har vi alle aktive posisjoner, og må regne ut summen av alle prosentene og av alle aksjene.
	(float)$accumulated_short_percent = '0';
	(float)$accumulated_short_stocks = '0';


	foreach ($posisjonsholder as $index => $posisjon) {

		if ($index == 0)
		{
			$last_updated = $posisjon->ShortingDate;
		}
		else
		{
			if ($last_updated < $posisjon->ShortingDate)
			{
				//set new latest updated date
				echo ' | Setting new newest position to ' . $posisjon->ShortingDate . '<br>';
				$last_updated = $posisjon->ShortingDate;
			}
		}

		(float)$accumulated_short_percent += $posisjon->ShortPercent;
		(float)$accumulated_short_stocks += $posisjon->NetShortPosition;

	}

	$accumulated_short_percent = round($accumulated_short_percent,2);
	$accumulated_short_percent = number_format($accumulated_short_percent,2,".",",");
	//Skal selskap og posisjoner, men bare hvis det er shorting i selskapet, og bare aktive posisjoner innad i selskapet

	$selskaper[] = new selskap($isin, $selskapnavnet, $accumulated_short_percent, $accumulated_short_stocks, $last_updated, $antall_posisjoner, $posisjonsholder, $currency);

}


//remove positions with zero positions
$count = count($selskaper);
$total_count = 0;

for ($i = 0; $i < $count; $i++)
{
	//var_dump($selskaper[$i]);

	$positionscount = count($selskaper[$i]->Positions);


	if (!$positionscount > 0)
	{
		echo $i . '. Deleting position for zero positions <br>';
		unset($selskaper[$i]);
		continue;
	}

	$total_count += count($selskaper[$i]->Positions);
}

echo 'Antall posisjoner nå: ' . $total_count . '<br>';

$selskaper = array_values($selskaper);


//write current
saveJSON($selskaper, $fileCurrent);

//write history
$fileCurrent = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json';
saveJSON($selskaper, $fileCurrent);


?>