<?php

//https://www.alphavantage.co/query?function=FX_INTRADAY&from_symbol=USD&to_symbol=NOK&interval=5min&apikey=4XI39Z4NYH3C983R

//bygg urlS
date_default_timezone_set('Europe/Oslo');

include '../production_europe/functions.php';

$filenames = [];
$urls = [];
$savepath = '../production_europe/json/currency/';

$tickerBox = [];
$tickerBox[] = 'NOK' ;
$tickerBox[] = 'EUR';
$tickerBox[] = 'DKK';
$tickerBox[] = 'SEK';


foreach ($tickerBox as $key => $ticker) {

    $ticker = $ticker . '.FOREX';

    if (!$data = ticker_download_eodhistorical($ticker))
    {
        errorecho('Count not download ' . $ticker . '<br>');
    }
    else
    {
        saveJSON($data, $savepath . strtolower($ticker) . '.json' );
    }
}


?>

