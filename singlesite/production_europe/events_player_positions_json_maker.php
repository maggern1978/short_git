<?php


//$land = 'denmark';
//$land = 'germany';

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

if (!$diff = readJSON('../production_europe/json/events/diff/' . $land . '.diff.json'))
{
	errorecho('Cannot read diff json file!');
	return;
}

$allpositions = $diff['positions'];

saveJSON($allpositions, '../production_europe/json/events/player/' . $land . '.events.player.current.json');

?>
