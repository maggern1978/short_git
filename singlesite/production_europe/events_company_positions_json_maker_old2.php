<?php 

//$land = 'denmark';
$land = 'germany';

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

if (!$diff = readJSON('../production_europe/json/events/diff/' . $land . '.diff.json'))
{
	errorecho('Cannot read diff json file!');
	return;
}

if (!$previous = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $diff['previousfile_date'] . '.' . $land . '.json'))
{
	errorecho('Cannot read previousfile_date json file!');
	return;
}

if (!$current = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $diff['currentfile_date'] . '.' . $land . '.json'))
{
	errorecho('Cannot read currentfile_date json file!');
	return;
}


echo 'Positions in: ' . count($diff['positions']) . '<br>';
$company_isin_box = [];
$company_name_box = [];

foreach ($diff['positions'] as $entry)
{

	$company_isin_box[] = mb_strtolower($entry['ISIN']);
	$company_name_box[mb_strtolower($entry['ISIN'])] = $entry['Name'];

}

$company_isin_box = array_unique($company_isin_box);
$company_isin_box = array_values($company_isin_box);

$changesCompanyBox = [];

//ta hvert navn og finn shortprosent i current og previous posisjoner
foreach ($company_isin_box as $key => $isin)
{
	echo $key . '. Doing ' . $isin . ' | ' . $company_name_box[$isin] . ' | ' ;

	$current_short_percent = 0; 
	$previous_short_percent = 0; 
	$previousNumPositions = 0;

	$found_current = false; 
	$found_previous = false; 

	//find same company in previous
	foreach ($previous as $selskap)
	{

		if ($isin == mb_strtolower($selskap['ISIN']))
		{
			//go through positions 
			$shortpercent_sum = 0;
			$found_previous = true;  

			foreach ($selskap['Positions'] as $position)
			{

				$shortpercent_sum += $position['ShortPercent']; 

			}

			$previous_short_percent = $shortpercent_sum; 

			break;
		}

	}

	//find in current
	foreach ($current as $selskap)
	{

		if ($isin == mb_strtolower($selskap['ISIN']))
		{
			//go through positions 
			$shortpercent_sum = 0; 
			$found_current = true; 

			foreach ($selskap['Positions'] as $position)
			{

				$shortpercent_sum += $position['ShortPercent']; 

			}

			$previousNumPositions = count($selskap['Positions']);

			$current_short_percent = $shortpercent_sum; 

			break;
		}

	}

	$entry = $selskap;
	unset($entry['Positions']); //positions not needed
	$entry['previousShortPercent'] =  $previous_short_percent;
	$entry['previousNumPositions'] =  $previousNumPositions;
	$entry['change'] =  $current_short_percent - $previous_short_percent;
	$entry['change'] = round($entry['change'],2);


	$changesCompanyBox[] = $entry;

	echo 'Shortpercent current: ' . $current_short_percent . ' | ';
	echo 'Shortpercent previous: ' . $previous_short_percent . '<br>';

}

usort($changesCompanyBox, function($a, $b) {
    return $b['change'] <=> $a['change'];
});


echo 'Number of positions out: ' . count($changesCompanyBox) . '<br>';
saveJSON($changesCompanyBox, '../production_europe/json/events/company/' . $land . '.events.company.current.json');

//var_dump($changesCompanyBox);


?>