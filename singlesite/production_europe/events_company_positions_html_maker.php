<?php

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

//$land = 'germany';

//$land = 'italy';

//check if file is older than 3 hours, if yes, then skip. 
echo 'Checking timestamp: ';
if (get_file_age_in_hours('../production_europe/json/events/company/' . $land . '.events.company.current.json') > 2.99) 
{
   echo 'More than three hours old, will skip....<br>';
   return;
} 
else 
{
  echo 'Less than three hours old, will continue....<br>';
}


if (!$data = readJSON('../production_europe/json/events/company/' . $land . '.events.company.current.json'))
{
	echo 'Error reading json, returning.';
	return;
}

$newestdate = '2001-01-01';

foreach ($data as $company)
{
	if ($newestdate < $company['LastChange'])
	{
		$newestdate = $company['LastChange'];
	}
}



ob_start();
?>

<div class="col-12 col-sm-12 col-md-12 col-lg-6 mb-2">
	<h4>New positions, latest changes by company</h4>
	<div style="overflow-y: auto; max-height:330px;" class="pr-2">
		<table class="table table-striped table-hover table-bordered table-sm table-most-shorted">
			<thead class="thead">
				<tr>
					<th>#</th>
					<th>Company</th>
					<th class="text-right most-shorted-table-heading-a-week-ago">Change</th>					
					<th class="text-right most-shorted-table-heading-share">Short</th>
				</tr>
			</thead>
			<tbody>


			<?php // Start with new positions 
			foreach ($data as $key => $position)
			{
				
				?>
				<tr>

				<?php
				$linkcompany = nametolink($position['Name']);
				$index = $key + 1;
				$change = $position['ShortPercent'] - $position['previousShortPercent'];
				$change = round($change,2);

				if ($change > 0)
				{
					$prefix = '+';
				}
				else
				{
					$prefix = '';
				}

				echo '<td>';
				echo $index . '.';
				echo '</td>';
				echo '<td>';
				echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for company" href="';
				echo 'details_company.php?company=' . $linkcompany . '&land=' . $land;
				echo '">';
				echo $position['Name'];
				echo '</a>';
				echo '</td>';
				echo '<td class="text-right">';
				echo $prefix . number_format($change,2, '.', '')   . '&nbsp;%';
				echo '</td>';
				echo '<td class="text-right">';
				echo number_format(round($position['ShortPercent'],2),2, '.', '')  . '&nbsp;%';
				echo '</td>';
				?>

				</tr>
				<?php 

			}
			?>
	</tbody>
	</table>
</div>
</div>
<?php


//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering

if (strripos($htmlStr, 'undefined') != false or strripos($htmlStr, 'error') != false )
{
	errorecho('Error or undefined found in text, will not save');
	return;
}
else
{

	ob_end_clean(); 
	$filename = '../production_europe/html/events/company/' . $land . '.events.company.current.html';
// Write final string to file
	file_put_contents($filename, $htmlStr);

}

?>