<?php

//$land = 'norway';

include_once('../production_europe/functions.php');

if (!$data = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json'))
{
	errorecho('Cannot read json! Returning...<br>');
	return;
}

$company_count = count($data);
$csv_holder = [];

for ($i = 0; $i < $company_count; $i++)
{

	foreach ($data[$i]['Positions'] as $position)
	{

		$csv_holder[] = [$position['PositionHolder'], $data[$i]['Name'], $data[$i]['ISIN'], $position['ShortPercent'], $position['ShortingDate']];

	}

}

usort($csv_holder, function ($item1, $item2) 
{
	if (!isset($item1[4]))
	{
		return $item2;
	}

	if (!isset($item2[4]))
	{
		return $item1;
	}

	return $item1[4] < $item2[4];
});

saveCSVx($csv_holder, '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.csv');


//history
if (!$data = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json'))
{
	errorecho('Cannot read json! Returning...<br>');
	return;
}

$company_count = count($data);
$csv_holder = [];

for ($i = 0; $i < $company_count; $i++)
{

	foreach ($data[$i]['Positions'] as $position)
	{

		$csv_holder[] = [$position['PositionHolder'], $data[$i]['Name'], $data[$i]['ISIN'], $position['ShortPercent'], $position['ShortingDate']];

	}

}

usort($csv_holder, function ($item1, $item2) 
{
	if (!isset($item1[4]))
	{
		return $item2;
	}

	if (!isset($item2[4]))
	{
		return $item1;
	}

	return $item1[4] < $item2[4];
});


saveCSVx($csv_holder, '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.csv');



?>