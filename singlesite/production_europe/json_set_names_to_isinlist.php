<?php 

$millisecondsts1 = microtime(true);

//modes
//norway, sweden, sweden_history, denmark, denmark_history
//read date from generated file
$date = date('Y-m-d');
//echo $date;


include '../production_europe/functions.php';

if (!isset($mode)) {
	echo 'Error, $mode not set! Returning';
	return;
}
else {
	echo '<br>Mode is ' . $mode . '<br>';
}

if (!isset($inputFile)) {
	echo 'Error, $inputFile not set! Returning';
	return;
}
else {
	echo '$inputFile is ' . $inputFile . '<br>';
}

switch ($mode) 
{
	      case "norway":
     		$jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'; //where the copy should be
     		break;
        case "norwayhistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
     		case "sweden":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "swedenhistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "denmark":
    		$jsonfile2 = '../shorteurope-com.luksus.no/dataraw/denmark/' . $date . '.denmark.json'; //where the copy should be
    		break;
    		case "denmarkhistory":
    		$jsonfile2 = '../shorteurope-com.luksus.no/dataraw/denmark.history/' . $date . '.denmark.json'; //where the copy should be
    		break;
    		case "finland":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "finlandhistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "germany":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "germanyhistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "france":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "francehistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "italy":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "italyhistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "united_kingdom":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "united_kingdomhistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "spain":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "spainhistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;        
        case "italy":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '/' . $date . '.' . $land . '.json'; //where the copy should be
        break;
        case "italyhistory":
        $jsonfile2 = '../shorteurope-com.luksus.no/dataraw/' . $land .  '.history/' . $date . '.' . $land . '.json'; //where the copy should be
        break;  

    }

    $json = $inputFile;

 //obs, dataene må ha semikolon som skilletegn

    $name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
    $isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

    $data = file_get_contents('../shorteurope-com.luksus.no/' . $json);
    $data = json_decode($data , true);	
    $jsoncount = count($data);

    if ($jsoncount <= 0) {
    	errorecho('Nedlastningsfeil!');
    	return;
    }

    for ($x = 0; $x < $jsoncount;$x++)	 
    {

    	if (!isset($data[$x]['ISIN'])) 
        {
          var_dump($data[$x]);
          continue;
      }

      $posisjonsCount = count($data[$x]['Positions']);

      if ($posisjonsCount == 0) 
      {
          echo '<br>Zero positions, unsetting company.<br>';
          unset($data[$x]);
          continue;
      }

	   //get isin
      $isinTarget = $data[$x]['ISIN'];
      //echo 'Isintarget er ' . $isinTarget . ': ';

      $foundsuccess = 0;

      if ($row = binary_search_multiple_hits($isinTarget, '', $isin_list, $name_list, $land))
      {
        //echo 'Old name: ' . $data[$x]['Name'] . ' | New name: ' . $row[2] . '<br>';
        $data[$x]['Name'] = $row[2];
        $foundsuccess = 1;
    }
    else
    {
        errorecho($x . ' ' . $isinTarget .  '. Binary search fails! ');
        continue;
    }


    //så posisjonene
    if ($foundsuccess == 1)
    {

      $posisjonsCount = count($data[$x]['Positions']);

      for ($i = 0; $i < $posisjonsCount; $i++) 
      {
        if (isset($data[$x]['Positions'][$i])) 
        {
            $data[$x]['Positions'][$i]['Name'] = $data[$x]['Name'];
        }	
    }
}
}


$data = array_values($data);

	//navnebytte: Her må posisjonene regrupperes
echo '<br>Navnebytte: Posisjonene må regrupperes:<br>';

foreach ($data as $key => $company)
{
   $hitcount = 0;
   $nametarget = strtolower($company['Name']);
   $isintarget = $company['ISIN'];

   foreach ($data as $index => $selskap)
   {

      if ($nametarget == strtolower($selskap['Name']) or $isintarget == $selskap['ISIN'])
      {

         if ($hitcount == 0)
         {
            $hitcount++;
            continue;
        }

        $hitcount++;

			//ta alle posisjonene 
        foreach ($selskap['Positions'] as $teller => $posisjon)
        {
            $data[$key]['Positions'][] = $posisjon;
    				//echo $key . '. ' . $nametarget . '. ';
    				//echo '<strong>Flytter posisjon ' . $teller . '.</strong> ';
    				//echo '<br>';
            $data[$key]['NumPositions']++;
        }

			//slett selskapsoppføring
        unset($data[$index]);

        usort($data[$key]['Positions'], function ($item1, $item2) {
            return $item1['ShortingDate'] < $item2['ShortingDate'];
        });

    }

}



}

$data = array_values($data);
$string =  $jsonfile2;

$fp = fopen($string, 'w');
fwrite($fp, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
fclose($fp);


//write current
$fp = fopen('../shorteurope-com.luksus.no/' . $inputFile, 'w');
fwrite($fp, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
fclose($fp);

$millisecondsts2 = microtime(true);
$milliseconds_diff = ($millisecondsts2 - $millisecondsts1);  
echo 'Runtime in seconds is ' . $milliseconds_diff . '<br>';

$hovedlisten = null;
$hovedlisten_isin = null;
$inputFile = null;
$data = null;


?>