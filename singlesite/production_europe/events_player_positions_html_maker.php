<?php

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

//$land = 'germany';
//$land = 'italy';

//check if file is older than 3 hours, if yes, then skip. 
echo 'Checking timestamp: ';
if (get_file_age_in_hours('../production_europe/json/events/player/' . $land . '.events.player.current.json') > 2.99) 
{
	echo 'More than three hours old, will skip....<br>';
	return;
} 
else 
{
	echo 'Less than three hours old, will continue....<br>';
}



if (!$data = readJSON('../production_europe/json/events/player/' . $land . '.events.player.current.json'))
{
	echo 'Error reading json, returning.';
	return;
}

$newBox = [];
$updatedUpBox = [];
$updatedDownBox = [];
$endedBox = [];
$changescount = 0;
$newestdate = '2001-01-01';
$companyNameBox = [];
$playerNameBox = [];

foreach ($data as $key => $change)
{
	
	$companyNameBox[] = $change['Name'];
	$playerNameBox[] = $change['PositionHolder'];;

	$changescount++;

	if ($change['status'] == 'update')
	{
		if ($change['ShortPercent'] > $change['previousShortPercent'])
		{
			$updatedUpBox[] = $change;
		}
		else
		{
			$updatedDownBox[] = $change;
		}


	}
	else if ($change['status'] == 'new')
	{
		$newBox[] = $change;
	}
	else if ($change['status'] == 'ended')
	{
		$endedBox[] = $change;
	}
	
	//finding newest date
	if ($newestdate < $change['ShortingDate'])
	{
		$newestdate = $change['ShortingDate'];
	}

}

$companyNameBox = array_unique($companyNameBox);
$playerNameBox = array_unique($playerNameBox);

$numberOfPlayers = count($playerNameBox);
$numberOfCompanies = count($companyNameBox);
$numberOfChanges = count($data);

ob_start();

?>

<div class="col-12 col-sm-12 col-md-12 col-lg-6 mb-2">
	<h4>New positions, latest changes by player</h4>
	<div style="overflow-y: auto; max-height:330px;">
		<ul class="list-group  pr-2">

		<?php // Start with new positions 
		foreach ($newBox as $position)
		{
			$linkplayer = nametolink($position['PositionHolder']);
			$linkcompany = nametolink($position['Name']);
			?>
			<li class="list-group-item list-group-news d-flex justify-content-between">
				<div class="d-flex list-group-news-div"> 
					<div class="mr-2">
						<i class="fa fa-2x fa-star exchange-header-orange" aria-hidden="true"></i>
					</div>
					<div>
						<?php 
						echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for player" href="';
						echo 'details.php?player=' . $linkplayer . '&land=' . $land;
						echo '">';
						$playername = mb_strtolower($position['PositionHolder']);
						echo ucwords($playername, ' - ');
						echo '</a>';
						echo ' starts shorting ';
						echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for company" href="';
						echo 'details_company.php?company=' . $linkcompany . '&land=' . $land;
						echo '">';
						echo $position['Name'];
						echo '</a>';
						?>
						with a position of <?php echo round($position['ShortPercent'],2); ?>&nbsp;%. 
					</div>
				</div>
			</li>
			<?php 
		}
		?>

		<?php

		foreach ($updatedUpBox as $position)
		{
			$linkplayer = nametolink($position['PositionHolder']);
			$linkcompany = nametolink($position['Name']);
			$change = $position['ShortPercent'] - $position['previousShortPercent'];
			?>
			<li class="list-group-item list-group-news d-flex justify-content-between">
				<div class="d-flex list-group-news-div"> 
					<div class="mr-2">
						<i class="fa fa-2x fa-arrow-circle-up exchange-header-up" aria-hidden="true"></i>
					</div>
					<div>
						<?php 
						echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for player" href="';
						echo 'details.php?player=' . $linkplayer . '&land=' . $land;
						echo '">';
						$playername = mb_strtolower($position['PositionHolder']);
						echo ucwords($playername, ' - ');
						echo '</a>';
						echo ' increases ';
						echo abs(round($change,2));
						echo ' % in ';
						echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for company" href="';
						echo 'details_company.php?company=' . $linkcompany . '&land=' . $land;
						echo '">';
						echo $position['Name'];
						echo '</a>';
						?>
						to <?php echo round($position['ShortPercent'],2); ?>&nbsp;%. 

					</div>
				</div>
			</li>
			<?php 
		} 
		?>

		<?php

		foreach ($updatedDownBox as $position)
		{
			$linkplayer = nametolink($position['PositionHolder']);
			$linkcompany = nametolink($position['Name']);
			$change = $position['ShortPercent'] - $position['previousShortPercent'];
			?>
			<li class="list-group-item list-group-news d-flex justify-content-between">
				<div class="d-flex list-group-news-div"> 
					<div class="mr-2">
						<i class="fa fa-2x fa-arrow-circle-down exchange-header-down" aria-hidden="true"></i>
					</div>
					<div>
						<?php 
						echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for player" href="';
						echo 'details.php?player=' . $linkplayer . '&land=' . $land;
						echo '">';
						$playername = mb_strtolower($position['PositionHolder']);
						echo ucwords($playername, ' - ');
						echo '</a>';
						echo ' reduces ';
						echo abs(round($change,2));
						echo ' % in ';
						echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for company" href="';
						echo 'details_company.php?company=' . $linkcompany . '&land=' . $land;
						echo '">';
						echo $position['Name'];
						echo '</a>';
						?>
						to <?php echo round($position['ShortPercent'],2); ?>&nbsp;%. 

					</div>
				</div>
			</li>
			<?php 
		} 
		?>

		<?php // Start with new positions 
		foreach ($endedBox as $position)
		{
			$linkplayer = nametolink($position['PositionHolder']);
			$linkcompany = nametolink($position['Name']);
			?>
			<li class="list-group-item list-group-news d-flex justify-content-between">
				<div class="d-flex list-group-news-div"> 
					<div class="mr-2">
						<i class="fa fa-2x fa-times exchange-header-violet" aria-hidden="true"></i>
					</div>
					<div>
						<?php 
						echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for player" href="';
						echo 'details.php?player=' . $linkplayer . '&land=' . $land;
						echo '">';
						$playername = mb_strtolower($position['PositionHolder']);
						echo ucwords($playername, ' - ');
						echo '</a>';
						echo ' is listed with no short in ';
						echo '<a class="text-event" data-toggle="tooltip" title="See all active positions for company" href="';
						echo 'details_company.php?company=' . $linkcompany . '&land=' . $land;
						echo '">';
						echo $position['Name'];
						echo '</a>';
						?>
						after a previous position of <?php echo round($position['ShortPercent'],2); ?>&nbsp;%. 
					</div>
				</div>
			</li>
			<?php 
		}
		?>
		
	</ul>
</div>
</div>

<?php

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering

if (strripos($htmlStr, 'undefined') != false or strripos($htmlStr, 'error') != false )
{
	errorecho('Error or undefined found in text, will not save');
	return;
}
else
{

	ob_end_clean(); 
	$filename = '../production_europe/html/events/player/' . $land . '.events.player.current.html';
// Write final string to file
	file_put_contents($filename, $htmlStr);

}

?>
