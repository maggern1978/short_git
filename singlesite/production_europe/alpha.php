<?php 

//Denne php-filen produserer shortlisten_land-filene
//_eodhistorical-version

//$land = 'united_kingdom';
//$land = 'sweden';
//$land = 'denmark';
//$land = 'norway';

//set timeout
set_time_limit(5000);

include_once('../production_europe/logger.php');
include_once('../production_europe/functions.php');

$basecurrency = get_base_currency($land);
$date = date('Y-m-d');

$json = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';
$file2 = '../shorteurope-com.luksus.no/datacalc/shortlisten_' . $land . '_current.csv';
$isinlisten = '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv';
$isinlisten_isinsortert = '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv';

require '../production_europe/functions.php';

if (!$data = json_decode(file_get_contents($json),true))
{
	echo 'Nedlastningsfeil!';
	logger('Kan ikke lese json-fil', 'Error i alpha.php. Land er ' . $land);
	return;
}

//gå igjennom og slett oppføring med status 4, som er betyr strøket
//trim også navn!

if ($land == 'norway')
{
	foreach ($data as $key => $selskap)	 {
		
		foreach ($selskap['Positions'] as $index => $position) {
			 //var_dump($position);
			$data[$key]['Positions'][$index]['PositionHolder'] = trim($position['PositionHolder']);

			if (isset($position['Status']) and $position['Status'] == 4) {
				unset ($data[$key]['Positions'][$index]);
			}
		}
	}
}

$data = array_values($data);

if (!$isinlisten = readCSV($isinlisten))
{
	echo 'could not read hovedlisten';
	logger('Kan ikke lese hovedlisten', 'Error i alpha.php. Land er ' . $land);
	return;
}

if (!$isinlisten_isinsortert = readCSV($isinlisten_isinsortert))
{
	echo 'could not read hovedlisten $isinlisten_isinsortert';
	logger('Kan ikke lese $isinlisten_isinsortert. ', 'Error i alpha.php. Land er ' . $land);
	return;
}

//finn isin-nummeret
$isin_posisjon_holder = array();
$tickers = array();
$error = array();
$names = array();
$percentages = array();
$ShortedSum = array();
$ISIN = array();
$shortOppdatering= array();
$objectBox = array();

//Les inn relevant data fra alle med aktive short og push verdier hvis isin finnes i hovedlisten
$isincounter = 0;
$errorcounter = 0;

echo 'Doing ' . count($data) . ' companies. <br>';
timestamp();

foreach ($data as $key => $entries) {

	if ($entries['ShortPercent'] > 0) {
		//echo 'Searching for ' . $entries['Name'] . ': ';

		//echo $entries['Name'] . ' ' . $entries['ISIN'] . '<br>';
				
		if ($selskap = binary_search_multiple_hits($entries['ISIN'], $entries['Name'], $isinlisten_isinsortert, $isinlisten, $land))
		{
			$error = 0;		
			$push = $selskap[1];
			$push .= '.' . $selskap[8];	
			$push = str_replace(' ', '-', $push);
			$page = new stdClass();

	    	//ta navnet fra isinlisten! Ellers blir det feil
			$page->name = $selskap[2];
			$page->verdiendring = '';
			$page->ticker = $push;
			$page->ShortPercent = $entries['ShortPercent'];
			$page->ShortedStocks = $entries['ShortedSum'];
			$page->ISIN = $entries['ISIN'];
			$page->shortOppdatering = strtotime($entries['LastChange']);
			$page->kursendring = '';
			$page->kursAksje = '';
			$page->sist_oppdatert = '';
			$page->volume = '';
			$page->change_in_percent = '';
			$page->shortOppdatering = date("Y-m-d", $page->shortOppdatering); 
			$page->currency = $selskap[3];
			$objectBox[] = $page;
		}
		else
		{	

			errorecho($key . '. Fant ikke: ' . $entries['Name'] . ' ' . $entries['ISIN'] . '<br>');
			saveJSON($entries, '../production_europe/isin_adder/misslist_active/from_alpha_php@' . $land . '@' . $entries['Name'] .  '.json');
			echo '<br>';
			$error = 'Alpha.php: Fant ikke: ' . $entries['ISIN'];			
		}

		if ($error == 1) {
	
			//logger($entries['Name'], $error);
		}
	}
	$isincounter++;
}


$countObjectBox = count($objectBox); 
$allTickers = [];

for ($i = 0; $i < $countObjectBox; $i++) {
	$allTickers[] = $objectBox[$i]->ticker;
}

$bulkTickerData = ticker_download_eodhistorical($allTickers);

//function 
$errorbox = [];

for ($i = 0; $i < $countObjectBox; $i++) {
	
	flush_start();

	$ticker = $objectBox[$i]->ticker;

	echo $i . '. Ticker er ' . $ticker . '. Company: ' . $objectBox[$i]->name . '. ';

	//look for data in array already downloaded, if not try download it manually;
	$foundSuccess = 0;
	$data = [];

	foreach ($bulkTickerData as $key => $company)
	{
		
		if ($ticker == $company['code'])
		{
			
			$data = $company;
			echo ' Fant i bulk data.';
			$foundSuccess = 1;
			break;
		}
	}

	if ($foundSuccess == 0)
	{
		errorecho('Ticker not found in bulk downloaded data, trying to single download.');
		$data = ticker_download_eodhistorical($ticker);
	}


	if (!isset($data['close']) or $data['close'] == 0)
	{
		echo $ticker . '---------------> error download <br>';

		    //logger('error downloading in alpha.php after trying both finnhub and yhaoo', '. Ticker er ' . $ticker);
		sleep(1);
		continue;
	}

	successecho(' Successfull<br>');

	$objectBox[$i]->sist_oppdatert = $data['timestamp'];
	$objectBox[$i]->kursAksje = (float)$data['close'];
	$objectBox[$i]->volume = $data['volume'];
	$endring = $data['change'];
	$verdi = (-$objectBox[$i]->ShortedStocks)*$endring;
	$objectBox[$i]->verdiendring = round($verdi,0);
	$objectBox[$i]->kursendring = (float)$endring;
	$objectBox[$i]->change_in_percent = $data['change_p'];
	$objectBox[$i]->change_in_percent = round($objectBox[$i]->change_in_percent,5);

	if ($objectBox[$i]->currency != $basecurrency)
	{
		echo 'Currency is not base currency: ' . $objectBox[$i]->currency . ' vs ' . $basecurrency . '<br>';

		if (!$multiply_factor = get_currency_multiply_factor($objectBox[$i]->currency, $basecurrency))
		{
			errorecho('Error getting multiply factor for currency, will unset<br>');
			unset($objectBox[$i]);
			continue;
		}

		echo 'Multiply factor is ' . $multiply_factor . '. ';

		$objectBox[$i]->base_currency_multiply_factor = $multiply_factor;
		$objectBox[$i]->verdiendring_base_currency = $objectBox[$i]->verdiendring * $multiply_factor;
	}
	else
	{
		$objectBox[$i]->verdiendring_base_currency = $objectBox[$i]->verdiendring;
		$objectBox[$i]->base_currency_multiply_factor = 1;
	}

	flush_end();
}

errorecho('<br>Not set: ');

//filter out
foreach ($objectBox as $key => $object)
{
	if (!isset($object->verdiendring_base_currency))
	{
		
		errorecho($key . '. Fant ikke: ' . $object->name . ' ' . $object->ISIN . '. Unsetting!<br><br>');
		saveJSON($object, '../production_europe/isin_adder/misslist_active/from_alpha_php@' . $land . '@' . $object->name . '_'. $object->ISIN  . '.json');
		//var_dump($object);	
		unset($objectBox[$key]);
	}
}

$objectBox = array_values($objectBox);

usort($objectBox,function($first,$second){
	return $first->verdiendring_base_currency < $second->verdiendring_base_currency;
});

//prepare array for csv export
$file_export = array(
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),
	array(),                                 
	array(), 
	);

$countedNew = count($objectBox);

$runner = 0; 
foreach ($objectBox as $counter => $object) 
{

	if (!isset($object->verdiendring_base_currency))       
	{
		var_dump($object);
		echo 'Unsetting...verdiendring mangler...<br>';
		unset($objectBox[$counter]);
		continue;
	}

	$file_export[$runner][0] = $object->name;
	$file_export[$runner][1] = $object->verdiendring;
	$file_export[$runner][2] = $object->ticker;
	$file_export[$runner][3] = $object->ISIN;
	$file_export[$runner][4] = $object->ShortedStocks;
	$file_export[$runner][5] = $object->ShortPercent;
	$file_export[$runner][6] = $object->kursAksje;
	$file_export[$runner][7] = $object->kursendring;
	$file_export[$runner][8] = $object->sist_oppdatert;
	$file_export[$runner][9] = $object->shortOppdatering;
	$file_export[$runner][10] = $object->volume;

        //prosentvis endring
	$file_export[$runner][11] = round((float)$object->change_in_percent,3); 
	$file_export[$runner][12] = $object->currency;
	$file_export[$runner][13] = $basecurrency;
	$file_export[$runner][14] = $object->verdiendring_base_currency; 
	$file_export[$runner][15] = $object->base_currency_multiply_factor;
	$runner++;
}

    //write header info
$header = array(
	array('Navn', 'Verdiendring', 'Ticker', 'Isin', 'Antall shortede aksjer', 'Prosent short', 'KursAksje', 
		'KursendingPrAksje','Sist oppdatert aksjekurs', 'Siste oppdatert shortdata', 'Volum', 'Kursendring i prosent','currency','basecurrency','verdiendring_i_base_currency','base_currency_multiply_factor'));

    //merge header aaray
$file_export = array_merge($header, $file_export);
echo 'Saving file ' . $file2 . '<br>';

    //delete empty lines
foreach ($file_export as $key => $row)
{
	if (!isset($row[0]))
	{
		unset($file_export[$key]);
	}
}

$file_export = array_values($file_export);

saveCSVx($file_export, $file2);

    //send errormessages!
    //loggerarray($errorbox, 'alpha.php');

?>