<?php

include '../production_europe/functions.php';

//read isin list
$data = readCSV('../shorteurope-com.luksus.no/dataraw/isin/history_sublist.csv');

if (count($data) < 1)
{
	errorecho('Error reading isin list<br>');
	return;
}

$endingBox = [];

foreach ($data as $key => $row)
{
	$endingBox[] = $row[8];
}

$endingBox = array_unique($endingBox);
$endingBox = array_values($endingBox);

//make sure directories exists

foreach ($endingBox as $key => $ending)
{

	$filename = '../production_europe/history_bulk/' . $ending;
	$filename = strtolower($filename);

	if (!file_exists($filename)) 
	{
	    mkdir($filename, 0777);
	    //echo "The directory $filename was successfully created.";

	} 
}

//downloading 

foreach ($endingBox as $key => $ending)
{

	if ($data = eod_bulk_download($ending))
	{

		//find newest date in stock info

		$startdate = $data[0]['date'];

		foreach ($data as $index => $stock)
		{
			if ($stock['date'] > $startdate)
			{
				$startdate = $stock['date'];
			}
		}

		saveJSON($data, '../production_europe/history_bulk/' . strtolower($ending)  . '/' . strtolower($ending) . '_current.json');
		saveJSON($data, '../production_europe/history_bulk/'. strtolower($ending) . '/' . strtolower($ending) . '_' . $startdate . '.json');
	}
}


?>