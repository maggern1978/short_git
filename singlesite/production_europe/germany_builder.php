<?php //Filene som skal kjøres
//include '../production_europe/functions.php';
//set error handler to pick up any error
	
	include '../production_europe/run.php';

	//laster ned aktive og historiske, og lager historisk. Tar så aktive fra historiske og lager til current. 

	$land = 'germany';

	echo '<br>germany_downloader starter <br>';
	include '../production_europe/germany_downloader.php';
	echo '<br>germany_downloader done <br>';

	//Konverter nedlastet fil til csv
	// Ta alle de aktive posisjonene og legg til de er aktive i csv-filen
	run('../production_europe/setActivePositions.php');

	run('../production_europe/mergeCSV.php');

	$inputFileName = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_merged.csv';

	run('../production_europe/csv_to_json_builder_history.php');

	run('../production_europe/csv_to_json_history_positions_cleaner.php');

	run('../production_europe/get_active_from_history.php');

	echo '<br><strong>filter_out_old_positions.php starter... </strong><br>';
	include '../production_europe/filter_out_old_positions.php'; //gjør om csv-filen til en json-fil med alle aktive posisjoner
	echo '<br>filter_out_old_positions.php done<br>';

	//gjør om navnene fra myndigheter til det som står i hovedlisten for selskaper. 

	$mode = 'germany';
	$inputFile = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.json';
	echo '../production_europe/json_set_names_to_isinlist.php starter. <br>';
	
	include('../production_europe/json_set_names_to_isinlist.php');
	echo '../production_europe/json_set_names_to_isinlist.php DONE. <br>';
	
	echo '<br> ' . $land . '_builder.php done!<br>';
?>