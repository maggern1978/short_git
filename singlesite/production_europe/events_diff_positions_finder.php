<?php 

//$land = 'sweden';
//$land = 'germany';

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

echo '<br>';

$limit_date_previous = '2020-01-01';

$date = date('Y-m-d');

while (!file_exists('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
		//echo $date . '<br>';
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

	if ($date < $limit_date_previous)
	{
		echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
		return;
	}
}	

//check if file is older than 3 hours, if yes, then skip. 
echo 'Checking timestamp: ';
if (get_file_age_in_hours('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json') > 2.99) 
{
	echo 'More than three hours old, will skip....<br>';
	return;
} 
else 
{
	echo 'Less than three hours old, will continue....<br>';
}


echo 'Reading new positions: '. '../shorteurope-com.luksus.no/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json</strong><br>';

//read todays positions
if (!$currentdata = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
	errorecho('Error reading json ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
	return;
}
$currentfilename = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json';
$date_current_read = $date;


//find the newest position date
$newestdate = '2001-01-01';

foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $key => $position)
	{
		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}
}


//echo 'Previous working day of that is ' . $previousday . '<br>';
if ($newestdate == date('Y-m-d', (strtotime ('-1 weekday', strtotime ($date)))))
{
	echo 'Dates match. File has positions with timestamp from previous weekday (' . $newestdate .'). <br>';
}
else
{
	echo 'Newest date in current positions is ' . $newestdate . '.<br>';
	echo('Dates DOES NOT match. Filename does not have positions with timestamp from previous weekday. Probably no new positions?<br>');
}

echo '<br><strong>Finding file with newest position that is older OR EQUAL than date ' . $newestdate . ' found for current.json:</strong> <br>';


$previous_newest_date = $newestdate;

$date = date('Y-m-d', strtotime("+1 weekday", strtotime($newestdate)));

while ($previous_newest_date == $newestdate)
{

	//finding the previous file
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	

	while (!file_exists('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		
		$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

		if ($date < $limit_date_previous)
		{
			echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
			return;
		}
	}	

	//cannot ble same filename as current
	if ($currentfilename == '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json')
	{
		continue;
	}

	echo '--| File is ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'. '<br>';

	//read previous positions
	if (!$previousdata = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		errorecho('Error reading previous json ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
		continue;
	}


	//finding newest date
	//find the newest position date
	
	$previous_newest_date = '2001-01-01';

	foreach ($previousdata as $key => $company)
	{
		foreach ($company['Positions'] as $key => $position)
		{
			if ($position['ShortingDate'] > $previous_newest_date)
			{
				$previous_newest_date = $position['ShortingDate'];
			}
		}
	}

	if ($previous_newest_date <= $newestdate)
	{
		successecho('--| Success. Newest date in previous positions is ' . $previous_newest_date . '.<br><br>');
		$date_previous_read = $date;
		break;
	}

}


echo 'Reading previous positions: ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json </strong>because that file has positions from the day before.<br> ';
echo '<br>';


$oldpositions = [];

foreach ($previousdata as $selskap)
{
	foreach ($selskap['Positions'] as $position)
	{
		$oldpositions[] = $position;
	}
}

$currentpositions = [];

foreach ($currentdata as $selskap)
{
	foreach ($selskap['Positions'] as $position)
	{
		$currentpositions[] = $position;
	}
}


$keepcontainer = [];

echo 'New positions: ' . count($currentpositions) .'<br>';
echo 'Old positions: ' . count($oldpositions) .'<br>';
echo 'Removing duplicates.<br>';

//se om ny posisjon også er i gammel

foreach ($currentpositions as $key => $currentposition)
{

	foreach ($oldpositions as $index => $oldposition)
	{

		if ($currentposition['ISIN'] == $oldposition['ISIN'] 
			and mb_strtolower($currentposition['PositionHolder']) == mb_strtolower($oldposition['PositionHolder']) 
			and round($currentposition['ShortPercent'],2) == round($oldposition['ShortPercent'],2))
		{
			//echo $key . '. Duplicate position found new vs old. Unsetting both.<br>';
			unset($currentpositions[$key]);
			unset($oldpositions[$index]);
			break;
			
		}

	}

}

echo 'New positions: ' . count($currentpositions) .'<br>';
echo 'Old positions: ' . count($oldpositions) .'<br>';
echo 'Finding updated positions<br>';

foreach ($currentpositions as $key => $currentposition)
{

	foreach ($oldpositions as $index => $oldposition)
	{

		if ($currentposition['ISIN'] == $oldposition['ISIN'] or mb_strtolower($currentposition['Name']) == mb_strtolower($oldposition['Name']))
		{

			if (mb_strtolower($currentposition['PositionHolder']) == mb_strtolower($oldposition['PositionHolder']))
			{

			echo $key. '. Found same player name and company in previous data. Setting current positions to updated and pushing to keepcontainer. Deleteing positions in old and previous positions<br>';
			$currentpositions[$key]['status'] = 'update';
			$currentpositions[$key]['previousShortPercent'] = round($oldposition['ShortPercent'],2);
			$keepcontainer[] = $currentpositions[$key];

			unset($currentpositions[$key]);
			unset($oldpositions[$index]);
			}
		}

	}

}


echo 'Remaining current positions are new. Adding ' . count($currentpositions) . ' positions. <br>';

foreach ($currentpositions as $key => $currentposition)
{

	$currentpositions[$key]['status'] = 'new';
	$currentpositions[$key]['previousShortPercent'] = 0;
	$keepcontainer[] = $currentpositions[$key];
	unset($currentpositions[$key]);

}

echo 'Remaining previous positions are ended. Adding ' . count($oldpositions) . ' positions. <br>';

foreach ($oldpositions as $key => $previousposition)
{

	$oldpositions[$key]['status'] = 'ended';
	$oldpositions[$key]['ShortPercent'] = 0;
	$keepcontainer[] = $oldpositions[$key];
	unset($oldpositions[$key]);

}

echo 'Positions out: ' . count ($keepcontainer) . '<br>';

$savecontainer = [];
$savecontainer['currentfile_date'] = $date_current_read;
$savecontainer['previousfile_date'] = $date_previous_read;
$savecontainer['positions'] = $keepcontainer;

saveJSON($savecontainer, '../production_europe/json/events/diff/' . $land . '.diff.json');

//var_dump($savecontainer);

?>