<?php 

//$land = 'sweden';
//$land = 'united_kingdom';
//$type = 'loser';

include '../production_europe/options_set_currency_by_land.php';


$selskapsliste = $land . '/' . $land . '_current.json';
$playerliste = '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
$saveUrlWinner = '../shorteurope-com.luksus.no/datacalc/toplist/' . $land . '/winner/';
$saveUrlLoser = '../shorteurope-com.luksus.no/datacalc/toplist/' . $land . '/loser/';
$loserFileName = 'loser.' . $land . '.current.json';
$winnerFileName = 'winner.' . $land . '.current.json';



//Last inn vinner
if ($type == 'winner') {
    $url = $saveUrlWinner . $winnerFileName;
    $string = file_get_contents($url);
    $data = json_decode($string, true);    
    $startord = 'Earned';
    $nullWords = 'No winners';
}

if ($type == 'loser') {
    $url = $saveUrlLoser . $loserFileName;
    $string = file_get_contents($url);
    $data = json_decode($string, true);
    $startord = 'Lost';
    $nullWords = 'No losers';
}

require '../production_europe/namelink.php';

// Turn on output buffering
ob_start();

$antall = count($data);

if ($antall == 0) 
{
    echo '<li class="list-group-item">';
    echo $nullWords;
    echo '</li>';
}
else 
{

    foreach ($data as $key => $player) { ?>

       <li class="list-group-item">
       <div>
       <div class="index" style="height:46px;"><?php echo $key + 1; ?></div>
        <div style="overflow: hidden; height: 24px;">
        <?php
        $player['playerName'] = strtolower($player['playerName'] ) ;
        $player['playerName']  = ucwords($player['playerName'] );
        $name = $player['playerName']; 
        $name = nametolink($player['playerName']);
        echo '<span class="font-weight-bold">';
        echo '<a';
        echo ' data-toggle="tooltip" title="' . $player['playerName'] . '"';
        echo 'href="details.php?player=' . $name . '&land=' . $land . '">';
        
        
        echo  $player['playerName']   . '</a>';
        echo '</span>';
        ?>
        </div>
      
       <?php

       $temp = abs($player['change']);
       echo $startord . ' ' . number_format($temp,2,".",",") . ' M ' . $currency_ticker .  ' in '; 
       echo $player['numberOfPositions']; 

       if ($player['numberOfPositions'] > 1) {
        echo ' positions';
       
       }
       else {
         echo ' position';
       }

       ?>  
       </li>

        <?php 
        if ($key > 2) 
        {
            break;
        }
} 

}

$filenameStart = '../shorteurope-com.luksus.no/winner_loser_display_';
$filenameCountry = $land;
$filenameType = $type;
$filenameEnd = '.html';
$filename = $filenameStart . $filenameCountry . '_' . $filenameType . $filenameEnd;

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename , $htmlStr);


?>