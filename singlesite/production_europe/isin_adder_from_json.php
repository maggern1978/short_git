<?php

set_time_limit(500);
include_once '../production_europe/functions.php';

if (!$files = listfiles('../production_europe/isin_adder/misslist'))
{
	errorecho('Error reading missdirectory, returning!<br>');
	return;
}

$searchbox = [];

foreach ($files as $key => $file)
{
	
	if (!$data = readJSON('../production_europe/isin_adder/misslist/' . $file))
	{
		errorecho($key . '. Error reading ' . $file . '<br>');
		continue;
	}

	//var_dump($data);

	if (isset($data['name']))
	{
		$name = namecleaner($data['name']);
		//var_dump($companyname);
	}
	else 
	{
		$name = false;
	}

	if (isset($data['ISIN']) and is_isin($data['ISIN']))
	{
		$isin = $data['ISIN'];
		//var_dump($isin);
	}
	else 
	{
		$isin = false;
	}
	
	$object = new stdClass;
	$object->ISIN = $isin;
	$object->name = $name;
	$searchbox[] = (array)$object;

	if ($key > 5)
	{
		break;
	}
}

$resultbox = [];
$searchresult = [];
$missbox = [];

foreach ($searchbox as $key => $entry)
{
	
	echo $key . '. ';

	if ($entry['ISIN'])
	{
		echo 'Searching for ' . $entry['ISIN'] . '. ';
		$isin_search_result = searchname($entry['ISIN']);
		
		if ($isin_search_result != '')
		{
	
			foreach ($isin_search_result as $result)
			{
				$searchresult[] = $result;
			}

		}
		else
		{
			errorecho('No results! ');
			$missbox[] = $entry['ISIN'];
		}		
	}

	if ($entry['name'])
	{
		echo 'Searching for ' . $entry['name'] . '. ';

		$searchstring = str_replace(' AB', '', $entry['name']);

		$name_search_result = searchname($entry['name']);

		if ($name_search_result != '')
		{
			foreach ($name_search_result as $result)
			{
				$searchresult[] = $result;
			}
		}
		else
		{
			errorecho('No results! ');
			//$missbox[] = $entry['companyname'];
		}
		
	}
	echo '<br>';
}

var_dump($searchresult);

foreach ($searchbox as $key => $entry)
{

	$searchresult_finnhub[] = search_finnhub($entry['ISIN']);
	$searchresult_finnhub[] = search_finnhub($entry['name']);


}

exit();

$searchresult_finnhub = [];

var_dump($missbox);

foreach ($missbox as $key => $entry)
{
	$result = search_finnhub($entry);
	$searchresult_finnhub[] = $result;
}

var_dump($searchresult_finnhub);
saveJSON($searchresult_finnhub, '../production_europe/isin_adder/finnhub_search_result.json');


function search_finnhub($string)
{

	//https://finnhub.io/api/v1/search?q=US5949181045&token=bp8pnvnrh5racm7obvp0

	$starturl = 'https://finnhub.io/api/v1/search?q=' . $string;
	$endurl = '&token=bp8pnvnrh5racm7obvp0';

	$url = $starturl . $endurl;

	$download = download($url);

	if ($data = json_decode($download, true))
	{
		return $data;
	}
	else
	{
		//var_dump($download);
		return false; 
	}	

}



function searchname($string)
{

	$token = '5da59038dd4f81.70264028';
	$starturl = 'https://eodhistoricaldata.com/api/search/';
	$endurl = '?api_token=' . $token;

	$url = $starturl . $string . $endurl;

	$download = download($url);

	if ($data = json_decode($download, true))
	{
		return $data;
	}
	else
	{
		//var_dump($download);
		return false; 
	}

}

function getfundamentals($ticker)
{

	$token = '5da59038dd4f81.70264028';
	$starturl = 'https://eodhistoricaldata.com/api/fundamentals/';
	$endurl = '?api_token=' . $token;
	$url = $starturl . $ticker. $endurl;

	if ($data = json_decode(download($url), true))
	{
		return $data;
	}

	else
	{
		return false; 
	}
}

function namecleaner($string)
{

	$array = [', L.P.', ' LP', ', S.A.', ' LLP', ' LP'];

	//$string = strtolower($string);

	foreach ($array as $text)
	{
		$string = str_ireplace($text, '', $string);
	}

	return trim($string);
}



$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
include_once '../production_europe/functions.php';
$isinliste = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv');
$isinliste_isinsortert = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv');

$file = fopen($filename,"r");

$csvdata = [];

while(!feof($file))
{
   $csvdata[] = fgets($file);
}

$csvdata = array_unique($csvdata);
$csvdata = array_values($csvdata);

fclose($file);

$keepContainer = [];

foreach ($csvdata as $key => $isin)
{

	$isin = trim($isin);

	if ($isin == '')
	{
		continue;
	}

	$row = '';

	if (is_isin(trim($isin)))
	{
		//try finding isin in isinlist

		if ($row = binary_search_isinliste_isin($isin, $isinliste_isinsortert)) //$needle, $haystack
		{
			echo $key . '. ' . $isin . ' Is isin, found in isinlist!';
			var_dump($isinliste_isinsortert[$row]);
			$row = $isinliste_isinsortert[$row];
		}
		else
		{
			echo $key . '. ' . $isin . ' Is isin, NOT found in isinlist!';
		}

	}
	else
	{
		echo 'not isin!<br>';
	}

	
	echo $key . '. searching for "' . $isin . '" ';

	if ($data = searchname($isin))
	{
		successecho(' success');
		$keepContainer[] = $data;
		var_dump($data);
	}
	else
	{
		errorecho('Search eodhistorical for ISIN gives no result. ');

		if (is_array($row))
		{

			$ticker = $row[1] . '.' . $row[8];

			$ticker = str_replace(' ', '-', $ticker);

			echo 'Searching for ' . $ticker . '<br>';

			if ($data = searchname($ticker))
			{
				successecho(' success');
				var_dump($data);
				$keepContainer[] = $data;

			}
		}
		else
		{
			echo 'False!';
		}
	
	
		

		//https://finnhub.io/api/v1/search?q=SE0004109627&token=bp8pnvnrh5racm7obvp0

	}
	echo '<br>';
	exit();
}

	if ($data = searchnamefinnhub(trim($isin)))
	{
		

	}
















saveJSON($keepContainer, '../production_europe/isin_adder/search_result.json');

$csvList = [];
$outstandingList = [];

foreach ($keepContainer as $selskap)
{
	$selskap = $selskap[0];	
	$ticker = $selskap['Code'] . '.' . $selskap['Exchange'];
	echo 'Ticker er ' . $ticker . ': ';

	if (!$fundamental = getfundamentals($ticker))
	{
		echo 'Fundamental download error, continuing<br>';
		continue;
	}

	saveJSON($fundamental, '../production_europe/json/fundamentals/' . $ticker . '.json');

	$temparray = [];
	$temparray[0]  = $selskap['ISIN'];
	$temparray[1]  = $selskap['Code'];
	$temparray[2]  = $selskap['Name'];
	$temparray[3]  = $selskap['Currency'];
	$temparray[4]  = $fundamental['General']['Sector'];
	$temparray[5]  = $fundamental['General']['Industry'];
	$temparray[6]  = $selskap['Country'];
	$temparray[7]  = $selskap['Exchange'];
	$temparray[8]  = $selskap['Exchange'];	
	
	$csvList[] = $temparray;
	$temparray[9]  = $fundamental['SharesStats']['SharesOutstanding'];

	//var_dump($fundamental['SharesStats']);

	$outstandingList[] = $temparray;

}


saveCSVsemikolon($csvList, '../production_europe/isin_adder/export_list_copy.csv');
saveCSVsemikolon($outstandingList, '../production_europe/isin_adder/export_outstanding_list_copy.csv');


//check if isin is already in list
foreach ($csvList as $key => $row)
{

	$foundsuccess = 0;

	foreach ($isinliste as $index => $isinrow)
	{
		if ($row[0] == $isinrow[0])
		{
			echo 'Isin already exists, unsetting...<br>';
			$foundsuccess = 1;
			break;
		}
	}

	if ($foundsuccess == 1)
	{
		unset($csvList[$key]);
	}
	
}

$csvList = array_values($csvList);

foreach ($csvList as $row)
{
	echo 'Adding ' . $row[2] . '<br>';
	$isinliste[] = $row;
}

usort($isinliste, function ($item1, $item2) {
    return mb_strtolower($item2[2]) < mb_strtolower($item1[2]);
});

//remove commas in names!

foreach ($isinliste as $key => $row)
{
	$isinliste[$key][2] = str_replace(',','', $row[2]);
}

echo '<br>';
saveCSVsemikolon($isinliste, '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv');
echo '<br>';


//lagre sorted array etter isin
usort($isinliste, function ($item1, $item2) {
    return $item2[0] < $item1[0];
});

echo '<br>';
saveCSVsemikolon($isinliste, '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv');
echo '<br>';

//outstanding listen
$tickerliste = readCSV('../shorteurope-com.luksus.no/datacalc/outstanding_shares_current_isinlist_main.csv' );

foreach ($outstandingList as $key => $row)
{

	$foundsuccess = 0;

	foreach ($tickerliste as $index => $isinrow)
	{
		if ($row[0] == $isinrow[0])
		{
			echo 'Isin already exists, unsetting...<br>';
			$foundsuccess = 1;
			break;
		}
	}

	if ($foundsuccess == 1)
	{
		unset($outstandingList[$key]);
	}
	
}

$outstandingList = array_values($outstandingList);

foreach ($outstandingList as $row)
{
	echo 'Adding ' . $row[2] . '<br>';
	$tickerliste[] = $row;
}

usort($tickerliste, function ($item1, $item2) {
    return mb_strtolower($item2[2]) < mb_strtolower($item1[2]);
});

//fjern kommaer!
foreach ($isinliste as $key => $row)
{
	$isinliste[$key][2] = str_replace(',','', $row[2]);
}

echo '<br>';
saveCSVsemikolon($tickerliste, '../shorteurope-com.luksus.no/datacalc/outstanding_shares_current_isinlist_main.csv');
echo '<br>';

//lagre sorted array etter isin
usort($tickerliste, function ($item1, $item2) {
    return $item2[0] < $item1[0];
});

echo '<br>';
saveCSVsemikolon($tickerliste, '../shorteurope-com.luksus.no/datacalc/outstanding_shares_current_isinlist_main_isinsortert.csv');
echo '<br>';

echo 'Resetting input file: ' . $filename . '<br>';
//$file = fopen($filename,"w");
//fclose($file);


?>