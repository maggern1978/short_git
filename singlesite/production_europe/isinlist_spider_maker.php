<?php

include_once('../production_europe/logger.php');
include_once('../production_europe/functions.php');
include_once('../production_europe/functions_spider.php');
print_mem();

$date = date('Y-m-d');
set_time_limit(800);

//$land = 'united_kingdom';

$history = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.csv'; //source for the positions. 
$active = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $land . '_current.csv'; //source for the positions. 
$spider_save_filename = '../production_europe/json/isin_spider/' . $land . '.spider.json'; //save

//les inn datane
$active = readCSVkomma($active);

//les inn datane
$history = readCSVkomma($history);

//join inn
$csvdata = array_merge($active, $history);

$csvdata = sanetize_csv_data($csvdata, $land);

//les inn tickerliste 
$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

//sort dataene etter dato
usort($csvdata, function ($item1, $item2) 
{
	if (!isset($item1[4]))
	{
		return $item2;
	}

	if (!isset($item2[4]))
	{
		return $item1;
	}

	return $item1[4] < $item2[4];
});

//gjør alt lowercase
$csvdata_count = count($csvdata);

for ($i = 0; $i < $csvdata_count; $i++)
{
	if (isset($csvdata[$i][1]))
	{
		$csvdata[$i][1] = mb_strtolower($csvdata[$i][1]);
	}
	
}




echo 'Positions in: ' . $csvdata_count . '<br>';

$allholder = [];

$spider_box = [];

$alternative_found_count = 0; 
$miss_cache = [];
$spider_hitbox = [];
$miss_counter = 0; 

for ($i = 0; $i < $csvdata_count; $i++)
{
	

	if (isset($miss_cache[$csvdata[$i][2]]))
	{
		//echo '<br>Misscache isin. Navn er ' . $miss_cache[$csvdata[$i][2]] . ' vs. '  . $csvdata[$i][1] . '. '; 

		if ($miss_cache[$csvdata[$i][2]] == $csvdata[$i][1])
		{
			//echo '<br>Misscache: Skipping ' . $csvdata[$i][2] . ' | ' . $csvdata[$i][1] . '<br>';
			echo 'x';
			$miss_counter++;
			continue;
		}
		
	}


	if (isset($spider_hitbox[$csvdata[$i][2]]))
	{
		$spider = $spider_hitbox[$csvdata[$i][2]];

		//echo '<br>spider_hitbox isin. Navn(ene) er "' . implode(', ', $spider->get_name_list())  . '" vs. '  . $csvdata[$i][1] . '. '; 
		//echo 'Isin and first name matches for ' . $csvdata[$i][2] . ' | ' . $csvdata[$i][1] . '. Changeing isin to first_isin<br>';
		//echo 'Hit isin : ' . $spider->get_hit_isin() . ' and hit name ' . $spider->get_hit_name() . '<br>';

		if ($spider->get_hit_isin())
		{
			$csvdata[$i][2] = $spider->get_hit_isin();
		}
		else if ($spider->get_hit_name())
		{
			$csvdata[$i][1] = $spider->get_hit_name();
		}
		
	}

	//$firstrow = binary_search_isinliste($name, $name_liste);
	if ($row = binary_search_multiple_hits($csvdata[$i][2], trimmer($csvdata[$i][1]), $isin_list, $name_list, $land))
	{
		echo '|';
	}
	else
	{
		echo '<br>' . $i . '. Did not find ' .  implode(',', $csvdata[$i]) . ', making spider. ';
		$spider_local = scanner($csvdata, $csvdata[$i][1], $csvdata[$i][2], $csvdata[$i][4]);

		$isin_list_local = $spider_local->get_isin_list();

		$count_isin_list_local = count($isin_list_local);
		$foundsuccess = 0;

		for ($x = 0; $x < $count_isin_list_local; $x++)
		{	

			if ($row = binary_search_multiple_hits($isin_list_local[$x], trimmer($csvdata[$i][1]), $isin_list, $name_list, $land))
			{
				//successecho('alternative found by ISIN search.<br>');
				//echo 'First isin: ' . $spider_local->get_first_isin() . '<br>';
				//var_dump($row);
				//var_dump($spider_local->get_isin_list());
				$spider_local->set_hit_isin($isin_list_local[$x]);

				$foundsuccess = 1;
				$alternative_found_count++;
				break;
			}

		}

		if ($foundsuccess == 0) //not found,try names
		{

			$name_list_local = $spider_local->get_name_list();

			$count_name_list_local = count($name_list_local);
			$foundsuccess = 0;

			for ($x = 0; $x < $count_name_list_local; $x++)
			{

				$target = $name_list_local[$x];

				if ($row = binary_search_multiple_hits($csvdata[$i][2], $name_list_local[$x], $isin_list, $name_list, $land))
				{
					successecho('Found by NAME search.<br>');
					//echo 'First name: ' . $spider_local->get_first_name() . '<br>';
					$spider_local->set_hit_name($name_list_local[$x]);

					//var_dump($row);
					//var_dump($spider_local->get_name_list());
					$foundsuccess = 1;
					$alternative_found_count++;	
					break;				
				}

			}

		}

		if ($foundsuccess == 0) //fails still
		{
			errorecho('Failed to find company.');
			$miss_cache[$csvdata[$i][2]] = $csvdata[$i][1];
			$miss_counter++;
		}
		else
		{
			$spider_hitbox[$csvdata[$i][2]] = $spider_local; //reuse spiders
			$spider_box[$spider_local->get_first_isin()] = $spider_local;
		}

		echo '<br>';
		
	}

	if ($i > 3100)
	{
		//break;
	}

	if ($i %  2000 == 0)
	{
		echo '<br><br>';
		saveJSON($spider_box, $spider_save_filename);
		echo '<br></b>';
	}
	
}
saveJSON($spider_box, $spider_save_filename);

//stats
echo '<br><br>-----------stats-------------<br>';
echo 'Alternative found: ' . $alternative_found_count . '<br>';
echo 'Antall posisjoner med bom: ' . $miss_counter . '<br>';
echo '<b>Misses:</b><br>';

foreach ($miss_cache as $key => $entry)
{
	echo $key . ' ' . $entry . '<br>';
}

print_mem();

?>