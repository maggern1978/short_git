<?php
//set error handler to pick up any error
set_error_handler('errHandle');

//denne filen skal kjøres en gang om dagen, nemlig når første live-story

$today = strtolower(date('l',strtotime('now')));

$targetBox = array();

if ($today == 'monday') {
	$targetBox[] = './stories/endrapport/friday/story_oslo_bors_friday.html';
	$targetBox[] = './stories/endrapport/friday/story_copenhagen_bors_friday.html';
	$targetBox[] = './stories/endrapport/friday/story_stockholm_bors_friday.html';
	$targetBox[] = './stories/endrapport/thursday/story_oslo_bors_thursday.html';
	$targetBox[] = './stories/endrapport/thursday/story_copenhagen_bors_thursday.html';
	$targetBox[] = './stories/endrapport/thursday/story_stockholm_bors_thursday.html';
}
else if ($today == 'tuesday') {
	$targetBox[] = './stories/endrapport/monday/story_oslo_bors_monday.html';
	$targetBox[] = './stories/endrapport/monday/story_copenhagen_bors_monday.html';
	$targetBox[] = './stories/endrapport/monday/story_stockholm_bors_monday.html';
	$targetBox[] = './stories/endrapport/friday/story_oslo_bors_friday.html';
	$targetBox[] = './stories/endrapport/friday/story_copenhagen_bors_friday.html';
	$targetBox[] = './stories/endrapport/friday/story_stockholm_bors_friday.html';
}
else if ($today == 'wednesday') {
	$targetBox[] = './stories/endrapport/tuesday/story_oslo_bors_tuesday.html';
	$targetBox[] = './stories/endrapport/tuesday/story_copenhagen_bors_tuesday.html';
	$targetBox[] = './stories/endrapport/tuesday/story_stockholm_bors_tuesday.html';
	$targetBox[] = './stories/endrapport/monday/story_oslo_bors_monday.html';
	$targetBox[] = './stories/endrapport/monday/story_copenhagen_bors_monday.html';
	$targetBox[] = './stories/endrapport/monday/story_stockholm_bors_monday.html';
}
else if ($today == 'thursday') {
	$targetBox[] = './stories/endrapport/wednesday/story_oslo_bors_wednesday.html';
	$targetBox[] = './stories/endrapport/wednesday/story_copenhagen_bors_wednesday.html';
	$targetBox[] = './stories/endrapport/wednesday/story_stockholm_bors_wednesday.html';
	$targetBox[] = './stories/endrapport/tuesday/story_oslo_bors_tuesday.html';
	$targetBox[] = './stories/endrapport/tuesday/story_copenhagen_bors_tuesday.html';
	$targetBox[] = './stories/endrapport/tuesday/story_stockholm_bors_tuesday.html';
}
else if ($today == 'friday') {
	$targetBox[] = './stories/endrapport/thursday/story_oslo_bors_thursday.html';
	$targetBox[] = './stories/endrapport/thursday/story_copenhagen_bors_thursday.html';
	$targetBox[] = './stories/endrapport/thursday/story_stockholm_bors_thursday.html';
	$targetBox[] = './stories/endrapport/wednesday/story_oslo_bors_wednesday.html';
	$targetBox[] = './stories/endrapport/wednesday/story_copenhagen_bors_wednesday.html';
	$targetBox[] = './stories/endrapport/wednesday/story_stockholm_bors_wednesday.html';
}

$destinationArray = ['1.html', '2.html','3.html','4.html','5.html','6.html'];
$destinationFolder =  "./stories/endrapport/";

foreach ($targetBox as $key => $target) {

	try {
		
		echo 'Trying to include ' . $targetBox[$key] . '<br>';
		$filepath = $destinationFolder . $destinationArray[$key];
		copy($target,$filepath);
		
	}

	catch(Exception $e) {
	  echo 'Message: ' . $e->getMessage();
	  error_log($e);
	}
}

//errorhandler som plukker opp alle feil
function errHandle($errNo, $errStr, $errFile, $errLine) {
    $msg = "$errStr in $errFile on line $errLine";
    error_log($msg);
    echo '<h1>' . $msg . '</h1>';
    throw new ErrorException($msg, $errNo);
}

?>