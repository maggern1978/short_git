<?php
//this files is for maintaince and must be startet manually


if (!function_exists('reformatcsv')) 
{
	function reformatcsv($data, $ticker)
	{

		$count = count($data);
		$allholder = [];

	//var_dump($data);


		for ($i = 2; $i < $count; $i++)
		{

			if(!isset($data[$i-1][4]) or !isset($data[$i][6]))
			{
				return;
			}
			

			$object = new stdClass;
			$object->previousClose = (float)$data[$i-1][4];
			$object->ticker = $ticker;
			$object->volume = (int)$data[$i][6];
			$object->close = $data[$i][4];
			$object->change = (float)$object->close - (float)$object->previousClose;
			$object->change = round($object->change,5);

			if ($object->close == 0 or $object->close == 'null' or $object->close == null or $object->previousClose == 0 or $object->previousClose == 'null' )
			{
			//var_dump($object);
			//echo '|';
				continue;
			}

			$allholder[][$data[$i][0]] = (array)$object;
		}

		$count = count($allholder);


		if ($count > 2)
		{
			return array_reverse($allholder);
		}
		else
		{
			return false;
		}

	}	
}


if (!function_exists('reformatdata')) 
{

	function reformatdata($data, $ticker)
	{

		$count = count($data);

		$allholder = [];

		for ($i = 0; $i < $count-1; $i++)
		{

			$object = new stdClass;
			$object->previousClose = (float)$data[$i+1]['close'];
			$object->ticker = $ticker;
			$object->volume = (int)$data[$i]['volume'];
			$object->close = $data[$i]['close'];
			$object->change = $data[$i]['close'] - $data[$i+1]['close'];
			$object->change = round($object->change,5);

			$allholder[][$data[$i]['date']] = (array)$object;
		}
		return $allholder;
	}
}


echo '<br>';
set_time_limit(19000);
date_default_timezone_set('Europe/Oslo');

include('../production_europe/logger.php');
require '../production_europe/functions.php';

$isinlist = readCSV('../shorteurope-com.luksus.no/dataraw/isin/history_sublist.csv');
$isincount = count($isinlist);

flush_start();
echo 'Getting ' . $isincount . ' number of stocks <br>';
flush_end();

$now = time();
//$two_weeks_ago = strtotime('-3 weeks');
$two_weeks_ago = 0;


$urlstart = 'https://eodhistoricaldata.com/api/eod/';
$urlend = '?from=2010-01-05&api_token=5da59038dd4f81.70264028&period=d&fmt=json';

//https://eodhistoricaldata.com/api/eod/CARL-B.CO?from=2000-01-05&api_token=5da59038dd4f81.70264028&period=d&fmt=json

$skipscounter = 0;
$skippingsave = 0;
$erroreod = 0;
$toofewvalues = 0; 
$savesuccess = 0;
$notseterrors = 0;

for ($i = 0; $i < $isincount; $i++) {


	//rapport
	$interval = 50;

	if ($i % $interval == 0 and $i != 0)
	{
		flush_start();
		echo '----------------------------------------<br>';
		echo 'Rapport for hver ' . $interval . ': $i er ' . $i . '<br>';
		echo '----------------------------------------<br>';
		timestamp();
		echo 'Files saved to disk: ' . $savesuccess;
		echo '<br>';
		echo 'skipscounter (more than 3 hours): ' . $skipscounter;
		echo '<br>';
		echo 'skippingsave: (Does not have todays stock date!)' . $skippingsave;
		echo '<br>';
		echo 'erroreod: ' . $erroreod;
		echo '<br>';
		echo 'toofewvalues: ' . $toofewvalues; 
		echo '<br>';
		echo 'notseterrors: ' . $notseterrors;
		echo '<br>';
		echo '<br>';
		flush_end();
		
	}


	if (isset($isinlist[$i][1]) and isset($isinlist[$i][8])) {
		$ticker = $isinlist[$i][1] . '.' . $isinlist[$i][8];
		$ticker = str_replace(' ','-',$ticker);
		$tickerending = substr($ticker,-2,2);
		$folder = '../production_europe/history/';

		//flush_start();
		$filename = $ticker . '.json';
		$filename = strtolower($filename);
		$filenameurl = $folder . $filename;


		if (file_exists($filenameurl))
		{

			$filetime_file_seconds = filemtime($filenameurl);
			$filetime_now_seconds = time();

			$time_difference_seconds = $filetime_now_seconds  - $filetime_file_seconds;
			$time_difference_hours = $time_difference_seconds/3600; 

			if ($time_difference_hours < 3)
			{
        		//echo $i . '. ' . $filenameurl . ' already updated within 3 hours, skipping<br>';
				$skipscounter++;
				continue; 
			}

		}

		$url = $urlstart . $ticker . $urlend;
        //echo $i . '. ' . $url .' ' . date('Y-m-d H:i:s') . '<br>';
		$today = date('Y-m-d');


		if ($data = json_decode(download($url), true))
		{
			
			//checking if todays stock info is included, if not skip save. 
			$dataCount = count($data);
			$todayFoundSuccess = 0;

			//double check that today is a weekday, if not accept last weekday
			while (isWeekend($today))
			{
				$today = date('Y-m-d', strtotime("-1 day", strtotime($today)));
				//echo 'Finding latest weekday...Now: ' . $today . '<br>';
			}

			for ($x = $dataCount-1; $x > $dataCount - 10; $x--)
			{

				if (!isset($data[$x]['date']))
				{
					continue;
				}

				if ($today == $data[$x]['date'])
				{
					$todayFoundSuccess = 1;
					break;
				}

			}

			if ($todayFoundSuccess == 0)
			{
				//errorecho('Last weekday stock price not found, skipping save. <br>');
				$skippingsave++;
				//continue;
			}

			$data = array_reverse($data);
			$data = reformatdata($data, $ticker);
			$data = float_format($data);
			
			saveJSON_silent($data, $folder . $filename);			
			$savesuccess++;
		}
		else 
		{
			$erroreod++;
			//errorecho ('Ticker download error from EOD: ' . $ticker . '<br>'); 
			//echo 'Doing yahoo download';

			$urlstartalt = 'https://query1.finance.yahoo.com/v7/finance/download/';
			$urlendalt = '&interval=1d&events=history&crumb=jIIyoewBT2o';

			$url = $urlstartalt . $ticker . '?period1=' . $two_weeks_ago . '&period2=' . time() . $urlendalt;
	        //echo $url .' ' . date('Y-m-d H:i:s') . '<br>';

			if (!$data = download($url))
			{
				//errorecho ('Ticker download error from Yahoo: ' . $ticker . '<br>'); 
				//flush_end();
				continue;
			}
			
			$data = array_map('str_getcsv', explode("\n", $data));

			if ($data = reformatcsv($data, $ticker))
			{
				//successecho ('Saving ' . $folder . $filename . '<br><br>');
				saveJSON_silent($data, strtolower($folder . $filename));
				$savesuccess++;
			}
			else
			{
				$toofewvalues++; 
				//echo '<strong>To few values, skipping save</strong><br>';
			}

		}

		//flush_end();
	}
	else
	{
		//errorecho($i . '. Error!<br>');
		$notseterrors++;
	}

}



?>