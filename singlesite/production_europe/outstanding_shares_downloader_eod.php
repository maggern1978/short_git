
<?php 

//This is based on the main isin list in datacalc/isin/
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include '../production_europe/functions.php';
set_time_limit(14000);
	
$filename = '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv';
$list = readCSV($filename);
$urlstart = 'https://eodhistoricaldata.com/api/eod/';
$urlstart = 'https://eodhistoricaldata.com/api/fundamentals/';
$urlend = '?api_token=5da59038dd4f81.70264028';
	
$skippingcounter = 0; 
$errorcounter = 0;
$progresscounter = 0;
$savecounter = 0; 

foreach ($list as $key => $row)
{
	
	
	$progresscounter++;
	if ($key % 200 == 0)
	{
		flush_start();
		echo "\n";
		echo 'Status:' . "\n";
		echo 'Skipped: ' . $skippingcounter . "\n";
		echo 'Errors: ' . $errorcounter . "\n";
		echo 'Progress: ' . $progresscounter . "\n";
		echo 'Savecounter: ' . $savecounter . "\n";
		timestamp();
		echo "\n";
		flush_end();
	}

	if (!isset($row[8]))
	{
		//echo 'Row[8] not set, skipping..<br>';
		$errorcounter++;
		continue;
	}

	$ticker = $row[1] . '.' . $row[8];
	$ticker = str_replace(' ', '-', $ticker);

	$url = $urlstart . $ticker . $urlend . '&filter=SharesStats::SharesOutstanding';
	$ticker = strtolower($ticker);
	
	$savepath = '../production_europe/json/outstanding_shares_part/'. $ticker . '.json';
	$savepath = strtolower($savepath);

	//only save files if files are 2 workdays or more old
	$today = date('Y-m-d');
	$yesterday = date('Y-m-d',(strtotime ('-1 weekday' , strtotime ($today))));

	$success = 0;
	
	if (file_exists($savepath))
	{
		$filetime = filemtime($savepath);
		$filedate = date('Y-m-d', $filetime);	
		
		if ($yesterday <= $filedate)
		{
			//echo $key . '. Skipping download of ' . $url . '. File date is ' . $filedate . "\n";
			$skippingcounter++;
			
			continue;
		}
		else
		{
			$success = 1;
		}
		
	}
	else
	{
		$success = 1;
	}
	
	if ($success == 1)
	{
		if ($data = json_decode(download($url),true))
		{
			$array['SharesOutstanding'] = $data;
			$array['isinlistdata'] = $row;
			//echo $key . '. Downloading ' . $url . '<br>';
			saveJSONsilent($array, strtolower($savepath));
			$savecounter++; 

		}
		else
		{
			//echo $key . '. Skipping download of ' . $url . '. ' ;
			//echo 'Download or json_decode error.';
			$errorcounter++;
			//var_dump($data);
		}
	}

	
}
	
	
	
?>