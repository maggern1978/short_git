<?php 

//$land = 'denmark';
//$land = 'italy';

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

echo '<br>';

$limit_date_previous = '2020-01-01';
$date = date('Y-m-d');

while (!file_exists('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
		//echo $date . '<br>';
	$date = date('Y-m-d', strtotime("-1 weekday", strtotime($date)));

	if ($date < $limit_date_previous)
	{
		echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
		return;
	}
}	

//check if file is older than 3 hours, if yes, then skip. 
echo 'Checking timestamp: ';
if (get_file_age_in_hours('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json') > 2.99) 
{
	echo 'More than three hours old, will skip.... ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json' .  ' <br>';
	return;
} 
else 
{
	echo 'Less than three hours old, will continue....<br>';
}

echo 'Reading new positions: '. '../shorteurope-com.luksus.no/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json</strong><br>';

//read todays positions
if (!$currentdata = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
	errorecho('Error reading json ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
	return;
}
$currentfilename = '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json';


//find the newest position date
$newestdate = '2001-01-01';

foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $key => $position)
	{
		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}
}


//echo 'Previous working day of that is ' . $previousday . '<br>';
if ($newestdate == date('Y-m-d', (strtotime ('-1 weekday', strtotime ($date)))))
{
	echo 'Dates match. File has positions with timestamp from previous weekday (' . $newestdate .'). <br>';
}
else
{
	echo 'Newest date in current positions is ' . $newestdate . '.<br>';
	errorecho('Dates DOES NOT match. Filename does not have positions with timestamp from previous weekday. Probably no new positions?<br>');
}

echo '<br><strong>Finding file with newest position that is older OR EQUAL than date ' . $newestdate . ' found for current.json:</strong> <br>';


$previous_newest_date = $newestdate;

$date = date('Y-m-d', strtotime("+1 weekday", strtotime($newestdate)));

while ($previous_newest_date <= $newestdate)
{

	//finding the previous file
	$date = date('Y-m-d', strtotime("-1 weekday", strtotime($date)));
	
	while (!file_exists('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		
		$date = date('Y-m-d', strtotime("-1 weekday", strtotime($date)));

		if ($date < $limit_date_previous)
		{
			echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
			return;
		}
	}	

	//cannot ble same filename as current
	if ($currentfilename == '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json')
	{
		continue;
	}

	echo '--| File is ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'. '<br>';

	//read previous positions
	if (!$previousdata = readJSON('../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		errorecho('Error reading previous json ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
		continue;
	}


	//finding newest date
	//find the newest position date
	
	$previous_newest_date = '2001-01-01';

	foreach ($previousdata as $key => $company)
	{
		foreach ($company['Positions'] as $key => $position)
		{
			if ($position['ShortingDate'] > $previous_newest_date)
			{
				$previous_newest_date = $position['ShortingDate'];
			}
		}
	}

	if ($previous_newest_date <= $newestdate)
	{
		successecho('--| Success. Newest date in previous positions is ' . $previous_newest_date . '.<br><br>');
		break;
	}

}

echo 'Reading previous positions: ' . '../shorteurope-com.luksus.no/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json </strong>because that file has positions from the day before.<br> ';

echo '<br>';

//collect all changes names from new list
$changesCompanyBox = [];

foreach ($currentdata as $key => $currentcompany)
{
	
	//find same company in previous and check for changes
	$targetName = mb_strtolower($currentcompany['Name']);
	$targetIsin = strtolower($currentcompany['ISIN']);

	$success = 0;
	$foundcount = 0;
	foreach ($previousdata as $index => $previouscompany)
	{

		if ($targetName == mb_strtolower($previouscompany['Name']) or $targetIsin == mb_strtolower($previouscompany['ISIN']))
		{
			$success = 1;
			$foundcount++;
			echo '<b>'. $key . '. ' . $targetName . ' exists in both current and previous. Checking for differences: </b><br>';

			$currentcompany['ShortPercent'] = round($currentcompany['ShortPercent'],2);
			$previouscompany['ShortPercent'] = round($previouscompany['ShortPercent'],2);

			if ($currentcompany['ShortPercent'] == $previouscompany['ShortPercent'] and $currentcompany['NumPositions'] == $previouscompany['NumPositions'])
			{
				echo 'Shortpercent and number of positions are identical.<br><br>';
			}
			else
			{
				echo $key . '. ' . $targetName  . '. ';
				echo 'Shortpercent previous: ' . $previouscompany['ShortPercent'] . ' and current ' . $currentcompany['ShortPercent']  . '<br>';
				echo 'Positions previous: ' . $previouscompany['NumPositions'] . ' and positions current ' . $currentcompany['NumPositions']  . '<br>';

				echo 'Shortpercent and number of positions are <strong>NOT</strong> identical.<br>';


				//collect names current
				$currents_names_box = [];
				$currentcompany['Positions'] = array_values($currentcompany['Positions']);
				$current_pos_count = count($currentcompany['Positions']);



				for ($x = 0; $x < $current_pos_count; $x++)
				{
					
					$currents_names_box[] = mb_strtolower($currentcompany['Positions'][$x]['PositionHolder']);
				}

				$currents_names_box = array_unique($currents_names_box);
				$currents_names_box = array_values($currents_names_box);

				//collect names previous

				$changed_positions_box_current = [];

				//search currentpositions for simiilar positions in old
				foreach ($currentcompany['Positions'] as $teller => $currentposition)
				{
					$found_duplicate = 0; 

					foreach ($previouscompany['Positions'] as $oldposition)
					{

						if (mb_strtolower($currentposition['PositionHolder']) == mb_strtolower($oldposition['PositionHolder']) and $currentposition['ShortPercent'] == $oldposition['ShortPercent'])
						{
							echo 'Duplicate position found new vs old<br>';
							$found_duplicate = 1; 
							break;
						}

					}

					if ($found_duplicate == 0)
					{
						echo $teller . '. Position not found, adding...<br>';
						$changed_positions_box_current[] = $currentposition;

					}


				}

				if (!empty($changed_positions_box_current))
				{
					var_dump($changed_positions_box_current);
				}


				//previosu to currrent

				$changed_positions_box_previous = [];

				//search currentpositions for simiilar positions in old
				foreach ($previouscompany['Positions'] as $teller => $oldposition)
				{
					$found_duplicate = 0; 

					foreach ($currentcompany['Positions'] as $currentposition)
					{

						if (mb_strtolower($currentposition['PositionHolder']) == mb_strtolower($oldposition['PositionHolder']) and $currentposition['ShortPercent'] == $oldposition['ShortPercent'])
						{
							echo 'Duplicate position found old vs new<br>';
							$found_duplicate = 1; 
							break;
						}

					}

					if ($found_duplicate == 0)
					{
						echo $teller . '. Position not found, adding...<br>';
						$changed_positions_box_previous[] = $oldposition;

					}


				}

				//is any of the boxes is not empty, then there is a changed position
				//then add to changescompanybox;
				if (!empty($changed_positions_box_previous) or !empty($changed_positions_box_current))
				{
					echo ' Adding previous data to current. ';
					echo '<br><br>';

					$currentdata[$key]['previousShortPercent'] = $previouscompany['ShortPercent'];
					$currentdata[$key]['previousNumPositions'] = $previouscompany['NumPositions'];
					$currentdata[$key]['status'] = 'update';
					$changesCompanyBox[] = $currentdata[$key];
				}
				else
				{
					echo 'No changed positions, continuing...<br><br>';
				}




		}
		if ($foundcount > 1)
		{
			errorecho('Findcount er ' . $foundcount . ' meaning erorr with names<br>');
		}			

	}

}

if ($success == 0)
{

		//new, only if date is today or previous day is right. 
	$today = date('Y-m-d');
	$yesterday = date('Y-m-d', strtotime("-1 weekday", strtotime($today)));

	echo $key . '. <strong>' . $targetName . ' not found in previouspositions</strong>. ';

	if ($currentcompany['LastChange'] == $today or $currentcompany['LastChange'] == $yesterday)
	{
		$currentdata[$key]['status'] = 'new';
		$currentdata[$key]['previousShortPercent'] = 0;
		$currentdata[$key]['previousNumPositions'] = 0;
		$changesCompanyBox[] = $currentdata[$key];
		echo 'Added<br>';
		
	}
	else
	{
		echo 'Date is not today or yesterday, skipping position (First round)...<br>';
	}

	
}

}

//now find positions that are ended, meaning previousposition companies that are not in current. 
echo '<br>';
echo 'Finding companies that are in previous, but not in current, meaning short has gone to zero. <br>';
$endedcount = 0;

foreach ($previousdata as $key => $previouscompany)
{

	$targetName = strtolower($previouscompany['Name']);
	$targetIsin = $previouscompany['ISIN'];

	$success = 0;
	foreach ($currentdata as $index => $currentcompany)
	{

		if ($targetName == strtolower($currentcompany['Name']) or $targetIsin == $currentcompany['ISIN'])
		{
			//echo $key . '. ' . $targetName . ' from previous also exists in current, skipping to next. ';
			$success = 1;
			break;
		}
	}

	if ($success == 0)
	{
		echo $key . '. ' . $targetName . ' from previous is <strong>NOT</strong> found in current positions, adding to changesCompanyBox <br>';
		$previousdata[$key]['status'] = 'ended';
		$previousdata[$key]['previousShortPercent'] = $previousdata[$key]['ShortPercent'];
		$previousdata[$key]['previousNumPositions'] = $previousdata[$key]['NumPositions'];
		$previousdata[$key]['ShortPercent'] = 0;
		$previousdata[$key]['NumPositions'] = 0;
		$changesCompanyBox[] = $previousdata[$key];
		$endedcount++;


	}

}

echo 'Positions that ended: ' . $endedcount . '<br>';
echo '<br>';

//set lastchange date

$count = count($changesCompanyBox);

for ($i = 0; $i < $count; $i++)
{
	$newestdate = '2001-01-01';

	foreach ($changesCompanyBox[$i]['Positions'] as $position)
	{

		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}

	$changesCompanyBox[$i]['LastChange'] = $newestdate;
}




//remove positions as they are not needed

$count = count($changesCompanyBox);

for ($i = 0; $i < $count; $i++)
{
	unset($changesCompanyBox[$i]['Positions']);
}

//set change

for ($i = 0; $i < $count; $i++)
{
	if (!isset($changesCompanyBox[$i]['previousShortPercent']))
	{
		errorecho('Error! missing previousShortPercent<br>');
		var_dump($changesCompanyBox[$i]);
		continue;
	}

	$changesCompanyBox[$i]['change'] = $changesCompanyBox[$i]['ShortPercent'] - $changesCompanyBox[$i]['previousShortPercent'];
	$changesCompanyBox[$i]['change'] = round($changesCompanyBox[$i]['change'],2);

}

//sort the positons
usort($changesCompanyBox, function ($item1, $item2) {
	return $item2['change'] > $item1['change'];
});

foreach ($changesCompanyBox as $key => $company)
{
	$changesCompanyBox[$key] = float_format($company);
}

//last check of dates. Should be from today or yesterday, if not new!
echo '<br><br>Last check of dates. Should be from today or yesterday, if not new!<br>';

$today = date('Y-m-d');
$yesterday = date('Y-m-d', strtotime("-1 weekday", strtotime($today)));

foreach ($changesCompanyBox as $key => $position)
{

	if ($position['LastChange'] == $today or $position['LastChange'] == $yesterday)
	{
		
	}
	else
	{
		
		if ($changesCompanyBox[$key]['status'] == 'new')
		{
			echo 'Date is not today or yesterday and positions is new, deøeting position (last check)...<br>';
			unset($changesCompanyBox[$key]);
		}
		
	}
}

$changesCompanyBox = array_values($changesCompanyBox);

echo 'Number of positions out: ' . count($changesCompanyBox) . '<br>';
saveJSON($changesCompanyBox, '../production_europe/json/events/company/' . $land . '.events.company.current.json');

//var_dump($changesCompanyBox);


?>