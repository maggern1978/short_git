<?php

//$land = 'united_kingdom';
//$land = 'norway';
//$land = 'germany';

echo '<br>Land er ' . $land . '<br>------------------------<br><br>';

set_time_limit(300);

include '../production_europe/options_set_currency_by_land.php';
include '../production_europe/functions.php';

gc_collect_cycles();
print_mem();

if (!isset($land))
{
    echo('Land not set');
    return;
}

$historyfile = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json';
$stockPricesUrl = '../production_europe/history/';
$allpositionssavepath = '../shorteurope-com.luksus.no/dataraw/stock.prices/' . $land . '/' . 'all_positions_' . $land . '.json';

$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' ); //for land hvor ticker ikke er i posisjonene. Bare Norge?
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' ); //for land hvor ticker ikke er i posisjonene. Bare Norge? 

//les inn x_current.json fra de ulike finanstilsynene
$json = readJSON($historyfile);

echo 'After files read in: ';
gc_collect_cycles();
print_mem();

//read date from generated file to set date
$date = date('Y-m-d');

//les inn alle posisjonene i shortlisten levert av finansmyndigheter

$player_array = array();
$AllPositionsBox = array();
$allCompaniesBox = array();
$countingPositions = 0;    
$counter = 0;
$company_array_test = array();

//find which currencis that is needed to download
$json_count = count($json);

$currency_box = [];

for ($i = 0; $i < $json_count; $i++)
{

    $local_pos_count = count($json[$i]['Positions']);

    for ($x = 0; $x < $local_pos_count; $x++)
    {

        if (isset($json[$i]['Positions'][$x]["currency"]))
        {
            $currency_box[] = $json[$i]['Positions'][$x]["currency"];
        }
    }
}

$currency_box = array_unique($currency_box);
$currency_box = array_values($currency_box);

//$currency_ticker
$currency_box_count = count($currency_box);
$currency_array = [];

echo 'Number of currencies: ' . $currency_box_count  . '<br>';
echo 'Getting updated currencies values<br>';

for ($i = 0; $i < $currency_box_count; $i++)
{
    $factor = 
    $factor = get_currency_multiply_factor($currency_ticker, $currency_box[$i]);

    $o = new stdClass;
    $o->from = $currency_ticker;
    $o->to = $currency_box[$i];
    $o->factor = $factor;

    $currency_array[] = (array)$o;

}


date_default_timezone_set('Europe/Oslo');
echo 'Setting currency og base currency for hver posisjon: <br>';



for ($i = 0; $i < $json_count; $i++)
{
    $selskap = $json[$i];
    unset($json[$i]);

    $company_array_test[] = trim($selskap['Name']); //stats
    $local_positions = [];

    $main_object = new stdClass;
    $main_object->CompanyName = trim($selskap['Name']);
    $main_object->ISIN = $selskap['ISIN'];

    $somePositions = array();

    $countingPositions += count($selskap['Positions']) ;

    $local_pos_count = count($selskap['Positions']);

    for ($x = 0; $x < $local_pos_count; $x++)
    {
        $posisjon = $selskap['Positions'][$x];
        unset($selskap['Positions'][$x]);

        $player_array[] = trim(($posisjon["PositionHolder"])); //stats

        $obj_position = new stdClass;
        $obj_position->basecurrency = $currency_ticker;

        if (isset($posisjon["currency"]))
        {
            $obj_position->currency = $posisjon["currency"];

            //find right 
            $currency_array_count = count($currency_array);

            for ($b = 0; $b < $currency_array_count; $b++)
            {
                if ($currency_array[$b]['to'] == $posisjon["currency"])
                {
                    $obj_position->base_currency_multiply_factor = $currency_array[$b]['factor'];
                    break;
                }
            }

        }
        else
        {
             $obj_position->base_currency_multiply_factor = 1;
        }
        //echo $key . '. ' . $obj_position->base_currency_multiply_factor . '<br>';
        //$obj_position->EntityId = $posisjon["EntityId"];

        //hvis ticker ikke er i posisjonene, finn ticker. 

        if (!isset($posisjon["ticker"]))
        {
            //echo 'Ticker er ikke satt. Søker etter ticker! <br>';

            if ($row = binary_search_multiple_hits($posisjon["ISIN"], ($posisjon["Name"]), $isin_list, $name_list, $land))
            {
                $ticker = $row[1] . '.' . $row[8];
                $obj_position->ticker = $ticker;
            }
            else
            {
                errorecho('Fant ikke ticker for ' . $posisjon["Name"] . ' | ' . $posisjon["ISIN"] .  ' i isin-listene!<br>');
                $obj_position->ticker = '';

            }

        }
        else
        {
            $obj_position->ticker = $posisjon["ticker"];
        }

        $obj_position->ShortingDateStart = date("Y-m-d", strtotime($posisjon["ShortingDate"]));
        $obj_position->ShortingDateEnd = '';
        $obj_position->ShortingDateStartStockPrice = '';
        $obj_position->ShortingDateEndStockPrice = '';
        $obj_position->StockPriceChange = '';
        $obj_position->isActive = $posisjon["isActive"];
        $obj_position->PositionHolder = trim(($posisjon["PositionHolder"]));
        $obj_position->ShortPercent = $posisjon["ShortPercent"];
        $obj_position->NumberOfStocks = $posisjon["NetShortPosition"];
        $obj_position->ISIN = $posisjon["ISIN"];
        $obj_position->StockName = trim(($selskap["Name"]));
        $obj_position->valueChange = '';
        
        $AllPositionsBox[] = $obj_position;  
        $local_positions[] =  $obj_position;

    }

    $main_object->Positions = $local_positions;
    $allCompaniesBox[] = (array) $main_object;
    $counter++;
 

}


$local_positions = 0;
$main_object = 0;
$json = 0;

//var_dump($allCompaniesBox);

$player_array = array_unique($player_array);
sort($player_array);
echo 'Antall playere: ' . count($player_array) . ' <br>';;
echo 'Antall posisjoner inn: ' . $countingPositions  . ' <br>';    

$company_array_test = array_unique($company_array_test);
echo 'Antall selskaper: ' . count($company_array_test) . '<br><br>';

gc_collect_cycles();
print_mem();

//første posisjon og sett sluttdato til i dag
$isFirst = '0';
$today = $date;
//echo ' I dag inn: ' . $today;

$today = str_replace(".","-",$today,$counter);
$endcounter = 0;

$allPositonsWithDates = array();

$all_positions_count = 0;
$all_positions_first_count = 0;
$player_name_hits = 0; 

gc_collect_cycles();
print_mem();


foreach ($allCompaniesBox as $teller => $selskap) //main round!
{
 echo '<br><strong>';
 echo $teller . '. Selskap: ' . $selskap["CompanyName"] . '</strong><br>';
 echo 'Antall posisjoner: ' . count($selskap["Positions"]) . ' | '; 

 $all_positions_first_count += count($selskap["Positions"]);
 timestamp();
    //lag en liste over alle playerne i selskapet
 $player_list = array();


 if (isset($selskap["Positions"])) 
 {
     $company_name = trim($selskap["CompanyName"]);
     $company_isin = $selskap["ISIN"];
 }
 else 
 {
     continue;
 }

 if (isset($selskap["Positions"])) 
 {
    foreach ($selskap["Positions"] as $posisjon) 
    {
        $player_list[] = mb_strtoupper(trim($posisjon->PositionHolder));
    }
}

$player_list = array_unique($player_list);
$player_list = array_values($player_list);

    //gå igjennom selskaps posisjoner for hver player og legg til posisjonene og slutt og startdatoer
    //var_dump($player_list);

gc_collect_cycles();
print_mem();
foreach ($player_list as $key => $playernavn) 
{
    echo '---' . $key . '. Playernavn er ' . $playernavn . ' | ';
    $player_positions = array();
    //samle inn all posisjoner

    if (isset($selskap["Positions"])) 
    {
        foreach ($selskap["Positions"] as $posisjon) {

                if (strtoupper($playernavn) == strtoupper($posisjon->PositionHolder)) //samle sammen hver enkelt players posisjoner.
                {
                    $player_positions[] = $posisjon;
                    $player_name_hits++; 

                }
            }
        }

        $date_before = '';
        $isFirst = 0;

        //sort positions to be sure
        usort($player_positions, function ($item1, $item2) {
            return $item2->ShortingDateStart <=> $item1->ShortingDateStart;
        });

        echo ' Antall posisjoner: ' . count($player_positions) . '<br>';

        foreach ($player_positions as $index => $pos) 
        {

            //Hvis første posisjon, sett ShortingDateEnd til i dag

            if ($isFirst == 0) 
            {
                if ($pos->isActive == 'yes')
                {
                    $pos->ShortingDateEnd = $today;
                    
                }
                else
                {
                    $pos->ShortingDateEnd = $pos->ShortingDateStart;
                }
                
                $date_before = $pos->ShortingDateStart;  
                $isFirst = 1;
            }
            else 
            {
                $date_before = date('Y-m-d',(strtotime ( '-1 weekday' , strtotime ($date_before) ) ));
                $pos->ShortingDateEnd = $date_before;

                //gjør klar ny sluttdato
                $date_before = $pos->ShortingDateStart;
            }

        }

        if (isset($selskap["Positions"]) and isset($player_positions[0])) 
        {
            $obj_2 = new stdClass; 
            $obj_2->StockName = strtoupper($company_name); 
            //echo 'Stockname is ' . $obj_2->StockName . '<br>';
            $obj_2->ISIN = $company_isin; 
            $obj_2->SumOfAllValueChangeForPlayer = '';
            $obj_2->playerName = $player_positions[0]->PositionHolder;
            $obj_2->ticker = $player_positions[0]->ticker;
            $obj_2->positions = $player_positions; 


            $all_positions_count += count($player_positions);

            $allPositonsWithDates[] = $obj_2;
        }

    }
    $endcounter++;

    $allCompaniesBox = 0;

}


echo 'Player name hits: ' . $player_name_hits . '<br>';
echo 'Antall posisjoner $all_positions_first_count: ' . $all_positions_first_count . '<br>';
echo 'Antall posisjoner A2: ' . $all_positions_count  . '<br>';

gc_collect_cycles();
print_mem();

$all_positions_count = 0;

foreach ($allPositonsWithDates as $teller => $selskap) //main round!
{

 $all_positions_count += count($selskap->positions);

}

echo 'Antall posisjoner A3: ' . $all_positions_count  . '<br>';


//var_dump($allPositonsWithDates[0]->positions);

//Gå igjennom alle posisjonene og sjekk at de som har fått satt
//datoen til i dag, også har verdien isActive = yes. Hvis ikke så er posisjonen feil.
//Feilen skyldes da at datasettet kun innholder en start-shorting dato, men ikke 
//en slutt-shorting dato (en med shortprosent under 0,5%). 
echo 'Progresjon: A4 <br>';

$sletteCounter = 0;
//var_dump($allPositonsWithDates[0]);
foreach ($allPositonsWithDates as $key => $player) 
{
    foreach ($player->positions as $index => $position) 
    {
        if ($position->ShortingDateEnd == $today) 
        {
            //echo 'Fant posisjon som har sluttdato i dag: ' . $today . '. ';
            //echo 'isActive er satt til ' . $position->isActive . '<br>';
            if ($position->isActive == 'no') 
            {
              //echo 'Sletter! ' . $key . ' ';
              //var_dump($allPositonsWithDates[$key]->positions[$index]);
              var_dump($allPositonsWithDates[$key]->positions[$index]);
              unset($allPositonsWithDates[$key]->positions[$index]);

              //echo 'Slettet ' . $key . ' / ' . $index;
              $sletteCounter++;
          }          
      }
  }
}

echo 'Slettet ' . $sletteCounter . ' posisjoner pga skulle vært aktiv og dato i dag.<br>';

$all_positions_count = 0;

foreach ($allPositonsWithDates as $teller => $selskap) //main round!
{
 $all_positions_count += count($selskap->positions);
}

echo 'Antall posisjoner A3: ' . $all_positions_count  . '<br>';


//ta selskapene
echo 'Tar alle posisjonene og ser etter kursdatoer i historiske nedlastede aksjerkurser:<br><br>';

$total_positions_count = 0; 

foreach ($allPositonsWithDates as $index => $selskap) 
{
    $total_positions_count += count($selskap->positions);
}

echo 'Antall posisjoner nå B2: ' . $total_positions_count . '<br>';


foreach ($allPositonsWithDates as $index => $selskap) 
{

    flush_start();
    echo $index . '. <strong>Selskap: ' . $selskap->StockName . '. Player: ' . $selskap->playerName . '</strong><br>';

    //var_dump($selskap);

    //sjekk om filen er der
    if (!isset($selskap->ticker)) 
    {
        echo 'Fant ikke ticker nr ' . $index . '. ' . $selskap->StockName . '<br>';
        flush_end();
        continue; 
    }
    
    flush_end();
    $ticker = str_replace(" ","-",$selskap->ticker,$count);

    //$pathen 
    $pathen = $stockPricesUrl . $ticker . '.json';
    $pathen = strtolower($pathen);

    echo 'Søker etter historiske aksjekurser i filen' . $pathen . '. ';

    $successFound = 0;
    if (file_exists($pathen)) 
    {
        $successFound = 1;
        $test_array = readJSON($pathen);
        successecho('Funnet!<br>');
    }
    else 
    {
        echo $index . '. ';
        errorecho('IKKE FUNNET: ' . $pathen . '! Posisjonen SLETTES!<br>');  
        continue;
    }

    $keys = array();
    $new_stock_array= array();

    //Sett key index riktig
    foreach ($test_array as $key => $stockday) 
    {
        $new_stock_array[key($stockday)] = $test_array[$key][key($stockday)];
    }   

    $AllPricesBox = array();

    //les inn alle stockprice-dagene             

    $success_start;
    $success_end; 
    $index = 0;

    echo '--- Prosesserer posisjoner: <br>';

    foreach ($selskap->positions as $key => $posisjon) 
    {

        //var_dump($posisjon);
        //finn startdato

        $starttarget = $posisjon->ShortingDateStart;
        $endtarget = $posisjon->ShortingDateEnd;
        
        //abort hvis noe er feil og dato er satt il 1970-01-01
        if ($starttarget == '1970-01-01') 
        {
            echo '<br>Datofeil! Aborterer! Her er posisjonen: ';
            var_dump($posisjon);
            unset($selskap->positions[$key]);
            continue;
        }

        //echo '---|--- Posisjon nr ' . $key . ':<br> ';

        if (isset($new_stock_array[$starttarget]['previousClose'])) 
        {
            //echo '---|--- Fant startdatoen: ' . $starttarget . '. ';
            //echo ' Verdien er: ' . $new_stock_array[$starttarget]['previousClose'] . '<br>';
            $posisjon->ShortingDateStartStockPrice = $new_stock_array[$starttarget]['previousClose'];
            $success_start = 'Suksess startdato';
        }
        else 
        {
            echo '---|--- Fant ikke startkursdato: ' . $starttarget;

            $tryCounter = 0;
            $fail = 0; 

            while (!isset($new_stock_array[$starttarget]['previousClose'])) 
            {

                $starttarget = date('Y-m-d',(strtotime ( '-1 weekday' , strtotime ($starttarget) ) ));
                echo '<br>Prøver med starttarget: ' . $starttarget . ' | ' ;
                echo '<br>    Trycounter er: ' . $tryCounter . '<br>';
                $tryCounter++;

                if ($tryCounter == 3 or $starttarget < $endtarget) 
                {
                    echo '. Fant ikke starttarget ' . $starttarget . '. Setter ShortingDateStartStockPrice til "-" .<br>'; 
                    //unset($selskap->positions[$key]);
                    $fail = 1; 
                    break;
                }                

            } 

            if ($fail == 1)
            {
                $posisjon->ShortingDateStartStockPrice = '-';
            }

            
            if (isset($new_stock_array[$starttarget]['previousClose']))
            {
                echo ' Fant ' . $starttarget . '<br>'; 
                $posisjon->ShortingDateStartStockPrice = $new_stock_array[$starttarget]['previousClose'];      
            }
            else
            {
                $posisjon->ShortingDateStartStockPrice = '-';
            }


        }

        if (isset($new_stock_array[$endtarget])) 
        {
            $posisjon->ShortingDateEndStockPrice = $new_stock_array[$endtarget]['close'];;
            $success_end = 'Sluttdato funnet.';
        }
        else 
        {
            echo '';
            echo '---|--- Fant ikke sluttdato: ' . $endtarget . '. ';

            $tryCounter = 0;
            $fail = 0; 

            while (!isset($new_stock_array[$endtarget])) 
            {
                $endtarget = date('Y-m-d',(strtotime ( '-1 weekday' , strtotime ($endtarget))));

                echo 'Ser etter neste dato: ' . $endtarget . '. ' ;
                echo 'Trycounter er: ' . $tryCounter . '.<br>';
                $tryCounter++;

                if ($tryCounter == 3) 
                {
                    echo $index . '. ';
                    echo $pathen;
                    echo ' Fant ikke ' . $endtarget . '. Setter ShortingDateEndStockPrice til "-".<br>'; 
                    //unset($selskap->positions[$key]);
                    $fail = 1;
                    break;
                }

            }

            if ($fail == 1)
            {
                $posisjon->ShortingDateEndStockPrice = '-';
                $posisjon->valueChange = '-';
                $posisjon->StockPriceChange = '-';
                continue;
            }

            //echo ' Fant ' . $endtarget . '. '; 
            //echo ' Verdien er: ' . $new_stock_array[$endtarget]['previousClose'] . '<br>';
            if (isset($new_stock_array[$endtarget]['previousClose']))
            {
                $posisjon->ShortingDateEndStockPrice = $new_stock_array[$endtarget]['close'];        
            }
            else
            {
                errorecho('$new_stock_array[$starttarget]["previousClose"] not set<br>');
                continue;
            }

        }

        if (is_numeric($posisjon->ShortingDateEndStockPrice) and is_numeric($posisjon->ShortingDateStartStockPrice)) 
        {
            $tempChange = (float)$posisjon->ShortingDateEndStockPrice - (float)$posisjon->ShortingDateStartStockPrice;
            $posisjon->StockPriceChange = number_format($tempChange,2,".","");

            $verdiutregning = $posisjon->ShortingDateEndStockPrice - $posisjon->ShortingDateStartStockPrice; 
            $verdiutregning = $verdiutregning * $posisjon->NumberOfStocks;

            $posisjon->valueChange = number_format($verdiutregning,2,".","");            
        }
        else 
        {
            $posisjon->StockPriceChange = '-';
            $posisjon->valueChange = '-';
        }

        $index++;                           

    }

}

echo 'Antall posisjoner nå B3: ' . $total_positions_count . '<br>';

//echo 'Alle posisjonene er tildelt verdiendring <br> ';
//ta bort alle posisjoner som har null i start- eller sluttkurs
$countMain = count($allPositonsWithDates);
$deleteCounter = 0;

$total_positions_count = 0;

for ($i = 0; $i < $countMain; $i++) 
{
    $allPositonsWithDates[$i]->positions = array_values($allPositonsWithDates[$i]->positions);

    if (isset($allPositonsWithDates[$i]->positions)) 
    {
        $countPos = count($allPositonsWithDates[$i]->positions);
        $total_positions_count += $countPos;
    }
    else 
    {
        continue;
    }

    for ($x = 0; $x < $countPos; $x++) 
    {
        if (isset($allPositonsWithDates[$i]->positions)) 
        {

            if (isset($allPositonsWithDates[$i]->positions[$x])) 
            {
                if (!(float)$allPositonsWithDates[$i]->positions[$x]->ShortingDateEndStockPrice > 0) 
                {
                    //echo 'Fant nullposisjon';
                    //var_dump($allPositonsWithDates[$i]->positions[$x]);
                    //$deleteCounter++;
                    //unset ($allPositonsWithDates[$i]->positions[$x]);
                }
                else if (!(float)$allPositonsWithDates[$i]->positions[$x]->ShortingDateStartStockPrice > 0 ) 
                {
                   //echo 'Fant nullposisjon2';
                   //var_dump($allPositonsWithDates[$i]->positions[$x]);
                   //$deleteCounter++;
                   //unset ($allPositonsWithDates[$i]->positions[$x]);
                }
                else if ($allPositonsWithDates[$i]->positions[$x]->ShortingDateStartStockPrice == '0.000') 
                {
                    echo 'SISTE!';
                }

            }
        }
    }
}
echo 'Antall posisjoner nå B4: ' . $total_positions_count . '<br>';
echo 'Deletecounter as a result of zero value in stock price: '.  $deleteCounter . '<br>';

$total_positions_count = 0; 

foreach ($allPositonsWithDates as $key => $player) {

    $positionCount = count($player->positions);
    $total_positions_count += $positionCount;

    if ($positionCount > 0) {
        //echo 'Selskapsnavn: ' . $player->StockName . ' ';
        //echo '--->Alle posisjonene til ' . $player->positions[0]->PositionHolder . '<br>';
        $sum = 0;

        foreach ($player->positions as $position) {
            //echo $position->ShortingDateStart . ' til ' . $position->ShortingDateEnd;
            //echo ' Verdiendring: ' . $position->valueChange . ' <br>';
            //var_dump($position->valueChange);
            //echo '<br> problem: ' . $position->valueChange;
            $allPositonsWithDates[$key]->playerName = $position->PositionHolder;  
            //var_dump($position);
            //echo $position->valueChange . ' <-' ;
            if ($position->valueChange != '-' and abs($position->valueChange) > 0) 
            {
                $sum += $position->valueChange;
            }

        }
        $player->SumOfAllValueChangeForPlayer = -$sum;      
        //$player->playerName = $player->positions[0]->PositionHolder;

        $sumMill = $sum/1000000*-1;

        //echo 'Total verdiendring: ' . $sum . ' (' . number_format($sumMill,2, ",",".") . ' millioner tjent/tapt)<br><br>';
    }
}

echo 'Antall posisjoner nå: ' . $total_positions_count . '<br>';

echo 'Ta bort alle posisjoner med null aksjer: <br>';
//Nå som alle posisjoner har fått alle nødvendige data, så ta ut posisjoner med null aksjer
//var_dump($allPositonsWithDates[15]);
$removecounter = 0; 
$total_positions_count = 0;

foreach ($allPositonsWithDates as $key => $player) 
{

    foreach ($player->positions as $index => $posisjon) 
    {
        if (!$posisjon->ShortPercent > 0) 
        {
            //var_dump($allPositonsWithDates[$key]->positions[$index]);
            unset($allPositonsWithDates[$key]->positions[$index] );
            $removecounter++;
        }
    }

    $allPositonsWithDates[$key]->positions = array_values($allPositonsWithDates[$key]->positions);
    $total_positions_count += count($allPositonsWithDates[$key]->positions);

}

echo 'Fjernet ' . $removecounter . ' posisjoner.<br>';
echo 'Antall posisjoner nå: ' . $total_positions_count . '<br>';

//var_dump($allPositonsWithDates[15]);
//ta bort playere med null posisjoner igjen


$totalcount = 0;

foreach ($allPositonsWithDates as $key => $player) 
{

    $count = count($player->positions);

    $totalcount += $count ;

    if ($count <= 0) 
    {            
        //echo 'unsetter <br>';
            //var_dump($allPositonsWithDates[$key]->positions[$index]);
            //echo 'Sletter player:';
            //var_dump($player);
        unset($allPositonsWithDates[$key]);
    }

}


$allPositonsWithDates = array_values($allPositonsWithDates);

echo 'Number of position being saved: ' .  $totalcount . '<br>';
//bygg url
//$filename1 = 'all_positions_nor.json';
$url = $allpositionssavepath;

saveJSON($allPositonsWithDates, $url);

$allPositonsWithDates = 0;

gc_collect_cycles();
print_mem();

successecho('Verdiutregning_selskap_sweden.php ferdig!');



?>