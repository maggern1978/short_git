<?php

echo 'Control_history_stock_downloader.php starter... Denne filen kjører history-filene.<br>';
include '../production_europe/history_isin_sublist_json_maker.php'; //takes all positions from isin lists and sorts out every company not mentioned in json-files.
include '../production_europe/history_maintainer_eod_downloader_sublist.php'; //downloads stock history for the sublist
include '../production_europe/history_bulk_downloader.php'; //downloads end-of-day stock data in bulk for all exchanges
include '../production_europe/history_bulk_daily_adder.php'; //adds bulk downloaded stock data to /history files if date is correct

?>