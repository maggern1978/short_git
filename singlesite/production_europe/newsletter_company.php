<?php

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

if (!$data = readJSON('../production_europe/json/events/company/' . $land . '.events.company.current.json'))
{
	//echo 'Error reading json, returning.';
	return;
}


//check that positions are from today or yesterday
//last check of dates. Should be from today or yesterday, if not new!

$today = date('Y-m-d');
$yesterday = date('Y-m-d', strtotime("-1 weekday", strtotime($today)));

foreach ($data as $key => $position)
{
	
	if (isset($position['LastChange']))
	{

		if ($position['LastChange'] == $today or $position['LastChange'] == $yesterday)
		{
			
		}
		else
		{
			unset($data[$key]);
		}
	}
}

$data = array_values($data);

if (!isset($data) or empty($data))
{
	return;
}

$newestdate = '2001-01-01';

foreach ($data as $company)
{
	if ($newestdate < $company['LastChange'])
	{
		$newestdate = $company['LastChange'];
	}
}

$border_string = ' style="border: 1px solid #dee2e6; border-collapse: collapse; padding: 6px; "';
$border_partial = '; border: 1px solid #dee2e6; border-collapse: collapse; padding: 6px; ';

?>
<div class="" style="font-family: Helvetica, arial, sans-serif; max-width: 500px; margin-bottom: 15px;">
	<div class="">
		<table class="table main table-sm" style="background-color: white; width: 100%; margin-bottom: 8px; <?php echo $border_partial; ?>">
			<thead class="thead">
				<tr style=" text-align: left; <?php echo $border_partial; ?> ">
					<th style="text-align: left; <?php echo $border_partial; ?> ">#</th>
					<th style=" <?php echo $border_partial; ?> ">Company</th>
					<th style="text-align: right; width: 80px; <?php echo $border_partial; ?> ">Change</th>					
					<th style="text-align: right; width: 80px; <?php echo $border_partial; ?> ">Short</th>
				</tr>
			</thead>
			<tbody <?php echo $border_string; ?> class="inside">

			<?php // Start with new positions 
			$index = 1;
			$cut_short = 0;

			foreach ($data as $key => $position)
			{

				if ($index > 19)
				{
					$cut_short = 1;
					break;
				}


				?>
				<tr>

				<?php
				$linkcompany = nametolink($position['Name']);
				$linkisin = nametolink($position['ISIN']);
			
				$change = $position['ShortPercent'] - $position['previousShortPercent'];
				$change = round($change,2);

				if ($change > 0)
				{
					$prefix = '+';
				}
				else
				{
					$prefix = '';
				}

				echo '<td style="width: 22px; ' . $border_partial . ';">';
				echo $index++ . '.';
				echo '</td>';
				echo '<td' . $border_string . ' >';
				echo '<a style="text-decoration: none;" href="';

				if ($mode == 'shortnordic')
				{
					echo 'https://shortnordic.com/detaljer_selskap.php?company=' . $linkcompany . '&land=' . $land;
				}
				else
				{
					echo 'https://shorteurope.com/details_company.php?isin=' . $linkisin  . '&land=' . $land; //@todo gjør om når samme site
				}				
				
				echo '">';
				if ($land == 'norway') //norge har bare store bokstaver
				{
					$name = mb_strtolower($position['Name']);
					echo ucwords($name);
				}
				else
				{
					echo $position['Name'];
				}
							
				echo '</a>';
				echo '</td>';
				echo '<td style="width: 80px; text-align: right; ' . $border_partial . '">';
				echo $prefix . number_format($change,2, '.', '')   . '&nbsp;%';
				echo '</td>';
				echo '<td style="width: 90px; text-align: right; ' . $border_partial . '">';

				if (round($position['ShortPercent'],2) == 0)
				{
					echo 'Under 0.5%';
				}
				else
				{
					echo number_format(round($position['ShortPercent'],2),2, '.', '')  . '&nbsp;%';
				}
				
				echo '</td>';
				?>

				</tr>
				<?php 

			}
			?>
	</tbody>
	</table>
	<?php

	if ($cut_short == 1)
	{
		echo '<p>(Showing first ' . $index . ' changes only. Visit ' . ucwords($mode) . '.com to see the remaining changes.)</p>';
	}
	?>

	<div style="text-align: left;">
	<?php
	if ($mode == 'shortnordic')
	{
		echo '<a style="color: black; " class="" href="https://' . $mode . '.com/most_shorted_companies_all.php?country=' . $land . '">';
		echo 'Link to most shorted ' . ucwords($land) . '.';
	}
	else
	{
		echo '<a style="color: black;"  class="" href="https://' . $mode . '.com/most_shorted_companies_all.php?country=' . $land . '">';
		echo 'Link to most shorted ' . ucwords($land) . '.';
	}
	?>
	</a>
	</div>
	
</div>
</div>
</div>