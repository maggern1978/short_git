<?php 

//$land = 'sweden';

$millisecondsts1 = microtime(true);

if (!function_exists('date_range_key')) {

	function date_range_key($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

		$dates = array();
		$current = strtotime($first);
		$last = strtotime($last);

		while( $current <= $last ) {
          //only work days
			if(date("w",$current) != 6 and date("w",$current) != 0) {
				$dates[date($output_format, $current)] = (float) 0.00;
			}
			$current = strtotime($step, $current);
		}
		return $dates;
	}
}

set_time_limit(5000);

//$land = "denmark";
$millisecondsts1 = microtime(true);

//for just doing a few companies
//$land = 'sweden';

require '../production_europe/functions.php';
//require '../production_europe/logger.php';

$name_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );


$historyjson = '../shorteurope-com.luksus.no/dataraw/' . $land . '.history/' . $land . '.history_current.json';
//$historyjson = 'germany.history_current.json';

$savefolder = '../production_europe/history_shorting/' . $land . '/';
$graphFolder = '../production_europe/history/';

//leser inn isinkoder
//germany.history_current.json

echo '<br><strong>Land er ' . $land . '</strong><br>';

if (!$historyjson = readJSON($historyjson))
{
	errorecho('Error reading json, returning...');
	return;
}


$historyjsoncount = count($historyjson);

for ($i = 0; $i < $historyjsoncount; $i++)
{
	$positionscount = count($historyjson[$i]['Positions']);
	echo '<strong>' . $i . ' | ' . ' ' . $historyjson[$i]['Name'] . '</strong><br>';

	//collect all names
	$localnamesbox = [];
	
	for ($x = 0; $x < $positionscount; $x++)
	{

		$localnamesbox[] = mb_strtolower($historyjson[$i]['Positions'][$x]['PositionHolder']);

		if ($land == 'norway') //remove norway specific timestamp
		{
			$historyjson[$i]['Positions'][$x]['ShortingDate'] = str_replace('T00:00:00', '', $historyjson[$i]['Positions'][$x]['ShortingDate']);
		}

	}
	
	$localnamesbox = array_unique($localnamesbox);
	$localnamesbox = array_values($localnamesbox);
	$localnamesboxcount = count($localnamesbox);
	
	//ta hvert navn og sett start og sluttdatoer
	$allocalpositionsbox = [];
	
	//ta et navn
	for ($z = 0; $z < $localnamesboxcount; $z++)
	{
		
		//samle inn alle posisjoner for player
		$targetname = $localnamesbox[$z];
		$localpositionsbox = [];
		for ($x = 0; $x < $positionscount; $x++)
		{
			if ($targetname == strtolower($historyjson[$i]['Positions'][$x]['PositionHolder']))
			{
				$localpositionsbox[] = $historyjson[$i]['Positions'][$x];
			}
			
		}
		echo $i . ' | '  . $z . ' | Playernavn ' . $targetname . '<br>';
		$localpositionsboxcount = count($localpositionsbox);

		if ($historyjson[$i]['Name']  == 'Peab AB')
		{
			//var_dump($historyjson[$i]['Positions'] );
		
		}

		//ta posisjonene
		$firstposition; 
		$previouspositiondate = '';

		//lag et unntak med navn med bare en posisjon. Den posisjonen må være aktiv, hvis ikke er det noe feil, og den må slettes. 
		if ($localpositionsboxcount == 1)
		{ 
			echo 'Count is 1, checking if position is active. ';

			if ($localpositionsbox[0]['isActive'] == 'no' )
			{
				echo 'Posistion is NOT active, will skip player and positions';
				continue;
			}
			else
			{
				echo 'Position is active, will include.<br>';
			}

		}
		
		for ($x = 0; $x < $localpositionsboxcount; $x++)
		{
			if ($x == 0)
			{
				$previouspositiondate = $localpositionsbox[$x]['ShortingDate'];
				
				if ($localpositionsbox[$x]['ShortPercent'] > 0.49)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = date('Y-m-d');
					$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($forrigeposisjonsstartdato)));
					continue;
				}
				else if ($localpositionsbox[$x]['ShortPercent'] < 0.50)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = $localpositionsbox[$x]['ShortingDate'];
					//$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($localpositionsbox[$x]['ShortingDate'])));
					continue;
				}				
				else
				{
					errorecho("Noe er galt med $localpositionsbox[$x]['ShortPercent'] <br>");
				}
			}
			else
			{			
				if ($localpositionsbox[$x]['ShortPercent'] > 0.49)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = $forrigeposisjonsstartdato;
					$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($forrigeposisjonsstartdato)));
					//echo '<br>' . $forrigeposisjonsstartdato . '<br>';
					continue;
				}
				else if ($localpositionsbox[$x]['ShortPercent'] == 0)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = $localpositionsbox[$x]['ShortingDate'] . '';
					//$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($localpositionsbox[$x]['ShortingDate'])));
					continue;
				}
				else if ($localpositionsbox[$x]['ShortPercent'] < 0.5)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($localpositionsbox[$x]['ShortingDate'])));
					//$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					continue;
				}									
				else
				{
					errorecho("Noe er galt med $localpositionsbox[$x]['ShortPercent'] <br>" . '. Verdien er "' . $localpositionsbox[$x]['ShortPercent'] . '" ');
				}
			}		
			
		}

		//take a look at the first position and check that is it active if over 0,49%. If not, take it out, the positin has not end date

		if (isset($localpositionsbox[0]) and $localpositionsbox[0]['ShortPercent'] > 0.49)
		{
			echo 'First position is positive, should be active, or somethine is wrong. ';

			if ($localpositionsbox[0]['isActive'] == 'no')
			{
				echo 'Position is not active, something is wrong. Will delete position as it has not end date.<br>';
				unset($localpositionsbox[0]);

				$localpositionsbox = array_values($localpositionsbox);
				$localpositionsboxcount = count($localpositionsbox);				

			}
			else
			{
				echo 'Position is active, so ok!<br>';
			}
		}


		for ($x = 0; $x < $localpositionsboxcount; $x++)
		{
			$allocalpositionsbox[] = $localpositionsbox[$x];
		}		
	}
	
	//sort by date
	usort($allocalpositionsbox, function ($item1, $item2) {
		return $item1['ShortingDate'] > $item2['ShortingDate'];
	});

	if ($historyjson[$i]['Name']  == 'peab ab')
	{
		var_dump($allocalpositionsbox);
		//var_dump($localpositionsbox);
		//exit();
	}


	
	$historyjson[$i]['Positions'] = $allocalpositionsbox;
	
	//saveJSON($allocalpositionsbox, 'test.json');
	//exit();
}

$historyjsoncount = count($historyjson);
//
for ($i = 0; $i < $historyjsoncount; $i++)
{
	

	if (!$positionscount = count($historyjson[$i]['Positions']))
	{
		errorecho($i . '. count er 0, skipping!<br>');
		//unset($historyjson[$i]);
		continue;
	}
	
	//find first and last date in positions
	$oldestdate = $historyjson[$i]['Positions'][$positionscount-1]['ShortingDate'];
	$mostrecentdate = $historyjson[$i]['Positions'][0]['ShortingDateEnd']; //oldest position
	//$mostrecentdate = $historyjson[$i]['Positions'][0]['ShortingDateEnd'];
	
	echo $i . '. ';
	echo 'Selskap: ' . $historyjson[$i]['Name'] . '. ';
	echo 'Most recent ' . $mostrecentdate . '<br>';
	echo 'Oldest ' . $oldestdate . '<br>';


	
	//var_dump($historyjson[$i]['Positions'] );
	//exit();


	//build date range
	$dateArray = date_range_key($oldestdate, $mostrecentdate, "+1 day", "Y-m-d");
	
  	//gå igjennom datoene og sjekk om posisjonene er gyldige i tidsrommet

	for ($x = 0; $x < $positionscount; $x++)
	{

		$startdate = $historyjson[$i]['Positions'][$x]['ShortingDate'];
		$shortpercent = $historyjson[$i]['Positions'][$x]['ShortPercent'];
		$round = 0; 

		//echo 'Start!';
    	//var_dump($historyjson[$i]['Positions'][$x]);

		while ($startdate <= $historyjson[$i]['Positions'][$x]['ShortingDateEnd'])
		{
			if (!isset($dateArray[$startdate])) 
			{
				$dateArray[$startdate] = 0;
			}

			$dateArray[$startdate] += (float)$shortpercent;
			//echo 'Round ' . $round++ . '. ';
			//echo 'Date is now ' . $startdate . '<br>';
			//echo 'Percent is ' . $dateArray[$startdate] . '<br>';

			$startdate = date('Y-m-d', strtotime("+1 weekday", strtotime($startdate)));
		}
		//echo 'End!';
		//var_dump($historyjson[$i]['Positions'][$x]);
	}

 	//sort by key
	ksort($dateArray);
	
	$dateArray = float_format($dateArray);
	
	foreach ($dateArray as $key => $day)
	{
		if ($day == 0)
		{
			continue;
		}
		else
		{
      //echo $key . ' ' . $day . '<br>';
		}
	}
  //find tickere isinlisten

	if (!$row = binary_search_multiple_hits($historyjson[$i]['ISIN'], $historyjson[$i]['Name'], $isin_list, $name_list, $land))
	{
		errorecho('Isin ' . $historyjson[$i]['ISIN'] . ' ' . $historyjson[$i]['Name'] . ' not found!<br>');
		continue;
	}

	$ticker = $row[1] . '.' . $row[8];
	$ticker = str_replace(' ', '-', $ticker);
	saveJSON($dateArray, $savefolder . strtolower($ticker) . '.json');
}



$millisecondsts2 = microtime(true);
$milliseconds_diff = ($millisecondsts2 - $millisecondsts1);  
echo 'Runtime in seconds is ' . $milliseconds_diff . '<br>';



?>

