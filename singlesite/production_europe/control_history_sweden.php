<?php

set_time_limit(5000);

$production = 1;
//nedlasting av aksjer ferdig!
require '../production_europe/functions.php';
require '../production_europe/logger.php';
require('../production_europe/run.php');


$land = 'sweden';
run('../production_europe/sweden_history_downloader.php'); 


//echo 'Starter sweden_ods_to_csv_converter_history.php <br>';
//include '../production_europe/sweden_ods_to_csv_converter_history.php';
//echo 'sweden_ods_to_csv_converter_history.php done!<br>';


$mode = 'history';
echo 'Starter sweden_xlsx_to_csv.php <br>';
include '../production_europe/sweden_xlsx_to_csv.php';
echo 'sweden_xlsx_to_csv.php done!<br>';


// Ta alle de aktive posisjonene og legg til de er aktive i csv-filen
run('../production_europe/setActivePositions.php');
run('../production_europe/mergeCSV.php');

$inputFileName = '../shorteurope-com.luksus.no/dataraw/sweden.history/sweden.history_merged.csv';
//run('csv_duplicates_finder.php');
run('../production_europe/csv_to_json_builder_history.php');

run('../production_europe/csv_to_json_history_positions_cleaner.php');

//gjør om navnene fra svenske myndigheter til det som står i hovedlisten for selskaper. 
//https://ShortEurope.com/dataraw/fi.se.history/fi.se.history_current.json
$mode = 'swedenhistory';
$inputFile = '../shorteurope-com.luksus.no/dataraw/sweden.history/sweden.history_current.json';

include('../production_europe/json_set_names_to_isinlist.php');

$land = 'sweden';
run('../production_europe/verdiutregning_selskap_sweden.php'); //lager all_positions_$land.json og positions_map_$land.json

run('../production_europe/json_splitter.php'); //splitter ut stock.prices verdiene

$land = 'sweden';
run('../production_europe/graph_short_generator.php'); //lager grafdata

echo '<br>History Sweden completed! :D';
?>