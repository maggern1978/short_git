<?php 

//This is based on the main isin list in dataCalc/isin/
//sets industri, secotr and currency if available
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include 'functions.php';
set_time_limit(4000);
	
$filename = '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv';

$list = readCSV($filename);


foreach ($list as $key => $row)
{
	if ($key == 0)
	{
		continue;
	}
		
	$ticker = $row[1] . '.' . $row[8];
	$ticker = str_replace(' ', '-', $ticker);
	$url = '../production_europe/json/fundamentals/' . $ticker . '.json';
	$url = strtolower($url);

	echo $key . '. Reading ' . $url . '. ';
	echo $row[2] . ' ' . $ticker . '. ';


	if (!file_exists($url))
	{
		errorecho('File not found, continuing<br>');
		continue;
	}

	if ($data = readJSON($url))
	{
		//var_dump($data);
		if (isset($data['SharesOutstanding']['General']['Sector']))
		{
			$list[$key][4] = $data['SharesOutstanding']['General']['Sector'];
			echo $list[$key][4] . ' | ';
		}
		
		if (isset($data['SharesOutstanding']['General']['Industry']))
		{
			$list[$key][5] = $data['SharesOutstanding']['General']['Industry'];
			echo $list[$key][5] . ' | ';
		}	
		
		if (isset($data['SharesOutstanding']['General']['CurrencyCode']))
		{
			$list[$key][3] = $data['SharesOutstanding']['General']['CurrencyCode'];
			echo $list[$key][3] . '. ';
		}
		else
		{
			errorecho('CurrencyCode not found. ');
		}
		
	}
	else
	{
		errorecho('Could not parse json');
	}
	echo '<br>';
	
}

$filename = '../shorteurope-com.luksus.no/dataraw/isin/isinlist_weekend_categories_adder.csv';
	
saveCSVsemikolon($list, $filename);
	
?>
