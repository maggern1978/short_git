<?php


//this file should be reworked to be same as active positions 
$land_navn = ucwords($land);

$playerliste = '../shorteurope-com.luksus.no/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';

$updated = filemtime($playerliste);
$date = date("o-m-d",$updated); 
$time = date("H:i",$updated); 

require '../production_europe/namelink.php';

if (!$allpositions = readJSON($playerliste))
{
  return;
}

$total_verdiendring = 0;
$total_eksponering = 0; 
$count = count($allpositions);
$found = 'no';

?>
<div class="container mt-2">
  <div class="row">
   <div class="col-lg-12">
     <?php
     $playertemp = strtolower($playernavn);
     $playertemp  = ucwords($playertemp);



     $descriptionTemp = '';

     for ($i = 0; $i < $count; $i++) {

       if (strtoupper($allpositions[$i]['playerName']) == strtoupper($playernavn) ) {
        $found = 'yes';
        $basecurrency = $allpositions[$i]['positions'][0]['basecurrency'];
      }
      else {
        continue;
      }
      ?>
      <h2><?php 
      $landtemp = str_replace('_',' ',$land_navn);
      echo ucwords($landtemp); 
      ?></h2> <?php

      $noName = 'set';
      include '../production_europe/detaljer_alle_header.php';

      $positionCount = count($allpositions[$i]['positions']);

      $totalValueChangeAllCompanies = 0;
      $totalBetSizeAllCompanies = 0;
      $totalLatestValueChangeAllCompanies = 0;

      for ($x = 0; $x < $positionCount; $x++) {
        echo '<tr>';
        echo '<td class="">';
        echo $x + 1;
        echo '</td>';
        echo '<td class="">';

        $selskapNew  = $allpositions[$i]['positions'][$x]['companyName'];
        $selskapNew  = ucwords($selskapNew);
        $singleCompany = nametolink($selskapNew);
        $singleCompany = strtoupper($singleCompany);
        $historyLink = 'details_company.php?company=' . $singleCompany . 
        '&land=' . $land;
        echo '<a data-toggle="tooltip"' . 'title="See active positions in company"' . ' class="text-dark" href="' . $historyLink  . '">';

        if(isset($description)) {
          $descriptionTemp = $descriptionTemp . $allpositions[$i]['positions'][$x]['companyName'] . ', ';
        }

        echo $allpositions[$i]['positions'][$x]['companyName'];
        echo '</a>';
        echo '</td>';
        $antall_aksjer = $allpositions[$i]['positions'][$x]['numberOfStocks'];
        echo '<td class="text-right active-positions-percent-cell" data-toggle="tooltip" title="' . number_format($antall_aksjer,0,".",",") . ' stocks">';
        $percent = round($allpositions[$i]['positions'][$x]['shortPercent'],2);
        echo number_format($percent,2) . ' %';
        echo '</td>';

        echo '<td class="text-right">';
        $posisjonsverdi = round($allpositions[$i]['positions'][$x]['marketValue'],2);
        $totalBetSizeAllCompanies += $posisjonsverdi * $allpositions[$i]['positions'][$x]['base_currency_multiply_factor'];

        echo number_format($posisjonsverdi,2,".",",") . '&nbsp;M&nbsp;' . $allpositions[$i]['positions'][$x]['currency'];
        echo '</td>';
        echo '<td class="text-right">';
        echo $allpositions[$i]['positions'][$x]['stockPrice'];
        echo '</td>';    	    	   		
        echo '<td class="text-right">';
        if ($allpositions[$i]['positions'][$x]['stockPriceChange'] > 0) 
        {
         echo '+';
       }
       echo round($allpositions[$i]['positions'][$x]['stockPriceChange'],2);
       echo ' (';
       if ($allpositions[$i]['positions'][$x]['stockPriceChange'] > 0) 
       {
         echo '+';
       }
       echo round($allpositions[$i]['positions'][$x]['valueChangePercent'],2); 
       echo '%)';
       echo '</td>';
       echo '<td class="text-right">';
       $verdiendring = round($allpositions[$i]['positions'][$x]['positionValueChange'],2);

       $totalLatestValueChangeAllCompanies += $verdiendring * $allpositions[$i]['positions'][$x]['base_currency_multiply_factor'];

       if ($verdiendring > 0) 
       {
         echo '+';
       }
       echo number_format($verdiendring,2,".",",") . ' M ';

       echo '</td>';
       echo '<td class="text-right" data-toggle="tooltip" title="Position started ' . $allpositions[$i]['positions'][$x]['positionStartDate'] . '.">';

      if (isset($allpositions[$i]['positions'][$x]['positionValueChangeSinceStart']) and $allpositions[$i]['positions'][$x]['positionValueChangeSinceStart'] != '' and $allpositions[$i]['positions'][$x]['positionValueChangeSinceStart'] != 0 and $allpositions[$i]['positions'][$x]['positionValueChangeSinceStart'] != '-') 
      {
        $startprice = round($allpositions[$i]['positions'][$x]['startStockPrice'],2);
        echo number_format($startprice,2);
      }
      else 
      {
        echo '<span data-toggle="tooltip" title="Data not available">-</span>';
      }
      echo '</td>';

      if (isset($allpositions[$i]['positions'][$x]['positionValueChangeSinceStart']) and $allpositions[$i]['positions'][$x]['positionValueChangeSinceStart'] != '' and $allpositions[$i]['positions'][$x]['positionValueChangeSinceStart'] != 0 and $allpositions[$i]['positions'][$x]['positionValueChangeSinceStart'] != '-') 
      {
        echo '<td class="text-right font-weight-bold ' .  getcolor($allpositions[$i]['positions'][$x]['positionValueChangeSinceStart']) .  '">';
        $totalChange = round(((float)$allpositions[$i]['positions'][$x]['positionValueChangeSinceStart']/1000000),3);
        $totalValueChangeAllCompanies += $totalChange * $allpositions[$i]['positions'][$x]['base_currency_multiply_factor'];	
        if ($totalChange > 0) {
          echo '+';
        }

        echo number_format($totalChange,2);
        echo ' M</td>';
      }
      else 
      {
        echo '<td class="text-right" data-toggle="tooltip" title="Data not available">';
         echo '-';
        echo '</td>';
      }

      echo '</tr>';   		
    }

    if ($positionCount > 1) {
     echo '<tr>'; 
     echo '<td>';
     echo '</td>';
     echo '<td>Sum (' . $basecurrency . '): ';
     echo '</td>';
     echo '<td>';
     echo '</td>';
     echo '<td class="text-right font-weight-bold">';
     echo number_format($totalBetSizeAllCompanies,2);
     echo '&nbsp;M&nbsp;</td>';
     echo '<td>';
     echo '</td>';
     echo '<td>';
     echo '</td>';
     echo '<td class="text-right font-weight-bold ' . getcolor($totalLatestValueChangeAllCompanies) . '">';
     echo round($totalLatestValueChangeAllCompanies,2);
     echo '&nbsp;M&nbsp;</td>';
     echo '<td>';
     echo '</td>';
     echo '<td class="text-right font-weight-bold ' .  getcolor($totalValueChangeAllCompanies) . ' ">';
     echo round($totalValueChangeAllCompanies,2);
     echo '&nbsp;M&nbsp;</td>';
     echo '</tr>'; 

		//add in summary values!
     $aggregatedNumberOfPositions += $positionCount;
     $aggregatedPositionValues += $totalBetSizeAllCompanies;
     $aggregatedLatestValueChange += $totalLatestValueChangeAllCompanies;
     $aggregatedTotalValueChange += $totalValueChangeAllCompanies;	


   }

   echo '</tbody>';
   echo '</table>';
   echo '</div>';
   echo '</div>';
   echo '</div>';

   if(isset($description)) {
    $description = $description . $descriptionTemp;
  }
  if(isset($keywords)) {
    $keywords = $keywords . $descriptionTemp;
  }
  return;

}

if ($found == 'no') {
	echo 'No hits in active positions ' . ucwords($land) . '.';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}


?>


