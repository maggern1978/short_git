<?php

switch ($land) 
{
    case "norway":
        $currency_ticker = 'NOK';
        $land_navn = $land;
        $countryText = 'Norway';
        $tooltip = 'The Oslo Stock Exchange';
        $image = 'banner-norway';
        $tooltip_earnings = 'Earnings in million NOK';
        $negative = 'no';
        break;
    case "sweden":
        $currency_ticker = 'SEK';
        $land_navn = $land;
        $countryText = 'Sweden';
        $tooltip = 'Nasdaq Stockholm';
        $image = 'banner-sweden';
        $tooltip_earnings = 'Earnings in million SEK';
        $negative = 'no';
        break;
    case "denmark":
        $currency_ticker = 'DKK';
        $land_navn = $land;
        $countryText = 'Denmark';
        $tooltip = 'Nasdaq Copenhagen';
        $image = 'banner-denmark';
        $tooltip_earnings = 'Earnings in million DKK';
        $negative = 'no';
        break;
    case "finland":
        $currency_ticker = 'EUR';
        $land_navn = $land;
         $countryText = 'Finland';
        $tooltip = 'Nasdaq Helsinki';
        $image = 'banner-finland';
        $tooltip_earnings = 'Earnings in million EUR';
        $negative = 'yes';
        break;
    case "germany":
        $currency_ticker = 'EUR';
        $land_navn = $land;
         $countryText = 'Germany';
        $tooltip = 'Dax';
        $image = 'banner-germany';
        $tooltip_earnings = 'Earnings in million EUR';
        $negative = 'no';
        break;
   case "united_kingdom":
        $currency_ticker = 'GBP';
        $land_navn = $land;
        $countryText = 'United Kingdom';
        $tooltip = 'FTSE 1000';
        $image = 'banner-united_kingdom';
        $tooltip_earnings = 'Earnings in million GBP';
        $negative = 'no';
        break;
   case "france":
        $currency_ticker = 'EUR';
        $land_navn = $land;
        $countryText = 'France';
        $tooltip = 'CAC 40 Index';
        $image = 'banner-france';
        $tooltip_earnings = 'Earnings in million EUR';
        $negative = 'no';
        break;
   case "italy":
        $currency_ticker = 'EUR';
        $land_navn = $land;
        $countryText = 'Italy';
        $tooltip = 'FTSE MIB';
        $image = 'banner-italy';
        $tooltip_earnings = 'Earnings in million EUR';
        $negative = 'no';
        break;
   case "spain":
        $currency_ticker = 'EUR';
        $land_navn = $land;
        $countryText = 'Spain';
        $tooltip = 'IBEX 35';
        $image = 'banner-spain';
        $tooltip_earnings = 'Earnings in million EUR';
        $negative = 'yes';
        break;
    
    default:
        return;
}

$path = '../shorteurope-com.luksus.no/datacalc/nokkeltall/nokkeltall_' . $land . '.current.csv'; 

if (!function_exists('getNumbers')) 
{
  function getNumbers($filenameAndUrl) 
  {

    $csvFile = file($filenameAndUrl);            
    $data = [];
    foreach ($csvFile as $line) 
      {            $data[] = str_getcsv($line);
      } 
      return $data;
    }
  }

$data = getNumbers($path);
//var_dump($data);
$plus = '';
if ($data[1][0] > 0) {
  $plus = '+';
}
ob_start();
if ($negative == 'no')  {
?>
<a href="<?php echo $land;?>.php">
<div class="container">
  <div class="row banner-padding">
    <div class="col-md-2 <?php echo $image; ?>">
      <span class="text-white"></span>
    </div>
    <div class="col-8 col-md-7 banner-norway-title banner-background-<?php echo $land_navn; ?>" >
      <span class="text-white "><?php echo $countryText; ?></span>
    </div>
    <div class="col-4 col-md-3 text-right banner-norway-percent banner-background-<?php echo $land_navn; ?> 
    " data-toggle="tooltip" title="<?php echo $tooltip_earnings; ?>"">
      <span class="text-white"><?php echo $plus . number_format($data[1][0],1); ?> M</span>
    </div>
  </div>
</div>
</a> 
<?php 
}
else 
{
?>

<a href="<?php echo $land;?>.php">
<div class="container">
  <div class="row banner-padding-finland banner-shadow ">
    <div class="col-md-2 <?php echo $image; ?>">
        <span class=""></span>
      </div>
      <div class="col-8  col-md-7 banner-norway-title banner-background-<?php echo $land_navn; ?>" data-toggle="tooltip" title="">
        <span class="text-blue"><?php echo $countryText; ?></span>
      </div>
      <div class="col-4   col-md-3 text-right banner-norway-percent banner-background-<?php echo $land_navn; ?> 
        " data-toggle="tooltip" title="<?php echo $tooltip_earnings; ?>"">
        <span class="text-black"><?php echo $plus . number_format($data[1][0],1); ?> M</span>
      </div>
  </div>
</div> 
</a>


<?php

}
  $htmlStr = ob_get_contents();
  // Clean (erase) the output buffer and turn off output buffering
  ob_end_clean(); 

  $destination = '../shorteurope-com.luksus.no/country_banner_' . $land . '.html';
  // Write final string to file
  file_put_contents($destination, $htmlStr);


?>