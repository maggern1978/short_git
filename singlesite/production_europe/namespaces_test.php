<?php

namespace search;

include_once('../production_europe/functions.php');

class searcher {

	public $isin = false;
	public $results_eod = false;
	public $results_finnhub = false;
	public $results_yahoo = false;	
	public $results_figi = false;

	private function download_eod()
	{
		$token = '5da59038dd4f81.70264028';
		$starturl = 'https://eodhistoricaldata.com/api/search/';
		$endurl = '?api_token=' . $token;
		$url = $starturl . rawurlencode($this->isin) . $endurl;

		if ($data = download_json($url))
		{
			//var_dump($data);

			if (!empty($data) and $data != '[]')
			{
				if ($data = json_decode($data))
				{
					$this->set_results_eod((array)$data); 
				}
				else
				{
					$this->set_results_eod(false); 
					echo 'json decode failed for eod<br>';
					var_dump($data);
					var_dump($url);
				}; 
			}
			else
			{
				$this->set_results_eod(false); 
			}

		}

	}

	private function download_finnhub()
	{

		$starturl = 'https://finnhub.io/api/v1/search?q=';
		$token = '&token=bp8pnvnrh5racm7obvp0';
		$url = $starturl . rawurlencode($this->isin) . $token;

		if ($data = json_decode(download_json($url)))
		{
			$data = $data->result;
			$this->set_results_finnhub((array)$data); 
		}
		else
		{
			echo 'json decode failed for finnhub<br>';
			var_dump($data);
			var_dump($url);
		}		
	}

	private function download_yahoo()
	{

		$starturl = 'https://query2.finance.yahoo.com/v1/finance/search?q=';
		$url = $starturl . rawurlencode($this->isin);

		if ($data = json_decode(download_json($url)))
		{

			$this->set_results_yahoo((array)$data->quotes); 
		}
		else
		{
			echo 'json decode failed for yahoo<br>';
			var_dump($data);
			var_dump($url);
		}

	}

	private function download_figi_isin()
	{

		// The url you wish to send the POST request to
		$url = 'https://api.openfigi.com/v3/mapping/';

		// Create a new cURL resource
		$ch = curl_init($url);

		// The data to send to the API
		$postData = array(
			array(
				"idType" => "ID_ISIN",
				"idValue" => $this->isin,
				)
			);

		// Setup cURL
		curl_setopt_array($ch, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_POSTFIELDS => json_encode($postData)
			));
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//print_r(json_encode($postData));

		// Send the request
		$response = curl_exec($ch);

		// Check for errors
		if ($response === FALSE)
		{
			$this->set_results_figi(false);
		}

		// Decode the response
		if (!$responseData = json_decode($response, TRUE))
		{
			$this->set_results_figi(false);
		}

		if (isset($responseData[0]['data']))
		{
			$this->set_results_figi($responseData[0]['data']);
		}
		else
		{
			if (isset($responseData[0]['warning']))
			{
				$this->set_results_figi(false);
			}
			else
			{
				$this->set_results_figi($responseData);
			}		
		}


	}

	private function check_isin($number)
	{
		if (validate_isin($number))
		{
			$this->set_isin($number);
		}
		else
		{
			$this->set_isin(false);
		}

	}

	private function set_isin($number)
	{
		$this->isin = $number;
	}

	private function set_results_eod($array)
	{
		$this->results_eod = $array;
	}

	private function set_results_finnhub($array)
	{
		$this->results_finnhub = $array;
	}

	private function set_results_yahoo($array)
	{
		$this->results_yahoo = $array;
	}

	private function set_results_figi($array)
	{
		$this->results_figi = $array;
	}

	private function object_to_array($object)
	{

		return json_decode(json_encode($object), true);

	}


	private function return_results()
	{
		$array = [];

		if (empty($this->results_eod))
		{
			$array['eod'] = false;
		}
		else
		{
			$array['eod'] = $this->object_to_array($this->results_eod);
		}		

		if (empty($this->results_yahoo))
		{
			$array['yahoo'] = false;
		}
		else
		{
			$array['yahoo'] = $this->object_to_array($this->results_yahoo);
		}		

		if (empty($this->results_finnhub))
		{
			$array['finnhub'] = false;
		}
		else
		{
			$array['finnhub'] = $this->object_to_array($this->results_finnhub);
		}

		if (empty($this->results_figi))
		{
			$array['figi'] = false;
		}
		else
		{
			$array['figi'] = $this->object_to_array($this->results_figi);
		}


		return $array;

	}

	public function search_isin($number)
	{
		echo 'Doing isin search: <br>';
		$this->check_isin($number);

		if (!$this->isin)
		{
			return false; 
		}

		$this->download_eod();
		$this->download_finnhub();
		$this->download_yahoo();
		$this->download_figi_isin();

		echo 'Search done, returning result... <br>';
		return $this->return_results();
	}

	public function search_name($number)
	{
		echo 'Doing name search: <br>';
		$this->set_isin($number);
		$this->download_eod();
		$this->download_finnhub();
		$this->download_yahoo();

		echo 'Search done, returning result... <br>';
		return $this->return_results();

	}

}





$search = new searcher;
$isin = 'NO0005052605';
//$isin = 'US42806J1060';

$result2 = $search->search_isin($isin);

var_dump($result2);


exit();



$name = new searcher;
$result = $name->search_name('Hertz Global Holdings Inc');

var_dump($result);

echo '<br>----------------------------------------<br>';

exit();



$m = rawurlencode('norsk hydro');

$url = ('https://finnhub.io/api/v1/search?q=' . $m . '&token=bp8pnvnrh5racm7obvp0');

var_dump(download($url));

exit();
//https://query2.finance.yahoo.com/v1/finance/search?q=hydro

// The url you wish to send the POST request to
$url = 'https://api.openfigi.com/v3/mapping/';

// Create a new cURL resource
$ch = curl_init($url);

// The data to send to the API
$postData = array(
	array(
		"idType" => "ID_ISIN",
		"idValue" => "SE0007387022"
		)
	);

// Setup cURL
curl_setopt_array($ch, array(
	CURLOPT_POST => TRUE,
	CURLOPT_RETURNTRANSFER => TRUE,
	CURLOPT_POSTFIELDS => json_encode($postData)
	));
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

print_r(json_encode($postData));

// Send the request
$response = curl_exec($ch);

// Check for errors
if($response === FALSE){
	die(curl_error($ch));
}

// Decode the response
$responseData = json_decode($response, TRUE);

// Print the date from 
var_dump($responseData[0]['data']);


?>