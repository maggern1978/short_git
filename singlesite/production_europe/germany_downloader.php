<?php

//dev
$mode = 'germany';
$method = 'germany';

///execute.php?method=fr
//execute.php?method=de
//execute.php?method=de_hist

include __DIR__.'/includes/default.php';
include __DIR__.'/includes/lib/vendor/autoload.php';
include __DIR__.'/includes/Request.php';
include __DIR__.'/functions.php';

class Scraper {

	function scrape($method){
		$date = date('Ymd');

		if ($method=='france')
		{
			$savepath = '../shorteurope-com.luksus.no/dataraw/france/';
			$filename = 'france_current.csv';
		}
		else if ($method=='germany')
		{
			$savepath = '../shorteurope-com.luksus.no/dataraw/germany/';
			$filename = 'germany_current.csv';
		}
		else if ($method == 'germany_history')
		{
			$savepath = '../shorteurope-com.luksus.no/dataraw/germany.history/';
			$filename = 'germany.history_current.csv';
		}

		$this->openCsv("$savepath/$filename", true);
		
		$this->parser = new \Smalot\PdfParser\Parser();
		$fields = [
			'Positions Holder',
			'Company Name',
			'Company Isin',
			'Short Position Percent',
			'Position Start Date',
		];
		$this->addToCsv($fields);
		if($method=='france')
			$this->searchDocuments();
		else if($method=='germany')
			$this->searchTableCsv();
		else if($method=='germany_history')
			$this->searchTableCsv(true);
	}

	function sluggify($text){
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// trim
		$text = trim($text, '-');
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		// lowercase
		$text = strtolower($text);
		if (empty($text)) {
			return 'n-a';
		}
		return $text;
	}
	function openCsv($name='out.csv', $overwrite=false){
		$flag = $overwrite==false ? 'a' : 'w';
		$this->csv = fopen($name, $flag);
		if($this->csv === false)
			throw new \Exception('Could not create csv file. Please check folder permissions');
		fwrite($this->csv, "\xEF\xBB\xBF");
	}
	function addToCsv($fields=[]){
		fputcsv($this->csv, $fields);
	}

	function searchTable(){
		$url = "https://www.bundesanzeiger.de/pub/en/to_nlp_start?0";
		$prev_url = $url;
		$contents = \Request::send('GET', $url);
		$contents = mb_convert_encoding($contents, 'HTML-ENTITIES', 'UTF-8');
		$dom = new \Zend\Dom\Query($contents);
		$rows = $dom->execute('.nlp-search .row');
		$last_page = (int)$dom->execute('.page_count span')[1]->nodeValue;
		for($page=1; $page<=$last_page; $page++){
			$contents = \Request::send('GET', $url, null, ['Referer: '. $prev_url]);
			preg_match('/"(.+?nav-pager-next).+?To next page/', $contents, $match);
			$prev_url = $url;
			$url = "https://www.bundesanzeiger.de/pub/en".trim(array_pop($match), '.');
			sleep(1);
			$dom = new \Zend\Dom\Query($contents);
			$rows = $dom->execute('.nlp-search .row.even, .nlp-search .row.odd');
			foreach($rows as $row){
				$content = simplexml_import_dom($row)->asXml();
				$content = mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8');
				$row_dom = new \Zend\Dom\Query($content);
				$cols = $row_dom->execute('div');
				$fields = [];
				foreach($cols as $col){
					$fields[] = explode("\n", trim($col->nodeValue))[0];
				}
				$fields = [
					strtoupper($fields[1]),
					$fields[2],
					$fields[3],
					trim(trim($fields[4], '%')),
					$fields[5],
				];
				$fields = array_map('trim', $fields);
				$this->addToCsv($fields);
			}
			if(isset($_REQUEST['testonly']))
				break;
		}
	}
	function getTableCsv($historical = false)
	{
		$historical = $historical ? 'true' : 'false';
		$url = "https://www.bundesanzeiger.de/pub/en/to_nlp_start";
		$contents = \Request::send('GET', $url);
		$dom = new \Zend\Dom\Query($contents);
		$url_el = $dom->execute('.search-form')[0];

		//var_dump($contents)		;

		if (is_null($url_el))
		{
			file_put_contents(__DIR__.'/debug.log', $contents);
			throw new \Exception("Could not find search form. Please send debug log to developer");
		}

		$url = $url_el->getAttribute('action');

		//var_dump($url);

		$url =  trim($url, '.');
		//var_dump($url);

		$data = [
			'fulltext' => '',
			'positionsinhaber' => '',
			'ermittent' => '',
			'isin' => '',
			'positionVon' => '',
			'positionBis' => '',
			'datumVon' => '',
			'datumBis' => '',
			'isHistorical' => $historical,
			'nlp-search-button' => 'Search net short positions',
		];

		$headers = [
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
			'Content-Type: application/x-www-form-urlencoded',
			'Referer: https://www.bundesanzeiger.de/',
		];

		$contents = \Request::send('POST', $url, http_build_query($data), $headers);

		$dom = new \Zend\Dom\Query($contents);

		$download_btn = $dom->execute('a[title="Download as CSV"]')[0];
		

		if (is_null($download_btn)) {
			
			//file_put_contents('germany.txt', $contents);
			//logger('Could not find download ' . $mode . ' active positions',' in positions_downloader_germany_and_france.php. Returning!');
			throw new \Exception('Could not find download');
			return;
		}
		$download_url = trim($download_btn->getAttribute('href'), '.');
		$download_url = "".trim($download_url, '.');
		$contents = \Request::send('GET', $download_url, null, [
			'Referer: https://www.bundesanzeiger.de/',
		]);
		$lines = explode("\n", $contents);
		foreach($lines as &$fields){
			$fields = str_getcsv($fields);
			$fields[3] = @str_replace(',', '.', $fields[3]);
		}
		array_shift($lines);

		//var_dump($lines);
		return $lines;
	}
	
	function searchTableCsv($historical=false){
		$lines = $this->getTableCsv(false);
		$hashes = [];
		foreach($lines as $line)
			$hashes[] = implode(',', $line);
		if($historical){
			$lines_hist = $this->getTableCsv(true);
			foreach($lines_hist as $fields){
				if(in_array(implode(',', $fields), $hashes))
					continue;
				$this->addToCsv($fields);
			}
		} else {
			foreach($lines as $fields){
				$this->addToCsv($fields);
			}
		}
	}

	function searchDocuments(){
		$url = "https://bdif.amf-france.org/en_US/Resultat-de-recherche-BDIF?formId=BDIF&DOC_TYPE=BDIF&LANGUAGE=en&subFormId=dpcn&BDIF_TYPE_INFORMATION=BDIF_TYPE_INFORMATION_DPCN&TEXT=&BDIF_RAISON_SOCIALE=&bdifJetonSociete=&REFERENCE=&DATE_PUBLICATION=&DATE_OBSOLESCENCE=&valid_form=Start+search&isSearch=true&PAGE_NUMBER=";
		$contents = \Request::send('GET', $url);
		preg_match_all('/Resultat-de-recherche-BDIF.+?PAGE_NUMBER=([0-9]+)/', $contents, $matches);
		$numbers = array_pop($matches);
		$last_page = max($numbers);

		$pagecountermax = 25;

		for ($page=0; $page<=$last_page; $page++){
			flush_start();
			echo '<br>';
			echo 'Page ' . $page . ' of ' . $last_page .  '. ' .  date('Y-m-d H:i:s') . '<br>';
			$page_url = $url.$page;
			$contents = \Request::send('GET', $page_url);
			preg_match_all('/\/technique\/proxy-lien[^"]+/', $contents, $matches);
			$links = array_pop($matches);
			flush_end();
			foreach($links as $key => $link){
				flush_start();
				echo $key . ' ';
				$link = 'https://bdif.amf-france.org/'.$link;
				$this->searchDocuments2($link);
				flush_end();
			}
			if(isset($_REQUEST['testonly']))
				break;

			//break to not search evertying again
			if ($page > $pagecountermax)
			{
				break;
			}

		}
	}
	function searchDocuments2($link){
		$contents = \Request::send('GET', $link);
		preg_match_all('/\/technique\/multimedia[^"]+/', $contents, $matches);
		$links = array_pop($matches);
		foreach($links as $link){
			$link = 'https://bdif.amf-france.org/'.$link;
			$fields = $this->doDocument($link);
			$fields = array_map('trim', $fields);
			$this->addToCsv($fields);
		}
	}
	function doDocument($link){
		parse_str(parse_url($link, PHP_URL_QUERY), $params);
		$tmp_dir = __DIR__.'/tmp';
		if(!file_exists($tmp_dir))
			mkdir($tmp_dir, 0755);
		$filename = $tmp_dir.'/'.$params['docId'].'.pdf';
		$filename_parsed = $tmp_dir.'/'.$params['docId'].'.pdf.json';
		if(!file_exists($filename)){
			$contents = \Request::send('GET', $link);
			file_put_contents($filename, $contents);
			flush_start();
			echo '. Saved ' . $filename . '<br>';
			flush_end();

		} else $contents = file_get_contents($filename);
		if(!file_exists($filename_parsed)){
			$pdf = $this->parser->parseFile($filename);
			$pages  = $pdf->getPages();
			$texts = [];
			foreach ($pages as $page) {
				$pieces = $page->getDataTm();
				foreach($pieces as $piece){
					list($map, $text) = $piece;
					$slug = $this->sluggify($text);
					$bottom = array_pop($map);
					$left = array_pop($map);
					//echo $slug . ' left: ' . $left . '. Bottom: ' . $bottom . '<br>';
					$texts[$slug] = [$left, $bottom, $text];
				}
			}
			$table_keys = [
				'position-holder', 'detenteur-de-la-position',
				'nom-de-l-metteur', 'name-of-the-issuer',
				'date-de-position-aaaa-mm-jj', 'position-date-yyyy-mm-dd',
				'position-courte-nette-detenue-en', 'net-short-position-size-in-percentage', 'isin'];
			$table_values = [];

			foreach($table_keys as $table_key){
				if(!isset($texts[$table_key])){
				    echo 'Not set . ' . $table_key . '<br>';
				    continue;
				}
					
				foreach($texts as $slug=>$text){
					if($slug==$table_key)continue;
					if($text[0]==$texts[$table_key][0])continue;
					if($text[1]!=$texts[$table_key][1])continue;
					$table_values[$table_key] = $text[2];
				}
			}
			//echo '<br>-------------------<br>';
			
			$addtext = '';
			
			foreach($texts as $slug=>$text)
			{
			    if (stripos($slug, 'metteur'))
			    {
			        //echo 'Found nom-de-l-metteur<br>';
			        $hoydetarget  = $text[1];
			        
			        //echo  $hoydetarget . '<br>';
			        
			        	foreach($texts as $sluger =>$texter)
			            {
			                if ( $texter[1] == $hoydetarget and $sluger != $slug)
			                {
			                    $addtext .= $texter[2];
			                    break;
			                }
			                
			            }
			        
			    }
			    
			    if ('name-of-the-issuer' == $slug)
			    {
			        //echo 'Found name-of-the-issuer<br>';
			         $hoydetarget  = $text[1];
			         //echo  $hoydetarget . '<br>';
			         
			        	foreach($texts as $sluger =>$texter )
			            {
			                if ( $texter[1] == $hoydetarget  and $sluger != $slug)
			                {
			                    $addtext .= $texter[2];
			                    break;
			                }
			                
			            }
			    }
			}
			
			//echo '<strong>' . $addtext . '</strong';
			//echo '<br><br>';
								
			$fields = [];
			$fields['positions_holder'] = '';
			$fields['company_name'] = '';
			$fields['company_isin'] = '';
			$fields['short_position_percent'] = '';
			$fields['position_start_date'] = '';
			if(isset($table_values['position-holder']))
				$fields['positions_holder'] .= $table_values['position-holder'];
			if(isset($table_values['detenteur-de-la-position']))
				$fields['positions_holder'] .= ' '.$table_values['detenteur-de-la-position'];
			if(isset($table_values['nom-de-l-metteur']))
				$fields['company_name'] .= $table_values['nom-de-l-metteur'];
			if(isset($table_values['name-of-the-issuer']))
				$fields['company_name'] .= ' '.$table_values['name-of-the-issuer'];
			if(isset($table_values['date-de-position-aaaa-mm-jj']))
				$fields['position_start_date'] .= $table_values['date-de-position-aaaa-mm-jj'];
			if(isset($table_values['position-date-yyyy-mm-dd']))
				$fields['position_start_date'] .= ' '.$table_values['position-date-yyyy-mm-dd'];
			if(isset($table_values['position-courte-nette-detenue-en']))
				$fields['short_position_percent'] .= $table_values['position-courte-nette-detenue-en'];
			if(isset($table_values['net-short-position-size-in-percentage']))
				$fields['short_position_percent'] .= ' '.$table_values['net-short-position-size-in-percentage'];
			if(isset($table_values['isin']))
				$fields['company_isin'] .= $table_values['isin'];
				
			$fields['company_name'] = $addtext;
			
			$contents = json_encode($fields);
			flush_start();
			file_put_contents($filename_parsed, $contents);
			echo 'Saved file ' . $filename_parsed . '<br>';
		    //exit();
			flush_end();
		} else {
			$contents = file_get_contents($filename_parsed);
			$fields = json_decode($contents, true);
		}
		return $fields;
	}
}

$scraper = new \Scraper();
try {

	$scraper->scrape('germany');

 }
 catch (\Exception $e){
	echo '<br>';
	echo 'Line: '.$e->getLine().'<br>';
	echo 'Message: '.$e->getMessage().'<br>';
	echo 'Trace: '.$e->getTraceAsString();
}

$scraper = new \Scraper();
try {

	$scraper->scrape('germany_history');
	
	}
 
 catch (\Exception $e){
	echo '<br>';
	echo 'Line: '.$e->getLine().'<br>';
	echo 'Message: '.$e->getMessage().'<br>';
	echo 'Trace: '.$e->getTraceAsString();
}