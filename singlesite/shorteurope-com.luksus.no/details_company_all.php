
    <?php include 'header.html'; 

$land = "";

require '../production_europe/functions.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["land"]);
}

include '../production_europe/input_check_country.php';

$landNavn = ucwords($land);
$pageTitle = 'All active positions by company' . ' - ' . $landNavn;

$mode = 'multi';

$description = 'All active short positions in ' . $landNavn . '. '; 

?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row">
    <div class="col-12">
    <div class="d-none d-md-block">
      <h2>All active positions in <?php echo $landNavn;  ?>, by company<a href="details_all.php?&land=<?php echo $land;?>" class="btn btn-info text-right float-right ml-3 mt-3" role="button">Sort on players</a></h2>
     </div> 
      <div class="d-md-none">
      <h2>All active positions in <?php echo $landNavn;  ?></h2><a href="details_all.php?&land=<?php echo $land;?>" class="btn btn-info text-right my-1 " role="button">Sort on players</a>
     </div>

<?php include '../production_europe/detaljer_selskap_alle_body.php'; ?>

  <?php include 'footer.php'; ?>
  <?php include 'footer_about.html'; ?>
 </body>