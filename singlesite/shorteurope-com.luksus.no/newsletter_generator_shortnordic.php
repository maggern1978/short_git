<?php

require '../production_europe/functions.php';
require '../production_europe/logger.php';

if (!isset($mode))
{
	$mode = 'shortnordic';
} 

date_default_timezone_set('Europe/Oslo');
include '../production_europe/newsletter_header.html';

if ($mode == 'shortnordic')
{
	$countries = ['sweden', 'norway', 'denmark', 'finland'];
}
else if($mode == 'shorteurope')
{
	$countries = get_countries();
}
else
{
	return;
}

$misslist = [];
//check which countries to sort out

foreach ($countries as $key => $land)
{

	if (!file_exists('../production_europe/json/events/company/' . $land . '.events.company.current.json') or !$data = readJSON('../production_europe/json/events/company/' . $land . '.events.company.current.json'))
	{
		$misslist[] = $land;
		unset($countries[$key]);
		continue;
	}

	if (!isset($data) or empty($data))
	{
		$misslist[] = $land;
		unset($countries[$key]);
		continue;
	}
}

?>
<div class="col-12" >
<p class="header-text" style="font-family: Helvetica, arial, sans-serif;">Newsletter from <a href="https://<?php echo $mode .'.com'; ?>">
<?php

	if ($mode == 'shortnordic')
	{
		echo 'ShortNordic.com';
	}
	else
	{
		echo 'ShortEurope.com';
	}

?>

</a></p>
</div>
<div class="col-12">
<h3 style="font-family: Helvetica, arial, sans-serif;">Changes to short positions <?php echo date('l', strtotime("now"));?></h3>
<!--<p>Pulished <?php echo mb_strtolower(date('l', strtotime("now"))) . ' ' .  date('Y-m-d H:i:s'); ?>.</p>-->
</div>
<?php

foreach ($countries as $key => $land)
{
	include '../production_europe/newsletter_summary.php';
	include '../production_europe/newsletter_company.php';
}

?>
<br>
<?php

if (!empty($misslist))
{
	foreach ($misslist as $key => $land)
	{
		?>
		<p  style="font-family: Helvetica, arial, sans-serif;">No changes to positions for <?php echo ucwords($land); ?>.</p>
		<?php
	}
}

?>

<p  style="font-family: Helvetica, arial, sans-serif;">Content generated <?php echo date('Y-m-d') . ' at ' . date('H:i'); ?>.</p>

<?php

include '../production_europe/newsletter_footer.html';

?>