<?php include 'header.html'; ?>

<section class="section section-lg">
      <div class="container">
        <div class="row justify-content-center text-center mb-lg">
          <div class="col-lg-8">
            <h2 class="display-3">What is shorting?</h2>
            <p class="lead text-muted">Shorting is the sale of an asset, for example stocks, that the seller has borrowed in order to profit from a subsequent fall in the price of the asset. Read more on <a href="https://en.wikipedia.org/wiki/Short_(finance)">Wikipedia</a>.<br><br>
            Example: If you think the price of gold will fall, you can borrow some gold from a friend, and sell it for example for 1000 dollars. If the price of gold falls 20%, you can buy back the same amount of gold for 800 dollars. You have then earned 200 dollars (minus a fee for loaning the gold).</p>
          </div>
        </div>
        <div class="row justify-content-center text-center mb-lg">
          <div class="col-lg-8">
            <h2 class="display-3">Contributors</h2>
            <p class="lead text-muted">The following people and businesses are part of the ShortEurope team. We are always open for input on how to improve this site. </p>
          </div>
        </div>
        <div class="row">

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4">
            <div class="px-4">
              <img src="./img/about/odd_web.jpg" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
              <div class="pt-4 text-center">
                <h5 class="title">
                  <span class="d-block mb-1">Odd Christian Landmark</span>
                  <small class="h6 text-muted">Technical architect</small>
                </h5>

              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4">
            <div class="px-4">
              <img src="./img/about/thomas_web.jpg" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
              <div class="pt-4 text-center">
                <h5 class="title">
                  <span class="d-block mb-1">Thomas R. Skjølberg</span>
                  <small class="h6 text-muted">Backend and coding</small>
                </h5>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4">
            <div class="px-4">
              <img src="./img/about/magnus_web.jpg" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
              <div class="pt-4 text-center">
                <h5 class="title">
                  <span class="d-block mb-1">Magnus R. Skjølberg</span>
                  <small class="h6 text-muted">CSS and PR</small>
                </h5>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4">
            <div class="px-4">
              <img src="./img/about/sportyer.png" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
              <div class="pt-4 text-center">
                <h5 class="title">
                  <span class="d-block mb-1">Sportyer AS</span>
                  <small class="h6 text-muted">Coding</small>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<div class="container" >
    <div class="row">
      <div class="col-lg-12 text-center">
      <h5>About ShortEurope.com</h5>
The short positions are downloaded from the financial authorities in the respective countries:<br><br>Norway: Finanstilsynet. <a href="https://ssr.finanstilsynet.no/">Source</a>.<br>Sweden: Finansinspektionen. <a href="https://www.fi.se/sv/vara-register/blankningsregistret/">Source</a> <br>
    Denmark: Finanstilsynet. <a href="https://oasm.finanstilsynet.dk/dk/soegmeddelelse.aspx">Source</a><br> Finland: Finanssivalvonta. <a href="https://www.finanssivalvonta.fi/sv/Kapitalmarknaden/Emittenter-och-investerare/transaktioner-utforda-av-personer-i-ledande-stallning/lyhyeksimyynti-taulukko/">Source</a>
    <br>Germany: Bundesanzeiger. <a href="https://www.bundesanzeiger.de/pub/en/to_nlp_start?0">Source</a>
  <br>France: Autorité des marchés financiers. <a href="https://bdif.amf-france.org/en_US/Resultat-de-recherche-BDIF?formId=BDIF&DOC_TYPE=BDIF&LANGUAGE=en&subFormId=dpcn&BDIF_TYPE_INFORMATION=BDIF_TYPE_INFORMATION_DPCN&TEXT=&BDIF_RAISON_SOCIALE=&bdifJetonSociete=&REFERENCE=&DATE_PUBLICATION=&DATE_OBSOLESCENCE=&valid_form=Start+search&isSearch=true&PAGE_NUMBER=">Source</a>    
 <br>United Kingdom: Financial Conduct Authority. <a href="https://www.fca.org.uk/markets/short-selling/notification-and-disclosure-net-short-positions">Source</a>
 <br>Spain: Comisión Nacional del Mercado de Valores. <a href="http://www.cnmv.es/portal/Consultas/Busqueda.aspx?id=29">Source</a>
  <br>Italy: Commissione Nazionale per le Società e la Borsa. <a href="http://www.consob.it/web/consob-and-its-activities/short-selling">Source</a>
 
    <br><br>

      ShortEurope.com is purely an information website. Do not make investment decisions based on the information on this site. The information about short positions are provided by the financial authorities in the respective countries, and is often incomplete. Some of the countries only provide the short percentage for each position with two decimal points, not the actual number of stocks. ShortEurope therefore estimates the number of stocks, but our numbers are sometimes incomplete or wrong. The estimation is based on the current number of stocks, normally updated once a day. Also, the numbers are not adjusted for dividends or splitting/joining stocks. 
      Always double check the numbers from this site. </br></br>
      A lot of historical positions are removed. This can be due to the stock price information not being available, or that the positions does not have the correct start- or endpoint, or that our price history provider does not have a complete price history. All the positions of companies that are no longer listed on the stock exchanges are deleted. There is also a chance that our code base is wrong. 
      Generally, shorting is for experts. If you are not an expert, buying normal stocks or funds is your best choice. Never invest money that you cannot afford to lose.  
      </br></br>
      The authorities only report positions above 0.5 percent. That means that aggregated short position in any given company is normally <i>bigger</i> than reported here on ShortEurope.com.  
 </br></br>
 <strong>Update times:</strong> Short positions in the respective countries are published every working day during the opening hours of the stock exchanges, and contains positions reported up to midnight the day before. Norway and Sweden updates at 15.30, Finland at 10.00 and after 17.00. Denmark updates several times a day. When available, ShortEurope downloads the new short positions and display updated data of all active and positions. 
  </br></br>
  The exact time for the short positions are not available, so ShortEurope makes some assumptions.
  If a position is reported for the date 16. July, ShortEurope assumes that the start price of the position is the closing price that day. Further, if the position is reported as 0 percent for 18. July, we set the position's end price to the closing price at 18. July. 
  </br></br>
  We use filters to sort out some short positions. For example are all positions older than 4 years removed from the active positions lists, as we consider them outdated. The only country currently reporting active positions older than four years is Germany. Also, short positions with company name or isin the we cannot find in our database, are removed. This may also apply for positions where stock prices (current or historic) are not available for some reason.  
  </br></br>
 Information on this site can be freely used and distributed, as long as the data and/or graphics are credited.</br> 
 Usage example: Credit: ShortEurope.com
 </br>
 If the information is used regurally, for example as a part of a newspaper, website or a monthly rapport, then ShortEurope's logo and url must also be included. 
  </br>
  Please contact us if you have any suggestions on how to improve this site or if you find any bugs. <br>
  send us an email on the following address: "magnus" at this domain "shorteurope.com".
    </br></br>
      </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>
 </main>
  </body>
</html>