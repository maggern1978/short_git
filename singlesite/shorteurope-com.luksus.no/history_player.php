

<?php include 'header.html'; 

//$land = 'nor';
require '../production_europe/namelink.php';
require '../production_europe/functions.php';

//$playernavn = $fil_teller = $i = "";
$playernavn = $selskapsnavn = $land = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $playernavn = test_input($_GET["player"]);
  $land = test_input($_GET["land"]);
  $playernavn = linktoname($playernavn);
  //echo $playernavn;
}

include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';

//get file location
$firsttwoletters = getFirstTwoLetters($playernavn);
$filename = $firsttwoletters . '.json';



if (!isset($firsttwoletters[0]) or !isset($firsttwoletters[1]) or !$selskapsposisjoner = readJSON('../shorteurope-com.luksus.no/dataraw/stock.prices/' . $land . '/players/' . $firsttwoletters[0] . '/' . $firsttwoletters[1] . '/' . $filename))
{


  ?>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php  
        echo 'No positions found for ' . ucwords($playernavn) .  ' in ' . ucwords($land) . '.<br>';
        ?>
      </div>
    </div>
  </div>
  <?php
  return;
}

$land_navn = str_replace('_',' ', $land);
$land_navn = ucwords($land_navn);

$pageTitle = 'Full shorting history for ' . ucwords(strtolower($playernavn)) . ' in ' . $land_navn;

$success = 0;

?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row">
    <div class="col-12">
      <?php 
      $playernavnNew  = strtolower($playernavn);
      $playernavnNew  = ucwords($playernavnNew);

      ?>
      <h2><?php echo $playernavnNew; ?><span class="float-right"><?php echo ucwords($land_navn); ?></span></h2>
      Summary for all available positions:<br>

      <?php 

      $selskapsposisjonercount = count($selskapsposisjoner);
      $totalSum = 0;
      $successanyhits = 0;


      for ($i = 0; $i < $selskapsposisjonercount; $i++) {

        if (!isset($selskapsposisjoner[$i]))
        {
          continue;
        }

        if (strtolower($selskapsposisjoner[$i]['PositionHolder']) == strtolower($playernavn))  
        {

          $hitholder = $selskapsposisjoner[$i];
          $successanyhits = 1;

          foreach ($selskapsposisjoner[$i]['summary'] as $summary)
          {

            $selskapNew = $summary['StockName'];
            $selskapNew  = strtolower($selskapNew);
            $selskapNew  = ucwords($selskapNew);
            $selskapNewx  = nametolink($selskapNew);


            if ($summary['SumOfAllValueChangeForPlayer'] > 0) 
            {
             $earned = ($summary['SumOfAllValueChangeForPlayer']/1000000);
             $earned = round($earned,2);
             $earned = number_format($earned,2,".",",");
             echo 'Earned <span class="text-success"><strong> ' . $earned;
             echo ' million ';
             echo '</strong> </span>on ';
             echo '<a href="#' . $summary['StockName'] . '">';
             echo $selskapNew . '</a>';
             echo '<a href="history_company.php?selskapsnavn=' . 
             $selskapNewx  . '&land=' . $land . '"> (history)</a><br>';

           }
           elseif ($summary['SumOfAllValueChangeForPlayer'] == 0) 
           {
             echo 'Result is <strong> ' . number_format(($summary['SumOfAllValueChangeForPlayer']/1000000),2,",",".");
             echo ' million '; 
             echo ' </strong></span>on ';
             echo '<a href="#' . $summary['StockName'] . '">';
             echo $selskapNew . '</a>';
             echo '<a href="history_company.php?player=X&selskapsnavn=' . 
             nametolink($selskapNew)  . '&land=' . $land . '"> (history)</a><br>';

           }
           else 
           {
             $lost = ($summary['SumOfAllValueChangeForPlayer']/1000000);
             $lost = round($lost,2);
             $lost = number_format($lost,2,".",",");
             echo 'Lost <span class="text-danger"><strong> ' . $lost;
             echo ' million ';
             echo '</strong> </span>on ';
             echo '<a href="#' . $summary['StockName'] . '">';
             echo $selskapNew . '</a>';

             echo '<a href="history_company.php?selskapsnavn=' . 
             nametolink($selskapNew)  . '&land=' . $land . '"> (history)</a><br>';
           }

           $totalSum += $summary['SumOfAllValueChangeForPlayer'];
           $success = 1; 
         }           
       }
     }

     if ($successanyhits > 0)
     { 

      $metatotal = $totalSum/1000000;
      echo '<h5 class="mt-2">Sum: ' . number_format($totalSum/1000000,2,".",",") . ' million ' . $currency_ticker . '</h5>';
      echo '<p>Note: Ongoing positions are updated after the stock exchange is closed. To see todays changes, see active positions.</p>';
      echo '<hr>';
    }

    if ($success == 0) {
      echo 'No hits!';
      return;
    }

    ?>
  </div>
</div>
</div>

<?php 

if ($success == 1) 
{

  foreach ($hitholder['summary'] as $companysummary)
  {
    $targetname = $companysummary['StockName'];

    ?>
    <div class="container">
      <div class="row">
        <div class="col-12">
          <?php

          $nameNew = $targetname;
          $nameNew = strtolower($nameNew);
          $nameNew  = ucwords($nameNew);

            //var_dump($location);
          echo '<h1 id="' . $targetname . '">' . $nameNew;
          echo '<span class="h5 pt-3 pull-right">Sum change: ';
          echo number_format(round($companysummary['SumOfAllValueChangeForPlayer']/1000000,2),2,".",",");
          echo ' million ' . $hitholder['positions'][0]['currency'] . '</h1>';

          ?>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-12">
          <table class="table table-striped">
            <thead class= "thead-dark">
              <tr>
                <th scope="">#</th>
                <th scope="col">From</th>
                <th scope="col">To</th>
                <th scope="col" class="text-right">StartPrice</th>
                <th scope="col" class="text-right">EndPrice</th>
                <th scope="col" class="text-right text-nowrap">Short-%</th>
                <th scope="col" class="text-right">#stocks</th>
                <th scope="col" class="text-right">Change</th>
              </tr>
            </thead>

            <?php 
            echo '<tbody>';

            //echo count($selskapsposisjoner[$location]['positions']);
            //var_dump($selskapsposisjoner[$location]['positions'][0]);
            $counter = 1;
            foreach ($hitholder['positions'] as $posisjon)
            {
              if ($posisjon['StockName'] != $targetname)
              {
                continue;
              }
                //var_dump($posisjon);
              $switch = 0;
              if ($posisjon['isActive'] == 'yes') {
                echo '<tr class="bg-yellow">';
                $switch = 1;

              }
              else {
                echo '<tr>';
                $switch = 0;
              }

              echo '<td>' . $counter . '.</td>';
              echo '<td class="text-nowrap">' . $posisjon['ShortingDateStart'] . '</td>';
              echo '<td class="text-nowrap">' . $posisjon['ShortingDateEnd'];
              if ($switch == 1) {
                echo ' (ongoing)';
              }

              echo '</td>';

              if (is_numeric($posisjon['ShortingDateStartStockPrice']))
              {
               echo '<td class="text-right">' . 
               number_format($posisjon['ShortingDateStartStockPrice'],2,".",","). '</td>';
             }
             else
             {
               echo '<td class="text-right">-</td>';
             }

             if (is_numeric($posisjon['ShortingDateEndStockPrice']))
             {
               echo '<td class="text-right">' . 
               number_format($posisjon['ShortingDateEndStockPrice'],2,".",","). '</td>';
             }
             else
             {
               echo '<td class="text-right">-</td>';
             }

             echo '<td class="text-right">' . 
              $posisjon['ShortPercent'] . '%</td>';

             if (is_numeric($posisjon['NumberOfStocks']))
             {
                 echo '<td class="text-right">' . 
             number_format($posisjon['NumberOfStocks'],0,".",",") . '</td>';
             }
             else
            {
                 echo '<td class="text-right">-</td>';
            }

            if (is_numeric($posisjon['valueChange']))
            {

             echo '<td class="text-right">' . 
             number_format($posisjon['valueChange']*-1/1000000,2,".",","). '&nbsp;M&nbsp;' . $posisjon['currency'] . '</td>';
            }
            else
            {
               echo '<td class="text-right">-</td>';
            }


             echo '</tr>';
             $counter++;
           }
           echo '</tbody>';
           echo '</table>';
           ?>
         </div>
       </div>

     </div>

     <br>

     <?php  } ?>
     <?php } ?>

     <?php include 'footer.php'; ?>
     <?php include 'footer_about.html'; ?>


   </main>

