<?php

$error_message = 'Email does not pass check. Please go back and try again.';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (!isset($_GET["email"]))
  {
  	echo $error_message;
  	return;
  }

  $email = test_input($_GET["email"]);
  
  if (isset($_GET["source"]))
  {
    $source = test_input($_GET["source"]);
  }
  else
  {
    $source = false;
  }

}

if (count($_GET) < 2)
{
	echo 'No subscriptions';
	return;
}


//Load Composer's autoloader
require '../../newsletter/vendor/autoload.php';
use EmailChecker\EmailChecker;

$checker = new EmailChecker();
if (!$checker->isValid($email))
{
	echo $error_message;
    return;
}


$emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

if (filter_var($emailB, FILTER_VALIDATE_EMAIL) === false ||  $emailB != $email ) 
{
    echo $error_message;
    exit(0);
}


if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    // invalid emailaddress
    echo $error_message;
    return;
}

//END OF VALIDATION
include '../../production_europe/functions.php';

$log_file_name = '../../newsletter/log/log.json';

if (!$data = readJSON($log_file_name))
{
	echo 'Read error, restarting... ';
	$data = [];
}

$subs = [];

foreach ($_GET as $key => $value)
{
  if ($key == 'email' or $key == 'source')
  {
    continue;
  }
  
  $subs[] = test_input($key);
}

$obj = [];
$obj['email'] = $email;
$obj['time'] = date('Y-m-d H:i:s');
$obj['timestamp'] = time();
$obj['source'] = $source;
$obj['ip'] = getUserIP();;
$obj['subscriptions'] = $subs;
//add inn secret code for editing 
$obj['secret'] = mt_rand(0,999999999);
$obj['hash'] = md5($email);

$data[] = $obj;

//check if resubmitting is happening

$hitcounter_email = 0;
$hitcounter_ip = 0;

//var_dump($data);

foreach ($data as $key => $input)
{

	if ($email == $input['email'] and $input['timestamp'] + 3600 > $obj['timestamp'])
	{
		//echo 'newer';
		$hitcounter_email++;
	}

	if ($obj['ip']== $input['ip'] and $input['timestamp'] + 3600 > $obj['timestamp'])
	{
		//echo 'newer';
		$hitcounter_ip++;
	}

	//clean if wanted
	if ($obj['timestamp'] - $input['timestamp'] > 3600*24*7)
	{
		//unset($data[$key]);
	}

}

if ($hitcounter_email > 2)
{
	echo 'Too many requests for this email. Please try again later. Contact us if the problem persists.';
	return;
}

if ($hitcounter_ip > 2)
{
	echo 'Too many requests from this ip adress. Please try again later. Contact us if the problem persists.';
	return;
}

$hash_email = $obj['hash'];

if (file_exists("../../newsletter/users/confirmed/$hash_email"))
{
  echo 'Email already confirmed.';
  return;
}

if (!file_exists("../../newsletter/users/unconfirmed/$hash_email"))
{
	mkdir("../../newsletter/users/unconfirmed/$hash_email");
}

saveJSONsilent($data, $log_file_name); //save

include '../../newsletter/send_email_confirmation.php';


//funksjon for å rense data
function test_input($data) 
{
  $data = filter_var($data, FILTER_SANITIZE_STRING);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// Function to get the user IP address
function getUserIP() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

?>