<?php

require '../production_europe/namelink.php';
require '../production_europe/functions.php';


if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $country = test_input($_GET["country"]);
}


$land = $country;



include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';

$positions = '../shorteurope-com.luksus.no/datacalc/sector/' . $land . '/sector_' . $land . '.current.json';


$land = $country;

$json = readJSON($positions);

$pageTitle = 'Most shorted sectors in ' . ucwords($country);
$description = 'List over all shorted sectors (including subsectors) and the market value of their short positions. Most shorted sectors are ';
$description = $description . $json[0]['sectorname'] . ', ' . $json[1]['sectorname'] . ' and ' . $json[2]['sectorname'] . '.';

$count = count($json);

//get total value
$totalValue = 0;
for ($i = 0; $count > $i; $i++) {
  $totalValue += $json[$i]['marketValue'];
}

//get highest sector, for making the bar full width
$higest = 0;
$ratio = 1/($json[0]['marketValue']/$totalValue);

//$totalValue = round($totalValue,2);

//take away a little for looks
$ratio = $ratio *0.9;
$percentTotal = 0;

?>
<?php include 'header.html'; ?>
<?php include 'ads/banner_720.html'; ?>
<script src="./assets/vendor/jquery/jquery.min.js"></script>

<div class="container">
  <div class="row ">
<div class="col-12">
<h3 class="">Short positions by sector in <?php echo ucwords($land); ?></h3> 
<p>Sector and subsector.<p>
<table class="table table-bordered table-hover table-sm">
    <thead class="">
      <tr>
        <th>#</th>
        <th>Sector</th>
        <th class="text-right" style="min-width: 80px"; >Market value in <?php echo $currency_ticker; ?></th>
        <th class="text-right">Percent of total</th>
        <th class=""></th>
      </tr>
    </thead>
    <tbody>

    <?php $counter = 0;
    foreach ($json  as $key => $sector) {
    $counter++;
    echo '<tr>';
    echo '<td>' . $counter . '.</td>';
    echo '<td>';
    echo $sector['sectorname'];
    echo '<span class="pull-right">';
    echo '</span>';
    echo '<div class=" subcategory' . $counter . '">';

    foreach ($sector['subcategories'] as $subcategory) {
      echo '<i class="fa text-warning fa-arrow-circle-right" aria-hidden="true"></i> ';
      echo '<a href ="sector_companies.php?country=' . $land . '&subsector=' . nametolink($subcategory['subcategoryname']);
      echo '">';
      echo $subcategory['subcategoryname'];
      echo '</a>';
      echo '<br>';
    }
    echo '</div>';
    echo '</td>';

    echo '<td class ="text-right">';
    $value = round($sector['marketValue'],1);
    echo $value;
    echo ' M ';

    echo '<div class=" subcategory' . $counter . '">';

    foreach ($sector['subcategories'] as $subcategory) {
      $localvalue = round($subcategory['subcategoryvalue'],1);
      echo '' . $localvalue . ' M<br>';
    }
    echo '</div>';
    echo '</td>';
    echo '<td class="text-right">';
    $percent = ($sector['marketValue']/$totalValue)*100;
    $percentTotal += $percent;
    echo round($percent,2);
    echo ' %';
    echo '<div class=" subcategory' . $counter . '">';

    foreach ($sector['subcategories'] as $subcategory) {
        $localvalue = $subcategory['subcategoryvalue'];
        $localpercent = ($localvalue/$totalValue)*100;
        echo round($localpercent,2);
        echo ' %<br>';
    }
    echo '</div>';
    echo '</td>';
    echo '<td class=""  style="width: 30%;">';
    echo '<div class="bg-primary" style="width: ';
    $cellWidth = $percent*$ratio;
    echo $cellWidth;
    echo '%; height: 100%">';
    echo '&nbsp;</div>';
    echo '<div class=" subcategory' . $counter . '">';

    foreach ($sector['subcategories'] as $subcategory) {
      $percent = ($subcategory['subcategoryvalue']/$totalValue)*100;
      echo '<div class="bg-warning " style="width: ';
      $cellWidth = $percent*$ratio;
      echo $cellWidth;
      echo '%; height: 23px; border-top: 3px solid white;">';
      echo '&nbsp;</div>';
    }
    echo '</div>';
    echo '</td>';
    echo '</tr>';
  
}
    echo '<tr class="font-weight-bold">';
    echo '<td>';
    echo '</td>';
    echo '<td>';
    echo 'Sum:';
    echo '</td>';
    echo '<td class="text-right">';
    echo number_format($totalValue,1,".",",");
    echo ' M</td>';
    echo '<td class="text-right">';
    echo round($percentTotal,1);
    echo '%</td>';
    echo '<td>';
    echo '</td>';
    echo '</tr>';

?>
 </tbody>
  </table>

       <?php
           echo "Updated: " . date("Y-m-d H:i",filemtime($positions));
        ?>
</div>
</div>
<br>
</div>
<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>

 </main>
  </body>
</html>