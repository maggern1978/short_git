<?php include 'header.html'; 


$country = $type = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
}

include '../production_europe/functions.php';
require '../production_europe/namelink.php';
include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';
require '../production_europe/logger.php';

if (!function_exists('readJSON')) {
  function readJSON($file_location_and_name) {
      $string = file_get_contents($file_location_and_name);
      return $data = json_decode($string, true);
  }
}

function normalizeDecimal($val, int $precision = 2): string
{
    $input = str_replace(' ', '', $val);
    $number = str_replace(',', '.', $input);
    if (strpos($number, '.')) {
        $groups = explode('.', str_replace(',', '.', $number));
        $lastGroup = array_pop($groups);
        $number = implode('', $groups) . '.' . $lastGroup;
    }
    return bcadd($number, 0, $precision);
}


//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


$filename = '../shorteurope-com.luksus.no/datacalc/shortlisten_' . $land . '_current.csv';

$landnavn = ucwords($land);
$graphlocation = '../shorteurope-com.luksus.no/datacalc/graphdata/' . $land . '/';

$csvFile = file($filename);            
$data = [];
foreach ($csvFile as $line) {
     $data[] = str_getcsv($line); 
} 

$pageTitle = 'Most shorted companies in' . ' ' . ucwords($landnavn);
$description = 'Short positions in ' . ucwords($landnavn) . ' ranked by percent. ';


$allBox = [];
//Hent ut relevante data
foreach ($data as $key => $entry) 
{
    if ($key == 0)
    {
        continue; 
    }

    $temp_navn = $entry[0];

    if ($land  == 'sweden' ) {
        $temp_navn = strtolower($temp_navn);
        $temp_navn = ucwords($temp_navn);
    }

    $object = new Stdclass;
    $object->selskapsnavn = $temp_navn;
    $object->shortprosent = $entry[5];
    $object->ticker = $entry[2];
    $object->value = (float)$entry[4] * (float)$entry[6];
    $object->basecurrency = $entry[12];
    $allBox[] = (array)$object;
}

$today = date('Y-m-d');
$a_week_ago = date('Y-m-d');
$a_week_ago = date( "Y-m-d", strtotime("-5 Weekday", strtotime($a_week_ago)));

usort($allBox, function($a, $b) {
    return $b['shortprosent'] > $a['shortprosent'];
});


foreach ($allBox as $index => $company) {

  break; //dont use this 
  
    $tickerLocation = $graphlocation  . strtolower($company['ticker']) . '.json';
  
    if (file_exists($tickerLocation)) {
        //echo $index . '. Fant: ' . $tickerLocation . ' for ' . $company['selskapsnavn'] . ' | ';

        if (!$stockdata = readJSON($tickerLocation))
        {
          //errorecho('Could not read json file ' . $tickerLocation . '<br>');
          $allBox[$index]['tidligereShortprosent'] = '-';
          continue;
        }

        $stockdatacount = count($stockdata);

        $foundsuccess = 0;

        for ($i = $stockdatacount-1; $i > 0; $i--)
        {
            if ($stockdata[$i]['date'] == $a_week_ago)
            {
                //echo 'Fant tickerdata for en uke siden på dato ' . $a_week_ago;
                if (isset($stockdata[$i]['shortPercent']))
                {
                  $length = strlen($stockdata[$i]['shortPercent']);
                }
                else
                {
                  $length = 0;
                }

              
                if ($length == 0)
                {
                    //errorecho('Shortprosent for tidligere dato er ikke oppgitt i tickerdata...<br>');
                    $allBox[$index]['tidligereShortprosent'] = '-';
                    break;
                }
                else
                {
                    //echo ' | adding shortprosent ' . $stockdata[$i]['shortPercent'] . '<br>';
                    $allBox[$index]['tidligereShortprosent'] = $stockdata[$i]['shortPercent'];
                    $foundsuccess = 1;
                }

                break;
            }
        }

        if ($foundsuccess == 0)
        {
            //echo('Fant ikke dato en uke tidligere!<br>');
            $allBox[$index]['tidligereShortprosent'] = '-';
        }

    }
    else 
    {
        //echo 'Fant IKKE TICKER: ' . $tickerLocation . '<br>';
        $allBox[$index]['tidligereShortprosent'] = '-';
    }

}

//var_dump($short_prosent);
//var_dump($selskapsnavn);
$totalsum = 0;

?>

<?php include 'ads/banner_720.html'; ?>

<div class="container">
  <div class="row ">
<div class="col-12">

<h1>Most shorted companies in <?php echo ucwords($landnavn); ?> by percent</h1> 
    <a href="most_shorted_companies_value.php?country=<?php echo strtolower($landnavn); ?>" class="btn mb-3 btn-primary" role="button">Most shorted by value 
    </a>
<table class="table table-bordered table-sm">
    <thead class="thead-dark">
      <tr>
        <th>#</th>
        <th>Name</th>
        <th class="text-right">Short</th>
        <!--
        <th class="text-right most-shorted-table-heading-a-week-ago"  data-toggle="tooltip" data-placement="top" title="<?php echo $a_week_ago; ?>">A week ago</th>-->
        <th class="text-right">Value</th>
      </tr>
    </thead>
    <tbody>
   
    <?php $counter = 1;
    foreach ($allBox as $key => $selskap) {
     
     $teller = $key + 1; 
    
    // if ($selskap['tidligereShortprosent'] == '-') 
     //{
     //   $arrow = ' <i class="fa fa-arrow-circle-right "> </i>';
     //}
     //else if ($selskap['shortprosent'] > $selskap['tidligereShortprosent']) 
     //{
     //   $arrow = ' <span class="text-success"><strong><i class="fa fa-arrow-circle-up "></i>';
    // }
     //else if ($selskap['shortprosent'] == $selskap['tidligereShortprosent']) 
     //{
     //    $arrow = ' <i class="fa fa-arrow-circle-right "> </i>';
     //}
     //else 
     //{
      //  $arrow = ' <span class="text-danger"><strong><i class="fa fa-arrow-circle-down "></i>';
     //}
     $arrow = '';

    echo '<tr>';
    echo '<td>' .  $teller  . '.</td>';
    $singleCompany = nametolink($selskap['selskapsnavn']);
    $singleCompany = strtoupper($singleCompany);
    echo '<td>';
    echo '<a href="details_company.php?company=' . $singleCompany  . '&land=' . $land . '">';
    echo $selskap['selskapsnavn'] . '</td>';
    echo '<td class="text-right">' . $selskap['shortprosent'] . '%' . $arrow . '</td>';
    //echo '<td class="text-right">' . $selskap['tidligereShortprosent'] . '%' . '</td>';
    $value = $selskap['value']/1000000;
    $totalsum += $value;
    echo '<td class="text-right">' . number_format(round($value,2),2,".",",") . ' M ' . $selskap['basecurrency'] . '</td>';
    echo '</tr>';
  
}    

echo '<tr class="font-weight-bold">';
echo '<td class="text-right">' . '</td>';
echo '<td>Sum: ' . '</td>';
echo '<td class="text-right">' . '</td>';
//echo '<td class="text-right">' . '</td>';
echo '<td class="text-right">' . number_format($totalsum,2,".",",")  . ' M</td>';
echo '</tr>';


$description = $description . 'A total of ' . $counter . ' companies are currently shorted for a value of ' . number_format($totalsum,0,".",",") . ' million ' . $currency_ticker ;

?>

    </tbody>
  </table>

    <div class="container">
        <div class="row">
           <?php
               echo "Updated: ".date("Y-m-d H:i",filemtime($filename));
            ?>
        </div>
    </div>

</div>
</div>

<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>
 </main>
  </body>
</html>