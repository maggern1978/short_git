<?php 

//This is based on the main isin list in dataCalc/isin/
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include '../production_europe/functions.php';
set_time_limit(4000);
	
$filename = '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_currency_adder_misslist.csv';

$list = readCSV($filename);
$urlstart = 'https://eodhistoricaldata.com/api/fundamentals/';
$urlend = '?api_token=5da59038dd4f81.70264028';

//https://eodhistoricaldata.com/api/shorts/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&from=2000-01-01
//$urlstart = 'https://eodhistoricaldata.com/api/shorts/';
//$urlend = $urlend . '&from=2000-01-01';

$misslist = [];

foreach ($list as $key => $row)
{
			
	$ticker = $row[1] . '.' . $row[8];
	$ticker = str_replace(" ", "-", $ticker);
	$url = '../production_europe/json/fundamentals/' . $ticker . '.json';
	$url = strtolower($url);
	
	
	if (!file_exists($url))
	{
		
		$weburl = $urlstart . $ticker . $urlend;
		echo $key . '. Reading ' . $url . '. ';
		echo (' File not found, downloading... ' . $weburl);

		if ($data = download($weburl))
		{
			
			if (strpos($data, 'not found'))
			{
				errorecho(' Message: "Ticker not found", skipping. ');
				saveJSON($data, $url);
			}
			else
			{
				$data = json_decode($data, true);

				saveJSON($data, $url);
			}
			
		}
		else
		{
			echo $key . '. Reading ' . $url . '. ';
			errorecho(' File not found. Download error ');
			$data = '0';
			saveJSON($data, $url);
		}
	}
	else
	{
		continue;
	}

	if ($key > 25)
	{
		//exit();
	}
	echo '<br>';

}
	

	
?>
