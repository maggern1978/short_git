<?php 

//This is based on the main isin list in dataCalc/isin/
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include '../production_europe/functions.php';
set_time_limit(4000);
	
$filename = '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder.csv';

$list = readCSV($filename);
$urlstart = 'https://eodhistoricaldata.com/api/fundamentals/';
$urlend = '?api_token=5da59038dd4f81.70264028';

//https://eodhistoricaldata.com/api/shorts/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&from=2000-01-01
//$urlstart = 'https://eodhistoricaldata.com/api/shorts/';
//$urlend = $urlend . '&from=2000-01-01';

$misslist = [];

foreach ($list as $key => $row)
{
	if ($key == 0)
	{
		continue;
	}
		
	$ticker = $row[1] . '.' . $row[8];
	$url = '../production_europe/json/fundamentals/' . $ticker . '.json';
	$url = strtolower($url);
	$url = str_replace(" ", "-", $url);

	echo $key . '. Reading ' . $url . '. ';

	if (!file_exists($url))
	{
		errorecho ('File not found, adding to misslist and continuing<br>');
		$misslist[] = $row;
		continue;
	}

	if ($data = readJSON($url))
	{
		if (isset($data['General']['Sector']))
		{
			echo ' Setting sector. ';
			$list[$key][4] = $data['General']['Sector'];
		}
		
		if (isset($data['General']['Industry']))
		{
			echo ' Setting Industry. ';
			$list[$key][5] = $data['General']['Industry'];
		}	
		
		if (isset($data['General']['CurrencyCode']))
		{
			echo ' Setting currency. ';
			$list[$key][3] = $data['General']['CurrencyCode'];
		}
		
	}
	else
	{
		errorecho("Reading of $url failed, adding to misslist<br>");
		$misslist[] = $row;
	}
	echo '<br>';
}
	
saveCSVsemikolon($list, '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_currency_adder.csv');
saveCSVsemikolon($misslist, '../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_currency_adder_misslist.csv');
	
?>
