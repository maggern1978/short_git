<?php 

//This is based on the main isin list in dataCalc/isin/
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include 'functions.php';
set_time_limit(4000);
	
$filename = 'main_isin_list.csv';

$list = readCSV($filename);

	
$urlstart = 'https://eodhistoricaldata.com/api/eod/';

$urlstart = 'https://eodhistoricaldata.com/api/fundamentals/';
$urlend = '?api_token=5da59038dd4f81.70264028';

//https://eodhistoricaldata.com/api/shorts/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&from=2000-01-01
//$urlstart = 'https://eodhistoricaldata.com/api/shorts/';
//$urlend = $urlend . '&from=2000-01-01';
	
foreach ($list as $key => $row)
{
	if ($key == 0)
	{
		continue;
	}
		
	$ticker = $row[1] . '.' . $row[8];
	$url = 'fundamental_download/' . $ticker . '.json';
	$url = strtolower($url);

	echo $key . '. Reading ' . $url . '. ';

	if (!file_exists($url))
	{
		echo 'File not found, continuing<br>';
		continue;
	}

	if ($data = readJSON($url))
	{
		if (isset($data['General']['Sector']))
		{
			$list[$key][4] = $data['General']['Sector'];
		}
		
		if (isset($data['General']['Industry']))
		{
			$list[$key][5] = $data['General']['Industry'];
		}	
		
		if (isset($data['General']['CurrencyCode']))
		{
			$list[$key][3] = $data['General']['CurrencyCode'];
		}
		
	}
	echo '<br>';
}
	
saveCSVsemikolon($list, $filename);
	
?>
