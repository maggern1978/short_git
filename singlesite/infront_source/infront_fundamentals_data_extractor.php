<?php 

//This is based on the main isin list in dataCalc/isin/
//sets industri, secotr and currency if available
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include '../production_europe/functions.php';
set_time_limit(4000);
$list = [];


$filelist = listfiles('download/fundamentals/');
	
foreach ($filelist as $key => $file)
{
	
	$url = 'download/fundamentals/' . $file;

	//GB0006640972;FOUR;"4imprint Group plc";GBX;"Communication Services";"Advertising Agencies";UK;LSE;LSE

	if ($data = readJSON($url))
	{
		//var_dump($data);

		if (isset($data['General']['ISIN']))
		{
			$list[$key][0] = $data['General']['ISIN'];
			echo $list[$key][0] . ' | ';
		}
		else
		{
			continue;
			$list[$key][0] = '';
		}

		if (isset($data['General']['Code']))
		{
			$list[$key][1] = $data['General']['Code'];
			echo $list[$key][1] . ' | ';
		}
		else
		{
			$list[$key][1] = '';
		}

		if (isset($data['General']['Name']))
		{
			$list[$key][2] = $data['General']['Name'];
			echo $list[$key][2] . ' | ';
		}
		else
		{
			$list[$key][2] = '';
		}

		if (isset($data['General']['CurrencyCode']))
		{
			$list[$key][3] = $data['General']['CurrencyCode'];
			echo $list[$key][3] . ' | ';
		}
		else
		{
			$list[$key][3] = '';
		}

		if (isset($data['General']['Sector']))
		{
			$list[$key][4] = $data['General']['Sector'];
			echo $list[$key][4] . ' | ';
		}
		else
		{
			$list[$key][4] = '';
		}


		if (isset($data['General']['Industry']))
		{
			$list[$key][5] = $data['General']['Industry'];
			echo $list[$key][5] . ' | ';
		}
		else
		{
			$list[$key][5] = '';
		}


		if (isset($data['General']['CountryName']))
		{
			$list[$key][6] = $data['General']['CountryName'];
			echo $list[$key][6] . ' | ';
		}
		else
		{
			$list[$key][6] = '';
		}

		if (isset($data['General']['Exchange']))
		{
			$list[$key][7] = $data['General']['Exchange'];
			echo $list[$key][7] . ' | ';
		}
		else
		{
			$list[$key][7] = '';
		}


		if (isset($data['General']['Exchange']))
		{
			$list[$key][8] = $data['General']['Exchange'];
			echo $list[$key][8] . ' | ';
		}
		else
		{
			$list[$key][8] = '';
		}

		
	}
	else
	{
		errorecho('Could not parse json');
	}
	echo '<br>';

}

//var_dump($list);
$filename = 'out/infront_isinlist.csv';

//lagre sorted array etter isin
usort($list, function ($item1, $item2) {
    return $item2[0] < $item1[0];
});

saveCSVsemikolon($list, $filename);
	
?>
