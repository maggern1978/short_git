<?php 

include '../production_europe/functions.php';

$files = listfiles('input/');

$all_list = [];


$tab = "\t";

foreach ($files as $file)
{

	$filename = 'input/' . $file;
	echo $file . '<br>';
	$fp = fopen($filename, 'r');

	while ( !feof($fp) )
	{
		$line = fgets($fp, 2048);	
		$data_txt = str_getcsv($line, $tab);

		$isin = '';
		$name = '';
		$ticker = $data_txt[0];

		if (has_many_minuses($data_txt,8))
		{
			//echo 'Too many minuses!<br>';
			//var_dump($data_txt);
			continue;
		}


		foreach ($data_txt as $key => $entry)
		{
			
			if (is_isin_new($entry))
			{
				$isin = $entry;
				$runner = $key +1;
				$name = $data_txt[$runner];
				break;
			}
		}

		if (!is_isin_new($isin))
		{
			continue;
		}

		if ($name == '' or $ticker == '' or $ticker == '-')
		{
			continue;
		}

		$ticker = str_replace(" ", "-", $ticker );

		//remove *
		$ticker = str_replace("*", "", $ticker);
		$name = str_replace("*", "", $name);
		$isin = str_replace("*", "", $isin);

		$line = array (
			$isin, $name, $ticker
			);
		
		echo '|';
		$all_list[] = $line;

	}                              

	echo '<br>';

	fclose($fp);

}

saveCSVx($all_list, 'out/infront_isin_list.csv');


function has_many_minuses($array, $count)
{

	$string_imploded = implode($array);
	$count_imploded = substr_count($string_imploded, '-');

	if ($count_imploded >= $count)
	{
		return true;
	}
	else
	{
		return false;
	}


}

function is_isin_new($string)
{
	if (!is_string($string))
	{
		return false;
	}

    //DE0006048432
	$string = trim($string);

	$length = strlen($string);

	if ($length != 12)
	{
		//echo 'Length er ' . $length . ' (false)<br>';
		return false;
	}

	for ($i = 0; $i < 12; $i++)
	{
		if ($i < 2)
		{
			continue;
		}

		if (!is_numeric($string[$i]))
		{
			//echo 'Is not numberic ' . $string[$i]. '<br>';
			return false;
		}

	}

	return true;

}



?>
