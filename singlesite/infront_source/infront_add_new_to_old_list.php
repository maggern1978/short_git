<?php

include '../production_europe/functions.php';
set_time_limit(4000);

$oldlist = readCSV('../shorteurope-com.luksus.no/dataraw/isin/hovedlisten_landkoder_isinsortert.csv');
$newlist = readCSV('out/infront_isinlist.csv');


//do search on isin
//if isin fails, then search for name
//function binary_search_isinliste_isin($needle, $haystack) 
//logic is adding old to new list

$newcount = count($newlist);
$oldcount = count($oldlist);

$add_candidates = [];

echo 'Companies in new list: ' . $newcount . '<br>';

//if old isin is not found in new list, then add to candidates list
for ($i = 0; $i < $oldcount; $i++) //ta en isin fra gammel liste
{

	$isintarget = $oldlist[$i][0];
	$foundsuccess = 0;

	if ($row = binary_search_isinliste_isin($isintarget, $newlist)) //$needle, $haystack
	{
		//echo $i . '. ' . $isintarget . ', found in new isinlist!<br>';
		//var_dump($isinliste_isinsortert[$row]);
		//$row = $isinliste_isinsortert[$row];
	}
	else
	{
		//echo $i . '. ' . $isintarget . ', NOT found in new isinlist!<br>';
		$add_candidates[] = $oldlist[$i];
	}

}

$candidates_count = count($add_candidates);

flush_start();
echo 'Numer of candidates: ' . $candidates_count . '<br>';
flush_end();

//take candicates and check if name is found in new list. If yes unset. 
for ($i = 0; $i < $candidates_count; $i++)
{
	flush_start();
	$nametarget = mb_strtolower($add_candidates[$i][2]);
	$success = 0;

	for ($x = 0; $x < $newcount; $x++)
	{

		if (mb_strtolower($newlist[$x][2]) == $nametarget)
		{
			echo $i . '. Found ' . $nametarget . ' in new list, will unset. <br>';
			unset($add_candidates[$i]);
			flush_end();
			$success = 1;
			break;
		}

	}

	if ($success == 0)
	{
		echo $i . '. Did not find ' . $nametarget . ', keeping. <br>';
		flush_end();
	}

	
	
}

//if row in old list does not match on name or isin, then add to new list
foreach ($add_candidates as $row)
{
	$newlist[] = $row;
}

//make sure new list only have one of each identical row

echo 'Count before array_uniqe: ' . count($newlist) . '<br>';

$newlist = array_unique($newlist, SORT_REGULAR);
$newlist = array_values($newlist);

echo 'Count after array_uniqe: ' . count($newlist) . '<br>';

//fjerne "-me" i tickere

for ($i = 0; $i < count($newlist); $i++)
{

	$newlist[$i][1] = str_ireplace("-me", "", $newlist[$i][1]);
}

//fjern isin oppføringer som er duplikater hvor en av børsene ikke er europeisk


//lagre sorted array etter navn
usort($newlist, function ($item1, $item2) {
    return $item2[2] < $item1[2];
});

saveCSVsemikolon($newlist, 'out/hovedlisten_landkoder_outstanding_temp.csv');

?>