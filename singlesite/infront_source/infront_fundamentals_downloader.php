
<?php 

//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028



include '../production_europe/functions.php';
set_time_limit(14000);

$isinfiles = listfiles('download/isin/');

$urlstart = 'https://eodhistoricaldata.com/api/fundamentals/';
$urlend = '?api_token=5da59038dd4f81.70264028';

$skippingcounter = 0; 
$errorcounter = 0;
$progresscounter = 0;
$savecounter = 0; 

foreach ($isinfiles as  $index => $file)
{

	
	if (!$list = readJSON('download/isin/' . $file))
	{
		continue;
	}

	foreach ($list as $key => $entry)
	{

		flush_start();
		$progresscounter++;
		if ($key % 10 == 0)
		{
			echo "\n";
			echo 'Status:' . "\n";
			echo 'Skipped: ' . $skippingcounter . "\n";
			echo 'Errors: ' . $errorcounter . "\n";
			echo 'Progress: ' . $progresscounter . "\n";
			echo 'Savecounter: ' . $savecounter . "\n";
			timestamp();
			echo "\n";
		}

		if (!isset($entry['ISIN']) or $entry['ISIN'] == '')
		{
			echo 'ISIN not set, skipping...<br>';
			$errorcounter++;
			flush_end();
			continue;
		}

		$ticker = $entry['Code'] . '.' . $entry['Exchange'];
		$ticker = str_replace(' ', '-', $ticker);

		$url = $urlstart . $ticker . $urlend . '';
		$ticker = strtolower($ticker);

		$savepath = 'download/fundamentals/'. $ticker . '.json';
		$savepath = strtolower($savepath);
		$success = 0;

		if (file_exists($savepath))
		{
			flush_end();
			continue;
		}

		if ($data = json_decode(download($url),true))
		{
			echo $key . '. Downloading ' . $url . '<br>';
			saveJSONsilent($data, strtolower($savepath));
			$savecounter++; 

		}
		else
		{
			$errorcounter++;
		}
		
		flush_end();

	}
	break;
}	

echo 'Doing name download<br>';

$isinfiles = listfiles('download/name/');

foreach ($isinfiles as  $index => $file)
{

	if (!$list = readJSON('download/name/' . $file))
	{
		continue;
	}

	foreach ($list as $key => $entry)
	{

		flush_start();
		$progresscounter++;
		if ($key % 10 == 0)
		{
			echo "\n";
			echo 'Status:' . "\n";
			echo 'Skipped: ' . $skippingcounter . "\n";
			echo 'Errors: ' . $errorcounter . "\n";
			echo 'Progress: ' . $progresscounter . "\n";
			echo 'Savecounter: ' . $savecounter . "\n";
			timestamp();
			echo "\n";
		}

		if (!isset($entry['ISIN']) or $entry['ISIN'] == '')
		{
			echo 'ISIN not set, skipping...<br>';
			$errorcounter++;
			flush_end();
			continue;
		}

		$ticker = $entry['Code'] . '.' . $entry['Exchange'];
		$ticker = str_replace(' ', '-', $ticker);

		$url = $urlstart . $ticker . $urlend . '';
		$ticker = strtolower($ticker);

		$savepath = 'download/fundamentals/'. $ticker . '.json';
		$savepath = strtolower($savepath);
		$success = 0;

		if (file_exists($savepath))
		{
			flush_end();
			continue;
		}

		if ($data = json_decode(download($url),true))
		{
			
			echo $key . '. Downloading ' . $url . '<br>';
			saveJSONsilent($data, strtolower($savepath));
			$savecounter++; 

		}
		else
		{
			//echo $key . '. Skipping download of ' . $url . '. ' ;
			//echo 'Download or json_decode error.';
			$errorcounter++;
			//var_dump($data);
		}
		
		flush_end();

	}
	
}

?>