<?php 
if (!function_exists('utf8ize')) {
    function safe_json_encode($value, $options = 0, $depth = 512, $utfErrorFlag = false) {
        $encoded = json_encode($value, $options, $depth);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
            return $encoded;
            case JSON_ERROR_DEPTH:
                return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
                case JSON_ERROR_STATE_MISMATCH:
                return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
                case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
                case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
                case JSON_ERROR_UTF8:
                $clean = utf8ize($value);
                if ($utfErrorFlag) {
                    return 'UTF8 encoding error'; // or trigger_error() or throw new Exception()
                }
                return safe_json_encode($clean, $options, $depth, true);
                default:
                return 'Unknown error'; // or trigger_error() or throw new Exception()

            }
        }
    }

    if (!function_exists('utf8ize')) {
        function utf8ize($mixed) {
            if (is_array($mixed)) {
                foreach ($mixed as $key => $value) {
                    $mixed[$key] = utf8ize($value);
                }
            } else if (is_string ($mixed)) {
                return utf8_encode($mixed);
            }
            return $mixed;
        }
    }

    if (!function_exists('Memory_Usage')) {
        function Memory_Usage($decimals = 2)
        {
            $result = 0;

            if (!function_exists('memory_get_usage'))
            {
                $result = memory_get_usage() / 1024;
            }

            else
            {
                if (!function_exists('exec'))
                {
                    $output = array();

                    if (substr(strtoupper(PHP_OS), 0, 3) == 'WIN')
                    {
                        exec('tasklist /FI "PID eq ' . getmypid() . '" /FO LIST', $output);

                        $result = preg_replace('/[\D]/', '', $output[5]);
                    }

                    else
                    {
                        exec('ps -eo%mem,rss,pid | grep ' . getmypid(), $output);

                        $output = explode('  ', $output[0]);

                        $result = $output[1];
                    }
                }
            }

            return number_format(intval($result) / 1024, $decimals, '.', '');
        }
    }

    if (!function_exists('readJSON')) {
        function readJSON($file_location_and_name) {

            if (file_exists($file_location_and_name))
            {
                $data = file_get_contents($file_location_and_name);
                return $data = json_decode($data, true);
            }
            else
            {
            //errorecho('Error, file ' . $file_location_and_name .  ' does not exist<br>');
                return false;
            }

        }
    }

    if (!function_exists('download_json')) {

        function download_json($url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSLVERSION,1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $data = curl_exec($ch);
            $error = curl_error($ch); 
    //var_dump($data);
            if ($error != '') {
                var_dump($error);
                throw new Exception("download error" . $url);
            }
            curl_close ($ch);
            sleep(0.61);
            return $data;
        }
    }

    if (!function_exists('download')) {

        function download($url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSLVERSION,1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            
            $data = curl_exec($ch);
            $error = curl_error($ch); 

            if ($error != '') {
                var_dump($error);
                return false;
            }
            curl_close ($ch);
            return $data;
        }
    }

 //obs, dataene må ha semikolon som skilletegn
    if (!function_exists('readCSV')) {
       function readCSV($file_location_and_name) {

        $csvFile = file($file_location_and_name);

        //Les in dataene 
        $name = [];
        foreach ($csvFile as $line) {
            $name[] = str_getcsv($line, ';');
        }

        //ordne utf koding
        $counter = 0;
        foreach ($name as &$entries) {
            $data[$counter] = array_map("utf8_encode", $entries);
            $counter++;
        }
        return $name;
    }
}

 //obs, dataene må ha semikolon som skilletegn
if (!function_exists('readCSVkomma')) {
   function readCSVkomma($file_location_and_name) {

    $csvFile = file($file_location_and_name);

        //Les in dataene 
    $name = [];
    foreach ($csvFile as $line) {
        $name[] = str_getcsv($line, ',');
    }

        //ordne utf koding
    $counter = 0;
    foreach ($name as &$entries) {
        $data[$counter] = array_map("utf8_encode", $entries);
        $counter++;
    }
    return $name;
}
}

if (!function_exists('saveCSVx')) {
    function saveCSVx($input, $file_location_and_name) {

        echo 'Saving csv-file "' . $file_location_and_name . '"<br>';
        $fp = fopen($file_location_and_name, 'w');

        foreach ($input as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }
}

if (!function_exists('saveJSON')) {
    function saveJSON($data, $filename) {

        if ($fp = fopen($filename, 'w'))
        {
            fwrite($fp, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
            fclose($fp); 
            echo "Saved file " . $filename . "<br>";
        }
        else
        {
            return false;
        }
    }
}


if (!function_exists('saveJSON_silent')) {
    function saveJSON_silent($data, $filename) {

        if ($fp = fopen($filename, 'w'))
        {
            fwrite($fp, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
            fclose($fp); 
            //echo "Saved file " . $filename . "<br>";
        }
        else
        {
            return false;
        }
    }
}

if (!function_exists('saveJSONsilent')) {
    function saveJSONsilent($data, $filename) {

        if ($fp = fopen($filename, 'w'))
        {
            fwrite($fp, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
            fclose($fp); 
            //echo "Saved file " . $filename . " \n";
        }
        else
        {
            return false;
        }
    }
}

if (!function_exists('getcolor')) {
    function getcolor($inputvalue) {

        if ($inputvalue == NULL or '') {
            return '';
        }

        if ($inputvalue > 0 ) {
            return 'exchange-header-up';
        }
        elseif ($inputvalue  == 0) {
            return 'exchange-header-neutral';   
        }
        elseif ($inputvalue  < 0) {
            return 'exchange-header-down';          
        }
    }
}

if (!function_exists('date_range')) {

  function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) 
  {

      $dates = array();
      $current = strtotime($first);
      $last = strtotime($last);

      while( $current <= $last ) {
          //only work days
          if(date("w",$current)!=6 and date("w",$current)!=0) {
            $dates[] = date($output_format, $current);
        }
        $current = strtotime($step, $current);
    }
    return $dates;
}
}


if (!function_exists('isWeekend')) {
  //Weekend function
  function isWeekend($date) {
      return (date('N', strtotime($date)) >= 6);
  } 
} 

if (!function_exists('download_candle_from_finnhub')) {
    function download_candle_from_finnhub($ticker, $starttime, $endtime)
    {

        $starturl = 'https://finnhub.io/api/v1/stock/candle?symbol=';
        $starturl .= strtoupper($ticker);
        $starturl .='&resolution=D&from=';
        $starturl .= $starttime;
        $starturl .= '&to=';
        $starturl .= $endtime;
        $endurl = '&token=bp8pnvnrh5racm7obvp0';
        $fullurl = $starturl . $endurl;

        echo $fullurl . '';

        //will try two times
        $NUM_OF_ATTEMPTS = 1;
        $attempts = 0;
        $data = '';

        sleep(1);

        do {    
            try
            {
                $data = json_decode(download_json($fullurl), true);

            } catch (Exception $e) {
                echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
                $attempts++;
                sleep(1);
                continue;
            }
            break;
        } while($attempts < $NUM_OF_ATTEMPTS);

        //test data
        if (!isset($data['c'][0]))
        {
         return false;
     }

     $timecount = count($data['t']);
     for ($i = 0; $i < $timecount; $i++) 
     {
        $data['t'][$i] = date("Y-m-d", $data['t'][$i]);
    }
    
    return $data;
}
}


if (!function_exists('download_ticker_from_finnhub')) {
    function download_ticker_from_finnhub($ticker)
    {

        $starturl = 'https://finnhub.io/api/v1/quote?symbol=';
        $undurl = '&token=bp8pnvnrh5racm7obvp0';
        $fullurl = $starturl . strtoupper($ticker) . $undurl;

        //will try two times
        $NUM_OF_ATTEMPTS = 1;
        $attempts = 0;
        $data = '';

        //echo $fullurl . '<br>';

        do {    
            try
            {
                $data = json_decode(download_json($fullurl), true);

            } catch (Exception $e) {
                echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
                $attempts++;
                sleep(2);
                continue;
            }
            break;
        } while($attempts < $NUM_OF_ATTEMPTS);

        if (isset($data['t']))
        {
            $data['t'] = date("Y-m-d", $data['t']);
        }

        //test data
        if (!isset($data['c']))
        {
         return false;
     }

     sleep(0.5);
     return $data;
 }
}

if (!function_exists('ticker_download_finnhub_and_yahoo')) {
    function ticker_download_finnhub_and_yahoo($ticker)
    {

        $starturl = 'https://finnhub.io/api/v1/quote?symbol=';
        $undurl = '&token=bp8pnvnrh5racm7obvp0';
        $fullurl = $starturl . strtoupper($ticker) . $undurl;

        //will try two times
        $NUM_OF_ATTEMPTS = 2;
        $attempts = 0;
        $data = '';

        //echo $fullurl . '<br>';

        do {    
            try
            {
                $data = json_decode(download_json($fullurl), true);

            } catch (Exception $e) {
                echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
                $attempts++;
                sleep(2);
                continue;
            }
            break;
        } while($attempts < $NUM_OF_ATTEMPTS);

        if (isset($data['t']))
        {
            $data['timestamp'] = date("Y-m-d H:i:s", $data['t']);
            $data['t'] = date("Y-m-d", $data['t']);
            sleep(2);
        }


        //alternative download!
        if (!isset($data['c']) or $data['c'] == 0)
        {
            echo '<br>Price not set in Finnhub data, using alternative download. <br>';

            $data = download_yahoo_ticker($ticker);

            $temp_array = [];
            $temp_array['c'] = $data['05. price'];
            
            if (isset($data['02. open']))
            {
                $temp_array['o'] = $data['02. open'];
            }
            else
            {
                $temp_array['o'] = '';
            }
            
            
            $temp_array['h'] = $data['high'];
            $temp_array['l'] = $data['low'];
            $temp_array['pc'] = $data['08. previous close'];
            $temp_array['t'] = $data['07. latest trading day'];
            $temp_array['timestamp'] = $data['timestamp'];
            $data = $temp_array;
            sleep(1);
            
        }
        
        return $data;
    }
}



if (!function_exists('download_yahoo_ticker')) {
    function download_yahoo_ticker($ticker)
    {

        $url = 'https://query1.finance.yahoo.com/v7/finance/quote?symbols=' . $ticker;
        
        echo '<br>Using yahoo download: ' . $url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);

        $data = curl_exec ($ch);
        $error = curl_error($ch); 
        if (curl_error($ch)) {
            $error_msg = curl_error($ch);
            echo $error_msg;
        }
        curl_close ($ch);
        
        $data = (array)json_decode($data);
        if (!$data = (array)($data['quoteResponse']))
        {
            echo 'Error downloading yahoo ticker or response has not the right data<br>';
            //return false;
        }
        //echo '<br><br>';
        //var_dump($data);
        //echo '<br><br>';

        if (!isset($data['result']['0']->regularMarketPrice))
        {
            echo ' | FALSE<BR>';
            return false;
        }
        else
        {
            echo '<br>';
        }

        $latestClose = $data['result']['0']->regularMarketPrice;  
        $previousClose = $data['result']['0']->regularMarketPreviousClose;
        $high = $data['result']['0']->regularMarketDayHigh;
        $low =  $data['result']['0']->regularMarketDayLow;
        $open =   $data['result']['0']->regularMarketOpen;
        $timestamp = $data['result']['0']->regularMarketTime + ($data['result']['0']->gmtOffSetMilliseconds/1000);
        
        $timestamp = (new DateTime("@$timestamp"))->format('Y-m-d H:i:s');
        $time = date('H:i:s', strtotime($timestamp));
        $tradingDay= date('Y-m-d', strtotime($timestamp));
        $timestamp = date('Y-m-d H:i:s', strtotime($timestamp));

        $change = $latestClose - $previousClose;
        $percent = (1-($previousClose/$latestClose))*100;
        
        $percent = round($percent ,4);
        $percent = (string)$percent . '%';
        
        $dataNew = 
        array(
            "08. previous close" => $previousClose,
            "07. latest trading day" => $tradingDay,
            "timestamp" => $timestamp,
            "time" => $time,
            "05. price" => round($latestClose,4),
            "10. change percent" => $percent,
            "09. change" => round($change,4),
            "timestamp" => $timestamp,
            "high" => $high,
            "low" => $low,
            "open" => $open
            );
        
        return $dataNew;
        
    }
}
if (!function_exists('errorecho')) {
    function errorecho($msg) {
        echo '<b>';
        echo '<span style="color: red; font-size: 120%;">';
        echo $msg;
        echo '</span></b>';
    }
}

if (!function_exists('successecho')) {
    function successecho($msg) {
        echo '<b>';
        echo '<span style="color: green; font-size: 120%;">';
        echo $msg;
        echo '</span></b>';
    }
}

if (!function_exists('yellowecho')) {
    function yellowecho($msg) {
        echo '<b>';
        echo '<span style="color: yellow; font-size: 120%;">';
        echo $msg;
        echo '</span></b>';
    }
}

if (!function_exists('orangeecho')) {
    function orangeecho($msg) {
        echo '<b>';
        echo '<span style="color: orange; font-size: 120%;">';
        echo $msg;
        echo '</span></b>';
    }
}

if (!function_exists('purpleecho')) {
    function purpleecho($msg) {
        echo '<b>';
        echo '<span style="color: purple; font-size: 120%;">';
        echo $msg;
        echo '</span></b>';
    }
}

if (!function_exists('blueecho')) {
    function blueecho($msg) {
        echo '<b>';
        echo '<span style="color: blue; font-size: 120%;">';
        echo $msg;
        echo '</span></b>';
    }
}

if (!function_exists('ticker_download_eodhistorical')) {
    function ticker_download_eodhistorical($ticker)
    {
        $starturl = 'https://eodhistoricaldata.com/api/real-time/';
        $undurl = '?api_token=5da59038dd4f81.70264028&fmt=json';


            //find out if one or several tickers are requested

        if (is_array($ticker))
        {

            $ticker = array_unique($ticker);
            $ticker = array_values($ticker);

                //https://eodhistoricaldata.com/api/real-time/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json&s=VTI,EUR.FOREX
            $count = count($ticker);

            $maxNumbersDownload = 20;

            $downloadrounds = ceil($count/$maxNumbersDownload);
            echo '<br>';
            echo 'Multi download of ' . $count  . ' tickers. '; 
            echo 'Will download ' . $maxNumbersDownload . ' tickers at the time, in ' . $downloadrounds . ' round(s).<br>';

            $x = 0;

            $urlarray = [];

            while($downloadrounds > $x)     
            {

                $tickernumberstart = $maxNumbersDownload * $x;
                $tickernumberend = ($maxNumbersDownload * $x) + $maxNumbersDownload;


                if ($tickernumberend > $count)
                {
                    $tickernumberend = $count;
                }

                //echo 'Start: ' . $tickernumberstart . '. End: ' . $tickernumberend . ' (minus 1)<br>';

                    //building the url
                for ($z = $tickernumberstart; $z < $tickernumberend; $z++)
                {

                    if (!isset($ticker[$z]))
                    {
                        //echo 'Z is ' . $z . '<br>';
                        continue;
                    }
                    else
                    {
                        //echo 'Z is ' . $z . '<br>';
                    }

                    if ($z == $tickernumberstart)
                    {
                        $url = 'https://eodhistoricaldata.com/api/real-time/' . $ticker[$tickernumberstart] . '?api_token=5da59038dd4f81.70264028&fmt=json&s=';
                    }
                    else if ($z == $tickernumberend - 1)
                    {
                        $url .= $ticker[$z];
                    }
                    else
                    {
                        $url .= $ticker[$z] . ',';
                    }

                }

                $urlarray[] = $url;
                    //echo $url . '<br>';

                $x++;
            }

            $dataholdertemp = [];

            foreach ($urlarray as $key => $url)
            {

                echo $url . '<br>';

                if ($datatickers = (array)json_decode(download($url)))
                {

                  if (isset($datatickers[0]))
                  {

                      foreach ($datatickers as $tickercompany)
                      {

                        $data = (array)$tickercompany;

                        if ($data = ticker_download_eodhistorical_tester($data))
                        {
                            $dataholdertemp[] = $data;   
                        }
                    }
                }
                else
                {

                    $data = $datatickers;

                    if ($data = ticker_download_eodhistorical_tester($data))
                    {
                        $dataholdertemp[] = $data;   
                    }

                }   

            }  
        }

        return $dataholdertemp;

    }

    else 
    {
                //single quoute 
        $url = $starturl . $ticker . $undurl;

        if ($data = (array)json_decode(download($url)))
        {

            $data = ticker_download_eodhistorical_tester($data);
            return $data;

        }
        else
        {
            errorecho('Error downloading from eodhistorical<br>');
            return false;
        }
    }

}
}

if (!function_exists('ticker_download_eodhistorical_tester')) {
    function ticker_download_eodhistorical_tester($datainput)
    {   

        if ('NA' === $datainput['timestamp'] || 'NA' === $datainput['volume'] || 'NA' === $datainput['close'] || 'NA' === $datainput['previousClose'])
        {
            errorecho('Error in ticker_download_eodhistorical_tester, data is missing. Ticker: ' . $datainput['code'] . '<br>');
            //var_dump($datainput);
            
            $ticker = $datainput['code'];

            echo 'Trying alternative download of ticker ' . $ticker . '<br>';

            if ($data = ticker_download_finnhub_and_yahoo($ticker))
            {

              $temp_array['code'] = $ticker;
              $temp_array['timestamp'] = $data['timestamp'];
              $temp_array['open'] = $data['o'];
              $temp_array['high'] = $data['h'];
              $temp_array['low'] = $data['l'];
              $temp_array['close'] = $data['c'];
              $temp_array['volume'] = false;
              $temp_array['previousClose'] = $data['pc'];
              $temp_array['change'] = $data['c'] - $data['pc'];

              if ($data['pc'] != 0)
              {
                $temp_array['change_p'] = ($temp_array['change']/$data['pc'])*100;
              }
              else
              {
                $temp_array['change_p'] = 0;
              }
            
            $temp_array['date'] = date("Y-m-d", strtotime($data['timestamp']));
            $temp_array['time'] = date("H:i:s", strtotime($data['timestamp']));
            
            return($temp_array);
        }
        else
        {
            errorecho('Alternative download failed, returning false<br>');
            return false;
        }
        
    }
    else
    {
            //format to correct timezone
        $datainput['timestamp'] = date("Y-m-d H:i:s", $datainput['timestamp']);
            // create a $dt object with the UTC timezone
        $dt = new DateTime($datainput['timestamp'], new DateTimeZone('UTC'));
            // change the timezone of the object without changing it's time
        $dt->setTimezone(new DateTimeZone('Europe/Oslo'));
            // format the datetime
        $dt->format('Y-m-d H:i:s');
        $datainput['timestamp'] = (string)$dt->format('Y-m-d H:i:s');
        $datainput['date'] = (string)$dt->format('Y-m-d');
        $datainput['time'] = (string)$dt->format('H:i:s');

        return $datainput;
    }
}
}

//funksjon for å rense data
if (!function_exists('test_input')) 
{
    function test_input($data) 
    {
      $data = filter_var($data, FILTER_SANITIZE_STRING);
      $data = trim($data);
      $data = stripslashes($data);
      //$data = htmlspecialchars($data);
      return $data;
  }
}

//legg til alle land her!

if (!function_exists('get_countries')) 
{
    function get_countries() 
    {
        $countries = ['germany', 'united_kingdom', 'france', 'italy', 'spain', 'sweden', 'norway', 'denmark', 'finland'];
        return $countries;
    }
}


if (!function_exists('download_ticker_history')) 
{
    function download_ticker_history($ticker, $fromdate) 
    {
        //https://eodhistoricaldata.com/api/eod/MCD.US?from=2017-01-05&api_token=5da59038dd4f81.70264028&period=d&fmt=json

        $url = 'https://eodhistoricaldata.com/api/eod/' . strtoupper($ticker) . '?from=' .  $fromdate . '&api_token=5da59038dd4f81.70264028&period=d&fmt=json';

        if ($data = json_decode(download($url), true))
        {
            return $data;
        }
        else
        {
            return false;
        }


    }
}

if (!function_exists('saveCSVsemikolon')) {
    function saveCSVsemikolon($input, $file_location_and_name) {
        $fp = fopen($file_location_and_name, 'w');

        foreach ($input as $fields) {
            fputcsv($fp, $fields,';');
        }

        fclose($fp);
        echo 'Saved file ' . $file_location_and_name . '<br>';
    }
}


if (!function_exists('shouldEventsRun')) 
{
    function shouldEventsRun($date) 
    {
     date_default_timezone_set("Europe/Oslo");

       //nyeste posisjon må være fra forrige arbeidsdag eller i dag
     $forrigeArbeidsDag = date( "Y-m-d", strtotime("-1 Weekday"));
     $today = date( "Y-m-d");

     if ($date == $forrigeArbeidsDag)
     {
        return true;
    }
    else if ($date == $today)
    {
        return true;
    }
    else
    {
        return false; 
    }
}
}

if (!function_exists('flush_start')) 
{
    function flush_start() 
    {
     ob_implicit_flush(true);
     ob_start();
 }
}


if (!function_exists('flush_end')) 
{
    function flush_end() 
    {
        ob_end_flush();
        //ob_flush();
    }
}

if (!function_exists('checktime')) 
{ 
    function checktime($start, $end)

    {
        date_default_timezone_set("Europe/Oslo");
        $now = date('H.i');

        if ($now >= $start && $now <= $end) 
        {
            return true;
        }
        else {
            return false;
        }

    }
}


if (!function_exists('timestamp')) 
{ 
    function timestamp()

    {
        date_default_timezone_set("Europe/Oslo");
        echo 'Timestamp: ' . date('H.i.s') . ' <br>';
    }
}

if (!function_exists('eod_bulk_download')) 
{ 
    function eod_bulk_download($ticker)
    {

        $starturl = 'https://eodhistoricaldata.com/api/eod-bulk-last-day/'; 
        $endurl = '?api_token=5da59038dd4f81.70264028&fmt=json';
        $url = $starturl . $ticker . $endurl;

        $data = download($url);

        if ($data = json_decode($data, true))
        {
            return $data;
        }
        else
        {
            return false;
        }

    }
}


if (!function_exists('save')) 
{ 
    function save($data, $location)
    {
        if ($myfile = fopen($location, "w"))
        {
            fwrite($myfile, $data);
            fclose($myfile);
            return true;
        }
        else
        {
            return false;
        }
    }
}

if (!function_exists('formatname')) 
{ 
    function formatname($string)
    {
        $string = str_replace('_', ' ', $string);
        return ucwords($string);
    }
}

if (!function_exists('float_format')) //fixes to decimals after komma
{
    function float_format($array)
    {

        if (!is_array($array))
        {
            errorecho('Not array<br>');
            var_dump($array);
            return $array;
        }

        foreach ($array as $key => $cell)
        {

            if(is_array($cell))
            {
                $array[$key] = float_format($cell);
                continue;
            }      

            if (is_float($cell))
            {
                //echo $key . ': ' . $cell .'<br>';

                $temp = round($cell,4);
                $temp = number_format($temp,4,".","");
            }
            else
            {
                continue;
            }

            $array[$key] = $temp;

        }

        //var_dump($array);

        return $array;   
    }

}




if (!function_exists('listfiles')) //fixes to decimals after komma
{
    function listfiles($path)
    {
        $files = array_diff(scandir($path), array('.', '..'));
        $files = array_values($files);
        return $files;

    }
}

if (!function_exists('is_isin')) 
{
    function is_isin($string)
    {
      if (!is_string($string))
      {
        return false;
    }

      //DE0006048432
    $string = trim($string);

    $length = strlen($string);

    if ($length != 12)
    {
        //echo '"' . $string . '". Length! <br>'; //$string
        return false;
    }

    if (!is_numeric(substr($string,2)))
    {
        //echo 'Numeric!<br>';
        return false;
    }

    return true;

}
}

if (!function_exists('get_base_currency')) 
{
    function get_base_currency($land)
    {
        if ($land == 'norway')
        {
            return 'NOK';
        }
        else if ($land == 'sweden')
        {
            return 'SEK';
        }
        else if ($land == 'denmark')
        {
            return 'DKK';
        }
        else if ($land == 'germany')
        {
            return 'EUR';
        }
        else if ($land == 'france')
        {
            return 'EUR';
        }
        else if ($land == 'united_kingdom')
        {
            return 'GBP';
        }   
        else if ($land == 'spain')
        {
            return 'EUR';
        }   
        else if ($land == 'finland')
        {
            return 'EUR';
        }
        else if ($land == 'italy')
        {
            return 'EUR';
        }   
        else
        {
            return false;
        }
    }
}

if (!function_exists('get_currency_multiply_factor')) 
{
    function get_currency_multiply_factor($fromcurrency, $tocurrency)
    {

        $fromcurrency = strtoupper($fromcurrency);
        $tocurrency = strtoupper($tocurrency);
        
        $jsonfolder = '../production_europe/json/currency/';
        $multiply_factor = 1;
        
        if ($fromcurrency == 'GBX' and $tocurrency == 'GBP' or $fromcurrency == 'GBP' and $tocurrency == 'GBX')
        {
            return 1/100;
        }
        
        //if same currency, return 1
        if ($fromcurrency == $tocurrency)
        {
            return 1;
        }
        
        $gbxfactor = 1;

        if ($fromcurrency == 'GBX')
        {
         $fromcurrency = 'GBP';
         $gbxfactor = 0.01;
     }



        //read in currencies
        //https://eodhistoricaldata.com/api/real-time/EUR.FOREX?api_token=5da59038dd4f81.70264028&fmt=json
     $filename = $fromcurrency . $tocurrency .  '.FOREX';
     $url_start = 'https://eodhistoricaldata.com/api/real-time/';
     $url_end = '?api_token=5da59038dd4f81.70264028&fmt=json';
     $url = $url_start . $filename . $url_end;

     if (!file_exists($jsonfolder . $filename . '.json'))
     {
            //download pair     
        echo ' File does ' . $filename . '.json not exist, downloading ' . $url . '<br>';
        
        if ($data = json_decode(download($url),true))
        {
            saveJSON($data, $jsonfolder . $filename. '.json');
        }
        else
        {
            errorecho(' Could not create ' . $url . '<br>');
            return false;
        }
        
    }
    else
    {
            //exists, check time
        $filetime_local = filemtime($jsonfolder . $filename . '.json');
        $now = time();     
        
        $difference = ($now - $filetime_local);
        
        
        if ($difference > 3600)
        {
            echo ' More than an hour old in function, will update. ';
            
            if ($data = json_decode(download($url),true))
            {
                saveJSON($data, $jsonfolder . $filename . '.json');             
            }
            else
            {
                errorecho(' Could not download, not able to update ' . $url . '. Will use old. <br>');
            }
        }
        else
        {
                //reading 
            if (!$data = readJSON($jsonfolder . $filename . '.json'))
            {
                errorecho(' Read error of json file ' . $filename . '<br>');
                return false; 
            }   
        }
    }
    
    if ($data['close'] == 'NA')
    {
        return false; 
    }


    $result = (float)$data['close'];

    if ($result > 0)
    {

        //echo 'Multiply factor ' . $result . '<br>';
        return $result * $gbxfactor;            
    }
    else
    {
        errorecho('Multiply factor is zero or false in functions!<br>');
        return false;
    }
    
}
}

if (!function_exists('getFirstTwoLetters')) 
{
    function getFirstTwoLetters ($string)
    {
        $string = mb_strtolower($string);
        $string = preg_replace('~[^a-z]+~', '', $string);   
        return substr($string,0,2);
    }
}

if (!function_exists('makedirectory')) 
{
    function makedirectory ($path)
    {

        if (file_exists($path)) 
        {
            if (!is_dir($path)) ////if file is already present, but it's not a dir
            { 
                unlink($path); 
                if (mkdir($path))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
            }
        } 
        else 
        { 
            if (mkdir($path)) //creates folder
            {
                return true;
            }
            else
            {
                return false;
            }
        }   
    }
}

if (!function_exists('binary_search_isinliste')) //name!
{
    function binary_search_isinliste($needle, $haystack) {
     {
      $min = 0;
      $max = count($haystack)-1;

      while ($max >= $min)
      {
        $mid = (int) (($min + $max) / 2);
            //echo 'Mid er ' . $mid . '. ';
        if (mb_strtolower($haystack[$mid][2]) == mb_strtolower($needle))
        {
            return $mid;
        }
        else if (mb_strtolower($haystack[$mid][2]) < mb_strtolower($needle)) 
        {
            $min = $mid + 1;
        }
        else 
        {
            $max = $mid - 1;
        }
    }
        // $needle was not found
    return false;
}
}
}

if (!function_exists('binary_search_isinliste_isin')) //isin kode
{
    function binary_search_isinliste_isin($needle, $haystack) 
    {
      $min = 0;
      $max = count($haystack)-1;
      while ($max >= $min)
      {
        $mid = (int) (($min + $max) / 2);
        //echo 'Mid er ' . $mid . '. ';
        if ($haystack[$mid][0] == $needle) return $mid;
        else if ($haystack[$mid][0] < $needle) $min = $mid + 1;
        else $max = $mid - 1;
    }
      // $needle was not found
    return false;
}
}

if (!function_exists('print_mem')) 
{
    function print_mem()
    {
        /* Currently used memory */
        $mem_usage = memory_get_usage();

        /* Peak memory usage */
        $mem_peak = memory_get_peak_usage();
        echo '<br>';
        echo 'The script is now using: <strong>' . round($mem_usage / 1024) . 'KB</strong> of memory.<br>';
        echo 'Peak usage: <strong>' . round($mem_peak / 1024) . 'KB</strong> of memory.<br><br>';
    }
}

if (!function_exists('number_format_heltall')) 
{
    function number_format_heltall($tall)
    {

        if (is_numeric($tall))
        {
            $tall = number_format($tall,0,".",",");
            return $tall;
        }

    }
}


if (!function_exists('is_isin_new')) 
{
    function is_isin_new($string)
    {
        if (!is_string($string))
        {
            return false;
        }

    //DE0006048432
        $string = trim($string);

        $length = strlen($string);

        if ($length != 12)
        {
        //echo 'Length er ' . $length . ' (false)<br>';
            return false;
        }

        for ($i = 0; $i < 12; $i++)
        {
            if ($i < 2)
            {
                continue;
            }

            if (!is_numeric($string[$i]))
            {
            //echo 'Is not numberic ' . $string[$i]. '<br>';
                return false;
            }

        }

        return true;
    }
}

if (!function_exists('binary_search_multiple_hits')) 
{
    function binary_search_multiple_hits ($isin, $name, $isin_sorted_list, $name_sorted_list, $land)
    {

        $name = mb_strtolower($name);
        $name = trim($name);
        $isin = trim($isin);

        //find index with first isin hit
        $hitcontainer = [];
        if ($firstrow = binary_search_isinliste_isin($isin, $isin_sorted_list))
        {

            //echo 'Isin part<br>';
            //echo 'First row is ' . $firstrow . '<br>'; 

            $first_hit_row = $isin_sorted_list[$firstrow]; 
            $hitcontainer[] = $first_hit_row;
            //check if there is more than one isinhit

            //downwards
            $downwards = true;

            $row = $firstrow; 

            while ($downwards )
            {
                $row = $row + 1;

                if (isset($isin_sorted_list[$row]) and $isin == $isin_sorted_list[$row][0])
                {
                    //echo 'Adding extra isin downwards.<br>';
                    $hitcontainer[] = $isin_sorted_list[$row];
                }
                else
                {
                    $downwards = false;
                }

            }

            $upwards = true;

            $row = $firstrow; 

            while($upwards)
            {
                $row = $row - 1;

                if (isset($isin_sorted_list[$row]) and $isin == $isin_sorted_list[$row][0])
                {
                    //echo 'Adding extra isin upwards.<br>';
                    $hitcontainer[] = $isin_sorted_list[$row];
                }
                else
                {
                    $upwards = false;
                }

            }

        }

        //var_dump($name_sorted_list);

        if ($firstrow = binary_search_isinliste($name, $name_sorted_list))
        {

            //echo 'name part<br>';
            //echo 'First row is ' . $firstrow . '<br>'; 

            $first_hit_row = $name_sorted_list[$firstrow]; 
            $hitcontainer[] = $first_hit_row;
            //check if there is more than one isinhit

            //downwards
            $downwards = true;

            $row = $firstrow; 

            while($downwards)
            {
                $row = $row + 1;

                if (isset($name_sorted_list[$row]) and $name == mb_strtolower($name_sorted_list[$row][2]))
                {
                    //echo 'Adding extra name downwards.<br>';
                    $hitcontainer[] = $name_sorted_list[$row];
                }
                else
                {
                    $downwards = false;
                }

            }

            $upwards = true;

            $row = $firstrow; 

            while($upwards)
            {
                $row = $row - 1;

                if (isset($name_sorted_list[$row]) and $name == mb_strtolower($name_sorted_list[$row][2]))
                {
                    //echo 'Adding extra name upwards.<br>';
                    $hitcontainer[] = $name_sorted_list[$row];
                }
                else
                {
                    $upwards = false;
                }

            }

        }
        else
        {
            //echo 'No hits in name<br>';
        }

        if (empty($hitcontainer)) //no hits
        {
            //looking for hits in the last 10 lines to get recently added files

            $isin_count = count($isin_sorted_list);
            $name_count = count($name_sorted_list);

            for ($i = $isin_count - 10; $i < $isin_count; $i++)
            {

                if (isset($isin_sorted_list[$i][0]) and $isin == $isin_sorted_list[$i][0])
                {
                    $hitcontainer[] = $isin_sorted_list[$i];
                }

            }

            for ($i = $name_count - 10; $i < $name_count; $i++)
            {

                if (isset($name_sorted_list[$i][2]) and $name == mb_strtolower($name_sorted_list[$i][2]))
                {
                    $hitcontainer[] = $name_sorted_list[$i];
                }

            }            

        }

        if (!empty($hitcontainer)) 
        {

            //remove duplicate entries
            foreach ($hitcontainer as $key => $row)
            {

                foreach ($hitcontainer as $index=> $subrow)
                {

                    if ($key == $index)
                    {
                        continue;
                    }

                    if ($row == $subrow)
                    {
                        //echo 'Unsetting...<br>';
                        unset($hitcontainer[$key]);
                        break;
                    }

                }

            }

            $hitcontainer = array_values($hitcontainer);

            //go trough and find country       

            $found_country_success = 0;
            $hitrow = '';

            foreach ($hitcontainer as $index => $row)
            {
             
                if (mb_strtolower($row[6]) == $land and isset($row[9])) 
                {
                    
                    $hitrow = $row;
                    $found_country_success = 1;
                    break;

                }

            }

                  
            if ($found_country_success == 0)
            {
                //if not hits, return the first
                return $hitcontainer[0];
            }
            else
            {
                return $hitrow;
            }

        }
        else
        {
            return false; 
        }

    }
}

if (!function_exists('remove_duplicates_in_array')) 
{
    function remove_duplicates_in_array($array) //must be normal indexed, not $data['hey'];
    {

        $keys = array_keys($array);
        $count = count($array);

        for ($i = 0; $i < $count; $i++)
        {
            if (!isset($array[$keys[$i]]))
            {
                continue; //already removed
            }

            $target = $array[$keys[$i]];
            $foundcounter = 0; 

            for ($x = 0; $x < $count; $x++)
            {

                if (isset($array[$keys[$x]]) and $target == $array[$keys[$x]])
                {

                    if ($foundcounter == 0) //first, itself, skip
                    {
                        $foundcounter++;
                        continue;
                    }
                    else
                    {
                        unset($array[$keys[$x]]);
                    }

                }

            }

        }

        return array_values($array);

    }
}

if (!function_exists('get_file_age_in_hours')) 
{
    function get_file_age_in_hours($filename)
    {

        if (file_exists($filename))
        {
            $timestamp = filemtime($filename);
            return (strtotime("now")-$timestamp)/3600;
        }
        else
        {
            return false; 
        }

    }
}
?>