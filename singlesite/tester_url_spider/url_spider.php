<?php
//Goutte
$time_start = time(); 

require 'vendor/autoload.php';
include_once 'functions.php';

$time_limit = 540;

set_time_limit($time_limit);

use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

$client = new Client();

//$start_url = 'http://shortmars2020/shortnordic-com.luksus.no/';
$start_url = 'http://shortsinglesite/shorteurope-com.luksus.no/';

$crawler = $client->request('GET', $start_url );

// Click on the "Security Advisories" link
//$link = $crawler->selectLink('all active positions')->link();
//$crawler = $client->click($link);


$first = $crawler->filter('a')->each(function ($node) {
	$href  = $node->attr('href');
    //$title = $node->attr('title');
    //$text  = $node->text();
	return compact('href');
});

$allholder = [];
$error = [];

echo 'Initial url count: ' . count($first) . '<br>';

foreach ($first as $key => $url)
{

	$time_end = time();
	$diff = $time_end - $time_start;

	if ($time_end - $time_start > $time_limit - 5)
	{
		errorecho('Out of time at ' . $diff . ' seconds , breaking and saving.<br>');
		break;
	}

	if ($url['href'] == 'https://twitter.com/shortnordic')
	{
		continue;
	}

	$crawler = $client->request('GET', $start_url . $url['href']);

	$data = $crawler->filter('a')->each(function ($node) {
		$href  = $node->attr('href');
	    //$title = $node->attr('title');
	    //$text  = $node->text();
		return compact('href');
	});

	$allholder[] = $data;

	$text = (string)$crawler->text();

	if ($result = stristr($text, ' error:')) 
	{
		echo $key . '. ' . $start_url .  $url['href'];
		echo " | Error found<br>";
		$error[$key]['url'] = $url['href'];
		$error[$key]['type'] = 'error';
		$error[$key]['result'] = $result;
		$error[$key]['text'] = $text;
		continue;

	} 

	if ($result = stristr($text, 'fatal')) 
	{
		echo $key . '. ' . $start_url .  $url['href'];
		echo " | Error found<br>";
		$error[$key]['url'] = $url['href'];
		$error[$key]['type'] = 'fatal';
		$error[$key]['result'] = $result;
		$error[$key]['text'] = $text;
		continue;
	} 

	if ($result = stristr($text, ' on line ')) 
	{
		echo $key . '. ' . $start_url .  $url['href'];
		echo " | Error found<br>";
		$error[$key]['url'] = $url['href'];
		$error[$key]['type'] = ' on line ';
		$error[$key]['result'] = $result;
		$error[$key]['text'] = $text;
		continue;
	} 	

	if ($result = stristr($text, 'unexpected ')) 
	{
		echo $key . '. ' . $start_url .  $url['href'];
		echo " | Error found<br>";
		$error[$key]['url'] = $url['href'];
		$error[$key]['type'] = 'unexpected ';
		$error[$key]['result'] = $result;
		$error[$key]['text'] = $text;
		continue;
	} 

	$time_end = time();
	$diff = $time_end - $time_start;

	if ($time_end - $time_start > $time_limit - 5)
	{
		errorecho('Out of time at ' . $diff . ' seconds, breaking and saving.<br>');
		break;
	}

}

saveJSON($error, 'errors_found.json');

echo 'Antall feil: ' . count($error) . '<br>';
var_dump($error);

$allholder_new = flatten($allholder);
$allholder_new = array_unique($allholder_new);

echo 'Antall url: ' . count($allholder) . '<br>';
var_dump($allholder_new);



function flatten(array $array) {
	$return = array();
	array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
	return $return;
}

$time_end = time();
$diff = $time_end - $time_start;
echo 'Runtime: ' . $diff . ' seconds.<br>';


//$crawler->filter('a')->each(function ($node) {
    //print $node->text()."\n";
//});

// Get the latest post in this category and display the titles
//$crawler->filter('html')->each(function ($node) {
    //print $node->text()."\n";
//});

//file_put_contents('test.html', $crawler);

//$crawler->filter('#most-popular > div.panel.open > ol > li.first-child.ol1 > a')->each(function ($node) {
//	var_dump($node->attr('href'));
//});

?>