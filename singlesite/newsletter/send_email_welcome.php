<?php

//Load Composer's autoloader
require '../../newsletter/vendor/autoload.php';

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
//require '../vendor/autoload.php';

//make sure source is either shortnordic or shorteurope
//set sender info based on source
if (isset($source) and $source != false)
{

    if (stripos($source, 'shortnordic.com') === false and stripos($source, 'shorteurope.com') === false) //must be one of two valid sources
    {
        $source = 'shorteurope.com';  
    }

}
else
{
    $source = 'shorteurope.com';  
}

//Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try 
{
    //Server settings
    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'mail.' . $source;                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'no-reply@' . $source;                     //SMTP username
    $mail->Password   = 'Reginais21';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('no-reply@' . $source, ucwords($source));
    $mail->addAddress($email);     //Add a recipient
    //$mail->addAddress('ellen@example.com');               //Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = 'Newsletter signup success ' . $source;
    $mail->Body    = 'Hi! <br><br><b>Thank you for signing up to our newsletters: <br></b>';

    foreach ($obj['subscriptions'] as $subscription)
    {
        $mail->Body .= ucwords($subscription) . '<br>';
    }

    $mail->Body .='<br><br><a href="https://shorteurope.com/newsletter/settings.php?code='. $hash_email . '&secret=' . $obj['secret'] .  '"><b>Use this link to edit your settings.</b></a>';
    $mail->Body .= '<br><br>Please contact us if you experience any errors. ';
    $mail->Body .= '<br><br>Sincerely<br>The ShortNordic/ShortEurope team';

    //alt
    $mail->AltBody = 'Hi! Newsletter signup success ';
    foreach ($obj['subscriptions'] as $subscription)
    {
        $mail->AltBody .= $subscription . "\n";
    }
    
    $mail->AltBody .= 'Use this link to edit your settings: ' . 'https://shorteurope.com/newsletter/settings.php?code=' . $hash_email . '&secret=' . $obj['secret'];

    $mail->send();
    echo 'Email message has been sent';
} 
catch (Exception $e) 
{
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}


