<?php

require '../production_europe/functions.php';
require '../production_europe/logger.php';

//error_reporting(0);
//ini_set('display_errors', 0);

if (!isset($mode))
{
	$mode = 'shortnordic';
} 

date_default_timezone_set('Europe/Oslo');
include '../newsletter/newsletter_header.html';

if ($mode == 'shortnordic')
{
	$countries_local = ['sweden', 'norway', 'denmark', 'finland'];
}
else if ($mode == 'shorteurope')
{
	$countries_local = get_countries();
}
else if ($mode == 'single')
{
	if (!isset($countries))
	{
		echo 'Error, error, warning, no countries!';
		return false;
	}
	
	$countries_local = $countries;
	$mode = 'shorteurope';
}
else
{
	return;
}



$misslist = [];
//check which countries to sort out

if (empty($countries_local))
{
	echo 'Error, error, warning';
	return false;
}

foreach ($countries_local as $key => $land)
{

	if (!file_exists('../production_europe/json/events/company/' . $land . '.events.company.current.json') or !$data = readJSON('../production_europe/json/events/company/' . $land . '.events.company.current.json'))
	{
		$misslist[] = $land;
		unset($countries_local[$key]);
		continue;
	}

	//check that positions are from today or yesterday
	//last check of dates. Should be from today or yesterday, if not new!

	$today = date('Y-m-d');
	$yesterday = date('Y-m-d', strtotime("-1 weekday", strtotime($today)));

	$data = array_values($data);

	if (!isset($data) or empty($data))
	{
		$misslist[] = $land;
		unset($countries_local[$key]);
		continue;
	}
}

?>
<div class="col-12">
<h3 style="font-family: Helvetica, arial, sans-serif;">Newest changes short positions <?php echo date('l', strtotime("now"));?></h3>
<!--<p>Pulished <?php //echo mb_strtolower(date('l', strtotime("now"))) . ' ' .  date('Y-m-d H:i:s'); ?>.</p>-->
</div>
<?php

$round = 0; 

foreach ($countries_local as $key => $land)
{
	include '../newsletter/newsletter_summary.php';
	include '../newsletter/newsletter_company.php';
	$round++; 
}

if (!empty($misslist))
{
	foreach ($misslist as $key => $land)
	{
		?>
		<p  style="margin-top:10px; font-family: Helvetica, arial, sans-serif;">No changes to positions for <?php echo ucwords($land); ?>.</p>
		<?php
	}
}
?>
<p  style="text-align: center; line-height: 1.5; font-family: Helvetica, arial, sans-serif; margin-top: 3px; margin-bottom: 3px;">Content generated <?php echo date('Y-m-d') . ' at ' . date('H:i'); ?>. <br>*Positions are normally published by the authorities the next business day after the position date. If position date is older than the previous business day, then no new updates have been published. </p>

<?php

include '../newsletter/newsletter_footer.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

?>