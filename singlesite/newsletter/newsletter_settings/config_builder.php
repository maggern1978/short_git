<?php

include '../../production_europe/functions.php';

$array = [];


$obj = new stdClass;
$obj->name = 'ShortNordic';
$obj->description = 'Daily position changes (Sweden, Denmark, Norway, Finland)';
$obj->type = 'single';
$array[] = (array)$obj;

$obj->name = 'ShortEurope';
$obj->description = 'Daily position changes (Nordic countries + Germany, France, UK, Italy and Spain)';
$obj->type = 'single';
$array[] = (array)$obj;

$countries = get_countries();

foreach ($countries as $country)
{
	$obj = new stdClass;
	$obj->name = $country;
	$obj->description = 'Daily short position changes.' ;
	$obj->type = 'multi';

	$array[] = (array)$obj;
}

saveJSON($array, 'newsletter_index.json');

var_dump($array);



?>