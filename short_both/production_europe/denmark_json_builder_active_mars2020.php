<?php
date_default_timezone_set('Europe/Oslo');


//obs! this files needs include('../production_europe/simplehtmldom_1_7/simple_html_dom.php'); to run alone!
echo '<br>Starting...<br>';
include('../production_europe/simplehtmldom_1_7/simple_html_dom.php');
include('../production_europe/functions.php');

//sorteringsfunksjon
if (!function_exists('comp')) {
    function comp($a, $b)
    {
        if ($a->ShortingDate == $b->ShortingDate) {
            return 0;
        }
        return ($a->ShortingDate > $b->ShortingDate) ? -1 : 1;
    }
}

if (!function_exists('datetotime')) {
    function datetotime ($date, $format = 'DD.MM.YYYY') {
        if ($format == 'YYYY-MM-DD') list($year, $month, $day) = explode('-', $date);
        if ($format == 'YYYY/MM/DD') list($year, $month, $day) = explode('/', $date);
        if ($format == 'YYYY.MM.DD') list($year, $month, $day) = explode('.', $date);

        if ($format == 'DD-MM-YYYY') list($day, $month, $year) = explode('-', $date);
        if ($format == 'DD/MM/YYYY') list($day, $month, $year) = explode('/', $date);
        if ($format == 'DD.MM.YYYY') list($day, $month, $year) = explode('.', $date);

        if ($format == 'MM-DD-YYYY') list($month, $day, $year) = explode('-', $date);
        if ($format == 'MM/DD/YYYY') list($month, $day, $year) = explode('/', $date);
        if ($format == 'MM.DD.YYYY') list($month, $day, $year) = explode('.', $date);

        return mktime(0, 0, 0, $month, $day, $year);
    }
}

if (!function_exists('checknamefornumbers')) {
     function checknamefornumbers ($name) {

        if(1 === preg_match('~[0-9]~', $name)){
            #has numbers
            echo '<br>Name has numbers';
        }
        else {
            return false;
        }
     }
}


include('../production_europe/logger.php');

set_time_limit(5000);

if (!function_exists('readCSV_tickerliste')) {
 //obs, dataene må ha semikolon som skilletegn
    function readCSV_tickerliste($file_location_and_name) {

        $csvFile = file($file_location_and_name);

        //Les in dataene 
        $name = [];
        foreach ($csvFile as $line) {
            $name[] = str_getcsv($line, ',');
        }

        //ordne utf koding
        $counter = 0;
        foreach ($name as &$entries) {
            $data[$counter] = array_map("utf8_encode", $entries);
            $counter++;
        }
        return $name;
    }
}

if (!function_exists('readCSVsemikolon')) {
 //obs, dataene må ha semikolon som skilletegn
    function readCSVsemikolon($file_location_and_name) {

        $csvFile = file($file_location_and_name);

        //Les in dataene 
        $name = [];
        foreach ($csvFile as $line) {
            $name[] = str_getcsv($line, ';');
        }

        //ordne utf koding
        $counter = 0;
        foreach ($name as &$entries) {
            $data[$counter] = array_map("utf8_encode", $entries);
            $counter++;
        }
        return $name;
    }
}

//Lagre
if (!function_exists('saveCSV')) {
    function saveCSV($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');
          fputcsv($fp, $input);
    fclose($fp);
    }
}

$path = '../short_data/dataraw/denmark/download_mars2020/';
$files = array_diff(scandir($path), array('.', '..'));

//sort by time downloaded
usort($files, function($a, $b) {
    return filemtime('../short_data/dataraw/denmark/download_mars2020/' . $a) > filemtime('../short_data/dataraw/denmark/download_mars2020/' . $b);
});

$positionsBox = [];
foreach ($files as $key => $file) 
{
    $arr = [];
    echo 'File is ' . $path . $file . '<br>';
    $html = file_get_html($path . $file);

    foreach($html->find('table tr td') as $e){
        //echo $e->plaintext . '<br>';
        $arr[] = trim($e->plaintext);
    }


    //var_dump($arr);

    $count = count($arr);
    //echo 'Count er :' . $count . '<br>';

    for ($i = 3; $i < $arr; $i = $i + 7) {

        if (!isset($arr[$i])) {
            break;
        }

        if (strpos($arr[$i+2],"CANCELLED")) {
            //echo 'CANCELLED';
            continue;
        }
        else if (strpos($arr[$i+2],"ANNULLERET")) {
            //echo 'CANCELLED';
            continue;
        }
       
        $position=new stdClass();
        $position->ShortingDate = $arr[$i+0];
        $position->description = $arr[$i+2];
        $position->Name = $arr[$i+3];
        $positionsBox[] = $position;
        //var_dump($position);
        //echo '<br><br>';
    }

}


$count = count($positionsBox);
$activeCounter = 0;


for ($i = 0; $i < $count; $i++) {
    echo '<br>';
    //snu datoen og legg til to sifre til år
    $dateArray = explode('.', $positionsBox[$i]->ShortingDate);
    $newdate = '20'. $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
    
    //trekk fra en arbeidsdag
    $newdate = date('Y-m-d', strtotime($newdate . '-1 Weekday'));
    
    
    $positionsBox[$i]->ShortingDate = $newdate;

     if (strpos($positionsBox[$i]->description,"%")) {
        $positionsBox[$i]->type = 'percent';
    }
    else if (strpos($positionsBox[$i]->description,"reduced") or 
        strpos($positionsBox[$i]->description,"nedbragt")) {
        $positionsBox[$i]->type = 'shares';
    }
    else {
        $positionsBox[$i]->type = 'unknown';
    }
    
    $positionsBox[$i]->Name = strstr($positionsBox[$i]->Name," A/S",true);

    if ($positionsBox[$i]->Name == false) {
        //alternative name finding 
        echo '<br> Name not found by finding A/S, doing alternative method: '  . $positionsBox[$i]->description . '<br>';

        $namex = strstr($positionsBox[$i]->description,'issued by '); 
        $namex = str_replace('issued by ', '', $namex);
        echo 'New name found is ' . $namex . '<br>';
        $positionsBox[$i]->Name = $namex;

    }

    $positionsBox[$i]->isActive = 'yes';
    $activeCounter++;

    echo $i . '. Name selskap er ' . $positionsBox[$i]->Name . '<br>';

    checknamefornumbers($positionsBox[$i]->Name);

    if ($positionsBox[$i]->type == 'shares') {
        echo '<br>Doing shares<br>';

        $positionsBox[$i]->PositionHolder = strstr($positionsBox[$i]->description," has", true);

        if ($positionsBox[$i]->PositionHolder == false) {
            $positionsBox[$i]->PositionHolder =  strstr($positionsBox[$i]->description," har nedbragt", true);
        }
         echo  'Positionholder er ' . $positionsBox[$i]->PositionHolder . '<br>';

         checknamefornumbers($positionsBox[$i]->PositionHolder);
         echo 'Dette er description: ' . $positionsBox[$i]->description . '<br>';

        $descriptionString = $positionsBox[$i]->description;

        $descriptionString = str_replace($positionsBox[$i]->Name, "", $descriptionString);
        $descriptionString = str_replace($positionsBox[$i]->PositionHolder, "", $descriptionString);

        $positionsBox[$i]->NetShortPosition = preg_replace('/[^0-9]+/', '', $descriptionString ); 
        $positionsBox[$i]->ShortPercent = ''; 
    }
    else if ($positionsBox[$i]->type == 'percent') { 
         echo '<br>Doing percent<br>';
         
        $positionsBox[$i]->PositionHolder =  strstr($positionsBox[$i]->description," holds", true);

        if ($positionsBox[$i]->PositionHolder == false) {
            $positionsBox[$i]->PositionHolder =  strstr($positionsBox[$i]->description," har en", true);
        }
        
        echo  'Positionholder er ' . $positionsBox[$i]->PositionHolder . '<br>';

        checknamefornumbers($positionsBox[$i]->PositionHolder);

        echo 'Dette er description: ' . $positionsBox[$i]->description . '<br>';

        //ta bort navn på selskap og player før man tar preg_replace

        $descriptionString = $positionsBox[$i]->description;

        $descriptionString = str_replace($positionsBox[$i]->Name, "", $descriptionString);
        $descriptionString = str_replace($positionsBox[$i]->PositionHolder, "", $descriptionString);

        echo 'descriptionstring er nå ' . $descriptionString . '<br>';

        $positionsBox[$i]->ShortPercent = preg_replace('/[^0-9]+/', '', $descriptionString); 
        $positionsBox[$i]->ShortPercent = $positionsBox[$i]->ShortPercent/100;
        $positionsBox[$i]->NetShortPosition = ''; 
    }
    else {
        $positionsBox[$i]->NetShortPosition = 'error'; 
        $positionsBox[$i]->ShortPercent = 'error'; 
    }

}

//Group positions on companies

echo 'Number of active positions: ' . $activeCounter . '<br>';
$startCount = $activeCounter;

$allCompanyNames = []; 

$count = count($positionsBox);


if  ($count == 0) {
    logger('Number of companies in denmark is zero', ' in denmark_json_builder_active.php. Returning!');
    return;
}


for ($i = 0; $i < $count; $i++) {
        
    //fiks selskaper som ikke har blittmed
    if ($positionsBox[$i]->Name =='') {
        //echo '2!';
        if (strpos($positionsBox[$i]->description,"Østasiatiske")) {
            $positionsBox[$i]->Name = 'Santa Fe Group';
        }
        else if (strpos($positionsBox[$i]->description,"Landbobank")) {
            $positionsBox[$i]->Name = 'Ringkjøbing Landbobank, Aktieselskab';   
        }
    }
    $allCompanyNames[] = $positionsBox[$i]->Name;
   
}

$allCompanyNames = array_unique($allCompanyNames);
//var_dump($allCompanyNames);

$selskapsBox = [];

foreach ($allCompanyNames as $name) {
    $company = new stdClass();
    $company->ISIN = '';
    $company->Name = $name;
    $company->ShortPercent = '';
    $company->ShortedSum = '';
    $company->LastChange = '';
    $company->ticker = '';
    
    
    //samle posisjonene med samme navn
    $holder = [];
        for ($i = 0; $i < $count; $i++) {
            if (isset($positionsBox[$i]->Name) and $positionsBox[$i]->Name == $name) {
                $holder[] = $positionsBox[$i];
                unset($positionsBox[$i]);
            }
        }
     $company->NumPositions = count($holder);   
    $company->Positions = $holder;
    $selskapsBox[] = $company;
}

//var_dump($selskapsBox);

$finalcount = 0;

foreach ($selskapsBox as $selskap) {
    foreach ($selskap->Positions as $position) {
        $finalcount++;
    }

}

echo '<br>Progresjon: Antall posisjoner ut er ' . $finalcount . '<br>';
echo 'Søker etter isin-data og antall aksjer data';
$name_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );


$selskapsBox_count = count($selskapsBox);
$missBox = [];

//første runde for å finne de som mangler
//var_dump($selskapsBox);

echo 'Trying to find missing data<br>';

for ($i = 0; $i < $selskapsBox_count; $i++) {

    echo '<br>';

    //ta navnet og søk i isinlisten
    $nameTarget = $selskapsBox[$i]->Name;

    echo $i . '. Trying to find ' . $nameTarget . ' ';
    //fiks navnene manuelt
    if ($nameTarget == 'Solar' ) {
        $selskapsBox[$i]->Name = 'Solar B'; // annet navn
        $nameTarget = 'Solar B';
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
    }

    if ($nameTarget == 'TDC' ) {
        unset ($selskapsBox[$i]); // konkurs
        echo 'Skipping<br>';
        continue;
    }
    else if ($nameTarget == 'NKT Holding' ) {
        $selskapsBox[$i]->Name = 'NKT'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0010287663'; //sett isin
        continue;
    }
    else if ($nameTarget == 'Novozymes' ) {
        $selskapsBox[$i]->Name = 'Novozymes B'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0060336014'; //sett isin
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
        continue;
    }
    else if ($nameTarget == 'H. Lundbeck' ) {
        $selskapsBox[$i]->Name = 'Lundbeck'; // annet navn
        $nameTarget = 'Lundbeck';
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
    }
    else if ($nameTarget == 'William Demant Holding' ) {
        $selskapsBox[$i]->Name = 'Demant'; // annet navn
        $nameTarget = 'Demant';
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
    }
    else if ($nameTarget == 'H. Lundbeck' ) {
        $selskapsBox[$i]->Name = 'Lundbeck'; // annet navn
        $nameTarget = 'Lundbeck';
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
    }
    else if ($nameTarget == 'Harboes Bryggeri' ) { //posisjon annulert, hopp over
        echo 'Skipping<br>';
        continue;
    }
    else if ($nameTarget == 'Dampskibsselskabet Norden' ) {
        $selskapsBox[$i]->Name = 'D/S Norden'; // annet navn
        $nameTarget = 'D/S Norden';
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
    }
    else if ($nameTarget == 'Coloplast' ) {
        $selskapsBox[$i]->Name = 'Coloplast B'; // annet navn
        $nameTarget = 'Coloplast B';
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
    }
    else if ($nameTarget == 'OW Bunker' ) {
        echo 'Skipping<br>';
        continue;
    }
    else if ($nameTarget == 'Carlsberg' ) {
        $selskapsBox[$i]->Name = 'Carlsberg B'; // annet navn
        $nameTarget = 'Carlsberg B';
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
    }
    else if ($nameTarget == 'Rockwool International' ) {
        $selskapsBox[$i]->Name = 'Rockwool Int. B'; // annet navn
        $nameTarget = 'Rockwool Int. B';
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
    }
    else if ($nameTarget == 'Auriga Industries' ) {
        echo 'Skipping<br>';
        continue; // ikke notert
    }
    else if ($nameTarget == 'ALK-Abello' ) {
        $selskapsBox[$i]->Name = 'ALK-Abelló B'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0060027142'; //sett isin 
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
        continue;
    }
    else if ($nameTarget == 'Nets' ) {
        echo 'Skipping<br>';
        continue; // ikke notert  
    }
    else if ($nameTarget == 'Ringkjøbing Landbobank, Aktieselskab' ) {
        $selskapsBox[$i]->Name = 'Ringkjøbing Landbobank'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0060854669'; //sett isin 
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
        continue;
    }
    else if ($nameTarget == 'A.P. Møller - Mærsk') {
        $selskapsBox[$i]->Name = 'A.P. Møller - Mærsk B'; // annet navn
        $selskapsBox[$i]->ISIN = 'DK0010244508'; //sett isin 
        echo 'Fant ' . $selskapsBox[$i]->Name . '<br>';
        continue;
    }
    else if ($nameTarget == 'ISS') {
        $nameTarget = 'ISS';
    }

    $nameTarget = str_replace(' A/S', '', $nameTarget);
    $nameTarget = trim($nameTarget);

    $successX = 0;

    if ($row = binary_search_multiple_hits('', $nameTarget, $isin_list, $name_list, 'denmark'))
    {
         $selskapsBox[$i]->ISIN = $row[0];
         $selskapsBox[$i]->numberOfStocks =  $row[9];
         //$selskapsBox[$i]->ticker =  $row[1] . '.' . $row[8]; 
         $successX = 1;
         successecho (' Success!<br>');
    }
    else
    {
        errorecho($nameTarget . ' not found<br>');
        //try with adding 

    }

    if ($successX == 0) 
    {
        errorecho('Fant IKKE: "' . $selskapsBox[$i]->Name . '" OBS: Dette må trolig rettes manuelt!<br>');
        saveJSON($selskapsBox[$i], '../production_europe/isin_adder/misslist_active/name@' . $land . '@' . $selskapsBox[$i]->Name .  '.json');
        $missBox[] = $selskapsBox[$i]->Name;
    }

}

$finalcount = 0;

foreach ($selskapsBox as $selskap) 
{
    foreach ($selskap->Positions as $position) 
    {
        $finalcount++;
    }
}

echo '<br>Progresjon A1: Antall posisjoner ut er ' . $finalcount . '<br><br>';

$count = count($selskapsBox);

for ($i = 0; $i < $count; $i++) {

    if (isset($selskapsBox[$i]->Positions)) 
    {
        $positionsCount = count($selskapsBox[$i]->Positions);
        $target = $selskapsBox[$i]->Name;

        for ($x = 0; $x < $positionsCount ; $x++) 
        {
            
            $selskapsBox[$i]->Positions[$x]->Name =  $target;
            
            //hvis ikke isin er satt
            if ($selskapsBox[$i]->ISIN == '') 
            {
                
                //fjern feilmelding av isin som er sjekket opp at er ok
                if ($target == 'Harboes Bryggeri') 
                {
                	
                }
                else {
                    echo 'Missing isin! ' . $selskapsBox[$i]->Name;
                    var_dump($selskapsBox[$i]->Positions);
                }

                unset ($selskapsBox[$i]);
                break;
            }
        }
    }
    else
    {
        echo 'Not set in ' . $i . '<br>';
        var_dump($selskapsBox[$i]);
    }
}

$finalcount = 0;

foreach ($selskapsBox as $selskap) 
{
    foreach ($selskap->Positions as $position) 
    {
        $finalcount++;
    }
}

echo '<br>Progresjon: A2: Antall posisjoner ut er ' . $finalcount . '<br>';

$selskapsBox = array_values($selskapsBox);
$count = count($selskapsBox);

for ($i = 0; $i < $count; $i++) 
{
     
    //find stock target
    $isinTarget = $selskapsBox[$i]->ISIN;
    $nameTarget = strtolower($selskapsBox[$i]->Name);
    
    $success = 0;

    if (isset($selskapsBox[$i]->numberOfStocks))
    {
        $numberOfStocks = $selskapsBox[$i]->numberOfStocks;
        $success = 1;
    }
    else
    {
        errorecho('number of stocks not found for ' . $nameTarget . '<br>');
        continue;
    }

    echo $nameTarget  . '. Number of stocks is ' . $numberOfStocks . '<br>';
    
    if (isset($selskapsBox[$i]->Positions)) {
        $selskapsBox[$i]->NumPositions = count($selskapsBox[$i]->Positions);
        $positionsCount = $selskapsBox[$i]->NumPositions;

        $ShortedSum = 0;
        $ShortPercent = 0;
        $NetShortPosition = 0;

        for ($x = 0; $x < $positionsCount ; $x++) 
        {

            //trim spaces and other stuff

            $selskapsBox[$i]->Positions[$x]->PositionHolder = trim($selskapsBox[$i]->Positions[$x]->PositionHolder);

            if ($selskapsBox[$i]->Positions[$x]->ShortPercent == '') {
                //Antall aksjer er satt, sett inn procent

                if ($numberOfStocks == 0) {
                    echo 'Number of stock is zero! <br>';
                    var_dump($selskapsBox[$i]->Positions[$x]);
                    continue;
                }

                $percent = (((float)$selskapsBox[$i]->Positions[$x]->NetShortPosition)/(int)$numberOfStocks)*100;
                
                $percent = number_format($percent,4,".","");
                $selskapsBox[$i]->Positions[$x]->ShortPercent = (float)$percent;
            }
            else if ($selskapsBox[$i]->Positions[$x]->NetShortPosition == '') {
                //echo 'hit!';

                //Antall aksjer er satt, sett inn procent
                $NetShortPosition = (($selskapsBox[$i]->Positions[$x]->ShortPercent)*$numberOfStocks);
                $NetShortPosition = round($NetShortPosition,4);
                $selskapsBox[$i]->Positions[$x]->NetShortPosition = $NetShortPosition/100;
                //var_dump($NetShortPosition);
            }
        }
    }
    else
    {
        errorecho('Not set! <br>');
    }
}


$companyCount = count($selskapsBox);

//sort positions from newest to oldest
for ($i = 0; $i < $companyCount; $i++) {
    $netShortTemp = 0;
    (float)$ShortPercentTemp = 0;
    $positionCount = count($selskapsBox[$i]->Positions);
    
    //sorter posisjonene fra nyest til eldst
    usort($selskapsBox[$i]->Positions, "comp");

    for ($z = 0; $z < $positionCount; $z++) {
        //Rund av beregnignen av antall aksjer shortet i hver posisjon
        $selskapsBox[$i]->Positions[$z]->NetShortPosition = 
        (int)number_format((float)$selskapsBox[$i]->Positions[$z]->NetShortPosition,0,"","");
        
        //tildel ISIN til hver posisjon
        $selskapsBox[$i]->Positions[$z]->ISIN = $selskapsBox[$i]->ISIN;

        //finn selskapene med aktive posisjoner og legg til temp verdi
        if ($selskapsBox[$i]->Positions[$z]->isActive == 'yes') {
            $netShortTemp += (float)$selskapsBox[$i]->Positions[$z]->NetShortPosition;
            $ShortPercentTemp += (float)$selskapsBox[$i]->Positions[$z]->ShortPercent;
        }
    //remove description of position and type
        unset($selskapsBox[$i]->Positions[$z]->description);
        unset($selskapsBox[$i]->Positions[$z]->type);
    }
    //tildel selskapet aggregerte verdier
    $selskapsBox[$i]->ShortedSum = $netShortTemp;
    $selskapsBox[$i]->ShortPercent = $ShortPercentTemp;
    $selskapsBox[$i]->LastChange = $selskapsBox[$i]->Positions[0]->ShortingDate; 
}


//read date from generated file
$date = date('Y-m-d');

//ta bort selskap hvis ingen posisjoner i det hele tatt
for ($i = 0; $i < $companyCount; $i++) {
    $positionCount = count($selskapsBox[$i]->Positions);
    if ($positionCount == 0) {
        echo 'Fant ingen posisjoner, unsetting ' . $selskapsBox[$i]->Name;
        unset ($selskapsBox[$i]);
        continue;
    }
    else {
        $selskapsBox[$i]->NumPositions = $positionCount;
    }

    $holderBox = [];

    foreach ($selskapsBox[$i]->Positions as $position) {
        $holderBox[] = $position;
    }
    $selskapsBox[$i]->ticker = '';
    $selskapsBox[$i]->Positions = $holderBox;

}
//reindeks
$selskapsBox = array_values($selskapsBox);

$finalcount = 0;

foreach ($selskapsBox as $selskap) {
    foreach ($selskap->Positions as $position) {
        $finalcount++;
    }
}

echo '<br>Antall posisjoner ut er nå: ' . $finalcount ;
echo '<br>Antall posisjoner fra start var: ' . $startCount ;

if ($finalcount != $startCount) {
    logger('Det er mismatch mellom antall aktive posisjoner inn og antall posisjoner ut! ', ' i denmark_json_builder_active.php');
}


//legg til tickere

foreach ($selskapsBox as $key => $selskap)
{

    if ($row = binary_search_multiple_hits($selskap->ISIN, $selskap->Name, $isin_list, $name_list, 'denmark'))
    {
         $selskapsBox[$key]->ticker =  $row[1] . '.' . $row[8]; 
         $selskapsBox[$key]->ticker = mb_strtolower($selskapsBox[$key]->ticker);
         $selskapsBox[$key]->ticker= str_replace(' ', '-', $selskapsBox[$key]->ticker);
         $count = count($selskapsBox[$key]->Positions);

         for ($i = 0; $i < $count; $i++)
         {
            $selskapsBox[$key]->Positions[$i]->ticker = $selskapsBox[$key]->ticker;
         }
    }
    else
    {
        errorecho($nameTarget . ' not found<br>');
    }
}


//write current
$fp = fopen('../short_data/dataraw/denmark/denmark_current.json', 'w');
fwrite($fp, json_encode($selskapsBox, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
fclose($fp);

//delete files if successfull!




?>