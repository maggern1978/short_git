<?php

include_once('../production_europe/logger.php');
include_once('../production_europe/functions.php');
print_mem();

$date = date('Y-m-d');
set_time_limit(5000);

//$land = 'sweden';

$csvlocation = '../short_data/dataraw/' . $land . '/' . $land . '_current.csv';
$fileCurrent = '../short_data/dataraw/' . $land . '/' . $land . '_current.json';
//les inn datane
$csvdata = readCSVkomma($csvlocation);

echo 'Positions in: ' . count($csvdata) . '<br>';

require '../production_europe/logger.php';

if (!class_exists('selskap')) 
{

	class selskap 
	{
	  //Creating properties
		public $ISIN;
		public $Name;
		public $ShortPercent;
		public $ShortedSum;
		public $LastChange;
		public $NumPositions;
		public $currency;
		public $ticker;
		public $Positions;

	    //assigning values
		public function __construct($isin, $name, $shortpercent, $shortedsum, $lastchange, $numpositions, $positions, $currency, $ticker) 
		{
			$this->ISIN = $isin;
			$this->Name = $name;
			$this->ShortPercent = $shortpercent;
			$this->ShortedSum = $shortedsum;
			$this->LastChange = $lastchange;
			$this->NumPositions = $numpositions;
			$this->currency = $currency;
			$this->ticker = $ticker;
			$this->Positions = $positions;


		}
	}
}

if (!class_exists('posisjon')) 
{
	class posisjon 
	{
		public $ShortingDate;
		public $PositionHolder;
		public $ShortPercent;
		public $NetShortPosition;
		public $ISIN;
		public $Name;
		public $isActive;
		public $currency;

	    //assigning values
		public function __construct($ShortingDate, $PositionHolder, $ShortPercent, $NetShortPosition, 
			$ISIN, $Name, $isActive, $currency, $ticker) 
		{

			$this->ShortingDate = $ShortingDate;
			$this->PositionHolder = $PositionHolder;
			$this->ShortPercent = $ShortPercent;
			$this->NetShortPosition = $NetShortPosition;
			$this->ISIN = $ISIN;
			$this->Name = $Name;
			$this->isActive = $isActive;
			$this->currency = $currency;

			if ($ticker != false)
			{
				$this->ticker = mb_strtolower(str_replace(" ", '-', $ticker));
			}
			else
			{
				$this->ticker = false;
			}

			
		}
	}
}

if (!function_exists('get_unique_indexed')) 
{

	function get_unique_indexed($data)
	{

		$data = array_unique($data);
		$data = array_values($data);
		return $data; 
	}
}

//gå igjennom slett der oppføringene ikke stemmer

if (!function_exists('sanetize_csv_data')) 
{
	function sanetize_csv_data($csvdata, $land)
	{

		$csvdata_count = count($csvdata);

		for ($i = 0; $i < $csvdata_count; $i++)
		{

			$removed = 0;

			for ($x = 0; $x < 5; $x++)
			{
			//removing komma in company and player names

				if (!isset($csvdata[$i][$x]))
				{
					errorecho('Data not set at ' . $i . ' and ' . $x . ' Unsettting<br> <br> ');
					unset($csvdata[$i]);
					$removed = 1;
					break;
				}

				$csvdata[$i][$x] = trim($csvdata[$i][$x]);
			$csvdata[$i][$x]  = trim($csvdata[$i][$x] , " \t\n\r\0\x0B\xc2\xa0"); //whitespaces

		}

		if ($removed == 0)
		{
			$csvdata[$i][0] = str_replace(",", "", $csvdata[$i][0]);
			$csvdata[$i][1] = str_replace(",", "", $csvdata[$i][1]);
		}
	}

	//finding duplicate values
	$csvdata = array_values($csvdata);


	if ($land == 'germany')
	{
		
		//clean the data
		$count = count($csvdata);
		for ($i = 1; $i < $count; $i++) {

			//fixing the komma in number
			$csvdata[$i][3] = str_replace(",", ".", $csvdata[$i][3]);
		}
		unset($csvdata[0]);
		$csvdata = array_values($csvdata);
	}

	return $csvdata;
}
}

$csvdata = sanetize_csv_data($csvdata, $land);
echo 'Count er nå ' . count($csvdata) . '<br>';

if (!function_exists('find_duplicates_csv_data')) 
{
	function find_duplicates_csv_data($csvdata)
	{

		$count = count($csvdata);
		
		$playernamebox = [];


		for ($i = 0; $i < $count; $i++)
		{
			if (isset($csvdata[$i][0]))
			{
				$playernamebox[] = mb_strtolower($csvdata[$i][0]);
			}
		}

		$playernamebox = array_unique($playernamebox);
		$playernamebox = array_values($playernamebox);

		$playernameboxCount = count($playernamebox);

		for ($i = 0; $i < $playernameboxCount; $i++)
		{
			//ta et playernavn
			$targetname = $playernamebox[$i];
			$companynamesBoxlocal = [];
			$positionsboxlocal = [];
			$companyIsinBoxLocal = [];

			$count = count($csvdata);

			//samle inn alle selskapsnavnene hvor player har shortposisjoner
			for ($x = 0; $x < $count; $x++)
			{

				if (isset($csvdata[$x][0]) and $targetname == mb_strtolower($csvdata[$x][0]))
				{
					$companynamesBoxlocal[] = mb_strtolower($csvdata[$x][1]);
					$companyIsinBoxLocal[] = mb_strtolower($csvdata[$x][2]);
					$positionsboxlocal[] = $csvdata[$x];
				}

			}

			//sjekk for duplicate selskapsoppføringer
			if (count(array_unique($companynamesBoxlocal)) < count($companynamesBoxlocal) or count(array_unique($companyIsinBoxLocal)) < count($companyIsinBoxLocal))
			{

				echo '<strong>' . $i . '. ' . $targetname  . ' </strong>';
				echo 'Duplicate values! Going through positions to remove oldest of each: <br>';

		//go through all values
				$positionsboxlocalCount = count($positionsboxlocal);

				$removepositionsBox = [];

		//by company name
				for ($z = 0; $z < $positionsboxlocalCount; $z++)
				{

					$companynametarget = $positionsboxlocal[$z][1];
					$hits = 0;

					foreach ($positionsboxlocal as $key => $row)
					{

						if ($companynametarget == $row[1])
						{

							if ($hits == 0)
							{
								$firstrow = $row;
								$hits++;
							}
							else
							{
						//echo 'Second position !++<br>';
								if ($firstrow[4] > $row[4])
								{
							//var_dump($row);
									$removepositionsBox[] = $row;

								}

							}

						}

					}

				}

		//by company isin
				for ($z = 0; $z < $positionsboxlocalCount; $z++)
				{

					$companyisintarget = $positionsboxlocal[$z][2];
					$hits = 0;

					foreach ($positionsboxlocal as $key => $row)
					{

						if ($companyisintarget == $row[2])
						{

							if ($hits == 0)
							{
						//echo 'First!';
						//var_dump($row);
						//echo '----------------------------<br>';
								$firstrow = $row;
								$hits++;
							}
							else
							{
						//echo 'Second position isin!++<br>';
								if ($firstrow[4] > $row[4])
								{
							//var_dump($row);
									$removepositionsBox[] = $row;

								}


							}

						}

					}

				}

				$removepositionsBox = array_unique($removepositionsBox,SORT_REGULAR);
		//var_dump($removepositionsBox);

				$removepositionsBoxCount = count($removepositionsBox);
				$removepositionsBox = array_values($removepositionsBox);

				for ($p = 0; $p < $count; $p++)
				{

					for ($o = 0; $o < $removepositionsBoxCount; $o++)
					{

						if ($removepositionsBox[$o] == $csvdata[$p])	
						{

							echo 'Unsetting position ' . $p . ' for being a duplicate for player ' . $csvdata[$p][0] . ' in ' . $csvdata[$p][1] . ' ' . $csvdata[$p][2] . ' ' . $csvdata[$p][3] . ' ' . $csvdata[$p][4] . '<br>';
							unset($csvdata[$p]);
							break;

						}
					}
				}

			}
		} 
		return $csvdata = array_values($csvdata);
	}
}

$csvdata = find_duplicates_csv_data($csvdata);

echo 'Count etter find_duplicates_csv_data er ' . count($csvdata) . '<br>';

//check if players have two positions in any company, should only be one!

//lag alle posisjonene som objekt
$posisjoner = array();
$errorholder = array();


//find newest date
$csvdata_count = count($csvdata);
$last_updated = '2001-01-01';

for ($i = 0; $i < $csvdata_count; $i++)
{
	if ($last_updated < $csvdata[$i][4])
	{
		$last_updated = $csvdata[$i][4];
	}

}
echo 'Newest position in data: ' . $last_updated . '<br>';


//les inn hovedlisten for å match isin mot ticker
//les inn tickerliste 
$name_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

$used_list_isin = array();
$used_list_companies = array();
$miss_box = [];
$miss_box_outstanding = [];

//deploy country spider for misses
include_once '../production_europe/functions_spider.php';

$spider = [];

foreach ($csvdata as $key => $position) 
{

		//hopp over de som er mer enn fire år gamle!
	$fourYearsAgo = date('Y-m-d', strtotime("-4 year"));

	if ($position[4] < $fourYearsAgo)
	{
		echo $key . '. Deleting old position: ' . $position[0] . ', ' . $position[1] . ', ' . $position[2] . ', ' . $position[3] . ', ' . $position[4] . '<br>';
		continue;
	}

	//finner isin, alternativt navn, ticker i isinlisten
	$targetname = mb_strtolower($position[1]);
	$targetisin = $position[2];

	$found_in_isin_list = 0;

	//function binary_search_multiple_hits ($isin, $name, $isin_sorted_list, $name_sorted_list, $land)

	//$firstrow = binary_search_isinliste($name, $name_liste);
	if ($row = binary_search_multiple_hits($targetisin, $targetname, $isin_list, $name_list, $land))
	{
		
		$currency = $row[3];
		$tickerselskap = $row[1] . '.' . $row[8];
		$tickerselskap = str_replace(" ", '-', $tickerselskap);

		if (isset($row[9]))
		{
			$number_of_stocks = (float)$row[9] * ((float)$position[3]/100);
			$number_of_stocks = round($number_of_stocks,0);

		}
		else
		{
			$number_of_stocks = false;
		}
	}
	else
	{
	   	if (empty($spider))
	   	{
	   		echo $key . '. Deploying spider to search for missing company. <br>';
	   		$spider = deploy_spider($land);
	   	}

	   	$spider_found = 0;

	   	//not found, try in spider
	   	if ($spider_isin = search_spider($spider, $targetisin, $targetname))
	   	{
	   		echo $key . '. Missing company ' . $targetname . ' ' .  $targetisin .  '. ';
	   		echo 'Found isin in spider! Doing new search with isin ' . $spider_isin . ' ';

			if ($row = binary_search_multiple_hits($spider_isin, $targetname, $isin_list, $name_list, $land))
			{
				successecho('Found!<br>');

				$currency = $row[3];
				$tickerselskap = $row[1] . '.' . $row[8];
				$tickerselskap = str_replace(" ", '-', $tickerselskap);

				$spider_found = 1;

				if (isset($row[9]))
				{
					$number_of_stocks = (float)$row[9] * ((float)$position[3]/100);
					$number_of_stocks = round($number_of_stocks,0);

				}
				else
				{
					$number_of_stocks = false;
				}
			}
			else
			{
				echo 'No hits while searching with spider hit. This should not happen!<br>';
			}   		
	   		
	   	}
	   	else
	   	{
	   		echo 'No hit in spider...<br>';
	   	}

	   	if ($spider_found == 0)
	   	{
	   		$currency = false;
			$tickerselskap = false;
			$number_of_stocks = false;
	   	}


	}		

	//bytt slash-strek med bindestrek i datoene

	$date_temp = $position[4];
	$dateNew = date('Y-m-d', strtotime($date_temp));

	$isActive = 'yes';
	$posisjoner[] = new posisjon($dateNew , $position[0], $position[3], $number_of_stocks, $position[2], $position[1], $isActive, $currency, $tickerselskap);

	$used_list_isin[] =  $position[3];
	$used_list_companies[] = mb_strtolower($position[1]);
}

echo '<br>';

$used_list_isin = get_unique_indexed($used_list_isin);
$used_list_companies = get_unique_indexed($used_list_companies);

//logg ikke funnet
$count = count($posisjoner);


echo '<b>Logging not found isin or outstanding stocks: </b><br>';
$misscount = 0; 

for ($i = 0; $i < $count; $i++)
{
	$position = (array)$posisjoner[$i];

	if (in_array(false, $position))
	{
		echo $i . '. ' . $position['Name'] . ', ' . $position['PositionHolder'] . ', ' . $position['ISIN']  . ',  ' . $position['ShortPercent'] . ',  ' . $position['ShortingDate'] . ' | ';	


		if ($position['NetShortPosition'] == false)
		{
			$filename = filter_var('../production_europe/isin_adder/misslist_active/outstanding_csv_to_json_builder@' . $land . '@' . $position['Name'] .  '.json', FILTER_SANITIZE_STRING);
			saveJSON($position, $filename);
			$misscount++; 
		}
		else
		{
			$filename = filter_var('../production_europe/isin_adder/misslist_active/isin_csv_to_json_builder@' . $land . '@' . $position['Name'] .  '.json', FILTER_SANITIZE_STRING);
			saveJSON($position, $filename);
			$misscount++;
		}

	}

}

echo 'Number of misses = ' . $misscount . '<br>';


//filter du dupletter med ulike case på navnene!

$used_list_isin = array_unique($used_list_isin);
$used_list_companies = array_unique($used_list_companies);


foreach ($used_list_companies as $key => $navn)
{
	$hitcounter = 0;
	foreach ($used_list_companies as $index => $subname)
	{
		if (mb_strtolower($navn) == mb_strtolower($subname))
		{
			if ($hitcounter == 0)
			{
				$hitcounter++;
				continue;
			}
			echo 'Unsetting duplicate name: ' . $used_list_companies[$index] . '<br>';
			unset($used_list_companies[$index]);
		}
	}
}

$used_list_companies = array_values($used_list_companies);

$selskaper = array();
$posisjonsholder_unik = $used_list_companies;
$poskopi = $posisjoner;

$maincounter = 0;

//ikke match mot navn, match heller mot isin

//behandle hver unike navn
foreach ($posisjonsholder_unik as $key => $posisjon) 
{
	//build company 

	//Ta selskapnavnet
	$selskapnavnet = $posisjon;

	//Finn isin
	$posisjonsholder = [];

	//ta player-navnet
	$isin ='';
	$counter = 0;
	$last_updated;
	
	//var_dump($posisjon);

	foreach ($posisjoner as $index => $pos)  
	{

		//var_dump($pos);
		if (mb_strtolower($pos->Name) == mb_strtolower($selskapnavnet)) 
		{
			$isin = $pos->ISIN;
			$currency = $pos->currency;
			$posisjonsholder[] = $pos;	
			$counter++;
		}

	}

	$maincounter += $counter;

	$antall_posisjoner = count($posisjonsholder);

	//Nå har vi alle aktive posisjoner, og må regne ut summen av alle prosentene og av alle aksjene.
	$accumulated_short_percent = 0;
	$accumulated_short_stocks = 0;

	$ticker = '';

	foreach ($posisjonsholder as $index => $posisjon2) 
	{

		if ($index == 0)
		{
			$last_updated = $posisjon2->ShortingDate;
		}
		else
		{
			if ($last_updated < $posisjon2->ShortingDate)
			{
				//set new latest updated date
				//echo ' | Setting new newest position to ' . $posisjon->ShortingDate . '<br>';
				$last_updated = $posisjon2->ShortingDate;
			}
		}

		$accumulated_short_percent += $posisjon2->ShortPercent;
		$accumulated_short_stocks += $posisjon2->NetShortPosition;
		$ticker = $posisjon2->ticker;

	}

	$accumulated_short_percent = round($accumulated_short_percent,2);
	$accumulated_short_percent = number_format($accumulated_short_percent,2,".",",");

	//Skal selskap og posisjoner, men bare hvis det er shorting i selskapet, og bare aktive posisjoner innad i selskapet
	$selskaper[] = new selskap($isin, $selskapnavnet, $accumulated_short_percent, $accumulated_short_stocks, $last_updated, $antall_posisjoner, $posisjonsholder, $currency, $ticker);

}

//remove positions with zero positions
$count = count($selskaper);
$totalcount = 0; 

for ($i = 0; $i < $count; $i++)
{
	$positionscount = count($selskaper[$i]->Positions);
	$totalcount += $positionscount;

	if (!$positionscount > 0)
	{
		echo $i . '. Deleting position for zero positions <br>';
		unset($selskaper[$i]);
	}
}

$selskaper = array_values($selskaper);
echo '<br>-------stats:-------<br>';
echo 'Companies out: ' . count($selskaper) . '<br>';
echo 'Positions out: ' . $totalcount . '<br>';

//write current
saveJSON($selskaper, $fileCurrent);
print_mem();

?>