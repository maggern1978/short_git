<?php

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

//$land = 'norway';
//$land = 'italy';

echo '<br>';

$limit_date_previous = '2020-01-01';

$date = date('Y-m-d');

while (!file_exists('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
		//echo $date . '<br>';
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

	if ($date < $limit_date_previous)
	{
		echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
		return;
	}
}	

//check if file is older than 3 hours, if yes, then skip. 
echo 'Checking timestamp: ';
if (get_file_age_in_hours('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json') > 2.99) 
{
   echo 'More than three hours old, will skip....<br>';
   return;
} 
else 
{
  echo 'Less than three hours old, will continue....<br>';
}


echo 'Reading new positions: '. '../short_data/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json</strong><br>';

//read todays positions
if (!$currentdata = readJSON('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
	errorecho('Error reading json ' . '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
	return;
}

$currentfilename = '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json';

//find the newest position date
$newestdate = '2001-01-01';

foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $key => $position)
	{
		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}
}


//echo 'Previous working day of that is ' . $previousday . '<br>';
if ($newestdate == date('Y-m-d', (strtotime ('-1 weekday', strtotime ($date)))))
{
	echo 'Dates match. File has positions with timestamp from previous weekday (' . $newestdate .'). <br>';
}
else
{
	echo 'Newest date in current positions is ' . $newestdate . '.<br>';
	echo('Dates DOES NOT match. Filename does not have positions with timestamp from previous weekday. Probably no new positions?<br>');
}

echo '<br><strong>Finding file with newest position that is older than newestedate found for current.json:</strong> <br>';

$previous_newest_date = $newestdate;

$date = date('Y-m-d', strtotime("+1 weekday", strtotime($newestdate)));

while ($previous_newest_date <= $newestdate)
{

	//finding the previous file
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	
	while (!file_exists('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		
		$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

		if ($date < $limit_date_previous)
		{
			echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
			return;
		}
	}

	//cannot ble same filename as current
	if ($currentfilename == '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json')
	{
		continue;
	}

	echo 'File is ' . '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'. '<br>';

	//read previous positions
	if (!$previousdata = readJSON('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		errorecho('Error reading previous json ' . '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
		continue;
	}


	//finding newest date
	//find the newest position date
	
	$previous_newest_date = '2001-01-01';

	foreach ($previousdata as $key => $company)
	{
		foreach ($company['Positions'] as $key => $position)
		{
			if ($position['ShortingDate'] > $previous_newest_date)
			{
				$previous_newest_date = $position['ShortingDate'];
			}
		}
	}

	if ($previous_newest_date <= $newestdate)
	{
		successecho('Newest date in current positions is ' . $previous_newest_date . '.<br>');
		break;
	}

}


echo 'Reading previous positions: ' . '../short_data/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json </strong>because that file has positions from the day before.<br> ';



$previousdate = $date;


if (!$data = readJSON('../short_data/json/events/player/' . $land . '.events.player.current.json'))
{
	echo 'Error reading json, returning.';
	return;
}

$changescount = 0;
$companyNameBox = [];
$playerNameBox = [];
$newestDate = '2000-01-01';

foreach ($data as $key => $change)
{
	
	$companyNameBox[] = $change['Name'];
	$playerNameBox[] = $change['PositionHolder'];;
	$changescount++;

	if ($newestDate < $change['ShortingDate'])
	{
		$newestDate = $change['ShortingDate'];
	}

}

$publishedate = date( "Y-m-d", strtotime("+1 Weekday", strtotime($newestDate)));

if ($publishedate > date('Y-m-d')) //never newer than today
{
	$publishedate = date('Y-m-d');
}

$companyNameBox = array_unique($companyNameBox);
$playerNameBox = array_unique($playerNameBox);

$numberOfPlayers = count($playerNameBox);
$numberOfCompanies = count($companyNameBox);

//var_dump($companyNameBox);

$numberOfChanges = count($data);

ob_start();

?>

<div class="col-12 m2-3">
	<h3>New positions</h3>

	<p>
	<span class="mt-2 font-weight-bold">Newest update was published  
		<?php 
		echo strtolower(date( "l", strtotime(($publishedate)))) . ' ' . date( "Y-m-d", strtotime(($publishedate))) . '. Comparing to positions published ' . strtolower(date( "l", strtotime($previousdate))) . ' ' . date( "Y-m-d", strtotime($previousdate)) . '.'; 
		?> 
	</span>
	<?php
	echo 'Any new updates today? ';
	if ($newestDate >= date( "Y-m-d", strtotime("-1 Weekday")))
	{
		echo '<span class="font-weight-bold exchange-header-up">YES</span>';
	}
	else
	{
		echo '<span class="font-weight-bold exchange-header-down">NO</span>';
	}		

	?>
	<br>
		<?php echo $numberOfChanges; ?> changes in <?php echo $numberOfCompanies; ?> companies by <?php echo $numberOfPlayers; ?> players.
	</p>
</div>

<?php

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 

$filename = '../short_data/html/events/header/' . $land . '.events.header.current.html';

// Write final string to file
file_put_contents($filename, $htmlStr);

?>