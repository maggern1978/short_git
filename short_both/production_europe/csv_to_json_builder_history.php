<?php
error_reporting(E_ALL);

include_once('../production_europe/logger.php');
include_once('../production_europe/functions.php');
print_mem();

$date = date('Y-m-d');
set_time_limit(5000);

//$land = 'sweden';

if ($land != 'france') //France has no active positions
{
	$csvlocation = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_merged.csv'; //source for the positions
}
else
{
	$csvlocation = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.csv'; //source for the positions. 
}

$fileCurrent = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.json'; //save
$filewithdate = '../short_data/dataraw/' . $land . '.history/' . $date . '.' . $land . '.json';

//les inn datane
$csvdata = readCSVkomma($csvlocation);

echo 'Positions in: ' . count($csvdata) . '<br>';
require '../production_europe/logger.php';

if (!class_exists('selskap')) 
{

	class selskap 
	{
	  //Creating properties
		public $ISIN;
		public $Name;
		public $ShortPercent;
		public $ShortedSum;
		public $LastChange;
		public $NumPositions;
		public $currency;
		public $ticker;
		public $Positions;

	    //assigning values
		public function __construct($isin, $name, $shortpercent, $shortedsum, $lastchange, $numpositions, $positions, $currency, $ticker) 
		{
			$this->ISIN = $isin;
			$this->Name = $name;
			$this->ShortPercent = $shortpercent;
			$this->ShortedSum = $shortedsum;
			$this->LastChange = $lastchange;
			$this->NumPositions = $numpositions;
			$this->currency = $currency;
			$this->ticker = $ticker;
			$this->Positions = $positions;
		}
	}
}

if (!class_exists('posisjon')) 
{
	class posisjon 
	{
		public $ShortingDate;
		public $PositionHolder;
		public $ShortPercent;
		public $NetShortPosition;
		public $ISIN;
		public $Name;
		public $isActive;
		public $currency;

	    //assigning values
		public function __construct($ShortingDate, $PositionHolder, $ShortPercent, $NetShortPosition, 
			$ISIN, $Name, $isActive, $currency, $ticker) 
		{

			$this->ShortingDate = $ShortingDate;
			$this->PositionHolder = $PositionHolder;
			$this->ShortPercent = $ShortPercent;
			$this->NetShortPosition = $NetShortPosition;
			$this->ISIN = $ISIN;
			$this->Name = $Name;
			$this->isActive = $isActive;
			$this->currency = $currency;

			if ($ticker != false)
			{
				$this->ticker = mb_strtolower(str_replace(" ", '-', $ticker));
			}
			else
			{
				$this->ticker = false;
			}
		}
	}
}

if (!function_exists('get_unique_indexed')) 
{

	function get_unique_indexed($data)
	{

		$data = array_unique($data);
		$data = array_values($data);
		return $data; 
	}
}

if (!function_exists('filter_name')) 
{

	function filter_name($name)
	{
		$array = [' ab', ' AB', ' (publ)', ' AB (publ)', ' ab (publ)'];

		foreach ($array as $key => $input)
		{
			$length = strlen($input);

			$cut = substr($name, -$length);

			if (strripos($input, $cut) !== false)
			{
				$cutpos = strripos($name, $input);

				//echo $key . '. ' . $name . ' | ' . $input . ' funnet. Cut er ' . $cut . '. Cutpos er ' . $cutpos . '. ';

				$name = substr($name, 0, $cutpos);
				
				
			}

		}
		
		//echo 'Navn ut er: ' . $name . '<br><br>';

		return $name;
	}
}


//gå igjennom slett der oppføringene ikke stemmer

if (!function_exists('sanetize_csv_data')) 
{
	function sanetize_csv_data($csvdata, $land)
	{

		$unsetcount = 0;

		$csvdata_count = count($csvdata);

		for ($i = 0; $i < $csvdata_count; $i++)
		{

			$removed = 0;

			for ($x = 0; $x < 5; $x++)
			{
			//removing komma in company and player names

				if (!isset($csvdata[$i][$x]))
				{
					//errorecho($i . '. Data not set at ' . $x . ' Unsettting... <br> ');
					unset($csvdata[$i]);
					$removed = 1;
					$unsetcount++;
					break;
				}

				$csvdata[$i][$x] = trim($csvdata[$i][$x]);
				$csvdata[$i][$x]  = trim($csvdata[$i][$x] , " \t\n\r\0\x0B\xc2\xa0"); //whitespaces

			}

			if ($removed == 0)
			{
				$csvdata[$i][0] = str_replace(",", "", $csvdata[$i][0]);
				$csvdata[$i][1] = str_replace(",", "", $csvdata[$i][1]);
			}
		}

		echo 'Unset ' . $unsetcount . ' positions for data not set.<br>';

	//finding duplicate values
		$csvdata = array_values($csvdata);


		if ($land == 'germany')
		{

		//clean the data
			$count = count($csvdata);
			for ($i = 1; $i < $count; $i++) {

			//fixing the komma in number
				$csvdata[$i][3] = str_replace(",", ".", $csvdata[$i][3]);
			}
			unset($csvdata[0]);
			$csvdata = array_values($csvdata);
		}

		return $csvdata;
	}
}


if (!function_exists('find_duplicates_csv_data')) 
{
	function find_duplicates_csv_data($data)
	{
		
		$count_data = count($data);
		$playernamebox = [];
		//echo 'Positions in: ' . $count_data . '<br>';
		//var_dump($data);

		for ($i = 0; $i < $count_data; $i++)
		{
			if (isset($data[$i][0]))
			{
				$playernamebox[] = mb_strtolower($data[$i][0]);
			}
		}

		$playernamebox = array_unique($playernamebox);
		$playernamebox = array_values($playernamebox);
		$playernameboxCount = count($playernamebox);

		$allpositions_box = [];
		$collected_positions = 0; 

		for ($z = 0; $z < $playernameboxCount; $z++)
		{
			//ta et playernavn
			$targetname = mb_strtolower($playernamebox[$z]);
			//echo '<b><br>' . $z . '. Targetname: "'. $targetname . '"</b><br>';
			$positionsboxlocal = [];
			
			//samle inn alle selskapsnavnene hvor player har shortposisjoner
			for ($x = 0; $x < $count_data; $x++)
			{

				if (!isset($data[$x]))
				{
					//echo 'x';
					continue;
				}

				if ($targetname == mb_strtolower($data[$x][0]))
				{
					//echo '|';
					$positionsboxlocal[] = $data[$x];
					$collected_positions++;
					//var_dump($csvdata[$x]);
					unset($data[$x]);
				}

			}

			//echo '<br>Remaining positions: ' . count($data) . '<br>';

			//remove duplicates
			$count = count($positionsboxlocal);
			//echo 'Local positions ' . $count . '<br>';
			//echo 'positionsboxlocal ' . $collected_positions . '<br>';

			for ($i = 0; $i < $count; $i++)
			{
				if (!isset($positionsboxlocal[$i]))
				{
				   //echo $i . 'Already removed.<br>';
	               continue; //already removed
	           }

	           $target = $positionsboxlocal[$i];
	           $foundcounter = 0; 

	           for ($x = 0; $x < $count; $x++)
	           {

	           	if (isset($positionsboxlocal[$x]))
	           	{

	           		if ($target[0] == $positionsboxlocal[$x][0] and $target[1] == $positionsboxlocal[$x][1] and $target[2] == $positionsboxlocal[$x][2] and $target[3] == $positionsboxlocal[$x][3] and $target[4] == $positionsboxlocal[$x][4])
	           		{

		                    if ($foundcounter == 0) //first, itself, skip
		                    {
		                    	$foundcounter++;
		                    	continue;
		                    }
		                    else
		                    {
		                    	//echo $i . 'Removing position<br>';
		                    	unset($positionsboxlocal[$x]);
		                    }

		                }

		            }

		        }
		    }


		    $positionsboxlocal = array_values($positionsboxlocal);
		    $positionsboxlocal_count = count($positionsboxlocal);

		    //echo 'positionsboxlocal_count: ' . $positionsboxlocal_count . '<br>';

		    for ($x = 0; $x < $positionsboxlocal_count; $x++)
		    {
		    	$allpositions_box[] = $positionsboxlocal[$x];
		    }

		    //echo '<br>-----------------------------<br>';

		}

		//echo 'Collected positions: ' . $collected_positions . '<br>';

		return array_values($allpositions_box);
	} 

}

$csvdata = sanetize_csv_data($csvdata, $land);
echo 'Count er nå ' . count($csvdata) . '<br>';

echo '<b>Doing duplicate data search:</b><br>';
$csvdata = find_duplicates_csv_data($csvdata);

echo 'Count etter find_duplicates_csv_data er ' . count($csvdata) . '<br>';


//lag alle posisjonene som objekt
$posisjoner = array();
$errorholder = array();

//les inn hovedlisten for å match isin mot ticker
//les inn tickerliste 
$name_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

$used_list_isin = array();
$used_list_companies = array();
$miss_box = [];
$miss_box_outstanding = [];

$alternative_search_array = [];
$alternative_hit = 0;
$misses_array = [];

//deploy country spider for misses
include_once '../production_europe/functions_spider.php';

$spider = [];

foreach ($csvdata as $key => $position) 
{

	//finner isin, alternativt navn, ticker i isinlisten
	$targetname = mb_strtolower($position[1]);
	$targetname = filter_name($targetname);

	$targetisin = $position[2];

	$found_in_isin_list = 0;

	//function binary_search_multiple_hits ($isin, $name, $isin_sorted_list, $name_sorted_list, $land)

	//$firstrow = binary_search_isinliste($name, $name_liste);
	if ($row = binary_search_multiple_hits($targetisin, $targetname, $isin_list, $name_list, $land))
	{
		//echo  $key . '. Found ' . $targetisin . ' ' . $targetname . '<br>';

		$alternative_search_array[$targetname][] = $targetisin;

		$currency = $row[3];
		$tickerselskap = $row[1] . '.' . $row[8];

		if (isset($row[9]))
		{
			$number_of_stocks = (float)$row[9] * ((float)$position[3]/100);
			$number_of_stocks = round($number_of_stocks,0);
		}
		else
		{
			$number_of_stocks = false;
		}
	}
	else
	{
		

		//searching alternative!
		//echo 'Doing alternative search for isin by name:<br>';

		if (isset($alternative_search_array[$targetname]))
		{

			$targetisin = $alternative_search_array[$targetname][0]; //0 is newest!

			if ($row = binary_search_multiple_hits($targetisin, $targetname, $isin_list, $name_list, $land))
			{
				//echo 'Found. <br>';
				$currency = $row[3];
				$tickerselskap = $row[1] . '.' . $row[8];

				if (isset($row[9]))
				{
					$number_of_stocks = (float)$row[9] * ((float)$position[3]/100);
					$number_of_stocks = round($number_of_stocks,0);
				}
				else
				{
					$number_of_stocks = false;
				}
				$alternative_hit++;
			}

		}
		else
		{

		   	if (empty($spider))
		   	{
		   		echo $key . '. Deploying spider to search for missing company. <br>';
		   		$spider = deploy_spider($land);
		   	}

		   	$spider_found = 0;

		   	//not found, try in spider
		   	if ($spider_isin = search_spider($spider, $targetisin, $targetname))
		   	{
		   		echo $key . '. Missing company ' . $targetname . ' ' .  $targetisin .  '. ';
		   		echo 'Found isin in spider! Doing new search with isin ' . $spider_isin . ' ';

				if ($row = binary_search_multiple_hits($spider_isin, $targetname, $isin_list, $name_list, $land))
				{
					successecho('Found!<br>');

					$currency = $row[3];
					$tickerselskap = $row[1] . '.' . $row[8];
					$tickerselskap = str_replace(" ", '-', $tickerselskap);

					$spider_found = 1;

					if (isset($row[9]))
					{
						$number_of_stocks = (float)$row[9] * ((float)$position[3]/100);
						$number_of_stocks = round($number_of_stocks,0);

					}
					else
					{
						$number_of_stocks = false;
					}
				}
				else
				{
					echo 'No hits while searching with spider hit. This should not happen!<br>';
				}   		
		   		
		   	}
		   	else
		   	{
		   		//echo 'No hit in spider...<br>';
		   	}

		   	if ($spider_found == 0)
		   	{
		   		$misses_array[$targetisin] = $position[1];
				$currency = false;
				$tickerselskap = false;
				$number_of_stocks = false;
				$ticker = false;
		   	}
		
		}

	}		

	//bytt slash-strek med bindestrek i datoene
	$date_temp = $position[4];
	$dateNew = date('Y-m-d', strtotime($date_temp));

	$tickerselskap = str_replace(" ", '-', $tickerselskap);

	if (isset($position[5]) and $position[5] == 'active')
	{
		$isActive = 'yes';
	}
	else
	{
		$isActive = 'no';
	}

	$posisjoner[] = new posisjon($dateNew , $position[0], $position[3], $number_of_stocks, $position[2], $position[1], $isActive, $currency, $tickerselskap);

	$used_list_isin[] =  $position[3];
	$used_list_companies[] = mb_strtolower($position[1]);
}

echo '<br>';



//make alternative grid, so if isin and name does not change at the same time, then company will be found. 

echo 'Alternative hits: ' . $alternative_hit . '<br>';

echo '<h3>Misses: </h3>';

foreach ($misses_array as $key=> $entry)
{
	echo $key . '. ' . $entry . '<br>';
}

//sort positions by date
usort($posisjoner, function ($item1, $item2) {
	return $item1->ShortingDate < $item2->ShortingDate;
});

$used_list_isin = get_unique_indexed($used_list_isin);
$used_list_companies = get_unique_indexed($used_list_companies);

//logg ikke funnet
$count = count($posisjoner);

echo '<b>Not found isin or outstanding stocks: </b><br>';
$misscount = 0; 

for ($i = 0; $i < $count; $i++)
{
	$position = (array)$posisjoner[$i];

	if (in_array(false, $position))
	{
		//echo $i . '. ' . $position['Name'] . ', ' . $position['PositionHolder'] . ', ' . $position['ISIN']  . ',  ' . $position['ShortPercent'] . ',  ' . $position['ShortingDate'] . ' <br>';	
		$misscount++; 

	}

}

echo 'Number of misses = ' . $misscount . '<br>';

//filter du dupletter med ulike case på navnene!
$used_list_isin = array_unique($used_list_isin);
$used_list_companies = array_unique($used_list_companies);

foreach ($used_list_companies as $key => $navn)
{
	$hitcounter = 0;

	foreach ($used_list_companies as $index => $subname)
	{
		if (mb_strtolower($navn) == mb_strtolower($subname))
		{
			if ($hitcounter == 0)
			{
				$hitcounter++;
				continue;
			}
			echo 'Unsetting duplicate name: ' . $used_list_companies[$index] . '<br>';
			unset($used_list_companies[$index]);
		}
	}
}

$used_list_companies = array_values($used_list_companies);

$selskaper = array();

$poskopi = $posisjoner;

$maincounter = 0;

//ikke match mot navn, match heller mot isin

//behandle hver unike navn
foreach ($used_list_companies as $key => $posisjon) 
{
	//build company 

	//Ta selskapnavnet
	$selskapnavnet = $posisjon;

	//Finn isin
	$posisjonsholder = array();

	//ta player-navnet

	$isin ='';
	$counter = 0;
	$last_updated;
	
	foreach ($posisjoner as $index => $pos)  {

		//var_dump($pos);
		if (mb_strtolower($pos->Name) == mb_strtolower($selskapnavnet)) 
		{

			$isin = $pos->ISIN;
			$currency = $pos->currency;
			$posisjonsholder[] = $pos;	
			$counter++;
		}

	}

	$maincounter += $counter;

	$antall_posisjoner = count($posisjonsholder);

	//Nå har vi alle aktive posisjoner, og må regne ut summen av alle prosentene og av alle aksjene.
	$accumulated_short_percent = 0;
	$accumulated_short_stocks = 0;

	$ticker = '';

	foreach ($posisjonsholder as $index => $posisjon2) 
	{

		if ($index == 0)
		{
			$last_updated = $posisjon2->ShortingDate;
			$ticker = $posisjon2->ticker;
		}
		else
		{
			if ($last_updated < $posisjon2->ShortingDate)
			{
				//set new latest updated date
				//echo ' | Setting new newest position to ' . $posisjon->ShortingDate . '<br>';
				$last_updated = $posisjon2->ShortingDate;
			}
		}

		$accumulated_short_percent += $posisjon2->ShortPercent;
		$accumulated_short_stocks += $posisjon2->NetShortPosition;

	}

	$accumulated_short_percent = round($accumulated_short_percent,2);
	$accumulated_short_percent = number_format($accumulated_short_percent,2,".",",");

	//Skal selskap og posisjoner, men bare hvis det er shorting i selskapet, og bare aktive posisjoner innad i selskapet
	$selskaper[] = new selskap($isin, $selskapnavnet, $accumulated_short_percent, $accumulated_short_stocks, $last_updated, $antall_posisjoner, $posisjonsholder, $currency, $ticker);

}

//Gå igjennom og sørg for at alle navnene under samme isin har samme navn
//navnet i den første posisjonen blir brukt
//remove positions with zero positions
$count = count($selskaper);

for ($i = 0; $i < $count; $i++)
{
	
	$name = $selskaper[$i]->Positions[0]->Name;
	$isin = $selskaper[$i]->ISIN;
	$selskaper[$i]->Name = $name;

	$pos_count = count($selskaper[$i]->Positions);

	for ($x = 0; $x < $pos_count; $x++)
	{

		$selskaper[$i]->Positions[$x]->Name = $name;
		$selskaper[$i]->Positions[$x]->ISIN = $isin;

	}

}
//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 




//remove positions with zero positions
$count = count($selskaper);
$totalcount = 0; 

for ($i = 0; $i < $count; $i++)
{
	$positionscount = count($selskaper[$i]->Positions);
	$totalcount += $positionscount;

	if (!$positionscount > 0)
	{
		echo $i . '. Deleting position for zero positions <br>';
		unset($selskaper[$i]);
	}
}

$selskaper = array_values($selskaper);

echo 'Companies out: ' . count($selskaper) . '<br>';
echo 'Positions out: ' . $totalcount . '<br>';

//write current
saveJSON($selskaper, $fileCurrent);
saveJSON($selskaper, $filewithdate);

print_mem();



?>