<?php
if (!function_exists('saveCSV')) {

    //Lagre nøkkeldata funksjon
    function saveCSV($input, $file_location_and_name) {
        $fp = fopen($file_location_and_name, 'w');

        foreach ($input as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }
}

if(!function_exists('readCSV_merge')){
   function readCSV_merge($file_location_and_name) {

    $csvFile = file($file_location_and_name);

    //Les in dataene 
    $name = [];
    foreach ($csvFile as $line) {
        $name[] = str_getcsv($line, ',');
    }

    //ordne utf koding
    $counter = 0;
    foreach ($name as &$entries) {
        $data[$counter] = array_map("utf8_encode", $entries);
        $counter++;
    }
    return $name;
}
}

if(!function_exists('joinFiles')){
    function joinFiles(array $files, $result) {
        if(!is_array($files)) {
            throw new Exception('$files` must be an array');
        }

        $wH = fopen($result, "w+");

        foreach($files as $file) {
            $fh = fopen($file, "r");

            while(!feof($fh)) {
                fwrite($wH, fgets($fh));
            }
            fclose($fh);
        fwrite($wH, "\n"); //usually last line doesn't have a newline
    }
    fclose($wH);
}
}

//$land = 'sweden';

$history = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.csv';
$active = '../short_data/dataraw/' . $land . '.history/' . $land  . '.current_active.csv';
$merged = '../short_data/dataraw/' . $land . '.history/' . $land  . '.history_merged.csv';

if ($land == 'germany')
{
    echo '<br>Germany has duplicate positions, so active positions must be removed from historic positions<br>';

    //germany has duplicate positions, so active positions must be removed from historic positions
    $historypositions = readCSVkomma($history);
    $activepositions = readCSVkomma($active);

    $activepositionscount = count($activepositions);
    $historypositionscount = count($historypositions);

    $removecounter = 0;

    //take all active positions 

    for ($i = 0; $i < $activepositionscount; $i++)
    {

        $targetarray = $activepositions[$i];

        unset($targetarray[5]);

        for ($x = 0; $x < $historypositionscount; $x++)
        {
            if (!isset($historypositions[$x]))
            {
                continue;
            }

            $result = array_diff($targetarray, $historypositions[$x]);

            if ($result)
            {


            }
            else
            {
                $removecounter++;
                unset($historypositions[$x]);
                break;
            }
        }

        //echo '<br>';
    }

    echo 'Removed ' . $removecounter  . ' positions from german historic positions<br>';

    $historypositions = array_values($historypositions);

    //also convert the kommas in the shortpercent
    $activepositionscount = count($activepositions);
    $historypositionscount = count($historypositions);

    for ($i = 0; $i < $activepositionscount; $i++)
    {
        if (isset($activepositions[$i][3]))
        {
            $activepositions[$i][3] = str_replace(",", ".", $activepositions[$i][3]);
        }

    }

    for ($i = 0; $i < $historypositionscount; $i++)
    {
        $historypositions[$i][3] = str_replace(",", ".", $historypositions[$i][3]);
    }

    saveCSV($historypositions, $history);
    saveCSV($activepositions, $active);

}

joinFiles(array($active, $history), $merged); //lagrer også

$merged_save_file_location = $merged;
$merged = readCSV_merge($merged);

$merged_count = count($merged);

for ($i = 0; $i < $merged_count; $i++)
{
    $localcount = count($merged[$i]);

    for ($x =0; $x < $localcount; $x++)
    {
        $merged[$i][$x] = trim($merged[$i][$x]);
    }

}


if ($land == 'sweden')
{
    echo 'Sweden. Scanning positions for "<"<br>';
    $merged_count = count($merged);
    $set_to_zero = 0;

    for ($i = 0; $i < $merged_count; $i++)
    {
        if (!isset($merged[$i]))
        {
            echo $i . ' data missing, continuing...<br>';
            continue;
        }

        if (!isset($merged[$i][3]))
        {
            echo $i . ' data missing. <br>';        

            //trying repair
            if (isset($merged[$i][0]) and isset($merged[$i][1]) and isset($merged[$i][2]))
            {
                if (isset($merged[$i+1][0]) and !isset($merged[$i+1][1]))
                {
                    $localarray = explode(",", $merged[$i+1][0]);

                    $merged[$i][3] = $localarray[1];
                    $merged[$i][4] = $localarray[2];

                    echo '--------------------------------------------------<br>';
                    echo 'Repaired position. <br>';
                    var_dump($merged[$i]);
                    echo '--------------------------------------------------<br>';

                    unset($merged[$i+1]);
                    $i--;
                    continue; 
                }

            }
            else
            {
                echo 'Unsetting position. <br>';
                unset($merged[$i]);
                continue;
            }

        }

        if (isset($merged[$i]) and stripos($merged[$i][3], "<") !== false)
        {
            $merged[$i][3] = 0;
            //echo $i . '. Position set to zero. <br>';
            $set_to_zero++;
        }

    }

    echo  $set_to_zero . ' positions set to zero<br>';

    $merged = array_values($merged);
}


foreach ($merged as $key => $row) 
{
    if ($row[0] == '') 
    {
        unset($merged[$key]);
    }
}

$merged = array_values($merged);

saveCSV($merged, $merged_save_file_location );

echo 'Saved file ' . $merged_save_file_location . '<br>';

$dataArrayA = null;
$historypositions = null;
$activepositions = null;

?>