<?php 

//this should run a few times a day only
echo '<br>';

//$land = 'norway';

$country = $land;

include '../production_europe/options_set_currency_by_land.php';
require '../production_europe/namelink.php';
require '../production_europe/functions.php';

$activePositions = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';

$name_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );

//$pageTitle = 'Newly shorted companies ' . ucwords($country);

$jsonx = readJSON($activePositions);

$jsonCount = count($jsonx);

$positionsBox = [];
$nameBox = [];

//take alle companies and find sector and subsector
for ($i = 0; $i < $jsonCount; $i++) {

  $positionCount = count($jsonx[$i]['positions']);

  for ($x = 0; $x < $positionCount; $x++) {

    if ($row = binary_search_multiple_hits($jsonx[$i]['positions'][$x]['ISIN'], $jsonx[$i]['positions'][$x]['companyName'], $isin_list, $name_list, $land))
    {
      $jsonx[$i]['positions'][$x]['sector'] = $row[4];
      $jsonx[$i]['positions'][$x]['subsector'] = $row[5];
      $sector_name_box[] = $row[4];
      $positionsBox[] = $jsonx[$i]['positions'][$x];
    }
    else
    {
      echo 'Could not find...<br>';
    }
  }
}

if (empty($sector_name_box))
{
  logger('$sector_name_box er tom i sector_json_maker.php' , ' for Shorteurope i land ' . $land);
  return;
}

$sector_name_box = array_unique($sector_name_box);
$sector_name_box = array_values($sector_name_box);

$sector_structure_box = [];

foreach ($sector_name_box as $key => $sector_name)
{
  //collect subcategories
  $subcategories_name_box = [];

  foreach ($positionsBox as $position)
  {

    if ($sector_name == $position['sector'])
    {
      $subcategories_name_box[] = $position['subsector'];
    }
  }

  $subcategories_name_box = array_unique($subcategories_name_box);
  $subcategories_name_box = array_values($subcategories_name_box);
  
  //make structure
  $obj = new stdClass;
  $obj->sectorname = $sector_name;
  $obj->marketValue = 0;
  $obj->subcategories = [];

  foreach ($subcategories_name_box as $subcategories_name)
  {

    $subobj = new stdClass;
    $subobj->subcategoryname = $subcategories_name;
    $subobj->subcategoryvalue = (float)0;
    $obj->subcategories[] = (array) $subobj;
  }

  $sector_structure_box[] = (array)$obj;
}


//fill inn market value

foreach ($sector_structure_box as $key => $sector_structure)
{

  $targetsector = $sector_structure['sectorname'];

  //echo $targetsector . ' ----------------------------- <br>';

  foreach ($positionsBox as $position)
  {

    if ($position['sector'] ==  $targetsector)
    {
      //echo $targetsector . ' hit, lets find subsector...<br>';
      foreach ($sector_structure['subcategories'] as $index => $subcategory)
      {
        
        if ($subcategory['subcategoryname'] == $position['subsector'])
        {
          //add in market value
          $sector_structure_box[$key]['subcategories'][$index]['subcategoryvalue'] += $position['marketValueBaseCurrency'];
          $sector_structure_box[$key]['marketValue'] += $position['marketValueBaseCurrency'];
        }
      }
    }
  }
  usort($sector_structure_box[$key]['subcategories'], function($first,$second){
    return $first['subcategoryvalue'] < $second['subcategoryvalue'] ;
  });
}

usort($sector_structure_box, function($first,$second){
  return $first['marketValue'] < $second['marketValue'] ;
});

$today = date("Y-m-d");

$url = '../short_data/datacalc/sector/' . $land . '/sector_' . $land . '.' . $today . '.json';
saveJSON($sector_structure_box, $url);

$url = '../short_data/datacalc/sector/' . $land . '/sector_' . $land . '.current.json';
saveJSON($sector_structure_box, $url);

?>

