<?php 

//$land = 'denmark';
//$land = 'italy';

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

echo '<br>';

$limit_date_previous = '2020-01-01';

$date = date('Y-m-d');

while (!file_exists('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
		//echo $date . '<br>';
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

	if ($date < $limit_date_previous)
	{
		echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
		return;
	}
}	

//check if file is older than 3 hours, if yes, then skip. 
echo 'Checking timestamp: ';
if (get_file_age_in_hours('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json') > 2.99) 
{
   echo 'More than three hours old, will skip....<br>';
   return;
} 
else 
{
  echo 'Less than three hours old, will continue....<br>';
}


echo 'Reading new positions: '. '../short_data/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json</strong><br>';

//read todays positions
if (!$currentdata = readJSON('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
{
	errorecho('Error reading json ' . '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
	return;
}
$currentfilename = '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json';

//find the newest position date
$newestdate = '2001-01-01';

foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $key => $position)
	{
		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}
}


//echo 'Previous working day of that is ' . $previousday . '<br>';
if ($newestdate == date('Y-m-d', (strtotime ('-1 weekday', strtotime ($date)))))
{
	echo 'Dates match. File has positions with timestamp from previous weekday (' . $newestdate .'). <br>';
}
else
{
	echo 'Newest date in current positions is ' . $newestdate . '.<br>';
	echo('Dates DOES NOT match. Filename does not have positions with timestamp from previous weekday. Probably no new positions?<br>');
}

echo '<br><strong>Finding file with newest position that is older OR EQUAL than date ' . $newestdate . ' found for current.json:</strong> <br>';


$previous_newest_date = $newestdate;

$date = date('Y-m-d', strtotime("+1 weekday", strtotime($newestdate)));

while ($previous_newest_date == $newestdate)
{

	//finding the previous file
	$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));
	

	while (!file_exists('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		
		$date = date('Y-m-d', strtotime("-1 day", strtotime($date)));

		if ($date < $limit_date_previous)
		{
			echo 'Json files not found, returning... Ingen nyere oppføringer.<br>';
			return;
		}
	}	

	//cannot ble same filename as current
	if ($currentfilename == '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json')
	{
		continue;
	}

	echo '--| File is ' . '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'. '<br>';

	//read previous positions
	if (!$previousdata = readJSON('../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json'))
	{
		errorecho('Error reading previous json ' . '../short_data/dataraw/' . $land . '/' . $date . '.' . $land . '.json' . '. Returning!<br>');
		continue;
	}


	//finding newest date
	//find the newest position date
	
	$previous_newest_date = '2001-01-01';

	foreach ($previousdata as $key => $company)
	{
		foreach ($company['Positions'] as $key => $position)
		{
			if ($position['ShortingDate'] > $previous_newest_date)
			{
				$previous_newest_date = $position['ShortingDate'];
			}
		}
	}

	if ($previous_newest_date <= $newestdate)
	{
		successecho('--| Success. Newest date in previous positions is ' . $previous_newest_date . '.<br><br>');
		break;
	}

}


echo 'Reading previous positions: ' . '../short_data/dataraw/' . $land . '/<strong>' . $date . '.' . $land . '.json </strong>because that file has positions from the day before.<br> ';
echo '<br>';
//collect all changes names from new list



$currentBox = [];

//collect positons in two arrays
foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $index => $position)
	{
		
		if (!isset($position['isActive']))
		{
			continue;
		}
		//only take active positions
		if ($position['isActive'] == 'yes')
		{
			//make sure that all positions have a company name, some are missing!
			$currentdata[$key]['Positions'][$index]['Name'] = $company['Name'];
			$currentBox[] = $currentdata[$key]['Positions'][$index];
		}

	}

}

$currentdata = 0; //resett

$previousBox = [];
//collect positons in two arrays
foreach ($previousdata as $key => $company)
{
	foreach ($company['Positions'] as $index => $position)
	{
		if (!isset($position['isActive']))
		{
			continue;
		}

		//only take active positions
		if ($position['isActive'] == 'yes') 
		{
			//make sure that all positions have a company name, some are missing!
			$previousdata[$key]['Positions'][$index]['Name'] = $company['Name'];
			$previousBox[] = $previousdata[$key]['Positions'][$index];
		}
	}
}

$previousdata = 0; //resett
$new_or_updated_Box = [];

//adding all positions with $newestdate to to new_or_updated_Box;


$addcounter = 0;

foreach ($currentBox as $key => $position)
{

	$formatted_date = date('Y-m-d', (strtotime($position['ShortingDate'])));

	if ($formatted_date >= $newestdate)
	{
		//$position['mode'] = 'new_in_current';
		$new_or_updated_Box[] = $position;
		unset($currentBox[$key]);
		$addcounter++;
	}
	
}

echo 'Added ' . $addcounter . ' positions from currentdata based on position date ' . $newestdate . ' to $new_or_updated_Box<br><br>';

//ta new_in_current-posisjonene og finn ut om posisjonen er helt ny, eller om det var en tidligere posisjon
echo '<br>';
echo 'Checking if player and company in $new_or_updated_Box had previous position, or is new position<br>';
echo 'Antall er ' . count($new_or_updated_Box ) . '<br>';

foreach ($new_or_updated_Box as $key => $position)
{

	$success = 0;

	$targetName = $position['Name'];
	$targetPositionHolder = $position['PositionHolder'];

	foreach ($previousBox as $index => $previousposition)
	{

		if (strtolower($previousposition['Name']) == strtolower($targetName) and strtolower($previousposition['PositionHolder']) == strtolower($targetPositionHolder))
		{
			echo $key. '. Player ' . $previousposition['PositionHolder'] . ' har en tidligere posisjon i selskapet ' .  $previousposition['Name'] . '. Settings status to update.<br>';
			$new_or_updated_Box[$key]['status'] = 'update';
			$new_or_updated_Box[$key]['previousposition'] = $previousposition;
			unset($previousBox[$index]);

			$success = 1;
			break;
		}
	}

	if ($success == 0)
	{
		
		echo $key . '. Player ' . $targetPositionHolder . ' har <strong>IKKE</strong> tidligere posisjon i selskapet ' . $targetName . '. Setting status to new.<br>';
		$new_or_updated_Box[$key]['status'] = 'new';
	}
}

//var_dump($new_or_updated_Box);

//comparing previous positions to current positions
//this is the other way around here
echo '<h2>Previous positions</h2>';
echo 'Finding positions in previousdata that are different or not existing in current positions, and hence are changed or ended. <br>';
echo 'Antall er ' . count($previousBox) . '<br>';

$previousadd = 0;

$changed_or_ended_box = [];

foreach ($previousBox as $key => $previousposition)
{
	
	//echo $key . '<br>';
	$targetPositionHolder = $previousposition['PositionHolder'];
	$targetShortingDate = $previousposition['ShortingDate'];
	$targetShortPercent = $previousposition['ShortPercent'];

	
	if (!isset($previousposition['Name']))
	{
		errorecho('Name not set in previous!');
	}

	$targetName = $previousposition['Name'];
	$targetISIN = $previousposition['ISIN'];
	$success = 0;

	foreach ($currentBox as $index => $currentposition)
	{
		
		if (!isset($currentposition['Name']))
		{
			errorecho('Name not set in previouspositon!');
			var_dump($currentposition);
			continue;
		}

		if ($targetShortingDate != $currentposition['ShortingDate'])
		{
			continue;
		}

		if (strtolower($targetPositionHolder) == strtolower($currentposition['PositionHolder']) and $targetShortPercent == $currentposition['ShortPercent'] and strtolower($targetName) == strtolower($currentposition['Name']) and $targetISIN == $currentposition['ISIN'])
		{
			//echo $key . '. Identisk posisjon funnet<br>';
			$success = 1;
			break;
		}

	}

	if ($success == 0)
	{
		//$currentposition['mode'] = 'only_in_previous';
		echo $key . '. ';
		echo 'Previous (identical) position not found in current position';
		echo 'adding to changed_or_ended_box<br>';
		$previousadd++;
		$changed_or_ended_box[] = $previousposition;
		//var_dump($previousposition);
	}
	
}


echo 'Added ' . $previousadd . ' positions from previousdata.<br>';


//looking for positions in current, if not, position is ended

$endedpositions = [];
echo '<br>';
echo 'Checking if previouspositions has same company and player in current. If not, position is ended. <br>';


foreach ($changed_or_ended_box as $key => $position)
{
	$targetName = $position['Name'];
	$targetPositionHolder = $position['PositionHolder'];

	$success = 0;

	foreach ($currentBox as $index => $currentposition)
	{


		if (mb_strtolower($currentposition['Name']) == mb_strtolower($targetName) and mb_strtolower($currentposition['PositionHolder']) == mb_strtolower($targetPositionHolder))
		{
			echo $key . '. Player ' . $currentposition['PositionHolder'] . ' har en posisjon i current i selskapet ' .  $currentposition['Name'] . '. ';
			
			if ($newestdate == $currentposition['ShortingDate'])
			{	

				$temp_array = $currentposition;
				$temp_array['status'] = 'update';
				$temp_array['previousposition'] = $position;
				$endedpositions[] = $temp_array;
				
			}
			else
			{
				echo 'Date is not newestdate, skipping...';
				echo 'Date is current ' . $currentposition['ShortingDate'] . ' vs previousdate ' . $position['ShortingDate']  .  ' vs newestdate ' . $newestdate . '';
			}
			echo '<br>';

			$success = 1;
			break;
		}
	}

	if ($success == 0)
	{
		echo $key . '. ' . $targetName . ' med playernavn ' . $targetPositionHolder . ' <strong>IKKE</strong> funnet. ';
		echo 'Posisjonen legges til med status ended <br>';
		$position['status'] = 'ended';
		$endedpositions[] = $position;
	}
}

//slå sammen arrayene

$allpositions = array_merge($new_or_updated_Box, $endedpositions);
$allpostionsCount = count($allpositions);

//var_dump($allpositions);

//sort the positons
usort($allpositions, function ($item1, $item2) {
	return $item2['PositionHolder'] < $item1['PositionHolder'];
});


//last check of dates. Should be from today or yesterday, if not new!
echo '<br><br>Last check of dates. Should be from today or yesterday, if not new!<br>';

$today = date('Y-m-d');
$yesterday = date('Y-m-d', strtotime("-1 weekday", strtotime($today)));

foreach ($allpositions as $key => $position)
{

	if ($position['ShortingDate'] == $today or $position['ShortingDate'] == $yesterday)
	{
		
	}
	else
	{
		if ($position['status'] == 'new')
		{
			echo 'Date is not today or yesterday, skipping position...<br>';
			unset($changesCompanyBox[$key]);
		}
		
	}
}

$changesCompanyBox = array_values($changesCompanyBox);


echo '<br>';
echo 'Total numer of positions that are changed: ' . $allpostionsCount . '<br><br>';

foreach ($allpositions as $key => $company)
{
	$allpositions[$key] = float_format($company);
}

saveJSON($allpositions, '../short_data/json/events/player/' . $land . '.events.player.current.json');


?>