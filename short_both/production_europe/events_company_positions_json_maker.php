<?php 

//$land = 'denmark';
//$land = 'germany';

require_once '../production_europe/namelink.php';
require_once '../production_europe/logger.php';
require_once '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

if (!$diff = readJSON('../short_data/json/events/diff/' . $land . '.diff.json'))
{
	errorecho('Cannot read diff json file!');
	return;
}

if (!$previous = readJSON('../short_data/dataraw/' . $land . '/' . $diff['previousfile_date'] . '.' . $land . '.json'))
{
	errorecho('Cannot read previousfile_date json file!');
	return;
}

if (!$current = readJSON('../short_data/dataraw/' . $land . '/' . $diff['currentfile_date'] . '.' . $land . '.json'))
{
	errorecho('Cannot read currentfile_date json file!');
	return;
}


echo 'Positions in: ' . count($diff['positions']) . '<br>';
$company_isin_box = [];
$company_name_box = [];

foreach ($diff['positions'] as $entry)
{

	$company_isin_box[] = mb_strtolower($entry['ISIN']);
	$company_name_box[mb_strtolower($entry['ISIN'])] = $entry['Name'];

}

$company_isin_box = array_unique($company_isin_box);
$company_isin_box = array_values($company_isin_box);

$changesCompanyBox = [];


///var_dump($company_isin_box);
//var_dump($company_name_box);

//ta hvert navn og finn shortprosent i current og previous posisjoner
foreach ($company_isin_box as $key => $isin)
{
	echo $key . '. Doing ' . $isin . ' | ' . $company_name_box[$isin] . ' | ' ;

	$current_short_percent = 0; 
	$previous_short_percent = 0; 
	$previousNumPositions = 0;

	$found_current = false; 
	$found_previous = false; 

	$found_previous = 0;

	//find same company in previous
	foreach ($previous as $selskap)
	{

		if ($isin == mb_strtolower($selskap['ISIN']))
		{
			//go through positions 
			$shortpercent_sum = 0;
		
			foreach ($selskap['Positions'] as $position)
			{

				$shortpercent_sum += $position['ShortPercent']; 

			}

			$previousNumPositions = count($selskap['Positions']);

			$previous_short_percent = $shortpercent_sum; 
			echo 'Fant i previous!<br>';
			$found_previous = 1;
			$entry = $selskap;
			break;
		}

	}

	if ($found_previous == 0)
	{
		echo 'Fant ikke i previous!<br>';
		$previous_short_percent = 0; 
		$shortpercent_sum = 0; 

	}

	$found_current = 0;
	//find in current
	foreach ($current as $selskap)
	{

		if ($isin == mb_strtolower($selskap['ISIN']))
		{
			//go through positions 
			$shortpercent_sum = 0; 
	
			foreach ($selskap['Positions'] as $position)
			{

				$shortpercent_sum += $position['ShortPercent']; 

			}

			$currentNumPositions = count($selskap['Positions']);

			$current_short_percent = $shortpercent_sum; 
			echo 'Fant i currrent!<br>';
			$found_current = 1;
			$entry = $selskap;
			break;
		}

	}

	if ($found_current == 0)
	{
		echo 'Fant ikke i current!<br>';
		$previousNumPositions = 0;
		$current_short_percent = 0; 
		$shortpercent_sum = 0; 
	}

	if ($found_current == 0 and $found_previous == 0)
	{
		errorecho('Not found in any! Will skip. <br>');
		continue;
	}
	
	
	unset($entry['Positions']); //positions not needed

	$entry['previousShortPercent'] =  $previous_short_percent;
	$entry['previousNumPositions'] =  $previousNumPositions;
	$entry['change'] =  $current_short_percent - $previous_short_percent;
	$entry['change'] = round($entry['change'],2);

	$changesCompanyBox[] = $entry;

	echo 'Shortpercent current: ' . $current_short_percent . ' | ';
	echo 'Shortpercent previous: ' . $previous_short_percent . '<br>';

}

usort($changesCompanyBox, function($a, $b) {
    return $b['change'] <=> $a['change'];
});


//var_dump($changesCompanyBox);

echo 'Number of positions out: ' . count($changesCompanyBox) . '<br>';
saveJSON($changesCompanyBox, '../short_data/json/events/company/' . $land . '.events.company.current.json');

//var_dump($changesCompanyBox);


?>