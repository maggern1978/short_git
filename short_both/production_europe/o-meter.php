<?php 


$filename = '../short_data/datacalc/shortlisten_' . $land . '_current.csv';

include '../production_europe/options_set_currency_by_land.php';

require '../production_europe/namelink.php';

if (!function_exists('settKlasse')) {
    function settKlasse($classTarget) {
        if ((float)$classTarget >= 0) {
            if ($classTarget >= 0 and $classTarget <= 1) {
                $classHolder = 'meter-up-0';
            }
            elseif ($classTarget >= 1.00000000001 and $classTarget <= 2) {
                $classHolder = 'meter-up-1';
            }
            elseif ($classTarget >= 2.000000000001 and $classTarget <= 3) {
                    $classHolder = 'meter-up-2';
            }
            elseif ($classTarget >= 3.000000000001 and $classTarget <= 4) {
                    $classHolder = 'meter-up-3';
            }
            elseif ($classTarget > 4) {
                    $classHolder = 'meter-up-4';
            }
            else {
              echo 'Error 102:';
              var_dump($classTarget);
              return;
            }
        }
        else {
            if ((float)$classTarget <= 0 and $classTarget >= -1) {
                $classHolder = 'meter-up-0';
            }
            elseif ($classTarget <= -1.0000000001 and $classTarget >= -2) {
                $classHolder = 'meter-down-1';
            }
            elseif ($classTarget <= -2.0000000001 and $classTarget >= -3) {
                    $classHolder = 'meter-down-2';
            }
            elseif ($classTarget <= -3.0000000001 and $classTarget >= -4) {
                    $classHolder = 'meter-down-3';
            }
            elseif ($classTarget < -4) {
                    $classHolder = 'meter-down-4';
            }
            else {
              echo 'Error 101';
              var_dump($classTarget);
              return;
            }
        }
        if (isset($classHolder)) {
          return $classHolder;
        }
        else {
          echo 'Error 103';
          return;
        }
        
    }   
}


//Last inn nyeste shorttabell 

            //Prepare data
            //Read in data

                $csvFile = file($filename);            
                $data = [];
                foreach ($csvFile as $line) {
                    $data[] = str_getcsv($line);
                } 
         
            unset($data[0]);
          //var_dump($data);
         

          ob_start();

          $antallSelskaper = count($data);
          echo '<ol class="o-meter">';
          //echo $antallSelskaper;

          $tickerBox = array();
          $nameBox = array();
          $percentBox = array();

          foreach ($data as $key => $line) {
              $tickerBox[] = $line[2];
              $tempName = strtolower($line[0]);
              $nameBox[] = ucwords($tempName);
              $percentBox[] = $line[11];
          }

          array_multisort($percentBox, SORT_DESC,$nameBox, $tickerBox);

          //var_dump($percentBox);
          //var_dump($nameBox);
          //var_dump($tickerBox);


          if ($antallSelskaper > 24) {
            
            //drep verdiene rundt null til det bare er 24 igjen

            $increment = 0;
            do {

                //only remove on number at a time;
                $increment += 0.1;
                foreach ($percentBox as $key => $percent) {
                    if (abs($percent) < $increment) {
                        unset($percentBox[$key]);
                        unset($nameBox[$key]);
                        unset($tickerBox[$key]);
                        //echo 'Unset ' . $key . '<br>';
                        break;
                    }
                }

            }
            while (count($percentBox) != 24);
        }
            //echo count($percentBox);

            //finn de midterste verdiene: 
            $midtverdiNr1 = ceil($antallSelskaper/2);
            $classHolder = '';


            echo '<ol class="o-meter">';

            foreach ($tickerBox as $key => $ticker) {
                
                //sett class
                $classTarget = $percentBox[$key];
                $class = settKlasse($classTarget); 



                $findpos = strpos($ticker,".",0);
                $tickerShort = substr($ticker, 0, $findpos);

                //Shorten maersk-b

                if (strtoupper($tickerShort) == strtoupper('MAERSK-B')) {
                  $tickerShort = 'MAERSK';
                }

                //edit link name
                $linkCompany   = nametolink($nameBox[$key]);
                $linkCompany = strtoupper($linkCompany);

                //echo out

                  echo '<a class="o-meter-link" href="details_company.php?company=' . $linkCompany  . '&land=' . $land . '">';
                  echo '<li class="' . $class;
                  echo '" data-toggle="tooltip"';
                  echo ' title="';
                  echo $nameBox[$key];
                  echo '"><div class="ticker">';
                  echo $tickerShort . '</div>';
                  echo '<div class="';
                  echo 'number">';
                  echo round($percentBox[$key],2);
                  echo ' %</div>';
                  echo '</a>';                
                  
            }

            echo '</ol>';

          
 
//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
//Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 

$fileNameStart = '../short_data/html/o-meter_';
$fileNameMiddle = $land;
$fileNameEnd = '.html';

$filename = $fileNameStart . $fileNameMiddle . $fileNameEnd;

// Write final string to file
file_put_contents($filename, $htmlStr);


  ?>

          
         
    


          
