<?php 

include '../production_europe/options_set_currency_by_land.php';

$filename = '../short_data/datacalc/shortlisten_' . $land . '_current.csv';


                //Last inn data
                //$filename = 'datacalc/shortlisten_nor_current.csv';
                $csvFile = file($filename);            
                $data = [];
                foreach ($csvFile as $line) {
                     $data[] = str_getcsv($line); 
                } 
                
                //find file timestamp
                
                if (file_exists($filename)) {
                    $filetime = filemtime($filename);
                }

                //Push data inn i treemap object
                $treemap = array();
                $counter = 0;
                $totalt = 0;

                foreach($data as $entry) {
                 
                    if ($counter != 0) {
                        $tall = (((float)$entry[4]*(float)$entry[6])/1000000);
                        $tall = $tall * $entry[15];
                        $tall = round($tall,0);
                        $totalt = $totalt + $tall;
                        //$treemap[] = new company($entry[0], $tall);

                        $page=new stdClass();

                        if ($land == 'sweden') {
                            $name = $entry[0];
                            $name = strtolower($name);
                            $page->name = ucwords($name);
                        }
                        else {
                             $page->name=$entry[0];   
                        }

                        
                        $page->value=$tall;
                        $treemap[] = $page;

                        }

                $counter++;
                }

usort($treemap,function($first,$second){
    return $first->value < $second->value;
});


// Turn on output buffering
ob_start();

            ?>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 pb-3">

 <div id="<?php echo $land; ?>4" style="width: 100%;height:350px;"></div>
    <script type="text/javascript">

        // based on prepared DOM, initialize echarts instance
        var myChart1<?php echo $land; ?> = echarts.init(document.getElementById('<?php echo $land; ?>4'));
        var verdier = <?php echo json_encode($treemap); ?>;
        var data = verdier;

        option = {
            toolbox: {
                show : true,
                itemSize: 15,
                feature : {
                    dataView : {
                    show: true,
                    title: 'Data',
                    lang: ['Treemap', 'Exit', 'Refresh'],
                    },
                },
            },
            animation: false,
            textStyle: {
                fontFamily: ['Encode Sans','sans-serif'],
            },
            animation: false,
            backgroundColor: '#f9f9f9',
            color: ['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'],

        title: { 
            
            text: 'Total value shortpositions: <?php echo number_format($totalt,0,".","."); ?>M <?php echo $currency_ticker;?> ',
            link: 'most_shorted_companies_value.php?country=<?php echo $land; ?>',
            left: 'center',
            subtext: '(Shorted stocks * price) Updated: <?php echo date("Y-m-d H:i", $filetime); ?>',
            padding: [10,0,0,0],
            subtextStyle: {
                color: 'black',
                fontSize: 14,
                },
            backgroundColor: '#f9f9f9',
            borderWidth: 5,
            borderColor: '#f9f9f9',
           
        },

        tooltip: {
            confine: true,
            formatter: '{b}: {c}M. <?php echo $currency_ticker;?> ',
            backgroundColor: "black",
            },

        series: [
            {
                name:'Treemap: Numbers in million <?php echo $currency_ticker;?>',
                top: 60,
                bottom: 25,
                left: 20,
                right: 20,
                type:'treemap',             

                itemStyle: {
                    normal: {
                        borderColor: '#fff'
                    }
                },
                data: verdier,
            }
        ]
    
        };

        // use configuration item and data specified to show chart
        myChart1<?php echo $land; ?>.setOption(option);
        
        window.addEventListener('resize', function(event){
          myChart1<?php echo $land; ?>.resize();
          });

    </script>


</div>

<?php 

$filenameStart = '../short_data/html/treemap_';
$filenameCountry = $land;
$filenameEnd = '.html';
$filename = $filenameStart . $filenameCountry . $filenameEnd;

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename , $htmlStr);


?>