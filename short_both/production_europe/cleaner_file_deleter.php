<?php

include '../production_europe/functions.php';
$countries = get_countries();
$directoryList = [];

echo '<br>';

foreach ($countries as $country)
{
	echo '<strong>' . $country . '</strong><br>';
	$prefix = '../short_data/dataraw/';
	$files = listfiles($prefix . $country);
	deletefiles($files, $prefix . $country . '/');

	$prefix = '../short_data/dataraw/';
	$files = listfiles($prefix . $country . '.history/');
	deletefiles($files, $prefix . $country . '.history/');

	$prefix = '../short_data/datacalc/players/';
	$files = listfiles($prefix . $country . '/');
	deletefiles($files, $prefix . $country . '/');

}


$files = listfiles('../short_data/history_bulk/');

foreach ($files as $file)
{

	$localfiles = listfiles('../short_data/history_bulk/' . $file . '/');
	deletefiles($localfiles, '../short_data/history_bulk/' . $file . '/');

}

$files = listfiles('../short_data/history/');

deletefiles($files, '../short_data/history/');




function deletefiles($array, $prefix)
{

	foreach ($array as $file)
	{

		//strpos(string,find,start)
		if ((is_file($prefix . $file)))
		{

			$tendaysago = date('Y-m-d', strtotime("-9 day"));
			$filetime = filemtime($prefix . $file);
			$filedate = date('Y-m-d', $filetime);

			if ($filedate < $tendaysago)
			{
				echo $prefix . $file . ': ' . $filedate.  ' older then ' . $tendaysago . '<br>';
				unlink($prefix . $file);
			}
		}
		else 
		{
			echo 'Skipping ' . $file . '<br>';
		}

	}

}


?>