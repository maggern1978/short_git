
<?php 

include '../production_europe/namelink.php';
require '../production_europe/logger.php';

if (!function_exists('readJSON')) {
  function readJSON($file_location_and_name) {
      $string = file_get_contents($file_location_and_name);
      return $data = json_decode($string, true);
  }
}

//$land = 'united_kingdom';
//$land = 'norway';

include '../production_europe/options_set_currency_by_land.php';

$filename = '../short_data/datacalc/shortlisten_' . $land . '_current.csv';
$graphlocation = '../short_data/history_shorting/' . $land . '/';

//last inn shortlisten som inneholder prosent short


$csvFile = file($filename);            
$data = [];
foreach ($csvFile as $line) {
    $data[] = str_getcsv($line); 
} 

$selskapsnavn = array();
$short_prosent = array();
$tickerBox = array();

//Hent ut relevante data
foreach ($data as $entry) {

    $temp_navn = $entry[0];
        if ($land  == 'sweden' ) {
            $temp_navn = strtolower($temp_navn);
            $temp_navn = ucwords($temp_navn);
            //echo 'hit';
        }
    $selskapsnavn[] = $temp_navn;
    $short_prosent[] = $entry[5];
    $tickerBox[] =  $entry[2];
}

//ta bort indeks
unset ($selskapsnavn[0]);
unset ($short_prosent[0]);
unset ($tickerBox[0]);

$changeLastWeekBox = array();
$setDate = 0;

$heatmapBoxAllHolder = [];

//sorter arrayene, størst først
array_multisort($short_prosent, SORT_DESC, 
                $selskapsnavn,
                $tickerBox);

foreach ($tickerBox as $index => $ticker) {
    $heatmapBox = [];
    $tickerLocation = $graphlocation  . strtolower($ticker) . '.json';

    if (file_exists($tickerLocation)) {
        //echo 'Fant: ' . $ticker . '<br>';

        $ticker = readJSON($tickerLocation);
        
        if (!$counted = count($ticker))
        {
            continue;
        }

        //var_dump($ticker);

        if ($counted == 0) {
            //echo 'Could not readjson, returning in heatmap.php' . ' ' . $tickerLocation;
            //logger ('Could not readjson, continuing in heatmap.php' , ' land er ' . $land . ' | tickerlocation er ' . $tickerLocation);
            continue;
        }

        for ($i = 0; $i < $counted; $i++) {
            
            if (!isset($ticker[$i]['shortPercent']))
            {
                //unset($ticker[$i]);
            }
        }
      

        $keys = array_keys($ticker);
        $ticker = array_values($ticker);
        $counted = count($ticker);
        //heatmap

        //find min and max
        $allPercentBox = array();

        for ($i = 0; $i < $counted; $i++) {

            $allPercentBox[] = $ticker;
        }





        $minValue = 0;
        $maxValue = max($allPercentBox);

        //var_dump($maxValue);

        for ($i = 0; $i < $counted; $i++) {
            $colorNumber = (float)$ticker;
            //$color = numberToColorHsl($colorNumber/$maxValue, 0, (float)$maxValue);      
            $heatmap = new stdClass;
            //$heatmap->color = $color;
            $heatmap->date = $keys[$i];
            $heatmap->name = $selskapsnavn[$index];

            $heatmap->shortPercent = $ticker[$i];


            //$heatmap->price = $ticker[$i]['price'];
            $heatmap->change = '';
            $heatmapBox[] = $heatmap;
        }
        
        $firstvalue;

        for ($i = 0; $i < $counted; $i++) {
            if (isset($firstvalue)) {
                //echo ' hit! med value ' . $firstvalue . '<br>';
                 if ($heatmapBox[$i]->shortPercent == $firstvalue) {
                    //echo '<br>Verdiene er like: ' . $heatmapBox[$i]->shortPercent . ' - ' . $firstvalue;
                    //var_dump($heatmapBox[$i]);
                    unset($heatmapBox[$i]);
                 }
                 else {
                    $heatmapBox[$i]->change = round((float)$heatmapBox[$i]->shortPercent - (float)$firstvalue,2); 
                    $firstvalue = $heatmapBox[$i]->shortPercent;
                 }
            }
            else {
                $firstvalue = $heatmapBox[$i]->shortPercent;
                $heatmapBox[$i]->change = 0;
               
                //echo '<br>FIRSTVALUE ER IKKE set!' . $heatmapBox[$i]->shortPercent . ' ' . $firstvalue;
            }
        }

        $heatmapBox = array_values($heatmapBox);
        $heatmapBoxAllHolder[] = $heatmapBox;
        
    }
    else {
        //echo 'File not found!';
        errorecho('Fant IKKE: ' . $ticker . '<br>');
        //logger('fant ikke' . $ticker, ' i mostShortedCompanies_b4.php');
        $changeLastWeekBox[] = 0;
    }
    if ($index == 1){
        //break;
    }



}

//var_dump($heatmapBoxAllHolder[9]);
//var_dump($heatmapBoxAllHolder[1][1]);

//slett alle innføringer etter verdi nummer 10 slik at det blir lettere med datene i echarts
ob_start();

?>

<div class="container">
  <div class="row ">
<div class="col-12">
<h1>Shorting analysis over time.<br> Active positions in <?php echo ucwords($land); ?>.</h1> 
<p>Here we display up to the last 40 accumulated daily changes in short positions for each company. From left (oldest) to right on each line of 10 changes. From top (oldest) to bottom. Bottom right is the most recent position. Red means an increase in the aggregated short position, green a decrease. Positions change relative to initial short position. Companies sorted by most shorted in percent.</p> 
  
    <?php 
    $redColor = '222,51,47';
    $greenColor = '34,172,90';

    $heatMapCount = count($heatmapBoxAllHolder);
    for ($i = 0; $i < $heatMapCount; $i++) {
        
        if (!isset($heatmapBoxAllHolder[$i][0]->name)){
            continue;
        }
        
        $positionsCount = count($heatmapBoxAllHolder[$i]);
        $indexNumber = $i +1 ;
        echo '';
        echo '<h3  class="pb-2">' . $indexNumber  . '. ';

        $linkname = nametolink($heatmapBoxAllHolder[$i][0]->name);


        echo '<a class="black-text" href = "details_company.php?company=' . $linkname . '&land=' . $land . '">';
        echo $heatmapBoxAllHolder[$i][0]->name;
        echo '</a> ';

        echo round($short_prosent[$i],2) . '%';
        echo '</h3>';
        echo '<div class="flex-column">';
        echo '<div class="d-inline-flex">';
        echo '<div class="d-inline-flex heatmap-width-holder-circles">';
        if ($positionsCount > 40) {
            $startPoint = $positionsCount - 40;
        }
        else {
            $startPoint = 0;
            $heatmapBoxAllHolder[$i][0]->change = 0;
        }

        $runner = 0;
        for ($x = $startPoint; $positionsCount > $x; $x++) {
            if ($x == $startPoint) {
                 $startValuePercent = round($heatmapBoxAllHolder[$i][$x]->shortPercent,1);
                 $startDate = $heatmapBoxAllHolder[$i][$x]->date;
            }
           
            $runner++;

            echo '<div class="flex-column mr-1 pb-2"';
            echo 'data-toggle="tooltip" data-placement="top" data-html="true"';
            echo 'title="' . $heatmapBoxAllHolder[$i][$x]->date;
            echo '<br>Shortpercent: ' . round($heatmapBoxAllHolder[$i][$x]->shortPercent,2) .  '"';
            echo '>';
            echo '<div>';

            if ($heatmapBoxAllHolder[$i][$x]->change >= 0 ) {
                echo '<svg width="24" height="24">
                  <circle cx="12" cy="12" r="11" style="fill:rgb(' . $redColor . 
                  ');stroke-width:0;stroke:rgb(255,255,255)" />
                </svg>';
            }
            else {
                echo '<svg width="24" height="24">
                  <circle cx="12" cy="12" r="11" style="fill:rgb(' . $greenColor . 
                  ');stroke-width:0;stroke:rgb(255,255,255)" />
                </svg>';                
            }
            echo '</div>';
            echo '<div class="text-center heatmap-percent-text">';
            if ($positionsCount == $x+1) {
                echo '<span class="font-weight-bold">';
            }
            echo round($heatmapBoxAllHolder[$i][$x]->shortPercent,1);
             if ($positionsCount == $x+1) {
                echo '</span>';
            }
            echo '</div>';
            echo '</div>';

            $z = $x;
            if ($positionsCount == $x+1 or $runner % 10 == 0 and $runner != 0) {
                $shortPercentChange = round($heatmapBoxAllHolder[$i][$x]->shortPercent - $startValuePercent,1);
                $earlier = new DateTime($startDate);
                $later = new DateTime($heatmapBoxAllHolder[$i][$x]->date);
                $diff = $later->diff($earlier)->format("%a");

                echo '</div>';
                echo '<div class="mx-3">';
                echo '<svg width="3" height="47">
                        <rect width="3" height="44" style="fill:rgb(0,0,0);stroke-width:0;
                            stroke:rgb(0,0,0)" />
                      </svg>';
                echo '</div>';
                echo '<div class="flex-column heatmap-width-holder">';
                echo '<div class="heatmap-percentage" '; 
                echo 'data-toggle="tooltip" data-placement="top" data-html="true" ';
                echo 'title="Change shortpercent ';
                echo round($heatmapBoxAllHolder[$i][$x]->shortPercent - $startValuePercent,3);
                echo '<br> (from ' . $startDate . ' to ' . $heatmapBoxAllHolder[$i][$x]->date . ') ';
                
                echo '"';
                echo '>';

                if ($shortPercentChange > 0) {
                    echo '+';
                }
                echo $shortPercentChange . '%';
                echo '</div>';
                echo '<div class="heatmap-days mt-1 text-center">';

                
                echo $diff . ' days';
                echo '</div>';
                echo '</div>';
                echo '';
                echo '<div class="mx-3 heatmap-bar d-none d-sm-block">';

                $barLength = abs($shortPercentChange)*18;
                if ($shortPercentChange>0) {
                    $color = $redColor ;
                } else {
                    $color = $greenColor ;
                }

                echo '<svg width="' . $barLength . '" height="19">';
                echo '<rect width="' . $barLength . '" height="19" style="fill:rgb(' . $color;
                echo ');stroke-width:0; stroke:rgb(0,0,0)"/> 
                       </svg>';
                echo '</div>';
                echo '</div></div><div><div class="d-inline-flex">';
                echo '<div class="d-inline-flex heatmap-width-holder-circles">';
                if (isset($heatmapBoxAllHolder[$i][$x+1])) {
                    $startValuePercent = $heatmapBoxAllHolder[$i][$x+1]->shortPercent;
                    $startDate = $heatmapBoxAllHolder[$i][$x+1]->date;
                }
            }
        }
        echo '</div>';
        echo '</div>';
        echo '<br>';
    }
?>

    </tbody>
  </table>
</div>
<?php 

$savelocation = '../short_data/html/';
$fileNameFirst = 'heatmap_';
$fileNameMiddle = $land;
$fileNameLast = '.html';

$filename = $savelocation . $fileNameFirst . $fileNameMiddle . $fileNameLast;
//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename, $htmlStr);
?>
