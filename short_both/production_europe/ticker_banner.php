 <?php 
 //denne filen behandler filer som er nedlastet
 //read Oslo børs header file    
 date_default_timezone_set('Europe/Oslo');
 require '../production_europe/functions.php';

 $stockexchanges = [];

 $array = ['name' => 'Frankfurt', 'ticker' => 'gdaxi'];
 $stockexchanges[] = $array;

 $array = ['name' => 'Stockholm', 'ticker' => 'omxspi'];
 $stockexchanges[] = $array;

 $array = ['name' => 'Oslo', 'ticker' => 'osebx'];
 $stockexchanges[] = $array;

 $array = ['name' => 'Copenhagen', 'ticker' => 'omxc20'];
 $stockexchanges[] = $array;

 $array = ['name' => 'Helsinki', 'ticker' => 'omxh25'];
 $stockexchanges[] = $array;



$today = date('Y-m-d');
$yesterday = date( "Y-m-d", strtotime("-1 Weekday", strtotime($today)));

echo 'Yesterday is ' . $yesterday . '<br>';

foreach ($stockexchanges as $key => $exchange)
{ 

//prepare data
$filename = '../short_data/json/ticker/' . $exchange['ticker'] . '.indx.json';

    if (!$data = readJSON($filename))
    {
        errorecho('could not read ' . $filename . '<br>');
        continue;
    }

    if ($exchange['ticker'] == 'osebx')
    {
        //getting yesterdays value

            if (!$osloyahoo = readJSON('../short_data/json/ticker/osebx.ol_yahoo.json'))
            {
                errorecho('could not read oslo history file<br>');
                continue;
            }


            if (!isset($osloyahoo['quoteResponse']['result']['0']['regularMarketPrice'])) {
                continue;
            }

            $latestClose = $osloyahoo['quoteResponse']['result']['0']['regularMarketPrice'];  
            $previousClose = $osloyahoo['quoteResponse']['result']['0']['regularMarketPreviousClose'];

            $change = $latestClose - $previousClose;
            echo 'Oslo change is: ' . $change . '. Close is ' . $latestClose ;

            $percent = (1-($previousClose/$latestClose))*100;
            echo '<br>Percent change is: ' . $percent ;

            $percent = round($percent ,2);

            $stockexchanges[$key]['percentchange'] = (string)number_format($percent,2,".",",");
            $stockexchanges[$key]['close'] = (string)number_format($latestClose,2,".",",") ;

    }
    else
    {
        if ($data['date'] == date('Y-m-d'))
        {
          $percent =  round($data['change_p'],2);
          $stockexchanges[$key]['percentchange'] = (string)number_format($percent,2,".",",");
          $close = round($data['close'],2);
          $stockexchanges[$key]['close'] = (string)number_format($close,2,".",",");
        }
        else
        {
          $stockexchanges[$key]['percentchange'] = '-';
          $stockexchanges[$key]['close'] = '-';

          errorecho('data not from today, keeping!<br>');
          var_dump($data);
        }

    }
    //var_dump($data);
    //$stockexchanges[$key]['change'];

}

var_dump($stockexchanges);

         

//timestamp
date_default_timezone_set('Europe/Oslo');
$now = date('l j. F H:i',strtotime("now"));
$now = strtolower($now);
ob_start();


?>
<div class="row banner-padding justify-content-center ">
    <div class="col-lg-8 border ">
    <div class="mt-1 "><h5 class=" text-center">Markets <?php echo $now; ?></h5></div>
  </div>
  <div class="row  justify-content-center">
    
  <?php 
foreach ($stockexchanges as $exchange)
{ 

    if ($exchange['percentchange'] > 0)
    {
      $hovedindeksen_switch = 'exchange-header-up';
    }
    else if ($exchange['percentchange'] == 0)
    {
      $hovedindeksen_switch = 'exchange-header-neutral';
    }
    else if ($exchange['percentchange'] < 0)
    {
      $hovedindeksen_switch = 'exchange-header-down';
    }    

    ?>
    <div class="text-center">
        <div class="py-0 pt-1 " data-toggle="tooltip" data-html="true" title="<?php echo strtoupper($exchange['ticker']); ?><br>Data is 15 minutes delayed">
            <span class="exchange-header <?php echo $hovedindeksen_switch ?>">
                <?php echo $exchange['percentchange'];   ?>%</span>
        </div>
            <div class="py-0 pb-1 px-2" >
                <span class="d-none d-sm-inline"> <strong><?php echo $exchange['name']; ?></strong> </span>
                <span class="d-inline d-sm-none exchange-index"> <strong><?php echo $exchange['name']; ?></strong> </span>
                <span class="exchange-index"><?php echo $exchange['close']; ?></span>
            </div>
        </div>

<?php } ?>
         

        </div>
    </div>

                 
<?php 
//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
echo 'Saving ' . '../short_data/html/banner_nordic.html<br>';
file_put_contents('../short_data/html/banner_nordic.html', $htmlStr);

?>
    
                    


