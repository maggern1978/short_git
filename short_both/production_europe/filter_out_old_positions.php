<?php

//$land = 'germany';

include '../production_europe/functions.php';

$inputFileName = '../short_data/dataraw/'. $land . '/' . $land . '_current.json';

if (!$data = readJSON($inputFileName))
{
	errorecho ('Read error of til ' . $inputFileName . '<br>');
}

$count = count($data);
$lastdate = date('Y-m-d', strtotime("-4 years"));


echo '<br>';
echo 'Inputfile is ' . $inputFileName . '<br>';
echo 'Searching for positions older than four years (' . $lastdate . ')<br>';

$unsettercounter = 0;
$total_count = 0; 

for ($i = 0; $i < $count; $i++)
{

	$posCount = count($data[$i]['Positions']);

	for ($x = 0; $x < $posCount; $x++)
	{
		if ($data[$i]['Positions'][$x]['ShortingDate'] < $lastdate)
		{
			echo $i . '. ' . $data[$i]['Name'] . ' Unsetting round: ' . $i . ' | Pos. no. ' . $x . '. Date is ' . $data[$i]['Positions'][$x]['ShortingDate']  . '<br>';
			unset($data[$i]['Positions'][$x]);
			$unsettercounter++;
		}
	}

	$data[$i]['Positions'] = array_values($data[$i]['Positions']);

	//slett posisjoner med null shortprosent
	$posCount = count($data[$i]['Positions']);

	for ($x = 0; $x < $posCount; $x++)
	{
		if ($data[$i]['Positions'][$x]['ShortPercent'] == 0)
		{
			echo $i . '. ' . $data[$i]['Name'] . ' Unsetting round: ' . $i . ' | Pos. no. ' . $x . '. Date is ' . $data[$i]['Positions'][$x]['ShortingDate']  . '. Reason: Zero shortpercent<br>';
			unset($data[$i]['Positions'][$x]);
			$unsettercounter++;
		}
	}

	$data[$i]['Positions'] = float_format(array_values($data[$i]['Positions'])); //to nuller etter tall

	//regn ut shortprosent og antall shortede aksjer for selskap
	$posCount = count($data[$i]['Positions']);

	$total_short_percent = 0; 
	$total_short_stocks = 0;

	for ($x = 0; $x < $posCount; $x++)
	{
		if(isset($data[$i]['Positions'][$x]['ShortPercent']))
		{
			$total_short_percent += $data[$i]['Positions'][$x]['ShortPercent'];
		}

		if (isset($data[$i]['Positions'][$x]['NetShortPosition']))
		{
			$total_short_stocks += $data[$i]['Positions'][$x]['NetShortPosition'];
		}
		
	}

	$data[$i]['ShortPercent'] = $total_short_percent;
	$data[$i]['ShortPercent'] = round($data[$i]['ShortPercent'],2);
    $data[$i]['ShortPercent'] = number_format($data[$i]['ShortPercent'],2,".","");

    $data[$i]['ShortedSum'] = $total_short_stocks;

	//tell opp antall posisjoner for hvert selskap igjen
	$posisjonsCount = count($data[$i]['Positions']);
	$data[$i]['NumPositions'] = $posisjonsCount;
	$total_count += $posisjonsCount;

	//slett selskap hvis null posisjoner igjen. 
	if ($posisjonsCount == 0) 
	{
		echo $i . '. ' . $data[$i]['Name'] . ' Zero positions, unsetting company.<br>';
		unset($data[$i]);
		continue;
	}

	//sett sist oppdatert i selskapsinfo til nyeste dato i posisjonene
	$posCount = count($data[$i]['Positions']);

	$newstdate = '2001-01-01';

	for ($x = 0; $x < $posCount; $x++)
	{
		if(isset($data[$i]['Positions'][$x]['ShortingDate']))
		{

			if ($newstdate < $data[$i]['Positions'][$x]['ShortingDate'])
			{
				$newstdate = $data[$i]['Positions'][$x]['ShortingDate'];
			}

		}
		
	}

	$data[$i]['LastChange'] = $newstdate;


	//sett selskapets isinnummer til alle posisjonene

	$posCount = count($data[$i]['Positions']);

	for ($x = 0; $x < $posCount; $x++)
	{
		if (isset($data[$i]['Positions'][$x]))
		{
			
			$data[$i]['Positions'][$x]['ISIN'] = $data[$i]['ISIN'];

		}
		
	}

	//sett selskapets navn til alle posisjonene

	$posCount = count($data[$i]['Positions']);

	for ($x = 0; $x < $posCount; $x++)
	{
		if (isset($data[$i]['Positions'][$x]))
		{
			
			$data[$i]['Positions'][$x]['Name'] = $data[$i]['Name'];

		}
		
	}


}



$data = array_values($data);

echo 'Notice: This file also recalculate shortpercent, shortedstock and numpositions for each company. Also fixes LastChange and sets same isin  and name on all companys positions. <br>';


echo 'Posisjoner ut: ' . $total_count . ' <br>';
successecho($unsettercounter . ' positions removed because of too old.<br>');

saveJSON($data, $inputFileName);

$inputFileName = '../short_data/dataraw/'. $land . '/' . date('Y-m-d.') . $land . '.json';
saveJSON($data, $inputFileName);


?>