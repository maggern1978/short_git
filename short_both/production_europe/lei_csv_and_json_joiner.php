<?php 

//https://api.gleif.org/api/v1/lei-records?page[size]=10&page[number]=1&filter[lei]=95980020140005558665
//https://documenter.getpostman.com/view/7679680/SVYrrxuU?version=latest
//https://www.gleif.org/en/lei-data/gleif-api

//https://api.gleif.org/api/v1/lei-records?filter%5Blei%5D=261700K5E45DJCF5Z735


//finding partens lei: https://api.gleif.org/api/v1/lei-records?filter%5Bowns%5D=95980020140005558665
//hvis ingen, så dette moderselskapet

include_once '../production_europe/functions.php';

$filename_csv = '../short_data/dataraw/spain/spain_current.csv';
$filename_json = '../short_data/json/lei_lookup/spain.json';


if (!$csv = readCSVkomma($filename_csv))
{
	errorecho('Could not reaad ' . $filename_csv);
}

if (!$json = readJSON($filename_json))
{
	errorecho('Could not reaad ' . $filename_json);
}



exit();



$result_box = [];

echo 'Searching for leis in database...<br>';

foreach ($data as $row) // download from lei database
{

	if (isset($row[2]))
	{

		if (!$lei_result = get_lei($row[2]))
		{
			echo "Could not download $row[2]";
			continue;
		}

		$result_box[] = $lei_result;

	}
}

echo 'Entries is now: ' . count($lei_result) . '<br>';


foreach ($result_box as $key => $lei_array)
{
	
	$name = $lei_array['data'][0]['attributes']['entity']['legalName']['name'];

	echo "$key. Trying isin link for $name: ";

	$result_box[$key]['isin_link'] = $lei_array['data'][0]['relationships']['isins']['links']['related'];

	if ($data =  json_decode(download($result_box[$key]['isin_link']), true))
	{
		$result_box[$key]['isin_data'] = $data;
		echo 'Download success for: ' . $result_box[$key]['isin_link'] . '<br>';
	}
	else
	{
		echo 'Download failed for ' . $result_box[$key]['isin_link'] . '<br>';
	}

}


//var_dump($result_box);

echo '<br>';
saveJSON($result_box, '../short_data/json/lei_lookup/spain.json');

function get_lei($lei)
{

	$url = 'https://api.gleif.org/api/v1/lei-records?filter%5Blei%5D=' . $lei;

	if ($data =  json_decode(download($url), true))
	{
		return $data;

	}
	else
	{
		return false; 
	}

}

?>