<?php

//set_time_limit(200);

//delete contents of file 


require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');
//$writer = new Xlsx($spreadsheet);
//$writer->save('hello world.xlsx');



$inputFileType = 'Xlsx';
if (file_exists('../short_data/dataraw/sweden.history/sweden.history_current.xlsx')) {
	$inputFileName = '../short_data/dataraw/sweden.history/sweden.history_current.xlsx';
}
else {
	echo 'File not found! ' . '../short_data/dataraw/sweden.history/sweden.history_current.xlsx';
}


//$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("dataraw/fi.se.history/fi.se.history_current.xlsx");

/**  Create a new Reader of the type defined in $inputFileType  **/

/**  Load $inputFileName to a Spreadsheet Object  **/
/**  Advise the Reader that we only want to load cell data  **/



/**  Define a Read Filter class implementing \PhpOffice\PhpSpreadsheet\Reader\IReadFilter  */
class MyReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($column, $row, $worksheetName = '') {
        //  Read rows 1 to 7 and columns A to E only
        if ($row >= 1 && $row <= 20000) {
            if (in_array($column,range('A','E'))) {
                return true;
            }
        }
        return false;
    }
}

/**  Create an Instance of our Read Filter  **/
$filterSubset = new MyReadFilter();

/**  Create a new Reader of the type defined in $inputFileType  **/
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
/**  Tell the Reader that we want to use the Read Filter  **/
$reader->setReadFilter($filterSubset);
/**  Load only the rows and columns that match our filter to Spreadsheet  **/
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load($inputFileName);

$writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);
$writer->save('../short_data/dataraw/sweden.history/sweden.history_current_temp.csv');

//echo 'Excelreader_history done!';

//under den gamle koden!





?>