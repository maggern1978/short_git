<?php

include_once '../production_europe/functions.php';

// First request for a sessionid
$ch = curl_init('http://www.cnmv.es/DocPortal/Posiciones-Cortas/NetShortPositions.xls');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Remove this line if you don't want to see the headers:
//curl_setopt($ch, CURLOPT_HEADER, 1);

$result = curl_exec($ch);

file_put_contents('../short_data/dataraw/spain/spain_active_and_historical_download.xls', $result);

curl_close ($ch);