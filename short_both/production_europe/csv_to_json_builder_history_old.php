<?php

//$land = 'finland';
//$land = 'france';
$land = 'sweden';
//$land = 'germany';

include('../production_europe/logger.php');
include('../production_europe/functions.php');
set_time_limit(3000);

//read date from generated file
$date = date('Y-m-d');

$millisecondsts1 = microtime(true);

echo '<br>';
echo date('Y-m-d H:i:s') . '<br>';

if ($land != 'france') //France has no active positions
{
	$csvlocation = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_merged.csv'; //source for the positions
}
else
{
	$csvlocation = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.csv'; //source for the positions. 
}

$fileCurrent = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.json'; //save
$filewithdate = '../short_data/dataraw/' . $land . '.history/' . $date . '.' . $land . '.history.json';

require '../production_europe/logger.php';

error_reporting(E_ALL);

if (!class_exists('selskap2')) {

	class selskap2 {
	  //Creating properties
		public $ISIN;
		public $Name;
		public $ShortPercent;
		public $ShortedSum;
		public $LastChange;
		public $NumPositions;
		public $currency;
		public $ticker;
		public $Positions;


	    //assigning values
		public function __construct($isin, $name, $shortpercent, $shortedsum, $lastchange, $numpositions, $positions, $currency, $ticker) 
		{
			$this->ISIN = $isin;
			$this->Name = $name;
			$this->ShortPercent = $shortpercent;
			$this->ShortedSum = $shortedsum;
			$this->LastChange = $lastchange;
			$this->NumPositions = $numpositions;
			$this->currency = $currency;
			$this->ticker = mb_strtolower(str_replace(" ", '-', $ticker));
			$this->Positions = $positions;


		}
	}
}

if (!class_exists('posisjon2')) {
	class posisjon2 {
	  //Creating properties
		public $ShortingDate;
		public $PositionHolder;
		public $ShortPercent;
		public $NetShortPosition;
		public $ISIN;
		public $Name;
		public $isActive;
		public $currency;
		public $ticker;
		
	    //public $Status;

	    //assigning values
		public function __construct($ShortingDate, $PositionHolder, $ShortPercent, $NetShortPosition, 
			$ISIN, $Name, $isActive, $currency, $ticker) 
		{

			$this->ShortingDate = $ShortingDate;
			$this->PositionHolder = $PositionHolder;
			$this->ShortPercent = $ShortPercent;
			$this->NetShortPosition = $NetShortPosition;
			$this->ISIN = $ISIN;
			$this->Name = $Name;
			$this->isActive = $isActive;
			$this->currency = $currency;
	        $this->ticker = mb_strtolower(str_replace(" ", '-', $ticker));
		}
	}
}

//les inn datane
//var_dump($csvdata[11]);
timestamp();
//gå igjennom slett der oppføringene ikke stemmer
$csvdata = readCSVkomma($csvlocation);
$count = count($csvdata);
$unsetcounter = 0;
echo 'Inputfile: ' . $csvlocation . '<br>';
echo 'Antall posisjoner inn: ' . count($csvdata) . '<br>';
timestamp();

//gå igjennom slett der oppføringene ikke stemmer
//samle inn datoer 
$dates_box = [];
$pos_counter = 0; 

$target = $csvdata[2][1]; //testing

for ($i = 0; $i < $count; $i++)
{

	$removed = 0;

	for ($x = 0; $x < 5; $x++)
	{
		//removing komma in company and player names
		if (!isset($csvdata[$i][$x]))
		{
			//errorecho('Data not set at ' . $i . ' and ' . $x . ' Unsettting<br> <br> ');
			//var_dump($csvdata[$i]);
			unset($csvdata[$i]);
			$unsetcounter++;
			$removed = 1;
			break;
		}
		else
		{
			$csvdata[$i][$x] = trim($csvdata[$i][$x]);
			$csvdata[$i][$x] = trim($csvdata[$i][$x] , "\t\n\r\0\x0B\xc2\xa0"); //whitespaces

		}

	}

	if ($removed == 0)
	{
		
		$csvdata[$i][0] = mb_strtolower($csvdata[$i][0]);
		$csvdata[$i][1] = mb_strtolower($csvdata[$i][1]);

		$csvdata[$i][0] = ucwords($csvdata[$i][0]);
		$csvdata[$i][1] = ucwords($csvdata[$i][1]);

		$csvdata[$i][0] = str_replace(",", "", $csvdata[$i][0]); //ta bort komma i navn
		$csvdata[$i][1] = str_replace(",", "", $csvdata[$i][1]); //tak vekk komma i navn
		$csvdata[$i][3] = str_replace(",", ".", $csvdata[$i][3]); //komma i tallene fra germany

		if($csvdata[$i][3] == '<0.5')  //if "<" is found, position is set to zero. Applies for for example sweden.
		{
			$csvdata[$i][3] = 0;
		}

		if ($csvdata[$i][3] < 0.50)  //under 0.50 is zero Applies for for Germany.
		{
			$csvdata[$i][3] = 0;
		}	

		$dates_box[] = trim($csvdata[$i][4]);	

		//dev
		

		if (mb_strtolower($target) == mb_strtolower($csvdata[$i][1]))
		{
			$pos_counter++;
		}

	}

}

echo $target . ' funnet ' . $pos_counter . ' ganger<br>';
echo 'Unsetting ' . $unsetcounter . ' positions for missing data<br>';


//lag alle posisjonene som objekt
if ($land == 'germany')
{
	unset($csvdata[0]);
	$csvdata = array_values($csvdata);
}

//make sure the positions are sorted by date
//sort by date

//$count = count($csvdata);
//echo 'Antall posisjoner inn: ' . count($csvdata) . '<br>';

//ta bort duplicater
//$csvdata = remove_duplicates_in_array($csvdata);
$csvdata_count = count($csvdata);
echo 'Posisjonscount A2 er ' . $csvdata_count . '<br>';
//echo 'Antallet posisjoner etter duplikatfjerning: ' . $csvdata_count . '<br>';
timestamp();

//ta dato for dato
$dates_box = array_unique($dates_box);
$dates_box = array_values($dates_box);
$dates_box_count = count($dates_box);




echo '<br>';
echo 'Cleaning duplicate positions:<br>';

$positions_box_all = [];
$hitcounter = 0; //make sure all positions are included
$removecounter_global = 0; 
$Hitcounter_foreach = 0;
$progress = 0;

for ($i = 0; $i < $dates_box_count; $i++)
{

	$targetdate = $dates_box[$i];
	$local_box = [];
	
	$hit_switch = 0; 


	for ($x = 0; $x < $csvdata_count; $x++)
	{

		if (!isset($csvdata[$x]))
		{
			continue;
		}

		if ($csvdata[$x][4] == $targetdate)
		{
			$local_box[] = $csvdata[$x];
			$hitcounter++;
		
		}
		else if ($csvdata[$x][4] != $targetdate and $hit_switch == 1)
		{
			//$progress = $progress - 3;
			break;
		}

	}

	$local_box_count = count($local_box);

	$round = 0; 

	$removecounter = 0; 

	for ($x = 0; $x < $local_box_count; $x++)
	{
		if (!isset($local_box[$x]))
		{
			continue;
		}

		$target_player = $local_box[$x][0];
		$target_company = $local_box[$x][1];
		$target_shortpercent = $local_box[$x][3];

		$found_itself_count = 0;

		for ($z = 0; $z < $local_box_count; $z++)
		{

			if (!isset($local_box[$z]))
			{
				continue;
			}

			if ($target_player == $local_box[$z][0] and $target_company == $local_box[$z][1] and $target_shortpercent == $local_box[$z][3] and $found_itself_count == 0)
			{
				//echo 'Found itself';
				$found_itself_count++;
				continue;
			
			}

			if ($target_player == $local_box[$z][0] and $target_company == $local_box[$z][1] and $found_itself_count > 0)
			{
				$found_itself_count++;

				if ($local_box[$z][3] != $local_box[$x][3]) 
				{
					//errorecho('$x er ' . $x . ' er duplikat, men IKKE LIK SHORTPROSENT! Den nederste i listen blir slettet.<br>');
				}
				else
				{
					//errorecho('$x er ' . $x . ' har duplikat. Den nederste i listen blir slettet.<br>');
				}

				//var_dump($local_box[$x]);
				//var_dump($local_box[$z]);
				//echo '-------------------------------<br>';
				unset($local_box[$z]);
				$removecounter++;
				$removecounter_global++;
				
			}			
			
		}

	
	}

	foreach ($local_box as $row)
	{
		$positions_box_all[] = $row;
		$Hitcounter_foreach++;
	}

	if ($removecounter > 0)
	{
		echo 'Date: ' . $dates_box[$i] . '. ';
		echo 'Antall posisjoner: ' . $local_box_count . '. ';
		successecho('Removed ' . $removecounter . ' positions. <br>');
	}
	else
	{
		//echo '<br>';
	}


	//echo 'Removecounter global er nå ' . $removecounter_global . '<br>';
	//echo 'Hitcounter er nå ' . $hitcounter . '<br>';
	//echo 'Hitcounter foreach er nå ' . $Hitcounter_foreach . '<br><br>';

}

$csvdata = $positions_box_all;

usort($csvdata, function ($item1, $item2) {
	return $item1[4] < $item2[4];
});


//usort($csvdata, function ($item1, $item2) {
//	return $item1[4] < $item2[4];
//});

$csvdatacount = count($csvdata);

echo 'Posisjonscount er ' . $csvdatacount . '<br>';

$pos_counter = 0; 

for ($i = 0; $i < $csvdatacount; $i++)
{
		//dev
		if (mb_strtolower($target) == mb_strtolower($csvdata[$i][1]))
		{
			$pos_counter++;
		}
}

echo $target . ' funnet ' . $pos_counter . ' ganger<br>';


//$used_list_companies = array();
$used_list_isin = array();
$used_list_companies = array();
$posisjoner = array();

//finn sist oppdatert ved å ta første dato fra fi.se_current.csv
$last_updated = $csvdata[0][4];

echo '<br>';
echo 'Last updated (nyeste posisjon oppgitt fra myndigheter): ' . $last_updated;

$notFoundCounter = 0; 


//echo 'loop starter her!';

$notFoundArray = [];

$name_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );
$miss_box = [];
$miss_box_outstanding = [];

for ($i = 0; $i < $csvdatacount; $i++)
{

	if (!isset($csvdata[$i]))
	{
		continue;
	}

	$position = $csvdata[$i];
	$key = $i;

	//bygger to arrays med alle børsselskaper og isin, som senere brukes til å lage selskapslisten
	$used_list_companies[] = mb_strtoupper($position[1]);
	$used_list_isin[] = mb_strtoupper($position[2]);
	
	//finner isin, alternativt navn, ticker i isinlisten
	$targetname = mb_strtolower($position[1]);
	$targetisin = $position[2];
	$found_in_isin_list = 0;

	//echo 'Targetname er "'  . $targetname . '"<br>';

	//function binary_search_multiple_hits ($isin, $name, $isin_sorted_list, $name_sorted_list, $land)
	//$firstrow = binary_search_isinliste($name, $name_liste);
	if ($row = binary_search_multiple_hits($targetisin, $targetname, $isin_list, $name_list, $land))
	{
		$altname = $row[2];
		$currency = $row[3];
		$tickerselskap = $row[1] . '.' . $row[8];
		$found_in_isin_list = 1;

		if (isset($row[9]) and $row[9] != 0)
		{
			$number_of_stocks = (float)$row[9] * ((float)$position[3]/100);
			$number_of_stocks = round($number_of_stocks,0);
			$success = 1;
		}
		else
		{
			$obj = new stdClass;
			$obj->name = $position[1];
			$obj->ISIN = $position[2];
			$miss_box_outstanding[] = (array)$obj;			
			continue;
		}
	}
	else
	{
		$obj = new stdClass;
		$obj->name = $position[1];
		$obj->ISIN = $position[2];
		$miss_box[] = (array)$obj;
		continue;
	}

	if ($success == 1) 
	{

		$date_temp = $position[4];
		$date_temp = str_replace('/','-',$date_temp,$count);

		$active = 'no';

		if (isset($position[5])) 
		{
			if ($position[5] =='active') 
			{
				$active = 'yes';
			}
		}

		$posisjoner[] = new posisjon2($date_temp , $position[0], $position[3], $number_of_stocks, $position[2], $position[1], $active, $currency,$tickerselskap);
	}

}


//var_dump($miss_box_outstanding);
echo '<br>';
$miss_box_outstanding = remove_duplicates_in_array($miss_box_outstanding);

echo '<br>Outstanding shares misses:<br>';
$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
$file = fopen($filename,"a");
foreach ($miss_box_outstanding as $key => $position)
{
	fputs($file, "\n" . $position['name']);
	echo $key . '. ' . $position['name'] . ' ' . $position['ISIN']  . '. Adding to misslist.csv ';
	$position['land'] = 'france';
	saveJSON($position, '../production_europe/isin_adder/misslist/outstanding@' . $land . '@' . $position['name'] .  '.json');
}
fclose($file);

echo '<br>';
$miss_box = remove_duplicates_in_array($miss_box);

echo 'Isin / name misses:<br>';
$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
$file = fopen($filename,"a");
foreach ($miss_box as $key => $position)
{
	fputs($file, "\n" . $position['name']);
	echo $key . '. ' . $position['name'] . ' ' . $position['ISIN']  . '. Adding to misslist.csv ';
	$position['land'] = 'france';
	saveJSON($position, '../production_europe/isin_adder/misslist/isin@' . $land . '@' . $position['name'] .  '.json');

}
fclose($file);


$used_list_isin = array_unique($used_list_isin);
$used_list_companies = array_unique($used_list_companies);



$used_list_companies = array_values($used_list_companies);

$selskaper = array();
$posisjonsholder_unik = $used_list_companies;
$poskopi = $posisjoner;
$maincounter = 0;

//behandle hver unike navn
foreach ($used_list_isin as $key => $posisjon) {
	//build company 

	//Finn isin
	$posisjonsholder = array();

	//ta player-navnet

	$name ='';
	$counter = 0;
	$last_updated;
	$isin = '';
	$selskapnavnet = '';
	foreach ($posisjoner as $index => $pos)  {

		//var_dump($pos);
		if (strtoupper($pos->ISIN) == strtoupper($posisjon)) {

			//echo ' hit! ' . $counter . ' ' . $selskapnavnet . ' ' ;
			$name = $pos->Name;
			$isin = $pos->ISIN;
			$currency = $pos->currency;
			//echo $isin . '<br>';
			$posisjonsholder[] = $pos;	
			$counter++;
			$selskapnavnet = $name;
		}

		if ($counter == 0) {
			$last_updated = $pos->ShortingDate;

		}

	}

	$maincounter += $counter;

	//ECHO '<BR>Hitcounter: ' . $maincounter . '<br>';
	$antall_posisjoner = count($posisjonsholder);

	//Nå har vi alle posisjoner, og må regne ut summen av alle prosentene og av alle aksjene.
	(float)$accumulated_short_percent = '0';
	(float)$accumulated_short_stocks = '0';

	$ticker = '';

	foreach ($posisjonsholder as $posisjon) 
	{
		if ($posisjon->isActive == 'yes')
		{
			(float)$accumulated_short_percent += (float)$posisjon->ShortPercent;
			(float)$accumulated_short_stocks += (float)$posisjon->NetShortPosition;
		}
		$ticker = $posisjon->ticker;
	}

	//Skal selskap og posisjoner, men bare hvis det er shorting i selskapet, og bare aktive posisjoner innad i selskapet
	$selskaper[] = new selskap2($isin, $selskapnavnet, $accumulated_short_percent, $accumulated_short_stocks, $last_updated, $antall_posisjoner, $posisjonsholder, $currency, $ticker);

}

//LAG UNNTAK
//remove positions with zero positions

$count = count($selskaper);
$deletecounter = 0;

for ($i = 0; $i < $count; $i++)
{
	//var_dump($selskaper[$i]);

	$positionscount = count($selskaper[$i]->Positions);

	if (!$positionscount > 0)
	{
				//echo $i . '. Deleting position for zero positions<br>.';
		$deletecounter++;
		unset($selskaper[$i]);
	}
}

echo $deletecounter . ' companies deleted for zero positions..<br>';

$selskaper = array_values($selskaper);

//Gå igjennom og sørg for at alle navnene under samme isin har samme navn
//navnet i den første posisjonen blir brukt
$totalcount = 0;
foreach ($selskaper as $key => $selskap) {
	$name = $selskap->Positions[0]->Name;
	$selskap->Name = $name;

	foreach ($selskap->Positions as $position) {
		$position->Name = $name;
		$totalcount++;
	}
}

echo 'Antall posisjoner ut er ' . $totalcount . '<br>';

//write current
$fp = fopen($fileCurrent, 'w');
fwrite($fp, json_encode($selskaper, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
fclose($fp);
echo date('Y-m-d H:i:s') . '<br>';

//$ts2 = strtotime("now");     
//$seconds = $ts2 - $ts1;                            
//echo 'Runtime is ' . $seconds . '<br>';

$millisecondsts2 = microtime(true);
$milliseconds_diff = ($millisecondsts2 - $millisecondsts1);  
echo 'Runtime in seconds is ' . $milliseconds_diff . '<br>';

print_mem();

//memory things
$tickerliste_isin = [];
$tickerliste_navn = [];
$selskaper = 0;
$csvdata = 0;
$outstanding_navn = 0;
$outstanding_isin = 0; 	
$posisjoner = 0;	
?>