<?php 

        $string = file_get_contents('../short_data/dataraw/currency/NOKUSD_current.json');
        $dollar = json_decode($string, true); 

        //find last update time
        if (isset($dollar['Meta Data'])) {
            $timestamp = $dollar['Meta Data']['4. Last Refreshed'];
            $timestamp = date('Y-m-d H:i', strtotime($timestamp.'+1 hour'));
        }
        else {
            $timestamp = '';
        }

        //dollar
        if (isset($dollar['Time Series FX (15min)'])) {
            $properties = array_keys($dollar['Time Series FX (15min)']);
            $first_key = $properties[0];

            //use keys to get close values from the json data
            $first_value = $dollar['Time Series FX (15min)'][$first_key]['4. close'];
            

        }
        else {
           $first_value = ''; 
        }

        $string = file_get_contents('../short_data/dataraw/currency/SEKUSD_current.json');
        $sekdollar = json_decode($string, true); 
        
        //find last update time
        if (isset($sekdollar['Meta Data'])) {
            $timestamp = $sekdollar['Meta Data']['4. Last Refreshed'];
            $timestamp = date('Y-m-d H:i', strtotime($timestamp.'+1 hour'));
        }
        else {
            $timestamp = '';
        }

        if (isset($sekdollar['Time Series FX (15min)'])) {
            $properties = array_keys($sekdollar['Time Series FX (15min)']);
            $sek_first_key = $properties[0];
            $sek_first_value = $sekdollar['Time Series FX (15min)'][$sek_first_key]['4. close'];
            
        }
        else {
           $sek_first_value = ''; 
        }
   
        $string = file_get_contents('../short_data/dataraw/currency/DKKUSD_current.json');
        $dkkdollar = json_decode($string, true); 

        //find last update time
        if (isset($dkkdollar['Meta Data'])) {
            $timestamp = $dkkdollar['Meta Data']['4. Last Refreshed'];
            $timestamp = date('Y-m-d H:i', strtotime($timestamp.'+1 hour'));
        }
        else {
            $timestamp = '';
        }

        if (isset($dkkdollar['Time Series FX (15min)'])) {
            $properties = array_keys($dkkdollar['Time Series FX (15min)']);
            $dkk_first_key = $properties[0];
            $dkk_first_value = $dkkdollar['Time Series FX (15min)'][$dkk_first_key]['4. close'];
            
        }
        else {
           $dkk_first_value = ''; 
        }
   
        $string = file_get_contents('../short_data/dataraw/currency/EURUSD_current.json');
        $eurodollar = json_decode($string, true); 
        //find last update time
        if (isset($eurodollar['Meta Data'])) {
            $timestamp = $eurodollar['Meta Data']['4. Last Refreshed'];
            $timestamp = date('Y-m-d H:i', strtotime($timestamp.'+1 hour'));
        }
        else {
            $timestamp = '';
        }
        if (isset($eurodollar['Time Series FX (15min)'])) {
            $properties = array_keys($eurodollar['Time Series FX (15min)']);
            $euro_first_key = $properties[0];
            $euro_first_value = $eurodollar['Time Series FX (15min)'][$euro_first_key]['4. close'];
            
        }
        else {
           $euro_first_value = ''; 
        }

$filenameNor = '../short_data/datacalc/nokkeltall/nokkeltall_norway.current.csv';
$filenameSwe = '../short_data/datacalc/nokkeltall/nokkeltall_sweden.current.csv';
$filenameDkk = '../short_data/datacalc/nokkeltall/nokkeltall_denmark.current.csv';
$filenameFin = '../short_data/datacalc/nokkeltall/nokkeltall_finland.current.csv';
$filenameGer = '../short_data/datacalc/nokkeltall/nokkeltall_germany.current.csv';

$NOKUSD = readcsvfile($filenameNor);
$SEKUSD = readcsvfile($filenameSwe);
$DKKUSD = readcsvfile($filenameDkk);
$FINUSD = readcsvfile($filenameFin);
$GERUSD = readcsvfile($filenameGer);

$USDSUM = ($NOKUSD[0]/$first_value) + ($SEKUSD[0]/$sek_first_value) + ($DKKUSD[0]/$dkk_first_value) + ($FINUSD[0]/$euro_first_value) + ($GERUSD[0]/$euro_first_value);
$EUROSUM = $USDSUM/(1/$euro_first_value);

$numberOfPositions = $NOKUSD[3] + $SEKUSD[3] + $DKKUSD[3] + $FINUSD[3] + $GERUSD[3];
$numberOfCompanies = $NOKUSD[4] + $SEKUSD[4] + $DKKUSD[4] + $FINUSD[4] + $GERUSD[4];

//total markedscamp

$USDMcapSum = ($NOKUSD[1]/$first_value) + ($SEKUSD[1]/$sek_first_value) + ($DKKUSD[1]/$dkk_first_value) + ($FINUSD[1]/$euro_first_value) + ($GERUSD[1]/$euro_first_value);
$EUROMcapSum = $USDMcapSum/(1/$euro_first_value);

$previousMcap = 0;
$percentage = 0;

if ($USDSUM > 0) {
  $previousMcap = $USDMcapSum - $USDSUM;
  $percentage = ($USDMcapSum/$previousMcap)-1; // is positive
}
else {
  $previousMcap = $USDMcapSum + abs($USDSUM);
  $percentage = 1-($USDMcapSum/$previousMcap);
}

$percentage = round($percentage * 100,2);

//rounding and make sum into billions
$USDSUM = round($USDSUM,2);
$EUROSUM = round($EUROSUM,2);

$USDMcapSum = $USDMcapSum/1000;
$EUROMcapSum = $EUROMcapSum/1000;
$USDMcapSum = round($USDMcapSum,3);
$EUROMcapSum = round($EUROMcapSum,3);


//sett plus minus sign
$sign = '';
$sayer = '';
if ($USDSUM > 0) {
  $sign = '+';
  $sayer = 'earned';
  $color = 'bg-success';
}
else {
  $sign = '';
  $sayer = 'lost';
  $color = 'bg-dark';
}



function readcsvfile($filename) {
  $csvFile = file($filename) or die('could not read nokkeltall file!');            
  $data = [];
  foreach ($csvFile as $key => $line) {
      if ($key == 0) {
        continue;
      }
      $data[] = str_getcsv($line);
  }

  $newArray = array();
  foreach ($data[0] as $input) {
       $newArray[] = $input;
  }

  return $newArray; 
}

ob_start();
?>

<div class="container">
 <div class="row banner-padding">
    <div class="col-12 badge badge-secondary badge-summary">
      Aggregated summary all short positions:
     </div>
  </div>
</div>   
<div class="container">
 <div class="row banner-padding justify-content-center">
    <div class="col-12 col-lg-8 rounded border <?php echo $color;?>">
      <div class="mt-1">
        <h3 class="text-center text-white title-summary" data-html="true" 
        data-toggle="tooltip" 
        title="
        <?php echo 'In euros: ' . $EUROSUM . ' million';?>
        <?php echo '<br>In NOK: ' . round($USDSUM *  $first_value,2) . ' million';?>
        <?php echo '<br>In SEK: ' . round($USDSUM * $sek_first_value,2) . ' million';?>
        <?php echo '<br>In DKK: ' . round($USDSUM * $dkk_first_value,2) . ' million';?>
        ">
          <?php echo $sign . $USDSUM . ' million USD ' . $sayer . ' (' . $percentage . '%)';?> </span>
        </h3>
      </div>
    </div>
  </div>
</div>
<div class="container">
 <div class="row banner-padding">
    <div class="col-12 summary-small-text " 
    data-html="true" data-toggle="tooltip" title="
    <?php  echo 'In euros: ' . $EUROMcapSum . ' billion';?>
    <?php echo '<br>In NOK: ' . round($USDMcapSum * $first_value,2) . ' billion';?>
    <?php echo '<br>In SEK: ' . round($USDMcapSum * $sek_first_value,2) . ' billion';?>
    <?php echo '<br>In DKK: ' . round($USDMcapSum * $dkk_first_value,2) . ' billion';?>
    ">
      Total short positions value: <?php echo $USDMcapSum; ?> billion USD<br> <?php echo $numberOfPositions; ?> positions in <?php echo $numberOfCompanies; ?> companies

        
     </div>
  </div>
</div>
<?php 
//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents('../short_data/summary.html', $htmlStr);
?>