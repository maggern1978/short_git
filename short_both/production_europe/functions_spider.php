<?php


function trimmer($input)
{
	return trim($input , "\t\n\r\0\x0B\xc2\xa0"); //whitespaces
}


function scanner($csv_array, $name, $isin, $date)
{

	$spider = new spider();
	$spider->add_name($name, $date);
	$spider->add_isin($isin, $date);

	$new = 1;
	$round = 0; 
	$csv_array_count = count($csv_array);

	while ($new > 0)
	{
		$savearraycount_pre = $spider->count_isins() + $spider->count_names();

		for ($i = 0; $i < $csv_array_count; $i++)
		{

			$row = $csv_array[$i];

			if ($spider->in_isin_array($row[2]))
			{
				//echo 'Fant ' . $row[2] . ' | ';

				if (!$spider->in_name_array($row[1]))
				{
					$spider->add_name($row[1], $row[4]);
				}

			}

			if ($spider->in_name_array($row[1]))
			{
				//echo 'Fant ' . $row[1] . ' | ';

				if (!$spider->in_isin_array($row[2]))
				{
					$spider->add_isin($row[2], $row[4]);
				}
			}

		}

		$savearraycount_post = $spider->count_isins() + $spider->count_names();
		$new = $savearraycount_post - $savearraycount_pre;
	}

	return $spider;

}

//var_dump(scanner($csvdata, $csvdata[0][1], $csvdata[0][2]));

class spider
{
	// Properties
	public $name_object_list = [];
	public $isin_object_list = [];
	public $isin_hit = false;
	public $name_hit = false;
  
  	// Methods
	function add_name($name, $date) 
	{
		if (!in_array(mb_strtolower($this->trim_whitespace($name)), $this->get_name_list()))
		{
			$array = ["name" => mb_strtolower($this->trim_whitespace($name)), "date" => $this->trim_whitespace($date)];
			$this->name_object_list[] = $array;
			$this->sort_names_by_date();
		}
	}

	function add_isin($isin, $date) 
	{
		if (!in_array($this->trim_whitespace($isin), $this->get_isin_list()))
		{
			$array = ["isin" => $this->trim_whitespace($isin), "date" => $this->trim_whitespace($date)];
			$this->isin_object_list[] = $array;
			$this->sort_isins_by_date();
		}
	}

	function trim_whitespace($input)
	{
		return trim($input , "\t\n\r\0\x0B\xc2\xa0"); //whitespaces
	}

	function set_hit_isin($input)
	{
		$this->isin_hit = $this->trim_whitespace($input);
	}

	function set_hit_name($input)
	{
		$this->name_hit = mb_strtolower($this->trim_whitespace($input));
	}

	//retrival
	function get_hit_isin()
	{
		return $this->isin_hit;
	}

	function get_hit_name()
	{
		return $this->name_hit;
	}


	function get_name_list() 
	{
		$count = count($this->name_object_list);
		$temp_array = [];

		for ($i = 0; $i < $count; $i++)
		{
			$temp_array[] = $this->name_object_list[$i]['name'];
		}

		return $temp_array;
	}

	function get_isin_list() 
	{

		$count = count($this->isin_object_list);
		$temp_array = [];

		for ($i = 0; $i < $count; $i++)
		{
			$temp_array[] = $this->isin_object_list[$i]['isin'];
		}

		return $temp_array;
	}	


	function get_first_isin() 
	{
		
		if (isset($this->isin_object_list[0]['isin']))
		{
			return $this->isin_object_list[0]['isin'];
		}
		else
		{
			return false;
		}
	}

	function get_first_name() 
	{
		
		if (isset($this->name_object_list[0]['name']))
		{
			return $this->name_object_list[0]['name'];
		}
		else
		{
			return false;
		}
	}

	function export() 
	{
		$array = ["namelist" => $this->name_object_list, "isinlist" => $this->isin_object_list];
		return $array;
	}

	//queries
	function in_isin_array($isin) 
	{
		if (in_array($this->trim_whitespace($isin), $this->get_isin_list()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function in_name_array($name) 
	{
		if (in_array(mb_strtolower($this->trim_whitespace($name)), $this->get_name_list()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function count_names() 
	{
		if ($count = count($this->get_name_list()))
		{
			return $count;
		}
		else
		{
			return 0;
		}
		
	}

	function count_isins() 
	{
		if ($count = count($this->get_isin_list()))
		{
			return $count;
		}
		else
		{
			return 0;
		}
	}

	function sort_isins_by_date()
	{
		usort($this->isin_object_list, function ($item1, $item2) 
		{
			return $item1['date'] < $item2['date'];
		});
	}

	function sort_names_by_date()
	{
		usort($this->name_object_list, function ($item1, $item2) 
		{
			return $item1['date'] < $item2['date'];
		});
	}
}

function deploy_spider($land) 
{
	if (!$spiderdata = readJSON('../short_data/json/isin_spider/' . $land . '.spider.json'))
	{
		errorecho('Could not deploy and read spider json!<br>');
		return false;	
	}

	$spiderweb = [];
	$spiderweb['spider'] = $spiderdata;

	foreach ($spiderdata as $key => $entry)
	{

		$isin_hit = $entry['isin_hit'];

		foreach ($entry['isin_object_list'] as $isin)
		{
			$spiderweb['deployed'][$isin['isin']] = $isin_hit;
		}

		foreach ($entry['name_object_list'] as $name)
		{
			$spiderweb['deployed'][$name['name']] = $isin_hit;
		}
	}

	return $spiderweb;

}

function search_spider($spider, $targetisin, $targetname)
{

	if ($spider == false)
	{
		return false;
	}

	if (isset($spider['deployed'][$targetisin]))
	{
		return $spider['deployed'][$targetisin];
	}

	if (isset($spider['deployed'][mb_strtolower($targetname)]))
	{
		return $spider['deployed'][$spider['deployed'][mb_strtolower($targetname)]];
	}

	return false;
}


if (!function_exists('sanetize_csv_data')) 
{
	function sanetize_csv_data($csvdata, $land)
	{

		$csvdata_count = count($csvdata);

		for ($i = 0; $i < $csvdata_count; $i++)
		{

			$removed = 0;

			for ($x = 0; $x < 5; $x++)
			{
			//removing komma in company and player names

				if (!isset($csvdata[$i][$x]))
				{
					//errorecho('Data not set at ' . $i . ' and ' . $x . ' Unsettting<br> <br> ');
					unset($csvdata[$i]);
					$removed = 1;
					break;
				}

				$csvdata[$i][$x] = trim($csvdata[$i][$x]);
			$csvdata[$i][$x]  = trim($csvdata[$i][$x] , " \t\n\r\0\x0B\xc2\xa0"); //whitespaces

		}

		if ($removed == 0)
		{
			$csvdata[$i][0] = str_replace(",", "", $csvdata[$i][0]);
			$csvdata[$i][1] = str_replace(",", "", $csvdata[$i][1]);
		}
	}

	//finding duplicate values
	$csvdata = array_values($csvdata);


	if ($land == 'germany')
	{
		
		//clean the data
		$count = count($csvdata);
		for ($i = 1; $i < $count; $i++) {

			//fixing the komma in number
			$csvdata[$i][3] = str_replace(",", ".", $csvdata[$i][3]);
		}
		unset($csvdata[0]);
		$csvdata = array_values($csvdata);
	}

	return $csvdata;
}
}

?>