<?php 

include_once '../production_europe/functions.php';

$countries = get_countries();

$activepositions = 0;
$activecompanies = 0; 

$allplayersnamescontainer = [];

//doing active positions
foreach ($countries as $key => $country)
{

	if (!$data = readJSON('../short_data/dataraw/' . $country . '/' . $country . '_current.json'))
	{
		echo 'Error reading, will skip.<br>';
		continue;
	}

	$activecompanies += count($data);

	echo 'After ' . $country . ': ' . $activecompanies . 'selskaper. Posisjoner: ';

	foreach ($data as $index => $company)
	{
		
		foreach ($company['Positions'] as $position)
		{
			$allplayersnamescontainer[] = mb_strtolower($position['PositionHolder']);

			if ($position['isActive'] == 'yes')
			{
				$activepositions++;
			}
		}
	}

	echo $activepositions . '<br>';
}

$allplayersnamescontainer = array_unique($allplayersnamescontainer);
$allplayersnamescontainercount = count($allplayersnamescontainer);
$numberofcountries = count($countries);

//historical
$totalplayersall = 0;
$totalcompaniesall = 0;
$totalpositionsall = 0;

$allplayersnamescontainerhistory = [];

foreach ($countries as $key => $country)
{
	//echo $country . '<br>';
	$dir = '../short_data/dataraw/stock.prices/' . $country . '/';
	$companiesindex =  $dir . 'companies/companies.index.json';
	$playersindex = $dir . 'players/players.index.json';

	//companies
	if (!$data = readJSON($companiesindex))
	{
		echo 'Error reading json index companies ' . $country . '<br>';
		continue;
	}

	$totalcompaniesall += count($data);

	foreach ($data as $index => $entry)
	{
		$totalpositionsall += $entry['numPositions'];
	}



	//players
	if (!$data = readJSON($playersindex))
	{
		echo 'Error reading json index players ' . $country . '<br>';
		continue;
	}

	foreach ($data as $index => $entry)
	{
		$allplayersnamescontainerhistory[] = $entry['PositionHolder'];
	}

}

$allplayersnamescontainerhistory = array_unique($allplayersnamescontainerhistory);
$allplayersnamescontainerhistorycount = count($allplayersnamescontainerhistory);

$activepositions = number_format_heltall($activepositions);
$allplayersnamescontainercount = number_format_heltall($allplayersnamescontainercount);
$activecompanies = number_format_heltall($activecompanies);

$totalpositionsall = number_format_heltall($totalpositionsall);
$totalcompaniesall = number_format_heltall($totalcompaniesall);
$allplayersnamescontainerhistorycount = number_format_heltall($allplayersnamescontainerhistorycount);


ob_start();
echo '<div class="d-none d-sm-none d-md-block">';
echo "<span class='font-weight-bold'>Tracking $activepositions active short positions in $activecompanies companies by $allplayersnamescontainercount players in $numberofcountries countries.</span><br> ";
echo "See also $totalpositionsall historical positions in $totalcompaniesall companies by $allplayersnamescontainerhistorycount players.";
echo '</div>';

echo '<div class="d-block d-sm-block d-md-none">';
echo "<span class='font-weight-bold'>Tracking $activepositions active short positions in $activecompanies companies by $allplayersnamescontainercount players in $numberofcountries countries.</span>";
echo "See also $totalpositionsall historical positions in $totalcompaniesall companies by $allplayersnamescontainerhistorycount players.";
echo '</div>';

$htmlStr = ob_get_contents();
ob_end_clean(); 
// Write final string to file
$filename = '../short_data/html/summary_positions_tracked.html';

file_put_contents($filename, $htmlStr);

?>

