<?php //include 'header.html'; 

//$land = 'sweden';
//$land = 'united_kingdom';

set_time_limit(25);

if (!function_exists('sortByOrder')) {
    function sortByOrder($a, $b) {
        return $a['positionValueChangeSinceStart'] < $b['positionValueChangeSinceStart'];
    }
}

require '../production_europe/logger.php';
require '../production_europe/namelink.php';
require '../production_europe/functions.php';

if (!function_exists('getcolor')) {
    function getcolor($inputvalue) {

        if ($inputvalue == NULL or '') {
            return '';
        }

        if ($inputvalue > 0 ) {
            return 'exchange-header-up';
        }
        elseif ($inputvalue  == 0) {
            return 'exchange-header-neutral';   
        }
        elseif ($inputvalue  < 0) {
            return 'exchange-header-down';          
        }
    }
}

if (!function_exists('getbackgroundcolor')) {
    function getbackgroundcolor($inputvalue) {

        if ($inputvalue > 0 ) {
            return 'background-color-green';
        }
        elseif ($inputvalue  == 0) {
            return 'background-color-neutral';   
        }
        elseif ($inputvalue  < 0) {
            return 'background-color-red';          
        }
    }
}


include '../production_europe/options_set_currency_by_land.php';

$playerposistionLocation = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
$shortliste = '../short_data/datacalc/shortlisten_' . $land . '_current.csv';
$landname = ucwords($land);

$json = file_get_contents($playerposistionLocation);
$json = json_decode($json, true);

//gå igjennom alle posisjoner og legg i box

$jsonCount = count($json);

if ($jsonCount == 0) {
    //echo 'Error! returning. Json count is zero';
    return;
}

//echo 'jsoncount er ' . $jsonCount;

$positionBox = [];
//var_dump($json[0]);
//var_dump($json[0]['positions'][0]);

for ($i = 0; $jsonCount > $i; $i++) {   

    foreach ($json[$i]['positions'] as $key => $position) {

        if (!isset($position['positionValueChangeSinceStart']))
        {
            echo 'Round ' . $i . '. ';
            errorecho('positionValueChangeSinceStart not set');
            var_dump($position);
            unset($json[$i]['positions'][$key]);
            $json[$i]['positions'] = array_values($json[$i]['positions']);
            continue; 
        }

        if ($position['positionValueChangeSinceStart'] == '') 
        {
         continue;
        }
        else 
        {
            $tempPos = $position;
        }

    $tempPos['playerName'] = $json[$i]['playerName'];
    $positionBox[] = $tempPos;

    }
    //echo '<br>';
}

$positionBoxCount = count($positionBox);

//sort by name
usort($positionBox, function($a, $b) {
    return $a['positionValueChangeSinceStartBaseCurrency'] < $b['positionValueChangeSinceStartBaseCurrency'];
});

?>

    <?php  ob_start();?>

    <table class="table table-bordered table-sm table-padding-bottom">
    <thead class="thead-dark">
      <tr>
        <th>Player and company</th>
        <th class="text-right">Change</th>
      </tr>
    </thead>
    <tbody>

    <?php 

    for ($i = 0; 4 > $i; $i++) {
        
        if (!isset($i))
        {
            return;
        }
        $index = $i + 1 ;

        if (!isset($positionBox[$i]['playerName']) or !isset($positionBox[$i]['companyName']))
        {
            return;
        }

        $linkplayer = nametolink($positionBox[$i]['playerName']);
        $linkcompany = nametolink($positionBox[$i]['companyName']);
    
    $millions = (float)$positionBox[$i]['positionValueChangeSinceStart'] /1000000;
    echo '<tr>';
    echo '<td class="">';
    echo '<a class="most-earning-text" data-toggle="tooltip" title="See all active positions for player" href="';
    echo 'details.php?player=' . $linkplayer . '&land=' . $land;
    echo '">';
    $name = strtolower($positionBox[$i]['playerName']);
    echo ucwords($name);
    echo '</a>'; 
    echo ' in <span class="font-weight-bold">';
    echo '<a class="most-earning-text" data-toggle="tooltip" title="See all active positions for company" href="';
    echo 'details_company.php?company=' . $linkcompany . '&land=' . $land;
    echo '">';
    $totalValueChangePercent = (1 - ($positionBox[$i]['stockPrice']/$positionBox[$i]['startStockPrice']))*100;
    echo ucwords($positionBox[$i]['companyName']);
    echo '</a>';
    echo '</span></td>';  

    $bet =  ($positionBox[$i]['startStockPrice'] * $positionBox[$i]['numberOfStocks'])/1000000;
    $bet = round($bet,2);

    echo '<td class="text-right" style="min-width: 78px;" data-toggle="tooltip" title="';
    if ($millions > 0) {
        echo '+';
    }
    echo round($totalValueChangePercent,2) . '%. ';
    echo 'Position startet ' . $positionBox[$i]['positionStartDate'] . ' at price ' . $positionBox[$i]['startStockPrice'];
    echo ' ' . $positionBox[$i]['currency'] ;
    echo ' (Latest price: ' . round($positionBox[$i]['stockPrice'],2) . '). ';

    echo '">';
    //echo ' (' . round($totalValueChangePercent,2) . '%)';
    if ($millions > 0) {
        echo '+';
    }
    echo round($millions,2) . '&nbsp;M ' . $positionBox[$i]['currency'];


    
    echo '</td>'; 


    }
     ?>
      </tbody>
      </table>
    <?php

$filenameStart = '../short_data/html/positionrank_';
$filenameMiddle = $land;
$filenameEnd = '.html';
$filename = $filenameStart . $filenameMiddle . $filenameEnd;


//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename, $htmlStr);

    ?>
   

  <?php //include 'footer.php'; ?>
  <?php //include 'footer_about.html'; ?>
