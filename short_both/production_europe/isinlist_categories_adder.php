<?php 

//This is based on the main isin list in dataCalc/isin/
//sets industri, secotr and currency if available
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include 'functions.php';
set_time_limit(4000);
	
$filename = '../short_data/dataraw/isin/hovedlisten_landkoder.csv';

$list = readCSV($filename);


foreach ($list as $key => $row)
{
	if ($key == 0)
	{
		continue;
	}
		
	$ticker = $row[1] . '.' . $row[8];
	$ticker = str_replace(' ', '-', $ticker);
	$url = '../short_data/json/fundamentals/' . $ticker . '.json';
	$url = strtolower($url);

	echo $key . '. Reading ' . $url . '. ';
	echo $row[2] . ' ' . $ticker . '. ';


	if (!file_exists($url))
	{
		errorecho('File not found, continuing<br>');
		continue;
	}

	if ($data = readJSON($url))
	{
		//var_dump($data);
		if (isset($data['General']['Sector']))
		{
			$list[$key][4] = $data['General']['Sector'];
			echo $list[$key][4] . ' | ';
		}
		else
		{
			errorecho('Sector not found. ');
		}

		
		if (isset($data['General']['Industry']))
		{
			$list[$key][5] = $data['General']['Industry'];
			echo $list[$key][5] . ' | ';
		}
		else
		{
			errorecho('Industry not found. ');
		}
		
		
		if (isset($data['General']['CurrencyCode']))
		{
			$list[$key][3] = $data['General']['CurrencyCode'];
			echo $list[$key][3] . '. ';
		}
		else
		{
			errorecho('CurrencyCode not found. ');
		}
		
	}
	else
	{
		errorecho('Could not parse json');
	}
	echo '<br>';
	
}

$filename = '../short_data/dataraw/isin/isinlist_weekend_categories_adder.csv';
saveCSVsemikolon($list, $filename);
	
?>
