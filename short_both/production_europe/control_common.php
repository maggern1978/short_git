<?php 
	
	date_default_timezone_set("Europe/Oslo");
	echo "The time is " . date("H:i:s") . '<br>';
	set_time_limit(500);

	date_default_timezone_set('Europe/Oslo');

	$production = 1;

	include('../production_europe/run.php');

	run('../production_europe/ticker_downloader.php'); //Lager ALLE ticker-data, internasjonale og nordiske børser, også historiske indekser

	run('../production_europe/open.php'); //Lager open.html som sier om noen av børsene er stengt

	//run('../production_europe/ticker_banner_international.php'); //Lager ticker-data, internasjonale og nordiske børser

	run('../production_europe/ticker_banner.php'); //Lager ticker-data banneret, internasjonale og nordiske børser

	run('../production_europe/currency.php'); //Laster ned valutadata til tickerlinje footer

	//Må komme etter currency!
	
	run('../production_europe/footer_about_shortnordic.php'); //Bygger footer_about.html med valutakurser

	run('../production_europe/footer_about_shorteurope.php'); //Bygger footer_about.html med valutakurser

	run('../production_europe/links_builder.php'); //lager søkedata

	run('../production_europe/summary_positions_tracked.php'); //lager oppsummering

	//run('../production_europe/isin_adder.php'); //legger til isins som jsonbuilder ikke finner. 
	
	run('../production_europe/cleaner_file_deleter.php'); //rydder opp gamle filer
	
	echo '<br>Control_common.php done! <br>';

?>