<?php 

set_time_limit(5000);

$production = 1;
//nedlasting av aksjer ferdig!
require '../production_europe/functions.php';
require '../production_europe/logger.php';
require('../production_europe/run.php');

$land = 'france';

run('../production_europe/csv_to_json_history_positions_cleaner.php');

//gjør om navnene fra myndigheter til det som står i hovedlisten for selskaper. 
$mode = 'francehistory';
$inputFile = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.json';
include('../production_europe/json_set_names_to_isinlist.php');

$land = 'france';
run('../production_europe/verdiutregning_selskap_sweden.php'); //lager all_positions_$land.json

run('../production_europe/json_splitter.php'); //splitter ut stock.prices verdiene

$land = 'france';
run('../production_europe/graph_short_generator.php'); //lager grafdata

echo '<br>History ' . $land  . ' completed! :D';
?>