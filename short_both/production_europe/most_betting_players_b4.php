<?php 

//$land = 'united_kingdom';

include '../production_europe/options_set_currency_by_land.php';

$selskapsliste = $land . '/' . $land . '_current.json';
$playerliste = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';


require '../production_europe/namelink.php';

$string = file_get_contents($playerliste);
$playerliste = json_decode($string, true);    

usort($playerliste, function ($item1, $item2) {
    return $item2['totalMarketValue'] <=> $item1['totalMarketValue'];
});

// Turn on output buffering
ob_start();

        $counter = 1;
        foreach ($playerliste as $key => $player)  {
            //var_dump($player);

            //TELL ANTALL POSISJONER
            $positionCounter = 0;
            foreach ($player['positions'] as $posisjon) {
                $positionCounter++;
            }

            echo '<li class="list-group-item ">';
            echo '<div>';
            echo '<div class="index" style="height:46px;">';
            echo $counter;
            echo '</div>';
            echo '<div style="overflow: hidden; height: 25px;">';

            $shorterx = nametolink($player['playerName']);
            echo '<strong><a href="details.php?player=' . $shorterx . '&land=' 
                . $land . '">';
            $shorter = $player['playerName'];
            
            $shorter   = strtolower($shorter) ;
            $shorter  = ucwords($shorter  );
            echo  $shorter . '</a></strong>' . '</span>';
            echo '</div><div>';
            echo '' . number_format($player['totalMarketValue'],2,",",".") . ' M ' .  $currency_ticker . ' in ';
            echo $positionCounter;

            if ($positionCounter > 1) {
                echo ' positions</li>';
            }
            else {
                echo ' position</li>';
            }
        
            //sett antall
            if ($counter > 3) {
                break;
            }
            $counter++;
        }

$filenameStart = '../short_data/html/most_betting_players_bf4_';
$filenameMiddle = $land;
$filenameEnd = '.html';
$filename = $filenameStart . $filenameMiddle . $filenameEnd;


//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename, $htmlStr);
        ?>



