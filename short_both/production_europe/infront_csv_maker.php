<?php

include_once '../production_europe/functions.php';
include_once '../production_europe/logger.php';

$souce_folder = '../short_data/json/infront_export/';
$destinaton_filename = '../infront_export/' . date('Y-m-d') . '.csv';

$countries = get_countries();
$main_csv_list = [];

foreach ($countries as $land) //copy from source files
{
	
	$filename = $souce_folder . $land . '_current.json';

	if (!$data = readJSON($filename))
	{
		errorecho('Read error of ' . $filename);
		logger('Read error of ' . $filename,  ', in infront_csv_maker');
		continue;
	}

	$country_code = get_country_code($land);
	
	$row = [];

	$counted_country = 0; 

	foreach ($data as $company)
	{
		$row[0] = $country_code;
		$row[1] = $company['ISIN'];
		$row[2] = round($company['ShortPercent'],2);
		$main_csv_list[] = $row;
		$counted_country++;
	}

	echo $land . '. Number of positions: ' . $counted_country . '<br>';

}

if (!empty($main_csv_list))
{
	if (saveCSVx($main_csv_list, $destinaton_filename))
	{
		logger($filename, ' save error! Skipping save in infront_csv_maker.php');
		return;
	}
}
else
{
	logger($filename, ' is empty! Skipping save in infront_csv_maker.php');
}


function get_country_code($string)
{
	switch ($string) 
	{
		case "norway":
		return 'NO';
		break;
		case "sweden":
		return 'SE';
		break;
		case "denmark":
		return 'DK';
		break;
		case "finland":
		return 'FI';
		break;
		case "united_kingdom":
		return 'UK';
		break;
		case "germany":
		return 'DE';
		break;
		case "france":
		return 'FR';
		break;
		case "italy":
		return 'IT';
		break; 
		case "spain":
		return 'SP';
		break;                         
		default:
		return false;
	}

}


?>