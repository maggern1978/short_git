<?php

function get_sessionid($result)
{
  $trueposition = strpos($result, 'session.sessionid=');
  $id = substr($result,$trueposition+18,32);

  return $id;
}

include '../production_europe/functions.php';

// First request for a sessionid
$ch = curl_init('https://www.fca.org.uk/publication/data/short-positions-daily-update.xlsx');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Remove this line if you don't want to see the headers:
//curl_setopt($ch, CURLOPT_HEADER, 1);

$result = curl_exec($ch);

file_put_contents('../short_data/dataraw/united_kingdom/united_kingdom_download.xlsx', $result);

curl_close ($ch);