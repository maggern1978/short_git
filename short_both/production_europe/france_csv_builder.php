<?php

include '../production_europe/functions.php';

$dir = '../production_europe/tmp/';
$files = listfiles($dir);

$count = count($files);

$csvfile = [];

for ($i = 0; $i < $count; $i++)
{

	if (strpos($files[$i],'.json',0))
	{
		if ($file = readJSON($dir . $files[$i]))
		{
			//var_dump($file);

			//"Positions Holder","Company Name","Company Isin","Short Position Percent","Position Start Date"

			if (!isset($file['positions_holder']))
			{
				var_dump($file);
				continue;
			}
			$temp[0] = $file['positions_holder'];
			$temp[1] = $file['company_name'];
			$temp[2] = $file['company_isin'];
			$temp[3] = $file['short_position_percent'];
			$temp[4] = $file['position_start_date'];

			//if position is under 0,5% set to zero. 
			if ($temp[3] < 0.5)
			{
				$temp[3] = 0;
			}

			$csvfile[] = $temp;
		}
		else
		{
			errorecho($i . '. Read error of ' . $files[$i]);
			continue;
		}
	}
	
}

usort($csvfile, function ($item1, $item2) {
    return $item2['4'] <=> $item1['4'];
});

saveCSVx($csvfile, '../short_data/dataraw/france/france_current.csv');
saveCSVx($csvfile, '../short_data/dataraw/france.history/france.history_current.csv');

?>

