<?php 

echo '<button onclick="showall()">Åpne alle</button>';
showall();

if (!function_exists('run2')) {
	function run2($filename) {
		$prodname = '../production_europe/' . $filename;

		global $land;
		global $inputFileName;
		global $type;

		if (file_exists($prodname)) {
			try {
				echo '<br><strong>' . $prodname . ' starter </strong>';
				$divid = 'filename' . mt_rand(0,10000) . mt_rand(0,10000);
			    runscript($divid);
			    echo '<button onclick="' . $divid .  '()">Åpne meldinger</button>';
			    echo '<div id=' . $divid . '  style="display:none; border:3px solid red; padding: 5px;">';
				//echo '<script>';
				//echo 'console.log(' . '1 +' . $divid . ')';
				//echo '</script>';
	    		require $prodname;
	    		echo '</div>';
			} catch (Exception $e) {
			    error_log($e);
			}
			echo ' | DONE<br>';
		}
		else {
			$filename = '../short_data/' . $filename;
			try {
			    echo '<br><strong>' . $filename . ' starter </strong>';
			    $divid = 'filename' . mt_rand(0,10000) . mt_rand(0,10000);
			    runscript($divid);
			    echo '<button onclick="' . $divid .  '()">Åpne meldinger</button>';
			    echo '<div id=' . $divid . '  style="display:none; border:3px solid red; padding: 5px;">';
				//echo '<script>';
				//echo 'console.log(' . ' 2 +' . $divid . ')';
				//echo '</script>';
				require $filename;
			    echo '</div>';
			    
			} catch (Exception $e) {
			    error_log($e);
			}
		
			echo ' | DONE<br>';
		}
	}
}


$filename = 'intraday_norway.php';
run2($filename);
run2('footer_about.php');

function runscript($divid) {
echo '<script>';
echo ' function ' . $divid . '() {';
echo ' var x = document.getElementById("' . $divid .  '");';
echo '  if (x.style.display === "none") {';
echo '    x.style.display = "block";';
echo '  } else {';
echo '    x.style.display = "none";';
echo '  }';
echo '}';
echo '</script>';

}

function showall() {
echo '<script>';
echo "if (typeof showall === 'function') { } else {";
echo 'function showall(){';
echo 'var divs = document.getElementsByTagName("div");';
echo 'for(var i = 0; i < divs.length; i++){';
echo 'divs[i].style.display = "block";';
echo '}';
echo '}';
echo '}';
echo '</script>';    
    
}

?>