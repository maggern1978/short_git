
<?php

require '../production_europe/logger.php';
include '../production_europe/options_set_currency_by_land.php';

$flagUrl = 'img/' . $land . '.svg';
$path = '../short_data/datacalc/nokkeltall/nokkeltall_' . $land . '.current.csv';
$selskapsliste = '../short_data/dataraw/' . $land . '/' . $land . '_current.json';


$csvFile = file($path);            
$data = [];
foreach ($csvFile as $line) {
    $data[] = str_getcsv($line);
} 

//var_dump($data);
//stocks
$stockUp = $data[1][14];
$stockDown = $data[1][15];
$stockZero = $data[1][16];
$sum = $stockUp + $stockDown + $stockZero;
$stockUpWidth = (($stockUp/$sum))*100;
$stockDownWidth = (($stockDown/$sum))*100;
$stockZeroWidth = (($stockZero/$sum))*100;

$playerUp = $data[1][17];
$playerDown = $data[1][18];
$playerZero = $data[1][19];
$playerSum = $playerUp + $playerDown + $playerZero;

if ($playerSum == 0 or $playerSum == FALSE) {
    logger ('playersum is zero, returning', ' in temperatures.php');
    return;
}

$playerUpWidth = (($playerUp/$playerSum ))*100;
$playerDownWidth = (($playerDown/$playerSum ))*100;
$playerZeroWidth = (($playerZero/$playerSum ))*100;

$positionUp = $data[1][20];
$positionDown = $data[1][21];
$positionZero = $data[1][22];
$positionSum = $positionUp + $positionDown + $positionZero;
$positionUpWidth = (($positionUp/$positionSum ))*100;
$positionDownWidth = (($positionDown/$positionSum ))*100;
$positionZeroWidth = (($positionZero/$positionSum ))*100;

date_default_timezone_set('Europe/Oslo');
$now = date('Y-m-d H:i',strtotime("now"));

// Turn on output buffering
ob_start();

?>
        <h3>Key statistics <span class="d-none d-md-inline">(latest trading day)</span>
            <img src="<?php echo $flagUrl; ?>" class="float-right flagg" alt="<?php echo ucwords($land); ?>" height="18px">
        </h3>
         <h4><span data-toggle="tooltip" title="The number of stocks that increased or decreased in value">Price change shorted companies</span><span class="pull-right updated-h4 align-middle">Updated: <?php echo $now; ?></span></h4>
    </div>
</div>
<div class="row temperature-padding">
    <div class="" style="width: <?php echo $stockUpWidth;?>%">
        <div class="bg-success stock-bar" > </div>
    </div>
    <div class="" style="width: <?php echo $stockZeroWidth;?>%">
        <div class="stock-bar-zero stock-bar stock-bar-padding" > </div>
    </div>
    <div class="" style="width: <?php echo $stockDownWidth;?>%">
        <div class="bg-danger stock-bar stock-bar-padding"></div>
    </div>
    <div class="col px-0 text-left">
        <span class="dot bg-success"></span> Up: <?php echo $stockUp; echo ' (' . number_format($stockUpWidth,1) . '&nbsp;%)';?>
    </div>
    <div class="col px-0 text-center">
        <span class="dot stock-bar-zero"></span> Unchanged: <?php echo $stockZero;  
        echo ' (' . number_format($stockZeroWidth,1) . '&nbsp;%)';?>
    </div>
    <div class="col px-0 text-right">
        <span class="dot bg-danger"></span> Down: <?php echo $stockDown; echo ' (' . number_format($stockDownWidth,1) . '&nbsp;%)'; ?>
    </div>
</div>
</div>
<br>
<div class="container">
  <div class="row">
    <div class="col-12">
        <h4><span data-toggle="tooltip" title="The number of players who earned or lost money">Players value change</span></h4>
    </div>
</div>
<div class="row temperature-padding">

    <div class="" style="width: <?php echo $playerUpWidth;?>%">
        <div class="bg-success stock-bar" > </div>

    </div>
    <div class="" style="width: <?php echo $playerZeroWidth;?>%">
        <div class="stock-bar-zero stock-bar stock-bar-padding" > </div>

    </div>
    <div class="" style="width: <?php echo $playerDownWidth;?>%">
        <div class="bg-danger stock-bar stock-bar-padding"></div>

    </div>

    <div class="col px-0 text-left">
        <span class="dot bg-success"></span> Up: <?php echo $playerUp; echo ' (' . number_format($playerUpWidth,1) . '&nbsp;%)'; ?>
    </div>
    <div class="col px-0 text-center">
        <span class="dot stock-bar-zero"></span> Unchanged: <?php echo $playerZero; echo ' (' . number_format($playerZeroWidth,1) . '&nbsp;%'; ?>)
    </div>
    <div class="col px-0 text-right">
        <span class="dot bg-danger"></span> Down: <?php echo $playerDown; echo ' (' . number_format($playerDownWidth,1) . '&nbsp;%)'; ?>
    </div>
</div>
</div>
<br>
<div class="container">
  <div class="row">
    <div class="col-12">
        <h4><span data-toggle="tooltip" title="Up represents earned money for the position holder">Short positions value change</span></h4>
    </div>
</div>
<div class="row temperature-padding">

    <div class="" style="width: <?php echo $positionUpWidth;?>%">
        <div class="bg-success stock-bar" > </div>

    </div>
    <div class="" style="width: <?php echo $positionZeroWidth;?>%">
        <div class="stock-bar-zero stock-bar stock-bar-padding" > </div>

    </div>
    <div class="" style="width: <?php echo $positionDownWidth;?>%">
        <div class="bg-danger stock-bar stock-bar-padding"></div>

    </div>

    <div class="col px-0 text-left">
        <span class="dot bg-success"></span> Up: <?php echo $positionUp; echo ' (' . number_format($positionUpWidth,1) . '&nbsp;%)'; ?>
    </div>
    <div class="col px-0 text-center">
        <span class="dot stock-bar-zero"></span> Unchanged: <?php echo $positionZero; echo ' (' . number_format($positionZeroWidth,1) . '&nbsp;%)';?>
    </div>
    <div class="col px-0 text-right">
        <span class="dot bg-danger"></span> Down: <?php echo $positionDown; echo ' (' . number_format($positionDownWidth,1) . '&nbsp;%)';


$filenameStart = '../short_data/html/temperatures_';
$filenameCountry = $land;
$filenameEnd = '.html';
$filename = $filenameStart . $filenameCountry . $filenameEnd;

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename, $htmlStr);

?>


