
<?php 

//$land = 'sweden';

include '../production_europe/options_set_currency_by_land.php';

$landnavn = ucwords($land); 
$filename = '../short_data/datacalc/shortlisten_' . $land .'_current.csv';

//Last inn nyeste shorttabell 

            //Prepare data
            //Read in data

                $csvFile = file($filename);            
                $data = [];
                foreach ($csvFile as $line) {
                     $data[] = str_getcsv($line); 
                } 
            
            //find file timestamp
                
            if (file_exists($filename)) {
                $filetime = filemtime($filename);
            }

            //Read make arrays for name and verdiendring

            //prepeare arrays for winners

            $names = array();
            $percentageChange = array();
            $verdiendring = array();
            $counter = 0; //
            $count = count($data);
            $success = 0;

            foreach ($data as $entries) { 
                if ($counter == $count-1 or $counter == 11) {//topp ti valgt, add en pga først linje ikke skal med
                    break;
                }
                if ($counter != 0 and $entries[1]>0) {
                        
                    $temp_navn = $entries[0];
                        if ($land == 'sweden' ) {
                            $temp_navn = strtolower($temp_navn);
                            $temp_navn = ucwords($temp_navn);
                        }

                    array_push($names, $temp_navn);
                    array_push($verdiendring, $entries[14]);

                    $kursDagenfor = $entries[6]-$entries[7];
                    $percentage = (($entries[7])/$kursDagenfor)*100;

                    $percentage = number_format($percentage,2,".","");
                    array_push($percentageChange, $percentage);
                    $success = 1;
                }
                $counter++;
            }; 


// Turn on output buffering
ob_start();

            ?>

        <div class="col-lg-6 pb-3">
          <div id="<?php echo $landnavn . '1'; ?>" style="width: 100%;height:350px;"></div>
          <script>
                var names = <?php echo json_encode($names); ?>;

            <?php 
            $counter = 0;
            foreach ($verdiendring as $endring) {
                echo 'var verdi' . $counter . ' = "' .  number_format($endring/1000000,2,".","") . '";';
                $counter++;
            }
            ?>
            </script>

          <script type="text/javascript">

        // based on prepared DOM, initialize echarts instance
        var myChart1<?php echo $landnavn; ?> = echarts.init(document.getElementById('<?php echo $landnavn . '1'; ?>'));

        var w = window.innerWidth;
        var h = window.innerHeight;

        if (w > 600) {
        }



        // specify chart configuration item and data
        var labelRight = {
            normal: {
                position: 'right',
                textBorderColor: '#f9f9f9',
                textBorderWidth: 2,

            }
        };
        var labelLeft = {
            normal: {
                position: 'left',
                textBorderColor: '#f9f9f9',
                textBorderWidth: 2,
            }
        };

        var labelCenter = {
            normal: {
                position: 'inside',
                color: 'white',
           }
        };


        option = {
            toolbox: {
                show : true,
                itemSize: 15,
                feature : {
                    dataView : {
                    show: true,
                    title: 'Toplist earning',
                    lang: ['Data', 'Exit', 'Refresh'],
                    },
                },
            },
            grid: {
            top:    80,
            bottom: 10,
            left:   '5%',
            right:  '5%',
            },
            animation: false,
            backgroundColor: '#f6f6f6',
            textStyle: {
                color: 'black',
                fontSize: '14',
                fontFamily: ['Encode Sans','sans-serif'],
            },

            title: {
                text: 'Toplist: Where money was made',
                subtext: 'Million <?php echo $currency_ticker ;?>. Updated: <?php echo date("Y-m-d H:i", $filetime); ?>',
                link: 'http://sh/details_company_all.php?&land=<?php echo $land; ?>',
                sublink: 'http://sh/details_company_all.php?&land=<?php echo $land; ?>',
                left: 'center',
                subtextStyle: {
                    color: 'black',
                    fontSize: 14,

                }
            },
            tooltip : {
                trigger: 'item',
                axisPointer : {            
                    type : 'shadow',     
                }
            },
      
            xAxis: {
                type : 'value',
                position: 'top',
                splitLine: {lineStyle:{type:'dashed'}},
                axisLabel: {
                    fontSize: '16',
                }
            },
            yAxis: {
                type : 'category',
                axisLine: {show: false},
                axisLabel: {show: false},
                axisTick: {show: false},
                splitLine: {show: false},
                data: names,

            },
            series : [
            {
                name:'Toplist winners: Numbers in million <?php echo $currency_ticker;?>',
                type:'bar',

                label: {
                    normal: {
                        show: true,
                        formatter: '{b}', 
                    }
                },
                itemStyle: {
                    color: '#025928',
                },
                data:[

                             <?php 
                $counter = 0;
                $sisteVerdi = count($verdiendring);
                $sisteVerdiTeller = $sisteVerdi;
                $labelRight = 'labelRight';
                $labelLeft = 'labelLeft';
                $labelCenter = 'labelCenter';

                foreach ($verdiendring as $key => $endring) {
                    $label = $labelRight;

                    if ($key == 0) {
                        $label = $labelCenter;
                    }

                    if ($key == 1) {
                        $ratio = $verdiendring[1]/$verdiendring[0];
                        
                        if ($ratio > 0.65) {
                            $label = $labelCenter;
                        }                  
                        
                    }
                    if ($key == 2) {

                        $ratio = $verdiendring[2]/$verdiendring[0];
                        
                        if ($ratio > 0.65) {
                            $label = $labelCenter;
                        }  
                      
                    } 

                    echo '{
                        value: ' .  number_format($endring/1000000,2,".","") . ', 
                        label:' . $label . ', 
                        tooltip: {
                            backgroundColor: "black",
                            formatter: "{b}: <br>+{c}M '. $currency_ticker . ' (won)';
                            echo '<br>Stock price: ' . $percentageChange[$counter] . '%"';
                        echo '},';
                    echo '},';
                    $counter++;
                    $sisteVerdiTeller--;
                }        
                ?>

                ]
            }
            ]
        };

        // use configuration item and data specified to show chart
        myChart1<?php echo $landnavn; ?>.setOption(option);
        
        window.addEventListener('resize', function(event){
          myChart1<?php echo $landnavn; ?>.resize();
        });

    </script>

</div>


<?php 
$filenameStart = '../short_data/html/most_earning_calc_';
$filenameEnd = '.html';

$filename = $filenameStart . $land . $filenameEnd;

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename , $htmlStr);
?>