<?php 

include '../production_europe/functions.php';

$currencies = ['nok', 'sek', 'eur', 'dkk'];

$allBox = [];

foreach ($currencies as $key => $currency)
{

    if (!$data = readJSON('../short_data/json/currency/' .  $currency . '.forex.json'))
    {
       errorecho('Error, could not read file');
       return;
    }

    if (!testdata($data))
    {
        errorecho('Error testing the data');
        return;
    }

    $data['timestamp'] = settime($data['timestamp']);
    $allBox[$currency] = $data;
}

ob_start();

?>

    <?php 

    foreach ($allBox as $key => $currency)
    {

    if ($key == 'nok')
    {
        echo '<strong>1 USD = </strong>';
    }

    $mirroredValue = 1/$currency['close'];
    $mirroredValue = number_format(round($mirroredValue,2),3,".",",");
    $timestamp = $currency['timestamp'];
    $close = number_format(round($currency['close'],2),2,".",","); 

    ?>

    <span data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo '1 NOK = ' . $mirroredValue . ' USD' . '<br>' . 'Updated: ' . $timestamp ; ?>"><?php echo $close . ' ' . strtoupper($key); ?>
    <?php
        if ($key != 'dkk')
        {
            echo '&nbsp;&nbsp;|&nbsp;&nbsp; ';
        }
    ?>
    </span>
    <?php    
     }
    ?>
    <br>
     <!-- <p class='text-center'><a class="twitter-follow-button"
  href="https://twitter.com/shorteurope"
  data-size="default" data-show-count="false">
Twitter @Shorteurope</a></p>-->
<p class='text-center'>"Always check who is betting against you".</p>
<?php //include '../production_europe/newsletter_subscribe_footer.html';?>
</footer>
<br>

<div class="container">
<div class="row">
<div class="col-12 text-center">
<h5><a href="https://shorteurope.com/newsletter/signup.php?source=shorteurope.com">Sign up to our daily newsletter</a></h5>
</div>
</div>
</div>

<?php 
//include '../short_data/header_ad.html';
//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents('../short_data/html/footer_about_shortnordic.html', $htmlStr);


function settime($time)
{

    //find time<ome difference
    $dtz = new DateTimeZone('Europe/Oslo');
    $time_in_oslo = new DateTime('now', $dtz);
    $timeoffset = (string) $dtz->getOffset($time_in_oslo)/3600;

    //format time
    $DateTime = new DateTime($time);
    $DateTime->modify('- ' . $timeoffset . ' hours');
    return  (string)$DateTime->format("Y-m-d H:i:s");

}

function testdata($data) // tesing if all data is there
{
    if (!isset($data['timestamp']))
    {
        return false; 
    }

    if (!isset($data['close']))
    {
        return false; 
    }

    if ($data['timestamp'] == '' or $data['timestamp'] == false or $data['timestamp'] == null)
    {
        return false; 
    }
    if ($data['close'] == '' or $data['close'] == false or $data['close'] == null)
    {
        return false; 
    }

    return true;
}


?>
