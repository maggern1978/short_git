

<?php 
include '../short_data/header.html';
require '../production_europe/logger.php';

echo '<br><br><br><br>';

if (!function_exists('download_json')) {

	function download_json($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSLVERSION,1);
	$data = curl_exec($ch);
	$error = curl_error($ch); 
	//var_dump($data);
	if ($error != '') {
		var_dump($error);
		throw new Exception("download error" . $url);
	}
	curl_close ($ch);
	return $data;
	}
}

	$fullurl = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=^VIX&outputsize=full&apikey=4XI39Z4NYH3C983R';
	$newticker = '';
	$NUM_OF_ATTEMPTS = 10;
	$attempts = 0;

	do {	
	    try
	    {
	    	$newticker = json_decode(download_json($fullurl), true);

	    	if (!isset($newticker['Time Series (Daily)'])) {
	    		echo 'Error 1';
	    		throw new Exception("download error in VIX.php" . $fullurl);
	    	}

	        sleep(0.61);

	    } catch (Exception $e) {
	        echo 'Forsøk nr: ' . $attempts . ' feilet!<br>';
	        logger($fullurl, 'Could not download this url in VIX.php');
	        $attempts++;
	        sleep(2);
	        continue;
	    }
	    break;
	} while($attempts < $NUM_OF_ATTEMPTS);

//var_dump($newticker['Time Series (Daily)']['2019-04-12']);

$count = count($newticker['Time Series (Daily)']);

$highBox = array();
$lowBox = array();
$lineBox = array();
$dateBox = array();
$keys = array_keys($newticker['Time Series (Daily)']);
for ($i = 0; $i < $count; $i++) {
	$highBox[] = $newticker['Time Series (Daily)'][$keys[$i]]['2. high'];
	$lowBox[] = $newticker['Time Series (Daily)'][$keys[$i]]['3. low'];
	$lineBox[] = $newticker['Time Series (Daily)'][$keys[$i]]['4. close'];
}

?>

<div class="col-12 col-sm-12 cold-md-12 col-lg-4 pb-3"> 
  <div id="vix" style="width: 100%;height:350px;"></div>
    <script>
    	var high = <?php echo json_encode($highBox); ?>;
    	var low = <?php echo json_encode($lowBox); ?>;
    	var line = <?php echo json_encode($lineBox); ?>;     
    	var dates = <?php echo json_encode($keys); ?>;

    	high.reverse();
    	low.reverse();
    	line.reverse();
    	dates.reverse();

    	var lowest = high[0];
    	var highest = low[0];
    	var count = high.length;
    	//console.log(count);
    	var difference =  new Array();

    	for (i = 0; i < count; i++) {
    		//console.log(high[i]);
    		difference[i] = high[i] - low[i];
    		difference[i]  = roundToTwo(difference[i]);
    		low[i]  = roundToTwo(low[i]);
    		high[i]  = roundToTwo(high[i]);
    		line[i]  = roundToTwo(line[i]);

    		if (low[i] < lowest) {
    			lowest = low[i];
    			//console.log('Lowest er nå: ' + lowest + ' at ' + i);
    		}

    	}    

    	lowest = Math.floor(lowest);

    	function roundToTwo(num) {    
    		return +(Math.round(num + "e+2")  + "e-2");
		}

    	//console.log('lowest done:' + lowest);
    	//console.log(minimum);

    	Math.floor(1.6);

        // based on prepared DOM, initialize echarts instance
        var vix = echarts.init(document.getElementById('vix'));
        var w;
        var h;
        var left;
        var right;
        var headingFontSize;
        var fontSizen;
        var toppen;

        setsizes();

      function setsizes() {
        w = vix.getWidth();
        h = window.innerHeight;

        if (w >= 1200) {
             left = '15%';
             right = '4%';
             headingFontSize = 18;
             fontSizen = '18';
             toppen = '60';
             //console.log('1200');
        }
        else if (w >= 992) {
             left = '15%';
             right = '4%';
             headingFontSize = 18;
             fontSizen = '17';
             toppen = '60';
             //console.log('992');
        }
        else if (w >= 768) {
             left = '6%';
             right = '3%';
             headingFontSize = 18;
             fontSizen = '18';
             toppen = '60';
             //console.log('768');
        }
        else if (w >= 576) {
             left = '9%';
             right = '3%';
             headingFontSize = 18;
             fontSizen = '16';
             toppen = '60';
             //console.log('576');
        }
        else if (w >= 440) {
             left = '12%';
             right = '3%';
             headingFontSize = 18;
             fontSizen = '15';
             toppen = '60';
             //console.log('440');
        }
        else if (w >= 352) {
             left = '12%';
             right = '3%';
             headingFontSize = 18;
             fontSizen = '15';
             toppen = '60';
             //console.log('392');
        }
        else {
             left = '18%';
             right = '3%';
             headingFontSize = 18;
             fontSizen = '15';
             toppen = '60';
             //console.log('else');
        }
      }


option = {
        toolbox: {
            show : true,
            itemSize: 15,
            feature : {
                dataView : {
                show: true,
                title: 'Data',
                lang: ['Data', 'Exit', 'Refresh'],
                },
            },
        },
    textStyle: {
        fontFamily: ['Encode Sans','sans-serif'],
    },
    title: {
        //backgroundColor: '#000000',
        left: 'left',
        link: 'https://en.wikipedia.org/wiki/VIX',
        padding: [10,10,10,10],
        text: 'The fear-index (CBOE Volatility Index, VIX)',
        subtext: 'Expectation of volatility implied by S&P 500 index options.',
        textStyle: {
            color: '#000000',
            fontSize: headingFontSize,
            
        },
        
   
    },
        tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            animation: false,
            label: {
                backgroundColor: '#ccc',
                borderColor: '#aaa',
                borderWidth: 1,
                shadowBlur: 0,
                shadowOffsetX: 0,
                shadowOffsetY: 0,
                textStyle: {
                    color: '#222'
                }
            }
        },
        formatter: function (params) {
            return params[0].name + ' <br />' + 'Low: ' +  params[0].value + '<br /> High: ' + high[params[0].dataIndex] + ' <br /> End: ' + params[2].value;
        }
    },
    grid: {
	    top:  toppen,
	    bottom: 60,
	    left:   left,
	    right:  right,
    },
    animation: false,
    backgroundColor: '#f6f6f6',
  
        xAxis: {
            type: 'category',
            data: dates,
        },
        yAxis: {
            min: lowest,
        },
        dataZoom: [
            {
                show: true,
                type: 'slider',
                bottom: 10,
                start: 97,
                end: 100,
                handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                handleSize: '75%',
                handleStyle: {
                  color: 'black',
                },
            },
        ],
        series: [{
            name: 'Low',
            type: 'line',
            data: low,
            lineStyle: {
                normal: {
                    opacity: 0
                }
            },
 
            stack: 'confidence-band',
            symbol: 'none'
        }, {
            name: 'High (+low)',
            type: 'line',
            data: difference,
            lineStyle: {
                normal: {
                    opacity: 0
                }
            },
            areaStyle: {
                normal: {
                    color: '#aaa'
                }
            },
            stack: 'confidence-band',
            symbol: 'none'
        }, {
            name: 'VIX (end of day)',
            type: 'line',
            data: line,
            hoverAnimation: false,
            symbolSize: 6,
            itemStyle: {
                normal: {
                    color: '#c23531'
                }
            },
            showSymbol: false
        }],
    };

      //ide: legg inn data så størrelser endrer seg.
       // use configuration item and data specified to show chart
        vix.setOption(option);
        
        window.addEventListener('resize', function(event){
             
        setsizes();
     
        option = {
          grid: {
            top:  toppen,
            bottom: 60,
            left:   left,
            right:  right,
            },
        title: {
          textStyle: {
              fontSize: headingFontSize,
          }
        },
        legend: {
          top: 36,
            textStyle: {
              fontSize: fontSizen,
            }
          }
        }
        vix.setOption(option);       
        vix.resize();

 });

    </script>
</div> 
