<?php

set_time_limit(5000);


//$inputFileName = 'dataraw/fi.se.history/fi.se.history_merged.csv';
//$inputFileName = 'dataraw/fi.se/fi.se_current.csv';

if (!function_exists('readCSV9')) {
    function readCSV9($file_location_and_name) {

        $csvFile = file($file_location_and_name);

        //Les in dataene 
        $name = [];
        foreach ($csvFile as $line) {
            $name[] = str_getcsv($line, ',');
        }

        //ordne utf koding
        $counter = 0;
        foreach ($name as &$entries) {
            $data[$counter] = array_map("utf8_encode", $entries);
            $counter++;
        }
        return $name;
    }
}

//Lagre
if (!function_exists('saveCSV9')) {
    function saveCSV9($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');

    foreach ($input as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
    }

   $sverige = readCSV9($inputFileName) or die("Klarte ikke å lese inn filen!"); 

//$sverige = readCSV('dataraw/fi.se/test.csv') or die("Klarte ikke å lese inn fi.se_current.csv");

$isin_length = $sverige[0][2];
$isin_length = strlen($isin_length);


foreach ($sverige as $posisjon) {
    if (isset($posisjon[3])) {
        if ($isin_length == $isin_length ) {
            $position_names[] = ($posisjon[1]);
            $isin_numbers[] = ($posisjon[2]);
        }
    }
}

$isin_numbers_copy = $isin_numbers;

$company_array = array(
        "firstname" => array(),
        "aliases" => array(),
        "ISINS" => array()
 );

$runner = 0;
foreach ($position_names as $names) {

    //set main name
    $company_array[$runner]["firstname"][0] = $names;
    
    //collect isin positions in array
    $isin_box = array();
    $counter = 0;

    foreach ($position_names as $target) {
        if ($names == $target) {
           $isin_box[] = $counter;
        }
        $counter++;
    } 
    
    //push found isins to main array and then destroy them
    foreach ($isin_box as $isin_number) {
        
        if (isset($isin_numbers[$isin_number])) {
        $company_array[$runner]["ISINS"][] = $isin_numbers[$isin_number];
        
  //echo 'Added ' . $isin_numbers[$isin_number] . ' to ' .  $names . ' | ';
        
        unset ($isin_numbers[$isin_number]);
        }
    }
       
    //var_dump($isin_box);
    
    $runner++;
}

//Nå har vi alle isinen baset på ett navn

//slett innføringer som ikke har noen isins
//echo '<br>';

$loper = 0; 
foreach ($company_array as $company) {
//echo count($company_array);

    if (!isset($company_array[$loper]["ISINS"])) {
        unset ($company_array[$loper]);
    }
    //else { echo 'else';}
    
    $loper++;
}

$company_array = array_values($company_array);

$counter = 0;

unset ($company_array[0]);
unset ($company_array[1]);
unset ($company_array[2]);

foreach ($company_array as $company) {
    //var_dump($company);  
    $counter++;
    //echo ' Counter: ' . $counter . '<br>';  
    
    if (isset($company["ISINS"])) {
        foreach ($company["ISINS"] as $isin) {
            //echo '<br> Posisjon: ' . $isin . ':';
            //søk etter isin og hvis treff, kopier navnet
            
            $target = $isin;
            $tellemaskin = 3;
                foreach ($company_array as $comp) {
                    if (isset($comp["ISINS"])) {
                        $nr = 0;

                        foreach ($comp["ISINS"] as $i) {
                            if ($target == $i) {
                                  
                                $company_array[$tellemaskin]["firstname"][] = $company["firstname"][0];


                            }
                            $nr++;
                        
                        }

                    }
                    $tellemaskin++; 
                }




        }
        //echo '<br>';
        //echo '--------------<br>';
   
    }
    else {
        //echo 'En ignorert!<br>';
    }  
  
}   


$all_array = array();

$runner = 0;
foreach ($company_array as $company) {

    $all_array[$runner]['NAME'] = array_unique($company['firstname']);
    $all_array[$runner]['ISIN'] = array_unique($company['ISINS']);

$runner++;
}

//echo '<br><br> Company array<br>';

//Nå går vi igjennom arrayen og sletter alle duplicater av datene

//var_dump($company_array);
//var_dump($position_names);
//echo count($company_array);

//Selskaper
foreach ($all_array as $company) {

    $selskapsteller = 0;

    foreach ($company['NAME'] as $name) {

        //$target = $name;
        $target = $name;
        //echo 'Target: ' . $target . ' ';
        
        $hit_counter = 0;

        foreach ($all_array as $nr => $target_company) {
            $target_counter = 0;
            
            //echo '<br> Søker i: ' . $target_company['NAME'][0] . '<br>';
            foreach ($target_company['NAME'] as $key => $find) {

                //echo ' Find er ' . $find . '<br>';
                //echo $key . ' ';

                if ($target == $find and $hit_counter == 0) 
                {
                    //echo ' First hit!<br> ';
                    $hit_counter++;

                    //Avbryt foreach ved første funn av et navn (så man ikke slettr seg selv)
                    break;
                }

                if ($target == $find) {
                    //echo 'Duplicate hit! <br> Find er: ' . $find . ' og key ' . $key . '. ';
                    //unset ($target_company['NAME'][$key]);
                    //echo 'Sletter ' . $nr . '<br>';
                    unset($all_array[$nr]);

                }
                else {
                    //echo ' no hit';
                }
                
            }
            //$sok_teller++;
            $target_counter++;          
        }

    }
    $selskapsteller++;
//echo '<br>------------------------------------<br>';

}


//isins

foreach ($all_array as $company) {

    $selskapsteller = 0;


    foreach ($company['ISIN'] as $name) {

        //$target = $name;
        $target = $name;
        //echo 'Target: ' . $target . ' ';
        
        $hit_counter = 0;

        foreach ($all_array as $nr => $target_company) {
            $target_counter = 0;
            
            //echo '<br> Søker i: ' . $target_company['NAME'][0] . '<br>';
            foreach ($target_company['ISIN'] as $key => $find) {

                //echo ' Find er ' . $find . '<br>';
                //echo $key . ' ';

                if ($target == $find and $hit_counter == 0) 
                {
                    //echo ' First hit!<br> ';
                    $hit_counter++;

                    //Avbryt foreach ved første funn av et navn (så man ikke slettr seg selv)
                    break;
                }

                if ($target == $find) {
                    //echo 'Duplicate hit! <br> Find er: ' . $find . ' og key ' . $key . '. ';
                    //unset ($target_company['NAME'][$key]);
                    //echo 'Sletter ' . $nr . '<br>';
                    unset($all_array[$nr]);

                }
                else {
                    //echo ' no hit';
                }
                
            }
            //$sok_teller++;
            $target_counter++;  
        
        
        }

    }
    $selskapsteller++;
//echo '<br>------------------------------------<br>';

}


$counter = 0;
foreach($all_array as $selskapskey => $selskap) {
    
    //echo 'Nr: ' . $counter . ' <br>';
    $counter++;

    //find first of names
    foreach ($selskap['NAME'] as $navn) {
        $firstname = $navn;
        break;
    }

    //find first og isin
    foreach ($selskap['ISIN'] as $isin) {
        $firstisin = $isin;
        break;
    }

    foreach ($selskap['NAME'] as $navnkey => $navnet) {    
        $namecounter = 0;

        //

        foreach ($sverige as $sverigekey => $row) {
            
            if (isset($row[1])) {

                if ($row[1] == $navnet) {

                    $sverige[$sverigekey][1] = $firstname;
                    $sverige[$sverigekey][2] = $firstisin;

                    //echo $firstname . ' ' .  $firstisin . '<br>';

                }
            }
        }
    }
};

saveCSV9($sverige, $inputFileName);

}

?>