<?php

//må ta utgangspunkt i norway_history_current.json
$land = 'sweden';

//$land = 'sweden';

$millisecondsts1 = microtime(true);

if (!function_exists('date_range_key')) {

	function date_range_key($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

		$dates = array();
		$current = strtotime($first);
		$last = strtotime($last);

		while( $current <= $last ) {
          //only work days
			if(date("w",$current) != 6 and date("w",$current) != 0) {
				$dates[date($output_format, $current)] = (float) 0.00;
			}
			$current = strtotime($step, $current);
		}
		return $dates;
	}
}

set_time_limit(5000);

//$land = "denmark";
$millisecondsts1 = microtime(true);

//for just doing a few companies
//$land = 'sweden';

require '../production_europe/functions.php';
//require '../production_europe/logger.php';

$name_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );


$historyjson = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.json';
//$historyjson = '../short_data/dataraw/' . $land . '/' . $land . '_current.json';
//$historyjson = 'germany.history_current.json';

$savefolder = '../short_data/history_shorting/' . $land . '/';
$graphFolder = '../short_data/history/';

//leser inn isinkoder
//germany.history_current.json

echo '<br><strong>Land er ' . $land . '</strong><br>';

if (!$historyjson = readJSON($historyjson))
{
	errorecho('Error reading json, returning...');
	return;
}

$historyjsoncount = count($historyjson);

//development, tar vekk alt som ikke er aktive posisjoner.
for ($i = 0; $i < $historyjsoncount; $i++)
{
	$positionscount = count($historyjson[$i]['Positions']);
	//echo '<strong>' . $i . ' | ' . ' ' . $historyjson[$i]['Name'] . '</strong><br>';

	//collect all names
	$localnamesbox = [];
	
	for ($x = 0; $x < $positionscount; $x++)
	{
		if ($historyjson[$i]['Positions'][$x]['isActive'] != 'yes')
		{
			unset($historyjson[$i]['Positions'][$x]['isActive']);
		}
	}

	$historyjson[$i]['Positions'] = array_values($historyjson[$i]['Positions']);

}

//normaliserer 


for ($i = 0; $i < $historyjsoncount; $i++)
{
	$positionscount = count($historyjson[$i]['Positions']);
	echo '<strong>' . $i . ' | ' . ' ' . $historyjson[$i]['Name'] . '</strong><br>';

	//collect all names
	$localnamesbox = [];
	
	for ($x = 0; $x < $positionscount; $x++)
	{

		$localnamesbox[] = mb_strtolower($historyjson[$i]['Positions'][$x]['PositionHolder']);

		if ($land == 'norway') //remove norway specific timestamp
		{
			$historyjson[$i]['Positions'][$x]['ShortingDate'] = str_replace('T00:00:00', '', $historyjson[$i]['Positions'][$x]['ShortingDate']);
		}

	}
	
	$localnamesbox = array_unique($localnamesbox);
	$localnamesbox = array_values($localnamesbox);
	$localnamesboxcount = count($localnamesbox);
	
	//ta hvert navn og sett start og sluttdatoer
	$allocalpositionsbox = [];
	
	//ta et navn
	for ($z = 0; $z < $localnamesboxcount; $z++)
	{
		
		//samle inn alle posisjoner for player
		$targetname = $localnamesbox[$z];
		$localpositionsbox = [];
		for ($x = 0; $x < $positionscount; $x++)
		{
			if ($targetname == strtolower($historyjson[$i]['Positions'][$x]['PositionHolder']))
			{
				$localpositionsbox[] = $historyjson[$i]['Positions'][$x];
			}
			
		}
		echo $i . ' | '  . $z . ' | Playernavn ' . $targetname . '<br>';
		$localpositionsboxcount = count($localpositionsbox);

		if ($historyjson[$i]['Name']  == 'Peab AB')
		{
			//var_dump($historyjson[$i]['Positions'] );
		
		}

		//ta posisjonene
		$firstposition; 
		$previouspositiondate = '';

		//lag et unntak med navn med bare en posisjon. Den posisjonen må være aktiv, hvis ikke er det noe feil, og den må slettes. 
		if ($localpositionsboxcount == 1)
		{ 
			echo 'Count is 1, checking if position is active. ';

			if ($localpositionsbox[0]['isActive'] == 'no' )
			{
				echo 'Posistion is NOT active, will skip player and positions';
				continue;
			}
			else
			{
				echo 'Position is active, will include.<br>';
			}

		}
		
		for ($x = 0; $x < $localpositionsboxcount; $x++)
		{
			if ($x == 0)
			{
				$previouspositiondate = $localpositionsbox[$x]['ShortingDate'];
				
				if ($localpositionsbox[$x]['ShortPercent'] >= 0.5)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = date('Y-m-d');
					$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($forrigeposisjonsstartdato)));
					continue;
				}
				else if ($localpositionsbox[$x]['ShortPercent'] < 0.50)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = $localpositionsbox[$x]['ShortingDate'];
					//$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($localpositionsbox[$x]['ShortingDate'])));
					continue;
				}				
				else
				{
					errorecho("Noe er galt med $localpositionsbox[$x]['ShortPercent'] <br>");
				}
			}
			else
			{			
				if ($localpositionsbox[$x]['ShortPercent'] > 0.49)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = $forrigeposisjonsstartdato;
					$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($forrigeposisjonsstartdato)));
					//echo '<br>' . $forrigeposisjonsstartdato . '<br>';
					continue;
				}
				else if ($localpositionsbox[$x]['ShortPercent'] == 0)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = $localpositionsbox[$x]['ShortingDate'] . '';
					//$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($localpositionsbox[$x]['ShortingDate'])));
					continue;
				}
				else if ($localpositionsbox[$x]['ShortPercent'] < 0.5)
				{
					$localpositionsbox[$x]['ShortingDateEnd'] = $localpositionsbox[$x]['ShortingDate'];
					$forrigeposisjonsstartdato = date('Y-m-d', strtotime("-1 weekday", strtotime($localpositionsbox[$x]['ShortingDate'])));
					//$forrigeposisjonsstartdato = $localpositionsbox[$x]['ShortingDate'];
					continue;
				}									
				else
				{
					errorecho("Noe er galt med $localpositionsbox[$x]['ShortPercent'] <br>" . '. Verdien er "' . $localpositionsbox[$x]['ShortPercent'] . '" ');
				}
			}		
			
		}

		//take a look at the first position and check that is it active if over 0,49%. If not, take it out, the positin has not end date

		if (isset($localpositionsbox[0]) and $localpositionsbox[0]['ShortPercent'] > 0.49)
		{
			echo 'First position is positive, should be active, or somethine is wrong. ';

			if (!isset($localpositionsbox[0]['isActive']))
			{
				var_dump($localpositionsbox[0]);
			}

			if ($localpositionsbox[0]['isActive'] == 'no')
			{
				echo 'Position is not active, something is wrong. Will delete position as it has not end date.<br>';
				unset($localpositionsbox[0]);

				$localpositionsbox = array_values($localpositionsbox);
				$localpositionsboxcount = count($localpositionsbox);				

			}
			else
			{
				echo 'Position is active, so ok!<br>';
			}
		}


		for ($x = 0; $x < $localpositionsboxcount; $x++)
		{
			$allocalpositionsbox[] = $localpositionsbox[$x];
		}		
	}
	
	//sort by date
	usort($allocalpositionsbox, function ($item1, $item2) {
		return $item1['ShortingDate'] > $item2['ShortingDate'];
	});

	
	$historyjson[$i]['Positions'] = $allocalpositionsbox;
	
	//saveJSON($allocalpositionsbox, 'test.json');
	//exit();
}

var_dump($historyjson[1]['Positions'][0]);



//finn eldste dato
$historyjson_count = count($historyjson);
$today = date('Y-m-d');
$oldestdate = $today;
$all_company_names = []; //samle inn selskapsposisjoner. De er fordelt på playere. 
$all_player_names = [];

for ($i = 0; $i < $historyjson_count; $i++)
{
	$position_count = count($historyjson[$i]['Positions']);

	$all_company_names[] = $historyjson[$i]['Name'];

	for ($x = 0; $x < $position_count; $x++)
	{
		$all_player_names[] = $historyjson[$i]['Positions'][$x]['Name'];

		if ($historyjson[$i]['Positions'][$x]['ShortingDate'] < $oldestdate)
		{
			$oldestdate = $historyjson[$i]['Positions'][$x]['ShortingDate'];
		}
	}
}

echo 'Eldste dato i posisjonene er ' . $oldestdate . '<br>';

$all_company_names = array_unique($all_company_names);
$all_company_names = array_values($all_company_names);

$all_player_names = array_unique($all_player_names);
$all_player_names = array_values($all_player_names);

//var_dump($company_sorted_box[0]);

//make array for filling in num pos, players, companies. 
$datearray = date_range($oldestdate, $today);
$datearray_count = count($datearray);

$date_object_box = [];

for ($i = 0; $i < $datearray_count; $i++)
{
	$obj = new stdClass;
	$obj->players = [];
	$obj->companies = [];
	$obj->positions = 0; 

	$date_object_box[$datearray[$i]] = (array)$obj;
}

//var_dump($date_object_box[$oldestdate]);

$historyjson_count = count($historyjson);

//gå igjennom posisjonene og mat inn 
for ($i = 0; $i < $historyjson_count; $i++)
{
	$position_count = count($historyjson[$i]['Positions']);

	$company_shorted_dates = []; //datoer hvor selskapet er shortet

	for ($x = 0; $x < $position_count; $x++)
	{

		$position = $historyjson[$i]['Positions'][$x];

		//find date range
		$date_range_local = date_range($position['ShortingDate'], $position['ShortingDateEnd']);

		foreach ($date_range_local as $index => $day)
		{
			$date_object_box[$day]['positions'] += 1;
			$date_object_box[$day]['players'][] = $position['PositionHolder'];
			$date_object_box[$day]['companies'][] = $position['Name'];
		}

	}

	foreach ($company_shorted_dates as $day => $date)
	{
		$date_object_box[$day]['companies'] += 1;
	}

	if ($i > 1)
	{
		//break;
	}

	
	//gå igjennom datoene hvor selskapet har vært shorted og legg til i stats

}

foreach ($date_object_box as $key => $day)
{
	
	$date_object_box[$key]['companies'] = array_unique($date_object_box[$key]['companies']);
	$date_object_box[$key]['players'] = array_unique($date_object_box[$key]['players']);

	var_dump($date_object_box[$key]);

	$date_object_box[$key]['companies_count'] = count($date_object_box[$key]['companies']);
	$date_object_box[$key]['players_count'] = count($date_object_box[$key]['players']);

	//var_dump($date_object_box[$key]);
	echo $key . '. Players: ' . $date_object_box[$key]['players_count'] . ' | Positions: ' . $day['positions'] . ' | Companies: ' . $date_object_box[$key]['companies_count']  . '<br>';
}

//var_dump($date_object_box);

?>