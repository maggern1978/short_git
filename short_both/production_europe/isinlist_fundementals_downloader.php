
<?php 

//This is based on the main isin list in datacalc/isin/
	
//https://eodhistoricaldata.com/api/eod/MSFT.US?api_token=5da59038dd4f81.70264028
//5da59038dd4f81.70264028
include 'functions.php';
set_time_limit(14000);
	
$filename = '../short_data/dataraw/isin/hovedlisten_landkoder.csv';
$list = readCSV($filename);
$urlstart = 'https://eodhistoricaldata.com/api/fundamentals/';
$urlend = '?api_token=5da59038dd4f81.70264028';
	
$skippingcounter = 0; 
$errorcounter = 0;
$progresscounter = 0;
$savecounter = 0; 


foreach ($list as $key => $row)
{
	
	flush_start();
	$progresscounter++;
	if ($key % 200 == 0)
	{
		echo "\n";
		echo 'Status:' . "\n";
		echo 'Skipped: ' . $skippingcounter . "\n";
		echo 'Errors: ' . $errorcounter . "\n";
		echo 'Progress: ' . $progresscounter . "\n";
		echo 'Savecounter: ' . $savecounter . "\n";
		timestamp();
		echo "\n";
	}

	if (!isset($row[8]))
	{
		//echo 'Row[8] not set, skipping..<br>';
		$errorcounter++;
		continue;
	}

	$ticker = $row[1] . '.' . $row[8];
	$ticker = str_replace(' ', '-', $ticker);

	$url = $urlstart . $ticker . $urlend . '';
	$ticker = strtolower($ticker);
	
	$savepath = '../short_data/json/fundamentals/'. $ticker . '.json';
	$savepath = strtolower($savepath);

	//only save files if files are 5 workdays or more old
	$today = date('Y-m-d');
	$time = date('Y-m-d',(strtotime ( '-5 weekday' , strtotime ($today))));

	$success = 0;
	
	if (file_exists($savepath))
	{
		$filetime = filemtime($savepath);
		$filedate = date('Y-m-d', $filetime);	
		
		if ($time <= $filedate)
		{
			//echo $key . '. Skipping download of ' . $url . '. File date is ' . $filedate . "\n";
			$skippingcounter++;
			flush_end();
			continue;
		}
		else
		{
			$success = 1;
		}
		
	}
	else
	{
		$success = 1;
	}
	
	if ($success == 1)
	{
		if ($data = json_decode(download($url),true))
		{
			$array['SharesOutstanding'] = $data;
			$array['isinlistdata'] = $row;
			echo $key . '. Downloading ' . $url . '<br>';
			saveJSONsilent($array, strtolower($savepath));
			$savecounter++; 

		}
		else
		{
			//echo $key . '. Skipping download of ' . $url . '. ' ;
			//echo 'Download or json_decode error.';
			$errorcounter++;
			//var_dump($data);
		}
	}

	flush_end();


}
	
	
	
?>