
<?php

//$land = 'sweden';

include '../production_europe/options_set_currency_by_land.php';

$filename = '../short_data/datacalc/shortlisten_' .  $land . '_current.csv';

    //prepeare arrays for losers

                //Last inn data
    
                $csvFile = file($filename);            
                $data = [];
                foreach ($csvFile as $line) {
                     $data[] = str_getcsv($line); 
                } 
                
                //find file timestamp
                
                if (file_exists($filename)) {
                    $filetime = filemtime($filename);
                }

            $names = array();
            $percentageChange = array();
            $verdiendring = array();
            $count = count($data);
            $success = 0;
            $successcounter = 0;

            //finn data. Count er 30
            for ($i = $count-1; 0 < $i; $i--) 
            { 

                if ($data[$i][6]-$data[$i][7] <= 0 )
                {
                    continue;
                }

                if ($successcounter == 10) 
                {
                    break;
                }

                if ($data[$i][1] < 0) 
                {

                    $temp_navn = $data[$i][0];
                    $temp_navn = strtolower($temp_navn);
                    $temp_navn = ucwords($temp_navn);
                           
                    array_push($names, $temp_navn);
                    array_push($verdiendring, $data[$i][14]);

                    $kursDagenfor = $data[$i][6]-$data[$i][7];
                    $percentage = (($data[$i][7])/$kursDagenfor)*100;

                    $percentage = number_format($percentage,2,".","");
                    array_push($percentageChange, $percentage);
                    $success = 1;
                    $successcounter++;
                }
             
            }; 

///most losing 
// Turn on output buffering
ob_start();

?>

        <div class="col-lg-6">
          <div id="<?php echo $land; ?>11" style="width: 100%;height:350px;"></div>
          <script>
                var names = <?php echo json_encode($names); ?>;

            <?php 
            $counter = 0;
            foreach ($verdiendring as $endring) {
                echo 'var verdi' . $counter . ' = "' .  number_format($endring/1000000,2,".","") . '";';
                $counter++;
            }
            ?>

            </script>

          <script type="text/javascript">

        // based on prepared DOM, initialize echarts instance
        var myChart2<?php echo $land; ?> = echarts.init(document.getElementById('<?php echo $land; ?>11'));

        // specify chart configuration item and data
        var labelRight = {
            normal: {
                position: 'right',
                shadowColor: 'blue',
                ShadowBlur: 10,

            }
        };
        var labelLeft = {
            normal: {
                position: 'left',
                textBorderColor: '#f9f9f9',
                textBorderWidth: 2,
            }
        };

        var labelCenter = {
            normal: {
                position: 'inside',
                color: 'white',
           }
        };

        option = {

            toolbox: {
                show : true,
                itemSize: 15,
                feature : {
                    dataView : {
                    show: true,
                    title: 'Data',
                    lang: ['Toplist losing', 'Exit', 'Refresh'],
                    },
                },
            },

                grid: {
                top:    80,
                bottom: 10,
                left:   '5%',
                right:  '5%',
                },
            animation: false,
            backgroundColor: '#f6f6f6',
            textStyle: {
                color: 'black',
                fontSize: '14',
                fontFamily: ['Encode Sans','sans-serif'],
            },

            title: {
                text: 'Toplist: Where money was lost',
                subtext: 'Million <?php echo $currency_ticker; ?> Updated: <?php echo date("Y-m-d H:i", $filetime); ?>',
                link: 'http://sh/details_company_all.php?&land=<?php echo $land; ?>',
                sublink: 'http://sh/details_company_all.php?&land=<?php echo $land; ?>',
                left: 'center',
                subtextStyle: {
                    color: 'black',
                    fontSize: 14,

                }
            },
            tooltip : {
                trigger: 'item',
                axisPointer : {            
                    type : 'shadow',     
                }
            },

            xAxis: {
                type : 'value',
                position: 'top',
                splitLine: {lineStyle:{type:'dashed'}},
                axisLabel: {
                    fontSize: '16',
                }
            },
            yAxis: {
                type : 'category',
                axisLine: {show: false},
                axisLabel: {show: false},
                axisTick: {show: false},
                splitLine: {show: false},
                data: names,

            },
            series : [
            {
                name:'Toplist losers: Numbers in million <?php echo $currency_ticker;?>',
                type:'bar',

                label: {
                    normal: {
                        show: true,
                        formatter: '{b}', 
                    }
                },
                itemStyle: {
                    color: '#cc171d',
                },
                data:[

                <?php 
                $counter = 0;
                $sisteVerdi = count($verdiendring);
                $sisteVerdiTeller = $sisteVerdi;
                $labelRight = 'labelRight';
                $labelLeft = 'labelLeft';
                $labelCenter = 'labelCenter';

                foreach ($verdiendring as $key => $endring) {
                    $label = $labelLeft;

                    if ($key == 0) {
                        $label = $labelCenter;
                    }

                    if ($key == 1) {
                        $ratio = $verdiendring[1]/$verdiendring[0];
                        
                        if ($ratio > 0.65) {
                            $label = $labelCenter;
                        }                  
                        
                    }
                    if ($key == 2) {

                        $ratio = $verdiendring[2]/$verdiendring[0];
                        
                        if ($ratio > 0.65) {
                            $label = $labelCenter;
                        }  
                      
                    } 

                    echo '{
                        value: ' .  number_format($endring/1000000,2,".","") . ', 
                        label:' . $label . ', 
                        tooltip: {
                            backgroundColor: "black",
                            formatter: "{b}: <br>{c}M ' . $currency_ticker . ' (loss)';
                            echo '<br>Stock price: +' . $percentageChange[$counter] . '%"';
                        echo '},';
                    echo '},';
                    $counter++;
                    $sisteVerdiTeller--;
                }        
                ?>

                ]
            }
            ]
        };

        // use configuration item and data specified to show chart
        myChart2<?php echo $land; ?>.setOption(option);

        window.addEventListener('resize', function(event){
          myChart2<?php echo $land; ?>.resize();
          });
    </script>
 
</div>
</div>
</div>

<?php 
$filenameStart = '../short_data/html/most_losing_calc_';
$filenameEnd = '.html';

$filename = $filenameStart . $land . $filenameEnd;

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename , $htmlStr);
?>