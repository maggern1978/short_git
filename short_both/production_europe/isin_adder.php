<?php

set_time_limit(500);

$filename = '../production_europe/isin_adder/isin_adder_misslist.csv';
include_once '../production_europe/functions.php';
$isinliste = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv');
$isinliste_isinsortert = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv');

$file = fopen($filename,"r");

$csvdata = [];

while(!feof($file))
{
   $csvdata[] = fgets($file);
}

$csvdata = array_unique($csvdata);
$csvdata = array_values($csvdata);

fclose($file);

$keepContainer = [];

foreach ($csvdata as $key => $isin)
{

	$isin = trim($isin);

	if ($isin == '')
	{
		continue;
	}

	$row = '';

	if (is_isin(trim($isin)))
	{
		//try finding isin in isinlist

		if ($row = binary_search_isinliste_isin($isin, $isinliste_isinsortert)) //$needle, $haystack
		{
			echo $key . '. ' . $isin . ' Is isin, found in isinlist!';
			var_dump($isinliste_isinsortert[$row]);
			$row = $isinliste_isinsortert[$row];
		}
		else
		{
			echo $key . '. ' . $isin . ' Is isin, NOT found in isinlist!';
		}

	}
	else
	{
		echo 'not isin!<br>';
	}

	
	echo $key . '. searching for "' . $isin . '" ';

	if ($data = searchname($isin))
	{
		successecho(' success');
		$keepContainer[] = $data;
		var_dump($data);
	}
	else
	{
		errorecho('Search eodhistorical for ISIN gives no result. ');

		if (is_array($row))
		{

			$ticker = $row[1] . '.' . $row[8];

			$ticker = str_replace(' ', '-', $ticker);

			echo 'Searching for ' . $ticker . '<br>';

			if ($data = searchname($ticker))
			{
				successecho(' success');
				var_dump($data);
				$keepContainer[] = $data;

			}
		}
		else
		{
			echo 'False!';
		}


	}
	echo '<br>';
	exit();
}

	if ($data = searchnamefinnhub(trim($isin)))
	{
		

	}

saveJSON($keepContainer, '../production_europe/isin_adder/search_result.json');


//check if isin is already in list
foreach ($csvList as $key => $row)
{

	$foundsuccess = 0;

	foreach ($isinliste as $index => $isinrow)
	{
		if ($row[0] == $isinrow[0])
		{
			echo 'Isin already exists, unsetting...<br>';
			$foundsuccess = 1;
			break;
		}
	}

	if ($foundsuccess == 1)
	{
		unset($csvList[$key]);
	}
	
}

$csvList = array_values($csvList);

foreach ($csvList as $row)
{
	echo 'Adding ' . $row[2] . '<br>';
	$isinliste[] = $row;
}

usort($isinliste, function ($item1, $item2) {
    return mb_strtolower($item2[2]) < mb_strtolower($item1[2]);
});

//remove commas in names!

foreach ($isinliste as $key => $row)
{
	$isinliste[$key][2] = str_replace(',','', $row[2]);
}

echo '<br>';
saveCSVsemikolon($isinliste, '../short_data/dataraw/isin/hovedlisten_landkoder.csv');
echo '<br>';


//lagre sorted array etter isin
usort($isinliste, function ($item1, $item2) {
    return $item2[0] < $item1[0];
});

echo '<br>';
saveCSVsemikolon($isinliste, '../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv');
echo '<br>';


foreach ($outstandingList as $key => $row)
{

	$foundsuccess = 0;

	foreach ($tickerliste as $index => $isinrow)
	{
		if ($row[0] == $isinrow[0])
		{
			echo 'Isin already exists, unsetting...<br>';
			$foundsuccess = 1;
			break;
		}
	}

	if ($foundsuccess == 1)
	{
		unset($outstandingList[$key]);
	}
	
}

$outstandingList = array_values($outstandingList);

foreach ($outstandingList as $row)
{
	echo 'Adding ' . $row[2] . '<br>';
	$tickerliste[] = $row;
}

usort($tickerliste, function ($item1, $item2) {
    return mb_strtolower($item2[2]) < mb_strtolower($item1[2]);
});

//fjern kommaer!
foreach ($isinliste as $key => $row)
{
	$isinliste[$key][2] = str_replace(',','', $row[2]);
}

echo '<br>';
saveCSVsemikolon($tickerliste, '../short_data/datacalc/outstanding_shares_current_isinlist_main.csv');
echo '<br>';

//lagre sorted array etter isin
usort($tickerliste, function ($item1, $item2) {
    return $item2[0] < $item1[0];
});

echo '<br>';
saveCSVsemikolon($tickerliste, '../short_data/datacalc/outstanding_shares_current_isinlist_main_isinsortert.csv');
echo '<br>';

echo 'Resetting input file: ' . $filename . '<br>';
//$file = fopen($filename,"w");
//fclose($file);

function searchname($string)
{

	$token = '5da59038dd4f81.70264028';
	$starturl = 'https://eodhistoricaldata.com/api/search/';
	$endurl = '?api_token=' . $token;

	$url = $starturl . $string . $endurl;

	$download = download($url);

	if ($data = json_decode($download, true))
	{
		return $data;
	}
	else
	{
		var_dump($download);
		return false; 
	}

}

function getfundamentals($ticker)
{

	$token = '5da59038dd4f81.70264028';
	$starturl = 'https://eodhistoricaldata.com/api/fundamentals/';
	$endurl = '?api_token=' . $token;
	$url = $starturl . $ticker. $endurl;

	if ($data = json_decode(download($url), true))
	{
		return $data;
	}

	else
	{
		return false; 
	}
}

?>