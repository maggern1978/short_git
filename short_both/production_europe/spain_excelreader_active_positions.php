<?php

include_once '../production_europe/functions.php';
include_once '../production_europe/logger.php';
require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;

$saveurl = '../short_data/dataraw/spain/spain_current.csv';

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');

//$writer = new Xlsx($spreadsheet);
//$writer->save('hello world.xlsx');
echo '<br>';

$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("../short_data/dataraw/spain/spain_active_and_historical_download.xls");

$sheetnames = $spreadsheet->getSheetNames();
//var_dump($sheetnames);

//$spreadsheet->setActiveSheetIndex(0);

$dataArray = $spreadsheet->getActiveSheet()
->ToArray(
           // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
        );

//var_dump($dataArray);



if (!isset($dataArray[8]['B'])) {
	echo 'Data is not set! Download of active positions not working!';
	logger('Klarte ikke laste ned spanske active posisjoner!', '');
	return;
}

$holderBox = [];
$counter = 0;

//check which version of the spreadsheets that is being used

if ($dataArray[4]['B'] == 'LEI')
{
	
	foreach ($dataArray as $key => $Row) {

		if ($counter > 3 and isset($Row['C'])) {
			$rad2 = rtrim($Row['C']);
			$rad3 = $Row['D'];
			$rad4 = (string)$Row['E'];
			$rad4  = date("Y-m-d", strtotime($rad4));
			$rad5 = (string)$Row['F'];
			$lei = (string)$Row['B'];
			$lagre_array = array($rad3, $rad2, $lei, $rad5, $rad4);

			$holderBox[] = $lagre_array;
		}

		$counter++;
	}
}
else
{
	logger('lei not set in ', 'spain_execelreader_active_positions');
}

//ta alle selskapsnavnene
$companyArray = [];
foreach ($holderBox as $key => $row) {
	$companyArray[] = $row[0]; 
}

//gjør unike
$companyArray = array_unique($companyArray);

//ta et selskapsnavn

$activePositionsBox = [];

foreach ($companyArray as $key => $company) {
	
	//samle inn posisjoner
	echo '<br>Main round for company: ' . $company . '<br>';
	$posisjonsBox = [];
	$playerNameBox = [];

	foreach ($holderBox as $key => $row) {
		if ($company == $row[0]) {
			$posisjonsBox[] = $row;
			$playerNameBox[] = $row[1];
		}
	}

	//var_dump($posisjonsBox);

	$playerNameBox = array_unique($playerNameBox);

	$count = 1;
	//ta hvert navn for hvert selskap
	foreach ($playerNameBox as $key => $playername) {
		//echo 'Looking for ' . $playername . '<br>';
		foreach ($posisjonsBox as $row) {
			if ($playername == $row[1]) {

				echo $count++ . '. Found playername: ' . $playername . '<br>';

				$activePositionsBox[] = $row;
				break;
			}
		}

	}
}


saveCSVx($activePositionsBox, $saveurl);



?>