<?php
switch ($land) {
  case "norway":
  $tradingviewprefix = 'OSL:';
  break;
  case "sweden":
  $tradingviewprefix = 'OMXSTO:';
  break;
  case "denmark":
  $tradingviewprefix = 'OMXCOP:';
  break;
  case "finland":
  $tradingviewprefix = 'OMXHEX:';
  break;
  case "germany":
  $tradingviewprefix = 'XETR:';
  break;  
}

$tickertradingview = $tickertradvingviewlink =  $tradingviewprefix . $tickertradingview;
$tickertradingview = str_replace("-", "_", $tickertradingview);
$tickertradvingviewlink = str_replace("-", ":", $tickertradvingviewlink );

//var_dump($tickertradvingviewlink);

if ($land == 'germany')
{
	
	if (strripos($ticker, 'xetra') == false)
	{
		return;
	}
}


?>

<div class="container mb-3">
	<div class="row ">
		<div class="col-12 col-sm-12 col-md-8 padding-left-0">
			<!-- TradingView Widget BEGIN -->
			<div class="tradingview-widget-container">
				<div class="tradingview-widget-container__widget"></div>
				<div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/<?php echo $tickertradvingviewlink; ?>/" rel="noopener" target="_blank"><span class="blue-text"><?php echo  $companynametradingview; ?> Fundamental Data</span></a> by TradingView</div>
				<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-financials.js" async>
					{
						"symbol": "<?php echo $tickertradingview;?>",
						"colorTheme": "light",
						"isTransparent": true,
						"largeChartUrl": "",
						"displayMode": "regular",
						"width": "100%",
						"height": "800",
						"locale": "en"
					}
				</script>
			</div>
			<!-- TradingView Widget END -->
		</div>
		<div class="col-12 col-sm-12 col-md-4 ">
			<!-- TradingView Widget BEGIN -->
			<div class="tradingview-widget-container">
				<div class="tradingview-widget-container__widget"></div>
				<div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/<?php echo $tickertradvingviewlink; ?>/" rel="noopener" target="_blank"><span class="blue-text"><?php echo $companynametradingview; ?> Profile</span></a> by TradingView</div>
				<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-symbol-profile.js" async>
					{
						"symbol": "<?php echo $tickertradingview;?>",
						"width": "100%",
						"height": "auto",
						"colorTheme": "light",
						"isTransparent": true,
						"locale": "en"
					}
				</script>
			</div>
			<!-- TradingView Widget END -->

		</div>

	</div>

</div>
</div>
<?php //var_dump($financials); ?>