<?php

//husk, den tar to ark denne filen!
//delete contents of file 
$saveurl = '../short_data/dataraw/spain.history/spain.history_current.csv';

include_once '../production_europe/functions.php';

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');

//$writer = new Xlsx($spreadsheet);
//$writer->save('hello world.xlsx');


$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("../short_data/dataraw/spain/spain_active_and_historical_download.xls");

$sheetnames = $spreadsheet->getSheetNames();
//var_dump($sheetnames);
$spreadsheet->setActiveSheetIndex(0);
//$spreadsheet->setActiveSheetIndex(0);

$dataArray = $spreadsheet->getActiveSheet()
    ->ToArray(
           // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
    );

//var_dump($dataArray);
if (!isset($dataArray[8]['B'])) {
	echo 'Data is not set! Download of active positions not working!';
	logger('Klarte ikke laste ned spanske active posisjoner!', '');
	return;
}

$holderBox = [];
$counter = 0;

//check which version of the spreadsheets that is being used
echo '<br>';

if ($dataArray[4]['B'] == 'LEI')
{
	echo '"LEI" found in line 4, column B<br>';
	foreach ($dataArray as $key => $Row) {

		if ($counter > 3 and isset($Row['C'])) {
			$rad2 = rtrim($Row['C']);
			$rad3 = $Row['D'];
			$rad4 = (string)$Row['E'];
			$rad4  = date("Y-m-d", strtotime($rad4));
			$rad5 = (string)$Row['F'];
			$lagre_array = array($rad3, $rad2, '', $rad5, $rad4);

			$holderBox[] = $lagre_array;
		}

		$counter++;
	}
}
else
{
	echo '"LEI" NOT found in line 4, column B<br>';
	foreach ($dataArray as $key => $Row) {

		if ($counter > 3 and isset($Row['B'])) {
			$rad2 = rtrim($Row['B']);
			$rad3 = $Row['C'];
			$rad4 = (string)$Row['D'];
			$rad4  = date("Y-m-d", strtotime($rad4));
			$rad5 = (string)$Row['E'];
			$lagre_array = array($rad3, $rad2, '', $rad5, $rad4);

			$holderBox[] = $lagre_array;
		}

		$counter++;
	}
}

//ta alle selskapsnavnene
$companyArray = [];
foreach ($holderBox as $key => $row) {
	$companyArray[] = $row[0]; 
}

//gjør unike
$companyArray = array_unique($companyArray);

//ta et selskapsnavn

$activePositionsBox = [];
$globalPositionCount = 0;

foreach ($companyArray as $key => $company) {
	
	//samle inn posisjoner
	echo '<br>Main round for company: ' . $company . '<br>';
	$posisjonsBox = [];
	$playerNameBox = [];

	$localPositionCount = 0;

	foreach ($holderBox as $key => $row) {
		if ($company == $row[0]) {
			$posisjonsBox[] = $row;
			$playerNameBox[] = $row[1];
			$localPositionCount++;
		}
	}

	
	echo 'Global positions: ' . $globalPositionCount . '. Local: ' . $localPositionCount . '<br>';
	//var_dump($posisjonsBox);

	$playerNameBox = array_unique($playerNameBox);

	$count = 0;
	//ta hvert navn for hvert selskap
	foreach ($playerNameBox as $key => $playername) {
		//echo 'Looking for ' . $playername . '<br>';
		foreach ($posisjonsBox as $index => $row) {
			if ($playername == $row[1]) {
				echo $count++ . '. Found playername: ' . $playername . ' (Unsetting the position in main array)<br>';
				$activePositionsBox[] = $row;
				//var_dump($posisjonsBox);
				unset($holderBox[$globalPositionCount + $index]);
				break;
			}
		}

	}
	$globalPositionCount += $localPositionCount;
}

$holderBox = array_values($holderBox);

//nå er alle historiske posisjoner samlet inn fra det første excel-arket, nå må vi ta inn resten også

$spreadsheet->setActiveSheetIndex(1);

$dataArray = $spreadsheet->getActiveSheet()
    ->ToArray(
           // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
    );



if ($dataArray[4]['B'] == 'LEI')
{
	echo '"LEI" found in line 4, column B<br>';
	foreach ($dataArray as $key => $Row) {

		if ($counter > 3 and isset($Row['C'])) {
			$rad2 = rtrim($Row['C']);
			$rad3 = $Row['D'];
			$rad4 = (string)$Row['E'];
			$rad4  = date("Y-m-d", strtotime($rad4));
			$rad5 = (string)$Row['F'];
			$lagre_array = array($rad3, $rad2, '', $rad5, $rad4);

			$holderBox[] = $lagre_array;
		}

		$counter++;
	}
}
else
{
	echo '"LEI" NOT found in line 4, column B<br>';
	foreach ($dataArray as $key => $Row) {

		if ($counter > 3 and isset($Row['B'])) {
			$rad2 = rtrim($Row['B']);
			$rad3 = $Row['C'];
			$rad4 = (string)$Row['D'];
			$rad4  = date("Y-m-d", strtotime($rad4));
			$rad5 = (string)$Row['E'];
			$lagre_array = array($rad3, $rad2, '', $rad5, $rad4);

			$holderBox[] = $lagre_array;
		}

		$counter++;
	}
}


//var_dump($holderBox);

//sortere alle posisjoner på dato
usort($holderBox, function($a, $b) {
    return $a[4] < $b[4];
});

//Lagre
saveCSVx($holderBox, $saveurl);
echo 'Saved ' . $saveurl . '<br>';



?>