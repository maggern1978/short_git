<?php 

//$land = 'norway';

require '../production_europe/namelink.php';
require '../production_europe/logger.php';
require '../production_europe/functions.php';

date_default_timezone_set('Europe/Oslo');

echo '<br>';

$land = 'finland';

//read todays positions
if (!$currentdata = readJSON('../short_data/dataraw/' . $land . '/' . $land . '_current.json'))
{
	errorecho('Error reading json ' . $land . '_current.json' . '. Returning!<br>');
	return;

}

//find the newest position date
$newestdate = '2001-01-01';

foreach ($currentdata as $key => $company)
{
	foreach ($company['Positions'] as $key => $position)
	{
		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}
}



if (!shouldEventsRun($newestdate))
{
	errorecho('Newest date in positions is ' . $newestdate . ', which is not the previous workday or today. Returning.');
	return;
}

//todo format the date

$newestdate = date('Y-m-d', (strtotime($newestdate)));

$today = date('Y-m-d');
$yesterday = date('Y-m-d', (strtotime ('-1 weekday', strtotime ($today))));

echo 'Newest date in current is ' . $newestdate . '<br>';
echo 'Previous working day is ' . $yesterday . '<br>';


if ($newestdate != $yesterday)
{
	errorecho ('Newest date found in todays positions is not from yesterday, as expected. Possible problem/exeption.<br>');
}

if ($newestdate == $today)
{
	echo 'Neweste position is from today. Setting target date for previous positions file name to one workday before.<br>';

	$newestdate = date('Y-m-d', (strtotime ('-1 weekday', strtotime ($newestdate))));
}

//if neweste date == today, 
echo 'Targetfile is then ' . $newestdate . '.' . $land . '.json, because that file has positions from the day before.<br> ';

if (!$previousdata = readJSON('../short_data/dataraw/' . $land . '/' . $newestdate . '.' . $land . '.json'))
{
	errorecho('Error reading current, returning');
	return;
}

//collect all changes names from new list

$changesCompanyBox = [];

foreach ($currentdata as $key => $currentcompany)
{
	
	//find same company in previous and check for changes
	$targetName = strtolower($currentcompany['Name']);
	$targetIsin = strtolower($currentcompany['ISIN']);

	$success = 0;
	$foundcount = 0;
	foreach ($previousdata as $index => $previouscompany)
	{

		if ($targetName == strtolower($previouscompany['Name']) or $targetIsin == $previouscompany['ISIN'])
		{
			$success = 1;
			$foundcount++;
			echo $key . '. ' . $targetName . ' exists in both current and previous. Checking for differences: ';

			if ($currentcompany['ShortPercent'] == $previouscompany['ShortPercent'] and $currentcompany['NumPositions'] == $previouscompany['NumPositions'])
			{
				echo 'Shortpercent and number of positions are identical.';
			}
			else
			{
				echo 'Shortpercent and number of positions are <strong>NOT</strong> identical. Adding previous data to current. ';
				$currentdata[$key]['previousShortPercent'] = $previouscompany['ShortPercent'];
				$currentdata[$key]['previousNumPositions'] = $previouscompany['NumPositions'];
				$currentdata[$key]['status'] = 'update';
				$changesCompanyBox[] = $currentdata[$key];
			}
			if ($foundcount > 1)
			{
				errorecho('Findcount er ' . $foundcount . ' meaning erorr with names');
			}
			
			echo '<br>';
			
		}

	}

	if ($success == 0)
	{
		$currentdata[$key]['status'] = 'new';
		$currentdata[$key]['previousShortPercent'] = 0;
		$currentdata[$key]['previousNumPositions'] = 0;
		$changesCompanyBox[] = $currentdata[$key];
		echo $key . '. <strong>' . $targetName . ' not found in previouspositions</strong><br>';
	}

}

//now find positions that are ended, meaning previousposition companies that are not in current. 
echo '<br>';
echo 'Finding companies that are in previous, but not in current, meaning short has gone to zero. <br>';
$endedcount = 0;

foreach ($previousdata as $key => $previouscompany)
{

	$targetName = strtolower($previouscompany['Name']);
	$targetIsin = $previouscompany['ISIN'];

	$success = 0;
	foreach ($currentdata as $index => $currentcompany)
	{

		if ($targetName == strtolower($currentcompany['Name']) or $targetIsin == $currentcompany['ISIN'])
		{
			echo $key . '. ' . $targetName . ' from previous also exists in current, skipping to next. ';
			$success = 1;
			break;
		}
	}

	if ($success == 0)
	{
		echo $key . '. ' . $targetName . ' from previous is <strong>NOT</strong> found in current positions, adding to changesCompanyBox ';
		$previousdata[$key]['status'] = 'ended';
		$previousdata[$key]['previousShortPercent'] = $previousdata[$key]['ShortPercent'];
		$previousdata[$key]['previousNumPositions'] = $previousdata[$key]['NumPositions'];
		$previousdata[$key]['ShortPercent'] = 0;
		$previousdata[$key]['NumPositions'] = 0;
		$changesCompanyBox[] = $previousdata[$key];
		$endedcount++;

		//var_dump($previousdata[$key]);

	}
 
 	echo '<br>';

}

echo 'Positions that ended: ' . $endedcount . '<br>';
echo '<br>';

//set lastchange date

$count = count($changesCompanyBox);

for ($i = 0; $i < $count; $i++)
{
	$newestdate = '2001-01-01';

	foreach ($changesCompanyBox[$i]['Positions'] as $position)
	{

		if ($position['ShortingDate'] > $newestdate)
		{
			$newestdate = $position['ShortingDate'];
		}
	}

	$changesCompanyBox[$i]['LastChange'] = $newestdate;
}


//remove positions as they are not needed

$count = count($changesCompanyBox);

for ($i = 0; $i < $count; $i++)
{
	unset($changesCompanyBox[$i]['Positions']);
}

//set change

for ($i = 0; $i < $count; $i++)
{
	if (!isset($changesCompanyBox[$i]['previousShortPercent']))
	{
		errorecho('Error! missing previousShortPercent<br>');
		var_dump($changesCompanyBox[$i]);
		continue;
	}

	$changesCompanyBox[$i]['change'] = $changesCompanyBox[$i]['ShortPercent'] - $changesCompanyBox[$i]['previousShortPercent'];
	$changesCompanyBox[$i]['change'] = round($changesCompanyBox[$i]['change'],2);

}

//sort the positons
usort($changesCompanyBox, function ($item1, $item2) {
  return $item2['change'] > $item1['change'];
});

foreach ($changesCompanyBox as $key => $company)
{
	$changesCompanyBox[$key] = float_format($company);
}




saveJSON($changesCompanyBox, '../short_data/json/events/company/' . $land . '.events.company.current.json');

//var_dump($changesCompanyBox);


?>