<?php 
echo '<strong>run.php starter </strong>';
if (!function_exists('run')) {
	function run($filename) {
		$prodname = '../production_europe/' . $filename;

		global $land;
		global $inputFileName;
		global $type;

		if (file_exists($prodname)) {
			try {
				echo '<br><strong>' . $prodname . ' starter. </strong>' . date('H:i:s');
	    		require $prodname;
			} catch (Exception $e) {
			    error_log($e);
			}

			echo ' | DONE<br>';

		}
		else 
		{
			$filename = '../short_data/' . $filename;
			try {
				echo '<br><strong>' . $filename . ' starter. </strong>' . date('H:i:s');
			    require $filename;
			} catch (Exception $e) {
			    error_log($e);
			}
			echo ' | DONE' . date('H:i:s') . '<br>';
		}
	}
} 
echo ' | DONE' . date('H:i:s') . '<br>';
?>