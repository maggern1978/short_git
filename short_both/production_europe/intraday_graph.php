<?php

//this file only reads and outputs a html

set_time_limit(30);
date_default_timezone_set("Europe/Oslo");

include('../production_europe/run.php');
run('isopen.php');

//$land = 'sweden';

//if (!isopen($land)) 
//{
  //echo '<strong>' . $land . ' is closed!... returning! </strong><br>';
  //return;  
//} 

//$land = 'norway';

include_once '../production_europe/functions.php';

switch ($land) {
  case "norway":
  $indexname = 'OSEBX';
  break;
  case "sweden":
  $indexname = 'OMXSPI';
  break;
  case "denmark":
  $indexname = 'OMXC20';
  break;
  case "germany":
  $indexname = 'GDAXI';
  break;
  case "finland":
  $indexname = 'OMXHPI';
  break;
  case "germany":
  $indexname = 'DAX';
  break;
  case "france":
  $indexname = 'CAC 40';
  break;      
  case "spain":
  $indexname = 'IBEX 35';
  break;
  case "united_kingdom":
  $indexname = 'FTSE 100';
  break; 
  case "italy":
  $indexname = 'FTSE MIB';
  break;   
  default:
  echo 'Land not set';
  return;
}

$intradayjson = $land . '_intraday.json';
$filefolder = '../short_data/intraday/';

//read index data

if (!$data = readJSON($filefolder . $intradayjson))
{
  echo 'Error reading ' . $filefolder . '<br>';
  return;
}

var_dump($data);

//filter data to only today
$today = (string)date('Y-m-d');
$todayText = strtolower(date('l'));


$timeSlots = [];
$indexPercentChange = [];
$index = [];
$shortValue = [];
$shortValuePercent = [];
$indexLastvalue = '';
$indexLastvaluePercent = '';
$shortLastvalue = '';
$shortLastvaluePercent = '';

$temparray = [];

$count = count($data);

for ($i = 0; $i < $count; $i++)
{
  if ($data[$i]['date'] == $today)
  {
    $data[$i]['time'] = date('H.i', strtotime($data[$i]['time']));
    $timeSlots[] = $data[$i]['time'];


    $indexPercentChange[] = $data[$i]['indexPercentChange'];
    $index[] = round($data[$i]['close'],2);
    $shortValue[] = round(($data[$i]['positionsValueChange']/1000000),2);
    $shortValuePercent[] = $data[$i]['positionsPercentChange'];
    
    //last values for headings etc
    $indexLastvalue = round($data[$i]['close'],2);
    $indexLastvaluePercent = round($data[$i]['indexPercentChange'],2);
    $shortLastvalue = round($data[$i]['positionsValueChange']/1000000,2);
    $shortLastvaluePercent = round($data[$i]['positionsPercentChange'],2);
    
    echo 'Shortvalues are: ' . '<br>';
    echo $indexLastvalue;
    echo '<br>';
    echo $indexLastvaluePercent;
    echo '<br>';
    echo $shortLastvalue;
    echo '<br>';
    echo $shortLastvaluePercent;
    echo '<br>';
    
    var_dump($data[$i]);
  }
}

if ($shortLastvaluePercent > 0)
{
  $plusprefixShorts = '+';
}
else {
  $plusprefixShorts = '';
}

if ($indexLastvaluePercent > 0)
{
  $plusprefixIndex = '+';
}
else {
  $plusprefixIndex = '';
}


//include '../short_data/header.html';

$legendtextAndName = 'Short pos. ' . $plusprefixShorts . $shortLastvalue  . 'M ' . $plusprefixShorts . $shortLastvaluePercent . ' %'; 
$legendtextAndNameIndex = $indexname . ': ' . $indexLastvalue . ' ' . $plusprefixIndex . $indexLastvaluePercent . ' %'; 

echo $legendtextAndName . '<br>';
echo $legendtextAndNameIndex . '<br>';

ob_start();
?>
<div id="intraday<?php echo $land; ?>" style="width: 100%;height:357px;"></div>
<script>

  var time_<?php echo $land; ?> = <?php echo json_encode($timeSlots); ?>;
  var index_<?php echo $land; ?> = <?php echo json_encode($indexPercentChange); ?>;
  var indexprice_<?php echo $land; ?> = <?php echo json_encode($index); ?>;
  var shortValue_<?php echo $land; ?> = <?php echo json_encode($shortValue); ?>;
  var shortValuePercent_<?php echo $land; ?> = <?php echo json_encode($shortValuePercent); ?>;

        // based on prepared DOM, initialize echarts instance
        var myChart5<?php echo $land; ?> = 
        echarts.init(document.getElementById('intraday<?php echo $land; ?>'));

        var w;
        var h;
        var left;
        var right;
        var headingFontSize;
        var fontSizen;
        var toppen;

        setsizes();

        function setsizes() {
          w = myChart5<?php echo $land; ?>.getWidth();
          h = window.innerHeight;

          if (w >= 1200) {
           left = '15%';
           right = '4%';
           headingFontSize = 18;
           fontSizen = '14';
           toppen = '70';
           console.log('1200');
         }
         else if (w >= 992) {
           left = '15%';
           right = '4%';
           headingFontSize = 18;
           fontSizen = '14';
           toppen = '70';
           console.log('992');
         }
         else if (w >= 768) {
           left = '6%';
           right = '3%';
           headingFontSize = 18;
           fontSizen = '14';
           toppen = '70';
           console.log('768');
         }
         else if (w >= 576) {
           left = '9%';
           right = '3%';
           headingFontSize = 16;
           fontSizen = '12';
           toppen = '70';
           console.log('576');
         }
         else if (w >= 440) {
           left = '12%';
           right = '3%';
           headingFontSize = 15;
           fontSizen = '12';
           toppen = '170';
           console.log('440');
         }
         else if (w >= 352) {
           left = '12%';
           right = '3%';
           headingFontSize = 15;
           fontSizen = '12';
           toppen = '70';
           console.log('392');
         }
         else {
           left = '18%';
           right = '3%';
           headingFontSize = 15;
           fontSizen = '14';
           toppen = '90';
           console.log('else');
         }
       }

       option = {
        toolbox: {
          show : true,
          itemSize: 12,
        },
        textStyle: {
          fontFamily: ['Encode Sans','sans-serif'],
        },
        grid: {
          top:  toppen,
          bottom: 60,
          left:   left,
          right:  right,
        },
        animation: false,
        backgroundColor: '#f6f6f6',
        title: {
      //backgroundColor: '#000000',
      left: 'left',
      padding: [10,10,10,10],
      itemGap: 100,
      text: 'Intraday <?php echo ucwords($land) . ' ' . $todayText ; ?>',
      textStyle: {
        color: '#000000',
        fontSize: headingFontSize,
        width: 400,
      },
      subtextStyle: {
        height: 30,
      },
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        crossStyle: {
          color: '#999'
        }
      },
      backgroundColor: '#000000',
      confine: true,
      //formatter: '{a} </br>Change: {c}%</br> Time: {b} {x}',
      formatter: function (params,ticket,callback) {
          //console.log(params)
          
          if (typeof params[1] === 'undefined' || params[1] === null) {
            var start = params[0].seriesName.substr(0, 4);
            if (start == 'Short') {
              var res = 'Time: ' + time_<?php echo $land; ?>[params[0].dataIndex] + '<br/>';
              res += 'All shorts: ' + shortValue_<?php echo $land; ?>[params[0].dataIndex] + ' M <br/>';
              res += 'Change: ' + shortValuePercent_<?php echo $land; ?>[params[0].dataIndex] + '%';
            }
            else {
              var res = 'Time: ' + time_<?php echo $land; ?>[params[0].dataIndex] + '<br/>';
              res += '<?php echo $indexname;?>: ' + indexprice_<?php echo $land; ?>[params[0].dataIndex] + '<br/>';
              res += 'Change: '+ index_<?php echo $land; ?>[params[0].dataIndex] + '%<br/>';
            }
          }
          else {
            var res = 'Time: ' + params[1].name + '<br/>';
            res += '<?php echo $indexname;?>: ' + indexprice_<?php echo $land; ?>[params[0].dataIndex] + '<br/>';
            res += 'Change: '+ + params[0].value + '%<br/><br/>';
            res += 'All shorts: ' + shortValue_<?php echo $land; ?>[params[0].dataIndex] + ' M';
            res += ' <br/>Change: ' + params[1].value + '%<br/>';
          }

          for (var i = 0, l = params.length; i < l; i++) {
            //res += params[i].dataIndex;
              //res += '<br/>' + params[i].seriesName; //+ ' : ' + params[0].value;
              //res += '<br/>' + params[i].seriesName; //+ ' : ' + params[1].value;
              //res += '<br/>'   ;
            }
            setTimeout(function (){
              callback(ticket, res);
            }, 0)
            return 'loading';
          }

        },
        legend: {
          top: 30,
          textStyle: {
            fontSize: fontSizen,
          },
          data: [{
            name: '<?php echo $legendtextAndNameIndex;?>',
        // compulsorily set icon as a circle
        icon: 'circle',
        // set up the text in red
        textStyle: {
          color: 'black',
        }
      }, 
      {
        name: '<?php echo $legendtextAndName; ?>',
        // compulsorily set icon as a circle
        icon: 'circle',
        // set up the text in red
        textStyle: {
          color: 'black'
        },
      }

      ],
    },
    xAxis: {
     type: 'category',
      //boundaryGap: false,
      //name: 'test',
      splitLine: {
        show: true,
        lineStyle: {
          type: 'dashed',
          width: 0,
          interval: 23,
        }
      },
      splitArea: {
        show: true,
        interval: 23,
      },
      //splitNumber: 10,
      axisTick: {
        show: true,
        inside: true,
        length: 7,
        interval: 23,
        lineStyle: {
          width: 2,
          show: true,
          type: 'solid',
        },

      },
      data: time_<?php echo $land; ?>,
      axisLine: {
        lineStyle: {
          width: 3,
          color: 'black',
        },
      },
    },
    yAxis: {
      type: 'value',
      name: '15-30 min delay. Updated <?php echo date('Y-m-d H:i');?> ',
      axisLabel: {
        formatter: '{value}%'
      },
      nameTextStyle: {
        fontSize: 11,
        padding: [15,0,0,150],
        align: 'left',
      },
      nameRotate: '0',
      nameLocation: 'start',
      splitLine: {
        lineStyle: {
          type: 'dashed'
        }
      },
      axisTick: {
        show: true,
        interval: 1,
        lenght: 1,
      },
    },
    series: [
    {
      data: index_<?php echo $land; ?>,
      //connectNulls: true,
      name: '<?php echo $legendtextAndNameIndex;?>',
      type: 'line',
      legendHoverLink: 'true',
      symbol: 'none',
      lineStyle: {
        width: 4,
        //color: 'red',
      },
      tooltip: {
        backgroundColor: '#00000',
        formatter: '{a} asdf :',
        tooltip: {
          formatter: 'as fasfd',
        }
      }
    },
    {
      data: shortValuePercent_<?php echo $land; ?>,
      //connectNulls: true,
      name: '<?php echo $legendtextAndName; ?>',
      type: 'line',
      symbol: 'none',
      tooltip: {
        backgroundColor: '#00000',
        formatter: '{a}: sadgffd',
      },
      lineStyle: {
        width: 4,
        color: '#2f4554',
      },
    }]
  };
    //ide: legg inn data så størrelser endrer seg.
     // use configuration item and data specified to show chart
     myChart5<?php echo $land; ?>.setOption(option);
     window.addEventListener('resize', function(event){

      setsizes();

      option = {
        grid: {
          top:  toppen,
          bottom: 60,
          left:   left,
          right:  right,
        },
        title: {
          textStyle: {
            fontSize: headingFontSize,
          }
        },
        legend: {
          top: 36,
          textStyle: {
            fontSize: fontSizen,
          }
        }
      }
      myChart5<?php echo $land; ?>.setOption(option);       
      myChart5<?php echo $land; ?>.resize();

    });

  </script>



<?php //  Return the contents of the output buffer
//$tid = date("h-i-s");
//echo $tid;
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents('../short_data/html/' . 'intraday_' . $land . '.html', $htmlStr);


?>







