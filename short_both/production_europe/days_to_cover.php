<?php

$stockPricesUrl = '../short_data/history/';
$filename = '../short_data/datacalc/shortlisten_' . $land . '_current.csv';
$land2 = $land;
$landnavn = ucwords($land);

include '../production_europe/options_set_currency_by_land.php';

if (!function_exists('saveCSV2')) {

    //Lagre nøkkeldata funksjon
    function saveCSV2($input, $file_location_and_name) {
    $fp = fopen($file_location_and_name, 'w');

    foreach ($input as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
    }
}

//$filename = 'datacalc/shortlisten_nor_current.csv';
$csvFile = file($filename);            
$data = [];
foreach ($csvFile as $line) {
    $data[] = str_getcsv($line); 
}

$selskapsnavn = array();
$days_to_cover = array();
$volume = array();
$counter = 0; 
$shortede_aksjer = array();


//push navn og finn days_to_cover for hvert selskap
foreach ($data as $key => $entries) {
	if ($key > 0) {
	    
	    if (!isset($entries[2]))
        {
            continue;
        }

        $targetTicker = $entries[2];
        //$targetTicker = str_replace('-', ' ', $targetTicker);

	    $targetFileName = $stockPricesUrl . $targetTicker . '.json';
	    
	    $targetFileName = strtolower($targetFileName);
	   
	    if (file_exists($targetFileName)) {
	         //echo '<br>Found ' . $targetFileName;
	         
	        $string = file_get_contents($targetFileName);
            $json = json_decode($string, true);
            
            $totalvolume = 0;
            $runcounter = 0;
            
            foreach ($json as $indx => $stockday) {
               foreach ($stockday as $teller => $input) {
               
                    $totalvolume = (int)$totalvolume + (int)$input['volume'];

               }
               if ($runcounter == 19) {
                    break;
                }
               $runcounter++;
            }
            
            if ($totalvolume > 0) {
                $averageVolume = $totalvolume/$runcounter;
            }
            else {
                $averageVolume = 0;
            }

            
            
            if ((float)$entries[4] == 0 or $averageVolume == 0) {
                $days_to_cover[] = (float)1;
            }
            else {
                 $day_to_cover_local = number_format(((float)$entries[4]/(float)$averageVolume),1,".","");
                 $days_to_cover[] = $day_to_cover_local;
            }
            
	    }
	    else {
	       echo '<br><br>MISS! Did not find file ' . $targetFileName . '<br><br>';
	       continue;
           //$days_to_cover[] = number_format(((float)$entries[4]/(float)$entries[10]),1,".","");

	    }
        
        //update datacalc/shortlisten with volumes. 
        $data[$key][10] = round($averageVolume,0);

    	$selskapsnavn[] = $entries[0];
    	//$days_to_cover[] = number_format(((float)$entries[4]/(float)$entries[10]),1,".","");
    	$volume[] = $entries[10];
    	$shortede_aksjer[] = $entries[4];
	}
	//

}

///save /update datacalc/shortlisten with volumes. 
saveCSV2($data, $filename );

//sorter dataene:
array_multisort($days_to_cover, SORT_DESC, 
				$selskapsnavn,
				$volume,
				$shortede_aksjer);

//ta topp ti
$topp_ti_navn = array();
$topp_ti_days_to_cover = array();
$topp_ti_volume = array();
$topp_ti_shortede_aksjer = array();
$counter = 0;

foreach ($days_to_cover as $day_to_cover) {
	if ($counter < 10) { //topp ti

        $temp_navn = $selskapsnavn[$counter];
        
        $temp_navn = strtolower($temp_navn);
        $temp_navn = ucwords($temp_navn);
            //echo 'hit';
    
		$topp_ti_navn[] = $temp_navn ;
		$topp_ti_volume[] = $volume[$counter];
		$topp_ti_days_to_cover[] = $day_to_cover;
		$topp_ti_shortede_aksjer[] = $shortede_aksjer[$counter];
	}
	$counter++;
}

// Turn on output buffering
ob_start();

?>
<div class="col-lg-6">
<div id="<?php echo $land; ?>77" style="width: 100%;height:350px;"></div>
          <script>
                var names = <?php echo json_encode($topp_ti_navn); ?>;

                    window.onresize = function() {
            $(".<?php echo $land; ?>77").each(function(){
                var id = $(this).attr('_echarts_instance_');
                window.echarts.getInstanceById(id).resize();
            });
        };
            </script>

          <script type="text/javascript">

       //var style = window.getComputedStyle(main10);
       //var width = parseFloat(style.width);

       //var x = width;
       //var y = 'px';
       //var z = x + y;
       //document.getElementById('main10').style.width=z;

        // based on prepared DOM, initialize echarts instance
        var myChartd<?php echo $land; ?> = echarts.init(document.getElementById('<?php echo 
            $land; ?>77'));
        // var myChart1 = echarts.init(document.getElementById('main10'), {renderer: 'svg', devicePixelRatio: '2'});

        // specify chart configuration item and data
        var labelRight = {
            normal: {
                position: 'right',
                textBorderColor: '#f9f9f9',
                textBorderWidth: 2,

            }
        };
        var labelLeft = {
            normal: {
                position: 'left',
                textBorderColor: '#f9f9f9',
                textBorderWidth: 2,
            }
        };

        var labelCenter = {
            normal: {
                position: 'inside',
                color: 'white',
           }
        };

        option = {

            toolbox: {
                show : true,
                itemSize: 15,
                feature : {
                    dataView : {
                    show: true,
                    title: 'Data',
                    lang: ['Days to cover', 'Exit', 'Refresh'],
                    },
                },
            },


            grid: {
            top:    80,
            bottom: 10,
            left:   '5%',
            right:  '5%',
            },
            animation: false,
            backgroundColor: '#f6f6f6',
            textStyle: {
                color: 'black',
                fontSize: '14',
                fontFamily: ['Encode Sans','sans-serif'],
            },

            title: {
                text: 'Days to cover-ratio <?php echo $landnavn;?>',
                link: 'days_to_cover_index.php?country=<?php echo $land2;?>',
                subtext: 'Shorted stocks divided on average volume the previous 20 days.',
                sublink: 'days_to_cover_index.php?country=<?php echo $land2;?>',
                left: 'center',
                subtextStyle: {
                    color: 'black',

                }
            },
            tooltip : {
                trigger: 'item',
                axisPointer : {            
                    type : 'shadow',     
                }
            },
            xAxis: {
                type : 'value',
                position: 'top',
                splitLine: {lineStyle:{type:'dashed'}},
                axisLabel: {
                    fontSize: '16',
                }
            },
            yAxis: {
                type : 'category',
                axisLine: {show: false},
                axisLabel: {show: false},
                axisTick: {show: false},
                splitLine: {show: false},
                data: names,

            },
            series : [
            {
                name:'Days to cover: Numbers in days <?php echo $currency_ticker;?>',
                type:'bar',

                label: {
                    normal: {
                        show: true,
                        formatter: '{b}', 
                    }
                },
                itemStyle: {
                    color: '#354655',
                },
                data:[

                             <?php 
                $counter = 0;

                $labelRight = 'labelRight';
                $labelLeft = 'labelLeft';
                $labelCenter = 'labelCenter';


                foreach ($topp_ti_days_to_cover as $key => $day_to_cover) {
                    
                    $label = $labelRight;

                    if ($key == 0) {
                        $label = $labelCenter;
                    }

                    if ($key > 0) {
                        $ratio = $topp_ti_days_to_cover[$key]/$topp_ti_days_to_cover[0];
                        
                        if ($ratio > 0.65) {
                            $label = $labelCenter;
                        }                  
                        
                    }

                    echo '{
                        value: ' .  number_format($day_to_cover,2,".","") . ', 
                        label:' . $label . ', tooltip: { backgroundColor: "black", formatter: "{b}: <br> Days to cover ratio: '
                        . number_format($day_to_cover,1,".",","). '<br>';
                        echo 'Shorted stocks: ' . number_format($topp_ti_shortede_aksjer[$counter],0,".",",")
                        .'<br>A high ratio indicates risk <br> for a short squeeze."';
                        
                        echo '},';
                    echo '},';
                    $counter++;
                    //$sisteVerdiTeller--;
                }        
                ?>
                ]
            }
            ]
        };

        // use configuration item and data specified to show chart
        myChartd<?php echo $land; ?>.setOption(option);
        window.addEventListener('resize', function(event){
          myChartd<?php echo $land; ?>.resize();
        });

    </script>


</div>
 </div>

<?php 
$filenameStart = '../short_data/html/days_to_cover_';
$filenameCountry = $land2;

$filenameEnd = '.html';
$filename = $filenameStart . $filenameCountry . $filenameEnd;

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents($filename , $htmlStr);

?>