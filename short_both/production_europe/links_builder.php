<?php 

require_once '../production_europe/functions.php';
require '../production_europe/namelink.php';

$countries = get_countries();

$allPlayersBox = array();
$selskapsNavnBoxAll = [];

foreach ($countries as $land) {

	//les in posisjonene
	if ($land == 'norway')
	{
		$playerpositions = '../short_data/dataraw/' . $land . '/' . $land . '_current.json';
	}
	else
	{
		$playerpositions = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.json';
	}
	
	$playerPositionsJson = file_get_contents($playerpositions);
	$playerPositionsJson = json_decode($playerPositionsJson, true);

	//samle inn selskapsnavnene og playernavnene samtidig
	$selskapsNavnBox = array();

	foreach ($playerPositionsJson as $key => $selskap) {
		$singleCompany   = linktoname($selskap['Name']);
	  	$singleCompany = strtoupper($singleCompany);
		$selskapsNavnBox[] = $singleCompany;

		foreach ($selskap['Positions'] as $posisjon) {
			$singlePlayer   = linktoname($posisjon['PositionHolder']);
	  		$singlePlayer  = strtoupper($singlePlayer);
			$allPlayersBox[] = $singlePlayer ;
		}
	}


	$selskapsNavnBox = array_unique($selskapsNavnBox);
	//$playerNavnBox = array_unique($playerNavnBox);
	sort($selskapsNavnBox);
	//sort($playerNavnBox);

	$selskapsNavnBoxAll[$land] = $selskapsNavnBox;
}


$allPlayersBox  = array_unique($allPlayersBox);

  /* create a dom document with encoding utf8 */
    $domtree = new DOMDocument('1.0', 'UTF-8');

    /* create the root element of the xml tree */
    $xmlRoot = $domtree->createElement("pages");
    /* append it to the document created */
    $xmlRoot = $domtree->appendChild($xmlRoot);
    
    //alle land's playere her!

    $UrlFirst = 'details.php?player=';
	$UrlLast = '&land=norway';

    foreach ($allPlayersBox as $key =>$player) {
	    $currentTrack = $domtree->createElement("link");
	    $currentTrack = $xmlRoot->appendChild($currentTrack);
	    $url = $UrlFirst . strtoupper($player) . $UrlLast;
	    $url = htmlspecialchars($url);
	    
	    $player = strtolower($player);
	    $player = ucwords($player);
	    $player   = linktoname($player);
	    $player = htmlspecialchars($player);

	    $currentTrack->appendChild($domtree->createElement('title',$player));
	    $currentTrack->appendChild($domtree->createElement('description','player'));
	    $currentTrack->appendChild($domtree->createElement('url', $url));
    }


   foreach ($selskapsNavnBoxAll as $index => $entry)
   {

    $selskapsUrlFirst = 'details_company.php?company=';
	$selskapsUrlLast = '&land=' . $index;

	    foreach ($entry as $key => $selskap) {
		    $currentTrack = $domtree->createElement("link");
		    $currentTrack = $xmlRoot->appendChild($currentTrack);
		    $url = $selskapsUrlFirst . strtoupper($selskap) . $selskapsUrlLast;
		    $url = htmlspecialchars($url);
		    $selskap = strtolower($selskap);
		    $selskap = ucwords($selskap);
		    $selskap   = linktoname($selskap);
		    $selskap = htmlspecialchars($selskap);
		    
		    $currentTrack->appendChild($domtree->createElement('title',$selskap));
		    $currentTrack->appendChild($domtree->createElement('description','Company'));
		    $currentTrack->appendChild($domtree->createElement('country', ucwords($index)));
		    $currentTrack->appendChild($domtree->createElement('url', $url));
	    }
	}

 
    /* get the xml printed */
    //echo $domtree->saveXML();
    $domtree->save('../short_data/livesearch.xml');
    //var_dump($domtree);

?>