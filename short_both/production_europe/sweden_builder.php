<?php //Filene som skal kjøres
//include '../production_europe/functions.php';
//set error handler to pick up any error

	$land = 'sweden';
	$production = 1;
	include '../production_europe/functions.php';

	//checking if it should run. 

	if (!checktime('15.30', '23.00') and $production == 0)
	{
		errorecho('Outside time interval 15.30 to 23.00 for running sweden_builder.php, returning<br>');
		//return;
	}
	else
	{
		echo 'Time interval correct, proceding to dowload positions...<br>';
	}

	echo '<br><strong>sweden_active_downloader.php starter... </strong><br>';
	include '../production_europe/sweden_active_downloader.php'; 
	echo '<br>sweden_active_downloader.php done <br>';

	$mode = 'active';
	echo 'Starter sweden_xlsx_to_csv.php <br>';
	include '../production_europe/sweden_xlsx_to_csv.php';
	echo 'sweden_xlsx_to_csv.php done!<br>';


	$inputFileName = '../short_data/dataraw/sweden/sweden_current.csv';
	echo '<br><strong>csv_duplicates_finder.php starter... </strong><br>';
	include '../production_europe/csv_duplicates_finder.php'; //
	echo '<br>csv_duplicates_finder.php done <br>';

	echo '<br><strong>csv_to_json_builder.php starter... </strong><br>';
	include '../production_europe/csv_to_json_builder.php'; //gjør om csv-filen til en json-fil med alle aktive posisjoner
	echo '<br>csv_to_json_builder.php done <br>';
	
	$mode = 'sweden';
	echo '<br><strong>filter_out_old_positions.php starter... </strong><br>';
	include '../production_europe/filter_out_old_positions.php'; //gjør om csv-filen til en json-fil med alle aktive posisjoner
	echo '<br>filter_out_old_positions.php done<br>';	

	//gjør om navnene fra svenske myndigheter til det som står i hovedlisten for selskaper. 
	echo '<br>../production/json_set_names_to_isinlist.php starts <br>';
	$mode = 'sweden';
    $inputFile = '../short_data/dataraw/sweden/sweden_current.json';
    include '../production_europe/json_set_names_to_isinlist.php';
    echo '<br>../production/json_set_names_to_isinlist.php done <br>';

	echo '<br>Sweden done!<br>';


?>