<?php 

include '../production_europe/functions.php';

set_time_limit(6000);

echo '<br>';
$path = '../short_data/json/outstandingstocks_yahoo/';

if (!$isinlist = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_sublist.csv'));

$errorcounter = 0;

$keepBox = [];

foreach ($isinlist as $key => $row)
{
    
    flush_start();
 

    if (!isset($row[1]) or !isset($row[8]))
    {
        continue;
    }

    $targetticker = $row[1] . '.' . $row[8];
    $targetticker = str_replace(' ', '-', $targetticker);

    $targetfilename = $path . strtolower($targetticker) . '.json';

     echo $key . '. ' .  $targetfilename . ' ';

    if (!$data = readJSON($targetfilename))
    {
        $errorcounter++;
        continue;
    }


    if (!isset($data['quoteResponse']))
    {
       
        echo 'SharesStats not set, continuing<br>';
        $errorcounter++;
        continue; 
    }

    else if ($data['quoteResponse']['result'][0]['sharesOutstanding'] == null)
    {
    
        echo $targetfilename . '. ';
        echo 'Shares are null, continuing<br>';
        $errorcounter++;
        continue; 
    }
    
    //var_dump($data['quoteResponse']['result'][0]);

    $isinlist[$key][9] = (string)$data['quoteResponse']['result'][0]['sharesOutstanding'];

    $isinlist[$key][10] = date('Y-m-d', filemtime($targetfilename));

    $keepBox[] = $isinlist[$key];
    successecho('ok');
    echo '<br>';

    flush_end();

}

echo '<br>Number of errors: ' . $errorcounter . '<br>';

$savefilename = '../short_data/datacalc/outstanding_shares_current_isinlist_yahoo.csv';

echo 'Saving ' . $savefilename . '<br>';
saveCSVsemikolon($keepBox, $savefilename);

?>

