           <?php 
           //denne filen behandler filer som er nedlastet
           //read Oslo børs header file    

           //Last inn nyeste data 
           $string = file_get_contents('../short_data/dataraw/tickere/oslobors_current.json');
           $oslobors_current = json_decode($string, true);    
                //var_dump($oslobors_current);

                //echo $oslobors_current['Global Quote']['01. symbol'];

           if (isset($oslobors_current['Global Quote']['10. change percent'])) {

               $hovedindeksen_endring = $oslobors_current['Global Quote']['10. change percent'];
               $hovedindeksen = $oslobors_current['Global Quote']['05. price'];

               $hovedindeksen = number_format((float)$hovedindeksen,2,".","");
               $hovedindeksen_endring = number_format((float)$hovedindeksen_endring,2,".","");

                    //lag logikk for å sette farge på utviklingen
                $hovedindeksen_switch = '';
                if ($oslobors_current['Global Quote']['09. change'] >=0)
                {
                    if ($oslobors_current['Global Quote']['09. change'] ==0)
                    {
                        $hovedindeksen_switch = 'exchange-header-neutral';
                    }
                    else 
                    {
                        $hovedindeksen_switch = 'exchange-header-up';
                    }

                }
                else 
                {
                    $hovedindeksen_switch = 'exchange-header-down';
                }
        }
        else {
             $hovedindeksen_switch = 'exchange-header-neutral';
             $hovedindeksen_endring = '-';
             $hovedindeksen = '-';
        }


        //stockholm
           $string = file_get_contents('../short_data/dataraw/tickere/stockholm_current.json');
           $stockholm_current = json_decode($string, true);    
                //var_dump($oslobors_current);

                //echo $oslobors_current['Global Quote']['01. symbol'];

           if (isset($stockholm_current['Global Quote']['10. change percent'])) {

               $stockholm_endring = $stockholm_current['Global Quote']['10. change percent'];
               $stockholm = $stockholm_current['Global Quote']['05. price'];

               $stockholm = number_format((float)$stockholm,2,".","");
               $stockholm_endring = number_format((float)$stockholm_endring,2,".","");

                    //lag logikk for å sette farge på utviklingen
                $stockholm_switch = '';
                if ($stockholm_current['Global Quote']['09. change'] >=0)
                {
                    if ($stockholm_current['Global Quote']['09. change'] ==0)
                    {
                        $stockholm_switch = 'exchange-header-neutral';
                    }
                    else 
                    {
                        $stockholm_switch = 'exchange-header-up';
                    }

                }
                else 
                {
                    $stockholm_switch = 'exchange-header-down';
                }
        }
        else {
             $stockholm_switch = 'exchange-header-neutral';
             $stockholm_endring = '-';
             $stockholm = '-';
        }



     //copenhagen
           $string = file_get_contents('../short_data/dataraw/tickere/copenhagen_current.json');
           $copenhagen_current = json_decode($string, true);    
                //var_dump($oslobors_current);

                //echo $oslobors_current['Global Quote']['01. symbol'];

           if (isset($copenhagen_current['Global Quote']['10. change percent'])) {

               $copenhagen_endring = $copenhagen_current['Global Quote']['10. change percent'];
               $copenhagen = $copenhagen_current['Global Quote']['05. price'];

               $copenhagen= number_format((float)$copenhagen,2,".","");
               $copenhagen_endring = number_format((float)$copenhagen_endring,2,".","");

                    //lag logikk for å sette farge på utviklingen
                $copenhagen_switch = '';
                if ($copenhagen_current['Global Quote']['09. change'] >=0)
                {
                    if ($copenhagen_current['Global Quote']['09. change'] ==0)
                    {
                        $copenhagen_switch = 'exchange-header-neutral';
                    }
                    else 
                    {
                        $copenhagen_switch = 'exchange-header-up';
                    }

                }
                else 
                {
                    $copenhagen_switch = 'exchange-header-down';
                }
        }
        else {
             $copenhagen_switch = 'exchange-header-neutral';
             $copenhagen_endring = '-';
             $copenhagen = '-';
        }
  

     //copenhagen
           $string = file_get_contents('../short_data/dataraw/tickere/helsinki_current.json');
           $helsinki_current = json_decode($string, true);    
                //var_dump($oslobors_current);

                //echo $oslobors_current['Global Quote']['01. symbol'];

           if (isset($helsinki_current['Global Quote']['10. change percent'])) {

               $helsinki_endring = $helsinki_current['Global Quote']['10. change percent'];
               $helsinki = $helsinki_current['Global Quote']['05. price'];

               $helsinki = number_format((float)$helsinki,2,".","");
               $helsinki_endring = number_format((float)$helsinki_endring,2,".","");

                    //lag logikk for å sette farge på utviklingen
                $helsinki_switch = '';
                if ($helsinki_current['Global Quote']['09. change'] >=0)
                {
                    if ($helsinki_current['Global Quote']['09. change'] ==0)
                    {
                        $helsinki_switch = 'exchange-header-neutral';
                    }
                    else 
                    {
                        $helsinki_switch = 'exchange-header-up';
                    }

                }
                else 
                {
                    $helsinki_switch = 'exchange-header-down';
                }
        }
        else {
             $helsinki_switch = 'exchange-header-neutral';
             $helsinki_endring = '-';
             $helsinki = '-';
        }
  
        //Get dollar value and change

//timestamp
date_default_timezone_set('Europe/Oslo');
$now = date('l j. F H:i',strtotime("now"));
$now = strtolower($now);
ob_start();
?>
<div class="row banner-padding justify-content-center ">
    <div class="col-lg-8 border ">
    <div class="mt-1 "><h5 class=" text-center">Markets <?php echo $now; ?></h5></div>
  </div>
  <div class="row  justify-content-center">
    <div class="text-center">
        <div class="py-0 pt-1 " data-toggle="tooltip" data-html="true" title="OSEBX. <br>Data is 15 minutes delayed">
            <span class="exchange-header <?php echo $hovedindeksen_switch ?>">
                <?php echo $hovedindeksen_endring  ?>%</span>
        </div>

            <div class="py-0 pb-1" >
                <span class="d-none d-sm-inline"> <strong>Oslo</strong> </span>
                <span class="d-inline d-sm-none exchange-index"> <strong>Oslo</strong> </span>
                <span class="exchange-index"><?php echo $hovedindeksen; ?></span>
            </div>
        </div>
        <div class="pl-3 text-center">
            <div class="py-0 pt-1" data-toggle="tooltip" data-html="true" title="OMXSPI, Nasdaq Stockholm. 
                <br>Data is 15 minutes delayed">
                <span class="exchange-header <?php echo $stockholm_switch ?>">
                    <?php echo $stockholm_endring  ?>%</span>
                </div>
                <div class="py-0 pb-1" >
                    <span class="d-none d-sm-inline"> <strong>Stockholm</strong> </span>
                    <span class="d-inline d-sm-none exchange-index"> <strong>Stkhlm</strong> </span>                            
                    <span class="exchange-index"><?php echo $stockholm; ?></span>
                </div>
            </div>

            <div class="pl-3 text-center" data-toggle="tooltip" data-html="true" title="OMXC20, Nasdaq Copenhagen.
                <br>Data is 15 minutes delayed">
                <div class="py-0 pt-1"><span class="exchange-header <?php echo $copenhagen_switch ?>"><?php echo $copenhagen_endring ?>%</span>
                </div>
                <div class="py-0 pb-1" >
                    <span class="d-none d-sm-inline"> <strong>Copenhagen</strong> </span>
                    <span class="d-inline d-sm-none exchange-index"> <strong>Cop</strong> </span> 
                    <span class="exchange-index"><?php echo $copenhagen; ?></span>
                </div>
            </div>

            <div class="pl-3 text-center" data-toggle="tooltip" data-html="true" title="OMXHPI, Nasdaq Helsinki.
                <br>Data is 15 minutes delayed">
                <div class="py-0 pt-1"><span class="exchange-header <?php echo $helsinki_switch ?>"><?php echo $helsinki_endring ?>%</span>
                </div>
                <div class="py-0 pb-1">
                    <span class="d-none d-sm-inline "> <strong>Helsinki</strong> </span>
                    <span class="d-inline d-sm-none exchange-index"> <strong>Hels</strong> </span> 
                    <span class="exchange-index"><?php echo $helsinki; ?></span>
                </div>
            </div>


        </div>
    </div>

                 
<?php 
//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
ob_end_clean(); 
// Write final string to file
file_put_contents('banner_nordic.html', $htmlStr);

?>
    
                    


