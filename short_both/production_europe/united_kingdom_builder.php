<?php //Filene som skal kjøres

	$land = 'united_kingdom';
	$production = 1;
	echo '<br><strong>Download of ' . 'united_kingdom' . ' starter... </strong><br>';
	include '../production_europe/logger.php';
	include '../production_europe/united_kingdom_download_active_and_historical_positions.php';
	
	$mode = 'active';
	include '../production_europe/united_kingdom_xlsx_to_csv.php';
	echo '<br><strong>Download of ' . 'united_kingdom' . ' done... </strong><br>';

	$inputFileName = '../short_data/dataraw/' . $land . '/' . $land . '_current.csv';
	//echo '<br><strong>csv_duplicates_finder.php starter... </strong><br>';
	//include '../production_europe/csv_duplicates_finder.php'; //
	//echo '<br>csv_duplicates_finder.php done <br>';


	echo '<br><strong>csv_to_json_builder.php starter... </strong><br>';
	include '../production_europe/csv_to_json_builder.php'; //gjør om csv-filen til en json-fil med alle aktive posisjoner
	echo '<br>csv_to_json_builder.php done <br>';
	
	echo '<br><strong>filter_out_old_positions.php starter... </strong><br>';
	include '../production_europe/filter_out_old_positions.php'; //gjør om csv-filen til en json-fil med alle aktive posisjoner
	echo '<br>filter_out_old_positions.php done<br>';	

	//gjør om navnene fra myndigheter til det som står i hovedlisten for selskaper. 
	echo '<br>../production/json_set_names_to_isinlist.php starts <br>';
	$mode = 'united_kingdom';
    $inputFile = '../short_data/dataraw/' . $land . '/' . $land . '_current.json';
    include '../production_europe/json_set_names_to_isinlist.php';
    echo '<br>../production/json_set_names_to_isinlist.php done <br>';

	echo '<br>United kingdom done!<br>';


?>