
<?php 

date_default_timezone_set('Europe/Oslo');

//$land = 'sweden';

require '../production_europe/run.php';
include_once ('../production_europe/logger.php');
include_once '../production_europe/functions.php';

switch ($land) {
    case "norway":
        $stockExchangeNameFormal = 'Norwegian stock exchange';
        $nationalName = 'Norwegian';
        $stockExchange = '../short_data/json/ticker/osebx.ol.json';
        $stockExchangeName = 'Oslo Stock Exchange';
        break;  
    case "sweden":
        $stockExchange = '../short_data/json/ticker/omxspi.indx.json';
        $stockExchangeName = 'Nasdaq Stockholm';
        $stockExchangeNameFormal = 'Nasdaq Stockholm';
        $nationalName = 'Swedish';
        break;
    case "denmark":
        $stockExchange = '../short_data/json/ticker/omxc20.indx.json';
        $stockExchangeName = 'Nasdaq Copenhagen';
        $stockExchangeNameFormal = 'Nasdaq Copenhagen';
        $nationalName = 'Danish';
        break;
    case "finland":
        $stockExchange = '../short_data/json/ticker/omxh25.indx.json';
        $stockExchangeName = 'Nasdaq Helsinki';
        $stockExchangeNameFormal = 'Nasdaq Helsinki';
        $nationalName = 'Finnish';
        break;
    case "germany":
        $stockExchange = '../short_data/json/ticker/gdaxi.indx.json';
        $stockExchangeName = 'DAX';
        $stockExchangeNameFormal = 'DAX';
        $nationalName = 'German';
        break;
    case "france":
        $stockExchange =  '../short_data/json/ticker/fchi.indx.json';
        $stockExchangeName = 'CAC 40';
        $stockExchangeNameFormal = 'CAC 40 index';
        $nationalName = 'French';
        break;      
    case "spain":
        $stockExchange = '../short_data/json/ticker/ibex.indx.json';
        $stockExchangeName = 'IBEX 35';
        $stockExchangeNameFormal = 'IBEX 35 index';
        $nationalName = 'Spanish';
        break;
      case "united_kingdom":
        $stockExchange = '../short_data/json/ticker/ftse.indx.json';
        $stockExchangeName = 'FTSE 100';
        $stockExchangeNameFormal = 'FTSE 100 index';
        $nationalName = 'British';
        break; 
     case "italy":
        $stockExchange = '../short_data/json/ticker/spmib.indx.json';
        $stockExchangeName = 'FTSE MIB';
        $stockExchangeNameFormal = 'FTSE MIB index';
        $nationalName = 'Italian';
        break;                              
}

$borsUpFolder = 'img/'  . $land  .'/opp/';
$borsDownFolder = 'img/'  . $land  .'/ned/';
$borsUpEqual = 'img/'  . $land  .'/flat/';

$borsUpFolderScan = '../shorteurope-com.luksus.no/img/'  . $land  .'/opp/';
$borsDownFolderScan = '../shorteurope-com.luksus.no/img/'  . $land  .'/ned/';
$borsUpEqualScan = '../shorteurope-com.luksus.no/img/'  . $land  .'/flat/';


$destination = '../short_data/html/stories/story_'  . $land  .'_current.html';

include '../production_europe/options_set_currency_by_land.php';

$selskapsliste = $land . '/' . $land . '_current.json';
$land_navn = ucwords($land);
$playerliste = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
$saveUrlWinner = '../short_data/datacalc/toplist/' . $land . '/winner/';
$saveUrlLoser = '../short_data/datacalc/toplist/' . $land . '/loser/';
$loserFileName = '../short_data/loser.' . $land . '.current.json';
$winnerFileName = '../short_data/winner.' . $land . '.current.json';
$nokkeltall = '../short_data/datacalc/nokkeltall/nokkeltall_' . $land . '.current.csv';


$headerHolder = '';
$storyTextHolder = '';

//found out if the stock change was up or down
//Last inn nyeste shorttabell 

if (!$stockExchangeJson = readJSON($stockExchange))
{
  errorecho('Error reading file, returning!');
  return;
}

var_dump($stockExchangeJson);
  
$stockExchangeChange = $stockExchangeJson['change_p'];
$stockExchangeChange = str_replace("%", "", $stockExchangeChange);
$today = strtolower(date('l',strtotime('now')));
$date = strtolower(date('j. M',strtotime('now')));
$time = strtolower(date('H:i',strtotime('now')));

//remove if error
if (abs($stockExchangeChange) > 20) {
  logger('$stockexchangeChange er større enn 10! (' . $stockExchangeChange . ') ', 'I story live rapport, will skip saving!');
  $skipsave = 1;
}
else {
	$skipsave = 0;
}


//last inn shortlisten med aksjekurser
$filename = $nokkeltall;
$csvFile = file($filename);            
$nokkeltall = [];
foreach ($csvFile as $line) {
    $nokkeltall[] = str_getcsv($line);
} 

$stockExchangeStatus;

$devMode = 0;
if ($devMode == 1) {
  $stockExchangeChangeRandom = rand(-5,5); 
  $shorterChangePercentRandom = rand(-5,5);

  //$stockExchangeChangeRandom = 0; 
  //$shorterChangePercentRandom = -5;

  $stockExchangeChange = $stockExchangeChangeRandom;
  $shorterChangePercent = $shorterChangePercentRandom;

  echo '<br>Stocks: ' . $stockExchangeChangeRandom . '<br>'; 
  echo 'Shorter: ' . $shorterChangePercentRandom . '<br>'; ;
}


//Choose pictures with shorters as the subject!
$shorterStatus = '';
$targetEarnings = $nokkeltall[1][0];

if ($targetEarnings > 0) {
    $shorterStatus = 'opp';
    
    $bildeUrl = $borsUpFolderScan;
    $bildeUrllink = $borsUpFolder;
}
elseif ($targetEarnings == 0) {
    $shorterStatus = 'equal';
    
    $bildeUrl = $borsUpEqualScan;
    $bildeUrllink = $borsUpEqual;
}
else {
    
    $bildeUrl = $borsDownFolderScan;
    $shorterStatus = 'ned';
    $bildeUrllink = $borsDownFolder;
}

$bildeDir = array_diff(scandir($bildeUrl), array('.', '..'));

//ta bort txt-filen.

foreach ($bildeDir as $key => $fil) {
    if (strpos($fil, '.txt') == true) {
    unset($bildeDir[$key]);
    }
}

$numberOfPictures = count($bildeDir);
//echo '  ' . $numberOfPictures;

$bildeDir = array_values($bildeDir);
//var_dump($bildeDir);

$bildeCount = count($bildeDir);
$randomImage = rand(0, $bildeCount-1);

//do image credit
//echo '<br>';
$textFilename = $bildeDir[$randomImage];
//echo $textFilename;
//echo '<br>';
$textFilename = str_replace(".jpg", ".txt", $textFilename);
//echo $textFilename;

//echo utf8_encode(readfile($bildeUrl . $textFilename));
$string = file_get_contents($bildeUrl . $textFilename);
$string  = utf8_encode($string);

if ($devMode != 1) {
  ob_start();
}

?>
<div class="ribbon"><span>INTRADAY</span></div>
<img class="card-img-top mb-1" data-toggle="tooltip" title="<?php echo $string; ?>" src="<?php echo $bildeUrllink . $bildeDir[$randomImage];?>" >

<?php

//set stock exchange up or down
if ($stockExchangeChange > 0) {
    $stockExchangeStatus = 'opp';
}
elseif ($stockExchangeChange == 0) {
    $stockExchangeStatus = 'equal';
}
else {
    $stockExchangeStatus = 'ned';
}

$stockExchangeChange = number_format($stockExchangeChange,2);

//echo '<br>Shorter status is:' . $shorterStatus;
//who did best, shorters or stock exchange, depending on the movement on the market?

if ($devMode != 1) {
  $shorterChangePercent = $nokkeltall[1][2];
}

$shortersDidIt;
if ($stockExchangeChange > 0) {
    //børsen opp
    if ($stockExchangeChange > -$shorterChangePercent) {
        $shortersDidIt = 'better';
        //echo 'better 1 ';
    }
    else if ($stockExchangeChange == $shorterChangePercent ) {
        $shortersDidIt = 'equal';
        //echo ' Equal 1';
    }
    else {
        $shortersDidIt = 'worse';
        //echo 'worse 2 ';
    }
}
else if ($stockExchangeChange == 0) {
    //børsen flat
    if ($stockExchangeChange > -$shorterChangePercent ) {
        $shortersDidIt = 'worse';
        //echo 'wors 3 ';
    }
    else if ($stockExchangeChange == $shorterChangePercent ) {
        $shortersDidIt = 'equal';
         //echo ' Equal 2 ';
    }
    else {
        $shortersDidIt = 'better';
        //echo 'better 4 ';
    }
}
else {
    //børsen ned
    if (-$stockExchangeChange > $shorterChangePercent ) {
        $shortersDidIt = 'worse';
        //echo 'worse 5 ';

    }
    else if ($stockExchangeChange == $shorterChangePercent ) {
        $shortersDidIt = 'equal';
         //echo ' Equal 3 ';

    }
    else {
        $shortersDidIt = 'better';
        //echo 'better 6 ';
    }
}

//start story
//choose picture

$downwords = array('is down', 'has fallen', 'has lost');
$upwords = array('is up', 'adds', 'rises', 'gains', 'climbs', 'increases','advances');
$shortersWorse = array('Shorters are not able to keep up, losing', 'The shorters could not matched that, losing', 'Shorters are doing worse than the market, decreasing');
$shortersBetter = array('Shorters are doing better, increasing', 'The shorters are better, adding', 'Shorters increases more, adding');
$downWordsLose = array('are losing', 'have slipped', 'have dropped', 'have lost');

shuffle($upwords);
shuffle($downwords);
shuffle($downWordsLose);
shuffle($shortersWorse);
shuffle($shortersBetter);

$randomNumber = rand(0, 10);
$randomNumberHeading = rand(0, 10);
$percent = $nokkeltall[1][2];
$money = $nokkeltall[1][0];

//header 
$mainSwitch = rand(1,2);

if ($devMode == 1) {
$percent = $shorterChangePercentRandom;
}

echo '<h4>';
//main switch 

//var_dump($mainSwitch);
if ($mainSwitch == 1) {

//Stock exchange up
//Open line about:

    if ($stockExchangeStatus == 'ned') { 

           if ($randomNumberHeading == 0 ) {
            $headerHolder =  'Tumbeling stocks in '.  $land_navn; echo $headerHolder;
           }
           else if ($randomNumberHeading == 1) {
            $headerHolder = 'The ' . $stockExchangeNameFormal . ' is in minus' ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 2) {
              $headerHolder = 'Red day in ' . $land_navn . '' ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 3) {
            $headerHolder = 'The ' . $nationalName. ' market falls' ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 4) {
            $headerHolder = 'Minus for ' . $stockExchangeNameFormal . '' ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 5) {
            $headerHolder = 'Downturn in ' . $land_navn ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 6) {
            $headerHolder = 'The ' . $stockExchangeNameFormal . ' is down' ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 7) {
            $headerHolder = $stockExchangeNameFormal . ' drops' ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 8) {
            $headerHolder = 'The ' . $stockExchangeNameFormal . ' is in red' ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 9) {
            $headerHolder = 'Falling stocks in ' . $land_navn ; echo $headerHolder;
           }
           else {
            $headerHolder = 'Falling stock prices in ' . $land_navn ; echo $headerHolder;
           }  
    } 

//Stock exchange down

    if ($stockExchangeStatus == 'opp') { 

           if ($randomNumberHeading == 0 ) {
           
             $headerHolder = 'Good day for stocks in '.  $land_navn ; echo $headerHolder;  
           }
           else if ($randomNumberHeading == 1) {
            
            $headerHolder = 'The ' . $stockExchangeNameFormal . ' is up ' . $today ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 2) {
            
            $headerHolder = $stockExchangeNameFormal . ' increases ' . $today ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 3) {
      
            $headerHolder = $stockExchangeNameFormal . ' climbs ' . $today ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 4) {
           
            $headerHolder = 'Upturn in the ' . $nationalName . ' market ' . $today ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 5) {
           
            $headerHolder = 'The ' . $stockExchangeNameFormal . ' is up ' . $today ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 6) {
         
            $headerHolder = 'The ' . $stockExchangeNameFormal . ' rises ' . $today ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 7) {
          
            $headerHolder = 'Positive day so far for ' . $nationalName . ' stocks' ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 8) {
    
            $headerHolder = $nationalName . ' stocks are up ' . $today ; echo $headerHolder;
           }
           else if ($randomNumberHeading == 9) {
    
            $headerHolder = 'Upturn for the ' . $nationalName . ' market' ; echo $headerHolder;
           }
          else {
        
            $headerHolder = 'Stocks are up in ' . $land_navn ; echo $headerHolder;
           }  
    } 
    
    if ($stockExchangeStatus == 'equal') { 
        $headerHolder = 'Flat day at the ' . $stockExchangeNameFormal ; echo $headerHolder;
    } 

}
else {
//Shorters won
    if ($shorterStatus == 'opp') { 

           if ($randomNumberHeading == 0 ) {
             echo 'Good day for shorters in '.  $land_navn;  
           }
           else if ($randomNumberHeading == 1) {
            echo 'Shorters are gainng in ' . $land_navn. '';
           }
           else if ($randomNumberHeading == 2) {
            echo 'Shorters are up in ' . $land_navn. '';
           }
           else if ($randomNumberHeading == 3) {
            echo 'Shorters increases in ' . $land_navn. '';
           }
           else if ($randomNumberHeading == 4) {
            echo 'Shorters climbs in ' . $land_navn. '';
           }
           else if ($randomNumberHeading == 5) {
            echo 'Shorters are up in ' . $land_navn. '';
           }
           else if ($randomNumberHeading == 6) {
            echo $nationalName . ' shorters earns';
           }
           else if ($randomNumberHeading == 7) {
            echo 'Positive for ' . $nationalName . ' shorters';
           }
           else if ($randomNumberHeading == 8) {
            echo 'Shorters gain in ' . $land_navn . '';
           }
           else if ($randomNumberHeading == 9) {
            echo $nationalName . ' shorters in the black';
           }
           else {
            echo $nationalName . ' shorters earns ' . $today;
           }  
    }

    //Shorters lost
    if ($shorterStatus == 'ned') { 

           if ($randomNumberHeading == 0 ) {
             echo 'Bad day so far for shorters in '.  $land_navn;  
           }
           else if ($randomNumberHeading == 1) {
            echo 'Minus for shorters on the ' . $stockExchangeNameFormal;
           }
           else if ($randomNumberHeading == 2) {
            echo 'Minus for shorters in ' . $land_navn;
           }
           else if ($randomNumberHeading == 3) {
            echo $land_navn . ': Shorters are losing';
           }
           else if ($randomNumberHeading == 4) {
            echo 'Red for shorters on the ' . $stockExchangeNameFormal;
           }
           else if ($randomNumberHeading == 5) {
            echo $land_navn . ': Red day for shorters';
           }
           else if ($randomNumberHeading == 6) {
            echo 'Negative for shorters in ' . $land_navn;
           }
           else if ($randomNumberHeading == 7) {
            echo 'Dropping values for shorters in ' . $land_navn;
           }
           else if ($randomNumberHeading == 8) {
            echo 'Loss for shorters in ' . $land_navn;
           }
           else if ($randomNumberHeading == 9) {
            echo $nationalName . ' shorters in minus so far';
           }
           else {
            echo ucfirst($today) . ': ' . $nationalName . ' shorters lose money';
           }  
    }
    if ($shorterStatus == 'equal') { 
        echo 'Flat day for the shorters';
    } 
}
echo '</h4>';
echo '<span class="font-weight-bold">' . $date . ' ' . $time . ': </span>';

//Open line about :
if ($stockExchangeStatus == 'ned') { 

    if ($shorterStatus == 'opp') {
        echo $stockExchangeName . ' ' . $downwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';

        if ($shortersDidIt == 'better') {
            echo $shortersBetter[0] . ' ' . $percent . ' percent. ';
        }
        else if ($shortersDidIt == 'equal') {
            echo 'Shorters have matched that with an increase of ' . ' ' . abs($percent) . ' percent. ';
        }
        else {
            echo 'Shorters have not kept up, gaining just ' . ' ' . abs($percent) . ' percent. ';
        }
        echo 'Total earnings are ' . number_format($money, 0) . ' million ' . $currency_ticker . '. ';
    }
    if ($shorterStatus == 'ned') {
        echo $stockExchangeName . ' ' . $downwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';
        echo 'The fall have not help the shorters, which ' . $downWordsLose[0] . ' ' . abs($percent) . ' percent on their positions. ';
       
       if ($randomNumber <=3 ) {
         echo 'Total losses are ' . abs(number_format($money, 0)) . ' million ' . $currency_ticker . '. ';
       }
       else if ($randomNumber <= 6) {
        echo 'In all, the ' . $nokkeltall[1][5] . ' players are out of ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . '. ';
       }
       else {
        echo 'Total losses are ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . ' among the ' . 
        $nokkeltall[1][5] . ' players.';
       }  
    }
    if ($shorterStatus == 'equal') {
        echo $stockExchangeName . ' ' . $downwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';
        echo 'The shorters result is a disappointing ' . ' ' . $percent . ' percent. ';  
    }
}

//echo '<br><br>---------------<br>';

if ($stockExchangeStatus == 'opp') { 
   
   if ($shorterStatus == 'opp') {
       
       echo $stockExchangeName . ' ' . $upwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';

       if ($randomNumber <= 3 ) {
         echo 'Against the sentiment, the shorters have earned ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . ' today, ';
         echo ' an increase of ' . abs($percent) . ' percent. ';
       }
       else if ($randomNumber <= 6) {
        echo 'In all, the ' . $nokkeltall[1][5] . ' short players have earned ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . ', which is ' . abs($percent) . ' percent. ' ;
       }
       else {
        echo 'The ' . $nokkeltall[1][5] . ' shorters have done good so far today, beating the market and earning '. $percent . ' percent, adding ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . ' to their holdings.' ;
       }
    }

    if ($shorterStatus == 'ned') {
     
        echo $stockExchangeName . ' ' . $upwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';

        if ($shortersDidIt == 'better') {
            echo 'The shorters losses so far are lesser than the market increase at just ' . abs($percent) . ' percent. ';
        }
        else {

           if ($randomNumber <= 3 ) {
            echo 'Shorters are doing worse than the market direction, losing ' . ' ' . abs($percent) . ' percent. ';
            echo 'Total losses are ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . '. ';
           }
           else if ($randomNumber <= 6) {
            echo 'The losses for the shorters are more than the market movement at ' . ' ' . abs($percent) . ' percent. ';
            echo 'Moneywise, that is ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . ' in losses.';
           }
           else {
            echo 'The losses for the shorters are more than the market movement at ' . ' ' . abs($percent) . ' percent. ';
            echo 'A aggregated sum of ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . ' has been lost.';
           }          
        }  
    }

    if ($shorterStatus == 'equal') {
        echo $stockExchangeName . ' ' . $downwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';
        echo 'The shorters are doing equally good with a change of ' . ' ' . $percent . ' percent. ';  
    }
}


if ($stockExchangeStatus == 'equal') {
   
   if ($shorterStatus == 'opp') {
       
       echo $stockExchangeName . '' . ' is now up' . ' ' . 
       number_format($stockExchangeChange, 0) . ' percent '  . $today . '. ';

       if ($randomNumber <= 3 ) {
         echo 'The shorters have earned ' . number_format($money,0) . ' million ' . $currency_ticker . ', ';
         echo ' an increase of ' . abs($percent) . ' percent. ';
       }
       else if ($randomNumber <= 6) {
        echo 'In all, the ' . $nokkeltall[1][5] . ' players have earned ' . number_format($money,0) . ' million ' . $currency_ticker . ', which is ' . $percent . ' percent. ' ;
       }
       else {
        echo 'The ' . $nokkeltall[1][5] . ' shorters are doing very well, beating the market and earning '. 
            abs($percent) . ' percent, adding ' . number_format($money,0) . ' million ' . $currency_ticker . ' to their holdings.' ;
       }
    }

    if ($shorterStatus == 'ned') {
     
        echo $stockExchangeName . '' . ' is down' . ' ' . $stockExchangeChange . ' percent '  . $today . '.';

        if ($shortersDidIt == 'better') {
            echo 'The shorters losses are lesser than the market increase at just ' . abs($percent) . ' percent. ';
        }
        else {
           if ($randomNumber <= 3 ) {
            echo 'Shorters are doing worse than the market, losing ' . ' ' . abs($percent) . ' percent so far. ';
            echo 'Total losses are ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . '. ';
           }
           else if ($randomNumber <= 6) {
            echo 'The losses for the shorters are ' . ' ' . abs($percent) . ' percent. ';
            echo 'Moneywise, that is ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . '.';
           }
           else {
            echo 'The losses for the shorters are more than the market movement at ' . ' ' . abs($percent) . ' percent. ';
            echo 'A aggregated sum of ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . ' is lost.';
           }          
        }       
    }

    if ($shorterStatus == 'equal') {
        echo $stockExchangeName . ' ' . $downwords[0] . ' ' . $stockExchangeChange . ' percent '  . $today . '. ';
        echo 'The shorters result is not as good at ' . ' ' . $percent . ' percent. ';  
    }
}
if ($devMode != 1) {
  //  Return the contents of the output buffer
  $htmlStr = ob_get_contents();
  // Clean (erase) the output buffer and turn off output buffering
  ob_end_clean(); 
  // Write final string to file
  if ($skipsave  != 1) {
  file_put_contents($destination, $htmlStr);
  }
}
?>


