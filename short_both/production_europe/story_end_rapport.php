
<?php 

date_default_timezone_set('Europe/Oslo');

require '../production_europe/run.php';
run('../production_europe/logger.php');
include '../production_europe/functions.php';

switch ($land) {
    case "norway":
        $stockExchangeNameFormal = 'Norwegian stock exchange';
        $nationalName = 'Norwegian';
        $stockExchange = '../short_data/json/ticker/osebx.ol.json';
        $stockExchangeName = 'Oslo Stock Exchange';
        break;  
    case "sweden":
        $stockExchange = '../short_data/json/ticker/omxspi.indx.json';
        $stockExchangeName = 'Nasdaq Stockholm';
        $stockExchangeNameFormal = 'Nasdaq Stockholm';
        $nationalName = 'Swedish';
        break;
    case "denmark":
        $stockExchange = '../short_data/json/ticker/omxc20.indx.json';
        $stockExchangeName = 'Nasdaq Copenhagen';
        $stockExchangeNameFormal = 'Nasdaq Copenhagen';
        $nationalName = 'Danish';
        break;
    case "finland":
        $stockExchange = '../short_data/json/ticker/omxh25.indx.json';
        $stockExchangeName = 'Nasdaq Helsinki';
        $stockExchangeNameFormal = 'Nasdaq Helsinki';
        $nationalName = 'Finnish';
        break;
    case "germany":
        $stockExchange = '../short_data/json/ticker/gdaxi.indx.json';
        $stockExchangeName = 'DAX';
        $stockExchangeNameFormal = 'DAX';
        $nationalName = 'German';
        break;
    case "france":
        $stockExchange =  '../short_data/json/ticker/fchi.indx.json';
        $stockExchangeName = 'CAC 40';
        $stockExchangeNameFormal = 'CAC 40 index';
        $nationalName = 'French';
        break;      
    case "spain":
        $stockExchange = '../short_data/json/ticker/ibex.indx.json';
        $stockExchangeName = 'IBEX 35';
        $stockExchangeNameFormal = 'IBEX 35 index';
        $nationalName = 'Spanish';
        break;
      case "united_kingdom":
        $stockExchange = '../short_data/json/ticker/ftse.indx.json';
        $stockExchangeName = 'FTSE 100';
        $stockExchangeNameFormal = 'FTSE 100 index';
        $nationalName = 'British';
        break; 
     case "italy":
        $stockExchange = '../short_data/json/ticker/spmib.indx.json';
        $stockExchangeName = 'FTSE MIB';
        $stockExchangeNameFormal = 'FTSE MIB index';
        $nationalName = 'Italian';
        break;                              
}

$borsUpFolder = 'img/'  . $land  .'/opp/';
$borsDownFolder = 'img/'  . $land  .'/ned/';
$borsUpEqual = 'img/'  . $land  .'/flat/';

$borsUpFolderScan = '../short_data/img/'  . $land  .'/opp/';
$borsDownFolderScan = '../short_data/img/'  . $land  .'/ned/';
$borsUpEqualScan = '../short_data/img/'  . $land  .'/flat/';



$destination = '../short_data/html/stories/story_'  . $land  .'_current.html';

include '../production_europe/options_set_currency_by_land.php';

$selskapsliste = $land . '/' . $land . '_current.json';
$land_navn = ucwords($land);
$playerliste = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
$saveUrlWinner = '../short_data/datacalc/toplist/' . $land . '/winner/';
$saveUrlLoser = '../short_data/datacalc/toplist/' . $land . '/loser/';
$loserFileName = '../short_data/loser.' . $land . '.current.json';
$winnerFileName = '../short_data/winner.' . $land . '.current.json';
$nokkeltall = '../short_data/datacalc/nokkeltall/nokkeltall_' . $land . '.current.csv';


$headerHolder = '';
$storyTextHolder = '';

//found out if the stock change was up or down
//Last inn nyeste shorttabell 

if (!$stockExchangeJson = readJSON($stockExchange))
{
  errorecho('Error reading file, returning!');
  return;
}

var_dump($stockExchangeJson);


  
$stockExchangeChange = $stockExchangeJson['change_p'];
$stockExchangeChange = str_replace("%", "", $stockExchangeChange);
$today = strtolower(date('l',strtotime('now')));
$date = strtolower(date('j. M',strtotime('now')));
$time = strtolower(date('H:i',strtotime('now')));


//last inn shortlisten med aksjekurser
$filename = $nokkeltall;
$csvFile = file($filename);            
$nokkeltall = [];
foreach ($csvFile as $line) {
    $nokkeltall[] = str_getcsv($line);
} 



//remove if error
if (abs($stockExchangeChange) > 20) {
  logger('$stockexchangeChange er større enn 10! (' . $stockExchangeChange . ') ', 'I story end rapport, will skip saving!');
  $skipsave = 1;
}
else {
  $skipsave = 0;
}

$stockExchangeStatus;

//set devmode here!
$devMode = 0;
if ($devMode == 1) {
  $stockExchangeChangeRandom = rand(-5,5); 
  $shorterChangePercentRandom = rand(-5,5);

  //$stockExchangeChangeRandom = 0; 
  //$shorterChangePercentRandom = -5;

  $stockExchangeChange = $stockExchangeChangeRandom;
  $shorterChangePercent = $shorterChangePercentRandom;

  echo '<br>Stocks: ' . $stockExchangeChangeRandom . '<br>'; 
  echo 'Shorter: ' . $shorterChangePercentRandom . '<br>'; ;
}

$bildeUrl;

//Choose pictures with shorters as the subject!
$shorterStatus = '';
$targetEarnings = $nokkeltall[1][0];

if ($targetEarnings > 0) {
    $shorterStatus = 'opp';
    $bildeUrl = $borsUpFolderScan;
    $bildeUrllink = $borsUpFolder;
}
elseif ($targetEarnings == 0) {
    $shorterStatus = 'equal';
    $bildeUrl = $borsUpEqualScan;
    $bildeUrllink = $borsUpEqual;
}
else {
    $shorterStatus = 'ned';
    $bildeUrl = $borsDownFolderScan;    
    $bildeUrllink = $borsDownFolder;
}

$bildeDir = array_diff(scandir($bildeUrl), array('.', '..'));

//ta bort txt-filen.

foreach ($bildeDir as $key => $fil) {
    if (strpos($fil, '.txt') == true) {
    unset($bildeDir[$key]);
    }
}

$numberOfPictures = count($bildeDir);
//echo '  ' . $numberOfPictures;

$bildeDir = array_values($bildeDir);
//var_dump($bildeDir);

$bildeCount = count($bildeDir);
$randomImage = rand(0, $bildeCount-1);

//do image credit
//echo '<br>';
$textFilename = $bildeDir[$randomImage];
//echo $textFilename;
//echo '<br>';
$textFilename = str_replace(".jpg", ".txt", $textFilename);
//echo $textFilename;

//echo utf8_encode(readfile($bildeUrl . $textFilename));
$string = file_get_contents($bildeUrl . $textFilename);
$string  = utf8_encode($string);

if ($devMode != 1) {
  ob_start();
}
//<!-- <div class="ribbon"><span><?php //echo $date; spørsmålstegn></span></div>-->
?>

<img class="card-img-top mb-1" data-toggle="tooltip" title="<?php echo $string; ?>" src="<?php echo $bildeUrllink . $bildeDir[$randomImage];?>" >

<?php

$imagelink = $bildeUrl . $bildeDir[$randomImage];
$imagelink = preg_replace("/&#?[a-z0-9]+;/i","",$imagelink); 

//set stock exchange up or down
if ($stockExchangeChange > 0) {
    $stockExchangeStatus = 'opp';
}
elseif ($stockExchangeChange == 0) {
    $stockExchangeStatus = 'equal';
}
else {
    $stockExchangeStatus = 'ned';
}

$stockExchangeChange = number_format(round($stockExchangeChange,2),2);

//var_dump($stockExchangeChange);

//echo '<br>Shorter status is:' . $shorterStatus;
//who did best, shorters or stock exchange, depending on the movement on the market?

if ($devMode != 1) {
  $shorterChangePercent = $nokkeltall[1][2];
}

$shortersDidIt;
if ($stockExchangeChange > 0) {
    //børsen opp
    if ($stockExchangeChange > -$shorterChangePercent) {
        $shortersDidIt = 'better';
        //echo 'better 1 ';
    }
    else if ($stockExchangeChange == $shorterChangePercent ) {
        $shortersDidIt = 'equal';
        //echo ' Equal 1';
    }
    else {
        $shortersDidIt = 'worse';
        //echo 'worse 2 ';
    }
}
else if ($stockExchangeChange == 0) {
    //børsen flat
    if ($stockExchangeChange > -$shorterChangePercent ) {
        $shortersDidIt = 'worse';
        //echo 'wors 3 ';
    }
    else if ($stockExchangeChange == $shorterChangePercent ) {
        $shortersDidIt = 'equal';
         //echo ' Equal 2 ';
    }
    else {
        $shortersDidIt = 'better';
        //echo 'better 4 ';
    }
}
else {
    //børsen ned
    if (-$stockExchangeChange > $shorterChangePercent ) {
        $shortersDidIt = 'worse';
        //echo 'worse 5 ';

    }
    else if ($stockExchangeChange == $shorterChangePercent ) {
        $shortersDidIt = 'equal';
         //echo ' Equal 3 ';

    }
    else {
        $shortersDidIt = 'better';
        //echo 'better 6 ';

    }
}

//start story
//choose picture

$downwords = array('ended down', 'fell', 'lost');
$upwords = array('was up', 'added', 'rose', 'gained', 'climbed', 'increased','advanced');
$shortersWorse = array('Shorters were not able to keep up, losing', 'The shorters could not match that, losing', 'Shorters did worse than the market, decreasing');
$shortersBetter = array('Shorters did better, increasing', 'The shorters performed better, adding', 'Shorters increased more, adding');
$downWordsLose = array('lost', 'slipped', 'dropped', 'sank', 'fell');

shuffle($upwords);
shuffle($downwords);
shuffle($downWordsLose);
shuffle($shortersWorse);
shuffle($shortersBetter);

$randomNumber = rand(0, 10);
$randomNumberHeading = rand(0, 10);
$percent = $nokkeltall[1][2];
$money = $nokkeltall[1][0];

//header 
$mainSwitch = rand(1,2);

if ($devMode == 1) {
$percent = $shorterChangePercentRandom;
}

echo '<h4 class="mt-1">';
//main switch 

$titleString = '';
$storyText = '';

//var_dump($mainSwitch);
if ($mainSwitch == 1) {

//Stock exchange up
//Open line about:

 
    if ($stockExchangeStatus == 'ned') { 

           if ($randomNumberHeading == 0 ) {
             $titleString = 'Stocks tumble in '.  $land_navn; echo $titleString;  
           }
           else if ($randomNumberHeading == 1) {
            $titleString = $stockExchangeNameFormal . ' ended in minus today'; echo $titleString;
           }
           else if ($randomNumberHeading == 2) {
            $titleString = 'Red day for ' . $stockExchangeNameFormal . ''; echo $titleString;
           }
           else if ($randomNumberHeading == 3) {
            $titleString = 'The ' . $nationalName . ' market falls'; echo $titleString;
           }
           else if ($randomNumberHeading == 4) {
            $titleString = 'Minus for ' . $stockExchangeNameFormal . ''; echo $titleString;
           }
           else if ($randomNumberHeading == 5) {
            $titleString = 'Downturn in ' . $land_navn . ''; echo $titleString;
           }
           else if ($randomNumberHeading == 6) {
            $titleString = $stockExchangeNameFormal . ' ended down'; echo $titleString;
           }
           else if ($randomNumberHeading == 7) {
            $titleString = '' . $stockExchangeNameFormal . ' dropped'; echo $titleString;
           }
           else if ($randomNumberHeading == 8) {
            $titleString = 'The ' . $stockExchangeNameFormal . ' in red'; echo $titleString;
           }
           else if ($randomNumberHeading == 9) {
            $titleString = 'Stocks fell in ' . $land_navn . ''; echo $titleString;
           }
           else {
            $titleString = 'Falling stock prices in ' . $land_navn; echo $titleString;
           }  
    } 

//Stock exchange down

    if ($stockExchangeStatus == 'opp') { 

           if ($randomNumberHeading == 0 ) {
             $titleString = 'Good day for stocks in '.  $land_navn; echo $titleString;  
           }
           else if ($randomNumberHeading == 1) {
            $titleString = 'The ' . $stockExchangeNameFormal . ' was up ' . $today; echo $titleString;
           }
           else if ($randomNumberHeading == 2) {
            $titleString = '' . $stockExchangeNameFormal . ' increased ' . $today; echo $titleString;
           }
           else if ($randomNumberHeading == 3) {
            $titleString = '' . $stockExchangeNameFormal . ' climbed ' . $today; echo $titleString;
           }
           else if ($randomNumberHeading == 4) {
            $titleString = 'Upturn in the ' . $nationalName . ' market ' . $today; echo $titleString;
           }
           else if ($randomNumberHeading == 5) {
            $titleString = 'The ' . $stockExchangeNameFormal . ' was up ' . $today; echo $titleString;
           }
           else if ($randomNumberHeading == 6) {
            $titleString = 'The ' . $stockExchangeNameFormal . ' rose ' . $today; echo $titleString;
           }
           else if ($randomNumberHeading == 7) {
            $titleString = 'Positive day for ' . $nationalName . ' stocks'; echo $titleString;
           }
           else if ($randomNumberHeading == 8) {
            $titleString = $nationalName . ' stocks finished up ' . $today; echo $titleString;
           }
           else if ($randomNumberHeading == 9) {
            $titleString = 'Upturn for the ' . $nationalName . ' market'; echo $titleString;
           }
          else {
            $titleString = 'Stocks finished up in ' . $land_navn; echo $titleString;
           }  
    } 
    
    if ($stockExchangeStatus == 'equal') { 
        $titleString = 'Flat day at the ' . $stockExchangeNameFormal; echo $titleString;

    } 

}
else {
//Shorters won
    if ($shorterStatus == 'opp') { 

           if ($randomNumberHeading == 0 ) {
             $titleString = 'Good day for shorters in '.  $land_navn; echo $titleString;  
           }
           else if ($randomNumberHeading == 1) {
            $titleString = 'Shorters gained in ' . $land_navn. ''; echo $titleString;
           }
           else if ($randomNumberHeading == 2) {
            $titleString = 'Shorters were up in ' . $land_navn. ''; echo $titleString;
           }
           else if ($randomNumberHeading == 3) {
            $titleString = 'Shorters increased in ' . $land_navn. ''; echo $titleString;
           }
           else if ($randomNumberHeading == 4) {
            $titleString = 'Shorters climbed in ' . $land_navn. ''; echo $titleString;
           }
           else if ($randomNumberHeading == 5) {
            $titleString = 'Shorters finished up in ' . $land_navn. ''; echo $titleString;
           }
           else if ($randomNumberHeading == 6) {
            $titleString = $nationalName . ' shorters earned'; echo $titleString;
           }
           else if ($randomNumberHeading == 7) {
            $titleString = 'Positive for ' . $nationalName . ' shorters'; echo $titleString;
           }
           else if ($randomNumberHeading == 8) {
            $titleString = 'Shorters gained in ' . $land_navn . ''; echo $titleString;
           }
           else if ($randomNumberHeading == 9) {
            $titleString = $nationalName . ' shorters in the black'; echo $titleString;
           }
           else {
            $titleString = $nationalName . ' shorters earned ' . $today; echo $titleString;
           }  
    }

    //Shorters lost
    if ($shorterStatus == 'ned') { 

           if ($randomNumberHeading == 0 ) {
             $titleString = 'Bad day for shorters in '.  $land_navn; echo $titleString;  
           }
           else if ($randomNumberHeading == 1) {
            $titleString = 'Minus for shorters on the ' . $land_navn; echo $titleString;
           }
           else if ($randomNumberHeading == 2) {
            $titleString = 'Minus for shorters in ' . $land_navn; echo $titleString;
           }
           else if ($randomNumberHeading == 3) {
            $titleString = $land_navn . ': Shorters lose'; echo $titleString;
           }
           else if ($randomNumberHeading == 4) {
            $titleString = 'Red day for shorters in ' . $land_navn; echo $titleString;
           }
           else if ($randomNumberHeading == 5) {
            $titleString = $land_navn . ': Red day for shorters'; echo $titleString;
           }
           else if ($randomNumberHeading == 6) {
            $titleString = 'Negative for shorters in ' . $land_navn; echo $titleString;
           }
           else if ($randomNumberHeading == 7) {
            $titleString = 'Dropping values for shorters in ' . $land_navn; echo $titleString;
           }
           else if ($randomNumberHeading == 8) {
            $titleString = 'Loss for shorters in ' . $land_navn; echo $titleString;
           }
           else if ($randomNumberHeading == 9) {
            $titleString = $nationalName . ' shorters lost'; echo $titleString;
           }
           else {
            $titleString = ucfirst($today) . ': ' . $nationalName . ' shorters lost money'; echo $titleString;
           }  
    }
    if ($shorterStatus == 'equal') { 
        $titleString = 'Flat day for the shorters'; echo $titleString;
    } 
}
echo '</h4>';
echo '<span class="font-weight-bold">End rapport ' . $date . ': </span>';
//$storyText .= 'End rapport ' . $date . ': ';

//Open line about :
if ($stockExchangeStatus == 'ned') { 

    if ($shorterStatus == 'opp') {
        $storyText .=  $stockExchangeName . ' ' . $downwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';

        if ($shortersDidIt == 'better') {
            $storyText .=  $shortersBetter[0] . ' ' . $percent . ' percent. ';
        }
        else if ($shortersDidIt == 'equal') {
            $storyText .=  'Shorters matched that with an increase of ' . ' ' . abs($percent) . ' percent. ';
        }
        else {
            $storyText .=  'Shorters could not keep up, gaining just ' . ' ' . abs($percent) . ' percent. ';
        }
        $storyText .=  'Total earnings was ' . number_format($money, 0) . ' million ' . $currency_ticker . '. ';
    }
    if ($shorterStatus == 'ned') {
        $storyText .=  $stockExchangeName . ' ' . $downwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';
        $storyText .=  'The fall did not help the shorters, which ' . $downWordsLose[0] . ' ' . abs($percent) . ' percent on their positions. ';
       
       if ($randomNumber <=3 ) {
         $storyText .=  'Total losses was ' . abs(number_format($money, 0)) . ' million ' . $currency_ticker . '. ';
       }
       else if ($randomNumber <= 6) {
        $storyText .=  'In all, the ' . $nokkeltall[1][5] . ' players was out of ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . '. ';
       }
       else {
        $storyText .=  'Total losses was ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . ' among the ' . 
        $nokkeltall[1][5] . ' players.';
       }  
    }
    if ($shorterStatus == 'equal') {
        $storyText .=  $stockExchangeName . ' ' . $downwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';
        $storyText .=  'The shorters result was a disappointing ' . ' ' . $percent . ' percent. ';  
    }
}

//$storyText .=  '<br><br>---------------<br>';

if ($stockExchangeStatus == 'opp') { 
   
   if ($shorterStatus == 'opp') {
       
       $storyText .=  $stockExchangeName . ' ' . $upwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';

       if ($randomNumber <= 3 ) {
         $storyText .=  'Against the sentiment, the shorters earned ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . ', ';
         $storyText .=  'an increase of ' . abs($percent) . ' percent. ';
       }
       else if ($randomNumber <= 6) {
        $storyText .=  'In all, the ' . $nokkeltall[1][5] . ' short players earned ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . ', which is ' . abs($percent) . ' percent. ' ;
       }
       else {
        $storyText .=  'The ' . $nokkeltall[1][5] . ' shorters did good, beating the market and earning '. $percent . ' percent, adding ' . abs(number_format($money ,0)) . ' million ' . $currency_ticker . ' to their holdings.' ;
       }
    }

    if ($shorterStatus == 'ned') {
     
        $storyText .=  $stockExchangeName . ' ' . $upwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';

        if ($shortersDidIt == 'better') {
            $storyText .=  'The shorters losses were lesser than the market increase at just ' . abs($percent) . ' percent. ';
        }
        else {

           if ($randomNumber <= 3 ) {
            $storyText .=  'Shorters did worse than the market direction, losing ' . ' ' . abs($percent) . ' percent. ';
            $storyText .=  'Total losses was ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . '. ';
           }
           else if ($randomNumber <= 6) {
            $storyText .=  'The losses for the shorters was more than the market movement at ' . ' ' . abs($percent) . ' percent. ';
            $storyText .=  'Moneywise, that is ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . ' in losses.';
           }
           else {
            $storyText .=  'The losses for the shorters was more than the market movement at ' . ' ' . abs($percent) . ' percent. ';
            $storyText .=  'A aggregated sum of ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . ' was lost.';
           }          
        }  
    }

    if ($shorterStatus == 'equal') {
        $storyText .=  $stockExchangeName . ' ' . $downwords[0] . ' ' . abs($stockExchangeChange) . ' percent '  . $today . '. ';
        $storyText .=  'The shorters did equally good with a change of ' . ' ' . $percent . ' percent. ';  
    }
}


if ($stockExchangeStatus == 'equal') {
   
   if ($shorterStatus == 'opp') {
       
       $storyText .=  $stockExchangeName . '' . ' ended with a change of ' . ' ' . 
       number_format($stockExchangeChange, 0) . ' percent '  . $today . '. ';

       if ($randomNumber <= 3 ) {
         $storyText .=  'The shorters earned ' . number_format($money,0) . ' million ' . $currency_ticker . ', ';
         $storyText .=  'an increase of ' . abs($percent) . ' percent. ';
       }
       else if ($randomNumber <= 6) {
        $storyText .=  'In all, the ' . $nokkeltall[1][5] . ' players was earned ' . number_format($money,0) . ' million ' . $currency_ticker . ', which is ' . $percent . ' percent. ' ;
       }
       else {
        $storyText .=  'The ' . $nokkeltall[1][5] . ' shorters did very well, beating the market and earning '. 
            abs($percent) . ' percent, adding ' . number_format($money,0) . ' million ' . $currency_ticker . ' to their holdings.' ;
       }
    }

    if ($shorterStatus == 'ned') {
     
        $storyText .=  $stockExchangeName . '' . ' ended with a change of ' . ' ' . $stockExchangeChange . ' percent '  . $today . '.';

        if ($shortersDidIt == 'better') {
            $storyText .=  'The shorters losses were lesser than the market increase at just ' . abs($percent) . ' percent. ';
        }
        else {
           if ($randomNumber <= 3 ) {
            $storyText .=  'Shorters did worse than the market, losing ' . ' ' . abs($percent) . ' percent. ';
            $storyText .=  'Total losses was ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . '. ';
           }
           else if ($randomNumber <= 6) {
            $storyText .=  'The losses for the shorters was ' . ' ' . abs($percent) . ' percent. ';
            $storyText .=  'Moneywise, that is ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . ' in losses';
           }
           else {
            $storyText .=  'The losses for the shorters was more than the market movement at ' . ' ' . abs($percent) . ' percent. ';
            $storyText .=  'A aggregated sum of ' . abs(number_format($money,0)) . ' million ' . $currency_ticker . ' was lost.';
           }          
        }       
    }

    if ($shorterStatus == 'equal') {
        $storyText .=  $stockExchangeName . ' ' . $downwords[0] . ' ' . $stockExchangeChange . ' percent '  . $today . '. ';
        $storyText .=  'The shorters result was not as good at ' . ' ' . $percent . ' percent. ';  
    }
}

echo $storyText;

if ($devMode != 1) {
  //  Return the contents of the output buffer
  $htmlStr = ob_get_contents();
  // Clean (erase) the output buffer and turn off output buffering
  ob_end_clean(); 

  $newDestination = 'stories/endrapport/'. $today . '/' . $destination . $today . '.html';
  // Write final string to file

  //if no error!
  if ($skipsave == 0) {

    $destination = '../short_data/html/stories/story_'  . $land  .'_current.html';

    file_put_contents($destination, $htmlStr);
    
    echo 'Saving to ' . $destination . '<br>';
    //add to current
    //$currentDestination = 'stories/' . $destination  . 'current' . '.html';
    //file_put_contents($currentDestination, $htmlStr);



    //add to twitter   
    $page = new stdClass();
    $page->title = $titleString;
    $page->storytext = $storyText;
    $page->imagelink = $imagelink;

    $land_navn = strtolower($land_navn);
    $data = json_encode($page, JSON_UNESCAPED_SLASHES);
    file_put_contents('../short_data/stories/twitter_' . $land_navn . '.json', $data);
    echo 'Saving to ' . 'stories/twitter_' . $land_navn . '.json';
    
    echo '<br>----------------------------------------------';
    var_dump($page);
    var_dump($titleString);
    var_dump($storyText);
  }
}
?>


