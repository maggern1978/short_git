<?php


include '../production_europe/isopen.php';

$norway = '';
$sweden = '';
$denmark = '';
$finland = '';

$closedcounter = 0;
$closedcountries = [];

if (isopen('norway')) {
	$norway = TRUE;
}
else {
	$norway = FALSE;
  $closedcounter++;
  $closedcountries[] = 'Oslo stock exchange';
}

if (isopen('sweden')) {
	$sweden = TRUE;
}
else {
	$sweden = FALSE;
  $closedcounter++;
  $closedcountries[] = 'Nasdaq Stockholm';
}

if (isopen('denmark')) {
	$denmark = TRUE;
}
else {
	$denmark = FALSE;
  $closedcountries[] = 'Nasdaq Copenhagen';
  $closedcounter++;
}

if (isopen('finland')) {
  $finland = TRUE;
}
else {
  $finland = FALSE;
  $closedcountries[] = 'Nasdaq Helsinki';
  $closedcounter++;
}

$count = count($closedcountries);
$message = '';

if ($count > 1) {
  for ($i = 0; $i < $count; $i++ ) {
    $message = $message . $closedcountries[$i];
    if ($i != $count-2) {
      $message = $message . ', ';
    }
    else {
      $message = $message . ' and '  . $closedcountries[$count-1] . ' are closed ';;
      break;
    }
  }
}
else if (count($closedcountries) == 1) {
  $message = $closedcountries[0] . ' is closed ';
}

echo $message;

if ($message != '') {

    ob_start();   

    echo '<div class="container">';

      echo '<div class="row justify-content-center">';

         

            if ($message != '') {

                echo $message . ' ' . strtolower(date('l')) . '.<br>';

            }

        

      echo '</div>';

    echo '</div>';


    $htmlStr = ob_get_contents();

    // Clean (erase) the output buffer and turn off output buffering

    ob_end_clean(); 

}

else {

    $htmlStr = '';

}

  $currentDestination = '../short_data/html/open.html';

  file_put_contents($currentDestination, $htmlStr);

  

  ?>

  

