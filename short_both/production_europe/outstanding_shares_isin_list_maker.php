<?php 

if (!function_exists('get_url_newest_source')) {

    function get_url_newest_source($ticker)
    {

        $yahoo_url = '../short_data/json/outstandingstocks_yahoo/' . strtolower($ticker) . '.json';
        $eod_url = '../short_data/json/outstanding_shares_part/' . strtolower($ticker) . '.json';

        if (file_exists($yahoo_url) and file_exists($eod_url))
        {
            $yahoo_time = filemtime($yahoo_url);
            $eod_time = filemtime( $eod_url);
        }
        else if (file_exists($eod_url))
        {
            return $eod_url;
        }
        else if (file_exists($yahoo_url))
        {
            return $yahoo_url;
        }              
        else 
        {
            return false;
        }

     //both exists, lets compare dates
    //if less than two days ago, choose eod.
        $two_days_ago = date('Y-m-d', strtotime("-2 weekdays", strtotime("now")));
    //echo $two_days_ago . '<br>';
    //echo strtotime($two_days_ago);
    //echo '<br>';    

        if ($eod_time >= $two_days_ago)
        {
            blueecho('Both exists. Returning eod (less than two days) ' . $eod_url . '<br>');
            return $eod_url;
        }  

    //else return newest!
        if ($eod_time >= $yahoo_time)
        {
           yellowecho('Returning eod ' . $eod_url . '<br>');
           return $eod_url;
       }
       else
       {
           orangeecho('Returning yahoo ' . $eod_url . '<br>');
           return $yahoo_time;
       }

   }
}


include '../production_europe/functions.php';

set_time_limit(6000);

echo '<br>';
$path = '../short_data/json/outstanding_shares_part/';
$isin_list_url = '../short_data/dataraw/isin/hovedlisten_landkoder.csv';

if (!$isinlist = readCSV($isin_list_url))
{  
    echo 'Error, reading...returning. ';
    return;
}



$errorcounter = 0;

foreach ($isinlist as $key => $row)
{

    flush_start();

    if (!isset($row[1]) or !isset($row[8]))
    {
        continue;
    }

    $targetticker = $row[1] . '.' . $row[8];
    $targetticker = str_replace(' ', '-', $targetticker);
    $targetfilename = $path . strtolower($targetticker) . '.json';


    if (!$url = get_url_newest_source($targetticker))
    {
        continue;
    }
    else
    {
        echo $url;
        echo '<br>';
    }


    if (!$data = readJSON($url))
    {
        echo $key . '. ';
        errorecho($targetfilename . '. Cannot read file. <br>');
        $errorcounter++;
        continue;
    }

    if (!isset($data['SharesOutstanding']))
    {
        echo $key . '. ';
        echo $targetfilename . '. ';
        echo 'SharesStats not set, continuing<br>';
        $errorcounter++;
        continue; 
    }
    else if ($data['SharesOutstanding'] == null)
    {
        echo $key . '. ';
        echo $targetfilename . '. ';
        echo 'Shares are null, continuing<br>';
        $errorcounter++;
        continue; 
    }
    else if ($data['SharesOutstanding'] == null)
    {
        echo $key . '. ';
        echo $targetfilename . '. ';
        echo 'SharesStats are null, continuing<br>';
        $errorcounter++;
        continue; 
    }

    echo $key . '. ';
    echo $targetfilename . '. ';
    successecho(' ok <br>');

    $isinlist[$key][9] = (string)$data['SharesOutstanding'];
    $isinlist[$key][10] = date('Y-m-d', filemtime($url));

    flush_end(); 
}


//filter out ' A/S' in list

foreach ($isinlist as $key => $row)
{

    $isinlist[$key][2] = str_ireplace(" A/S", '', $row[2]);
    $isinlist[$key][2] = trim($isinlist[$key][2]);

}

echo '<br>Number of errors: ' . $errorcounter . '<br>';
echo 'Saving ' . $isin_list_url . '<br>';

$isin_sorted_name = '../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv';

//lagre sorted array etter isin
usort($isinlist, function ($item1, $item2) {
    return $item2[0] < $item1[0];
});

saveCSVsemikolon($isinlist, $isin_sorted_name);

$isin_list_url = '../short_data/dataraw/isin/hovedlisten_landkoder.csv';

//lagre sorted array etter navn
usort($isinlist, function ($item1, $item2) {
    return mb_strtolower($item2[2]) < mb_strtolower($item1[2]);
});

saveCSVsemikolon($isinlist, $isin_list_url);



?>

