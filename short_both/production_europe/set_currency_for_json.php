<?php

//trenger en inputfile og land for basecurrency
include '../production_europe/functions.php';

//$inputFile = '../short_data/dataraw/' .  $land . '/' .  $land . '_current.json';
//read inputfile

if (!$data = readJSON($inputFile))
{
	errorecho('Could not read ' . $inputFile . ' returning...');
	return;
}

echo 'Inputfile is ' . $inputFile . '<br>';

$name_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv' );
$isin_list = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder_isinsortert.csv' );


$datacount = count($data);


for ($i = 0; $i < $datacount; $i++)
{
	//ta et selskap

	$targetisin = $data[$i]['ISIN'];
	$targetname = mb_strtolower($data[$i]['Name']);

	//finn i isinlisten
	$foundsuccess = 0;

	if ($row = binary_search_multiple_hits($targetisin, $targetname, $isin_list, $name_list, $land))
	{
		$currency = $row[3];
		$foundsuccess = 1;
	}

	if ($foundsuccess == 0)
	{
		errorecho($i . '. ' . $targetname . ' not found, continuing without setting currency for ' .  count($data[$i]['Positions']) . ' ...<br>');
		continue;
	}
	//appling to positions
	$positionscount = count($data[$i]['Positions']);
	
	echo $i . '. Found ' .  $targetname . '. Setting ' . $positionscount . ' positions <br>';
	for ($x = 0; $x < $positionscount; $x++)
	{
		$data[$i]['Positions'][$x]['currency'] = $currency;
	
	}
}

saveJSON($data, $inputFile);

?>