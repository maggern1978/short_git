<?php

//splits files into smaller segments for both players and companies
include_once '../production_europe/functions.php';
set_time_limit(160);

//$land = 'germany';
//$land = 'united_kingdom';
//$land = 'sweden';
//make directories

$dir = '../short_data/dataraw/stock.prices/' . $land . '/'; //history

if (!$data = readJSON($dir . 'all_positions_' . $land . '.json'))
{
	errorecho ('Could not read json');
	return;
}

//flatten the structure
$allpositions = [];
$allplayerNames = [];
$allcompanyNames = [];
$datacount = count($data);

for ($i = 0; $i < $datacount; $i++)
{
	
	$data[$i]['positions'] = array_values($data[$i]['positions']);
	$allcompanyNames[]= $data[$i]['StockName'];	
	$allplayerNames[] = $data[$i]['playerName'];	
	
	$poscount = count($data[$i]['positions']);
	
	for ($x = 0; $x < $poscount; $x++)
	{
		$data[$i]['positions'][$x]['playerTwoLetters'] = getFirstTwoLetters($data[$i]['playerName']);
		$data[$i]['positions'][$x]['companyTwoLetters'] = getFirstTwoLetters($data[$i]['StockName']);
		$data[$i]['positions'][$x]['ticker'] = $data[$i]['ticker'];
				
		$allpositions[] = $data[$i]['positions'][$x];
		unset($data[$i]['positions'][$x]);	
	}
}

//collect all the names
//get the first two letters
$allplayerNames = array_unique($allplayerNames);
$allplayerNames = array_values($allplayerNames);
$allplayerNamesCount = count($allplayerNames);
$playerNamesTwoLettersBox = [];

$allcompanyNames = array_unique($allcompanyNames);
$allcompanyNames = array_values($allcompanyNames);
$allcompanyNamesCount = count($allcompanyNames);
$companyNamesTwoLettersBox = [];

//generate the two letters arrays

for ($i = 0; $i < $allplayerNamesCount; $i++)
{
	$playerNamesTwoLettersBox[] = getFirstTwoLetters($allplayerNames[$i]);
}

for ($i = 0; $i < $allcompanyNamesCount; $i++)
{
	$companyNamesTwoLettersBox[] = getFirstTwoLetters($allcompanyNames[$i]);
}

$companyNamesTwoLettersBox = array_unique($companyNamesTwoLettersBox);
$companyNamesTwoLettersBox = array_values($companyNamesTwoLettersBox);

$playerNamesTwoLettersBox = array_unique($playerNamesTwoLettersBox);
$playerNamesTwoLettersBox = array_values($playerNamesTwoLettersBox);


//go through the two letter list for companies
$basedirectory = $dir . 'companies/';

$companyNamesTwoLettersBoxCount = count($companyNamesTwoLettersBox);
$allpositionsCount = count($allpositions);

echo '<br><br>';
echo 'Doing ' . $companyNamesTwoLettersBoxCount . ' companies...<br>';

$summaryAllHolderPlayers = [];

for ($i = 0; $i < $companyNamesTwoLettersBoxCount; $i++)
{
	//echo $i . '. Doing "' . $companyNamesTwoLettersBox[$i] . '".';
	$tempholder = [];
	
	$localCompanyNamesBox = [];
	for ($x = 0; $x < $allpositionsCount; $x++)
	{
		
			if ($allpositions[$x]['companyTwoLetters'] == $companyNamesTwoLettersBox[$i])
			{
				$tempholder[] = $allpositions[$x];
				$localCompanyNamesBox[] = $allpositions[$x]['StockName']; 
			}
		
	}
	
	$localCompanyNamesBox = array_unique($localCompanyNamesBox);
	$localCompanyNamesBox = array_values($localCompanyNamesBox);
	$localCompanyNamesBoxCount = count($localCompanyNamesBox);
	
	$localAllholder = [];
	
	//gå igjennom navnene
	for ($z = 0; $z < $localCompanyNamesBoxCount; $z++)
	{
		$hitholder = [];
		
		//gå igjennom posisjonene
		
		$tempholderCount = count($tempholder);
		
		for ($g = 0; $g < $tempholderCount; $g++)
		{
			if ($localCompanyNamesBox[$z] == $tempholder[$g]['StockName'])
			{
				$hitholder[] = $tempholder[$g];
			}
		}

		usort($hitholder, function ($item1, $item2) {
			return $item2['ShortingDateStart'] > $item1['ShortingDateStart'];
		});
		
		//lag input pr navn
		$localAllholder[$z]['StockName'] = $localCompanyNamesBox[$z];
		$localAllholder[$z]['ISIN'] = $hitholder[0]['ISIN'];
		$localAllholder[$z]['ticker'] = $hitholder[0]['ticker'];

		//calculate summaries

		$hitholderCount = count($hitholder);
		$hitholderNameBox = [];

		//ta inn playernavnene
		for ($u = 0; $u < $hitholderCount; $u++)
		{
			$hitholderNameBox[] = $hitholder[$u]['PositionHolder'];
		}

		$hitholderNameBox = array_values(array_unique($hitholderNameBox));
		$hitholderNameBoxCount = count($hitholderNameBox);

		$summaryBox = [];

		//ta hvert navn og regn ut
		for ($u = 0; $u < $hitholderNameBoxCount; $u++)
		{
			//echo $u . '. ' . $hitholderNameBox[$u] . ' <br>';
			$localpositionscount = 0;
			$SumOfAllValueChangeForPlayer = 0;

			//gå igjennom alle posisjonene
			for ($o = 0; $o < $hitholderCount; $o++)
			{
				if (!isset($hitholder[$o]['base_currency_multiply_factor']))
				{
					//unset($hitholder[$o]);
					//continue;
					$hitholder[$o]['base_currency_multiply_factor'] = 1;
				}				

				if ($hitholderNameBox[$u] == $hitholder[$o]['PositionHolder'])
				{		
					$localpositionscount++;

					if (is_numeric($hitholder[$o]['ShortingDateStartStockPrice']) and is_numeric($hitholder[$o]['ShortingDateEndStockPrice']))
					{
						
						$SumOfAllValueChangeForPlayer += ($hitholder[$o]['ShortingDateStartStockPrice'] - $hitholder[$o]['ShortingDateEndStockPrice']) * $hitholder[$o]['NumberOfStocks'] * $hitholder[$o]['base_currency_multiply_factor'];
					}

					
				}

			}

			$hitholder = array_values($hitholder);

			$object = new stdClass;
			$object->PositionHolder = $hitholderNameBox[$u];
			$object->numPositions = $localpositionscount;
			$object->SumOfAllValueChangeForPlayer = round($SumOfAllValueChangeForPlayer,2);
			$summaryBox[] = (array)$object;
			$summaryAllHolderPlayers[] = (array)$object;

		}
		$localAllholder[$z]['positions'] = $hitholder;
		$localAllholder[$z]['summary'] = $summaryBox;
	}
	
	//lag directory
	$targetletters = $companyNamesTwoLettersBox[$i];	
	
	//check and make directory if needed
	makedirectory($basedirectory . $targetletters[0]);
	makedirectory($basedirectory . $targetletters[0] . '/' . $targetletters[1]);
	
	saveJSONsilent($localAllholder, $basedirectory . $targetletters[0] . '/' . $targetletters[1] . '/' . $targetletters[0] . $targetletters[1] . '.json');

}
//add up players to make index
$allplayerNamesExport = [];

$summaryAllHolderPlayersCount = count($summaryAllHolderPlayers);

for ($i = 0; $i < $allplayerNamesCount; $i++)
{
	//take a name
	$targetname = $allplayerNames[$i];
	$allplayerNamesExport[$i]['numPositions'] = 0;
	$allplayerNamesExport[$i]['PositionHolder'] = $targetname;

	for ($x = 0; $x < $summaryAllHolderPlayersCount; $x++)
	{
		if ($targetname == $summaryAllHolderPlayers[$x]['PositionHolder'])
		{
			$allplayerNamesExport[$i]['numPositions'] += $summaryAllHolderPlayers[$x]['numPositions'];
		}

	}

}

usort($allplayerNamesExport, function ($item1, $item2) {
	return $item2['PositionHolder'] < $item1['PositionHolder'];
});

saveJSON($allplayerNamesExport, $dir . 'players/players.index.json');

//same for players
//go through the two letter list for companies
$basedirectory = $dir . 'players/';

$playerNamesTwoLettersBoxCount = count($playerNamesTwoLettersBox); 

$summaryAllHolderCompanies = [];

echo '<br><br>';
echo 'Doing ' . $playerNamesTwoLettersBoxCount . ' indexes...<br>';



for ($i = 0; $i < $playerNamesTwoLettersBoxCount; $i++)
{
	echo $i . '. Doing "' . $playerNamesTwoLettersBox[$i] . '". ';
	$tempholder = [];
	
	$localPlayersNamesBox = [];

	//getting posistions with matching two letters
	for ($x = 0; $x < $allpositionsCount; $x++)
	{
		
			if ($allpositions[$x]['playerTwoLetters'] == $playerNamesTwoLettersBox[$i])
			{
				$tempholder[] = $allpositions[$x];
				$localPlayersNamesBox[] = $allpositions[$x]['PositionHolder']; 
			}
		
	}
	
	//grouping by player name
	$localPlayersNamesBox = array_unique($localPlayersNamesBox);
	$localPlayersNamesBox = array_values($localPlayersNamesBox);
	$localPlayersNamesBoxCount = count($localPlayersNamesBox);
	
	$localAllholder = [];
	
	//gå igjennom navnene
	for ($z = 0; $z < $localPlayersNamesBoxCount; $z++)
	{
		$hitholder = [];
		
		//gå igjennom posisjonene
		
		$tempholderCount = count($tempholder);
		
		for ($g = 0; $g < $tempholderCount; $g++)
		{
			if ($localPlayersNamesBox[$z] == $tempholder[$g]['PositionHolder'])
			{
				$hitholder[] = $tempholder[$g];
			}
		}
		usort($hitholder, function ($item1, $item2) {
			return $item2['ShortingDateStart'] > $item1['ShortingDateStart'];
		});


		//calculate summaries

		$hitholderCount = count($hitholder);
		$hitholderNameBox = [];

		//ta inn playernavnene
		for ($u = 0; $u < $hitholderCount; $u++)
		{
			$hitholderNameBox[] = $hitholder[$u]['StockName'];
		}

		$hitholderNameBox = array_values(array_unique($hitholderNameBox));
		$hitholderNameBoxCount = count($hitholderNameBox);

		$summaryBox = [];

		//ta hvert navn og regn ut
		for ($u = 0; $u < $hitholderNameBoxCount; $u++)
		{
			//echo $u . '. ' . $hitholderNameBox[$u] . ' <br>';
			$localpositionscount = 0;
			$SumOfAllValueChangeForPlayer = 0;

			//gå igjennom alle posisjonene
			for ($o = 0; $o < $hitholderCount; $o++)
			{
				if ($hitholderNameBox[$u] == $hitholder[$o]['StockName'])
				{
					if (isset($hitholder[$o]['base_currency_multiply_factor']))
					{
						$localpositionscount++;

						if (is_numeric($hitholder[$o]['ShortingDateStartStockPrice']) and is_numeric($hitholder[$o]['ShortingDateEndStockPrice']))
						{
							$SumOfAllValueChangeForPlayer += ($hitholder[$o]['ShortingDateStartStockPrice'] - $hitholder[$o]['ShortingDateEndStockPrice']) * $hitholder[$o]['NumberOfStocks'] * $hitholder[$o]['base_currency_multiply_factor'];
						}

						
					}
					else
					{
						errorecho('base_currency_multiply_factor not set for' . $hitholder[$o]['StockName'] . '<br>');
					}
				}
			}

			$object = new stdClass;
			$object->StockName = $hitholderNameBox[$u];
			$object->numPositions = $localpositionscount;
			$object->SumOfAllValueChangeForPlayer = round($SumOfAllValueChangeForPlayer,2);
			$summaryBox[] = (array)$object;
			$summaryAllHolderCompanies[] = (array)$object;;

		}
		
		//lag input pr navn
		$localAllholder[$z]['PositionHolder'] = $localPlayersNamesBox[$z];
		$localAllholder[$z]['positions'] = $hitholder;
		$localAllholder[$z]['summary'] = $summaryBox;

	}
	
	//lag directory
	$targetletters = $playerNamesTwoLettersBox[$i];	
	
	//check and make directory if needed
	makedirectory($basedirectory . $targetletters[0]);
	makedirectory($basedirectory . $targetletters[0] . '/' . $targetletters[1]);
	saveJSON($localAllholder, $basedirectory . $targetletters[0] . '/' . $targetletters[1] . '/' . $targetletters[0] . $targetletters[1] . '.json');

}

//saveJSON($summaryAllHolderCompanies, $dir . 'companies/companies.index.json');

//add up companies to make index
$allcompaniesNamesExport = [];

$summaryAllHolderCompaniescount = count($summaryAllHolderCompanies);

$addedupcounter = 0;
for ($i = 0; $i < $allcompanyNamesCount; $i++)
{
	//take a name
	$targetname = mb_strtolower($allcompanyNames[$i]);
	$allcompaniesNamesExport[$i]['numPositions'] = 0;
	$allcompaniesNamesExport[$i]['StockName'] = $targetname;

	for ($x = 0; $x < $summaryAllHolderPlayersCount; $x++)
	{
		if (isset($summaryAllHolderCompanies[$x]['StockName']) and $targetname == mb_strtolower($summaryAllHolderCompanies[$x]['StockName']))
		{
			$allcompaniesNamesExport[$i]['numPositions'] += $summaryAllHolderCompanies[$x]['numPositions'];
			//echo '$summaryAllHolderCompanies[$x]["numPositions"] er ' . $summaryAllHolderCompanies[$x]['numPositions'] . '<br>';
		}

	}

}

//sorter 

usort($allcompaniesNamesExport, function($a, $b) {
    return $a['StockName'] <=> $b['StockName'];
});

saveJSON($allcompaniesNamesExport, $dir . 'companies/companies.index.json');

$allpositions = null;
$localAllholder = null;
$data = null;

?>