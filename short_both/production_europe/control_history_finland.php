<?php 

set_time_limit(5000);

$production = 1;
//nedlasting av aksjer ferdig!
require '../production_europe/functions.php';
require '../production_europe/logger.php';
require('../production_europe/run.php');

$land = 'finland';
run('../production_europe/finland_dl_historical_positions.php'); 

//Konverter nedlastet fil til csv
// Ta alle de aktive posisjonene og legg til de er aktive i csv-filen
run('../production_europe/setActivePositions.php');
run('../production_europe/mergeCSV.php');

$inputFileName = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_merged.csv';
//run('../production_europe/csv_duplicates_finder.php');
run('../production_europe/csv_to_json_builder_history.php');
run('../production_europe/csv_to_json_history_positions_cleaner.php');

//gjør om navnene fra myndigheter til det som står i hovedlisten for selskaper. 
$mode = 'finlandhistory';
$inputFile = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.json';
include('../production_europe/json_set_names_to_isinlist.php');

$land = 'finland';
run('../production_europe/verdiutregning_selskap_sweden.php'); //lager all_positions_$land.json og positions_map_$land.json

run('../production_europe/json_splitter.php'); //splitter ut stock.prices verdiene

$land = 'finland';
run('../production_europe/graph_short_generator.php'); //lager grafdata

echo '<br>History ' . $land  . ' completed! :D';
?>