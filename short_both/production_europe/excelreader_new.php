<?php
$placement = 'short_data';

//read date from generated file
$date = date('Y-m-d');

//Fiks riktig dato-formatering
$new_date = str_replace(".", "-",$date,$i);

//bygg url 
$fi = '.sweden';

$file_fi = $date . $fi;



//delete contents of file 
$fh = fopen('../' . $placement . '/dataraw/sweden/sweden_current.csv', 'w' );
fclose($fh);

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');

//$writer = new Xlsx($spreadsheet);
//$writer->save('hello world.xlsx');


$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("../". $placement . "/dataraw/sweden/sweden_current.xlsx");

$dataArray = $spreadsheet->getActiveSheet()
    ->ToArray(
           // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
    );

$success = 0;
if (!isset($dataArray[8]['B'])) {
	echo 'Data is not set! Download of active positions not working!';
	logger('Klarte ikke laste ned svenske active posisjoner!', '');
	;
}
else {
	$success = 1;
}

$counter = 0;
if ($success = 1) {
	foreach ($dataArray as $Row) {

			if ($counter > 5 and isset($Row['A'])) {
				//var_dump($Row);
			$rad1 = $Row['A'];
			$rad1 = rtrim($rad1);

			$rad2 = rtrim($Row['B']);
			
			//trim vekk rare verdier
			$rad2 = rtrim($rad2);
			//echo $rad3;
			$rad2 = str_replace( chr( 194 ) . chr( 160 ), '', $rad2);
			//echo $rad3;
			$rad2 = strtoupper($rad2);
			
			//fjerne space i isin
			$rad3 = preg_replace('/\s+/', '', $Row['C']);
			//$rad4 = $Row[3];
			
			//formater bort kommaer

			$rad4 = str_replace(',','.', $Row['D']);

			//$rad5 = $Row[4];
			
			$rad5 = (string)$Row['E'];
            
            

			//echo 'Dato er: ' . $rad5 . '<br>';
				
			

			$lagre_array = array($rad1, $rad2, $rad3, $rad4, $rad5);
			//echo $counter . '<br>';
	    	//saveCSV($Row, 'testcsv.csv');
	    	//echo 'Saving dataraw/fi.se/fi.se_current.csv<br>';
	    	
			saveCSV4($lagre_array, '../' . $placement . '/dataraw/sweden/sweden_current.csv');
			}
			//echo $counter . ' ' ;

			$counter++;
	}
}

//copy file 
$copy_filename = '../' . $placement . '/dataraw/sweden/' . $file_fi . '.csv';
copy("../" . $placement . "/dataraw/sweden/sweden_current.csv", $copy_filename);

//Lagre

echo 'Saving ' . $copy_filename . '<br>';

function saveCSV4($input, $file_location_and_name) {
$fp = fopen($file_location_and_name, 'a');

fputcsv($fp, $input);

fclose($fp);
}



 //obs, dataene må ha semikolon som skilletegn
 function readCSV888($file_location_and_name) {

    $csvFile = file($file_location_and_name);

    //Les in dataene 
    $name = [];
    foreach ($csvFile as $line) {
        $name[] = str_getcsv($line, ';');
    }

    //ordne utf koding
    $counter = 0;
    foreach ($name as &$entries) {
    	$data[$counter] = array_map("utf8_encode", $entries);
    	$counter++;
    }
    return $name;
}

//Lagre
function saveCSV777($input, $file_location_and_name) {
$fp = fopen($file_location_and_name, 'w');

foreach ($input as $fields) {
    fputcsv($fp, $fields);
}

fclose($fp);
}

//Remove variables for memory footprint

unset($spreadsheet);
unset($dataArray);


?>