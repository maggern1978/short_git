<?php

if (!function_exists('daysBetween')) 
{
    function daysBetween($dt1, $dt2) 
    {



        return date_diff(
            date_create($dt2),  
            date_create($dt1)
            )->format('%a');
    }
}

$development = 0; // pass på, denne gjør om på dataflyten

if (isset($land)) {
    $country = $land;    
}

//$land = 'germany';

$country = $land;

include '../production_europe/options_set_currency_by_land.php';
$positions3 = '../short_data/datacalc/newpositions/' . $land . '/newpositions_' . $land . '.json';
require '../production_europe/namelink.php';
require '../production_europe/functions.php';

$pageTitle = 'Newest shorted companies in ' . ucwords($country);
$json = readJSON($positions3);
$count = count($json);

//var_dump($json);

if ($development == 1) {
 include '../short_data/html/header.html'; 
}

?>

<div class="container mb-3">
  <div class="row ">
    <div class="col-12 col-sm-12 col-md-6 col-lg-4">
        <div>
           <h5 class=""><span data-toggle="tooltip" title="Days since the companies aggregated short position went from zero to a positive value.">Newest shorted companies</span></h5>
       </div>
       <?php


// Turn on output buffering
       if ($development == 0) {
        ob_start();
    }

    $counter = 0;
    echo '<div class="d-flex flex-column">';

    foreach ($json  as $key => $company) {
        $counter++;
        $shorter =  nametolink($company['name']);
        
        if ($company['firstZeroDate'] == '-')
        {
            continue;
        }

        echo '<div class="d-flex flex-row ">';
        echo '<div class="px-2 newposition-numbers-box newposition-box bg-primary text-white"><span class="h4 newposition-font text-white pr-1">';


        $days = number_format(daysBetween(date("Y-m-d"), $company['firstZeroDate']), 0, ".",",");
        if ($days == 1) {
            echo $days;
            echo '</span><span class="newposition-small-font">day ago</span>';   
        }
        else {
            echo $days;
            echo '</span><span class="newposition-small-font">days ago</span>';   
        }

    //echo '</span><span class="newposition-small-font">days ago</span>';   
        echo '</div>';
        echo '<div data-toggle="tooltip" title="' . "Click to see company's active positions." . '" class="ml-2 mt-1 h4 black-text newposition-company">';
        echo '<a href="details_company.php?company=';
        echo nametolink($company['name']);
        echo '&land=';
        echo $country; 
    //http://sh/details_company.php?company=NETCOMPANY%20GROUP&land=denmark
        echo '">';
        echo '<span class="text-black">';
        echo $company['name'];
        echo '</span>';
        echo '</a>';
        echo '</div>';
        echo '</div>';
        echo '<div class="newpositon-border-bottom';

        if ($counter == 4) {
            echo '">';
        }
        else {
            echo ' newposition-bottom-padding">';
        }

        echo '</div>';

    //sett antall
        if ($counter > 3) {
            break;
        }
    } 

    echo '</div>';   

    $filenameStart = '../short_data/html/newestpositions_';
    $filenameMiddle = $country;
    $filenameEnd = '.html';
    $filename = $filenameStart . $filenameMiddle . $filenameEnd;

    if ($development == 0) {
//  Return the contents of the output buffer
        $htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
        ob_end_clean(); 
// Write final string to file
        file_put_contents($filename, $htmlStr);
    }

    ?>
