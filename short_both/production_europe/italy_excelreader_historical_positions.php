<?php

//delete contents of file 
$saveurl = '../short_data/dataraw/italy/italy_current.csv';

include '../production_europe/functions.php';
include '../production_europe/logger.php';

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');

//$writer = new Xlsx($spreadsheet);
//$writer->save('hello world.xlsx');
$url = '../short_data/dataraw/italy/italy_download.xlsx';

$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($url);

$sheetnames = $spreadsheet->getSheetNames();
//var_dump($sheetnames);

$spreadsheet->setActiveSheetIndex(0);

$dataArray = $spreadsheet->getActiveSheet()
    ->ToArray(
           // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
    );

//var_dump($dataArray);

$success = 0;
if (!isset($dataArray[8]['B'])) {
	echo 'Data is not set! Download of active positions not working!';
	logger('Klarte ikke laste ned italienske active posisjoner!', ' i italy excelreader');
	return;
}
else {
	$success = 1;
}

$holderBox = [];

$counter = 0;
if ($success = 1) {
	foreach ($dataArray as $key => $Row) {

			if ($counter > 1 and isset($Row['B'])) {

			$rad1 = rtrim($Row['A']);

			//echo $rad1;

			$rad2 = rtrim($Row['B']);
						
			//fjerne space i isin
			$rad3 = $Row['C'];
			//$rad4 = $Row[3];
			
			//formater bort kommaer
			$rad4 = (string)$Row['D'];

			$rad4 = str_replace(",", ".", $rad4);

			//$rad5 = $Row[4];
			$rad5 = $Row['E'];
			//$rad5 = $Row['E'];
				
			//echo '"' . $rad5 . '"';

			$myDateTime = DateTime::createFromFormat('d/m/Y', $rad5);
			$rad5  = $myDateTime->format('Y-m-d');
				
			$lagre_array = array($rad1, $rad2, $rad3, $rad4, $rad5);
			//echo $counter . '<br>';
	    	//saveCSV($Row, 'testcsv.csv');
	    	
			$holderBox[] = $lagre_array;
			}
			//echo $counter . ' ' ;

			$counter++;
	}
}


//var_dump($holderBox);

//sortere alle posisjoner på dato
usort($holderBox, function($a, $b) {
    return $a[4] < $b[4];
});

//Lagre
saveCSVx($holderBox, $saveurl);

//historiske posisjoner

$holderBox = [];

$spreadsheet->setActiveSheetIndex(1);

$dataArray = $spreadsheet->getActiveSheet()
    ->ToArray(
           // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
    );

$saveurl = '../short_data/dataraw/italy.history/italy.history_current.csv';

$counter = 0;

foreach ($dataArray as $key => $Row) {

			if ($counter > 1 and isset($Row['B'])) {

			$rad1 = rtrim($Row['A']);

			//echo $rad1;

			$rad2 = rtrim($Row['B']);
						
			//fjerne space i isin
			$rad3 = $Row['C'];
			//$rad4 = $Row[3];
			
			//formater bort kommaer
			$rad4 = (string)$Row['D'];

			$rad4 = str_replace(",", ".", $rad4);

			//$rad5 = $Row[4];
			$rad5 = $Row['E'];
			//$rad5 = $Row['E'];
				
			//echo '"' . $rad5 . '"';

			$myDateTime = DateTime::createFromFormat('d/m/Y', $rad5);
			$rad5  = $myDateTime->format('Y-m-d');
				
			$lagre_array = array($rad1, $rad2, $rad3, $rad4, $rad5);
			//echo $counter . '<br>';
	    	//saveCSV($Row, 'testcsv.csv');
	    	
			$holderBox[] = $lagre_array;
			}
			//echo $counter . ' ' ;

			$counter++;
	}

//sortere alle posisjoner på dato
usort($holderBox, function($a, $b) {
    return $a[4] < $b[4];
});


saveCSVx($holderBox, $saveurl);



?>