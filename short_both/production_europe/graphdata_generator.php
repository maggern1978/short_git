<?php 

set_time_limit(5000);

//$land = "sweden";
$millisecondsts1 = microtime(true);

//for just doing a few companies
//$land = 'germany';

require '../production_europe/functions.php';
require '../production_europe/logger.php';

$savefolder = '../short_data/datacalc/graphdata/' . $land . '/';
$activePositions = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
$fil = '../short_data/dataraw/' . $land . '/' . $land . '_current.json';
$shortliste = '../short_data/datacalc/shortlisten_' . $land . '_current.csv';
$stockPricesUrl = '../short_data/history/';
$saveUrl = '../short_data/dataraw/stock.prices/' . $land . '/';
$allPositions = '../short_data/dataraw/stock.prices/' . $land . '/all_positions_' . $land . '.json';
$graphFolder = '../short_data/history/';

//leser inn isinkoder
echo 'Land er ' . $land . '<br>';

//les inn finanstilsynet_current.json med alle posisjoner
$selskapsposisjoner = file_get_contents($allPositions);
$selskapsposisjoner = json_decode($selskapsposisjoner, true);
$selskapsposisjoner = array_values($selskapsposisjoner);

$success = 0;

if (!$countSelskapsposisjoner = count($selskapsposisjoner)) {
  echo '$countSelskapsposisjoner er tom!';
  return;
}

//collect all names
//find newest position date

$nameBox = [];
$newestdateinpositions = '2001-01-01';

for ($i = 0; $i < $countSelskapsposisjoner; $i++) {
  $nameBox[] = $selskapsposisjoner[$i]['StockName'];


  //$selskapsposisjoner[$i]['positions'] = array_values($selskapsposisjoner[$i]['positions']);

  $localcount = count($selskapsposisjoner[$i]['positions']);
  for ($x = 0; $x < $localcount; $x++)
  {
    if ($newestdateinpositions < $selskapsposisjoner[$i]['positions'][$x]['ShortingDateEnd'])
    {
      $newestdateinpositions = $selskapsposisjoner[$i]['positions'][$x]['ShortingDateEnd'];
    }
  }
}

$newestdateinpositions = date('Y-m-d');

echo 'Nyeste posisjon: ' . $newestdateinpositions . '<br>';
$nameBox = array_unique($nameBox);
$nameBox = array_values($nameBox);
$nameBoxCount = count($nameBox);

echo '<br> Making graphs for ' . $nameBoxCount .  ' companies: ' . $land  .  ' <br> ';  

$ojectholderAll = [];

for ($i = 0; $i < $nameBoxCount; $i++) {
  $nameTarget = $nameBox[$i];
  $ticker = '';
  //samle inn alle posisjoner for selskap
  $posisjonsholder = [];

  for ($x = 0; $x < $countSelskapsposisjoner; $x++) 
  {
    if ($nameTarget == $selskapsposisjoner[$x]['StockName']) 
    {
      $ticker = $selskapsposisjoner[$x]['ticker'];
        //echo 'Found ' . $nameTarget . '<br>';
      foreach ($selskapsposisjoner[$x]['positions'] as $position) 
      {
        $posisjonsholder[] = $position;
      }

    }
  }

  //finn den eldste og nyeste posisjonen
  $posisjonsholdercount = count($posisjonsholder);

  $oldest = '';
  $newest = '';
  for ($x = 0; $x < $posisjonsholdercount; $x++) {
    //set first
    if ($x == 0) {
      $oldest = $posisjonsholder[$x]['ShortingDateStart'];
      $newest = $posisjonsholder[$x]['ShortingDateEnd'];
      continue; 
    }

    if ($posisjonsholder[$x]['ShortingDateStart'] < $oldest) {
      $oldest = $posisjonsholder[$x]['ShortingDateStart'];
    }

    if ($posisjonsholder[$x]['ShortingDateEnd'] < $newest) {
     $newest = $posisjonsholder[$x]['ShortingDateEnd'];
   }

 }

  //neste er alltid i dag
 $newest = (string)date("Y-m-d");

   //echo $newest .'<br>';
  //set date range
 $dateArray = date_range($oldest, $newest, "+1 day", "Y-m-d");
 $dateArrayCount = count($dateArray);
  //echo 'Antall dager er ' . $dateArrayCount . '<br>';

 $ojectholder = [];

 for ($x = 0; $x < $dateArrayCount; $x++) {
  $dateTarget = $dateArray[$x];

  //if date is newer than positions date
  if ($newestdateinpositions < $dateTarget)
  {
    $date = new stdClass();
    $date->date = $dateTarget;
    $date->price = '';
    $date->name = $nameTarget;
    $date->ticker = $ticker;
    $ojectholder[] = $date;
    continue;
  }

  $shortsum = 0;

    //gå igjennom alle posisjonene og se om datoen er innenfor rangen.
  for ($z = 0; $z < $posisjonsholdercount; $z++) {

    $start = $posisjonsholder[$z]['ShortingDateStart'];
    $end = $posisjonsholder[$z]['ShortingDateEnd'];

    if ($dateTarget >= $start && $dateTarget <= $end) {
      $shortsum += $posisjonsholder[$z]['ShortPercent'];
    }
  }

  $date = new stdClass();
  $date->date = $dateTarget;
  $date->shortPercent = round($shortsum,3);
  $date->price = '';
  $date->name = $nameTarget;
  $date->ticker = $ticker;
  $ojectholder[] = $date;
}

$ojectholderAll[] = $ojectholder;

}

//finn kursdatoene
$ojectholderAllcount = count($ojectholderAll);
echo 'Antall selskaper ' . $ojectholderAllcount . '<br>';

for ($i = 0; $i < $ojectholderAllcount; $i++) {
  $ticker = $ojectholderAll[$i][0]->ticker;
  $ticker = str_replace(' ', '-', $ticker, $count);
  $pathen = $graphFolder . $ticker . '.json';
  $pathen = strtolower($pathen);

  if (file_exists($pathen)) {

    echo $i . '. ';
    echo "File exist:" . $pathen . '. ';
    
    $stockPrices = file_get_contents($pathen);
    $stockPrices = json_decode($stockPrices, true);

    $dates = array_keys($stockPrices);

    $localobjectcount = count($ojectholderAll[$i]);
    echo '$stockPrices count er: ' . count($stockPrices) . '. Shortdager er ' . $localobjectcount . '.<br>';

    //var_dump($stockPrices);

      //fix indeksen
    $new_stock_array = array();

    foreach ($stockPrices as $key => $stockday) {
      $new_stock_array[key($stockday)] = $stockPrices[$key][key($stockday)];
      unset($stockPrices[$key][key($stockday)]);
    } 

    $stockPrices = $new_stock_array;
    $addcounter = 0;

    for ($x = 0; $x <  $localobjectcount; $x++) {
      $targetDate = $ojectholderAll[$i][$x]->date;

      if (isset($stockPrices[$ojectholderAll[$i][$x]->date])) {
        $ojectholderAll[$i][$x]->price = $stockPrices[$ojectholderAll[$i][$x]->date]['close'];
        $addcounter++;
      }
    }
    echo $addcounter . ' changes made. <br>';
    echo '<br>';

  }
  else {
    echo '<br><br><strong>!!!Market data not available for: '; 
    echo $pathen;
    echo '. continuing!</strong><br><br>';
    continue;
    
  }

  $saveLocation = $savefolder . strtolower($ticker) . '.json';

  //sort out the dates that are already filled
  if (file_exists($saveLocation) and $graphdata_existing = readJSON($saveLocation))
  {

    $graphdata_existing_count = count($graphdata_existing);
    $ojectholderAllCount = count($ojectholderAll[$i]);

      //indeksin for date
    for ($x = 0; $x < $graphdata_existing_count; $x++)
    {
      $graphdata_existing_key[$graphdata_existing[$x]['date']] = $graphdata_existing[$x];
    }


    for ($z = 0; $z < $ojectholderAllCount; $z++)
    {

      $targetDate = $ojectholderAll[$i][$z]->date;
      $found = 0;

      for ($x = 0; $x < $graphdata_existing_count; $x++)
      {

        if (isset($graphdata_existing_key[$targetDate]))
        {
            //check if prize is the same
          if ($graphdata_existing_key[$targetDate]['price'] !=  $ojectholderAll[$i][$z]->price)
          {
            $graphdata_existing_key[$targetDate]['price'] =  $ojectholderAll[$i][$z]->price;
          } 

          $found = 1;
          break;
        }
      }

      //exit();

        //if not found, add position
      if ($found == 0) 
      {
        $graphdata_existing[] = (array)$ojectholderAll[$i][$z];
      }

    }

    usort($graphdata_existing, function ($item1, $item2) {
      return $item1['date'] <=> $item2['date'];
    });


    //slett dager med null i oppføring i shortpercent eller pris
    $count = count($graphdata_existing);


    $graphdata_existing = array_values($graphdata_existing);

    $fp = fopen($saveLocation, 'w');
    //fwrite($fp, json_encode($ojectholderAll[$i], JSON_PRETTY_PRINT));
    fwrite($fp, json_encode($graphdata_existing, JSON_PRETTY_PRINT));


    fclose($fp);
    
  }
  else 
  {

    $count = count($ojectholderAll[$i]);
    $ojectholderAll[$i] = array_values($ojectholderAll[$i]);

    echo '<br> Saving new file ' . $saveLocation . '<br>';
    $fp = fopen($saveLocation, 'w');
    //fwrite($fp, json_encode($ojectholderAll[$i], JSON_PRETTY_PRINT));
    fwrite($fp, json_encode($ojectholderAll[$i], JSON_PRETTY_PRINT));

    $ojectholderAll[$i] = null;

  }

}

$millisecondsts2 = microtime(true);
$milliseconds_diff = ($millisecondsts2 - $millisecondsts1);  
echo 'Runtime in seconds is ' . $milliseconds_diff . '<br>';

?>

