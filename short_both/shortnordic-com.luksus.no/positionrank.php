    <?php include 'header.html'; ?>

<?php
set_time_limit(15);

require_once '../production_europe/logger.php';
require_once '../production_europe/namelink.php';
require_once '../production_europe/functions.php';


if (!function_exists('getbackgroundcolor')) {
    function getbackgroundcolor($inputvalue) {

        if ($inputvalue > 0 ) {
            return 'background-color-green';
        }
        elseif ($inputvalue  == 0) {
            return 'background-color-neutral';   
        }
        elseif ($inputvalue  < 0) {
            return 'background-color-red';          
        }
    }
}


$land = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
}


if ($land == "") {
    return;
}

include '../production_europe/input_check_country.php';
$landname = ucwords($land);
$playerliste = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';

$json = file_get_contents($playerliste);
$json = json_decode($json, true);

//gå igjennom alle posisjoner og legg i box

$jsonCount = count($json);

if ($jsonCount == 0) {
    //echo 'Error! returning. Json count is zero';
    return;
}



$positionBox = [];
$basecurrency = '';

for ($i = 0; $jsonCount > $i; $i++) {   
    //echo 'Round ' . $i . '<br>';
  
    foreach ($json[$i]['positions'] as $key => $position) {
     
         if (isset($position['positionValueChangeSinceStart']) and $position['positionValueChangeSinceStart'] != '' and $position['positionValueChangeSinceStart'] != '-') 
         {
            $tempPos = $position;
         }
         else 
         {
       
            unset($json[$i]['positions'][$key]);
            continue;
         }

     $tempPos['playerName'] = $json[$i]['playerName'];
     $basecurrency = $position['basecurrency'];
     $positionBox[] = $tempPos;

    }

}



$positionBoxCount = count($positionBox);

function sortByOrder($a, $b) {
    return $a['positionValueChangeSinceStartBaseCurrency'] < $b['positionValueChangeSinceStartBaseCurrency'];
}

usort($positionBox, 'sortByOrder');

?>

<?php include 'ads/banner_720.html'; ?>

<div class="container mb-3">
  <div class="row no-gutters"> 
  <div class="col-12 col-lg-10 offset-lg-2">
        <h3 class="">Top earning and losing active positions in 
        <?php echo $landname; ?>
        </h3>
        Ranked by change in <?php echo $basecurrency; ?>. Positions where we could not find start price, are excluded. 
        </div>
    </div>
</div>

<div class="container">
  <div class="row "> 
  <div class="col-12 col-lg-8 offset-lg-2">
    
    <?php 

    for ($i = 0; $positionBoxCount > $i; $i++) {
        $index = $i + 1 ;
    ?>
   
    <div class="row mb-3 playerpositions_ranking_bottom_line">

    <?php 

    $millions = (float)$positionBox[$i]['positionValueChangeSinceStartBaseCurrency'] /1000000;

    
    echo '<div class="w-10 ';
    echo getbackgroundcolor($millions);

    echo ' playerpositions_ranking_index text-center">';
    echo '<span class="h5 text-white">' . $index . '</span>';
    echo '</div>';

    echo '<div class="w-10 ">';
    echo '<div class="px-2">';
    echo '<span data-toggle="tooltip" title="In ' . $basecurrency .   ' "  class="h4">';
    if ($millions > 0) {
        echo '+';
    }   
    echo round($millions,2) . 'M</span>';
    echo '</div>';
    echo '<div class="text-center">';
    echo '(';
    if ($millions > 0) {
        echo '+';
    } 
    
    $totalValueChangePercent = (1 - ($positionBox[$i]['stockPrice']/$positionBox[$i]['startStockPrice']))*100;

    echo round($totalValueChangePercent,2) . '%)';
    $linkplayer = nametolink($positionBox[$i]['playerName']);
    $linkcompany = nametolink($positionBox[$i]['companyName']);

    echo '</div>';
    echo '</div>';
    echo '<div class="col ">';
    echo '<a data-toggle="tooltip" title="See all active positions for player" href="';
    echo 'details.php?player=' . $linkplayer . '&land=' . $land;
    echo '">';
    $name = strtolower($positionBox[$i]['playerName']);
    echo ucwords($name); 
    echo '</a>';

    echo ' in ';
    echo '<a data-toggle="tooltip" title="See all active positions for company" href="';
    echo 'details_company.php?company=' . $linkcompany . '&land=' . $land;
    echo '">';
    echo '<span class="font-weight-bold black-text">' . ucwords($positionBox[$i]['companyName']) . '</span>. ' ;
    echo '</a>';
    echo '<br class="d-none d-md-block">';
    echo 'Startet ' . $positionBox[$i]['positionStartDate'] . ' at price ' . round($positionBox[$i]['startStockPrice'],2);
    echo ' ' . $positionBox[$i]['currency'] ;
    echo ' (Latest price: ' . round($positionBox[$i]['stockPrice'],2) . '). ';
    $bet =  ($positionBox[$i]['startStockPrice'] * $positionBox[$i]['numberOfStocks'])/1000000;
    $bet = round($bet,2);
    echo '<br class="d-none d-md-block">';
    echo 'Position value at start: ' . $bet . ' million ' . $positionBox[$i]['currency'] . '.';  
    echo '</div>'; 
    echo '</div>'; 

     ?>

    <?php 
 
       }

       $description = 'Top earning and losing active positions in ' . $land;
    ?>
  </div>   
  </div> 
  </div>
</div>
<?php $pageTitle = 'Top earning and losing active positions in ' . $landname; ?>;
  <?php include 'footer.php'; ?>
  <?php include 'footer_about.html'; ?>
 </body>