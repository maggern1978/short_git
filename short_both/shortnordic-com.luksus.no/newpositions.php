<?php

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $country = test_input($_GET["country"]);
}

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

$land = $country;

include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';


$positions = '../short_data/datacalc/newpositions/' . $land . '/newpositions_' . $land . '.json';

require '../production_europe/namelink.php';
require '../production_europe/functions.php';

$land = $country;
$pageTitle = 'Newest shorted companies in ' . ucwords($country);

$json = readJSON($positions);

$count = count($json);

$earliestDate = $json[$count-1]['firstZeroDate'];

?>


<?php include 'header.html'; ?>
<?php include 'ads/banner_720.html'; ?>

<div class="container">
  <div class="row ">
<div class="col-12">
<h3>Newest shorted companies in <?php echo ucwords($land); ?></h3> 
<p>List of companies which has gone from a zero percent short to a positive short. </p>
<table class="table table-bordered table-hover table-sm">
    <thead class="thead-dark">
      <tr>
        <th>#</th>
        <th>Company</th>
        <th class="text-right">Short started</th>
        <th class="text-right">Date</th>
        <th class="text-right" data-toggle="tooltip" title="How many days the company had a 0 percent short before the shorting started again">Without short for</th>
      </tr>
    </thead>
    <tbody>

    <?php $counter = 0;
    foreach ($json  as $key => $company) {
    $counter++;

    $companyx =  nametolink($company['name']);
    echo '<tr>';
    echo '<td>' . $counter . '.</td>';
    echo '<td>' . '<a href="details_company.php?company=' . $companyx . '&land=' . $land . '">';
    

    $name   = strtolower($company['name']) ;
    $name   = ucwords($name);

    echo  $name . '</a>' . '</td>';

    if ($company['firstZeroDate'] != '-') {
      echo '<td class="text-right" style="min-width: 83px;">' . number_format(daysBetween(date("Y-m-d"), $company['firstZeroDate']), 0, ".",",") . ' days ago';
      echo '</td>';    
      echo '<td class="text-right" style="min-width: 90px;">' . $company['firstZeroDate'];

   
      echo '</td>';
      echo '<td class="text-right">';
      if ($company['daysWithZero'] == 1) {
        echo $company['daysWithZero'] . ' day';
      }
      else {
        echo $company['daysWithZero'] . ' days';
      }
      echo '</td>';
      echo '<tr>';
      }
    else {
      echo '<td class="text-right" style="min-width: 83px;">' . 'N/A*' . '';    
      echo '</td>';    
      echo '<td class="text-right" style="min-width: 90px;">-';


      echo '</td>';
      echo '<td class="text-right">-';
      echo '</td>';
      echo '<tr>';
    }

}
function daysBetween($dt1, $dt2) {
    return date_diff(
        date_create($dt2),  
        date_create($dt1)
    )->format('%a');
}

?>
 </tbody>
  </table>
  <p>*Longer than available data.</p>
       <?php
           echo "Updated: " . date("Y-m-d H:i",filemtime($positions));
        ?>
</div>
</div>
<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>

 </main>
  </body>
</html>