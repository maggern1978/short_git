<?php 

//THIS IS A COPY DO DETAILS_COMPANY.PHP

include 'header.html'; 
$company  = $land = "";

include_once '../production_europe/namelink.php';
include_once '../production_europe/functions.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") 
{
  if (!isset($_GET["land"])) 
  {
    if (!isset($_GET['country']))
    {
      return;
    }
    else
    {
      $country_code = test_input($_GET["country"]);
    }
  }
  else
  {
   $land = test_input($_GET["land"]);
 }
 
 if (isset($_GET["company"])) 
 {
  $singleCompany = test_input($_GET["company"]);
  $singleCompany = linktoname($singleCompany);
}
else
{
 $singleCompany = '';
}
if (isset($_GET["isin"])) 
{
  $isin = test_input($_GET["isin"]); 
}  
else
{
  $isin = '';
}
}


if (isset($country_code))
{
  switch ($country_code) {
    case "NO":
    $land = 'norway';
    break;
    case "SE":
    $land = 'sweden';
    break;
    case "DK":
    $land = 'denmark';
    break;
    case "DE":
    $land = 'germany';
    break;
    case "SP":
    $land = 'spain';
    break;
    case "IT":
    $land = 'italy';
    break;
    case "UK":
    $land = 'united_kingdom';
    break;
    case "FR":
    $land = 'france';
    break; 
    case "FI":
    $land = 'finland';
    break;                       
    default:
    return;
  }
}



$is_isin = is_isin($isin);

$landNavn = ucwords($land);
include_once '../production_europe/input_check_country.php';
include_once '../production_europe/options_set_currency_by_land.php';

?>

<style type="text/css">

  .sticky {
    height: 280px;
    width: 100%;
    background-color: yellow;
    text-align: center;
    top: 0;
    z-index: 999999;
  }

</style>
<!--
<div id="stickycontainer" class="container">
  <div id="stickydiv" class="sticky">Sticky</div>
  <div id="stickycompensator" ></div>
</div>
-->

<?php //include 'ads/banner_720.html'; //tas bare med hvis det er hits, så ligger i detaljer_selskap_body.php ?>
<?php include '../production_europe/detaljer_selskap_body.php'; ?>

<div class="container">
  <div class="row">
    <div class="col-12">
      <?php include '../production_europe/company_graph.php'; ?>
    </div>
  </div>
</div>
</div>
<br>

<?php 
if (isset($tickertradingview))
{
  if ($land != 'france' and $land != 'italy' and $land != 'spain' and $land != 'united_kingdom' )
  {
    include '../production_europe/company_info.php'; 
  }
}

?>

<div class="container">
  <div class="row">
    <div class="col-12">
      <div id='news'> </div>
    </div>
  </div>
</div>
<br>


<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>
<?php
if (!isset($ticker)) {
  return;
}
?>

<?php 

  //removing single letters from google news search
$lastlocationofspace = strripos($selskapsnavn, ' ', 0);
$stringlength = strlen($selskapsnavn);

if ($lastlocationofspace == $stringlength-2)
{
 $selskapsnavn = substr($selskapsnavn, 0, $lastlocationofspace) ;
}

$selskapsnavnNew = str_replace(" ", "%20", $selskapsnavn); 
$selskapsnavnNew = '"' . $selskapsnavnNew . '"' . '%20stock';

?>

<script type="text/javascript">

  var holder = [];
  var xml;
  var noe = [];

  <?php $targetUrl = "{ url: 'https://news.google.com/rss/search?q=" . $selskapsnavnNew . "+when:180d&hl=en-US&gl=US&ceid=US:en' }"; ?>

  var data = <?php echo $targetUrl; ?> 

  $.ajax({
    url: "proxy.php",
    async: true,
    type: "POST",
    dataType: "json",
    data: data,
    success: function (result) {
        //console.log(result);
        //var holder = result;
        xml = parseXml(result);
        //noe[0] = result; 
        
        //console.log(xml);
        //console.log(xml[0]['title']);

        xml.sort(function(a, b) {
          a = new Date(a.pubDate);
          b = new Date(b.pubDate);
          return a>b ? -1 : a<b ? 1 : 0;
        });

        length = xml.length;

        document.getElementById("news").innerHTML += '<div class="my-2"><span class="h2">News for <?php echo $selskapsnavn; ?></span><span class="p pt-2 float-right">Source: Google</span>';

        for (i = 0; i < length; i++) {

          if (i > 20) {
            //console.log(i);
            break;
          }

          date = formatDate(xml[i]['pubDate']);
          document.getElementById("news").innerHTML += '<div class="mb-1 d-flex"><div class="mr-2 news-date"><span class="font-weight-bold">' + date + ' </span></div><div><a href="' + xml[i]['link']  + '">' + xml[i]['title'] + '</a></div></div>';
        }

      },

    });

  function formatDate(date) {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
      month = '0' + month;
    if (day.length < 2) 
      day = '0' + day;

    return [year, month, day].join('-');
  }

  function getNode(node, tagToRetrieve) {
    var htmlData = node.getElementsByTagName(tagToRetrieve)[0].innerHTML;
  return unescape(htmlData); // decode HTML entities, see lodash/underscore
}

function parseXml(xmlStr) {

  parser = new DOMParser();
  xmlDoc = parser.parseFromString(xmlStr,"text/xml");
  
  var items = Array.from(xmlDoc.getElementsByTagName('item'));

  var feeds = [];
  items.forEach(function (item) {
    feeds.push({
      title: getNode(item, 'title'),
      link: getNode(item, 'link'),
      pubDate: getNode(item, 'pubDate'),
      description: getNode(item, 'description'),
    });
  });
  return feeds;
}
</script>

</body>