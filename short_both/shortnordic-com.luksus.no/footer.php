  <!-- Core -->
  <script src="./js/jquery.min.js"></script>
  <script src="./assets/vendor/popper/popper.min.js"></script>
  <script src="./assets/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="./assets/vendor/headroom/headroom.min.js"></script>
  <script src="./assets/vendor/headroom/jQuery.headroom.js"></script>

  <!-- Optional plugins -->

<script type="text/javascript">
  $(".headroom").headroom();
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   

    <?php 
    if (isset($pageTitle)) {
      echo 'document.title = "' . $pageTitle . '"; ';
      }
    if (isset($keywords)) {
      echo 'document.getElementsByName("keywords")[0].content = "' . $keywords . '";';
    }
    if (isset($description)) {
      echo 'document.getElementsByName("description")[0].setAttribute("content", "' . $description . '")';
    } 
    ?>
   
});

$(document).click(function(e){ 
  var elem = $(e.target).attr('id'); 
  if(elem !== 'livesearch' && elem !== 'searchform')
    { $('#livesearch').fadeOut('fast'); } })

$(document).click(function(e){ 
  var elem = $(e.target).attr('id'); 
  if(elem == 'livesearch' || elem == 'searchform')
    { $('#livesearch').fadeIn('fast'); } })


</script>
<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
//https://github.com/osano/cookieconsent/
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#eaf7f7",
      "text": "#5c7291"
    },
    "button": {
      "background": "#56cbdb",
      "text": "#ffffff"
    }
  },
  "content": {
    "href": "https://Shortnordic.com/cookiepolicy.php"
  }
});
</script>
<br>
<footer>
    <p><span class="font-weight-bold"> © <?php echo date("Y"); ?> <a style="text-decoration:none;" href="about.php">Shortnordic.com </a> </span>- a free tool for investors and journalists
    </p>

