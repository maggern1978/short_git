<?php


if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$subsector = test_input($_GET["subsector"]);
	$country = test_input($_GET["country"]);
}
else
{
	return;
}

require '../production_europe/functions.php';
require '../production_europe/namelink.php';

//funksjon for å rense data
function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

$subsector = linktoname($subsector);

$csv = readCSV('../short_data/dataraw/isin/hovedlisten_landkoder.csv');
$count = count($csv);

?>

<?php include 'header.html'; ?>
<?php include 'ads/banner_720.html'; ?>

<div class="container">
	<div class="row ">
		<div class="col-12">
		<h1>Subsector <?php echo '"' . $subsector . '"' . ' for ' . ucwords($country); ?> </h1>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Navn</th>
						<th>Ticker</th>
						<th>Sector</th>
						<th>Subsector</th>
						<th>ISIN</th>
					</tr>
				</thead>
				<tbody>
					
						<?php

						for ($i = 1; $i < $count; $i++)
						{
							//var_dump($csv[$i]);

							if (mb_strtolower($csv[$i][5]) == mb_strtolower($subsector) and mb_strtolower($country) == mb_strtolower($csv[$i][6]))
							{
								echo '<tr>';
								echo '<td>';
								echo $csv[$i][2];
								echo '</td>';
								echo '<td>';
								echo $csv[$i][1];
								echo '</td>';
								echo '<td>';
								echo $csv[$i][4];
								echo '</td>';
								echo '<td>';
								echo $csv[$i][5];
								echo '</td>';
								echo '<td>';
								echo $csv[$i][0];
								echo '</td>';
								echo '<tr>';
							}
						}

						?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>
<?php //var_dump($csv[1]);?>
 </main>
  </body>
</html>