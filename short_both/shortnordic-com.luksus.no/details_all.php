
    <?php include 'header.html'; 

//$playernavn = $fil_teller = $i = "";
$land = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["land"]);
}


//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';


$landNavn = ucwords($land);
$url = '../short_data/html/detaljer_alle_' . $land . '.html';


$pageTitle = 'All active positions by player' . ' in ' . $landNavn;
$description = 'See updated list of all short positions currently held in ' . $landNavn . ' and the latest price and value change.'; 


?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h2>All active positions in <?php echo $landNavn;  ?>, by player<a href="details_company_all.php?&land=<?php echo $land;?>" class="btn btn-info text-right float-right" role="button">Sorted on companies</a></h2>
      

<?php include $url; ?>

  <?php include 'footer.php'; ?>
  <?php include 'footer_about.html'; ?>
   </main>
 </body>