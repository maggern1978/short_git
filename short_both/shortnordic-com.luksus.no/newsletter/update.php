<?php

$error_message = 'Settings could not be updated. Please try again or contact support.';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (!isset($_GET["code"]))
  {
  	echo $error_message;
  	return;
  }

  if (!isset($_GET["secret"]))
  {
  	echo $error_message;
  	return;
  }

  if (!isset($_GET["newsletters"]))
  {
    echo $error_message;
    return;
  }

  $code = test_input($_GET["code"]);
  $secret = test_input($_GET["secret"]);
  $newsletters_user = test_input($_GET["newsletters"]);
  
}

$newsletters_user = (explode(",",$newsletters_user));

//var_dump($newsletters_user);

include '../../production_europe/functions.php';

if (!file_exists('../../newsletter/users/confirmed/' . $code))
{
	echo 'Error. Cannot access settings. Please contact us.';
	return;
}

//read settings
if (!$data = readJSON('../../newsletter/users/confirmed/' . $code .'/' . $code . '.json'))
{
  echo 'Error. Cannot access settings. Please contact us.';
  return;
}

if ($data['secret'] != $secret)
{
  echo 'Error. Cannot access settings. Please contact us.';
  return;
}

$newsletters_index = readJSON('../../newsletter/newsletter_settings/newsletter_index.json');

$new_subscriptions = [];

foreach ($newsletters_index as $newsletter)
{

  foreach ($newsletters_user as $user)
  {
    if ($newsletter['name'] == $user)
    {
      $new_subscriptions[] = $user;
    }
  }

}

$data['subscriptions'] = $new_subscriptions;

saveJSONsilent($data, '../../newsletter/users/confirmed/' . $code .'/' . $code . '.json');

echo 'Settings updated.<br><br><span class="font-weight-bold mt-2">Your subscriptions are now: </span><br>';

if (count($new_subscriptions) > 0)
{

  foreach ($new_subscriptions as $line)
  {
    echo ucwords($line) . '<br>';
  }

}
else
{
  echo 'None';
}

//funksjon for å rense data
function test_input($data) 
{
  $data = filter_var($data, FILTER_SANITIZE_STRING);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


?>