<?php

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if (!function_exists('readJSON')) 
{
function readJSON($file_location_and_name) 
{

    if (file_exists($file_location_and_name))
    {

        if ($data = file_get_contents($file_location_and_name))
        {

            if ($data = json_decode($data, true))
            {
                return $data;
            }

        }
        
    }

    return false;
}
}

if (!function_exists('check_hold_until')) 
{
    function check_hold_until($newsletter_name, $timetable) 
    {
    
        foreach ($timetable as $subscription)
        {

            if ($newsletter_name == $subscription['name'])
            {

                $time_now = date('H.i');
                $time_now = (float)$time_now;

                if ($time_now > $subscription['hold_until'])
                {
                    return true; 
                }

                break;

            }

        }

        return false;

    }

}


error_reporting(E_STRICT | E_ALL);
date_default_timezone_set('Etc/UTC');

//Load Composer's autoloader
require 'vendor/autoload.php';
require '../production_europe/logger_priority.php';

//Passing `true` enables PHPMailer exceptions
$mail = new PHPMailer(true);

//Server settings
//$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
$mail->isSMTP();                                            //Send using SMTP
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
$mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
$mail->SMTPAuth   = true;                                   //Enable SMTP authentication
$mail->CharSet = "UTF-8";

//Same body for all messages, so set this before the sending loop
//If you generate a different body for each recipient (e.g. you're using a templating system),
//set it inside the loop

//prepopulate the main newsletters
$prepoulated_newsletters = [];

//shortnordic
try 
{
    ob_start();      

    if (!include('../newsletter/newsletter_generator_shortnordic.php'))
    {
        throw new Exception("ERROR:");       
    }

    $output = ob_get_clean(); //Get current buffer contents and delete current output buffer

    if (!stripos($output, 'error') and stripos($output, 'Finland') and stripos($output, 'Norway') and stripos($output, 'Sweden') and stripos($output, 'Denmark'))
    {
        $prepoulated_newsletters['shortnordic'] = $output;

    }
    else
    {
        throw new Exception("Error with shortnordic email:");
    }
   
} 

catch (Exception $e) 
{
    echo 'Read error of shortnordic php file or error found, will skip. <br>';
    file_put_contents('shortnordic_error_file.html', $output);
   
}

//shorteurope
try 
{
    ob_start();      

    if (!include('../newsletter/newsletter_generator_shorteurope.php'))
    {
        throw new Exception("ERROR:");       
    }

    $output = ob_get_clean(); //Get current buffer contents and delete current output buffer

    if (!stripos($output, 'error') and stripos($output, 'finland') and stripos($output, 'norway') and stripos($output, 'sweden') and stripos($output, 'denmark') and stripos($output, 'france') and stripos($output, 'germany') and stripos($output, 'italy') and stripos($output, 'spain') and stripos($output, 'united_kingdom'))
    {
        $prepoulated_newsletters['shorteurope'] = $output;
    }
    else
    {
        throw new Exception("ERROR:");
    }
   
} 

catch (Exception $e) 
{
    echo 'Read error of shortnordic php file or error found, will skip. <br>';
   
}

//read folder list
$user_folder = 'users/confirmed/';
if (!$user_directories = array_diff(scandir($user_folder), array('.', '..')))
{
    echo 'Error reading directory users/confirmed/...<br>';
}

$user_directories  = array_values($user_directories);

//read newsletter_index to get timetable

if (!$timetable = readJSON('newsletter_settings/newsletter_index.json'))
{
    echo 'Error reading newsletter_index.json...<br>';
}

//process folders for user info
foreach ($user_directories as $key => $user)
{

    //first, read the use file
    try 
    {
        echo $key . '. User: ' . $user . ' ';

        $filename = $user_folder . $user . '/' . $user . '.json';

        if (!$user_data = readJSON($filename))
        {

            throw new Exception("ERROR:");
        }

        //var_dump($user_data);
    }
        
    catch (Exception $e) 
    {
        echo 'ERROR READING USER JSON FILE ' . $filename . '<br>';
        continue;
    }

    //check subscriptions 
    if (empty($user_data['subscriptions']))
    {
        echo $key . '. ' . ' User ' . $user_data['email'] . ' has no subscriptions, skipping....<br>';
        continue; 
    }

    //second read log and make sure that emails are not sent

    //read log file
    $log_filename_user = $user_folder . $user . '/' . date('Y-m-d') . '.json';

    try 
    {
        if (file_exists($log_filename_user))
        {
            if (!$log_data = readJSON($log_filename_user))
            {
                 throw new Exception("ERROR:");  
            }
        }
        else
        {
            $log_data = [];
            
        }
    } 

    catch (Exception $e) 
    {
        echo 'Read error of user json file, will skip. <br>';
        continue;
    }

    $already_sent_log = [];

    foreach ($log_data as $entry)
    {
        foreach ($entry['origin'] as $already_sent_newsletter)
        {
            $already_sent_log[] = $already_sent_newsletter;
        }
        
    }

    $already_sent_log = array_unique($already_sent_log);
    $already_sent_log = array_values($already_sent_log);

    $subscription_send_list = $user_data['subscriptions'];

    foreach ($subscription_send_list as $index => $current_subscription)
    {

        foreach ($already_sent_log as $already_sent_subscription)
        {
            if (mb_strtolower($already_sent_subscription) == mb_strtolower($current_subscription))
            {
                unset ($subscription_send_list[$index]);
            }

        }

    }

    //var_dump($subscription_send_list);
 
    if (empty($subscription_send_list))
    {
        echo 'All subscriptions: ' . implode(" | ", $subscription_send_list) . ' -> already sent, skipping...<br>';
    }
    else
    {
        echo $user_data['email'] . ': Send list: ' . implode(" | ", $subscription_send_list) . '<br>';
    }
   
    //generate all subscription texts
    $generated_subscriptions = [];
    $country_list = ['germany','united_kingdom','france', 'italy', 'spain', 'norway', 'sweden', 'denmark', 'finland'];

    foreach ($subscription_send_list as $key => $subscription) //first 
    {
        
        //shortnordic
        if (mb_strtolower($subscription) == 'shortnordic')
        {
            try 
            {
                if (!isset($prepoulated_newsletters['shortnordic']))
                {
                    throw new Exception('ERROR, shortnordic does not exist in $prepoulated_newsletters! <br>');     
                }

                $obj = new stdClass;
                $obj->subject = 'ShortNordic.com daily newsletter'; //depends on source!
                $obj->body = $prepoulated_newsletters['shortnordic'];
                $obj->origin[] = 'shortnordic';

                if (check_hold_until('shortnordic', $timetable))
                {
                    $generated_subscriptions[] = (array)$obj;
                }
                else
                {
                    echo 'shortnordic outside timetable, will skip... ' . "<br>";
                }

                unset($subscription_send_list[$key]);

                continue; 
            } 

            catch (Exception $e) 
            {
                echo 'Error while processin shornordic newsletter in subscription send list. Will continue; <br>';
                unset($subscription_send_list[$key]);
                continue;
            }

        }

        //shortuerope
        if (mb_strtolower($subscription) == 'shorteurope')
        {
            try 
            {
                if (!isset($prepoulated_newsletters['shorteurope']))
                {
                    throw new Exception('ERROR, shorteurope does not exist in $prepoulated_newsletters! <br>'); 
                    continue;          
                }

                $obj = new stdClass;
                $obj->subject = $obj->subject = 'ShortEurope.com daily newsletter'; //depends on source!
                $obj->body = $prepoulated_newsletters['shorteurope'];
                $obj->origin[] = 'shorteurope';

                if (check_hold_until('shorteurope', $timetable))
                {
                    $generated_subscriptions[] = (array)$obj;
                }
                else
                {
                    echo 'shorteurope outside timetable, will skip... ' . "<br>";
                }

                unset($subscription_send_list[$key]);
                continue; 
            } 

            catch (Exception $e) 
            {
                echo 'Error while processin shorteurope newsletter in subscription send list. <br>';
                unset($subscription_send_list[$key]);
                continue;
            }

        }
       
    }

    if (!empty($subscription_send_list))
    {
        'Echo adding in single countries. <br>';

            try 
            {
                $countries = [];

                foreach ($subscription_send_list as $country)
                {
                     
                    foreach ($country_list as $original_country)
                    {
                        if ($country == $original_country)
                        {
                            $countries[] = $country;
                            break;
                        }

                    }
                    
                }
                $countries_backup =  $countries;

                ob_start();      

                $mode = 'single'; //to get only seperate countries

                if (!include('../newsletter/newsletter_generator_shortnordic.php'))
                {
                    throw new Exception("ERROR:");       
                }

                $output = ob_get_clean(); //Get current buffer contents and delete current output buffer

                if (stripos($output, 'error') or stripos($output, 'warning'))
                {
                    echo 'Error or warning found in text!<br>';
                    var_dump($output);
                    throw new Exception("ERROR:");
                }
              

                $obj = new stdClass;
                $obj->subject = $obj->subject = 'ShortEurope.com daily newsletter'; //depends on source!
                $obj->body = $output;
                $obj->origin = $countries_backup;

                if (check_hold_until('norway', $timetable))
                {
                    $generated_subscriptions[] = (array)$obj;
                }
                else
                {
                    echo 'Norway outside timetable, will skip... ' . "<br>";
                }
              
            } 

            catch (Exception $e) 
            {
                echo 'Error while processing single countries newsletter in subscription send list. Error or warning found in email text or shortnordic generator has error. <br>';
                continue;
            }        

    }

    //var_dump($generated_subscriptions);
    //var_dump($generated_subscriptions);

    //set sender info based on source
    if (!empty($user_data['source']))
    {

        if (stripos($user_data['source'], 'shortnordic.com') === false and stripos($user_data['source'], 'shorteurope.com') === false) //must be one of two valid sources
        {
            $source = 'shorteurope.com';  
        }
        else
        {
            $source = $user_data['source']; 
        }

    }
    else
    {
        echo 'Empty user source! Will set default! <br><br>';
        $source = 'shorteurope.com';  
    }
    
    $mail->Host       = 'mail.' . $source;                     //Set the SMTP server to send through
    $mail->Username   = 'no-reply@'  . $source;                      //SMTP username
    $mail->Password   = 'Reginais21';                               //SMTP password

    foreach ($generated_subscriptions as $subscription)
    {

    //second, make sure that you can generate email
    //genereate right html 

    $mail->setFrom('no-reply@' . $source, ucwords($user_data['source']));

    $body = $subscription['body'];
    $body .='<div style="margin-top: 1.5rem; text-align: center; color: gray;">';
    $body .='<a href="https://shorteurope.com/newsletter/settings.php?code='. $user_data['hash'] . '&secret=' . $user_data['secret'] .  '" style="text-decoration: none;"><span style="color: gray">Use this link to edit your subscription settings.</span></a>';
    $body .='</div>';
    $body .='</div>';
    $body .='</div>';

    $mail->Subject = $subscription['subject'];

    $mail->msgHTML($body);
    
    //msgHTML also sets AltBody, but if you want a custom one, set it afterwards
    //$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

    try 
    {
        $mail->addAddress($user_data['email']);
    } 
    catch (Exception $e) 
    {
        echo 'Invalid address skipped: ' . htmlspecialchars($user_data['email']) . '<br>';
        continue;
    }

    try 
    {
        $mail->send();
        echo 'Message sent to: ' . htmlspecialchars($user_data['email']) . ' (' .
        htmlspecialchars($user_data['email']) . ') Subject: ' . $subscription['subject'] . "\n";

        //Mark  EMAIL as sent in the DB
        $log = readJSON($log_filename_user);
        $log[] = $subscription;
        saveJSON($log, $log_filename_user);

    } 

    catch (Exception $e) 
    {
        echo 'Mailer Error (' . htmlspecialchars($user_data['email']) . ') ' . $mail->ErrorInfo . '<br>';
        //Reset the connection to abort sending this message
        //The loop will continue trying to send to the rest of the list
        $mail->getSMTPInstance()->reset();
    }
    //Clear all addresses and attachments for the next iteration
    $mail->clearAddresses();
    $mail->clearAttachments();

    }
}




