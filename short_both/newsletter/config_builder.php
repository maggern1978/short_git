<?php

include '../production_europe/functions.php';

$array = [];
$obj = new stdClass;
$obj->name = 'shortnordic';
$obj->description = 'Daily position changes (Sweden, Denmark, Norway, Finland)';
$obj->type = 'single';
$obj->hold_until = (float)15.40;

$array[] = (array)$obj;

$obj->name = 'shorteurope';
$obj->description = 'Daily position changes (Nordic countries + Germany, France, UK, Italy and Spain)';
$obj->type = 'single';
$obj->hold_until = (float)16.00;

$array[] = (array)$obj;

$countries = get_countries();

foreach ($countries as $country)
{
	$obj = new stdClass;
	$obj->name = $country;
	$obj->description = 'Daily short position changes.' ;
	$obj->type = 'multi';
	$obj->hold_until = (float)16.00;

	$array[] = (array)$obj;
}

saveJSON($array, 'newsletter_settings/newsletter_index.json');


?>