<?php

switch ($land) {
    case "norway":
       
        $selskapsliste = 'ssr.finanstilsynet/finanstilsynet_current.json';
        $currency_ticker = 'NOK';
        $land_navn = 'norway';
        $path = 'datacalc/nokkeltall/nokkeltall_nor.current.csv';
        $countryText = 'The Norwegian market';
        $tooltip = 'The Oslo Stock Exchange';
        $image = 'banner-norge';
        $tooltip_earnings = 'Earnings in million NOK';
        break;
    case "sweden":
        
        $selskapsliste = 'fi.se/fi.se_current.json';
        $currency_ticker = 'SEK';
        $land_navn = 'sweden';
        $path = 'datacalc/nokkeltall/nokkeltall_swe.current.csv';
        $countryText = 'The Swedish market';
        $tooltip = 'Nasdaq Stockholm';
        $image = 'banner-sverige';
        $tooltip_earnings = 'Earnings in million SEK';
        break;
    case "denmark":
        
        $selskapsliste = 'denmark/denmark_current.json';
        $currency_ticker = 'DKK';
        $land_navn = 'denmark';
        $path = 'datacalc/nokkeltall/nokkeltall_dkk.current.csv';
        $countryText = 'The Danish market';
        $tooltip = 'Nasdaq Copenhagen';
        $image = 'banner-denmark';
        $tooltip_earnings = 'Earnings in million DKK';
        break;
    case "finland":
        $land = "fin";
        break;
    case "iceland":
        //echo "is";
        break;
    default:

}

 if (!function_exists('getNumbers')) {
function getNumbers($filenameAndUrl) {

    $csvFile = file($filenameAndUrl);            
        $data = [];
        foreach ($csvFile as $line) 
{            $data[] = str_getcsv($line);
        } 
        return $data;
    }
}

$data = getNumbers($path);
//var_dump($data);
$plus = '';
if ($data[1][0] > 0) {
  $plus = '+';
}
ob_start();
?>
<div class="container">
  <div class="row banner-padding">
    <div class="col-md-2 <?php echo $image; ?>">
      <span class="text-white"></span>
    </div>
    <div class="col-8 col-md-7 banner-norge-title banner-background-<?php echo $land_navn; ?>" data-toggle="tooltip" title="<?php echo $tooltip ;?>">
      <span class="text-white "><?php echo $countryText; ?></span>
    </div>
    <div class="col-4 col-md-3 text-right banner-norge-percent banner-background-<?php echo $land_navn; ?> 
    " data-toggle="tooltip" title="<?php echo $tooltip_earnings; ?>"">
      <span class="text-white"><?php echo $plus . number_format($data[1][0],1); ?>M</span>
    </div>
  </div>
</div> 
<?php 
  $htmlStr = ob_get_contents();
  // Clean (erase) the output buffer and turn off output buffering
  ob_end_clean(); 

  $destination = 'country_banner_' . $land . '.html';
  // Write final string to file
  file_put_contents($destination, $htmlStr);


?>