
<?php include 'header.html'; ?>
<?php 

$country = $type = "";

require '../production_europe/namelink.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
  $type = test_input($_GET["type"]);
}

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';


$land_navn = ucwords($land);
$title = ' by value change last trading day ' . $land_navn;

$saveUrlWinner = '../short_data/datacalc/toplist/' . $land . '/winner/winner.' . $land . '.current.json';
$saveUrlLoser = '../short_data/datacalc/toplist/' . $land . '/loser/loser.' . $land . '.current.json';

//Last inn vinner
if ($type == 'winner') {
  $string = file_get_contents($saveUrlWinner);
  $data = json_decode($string, true);    
  $startord = 'Earned';
  $title = 'Winners' . $title;
  $pageTitle = 'Biggest winners' . ' - ' . $land_navn;
  $description = 'Biggest winners' . ' in ' . $land_navn . ' last trading day.';
  $descriptionEnd = 'earned';
  $playerliste = $saveUrlWinner;
}

if ($type == 'loser') {
  $string = file_get_contents($saveUrlLoser);
  $data = json_decode($string, true);
  $startord = 'Lost';
  $title = 'Losers' . $title;
  $pageTitle = 'Biggest losers' . ' - ' . $land_navn;
  $description = 'Biggest losers' . ' in ' . $land_navn . ' last trading day.';
  $descriptionEnd = 'lost';
  $playerliste = $saveUrlLoser;
}
//var_dump($data);
?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row ">
    <div class="col-12">
      <h2 class="pb-2"><?php echo $title . ' in ' . $currency_ticker ; ?> </h2> 
      <table class="table table-bordered table-striped table-hover table-sm">
          <thead class="thead-dark">
            <tr>
              <th># </th>
              <th>Name</th>
              <th class="text-right" style="width:110px;"># of positions</th>
              <th class="text-right" style="width:110px;">Value change</th>
          </tr>
      </thead>
      <tbody>
<?php
$total = 0;
$totalPositions = 0;

$index = 0;
foreach ($data as $key => $player) { ?>

           <tr>
           <td>
           <?php 
           $index++;
           echo $index . '.'; ?>
           </td>
           <td>
            
            <?php
            $playerx = nametolink($player['playerName']);
            echo '<span>';
            echo '<a href="details.php?player=' . $playerx  . '&land=' . $land . '">';
            
            $player['playerName'] = strtolower($player['playerName']) ;
            $player['playerName']  = ucwords($player['playerName'] );
            echo  $player['playerName']   . '</a>';
            echo '</span>';
            ?>
            </td>
            <td  class="text-right">
      
           <?php
           echo $player['numberOfPositions']; 
           $totalPositions += $player['numberOfPositions'];

           echo '</td><td  class="text-right">';
           $temp = ($player['change']);
           $total += $temp;
           echo number_format($temp,2,".",",") . ' M'; 
      }
           ?>  
           </td></tr>
           <tr class="font-weight-bold">
             <td></td>
             <td>Sum:</td>
             <td class="text-right"><?php echo $totalPositions; ?></td>
             <td class="text-right"><?php echo round($total,2) . ' M'; ?> </td>
           </tr>
           </tbody>
           </table>
       <?php
           echo "Updated: " . date("Y-m-d H:i",filemtime($playerliste));

           if (abs($total) > 0) {
            $description = $description . ' ' . $index . ' players ' . $descriptionEnd . ' ' . abs(round($total,2)) .  ' million ' . $currency_ticker . ' holding ' . $totalPositions . ' positions in total. See the full list.' ;
            }
        ?>

</div>
</div>
<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>

 </main>
  </body>
</html>