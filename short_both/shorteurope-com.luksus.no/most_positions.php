
<?php include 'header.html'; 

$country = $type = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
}

require '../production_europe/namelink.php';

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

//$land = 'Sverige';
//$land = 'norway';


include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';

$date = date('Y-m-d');
$land_navn = ucwords($land);
$playerliste = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
$pageTitle = 'Most short positions' . ' in ' . ucwords($land_navn);

//Last inn nyeste shorttabell 
$string = file_get_contents($playerliste);
$string = json_decode($string, true);		

//var_dump($playerliste);

$companyArray = array();
$positionCountArray = array();

foreach ($string as $key => $player) {
	$companyArray[] = $player['playerName'];
	$counter = 0;
	foreach ($player['positions'] as $index => $position) {
		$counter++;
	}
	$positionCountArray[] = $counter;
}

array_multisort($positionCountArray, SORT_DESC,
	$companyArray);

  ?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row ">
<div class="col-12">
<h3>Most positions <?php echo ucwords($land_navn); ?></h3> 
<table class="table table-bordered table-hover table-sm">
    <thead class="thead-dark">
      <tr>
        <th>#</th>
        <th>Player name</th>
        <th class="text-right">Positions</th>
      </tr>
    </thead>
    <tbody>
   

    <?php $counter = 0;
    $descriptionTemp = '';
	foreach ($companyArray as $key => $shorter) {
	$counter++;



	$shorterx = nametolink($shorter);
	echo '<tr>';
	echo '<td>' . $counter . '.</td>';
	echo '<td>' . '<a href="details.php?player=' . $shorterx . '&land=' . $land . '">';
	

	$shorter   = strtolower($shorter) ;
	$shorter   = ucwords($shorter);

  if ($counter < 3) {
    $descriptionTemp = $descriptionTemp . $shorter . ', ';
  }
  if ($counter == 3) {
    $descriptionTemp = $descriptionTemp . 'and ' . $shorter . '';
  }

	echo  $shorter . '</a>' . '</td>';
	echo '<td class="text-right">' . $positionCountArray[$key] . '</td>';
	echo '<tr>';


}
$description = 'Se list of players with most number of positions in ' . $land_navn . ' (' . $counter . ' positions in total.)'; 
$description = $description  . ' Top three are ' . $descriptionTemp . '.';

?>
 </tbody>
  </table>
       <?php
           echo "Updated: " . date("Y-m-d H:i",filemtime($playerliste));
        ?>
</div>
</div>
<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>

 </main>
  </body>
</html>