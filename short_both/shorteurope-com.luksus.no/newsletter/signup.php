<!DOCTYPE html>
<html lang="en">
<head>
	<title>Newsletter signup</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<?php

if ($_SERVER["REQUEST_METHOD"] == "GET") 
{
  if (isset($_GET["source"]))
  {
  	$source = test_input($_GET["source"]);
  }
  else
  {
  	$source = 'shorteurope.com';
  }
}

//funksjon for å rense data
function test_input($data) 
{
  $data = filter_var($data, FILTER_SANITIZE_STRING);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


?>

<style>
	/* Style the form element with a border around it */
	form {
		border: 4px solid #f1f1f1;
	}

	/* Add some padding and a grey background color to containers */
	.container {
		padding: 20px;
		padding-bottom: 5px;
		background-color: #f1f1f1;
	}

	/* Style the input elements and the submit button */
	input[type=text], input[type=submit] {
		width: 100%;
		padding: 12px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		box-sizing: border-box;
	}

	/* Add margins to the checkbox */
	input[type=checkbox] {
		margin-top: 16px;
	}


</style>

<script type="text/javascript">
	function ValidateEmail(inputText)
	{
		
		var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		if (inputText.value.match(mailformat))
		{
			alert("Valid email address!");
			//document.form1.text1.focus();
			return true;
		}
		else
		{
			alert("You have entered an invalid email address!");
			//document.form1.text1.focus();
			return false;
		}
	}

</script>
<body>
	<div class="container">
		<form action="verifier.php">
			<div class="container">
				<h2>Subscribe to our newsletter</h2>
				<p>We'll send daily updates with the latest changes in short positions. </p>
			</div>
			<div class="container" style="background-color:white">
				<input id="mail" onkeypress="ValidateEmail(this)" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" name="email" type="text" placeholder="Enter your email address" name="mail" required>
				<div class="d-flex flex-column">
					<div class="font-weight-bold mt-2">
						Single separate newsletters:
					</div>

					<?php 
					$newsletters = readJSON('../../newsletter/newsletter_settings/newsletter_index.json');

					foreach ($newsletters as $key => $newsletter)
					{
						if ($newsletter['type'] != 'single')
						{
							continue;
						}
						?>
						<div>
							<input type="checkbox" checked name="<?php echo mb_strtolower($newsletter['name']); ?>"> <?php echo ucwords($newsletter['name']) . ' - ' . $newsletter['description']; ?>
						</div>

						<?php
					}

					?>
					<div class="font-weight-bold mt-4">
						Choose countries in which to include into one daily newsletter:
					</div>
					<?php 
					

					foreach ($newsletters as $key => $newsletter)
					{
						if ($newsletter['type'] != 'multi')
						{
							continue;
						}
						?>
						<div>
							<input type="checkbox" name="<?php echo $newsletter['name']; ?>"> <?php echo ucwords($newsletter['name']) . ' - ' . $newsletter['description']; ?>
						</div>

						<?php
					}

					?>
					<input type="hidden" name="source" value='<?php echo $source; ?>'> 
						
				</div>
			</div>

			<div class="container">
				<input id="submit-button" type="submit" class="btn btn-info" value="Subscribe"/>
			</div>
		</form>
	</div>
</body>
</html>

<script type="text/javascript">

	function ValidateEmail(inputText)
	{
		var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

		if (inputText.value.match(mailformat))
		{
      //alert("Valid email address!");
      //document.form1.text1.focus();


      $('input[type="submit"]').removeAttr('disabled');
      $( "#submit-button" ).removeClass( "btn-info");
      $( "#submit-button" ).addClass( "btn-success");

      console.log('Valid!');
      return true;
  }
  else
  {
      //alert("You have entered an invalid email address!");
      //document.form1.text1.focus();
      $( "#submit-button" ).removeClass( "btn-success");
      $( "#submit-button" ).addClass( "btn-info");
      $('input[type="submit"]').attr('disabled','disabled');
      return false;
  }
}

$('input[type="submit"]').attr('disabled','disabled');

$('#mail').focus()

</script>

<?php
function readJSON($file_location_and_name) {

	if (file_exists($file_location_and_name))
	{
		$data = file_get_contents($file_location_and_name);
		return $data = json_decode($data, true);
	}
	else
	{
            //errorecho('Error, file ' . $file_location_and_name .  ' does not exist<br>');
		return false;
	}

}

?>
