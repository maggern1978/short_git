<!DOCTYPE html>
<html lang="en">
<head>
  <title>Newsletter settings</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<?php

$error_message = 'Email could not be verified. Please try again.';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (!isset($_GET["code"]))
  {
  	echo $error_message;
  	return;
  }

  if (!isset($_GET["secret"]))
  {
  	echo $error_message;
  	return;
  }

  $code = test_input($_GET["code"]);
  $secret = test_input($_GET["secret"]);
  
}

include '../../production_europe/functions.php';

if (!file_exists('../../newsletter/users/confirmed/' . $code))
{
	echo 'Error. Cannot access settings. Please contact us.';
	return;
}

//read settings
if (!$data = readJSON('../../newsletter/users/confirmed/' . $code .'/' . $code . '.json'))
{
  echo 'Error. Cannot access settings. Please contact us.';
  return;
}

if ($data['secret'] != $secret)
{
  echo 'Error. Cannot access settings. Please contact us.';
  return;
}

$newsletters = readJSON('../../newsletter/newsletter_settings/newsletter_index.json');

?>
<style type="text/css">
  .container {
    padding: 20px;
    padding-bottom: 5px;
    background-color: #f1f1f1;
  }

</style>

<script>

var js_array =<?php echo json_encode($newsletters );?>;
var code = '<?php echo $code; ?>';
var secret = '<?php echo $secret; ?>';

//console.log(js_array);

  function update() 
  {   

    length = js_array.length; 

    var string = '';

    for (i = 0; i < length; i++)
    {

      if ($('#' + js_array[i]['name']).prop("checked"))
      {
        //console.log(js_array[i]['name'] + ' checked!');

        string += js_array[i]['name'] + ',';

      }

    }   

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("result").innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET", "update.php?newsletters=" + string + '&code=' + code + '&secret=' + secret, true);
    xmlhttp.send();
  }

</script>
</head>
<body>

  <div class="container mt-3">
    <div class="row">
      <div class="col-12">
        <h3>Newsletter settings for <?php echo $data['email']; ?> </h3>
        <div class="d-flex flex-column">
          <div class="font-weight-bold mt-2">
            <h4>Subscriptions:</h4>
          </div>
          

          <form action="update.php">

              <div class="font-weight-bold mt-2">
              Single separate newsletters:
              </div>

            <?php 

            foreach ($newsletters as $key => $newsletter)
            {

              if ($newsletter['type'] != 'single')
              {
                continue;
              }

              if (in_array(mb_strtolower($newsletter['name']), $data['subscriptions']))
              {

                ?>
                <div class="mb-1">
                  <input id="<?php echo $newsletter['name']?>" type="checkbox" checked name="<?php echo mb_strtolower($newsletter['name']); ?>"> <?php echo ucwords($newsletter['name']) . ' - ' . $newsletter['description']; ?>
                </div>

                <?php
              }
              else
              {
                ?>

                <div class="mb-1">
                  <input id="<?php echo $newsletter['name']?>" type="checkbox" name="<?php echo mb_strtolower($newsletter['name']); ?>"> <?php echo ucwords($newsletter['name']) . ' - ' . $newsletter['description']; ?>
                </div>

                <?php          
              }

            }

            ?>

            <div class="font-weight-bold mt-4">
            Choose countries in which to include into one daily newsletter:
            </div>

            <?php

            foreach ($newsletters as $key => $newsletter)
            {

              if ($newsletter['type'] != 'multi')
              {
                continue;
              }
              
              if (in_array(mb_strtolower($newsletter['name']), $data['subscriptions']))
              {

                ?>
                <div class="mb-1">
                  <input id="<?php echo $newsletter['name']?>" type="checkbox" checked name="<?php echo mb_strtolower($newsletter['name']); ?>"> <?php echo ucwords($newsletter['name']) . ' - ' . $newsletter['description']; ?>
                </div>

                <?php
              }
              else
              {
                ?>

                <div class="mb-1">
                  <input id="<?php echo $newsletter['name']?>" type="checkbox" name="<?php echo mb_strtolower($newsletter['name']); ?>"> <?php echo ucwords($newsletter['name']) . ' - ' . $newsletter['description']; ?>
                </div>

                <?php          
              }

            }


            ?>
            <div class="my-2">
              <button type="button" onclick="update()" class="btn btn-success">Update</button>
            </div>
            <div id="result">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
<?php


//funksjon for å rense data
function test_input($data) 
{
  $data = filter_var($data, FILTER_SANITIZE_STRING);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


?>