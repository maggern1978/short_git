<?php

include '../../newsletter/bootstrap4.top.html';

$error_message = 'Email could not be verified. Please try again.';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (!isset($_GET["code"]))
  {
  	echo $error_message;
  	return;
    include '../../newsletter/bootstrap4.bottom.html';
  }

  if (!isset($_GET["secret"]))
  {
  	echo $error_message;
  	return;
    include '../../newsletter/bootstrap4.bottom.html';
  }

  $code = test_input($_GET["code"]);
  $secret = test_input($_GET["secret"]);
  
}

include '../../production_europe/functions.php';

if (file_exists('../../newsletter/users/confirmed/' . $code))
{
	echo 'Email already verified.';
  include '../../newsletter/bootstrap4.bottom.html';
	return;
}

if (!file_exists('../../newsletter/users/unconfirmed/' . $code))
{
	echo $error_message . '<br>';
	echo $code . ' does not exist.';
  include '../../newsletter/bootstrap4.bottom.html';
	return;
}

//find settings
$data = readJSON('../../newsletter/log/log.json');

$found = 0; 

$settings = 0;

foreach ($data as $key => $input)
{

	if ($input['hash'] == $code and $input['secret'] == $secret)
	{
		$settings = $input;
		$found = 1; 
	}

}

if ($found == 0)
{
	echo $error_message;
	echo '<br>Start process from scratch.';
  include '../../newsletter/bootstrap4.bottom.html';
  return;
}


if (!file_exists("../../newsletter/users/confirmed/$code"))
{
	mkdir("../../newsletter/users/confirmed/$code");
}

if(file_exists('../../newsletter/users/unconfirmed/' . $code) and !rmdir('../../newsletter/users/unconfirmed/' . $code)) {
  echo ("Error, could not remove id.");
  include '../../newsletter/bootstrap4.bottom.html';
  return;
}

//move folder and save settings file
saveJSONsilent($settings, '../../newsletter/users/confirmed/' . $code . '/' . $code .  '.json');

echo 'Email verified successfully. Access your settings with link in welcome email.<br>';

//rename to have same sending syntax for emails
$hash_email = $code;
$email = $settings['email'];
$obj = $settings;

//send email
include '../../newsletter/send_email_welcome.php';

include '../../newsletter/bootstrap4.bottom.html';

//var_dump($settings);

//funksjon for å rense data
function test_input($data) 
{
  $data = filter_var($data, FILTER_SANITIZE_STRING);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// Function to get the user IP address
function getUserIP() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

?>