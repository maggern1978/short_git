<?php
$xmlDoc=new DOMDocument();
$xmlDoc->load("livesearch.xml");

$x=$xmlDoc->getElementsByTagName('link');

//get the q parameter from URL
//$q=$_GET["q"];

$q = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $q = test_input($_GET["q"]);
}

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

//lookup all links from the xml file if length of q>0
if (strlen($q)>0) {
  $hint="";
  for($i=0; $i<($x->length); $i++) {
    $y=$x->item($i)->getElementsByTagName('title');
    $w=$x->item($i)->getElementsByTagName('description');
    $c=$x->item($i)->getElementsByTagName('country');
    $z=$x->item($i)->getElementsByTagName('url');   

    if ($y->item(0)->nodeType==1) {
    
      //find a link matching the search text
      if (stristr($y->item(0)->nodeValue,$q)) {
        
        if ($hint=="") {
          $hint="<a href='" . 
          $z->item(0)->nodeValue . 
          "' target='_self'>" . 
          $y->item(0)->nodeValue . "</a>"
          . " (" . $w->item(0)->nodeValue . ') ';
          if (isset($c->item(0)->nodeValue)){  
           $hint = $hint . $c->item(0)->nodeValue;
          }
          

        } 
        else {
          $hint=$hint . "<br/><a href='" . 
          $z->item(0)->nodeValue . 
          "' target='_self'>" . 
          $y->item(0)->nodeValue . "</a>"
          . " (" . $w->item(0)->nodeValue . ") "   
          ; 
          if (isset($c->item(0)->nodeValue)){
            $hint = $hint . ' ' . $c->item(0)->nodeValue;
          }
         
          ;
        }
      }
    }
  }
}
else {
  $hint="";
}

// Set output to "no suggestion" if no hint was found
// or to the correct values
if ($hint=="") {
  $response="No hits";
} else {
  
$hint = '<div class="searchHit">' . $hint . '</div>';
  $response=$hint;
}

//output the response
echo $response;
?>