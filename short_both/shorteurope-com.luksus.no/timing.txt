﻿

Hvert 15. minutt fra 09.15 til 17.45
"control_main.php" //norway.php, denmark.php, sweden.php, common.php og intraday sweden og denmark

Hvert 5. minutt fra 09.15 til 16.55
"intraday_norway.php"

09.15 - en gang om dagen
"control_stories_morning_switcher.php" // kan kjøres etter første liveoppdatering

Hvert 15. minutt fra 09.15 til 16.45
"control_stories_live_rapport_nor.php"

Hvert 15. minutt fra 09.15 til 17.45
"control_stories_live_rapport_swe_dkk.php"

16.47 - en gang om dagen
"control_stories_end_rapport_nor.php"

17.47 - en gang om dagen
"control_stories_end_rapport_swe_dkk.php"

15:36 og 18.00 - en gang om dagen
"control_daily_history.php" // tar all historie

En gang i uken
"outstanding_shares_downloader_yahoo.php" //laster ned antall aksjer

15.35 og 16.00 (backup) - to ganger om dagen
"sweden_dl.php" //oppdaters svenske posisjoner

15.35 og 16.00 (backup)  - to ganger om dagen
"norway_downloader.php" //oppdaters norske posisjoner

