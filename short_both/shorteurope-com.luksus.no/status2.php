<!DOCTYPE html>
<html lang="en">
<head>
	<title>Status Shorteurope.com</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<style>
	
	* {
		outline: 0px solid blue;  
	}	

</style>

<body>

	<div class="container mb-0">
		<div class="col-12">
			<h5 id="feiltittel" class="text-center">Statusrapport</h5>
			<div style="color: red;" id="feilbox">
			</div>
		</div>
	</div>
	<?php
	$errorBox = [];
	include '../production_europe/functions.php';
	$countries = get_countries();
	$statscounter = 0;

	$filessources = [];

    //https://stackoverflow.com/questions/9257505/using-braces-with-dynamic-variable-names-in-php

	?>
	
	<?php

	//generate files	
	$allbox = [];

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 24;
	$object->description = 'Nedlasting av posisjoner og produksjon av current.json i /dataraw/$land/. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Builders - current.json';

	foreach ($countries as $key => $land)
	{
		
		//strrpos(string,find,start)
		$filetemplate = '../short_data/dataraw/' . $land . '/' . $land . '_current.json';
		$object->files[] = $filetemplate;

		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;
	}

	$files[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Shortlisten - kursoppdateringer med alpha.php. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Active positions - shortlisten';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/datacalc/' . 'shortlisten_' . $land . '_current.csv';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;
	}

	$files[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 24;
	$object->description = 'Eventes for selskaper i json/events/. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Events - company';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/json/events/company/' . $land . '.events.company.current.json';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;		
	}

	$files[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 24;
	$object->description = 'Eventes for playeres i json/events/. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Events - player';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/json/events/player/' . $land . '.events.player.current.json';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;		
	}

	$files[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 24;
	$object->description = 'Nedlasting og produksjon av history_current.json i dataraw $land . history. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'History';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/dataraw/' . $land . '.history/' . $land . '.history_current.json';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;			
	}

	$files[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av playerpositions.json i /datacalc/players/playerpositions.$land.current.json. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Playerpositions';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av short_data/mostShortedCompanies_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Most shorted companies';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/mostShortedCompanies_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av short_data/o-meter_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'O-meter companies';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/o-meter_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av tabell_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Tabeller';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/tabell_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av detaljer_alle_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Detaljer_alle_body';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/detaljer_alle_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av temperatures_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Temperatures';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/temperatures_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av treemap_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Treemaps';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/treemap_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av sector_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Sector';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/sector_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av positionrank_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Positionrank';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/positionrank_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av nokkeltall_venstre_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Nøkkeltall - venstre';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/nokkeltall_venstre_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av nokkeltall_hoyre_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Nøkkeltall - høyre';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/nokkeltall_hoyre_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av newestpositions_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Nyeste posisjoner';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/newestpositions_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	



	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av most_losing_calc_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Most losing';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/most_losing_calc_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av most_earning_calc_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Most earning';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/most_earning_calc_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	



	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av most_betting_players_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Most betting';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/most_betting_players_bf4_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av heatmap_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Heatmap';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/heatmap_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av five_biggest_positions_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Fem største posisjoner';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/five_biggest_positions_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av days_to_cover_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Days to cover';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/days_to_cover_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av country_banner_$land.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Country banner';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/country_banner_' . $land . '.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av winner_loser_display_$land_WINNER.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Winners_loser display winner';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/winner_loser_display_' . $land . '_winner.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av winner_loser_display_$land_LOSER.html. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Winners_loser display loser';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/html/winner_loser_display_' . $land . '_loser.html';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av nokkeltall_$land.csv. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Nøkkeltall_ $land.csv';

	foreach ($countries as $key => $land)
	{
		$filetemplate = '../short_data/datacalc/nokkeltall/nokkeltall_' . $land . '.current.csv';
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	



	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av produksjon/intraday/*.json Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'intraday grunnlagsdata';

	$tempfiles = array_diff(scandir('../short_data/intraday/'), array('.', '..'));

	foreach ($tempfiles as $key => $file)
	{
		$filetemplate = '../short_data/intraday/' . $file;
		$object->files[] = $filetemplate;
		
		$pos = strrpos($filetemplate, "/",0);
		$description = substr($filetemplate, $pos+1);
		$object->filesDescription[] = $description;				
	}

	$files[] = (array) $object;
	//----------------------------------------	


	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 1;
	$object->description = 'Produksjon av produksjon/html/intrday*land.html Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Grafer - intraday';

	$tempfiles = array_diff(scandir('../short_data/html/'), array('.', '..'));



	foreach ($tempfiles as $key => $file)
	{
		if (strpos($file, '.html',0))
		{

			$filetemplate = '../short_data/html/' . $file;
			$object->files[] = $filetemplate;

			$pos = strrpos($filetemplate, "/",0);
			$description = substr($filetemplate, $pos+1);
			$object->filesDescription[] = $description;				

		}
	}

	$files[] = (array) $object;
	//----------------------------------------	



	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 24;
	$object->description = 'Produksjon av ../short_data/dataraw/stock.prices/ . $country. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'History hovedfil allpositions.';


	foreach ($countries as $country)
	{
		$tempfiles[] = '../short_data/dataraw/stock.prices/' . $country . '/all_positions_' . $country . '.json';
	}


	foreach ($tempfiles as $key => $file)
	{
		if (strpos($file, '.json',0))
		{

			$filetemplate = $file;
			$object->files[] = $filetemplate;

			$pos = strrpos($filetemplate, "/",0);
			$description = substr($filetemplate, $pos+1);
			$object->filesDescription[] = $description;				

		}
	}

	$files[] = (array) $object;
	//----------------------------------------


	$totalrunner = 0;

	?>
	<div class="container-fluid">
		<div class="row">
			<?php

	//main round
			foreach ($files as $key => $object)
			{

				?>

				<script>var errors<?php echo $statscounter; ?> = 0; </script>
				<div class="col-12 col-md-6 class="border"">
					<div class="border d-flex flex-row bd-highlight mb-0  align-items-center">
						<div class="p-1"><span class="h6"><?php $temp = $key + 1; echo $temp; ?>.</span></div>
						<div class="p-1 w-50 "><span class="h6"><?php echo $object['title']; ?> </span></div>
						<div class="p-1 px-2 ml-2 border " id="tittel<?php echo $statscounter; ?>"></div>
						<div class="my-auto "><button onclick="toggleshow(<?php echo $statscounter; ?>)" class=" ml-2 btn btn-secondary btn-block " id="button<?php echo $statscounter; ?>">Vis</button></div>
						<div class="p-1 ml-3  " id="beskrivelse<?php echo $statscounter; ?>"></div>
					</div>

					<div id="table<?php echo $statscounter; ?>">
						<h6><?php echo $object['description']; ?> </h6>    
						<table  class="table table-striped table-sm">
							<thead>
								<tr>
									<th>#</th>
									<th>Fil</th>
									<th>Status</th>
									<th>Sist oppdatert</th>
									<th>Timestamp</th>
									<th>Filstørrelse</th>
								</tr>
							</thead>
							<tbody>
								<script>
									var latestfiletime<?php echo $statscounter; ?> = 0 
									var $timemessage<?php echo $statscounter; ?> =  '';
								</script>
								<?php
								foreach ($object['files'] as $index => $file)
								{
							//$file = $files[$key];

									if (!$mdate = date("Y-m-d H:i:s", filemtime($file)))
									{
										$mdate = 'Error';
									}

									$ts1 = filemtime($file);
									$ts2 = strtotime("now");     
									$seconds_diff = $ts2 - $ts1;                            
									$seconds = floor($seconds_diff);
									$minutes = $seconds/60;
									$minutes = floor($minutes);
									$filesize = human_filesize(filesize($file));


									if (($minutes-5 < $object['timelimitHours']*60) and $filesize > 0.12)
									{
										$color = 'green';
										$status = 'OK';
										echo '<script>';
								//echo 'errors0++';
										echo '</script>';							
									}
									else
									{
										$color = 'red';
										$status = 'Feil';
										$errorBox[] = $file;
										echo '<script>';
										echo 'errors' . $statscounter . '++';
										echo '</script>';
									}
									?>

									<tr>
										<td><?php $runners = $index + 1; echo $runners; ?> </td>
										<td><?php echo $object['filesDescription'][$index] ;?> </td>
										<td class="font-weight-bold" style="color: <?php echo $color; ?> "><?php echo $status; ?></td>
										<td>

											<?php 

											if ($status == 'OK')
											{
												$hours = floor($minutes/60);
												$remaining_minutes = $minutes - ($hours *60);
												if ($hours > 0)
												{
													$message = $hours . ' timer og ' . $remaining_minutes . '<span class="d-block d-md-none">min</span> <span class="d-none d-md-block">minutter siden</span>';
												}
												else 
												{
													$message = $remaining_minutes . '<span class="d-block d-md-none">min</span> <span class="d-none d-md-block">minutter siden</span>';
												}

												echo $message;
											}
											else
											{

												$hours = floor($minutes/60);
												$remaining_minutes = $minutes - ($hours *60);

												if ($hours > 0)
												{
													$message = $hours . ' timer og ' . $remaining_minutes . '<span class="d-block d-md-none">min</span> <span class="d-none d-md-block">minutter siden</span>';
												}
												else 
												{
													$message = $remaining_minutes . '<span class="d-block d-md-none">min</span> <span class="d-none d-md-block">minutter siden</span>';
												}

												echo $message;
											}

											?> 
											<script>
												var latestfiletimetemp = <?php echo $seconds; ?>;
												if (latestfiletimetemp > latestfiletime<?php echo $statscounter; ?>)
												{
													latestfiletime<?php echo $statscounter; ?> = latestfiletimetemp;
													$timemessage<?php echo $statscounter; ?> = '<?php echo $message; ?>';
												}
											</script>
										</td>
										<td><?php echo $mdate; ?></td>
										<td><?php echo human_filesize(filesize($file)); ?></td>
									</tr>
									<?php 
								}
								$statscounter++;
								?>
							</tbody>
						</table>
					</div>
				</div>

				<?php 
				$totalrunner = $statscounter;
			}
//end of first section
			?>
		</div>
	</div>
	
	<?php

	$directories = [];

    //------------------------------------------------------------------------------------
	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 24;
	$object->description = 'Generator til short_data/history_shorting/$land. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Graphdata - directory';

	$filetemplate = '../short_data/history_shorting/';
	$filetemplateFiles = [];
	
	foreach ($countries as $key => $land)
	{
		$tempfiles = array_diff(scandir($filetemplate . $land), array('.', '..'));

		foreach ($tempfiles as $runner => $tempfile)
		{
			$tempfiles[$runner] = $land . '/' . $tempfile;
		}

		$filetemplateFiles[] = $tempfiles;
	}
	
	$object->files = array_flatten($filetemplateFiles);
	$object->filesDescription = '/short_data/history_shorting/';
	
	$directories[] = (array) $object;
	//----------------------------------------

	$object = new stdClass;
	$object->files = [];
	$object->timelimitHours = 24;
	$object->description = 'Nedlasting aksjehistorikk til short_data/history/. Grense: ' . $object->timelimitHours . ' timer. ';
	$object->title = 'Aksjehistorikk - directory';

	$filetemplate = '../short_data/history/';
	$filetemplateFiles = array_diff(scandir($filetemplate), array('.', '..'));

	$object->files = $filetemplateFiles;

	$object->filesDescription = '/short_data/history/';
	
	$directories[] = (array) $object;
	//----------------------------------------

	//main round
	foreach ($directories as $key => $object)
	{

		?>

		<script>var errors<?php echo $statscounter; ?> = 0; </script>
		<div class="col-12 col-md-6 class="border"">
			<div class="border p-1 d-flex flex-row bd-highlight mb-1  align-items-center">
				<div class="p-1"><span class="h6"><?php $temp = $totalrunner++ + 1; echo $temp; ?>.</span></div>
				<div class="p-1 w-50 "><span class="h6"><?php echo $object['title']; ?> </span></div>
				<div class="p-1 px-2 ml-2 border " id="tittel<?php echo $statscounter; ?>"></div>
				<div class="my-auto "><button onclick="toggleshow(<?php echo $statscounter; ?>)" class=" ml-2 btn btn-secondary btn-block " id="button<?php echo $statscounter; ?>">Vis</button></div>
				<div class="p-1 ml-3  " id="beskrivelse<?php echo $statscounter; ?>"></div>
			</div>
			
			<div id="table<?php echo $statscounter; ?>">
				<h6><?php echo $object['description']; ?> </h6>    
				<table  class="table table-striped table-sm">
					<thead>
						<tr>
							<th>Mappe</th>
							<th class=" text-right">I dag</th>
							<th class=" text-right">I går</th>
							<th class=" text-right">-2 dager</th>
							<th class=" text-right">Eldre</th>
						</tr>
					</thead>
					<tbody>
						<script>
							var latestfiletime<?php echo $statscounter; ?> = 0 
							var $timemessage<?php echo $statscounter; ?> =  '';
						</script>
						<?php

						$today  = $today  = date("Y-m-d");
						$yesterday = date("Y-m-d", strtotime("-1 day"));
						$todaysago = date("Y-m-d", strtotime("-2 day"));
						//echo $yesterday;
						
						$todaycounter = 0;
						$yesterdaycounter = 0;
						$todaysagocounter = 0;
						$oldercounter = 0;
						$errorcount = 0;
						$totalcount = 0;
						
						
						foreach ($object['files'] as $index => $file)
						{


							$filelocation = '..' . $object['filesDescription'] . $file;

							$mdate = date("Y-m-d", filemtime($filelocation));

							if ($mdate == $today)
							{
								$todaycounter++;
							}
							else if ($mdate == $yesterday)
							{
								$yesterdaycounter++; 
							}
							else if ($mdate == $todaysago)
							{
								$todaysagocounter++; 
							}
							else
							{
								$oldercounter++;
								echo '<script>';
								echo 'errors' . $statscounter . '++';
								echo '</script>';
							}
							$totalcount++;
						}
						


						$color = 'green';                

						?>

						<script>

							$timemessage<?php echo $statscounter; ?> = '<?php echo $oldercounter . ' old files, ' . $todaycounter . ' from today'; ?>';
							
						</script>					    

						<tr>
							<td>/production_europe/history/</td>
							<td class=" text-right font-weight-bold" style="color: <?php echo $color; ?>"><?php echo $todaycounter;?> </td>
							<td class=" text-right"><?php echo $yesterdaycounter; ?></td>
							<td class=" text-right font-weight-bold" style="color: red;"><?php echo $todaysagocounter; ?></td>
							<td class=" text-right font-weight-bold" style="color: red;"><?php echo $oldercounter; ?></td>
						</tr>

						<?php 
						
						$statscounter++;
						?>
					</tbody>
				</table>
			</div>
		</div>
		<?php 

	}

	?>		
</div>
</div>



<div class="container-fluid mx-5 my-4">
	<div class="row">
		<div class="col-12">
			<h4>Duplicater i dataraw: </h4>
		
		<?php 

		$date_today = date('Y-m-d');		
		$date_minus_1 = date('Y-m-d', strtotime("-1 weekday", strtotime($date_today)));
		$date_minus_2 = date('Y-m-d', strtotime("-2 weekday", strtotime($date_today)));

		$directory = '../short_data/dataraw/';
		$anyfound = 0;

		foreach ($countries as $key => $country)
		{
			$display_country = 0;
			$files = listfiles($directory . $country .'/');

			//get an array of filee hashes
			$hashArr = [];
			    foreach ($files as $file) 
			    {
			    if (is_file(($directory . $country . '/' . $file)))
			    {
				    $fileHash = md5_file($directory . $country . '/' . $file);
				    $hashArr[$file] = $fileHash;
			    }
			
			}

			//search for duplicates
			$arr_unique = array_unique($hashArr);
			$arr_duplicates = array_diff_assoc($hashArr, $arr_unique);


			foreach ($arr_duplicates as $key => $duplicate)
			{
			
				if (!strpos($key, "curr") !== false)
				{
					
					//if dates are close to now
					if (strpos($date_today, "curr") !== false or strpos($date_minus_1, "curr") !== false or strpos($date_minus_2, "curr") !== false)
					{
						//echo $key . ' | ';
						if ($display_country == 0)
						{
							echo '<b>' . ucwords($country) . '</b><br>';
							$display_country = 1;
							$anyfound = 1;
						}						
						errorecho($key . ' ' . $duplicate . '<br>');
					}

					
				}

			}

		}
		if ($anyfound == 0)
		{
			successecho ('No duplicates found.<br>');
		}
		?>

		</div>
	</div>
</div>


<?php
$file = '../production_europe/error_log';

if (file_exists($file))
{
	
	?>
	<div class="container-fluid mx-5 my-4">
		<div class="row">
			<div class="col-12">
				<?php
				echo '<strong>30 newest error messages from ' . $file . ': </strong><br>';
				$fh = fopen($file, "r");
				fseek($fh, -8192, SEEK_END);
				$lines = array();
				while($lines[] = fgets($fh)) {}

					$lines = array_reverse($lines);

				foreach($lines as $key => $line)
				{
					if ($key == '')
					{
						continue;
					}

					echo $key . '. ' . $line . '<br>';

					if ($key > 29)
					{
						break;
					}
				}
				?>
			</div>
		</div>
	</div>
	<?php
}

$file = '../short_data/error_log';

if (file_exists($file))
{
	
	?>
	<div class="container-fluid mx-5">
		<div class="row">
			<div class="col-12">
				<?php
				echo '<strong>30 newest error messages from ' . $file . ': </strong><br>';
				$fh = fopen($file, "r");
				fseek($fh, -8192, SEEK_END);
				$lines = array();
				while($lines[] = fgets($fh)) {}

					$lines = array_reverse($lines);

				foreach($lines as $key => $line)
				{
					if ($key == '')
					{
						continue;
					}

					echo $key . '. ' . $line . '<br>';

					if ($key > 29)
					{
						echo 'Break, there are more lines!';
						break;
					}
				}
				?>
			</div>
		</div>
	</div>
	<?php
}

?>

<div class="container-fluid mx-5 my-3">
	<div class="row">
		<div class="col-12">
			<?php
			$files = listfiles('../production_europe/isin_adder/misslist_active/');
			echo '<strong>Misslist index (all files): </strong><br>';
			$now = time();

			$timestamps = [];

			foreach($files as $key => $filename)
			{
				$obj = new stdClass;
				$obj->time = filemtime('../production_europe/isin_adder/misslist_active/' . $filename);
				$obj->name = $filename;
				$obj->timestamp = date("Y-m-d H:i:s", $obj->time);
				$timestamps[] = (array)$obj;


			}

			usort($timestamps, function ($item1, $item2) {
				return $item2['time'] < $item1['time'];
			});

			foreach($timestamps as $key => $entry)
			{

				$index = $key + 1; 

				if ($now - $entry['time'] < 3600*12)
				{
					echo $index . '. ' . $entry['name'] . ' | Timestamp: ' . $entry['timestamp'] .'<br>';
				}
			}
			?>
		</div>
	</div>
</div>



<br><br><br>	
</body>
</html>
<script>

	var statscounter = <?php echo $statscounter;?>;

	console.log('Statscounter er ' + statscounter);

	for (i = 0; i < statscounter; i++) 
	{
		console.log('Feil: ' + i + ' ' + window['errors'+ i]);

		if (window['errors'+ i] > 0)
		{
			$('#tittel'+ i).append('<span class="text-white bg-danger h6"> ' + window['errors'+ i] + ' feil' + '</span>');
			$("#tittel"+ i).addClass("bg-danger ");
			$("#table" + i).hide();
		}
		else
		{
			$('#tittel' + i).append('<span class="text-white font-weight-bold my-auto h6">OK</span>'); 
			$("#tittel"+ i).addClass("bg-success ");
			$("#table" + i).hide();
		}

		$('#beskrivelse'+ i).html(window['$timemessage'+ i]);
	}

	function toggleshow(number)
	{
		$("#table"+ number).slideToggle();
		
		var divContent = $('#button'+number).text();
		
		if (divContent == 'Vis')
		{
			$('#button'+number).html('Skjul');
		}
		else
		{
			$('#button'+number).html('Vis');
		}

	}

</script>

<?php

function human_filesize($size, $precision = 2) 
{
	for ($i = 0; ($size / 1024) > 0.9; $i++, $size /= 1024) {}
		return round($size, $precision). ' ' . ['B','kB','MB','GB','TB','PB','EB','ZB','YB'][$i];
}

function array_flatten($array) 
{

	$return = array();
	foreach ($array as $key => $value) 
	{
		if (is_array($value)){ $return = array_merge($return, array_flatten($value));}
		else {$return[$key] = $value;}
	}
	return $return;

}

?>