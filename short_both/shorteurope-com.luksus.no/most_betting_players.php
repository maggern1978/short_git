<?php include 'header.html'; 

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
}

require '../production_europe/namelink.php';

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

$playerliste = '../short_data/datacalc/players/' . $land . '/playerpositions.' . $land . '.current.json';

include '../production_europe/input_check_country.php';
include '../production_europe/options_set_currency_by_land.php';
$land_navn = ucwords($land);

$land_navn = ucwords($land_navn);

$string = file_get_contents($playerliste);
$string = json_decode($string, true);  


usort($string, function ($item1, $item2) {
    return $item2['totalMarketValue'] <=> $item1['totalMarketValue'];
});


$pageTitle = 'Biggest shorters' . ' - ' . $land_navn;

$description = 'Biggest shorters' . ' in ' . $land_navn . ' ranked by value. ';

?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row ">
<div class="col-12">
    <h2 class="pb-2">Biggest short players by value <?php echo $land_navn . ' in ' .  $currency_ticker; ?> </h2> 
    <table class="table table-bordered table-striped table-hover table-sm">
        <thead class="thead-dark">
          <tr>
            <th>#</th>
            <th>Name</th>
            <th class="text-right" style="width:110px;"># of positions</th>
            <th class="text-right" style="width:110px;">Value</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $counter = 1;
        $descriptionTemp = '';
        foreach ($string as $key => $player)  {
            //var_dump($player);



            //TELL ANTALL POSISJONER
            $positionCounter = 0;
            foreach ($player['positions'] as $posisjon) {
                $positionCounter++;
            }

            $shorter = nametolink($player['playerName']);
            echo '<tr>';
            echo '<td>' . $counter . '.</td>';
            echo '<td>' . '<a href="details.php?player=' . $shorter . '&land=' . $land . '">';
            //$shorter = linktoname($shorter);

            $shorter   = strtolower($player['playerName']) ;
            $shorter  = ucwords($shorter  );
            echo  $shorter . '</a>' . '</td>';
            echo '<td class="text-right" style="width: 90px;">' . $positionCounter . '</td>';
            echo '<td class="text-right" style="width: 80px;">' . number_format($player['totalMarketValue'],0,",",".") . ' M</td>';
            echo '</tr>';
            
            $playerName = ucwords(strtolower($player['playerName']));

            if ($key < 2) {
                $descriptionTemp = $descriptionTemp . $playerName  . ' (' . $positionCounter . ' positions worth ' . number_format($player['totalMarketValue'],0,",",".") . ' million ' . $currency_ticker . '), ';
            }
            if ($key == 2) {
                $descriptionTemp = $descriptionTemp . 'and ' . $playerName  . ' (' . $positionCounter . ' positions worth ' . number_format($player['totalMarketValue'],0,",",".") . ' million ' . $currency_ticker . '). ';
            }

            $counter++;
        }

        $description = $description . 'Top three shorters are ' . $descriptionTemp;

        ?>
    </tbody>
</table>
</div>
</div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12">
       <?php
           echo "Updated: " . date("Y-m-d H:i",filemtime($playerliste));
        ?>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>
 </main>
  </body>
</html>