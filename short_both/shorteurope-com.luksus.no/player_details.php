<?php include 'header.html'; ?>

<?php
require '../production_europe/namelink.php';
require '../production_europe/functions.php';

//$playernavn = $fil_teller = $i = "";
$playernavn = $selskapsnavn  = $land = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $playernavn = test_input($_GET["player"]);
  $selskapsnavn  = test_input($_GET["selskapsnavn"]);
  $land = test_input($_GET["land"]);
 
}


$playerNavnLink = strtoupper($playernavn);
$playernavn = linktoname($playernavn);
$playernavn = trim(strtoupper($playernavn));
$startUrl = 'history_player.php?player=';
$endUrl = '&land='; 
$linkUrl = $startUrl . nametolink($playernavn) . $endUrl;

$pageTitle = $playernavn ;

//velg land som skal være med:
//$land_array = ['nor', 'swe', 'fin', 'dkk', 'is'];
$land_array = ['norway', 'sweden', 'denmark', 'finland'];
$landArrayNames = ['Norway', 'Sweden', 'Denmark', 'Finland'];

$link_array = $land_array;

$firstland = '';
$firstLandName = '';

foreach ($land_array as $key => $landMaal) {
	if ($land == $landMaal) {
		$firstland = $landMaal;
    $firstLandName = $landArrayNames[$key];
    unset($landArrayNames[$key]);
		unset($land_array[$key]);
		break;
	}
}

$lowerCaseName = strtolower($playernavn);
$lowerCaseName  = ucwords($lowerCaseName);
?>
<?php include 'ads/banner_720.html'; ?>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h1>Active positions for <?php echo $lowerCaseName; ?></h1>
      <h5>Value change last trading day only</h5>
       <a class="btn btn-primary my-2" href="<?php echo $linkUrl . $firstland; ?> " role="button">
        Player's history <?php echo $firstLandName;?></a>
      <?php foreach ($land_array as $index => $land) { ?>
      <a class="btn btn-primary my-2" href="<?php echo $linkUrl . $land; ?> " role="button">
        Player's history <?php echo $landArrayNames[$index];?></a>
      <?php } ?>

        </div>
      </div>
    </div>
  </div>
  <br>

<?php



//var_dump($land_array);
$total_eksponering_alle_land = 0;
$flere_land_switch = 0;
$success = 0;
$land = $firstland;
include 'detaljer_bit.php';

	if ($success == 1) {
		$flere_land_switch++;
	}

?>

<br>

<?php
foreach ($land_array as $key => $landMaal) {
	$land = $landMaal;
	include 'detaljer_bit.php';

	if ($success == 1) {
		$flere_land_switch++;
	}
}
?>

  <?php include 'footer.php'; ?>
  <?php include 'footer_about.html'; ?>
 </body>