<?php include 'header.html'; 

//$land = 'nor';
require '../production_europe/namelink.php';

//$selskapsnavn = $fil_teller = $i = "";
$land = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $land = test_input($_GET["country"]);
}

//funksjon for å rense data
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

$land_navn = ucwords($land);
$allPositions = '../shorteurope-com.luksus.no/dataraw/stock.prices/' . $land . '/companies/companies.index.json';

$pageTitle = 'All short positions, active and historical, sorted by company in '. $land_navn;
$description = 'Look into thousands of short positions by player and by company. Se excatly how much a player has earned over several years. ';

//les inn finanstilsynet_current.json med alle posisjoner
$string = file_get_contents($allPositions);
if (!$selskapsposisjoner = json_decode($string, true))
{
  echo 'Read error.<br>';
  return;
}


?>
<?php include 'ads/banner_720.html'; ?>
<div class="container">
  <div class="row">
    <div class="col-12">

      <h2>History for all companies in <?php echo $land_navn;?></h2>
      Summary for all available short positions:<br>
      <br>

   </div>
 </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-6">

      <?php
      $count = count($selskapsposisjoner)/2;

      $turn = 0;
      foreach ($selskapsposisjoner as $key => $selskap) {
       $selskapsnavnNew  = strtolower($selskap['StockName']);
       $selskapsnavnNew  = ucwords($selskapsnavnNew);
       $nameNew = nametolink($selskapsnavnNew);

       echo '<div class="mb-1">';
       echo '<a href="' . 'history_company.php?selskapsnavn=' . 
       $nameNew . '&land=' . $land . '">';
       echo $selskapsnavnNew  . '</a> ';

       if ($selskap['numPositions'] == 1)
      {
         echo '(' . $selskap['numPositions'] . ' position)' ;
      }
      else if ($selskap['numPositions'] > 1)
      {
         echo '(' . $selskap['numPositions'] . ' positions)' ;
      }
             
       echo '</div>';
       if ($count < $key || $turn == 0) {
        echo '</div>';
        echo '<div class="col-6">';
      }
    }

    ?>

  </div>
</div>
</div>
<?php include 'footer.php'; ?>
<?php include 'footer_about.html'; ?>

</main>

