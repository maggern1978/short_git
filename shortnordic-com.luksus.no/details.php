<?php include 'header.html'; 

require('../production_europe/functions.php');
require('../production_europe/logger.php');
require '../production_europe/namelink.php';


//$playernavn = $fil_teller = $i = "";
$playernavn = $selskapsnavn  = $land = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $playernavn = test_input($_GET["player"]);
  if (isset($_GET["selskapsnavn"])) {
    $selskapsnavn  = test_input($_GET["selskapsnavn"]);
  }
  $land = test_input($_GET["land"]);
}

$playernavn = linktoname($playernavn);
$playernavn = trim(strtoupper($playernavn));
$playerNavnLink = strtoupper($playernavn);
$playerNavnLink = nametolink($playerNavnLink);

$startUrl = 'history_player.php?player=';
$endUrl = '&land='; 
$linkUrl = $startUrl . $playerNavnLink . $endUrl;

$pageTitle = $playernavn . ' - all short positions';

//velg land som skal være med:
//$land_array = ['nor', 'swe', 'fin', 'dkk', 'is'];
$land_array = ['norway', 'sweden', 'denmark', 'finland', 'germany', 'italy', 'france','united_kingdom','spain'];
$landArrayNames = ['Norway', 'Sweden', 'Denmark', 'Finland', 'Germany', 'Italy', 'France', 'United Kingdom','Spain'];

//correct input to get all use cases
//take away data that are not one of the countries:
//check if country is found in array

$foundCountry = 0;
$landCount = count($land_array);

for ($i = 0; $i < $landCount; $i++) {

  if ($land_array[$i] == $land) {
    $foundCountry = '1';
    break;
  }

}

if ($foundCountry != 1) {
  echo 'Not found';
  return;
}

include '../production_europe/options_set_currency_by_land.php';

$firstLandName = '';

//ta bort første land fra array'ene og bruk første
for ($i = 0; $i < $landCount; $i++) {

  if ($land_array[$i] == $land) {
    $firstland = $land;
    $firstLandName = $landArrayNames[$i];
    unset($land_array[$i]);
    unset($landArrayNames[$i]);
    $land_array = array_values($land_array); 
    $landArrayNames = array_values($landArrayNames); 
    break;
  }

}

$lowerCaseName = strtolower($playernavn);
$lowerCaseName  = ucwords($lowerCaseName);
$aggregatedNumberOfPositions = 0;
$aggregatedPositionValues = 0;
$aggregatedLatestValueChange = 0;
$aggregatedTotalValueChange = 0;
$description = 'Updated short positions for ' . $lowerCaseName . ' in companies: ';
$keywords = $lowerCaseName . ', ';
?>

<?php include 'ads/banner_720.html'; ?>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h1>All active positions for <?php echo $lowerCaseName; ?></h1>
       <a class="btn btn-primary my-2" href="<?php echo $linkUrl . $firstland; ?> " role="button">
        Player's history <?php echo $firstLandName;?></a>
      <?php foreach ($land_array as $index => $land) { ?>
      <a class="btn btn-primary my-2" href="<?php echo $linkUrl . $land; ?> " role="button">
        Player's history <?php echo $landArrayNames[$index];?></a>
      <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <br>
<?php


//var_dump($land_array);
$total_eksponering_alle_land = 0;
$flere_land_switch = 0;
$success = 0;
$land = $firstland;

include '../production_europe/detaljer_bit.php';
	if ($success == 1) {
		$flere_land_switch++;
	}

?>

<?php

foreach ($land_array as $key => $landMaal) {
	$land = $landMaal;
  include '../production_europe/options_set_currency_by_land.php';
	include '../production_europe/detaljer_bit.php';

	if ($success == 1) {
		$flere_land_switch++;
	}
}

//add in player summary here! Just get the numbers in dollar and euro.

?>
<br>

  <?php include 'footer.php'; ?>
  <?php include 'footer_about.html'; ?>

</main>
