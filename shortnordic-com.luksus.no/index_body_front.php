<?php 

switch ($mainCountrySwitch) {
  case "norway":
  $nationality = 'Norwegian';
  $adCounter = 1;
  break;
  case "sweden":
  $nationality = 'Swedish';
  $adCounter = 2;
  break;
  case "denmark":
  $nationality = 'Danish';
  $adCounter = 3;
  break;
  case "finland":
  $nationality = 'Finnish';
  $adCounter = 4;
  break;
  case "germany":
  $nationality = 'German';
  $adCounter = 5;
  break;
  case "italy":
  $nationality = 'Italian';
  $adCounter = 6;
  break;        
  case "united_kingdom":
  $nationality = 'British';
  $adCounter = 7;
  break; 
  case "france":
  $nationality = 'French';
  $adCounter = 8;
  break;         
  case "spain":
  $nationality = 'Spanish';
  $adCounter = 9;
  break;  
}

$land = $mainCountrySwitch;
$land_navn = ucwords($mainCountrySwitch);
$land_navn_lowercase = $land = strtolower($land);

?>

<div id="<?php echo $land_navn_lowercase;?>">
  <?php 
  $land = $mainCountrySwitch;
  $filename = 'country_banner_' . $land . '.html';
  include $filename;
  ?>
</div>
<br>

<div class="container mb-4">
  <div class="row">
  <div class="col-12">
  For all available statistics, go to the <?php echo $nationality; ?> <a href="<?php echo $land . '.php'; ?> ">national page.</a> 
  </div>
  </div>
</div>


<div class="container" >
  <div class="row">
    <div class="col-lg-6">
      <?php $land = $mainCountrySwitch; ?>
      <?php $filname = 'tabell_' . $land . '.html'; ?>
      <?php include $filname; ?>
    </div>
    <?php 
    $land = $mainCountrySwitch; 
    $filename = 'mostShortedCompanies_' . $land . '.html';
    include $filename; 
    ?>
  </div>
</div>
<br>
<div class="container">
  <div class="row ">
    <div class="col-12 col-sm-12 col-md-6 col-lg-4 distance meter "> 
      <?php 
      $land = $mainCountrySwitch;
      $fileName = 'o-meter_' . $land . '.html';
      include $fileName;
      ?>
    </div>
    <div class="col-12 col-sm-12 col-md-6 col-lg-4 pb-3">
      <div class=" ">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item" style="width: 36.0%;">
            <a class="nav-link active" href="#<?php echo $mainCountrySwitch;?>winner" role="tab" data-toggle="tab">
              <div style="height:9px"></div>
              <h5 class=""><span data-toggle="tooltip" title="Latest trading day">Winners</span></h5>
            </a>
          </li>

          <li class="nav-item" style="width: 63.9%;">
            <a class="nav-link" href="#<?php echo $mainCountrySwitch;?>loser" role="tab" data-toggle="tab">
             <div style="height:9px"></div>
             <h5 class="" ><span data-toggle="tooltip" title="Latest trading day">
               Losers</span>
               <span class="float-right">
                <img src="./img/<?php echo $mainCountrySwitch;?>.svg" 
                class="float-right flagg" alt="<?php echo $land_navn;?>" height="18px">
              </span>
            </h5>
          </a>
        </li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content tab-border">
        <div role="tabpanel" class="tab-pane fade in show active" id="<?php echo $mainCountrySwitch;?>winner">
          <ul class="list-group list-group-flush">
            <?php 
            $land = $mainCountrySwitch;
            $type = 'winner';
            $filenameStart = 'winner_loser_display_';
            $filename = $filenameStart . $land . '_' . $type . '.html';
            include $filename;
            ?>

          </ul>
          <a href="toplist.php?country=<?php echo $mainCountrySwitch;?>&type=winner" class="float-right">See all winners</a>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="<?php echo $mainCountrySwitch;?>loser">
          <ul class="list-group list-group-flush">
            <?php 
            $land = $mainCountrySwitch;
            $type = 'loser';
            $filenameStart = 'winner_loser_display_';
            $filename = $filenameStart . $land . '_' . $type . '.html';
            include $filename;
            ?>
          </ul>
          <a href="toplist.php?country=<?php echo $mainCountrySwitch;?>&type=loser" class="float-right">See all losers</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-12 col-md-12 col-lg-4">
    <div class="">
      <div class="card-body px-0 tab-border-4-sides tab-bottom">
       <h5 class="px-3"><span data-toggle="tooltip" title="Latest trading day">Biggest shorters</span>
         <img src="./img/<?php echo $mainCountrySwitch;?>.svg" class=" float-right flagg" alt="<?php echo $land_navn;?>" 
         height="18px"></h5>
         <ul class="list-group list-group-flush ">
          <?php 
          $filenameStart = 'most_betting_players_bf4_';
          $filenameMiddle = $mainCountrySwitch;
          $filenameEnd = '.html';
          $filename = $filenameStart . $filenameMiddle . $filenameEnd;
          include $filename; 
          ?>
        </ul>
      </div>
      <a href="most_betting_players.php?country=<?php echo $mainCountrySwitch;?>" class="float-right">See all</a>
    </div>
  </div>
</div>
</div>
<div class="container">
  <div class="row">
    <div class="col-12">
      <?php $land = $mainCountrySwitch; 
      $filename = 'temperatures_' . $land . '.html';
      //include $filename; ?>
    </div>
  </div>
</div>
<br>
<section class="section section-lg section-shaped py-2 pb-0 pt-0">
  <div class="container">
    <div class="row">
      <?php 
      $land = $mainCountrySwitch;
      $filename = 'nokkeltall_venstre_' . $land . '.html';
      include $filename; ?>
      <?php 
      $land = $mainCountrySwitch;
      $filename = 'nokkeltall_hoyre_' . $land . '.html';
      include $filename; ?> 
    </div>
  </div>
</section>


<div class="container mb-4">
  <div class="row">
    <?php $land = $mainCountrySwitch; ?>
    <?php $filename = '../production_europe/html/events/header/' . $land . '.events.header.current.html'; ?>
    <?php include $filename; ?>
  
    <?php $filename = '../production_europe/html/events/player/' . $land . '.events.player.current.html'; ?>
    <?php include $filename; ?>

    <?php $filename = '../production_europe/html/events/company/' . $land . '.events.company.current.html'; ?>
    <?php include $filename; ?>
  </div>
</div> 


<section class="section section-lg section-shaped py-2 pb-0 pt-0">
  <div class="container mb-3">
    <div class="row">
     <div class="col-12">
      <h5>Quick links <?php echo ucwords($land_navn); ?>:</h5>
    </div>
  </div>
  <div class="row ">

   <div class="col-12 col-sm-12 col-md-6 col-lg-3 margin-bottom">
    <a class="btn btn-block btn-primary" href="most_shorted_companies_value.php?country=<?php echo $mainCountrySwitch;?>" role="button">Most shorted by value</a>
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-3 margin-bottom">
    <a class="btn btn-block btn-primary" href="most_shorted_companies_all.php?country=<?php echo $mainCountrySwitch;?>" role="button">Most shorted in %</a>
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-3 margin-bottom">
    <a class="btn btn-block btn-primary" href="details_company_all.php?&land=<?php echo $land;?>" role="button">Active shorts</a>
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-3 margin-bottom">
    <a class="btn btn-block btn-primary" href="most_positions.php?country=<?php echo $mainCountrySwitch;?>" role="button">Most positions</a>
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-3 margin-bottom">
    <a class="btn btn-block btn-primary" href="log.php?country=<?php echo $mainCountrySwitch?>&index=0" role="button">Changelog</a>
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-3 margin-bottom">
    <a class="btn btn-block btn-primary" href="days_to_cover_index.php?country=<?php echo $mainCountrySwitch;?>" role="button">Days-to-cover-ratio</a>
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-3 margin-bottom">
    <a class="btn btn-block btn-primary" href="heatmap_display.php?&country=<?php echo $mainCountrySwitch;?>" role="button">Shorting analysis</a>
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-3">
    <a class="btn btn-block btn-primary" href="positionrank.php?&country=<?php echo $mainCountrySwitch;?>" role="button">Positions rank</a>
  </div>
</div>
</div>
</section>
<!--
<div class="container pb-4">
  <div class="row ">
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
      <?php 
      $filePath = 'ads/index/' . $adCounter . '.html';
      //include $filePath; 
      ?>
    </div>
  </div>
</div>
-->


<div class="container mb-3">
  <div class="row ">
    <div class="col-12 col-sm-12 col-md-6 col-lg-4 mb-3">
      <div class="pb-2">
       <h5><span data-toggle="tooltip" title="Days since the companies aggregated short position went from zero to a positive value.">Newest shorted companies</span>
       </div>

       <?php 
       $filenameStart = 'newestpositions_';
       $filenameMiddle = $mainCountrySwitch;
       $filenameEnd = '.html';
       $filename = $filenameStart . $filenameMiddle . $filenameEnd;
       include $filename; 
       ?>
       <div class="mt-2">
        <a href="newpositions.php?country=<?php echo $mainCountrySwitch;?>" class="float-right">See full list</a>
      </div>
    </div>
    <div class="col-12 col-sm-12 col-md-6 col-lg-4 mb-3">
      <div class="card-body py-3 bg-grey  text-white">
       <h5 class="px-0 text-white"><span data-toggle="tooltip" title="List of the most shorted sectors by value">Most shorted sectors <?php echo ucwords($mainCountrySwitch);?></span>
       </h5>
       <?php 
       $filenameStart = 'sector_';
       $filenameMiddle = $mainCountrySwitch;
       $filenameEnd = '.html';
       $filename = $filenameStart . $filenameMiddle . $filenameEnd;
       include $filename; 
       ?>
     </div>
   </div>
   <div class="mt-2">
     <a href="sector.php?country=<?php echo $mainCountrySwitch;?>" class="pull-right">See all sectors and subsectors</a>
   </div>
 </div>
 <div class="col-12 col-sm-12 col-md-6 col-lg-4 mb-3">
  <h5>Most earning positions since start</h5> 
  <?php 
  $filenameStart = 'positionrank_';
  $filenameMiddle = $mainCountrySwitch;
  $filenameEnd = '.html';
  $filename = $filenameStart . $filenameMiddle . $filenameEnd;
  include $filename; 
  ?>
  <div class="mt-2">
    <a href="positionrank.php?&country=<?php echo $mainCountrySwitch;?>" class="pull-right">Se all</a>
  </div>
</div>

</div>
</div>
</div>

<div class="container mb-3">
  <div class="row py-2 ">
    <div class="col-12">
      <div class="card text-white bg-grey mb-0">
        <div class="card-body text-center">
          <span class="h2 card-title text-white">History: See all positions in the 
            <?php echo $nationality;?> market <br class="d-none d-lg-block">sorted by 
            <a class="text-white " href="history_player_index.php?country=<?php echo $land;?>"><i class="fa fa-arrow-circle-right"></i> player</a> 
            or <a class="text-white" href="history_company_index.php?country=<?php echo $land;?>">
            <i class="fa fa-arrow-circle-right"></i> company</a></span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>


