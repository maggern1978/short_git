<?php include 'header_original.html'; ?>

<div class="section-first bg-gradient-blueish">
</div>

<?php 
$countries = ['norway', 'sweden', 'denmark', 'italy', 'united_kingdom', 'germany','spain'];
shuffle($countries);
?>


<section class="section section-lg bg-gradient-blueish pb-3 pt-0">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3">
        <div class="card">
          <div class="card-body"> 
            <?php include '../production_europe/html/stories/story_' . $countries[0] . '_current.html';?>
              <a href="#<?php echo $countries[0]; ?> " class="">
              <span class="pull-right font-weight-bold">Read more</span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3 ">
        <div class="card">
          <div class="card-body"> 
            <?php include '../production_europe/html/stories/story_' . $countries[1] . '_current.html';?>
              <a href="#<?php echo $countries[1]; ?> " class="">
              <span class="pull-right font-weight-bold">Read more</span>
            </a>
          </div>
        </div>
      </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3">
      <div class="card">
        <div class="card-body">
            <?php include '../production_europe/html/stories/story_' . $countries[2] . '_current.html';?>
            <a href="#<?php echo $countries[2]; ?> " class="">
            <span class="pull-right font-weight-bold">Read more</span>
          </a>
        </div>
      </div>  
    </div><!--
    <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3">
      <div class="card">
        <div class="card-body">
            <?php //include '../production_europe/html/stories/story_denmark_current.html';?>
            <a href="#denmark" class="">
            <span class="pull-right font-weight-bold">Read more</span>
          </a>
        </div>
      </div>  
    </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3">
      <div class="card">
        <div class="card-body">
            <?php //include '../production_europe/html/stories/story_finland_current.html';?>
            <a href="#finland" class="">
            <span class="pull-right font-weight-bold">Read more</span>
          </a>
        </div>
      </div>  
    </div>-->
  </div>
</div>
</div>
</section>

<div class="container mb-3 mt-3">
  <div class="row">
    <div class="col-12">
    <div class="d-flex justify-content-center text-center summary-text">
      <?php include '../production_europe/html/summary_positions_tracked.html'; ?>
     </div>
   </div>
 </div>
</div>

<?php include 'ads/banner_front.html'; ?>

<!--
<br>
<div class="container" >
    <div class="row">
    <div class="col-12 col-sm-6 cold-md-3 col-lg-3 pb-3"> 
      <?php //include '../production_europe/html/intraday_germany.html'; ?>
    </div>  
    <div class="col-12 col-sm-6 cold-md-3 col-lg-3 pb-3"> 
      <?php //include '../production_europe/html/intraday_sweden.html'; ?>
    </div>  
    <div class="col-12 col-sm-6 cold-md-3 col-lg-3 pb-3"> 
      <?php //include '../production_europe/html/intraday_norway.html'; ?>
    </div>  

    <div class="col-12 col-sm-6 cold-md-3 col-lg-3 pb-3"> 
      <?php //include '../production_europe/html/intraday_denmark.html'; ?>
    </div>  
    </div>
</div>


-->





<?php //include 'open.html'; ?>

<br>


<div class="container">
  <?php include 'banner_nordic.html'; ?>
</div>

<br>
<br>

<?php 
    $mainCountrySwitch = 'germany';
    include 'index_body_front.php'; 
    $mainCountrySwitch = 'united_kingdom';
    include 'index_body_front.php';   
    $mainCountrySwitch = 'france';    
    include 'index_body_front.php';   
    $mainCountrySwitch = 'italy';
    include 'index_body_front.php';      
    $mainCountrySwitch = 'spain';
    include 'index_body_front.php';   
    $mainCountrySwitch = 'sweden';
    include 'index_body_front.php'; 
    $mainCountrySwitch = 'norway';
    include 'index_body_front.php'; 
    $mainCountrySwitch = 'denmark';
    include 'index_body_front.php'; 
    $mainCountrySwitch = 'finland';
    include 'index_body_front.php'; 
   
?>
<!--
<div class="container my-2">
  <div class="row ticker-padding justify-content-center">
    <?php // include 'banner_international.html'; ?>
  </div>
</div>

  <div class="container mt-5">
    <div class="row">     
      <div class="col-12">
        <h3>End rapports from two previous days</h3>
      </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3">
        <div class="card shadow ">
          <div class="card-body"> 
            <?php // include 'stories/endrapport/1.html';?>
              <a href="#norway" class="">
              <span class="pull-right font-weight-bold">Read more</span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3 ">
        <div class="card shadow ">
          <div class="card-body"> 
            <?php // include 'stories/endrapport/2.html';?>
              <a href="#sweden" class="">
              <span class="pull-right font-weight-bold">Read more</span>
            </a>
          </div>
        </div>
      </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3">
      <div class="card shadow ">
        <div class="card-body">
            <?php // include 'stories/endrapport/3.html';?>
            <a href="#denmark" class="">
            <span class="pull-right font-weight-bold">Read more</span>
          </a>
        </div>
      </div>  
    </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3">
        <div class="card shadow ">
          <div class="card-body"> 
            <?php // include 'stories/endrapport/4.html';?>
              <a href="#norway" class="">
              <span class="pull-right font-weight-bold">Read more</span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3 ">
        <div class="card shadow ">
          <div class="card-body"> 
            <?php // include 'stories/endrapport/5.html';?>
              <a href="#sweden" class="">
              <span class="pull-right font-weight-bold">Read more</span>
            </a>
          </div>
        </div>
      </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-4 pt-3">
      <div class="card shadow ">
        <div class="card-body">
            <?php // include 'stories/endrapport/6.html';?>
            <a href="#denmark" class="">
            <span class="pull-right font-weight-bold">Read more</span>
          </a>
        </div>
      </div>  
    </div>
  </div>
</div>
</div>
-->
<br>
  <?php $pageTitle = 'ShortEurope - home page'; ?>
  <?php include 'footer.php'; ?>
  <?php include 'footer_about.html'; ?>

 <br>
 </main>
  </body>
</html>